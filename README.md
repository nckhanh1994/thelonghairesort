server {
	listen 80;
	server_name thuenhare.net;
	access_log off;
	error_log /data/www/thuenha/logs/error.log;
	root /data/www/thuenha/html/public;
	location / {
	   if (!-f $request_filename) {
		rewrite ^(.*)$ /index.php?q=$1 last;
		break;
	   }
	}

	location ~ /\.{deny  all;}
	error_page 403 @maintain;
	location @maintain{
	   allow all;
	   root /data/www/thuenha/html/public/;
	   rewrite ^(.*)$ /maintain.html break;
	}
	location ~ \.php$ {
	   include /etc/nginx/fastcgi_params;
	   fastcgi_pass 127.0.0.1:9000;
	   fastcgi_param APPLICATION_ENV production;
	   fastcgi_index index.php;
	   fastcgi_param SCRIPT_FILENAME /data/www/thuenha/html/public$fastcgi_script_name;
	}
}


server {
	listen 80;
	server_name static.thuenhare.net;
	error_log /data/www/thuenha/logs/static_error.log;
	root /data/www/thuenha/html/static;
	add_header Pragma public;
	add_header Cache-Control "public, must-revalidate, proxy-revalidate";
	expires           max;
	access_log off;
	log_not_found     off;

	add_header "Access-Control-Allow-Origin" "*";

	location ~ /\.{deny  all;}

	location ~* \.(eot|ttf|woff|otf|svg)$ {
	 add_header "Access-Control-Allow-Origin" "*";
	}

	# Deny access to sensitive files.
	location ~ (\.inc\.php|\.tpl|\.sql|\.tpl\.php|\.db)$ {
	 deny all;
	}

	concat_types text/css application/javascript;

	location ~* /js {
	 concat on;
	 concat_max_files 30;
	}
	location ~* /css {
	 concat on;
	 concat_max_files 30;
	}

	location ~ /\.ht {
	 deny all;
	}
}

server {
	listen 80;
	server_name api.thuenhare.net;
	error_log /data/www/thuenha/logs/api_error.log;
	root /data/www/thuenha/html/public/;
	add_header Pragma public;
	add_header Cache-Control "public, must-revalidate, proxy-revalidate";
	access_log off;
	log_not_found off;

	location / {
	   if (!-f $request_filename) {
      rewrite ^(.*)$ /api.php?q=$1 last;
      break;
	   }
	}

	location ~ /\.{deny  all;}
	error_page 403 @maintain;
	location @maintain{
	   allow all;
	   root /data/www/thuenha/html/public/;
	   rewrite ^(.*)$ /maintain.html break;
	}
	location ~ \.php$ {
	   include /etc/nginx/fastcgi_params;
	   fastcgi_pass 127.0.0.1:9000;
	   fastcgi_param APPLICATION_ENV production;
	   fastcgi_index api.php;
	   fastcgi_param SCRIPT_FILENAME /data/www/thuenha/html/public$fastcgi_script_name;
	   # Simple requests
     #if ($request_method ~* "(GET|POST)") {
     #  add_header "Access-Control-Allow-Origin"  *;
     #}

     # Preflighted requests
     #if ($request_method = OPTIONS ) {
     #  add_header "Access-Control-Allow-Origin"  *;
     #  add_header "Access-Control-Allow-Methods" "GET, POST, OPTIONS, HEAD";
     #  add_header "Access-Control-Allow-Headers" "Authorization, Origin, X-Requested-With, Content-Type, Accept";
     #  return 200;
     #}
	}
}
