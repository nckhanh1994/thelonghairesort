<?php
  
  namespace Base\Schema;
  
  use Schema\OrganizationSchema;
  
  use Util\SchemaUtil;
  
  class Schema
  {
    private $_type = null;
    
    public function __construct($type)
    {
      $this->_type = $type;
      
      return $this;
    }
    
    public function build()
    {
      $instance = null;
      switch ($this->_type) {
        case SchemaUtil::ORGANIZATION:
          $instance = new OrganizationSchema();
          break;
        default:
          break;
        
      }
      
      return $instance->build();
    }
  }

?>
