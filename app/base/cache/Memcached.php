<?php
  
  namespace Cache;
  
  class Memcached
  {
    protected $_cache;
    protected $_options;
    protected $_isPhpCli;
    
    public function __construct()
    {
      $di = \Phalcon\DI::getDefault();
      
      $this->_cache = $di->get('cache');
      
      return $this;
    }
    
    public function read($keyCache)
    {
      try {
        if ($this->_isPhpCli === true) {
          return false;
        }
        
        return $this->_cache->get($keyCache);
      } catch (\Exception $exc) {
        if (APPLICATION_ENV !== 'production') {
          die($exc->getMessage());
        }
      }
    }
    
    public function add($keyCache, $params, $lifetime = 259200)
    {
      try {
        if ($this->_isPhpCli === true) {
          return false;
        }
//        if (isset($lifetime)) {
//          $this->_cache->setFrontend(new FrontData(['lifetime' => $lifetime]));
//        }
        return $this->_cache->add($keyCache, $params);
      } catch (\Exception $exc) {
        if (APPLICATION_ENV !== 'production') {
          die($exc->getMessage());
        }
        
        return false;
      }
    }
    
    public function increase($keyCache, $params)
    {
      try {
        if ($this->_isPhpCli === true) {
          return false;
        }
        
        return $this->_cache->increment($keyCache, $params);
      } catch (\Exception $exc) {
        if (APPLICATION_ENV !== 'production') {
          die($exc->getMessage());
        }
        
        return false;
      }
    }
    
    public function remove($keyCache)
    {
      try {
        if ($this->_isPhpCli === true) {
          return false;
        }
        
        return $this->_cache->delete($keyCache);
      } catch (\Exception $exc) {
        if (APPLICATION_ENV !== 'production') {
          die($exc->getMessage());
        }
        
        return false;
      }
    }
    
  }
