<?php
  
  namespace Base\Social;
  
  class Google
  {
    
    private $google = null;
    private $urlCallback = '';
    
    public function __construct()
    {
      $this->google = new \Google_Client();
      $this->google->setClientId("157167261602-o6kc7uni4tb1mis52l2lkri2d9h590d9.apps.googleusercontent.com");
      $this->google->setClientSecret('ZY4Uiwfgv9m9VSIohwCeFJqf');
      $this->google->addScope("email");
      $this->google->addScope("profile");
    }
    
    function getLoginUrl()
    {
      if ($this->getUrlCallback() == '') {
        return "#";
      }
      $this->google->setRedirectUri($this->getUrlCallback());
      
      return $this->google->createAuthUrl();
    }
    
    function setUrlCallback($url)
    {
      $this->urlCallback = $url;
      
      return $this;
    }
    
    function getUrlCallback()
    {
      return $this->urlCallback;
    }
    
    function getLoginResponse($code = '')
    {
      if (empty($code)) {
        return false;
      }
      try {
        $this->google->setRedirectUri($this->getUrlCallback());
        $this->google->authenticate($code);
        $accessToken = $this->google->getAccessToken();
        $this->google->setAccessToken($accessToken);
        $service = new \Google_Service_Oauth2($this->google);
        $user    = $service->userinfo->get(); //get user info
        
        return $user;
      } catch (\Google_Auth_Exception $ex) {
        return false;
      }
    }
    
    function getPremission()
    {
      return $this->premission;
    }
    
    function setPremission($premission)
    {
      $this->premission = $premission;
    }
    
  }
