<?php
  
  namespace Base\Social;
  
  class Facebook
  {
    
    private $fb = null;
    private $permission = ['email', 'public_profile'];
    private $urlCallback = '';
    
    public function __construct()
    {
      $this->fb = new \Facebook\Facebook([
        'app_id'                => '274604003200192',
        'app_secret'            => 'ec28d7110ce69c53f14ed80d2ec80252',
        'default_graph_version' => 'v3.2',
      ]);
    }
    
    function getLoginUrl()
    {
      if ($this->getUrlCallback() == '') {
        return "#";
      }
      
      $helper = $this->fb->getRedirectLoginHelper();
      
      $loginUrl = $helper->getLoginUrl($this->getUrlCallback(), $this->getPermission());
      
      return $loginUrl;
    }
    
    function getUrlCallback()
    {
      return $this->urlCallback;
    }
    
    function setUrlCallback($url)
    {
      $this->urlCallback = $url;
      
      return $this;
    }
    
    function getPermission()
    {
      return $this->permission;
    }
    
    function setPermission($permission)
    {
      $this->permission = $permission;
    }
    
    function getLoginResponse()
    {
      $helper = $this->fb->getRedirectLoginHelper();
      try {
        $accessToken = $helper->getAccessToken();
      } catch (\Facebook\Exceptions\FacebookResponseException $e) {
        echo 'Graph returned an error: ' . $e->getMessage();
        
        return false;
      } catch (\Facebook\Exceptions\FacebookSDKException $e) {
        return false;
      }
      
      if (isset($accessToken)) {
        $this->fb->setDefaultAccessToken($accessToken);
        try {
          $response = $this->fb->get('/me?fields=email,name,gender');
          $userNode = $response->getGraphUser();
        } catch (\Facebook\Exceptions\FacebookResponseException $e) {
          return false;
        } catch (\Facebook\Exceptions\FacebookSDKException $e) {
          return false;
        }
        
        return $userNode;
      }
      
      return false;
    }
    
  }
