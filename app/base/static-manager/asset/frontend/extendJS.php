<?php
  return [
    'v1' => [
      'default' => [
        'global'  => [
          ['dir' => 'user', 'name' => 'reg.js'],
        ],
        'compile' => []
      ],
      'extend'  => [
        'global'  => [
          'frontend:user:reg'      => [
            ['dir' => 'user', 'name' => 'reg.js'],
          ],
          'frontend:user:login'    => [
            ['dir' => 'user', 'name' => 'login.js'],
          ],
          'frontend:user:rent'     => [
            ['dir' => 'user', 'name' => 'rent.js'],
          ],
          'frontend:user:editrent' => [
            ['dir' => 'user', 'name' => 'rent.js'],
          ],
        ],
        'compile' => []
      ]
    ],
  ];
