<?php
  return [
    'v1' => [
      'default' => [
        'global'  => [
          ['dir' => 'user', 'name' => 'rent.css'],
        ],
        'compile' => []
      ],
      'extend'  => [
        'global'  => [],
        'compile' => []
      ]
    ],
    'v2' => [
      'default' => [
        'global'  => [],
        'compile' => []
      ],
      'extend'  => [
        'global'  => [],
        'compile' => []
      ]
    ],
  ];
