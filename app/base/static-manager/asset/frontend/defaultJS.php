<?php
  return [
    'v1' => [
      'default' => [
        'global'  => [
          ['dir' => 'global', 'name' => 'function.js'],
        ],
        'compile' => []
      ],
      'extend'  => [
      
      ]
    ],
    'v2' => [
      'default' => [
        'global'  => [
          ['dir' => 'global', 'name' => 'function.js'],
          ['dir' => 'global', 'name' => 'script.js'],
        ],
        'compile' => []
      ],
      'extend'  => [
  
      ]
    ],
  ];
