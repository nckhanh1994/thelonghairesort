<?php
  return [
    'v1' => [
      'default' => [
        'global'  => [
          ['dir' => 'app', 'name' => 'web324c.js'],
        ],
        'compile' => []
      ],
      'extend'  => [
        'global'  => [
          'frontend:user:reg'      => [
            ['dir' => 'jquery_validate', 'name' => 'jquery.validate.min.js'],
          ],
          'frontend:user:login'    => [
            ['dir' => 'jquery_validate', 'name' => 'jquery.validate.min.js'],
          ],
          'frontend:user:rent'     => [
            ['dir' => 'jquery_validate', 'name' => 'jquery.validate.min.js'],
            ['dir' => 'nile_upload', 'name' => 'nile_upload.js'],
          ],
          'frontend:user:editrent' => [
            ['dir' => 'jquery_validate', 'name' => 'jquery.validate.min.js'],
            ['dir' => 'nile_upload', 'name' => 'nile_upload.js'],
          ],
        ],
        'compile' => []
      ]
    ],
    'v2' => [
      'default' => [
        'global'  => [
          ['dir' => 'jquery', 'name' => 'jquery-3.4.1.min.js'],
          ['dir' => 'swiper', 'name' => 'swiper.min.js'],
          ['dir' => 'bootstrap3-typeahead', 'name' => 'bootstrap3-typeahead.min.js'],
        ],
        'compile' => []
      ],
      'extend'  => [
        'global'  => [
          'frontend:item:view' => [
            ['dir' => 'jquery_validate', 'name' => 'jquery.validate.min.js'],
            ['dir' => 'light_gallery', 'name' => 'lightgallery.min.js'],
          ]
        ],
        'compile' => []
      ]
    ],
  ];
