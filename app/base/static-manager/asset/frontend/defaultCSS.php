<?php
  return [
    'v1' => [
      'default' => [
        'global'  => [
          ['dir' => 'global', 'name' => 'style.css'],
        ],
        'compile' => []
      ],
      'extend'  => [
      
      ]
    ],
    'v2' => [
      'default' => [
        'global'  => [
          ['dir' => 'global', 'name' => 'style.css'],
          ['dir' => 'global', 'name' => 'responsive.css'],
        ],
        'compile' => []
      ],
      'extend'  => [
      
      ]
    ],
  ];
