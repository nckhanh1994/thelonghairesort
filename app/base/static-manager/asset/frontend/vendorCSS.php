<?php
  return [
    'v1' => [
      'default' => [
        'global'  => [
          ['dir' => 'app', 'name' => 'font-awesome.min5e1f.css'],
        ],
        'compile' => []
      ],
      'extend'  => [
        'global'  => [],
        'compile' => []
      ]
    ],
    'v2' => [
      'default' => [
        'global'  => [
          ['dir' => 'app', 'name' => 'vendor.css'],
          ['dir' => 'app', 'name' => 'theme-default.css'],
          ['dir' => 'fontawesome', 'name' => 'font-awesome.min.css'],
          ['dir' => 'swiper', 'name' => 'swiper.min.css'],
          ['dir' => 'swiper', 'name' => 'swiper.min.css'],
          ['dir' => 'bootstrap3-typeahead', 'name' => 'typeahead.css'],
        ],
        'compile' => []
      ],
      'extend'  => [
        'global'  => [
          'frontend:item:view' => [
            ['dir' => 'light_gallery', 'name' => 'lightgallery.min.css'],
          ]
        ],
        'compile' => []
      ]
    ],
  ];
