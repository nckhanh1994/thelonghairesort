<?php
  return [
    'v1' => [
      'default' => [
        'global' => [
          ['dir' => 'app', 'name' => 'vendors.bundle.js'],
        ],
        'compile' => []
      
      ],
      'extend'  => [
        'global'  => [
          'backend:item:index' => [
            ['dir' => 'bootstrap-typeahead', 'name' => 'bootstrap3-typeahead.min.js'],
          ]
        ],
        'compile' => []
      ]
    ],
  ];
