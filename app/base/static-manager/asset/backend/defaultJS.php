<?php
  return [
    'v1' => [
      'default' => [
        'global'  => [
          ['dir' => 'global', 'name' => 'scripts.bundle.js'],
        ],
        'compile' => []
      ],
      'extend'  => [
        'global'  => [
        
        ],
        'compile' => [
        
        ]
      ]
    ],
  ];
