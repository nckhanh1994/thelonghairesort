<?php
  return [
    'v1' => [
      'default' => [
        'global'  => [
          ['dir' => 'global', 'name' => 'light_1.css'],
          ['dir' => 'global', 'name' => 'light_2.css'],
          ['dir' => 'global', 'name' => 'dark_1.css'],
          ['dir' => 'global', 'name' => 'dark_2.css'],
          ['dir' => 'global', 'name' => 'custom.css'],
          ['dir' => 'global', 'name' => 'responsive.css'],
        ],
        'compile' => []
      ],
      'extend'  => [
        'global'  => [
          'backend:auth:login' => [
            ['dir' => 'global', 'name' => 'login_2.css'],
          ]
        ],
        'compile' => []
      ]
    ],
  ];
