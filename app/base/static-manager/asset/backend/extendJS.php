<?php
  return [
    'v1' => [
      'default' => [
        'global'  => [
          ['dir' => 'global', 'name' => 'app.min.js'],
        ],
        'compile' => [
          'vendor.js'
        ]
      ],
      'extend'  => [
        'global'  => [
        
        ],
        'compile' => [
          'backend:index:index' => ['index.js'],
          
          'backend:user:index' => ['user.js'],
          'backend:user:add'   => ['user.js'],
          'backend:user:edit'  => ['user.js'],
          
          'backend:role:index' => ['role.js'],
          'backend:role:add'   => ['role.js'],
          'backend:role:edit'  => ['role.js'],
          'backend:role:grant' => ['role.js'],
          
          'backend:item:index' => ['item.js'],
          'backend:item:add'   => ['item.js'],
          'backend:item:view'  => ['item.js'],
          'backend:item:edit'  => ['item.js'],
        ]
      ]
    ],
  ];
