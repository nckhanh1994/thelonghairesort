<?php
  return [
    'v1' => [
      'default' => [
        'global'  => [
          ['dir' => 'app', 'name' => 'vendors.bundle.css'],
        ],
        'compile' => []
      ],
      'extend'  => [
        'global'  => [
          'backend:item:index' => [
            ['dir' => 'vue-select', 'name' => 'vue-select.css'],
          ],
          'backend:item:add'   => [
            ['dir' => 'vue-select', 'name' => 'vue-select.css'],
            ['dir' => 'vue-dropzone', 'name' => 'vue2Dropzone.min.css'],
          ],
          'backend:item:view'  => [
            ['dir' => 'vue-select', 'name' => 'vue-select.css'],
            ['dir' => 'vue-dropzone', 'name' => 'vue2Dropzone.min.css'],
          ],
          'backend:item:edit'  => [
            ['dir' => 'vue-select', 'name' => 'vue-select.css'],
            ['dir' => 'vue-dropzone', 'name' => 'vue2Dropzone.min.css'],
          ]
        ],
        'compile' => [
        ]
      ]
    ],
  ];
