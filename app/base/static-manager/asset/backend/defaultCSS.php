<?php
  return [
    'v1' => [
      'default' => [
        'global'  => [
          ['dir' => 'global', 'name' => 'style.bundle.css'],
        ],
        'compile' => []
      ],
      'extend'  => [
        'global' => [],
        'compile' => []
      ]
    ],
  ];
