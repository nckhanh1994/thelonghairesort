<?php
  
  namespace Base\StaticManager;
  
  class StaticManager
  {
    private $_vendorCSS = [];
    private $_vendorJS = [];
    private $_defaultCSS = [];
    private $_defaultJS = [];
    private $_extendCSS = [];
    private $_extendJS = [];
    
    public function __construct()
    {
      $this
        ->setVendorCSS()
        ->setVendorJS()
        ->setDefaultCSS()
        ->setDefaultJS()
        ->setExtendJS()
        ->setExtendCSS();
    }
    
    public function setVendorCSS()
    {
      $vendorCSS = require BASE_PATH . '/static-manager/asset/' . MODULE . '/vendorCSS.php';
      
      $arrVendorCSS = !empty($vendorCSS[ TEMPLATE_VERSION ]) ? $vendorCSS[ TEMPLATE_VERSION ] : [];
      
      $key                      = 'global';
      $this->_vendorCSS[ $key ] = $arrVendorCSS['default'][ $key ];
      if (isset($arrVendorCSS['extend'][ $key ][ RESOURCE ])) {
        $this->_vendorCSS[ $key ] = array_merge($this->_vendorCSS[ $key ], $arrVendorCSS['extend'][ $key ][ RESOURCE ]);
      }
      
      $key                      = 'compile';
      $this->_vendorCSS[ $key ] = $arrVendorCSS['default'][ $key ];
      if (isset($arrVendorCSS['extend'][ $key ][ RESOURCE ])) {
        $this->_vendorCSS[ $key ] = array_merge($this->_vendorCSS[ $key ], $arrVendorCSS['extend'][ $key ][ RESOURCE ]);
      }
      
      return $this;
    }
    
    public function setVendorJS()
    {
      $vendorJS = require BASE_PATH . '/static-manager/asset/' . MODULE . '/vendorJS.php';
      
      $arrVendorJS = !empty($vendorJS[ TEMPLATE_VERSION ]) ? $vendorJS[ TEMPLATE_VERSION ] : [];
      
      $key                     = 'global';
      $this->_vendorJS[ $key ] = $arrVendorJS['default'][ $key ];
      if (isset($arrVendorJS['extend'][ $key ][ RESOURCE ])) {
        $this->_vendorJS[ $key ] = array_merge($this->_vendorJS[ $key ], $arrVendorJS['extend'][ $key ][ RESOURCE ]);
      }
      
      $key                     = 'compile';
      $this->_vendorJS[ $key ] = $arrVendorJS['default'][ $key ];
      if (isset($arrVendorJS['extend'][ $key ][ RESOURCE ])) {
        $this->_vendorJS[ $key ] = array_merge($this->_vendorJS[ $key ], $arrVendorJS['extend'][ $key ][ RESOURCE ]);
      }
      
      return $this;
    }
    
    public function setDefaultCSS()
    {
      $defaultCSS = require BASE_PATH . '/static-manager/asset/' . MODULE . '/defaultCSS.php';
      
      $arrDefaultCSS = !empty($defaultCSS[ TEMPLATE_VERSION ]) ? $defaultCSS[ TEMPLATE_VERSION ] : [];
      
      $key = 'global';
      if (isset($arrDefaultCSS['default'][ $key ])) {
        $this->_defaultCSS[ $key ] = $arrDefaultCSS['default'][ $key ];
      }
      
      $key = 'compile';
      if (isset($arrDefaultCSS['default'][ $key ])) {
        $this->_defaultCSS[ $key ] = $arrDefaultCSS['default'][ $key ];
      }
      
      return $this;
    }
    
    public function setDefaultJS()
    {
      $defaultJS = require BASE_PATH . '/static-manager/asset/' . MODULE . '/defaultJS.php';
      
      $arrDefaultJS = !empty($defaultJS[ TEMPLATE_VERSION ]) ? $defaultJS[ TEMPLATE_VERSION ] : [];
      
      $key = 'global';
      if (isset($arrDefaultJS['default'][ $key ])) {
        $this->_defaultJS[ $key ] = $arrDefaultJS['default'][ $key ];
      }
      
      $key = 'compile';
      if (isset($arrDefaultJS['default'][ $key ])) {
        $this->_defaultJS[ $key ] = $arrDefaultJS['default'][ $key ];
      }
      
      return $this;
    }
    
    public function setExtendCSS()
    {
      $extendCSS = require BASE_PATH . '/static-manager/asset/' . MODULE . '/extendCSS.php';
      
      $arrExtendCSS = !empty($extendCSS[ TEMPLATE_VERSION ]) ? $extendCSS[ TEMPLATE_VERSION ] : [];
      
      $key                      = 'global';
      $this->_extendCSS[ $key ] = $arrExtendCSS['default'][ $key ];
      if (isset($arrExtendCSS['extend'][ $key ][ RESOURCE ])) {
        $this->_extendCSS[ $key ] = array_merge($this->_extendCSS[ $key ], $arrExtendCSS['extend'][ $key ][ RESOURCE ]);
      }
      
      $key                      = 'compile';
      $this->_extendCSS[ $key ] = $arrExtendCSS['default'][ $key ];
      if (isset($arrExtendCSS['extend'][ $key ][ RESOURCE ])) {
        $this->_extendCSS[ $key ] = array_merge($this->_extendCSS[ $key ], $arrExtendCSS['extend'][ $key ][ RESOURCE ]);
      }
      
      return $this;
    }
    
    public function setExtendJS()
    {
      $extendJS = require BASE_PATH . '/static-manager/asset/' . MODULE . '/extendJS.php';
      
      $arrExtendJS = !empty($extendJS[ TEMPLATE_VERSION ]) ? $extendJS[ TEMPLATE_VERSION ] : [];
      
      $key                     = 'global';
      $this->_extendJS[ $key ] = !empty($arrExtendJS['default']) ? $arrExtendJS['default'][ $key ] : [];
      if (isset($arrExtendJS['extend'][ $key ][ RESOURCE ])) {
        $this->_extendJS[ $key ] = array_merge($this->_extendJS[ $key ], $arrExtendJS['extend'][ $key ][ RESOURCE ]);
      }
      
      $key                     = 'compile';
      $this->_extendJS[ $key ] = !empty($arrExtendJS['default'][ $key ]) ? $arrExtendJS['default'][ $key ] : [];
      if (isset($arrExtendJS['extend'][ $key ][ RESOURCE ])) {
        $this->_extendJS[ $key ] = array_merge($this->_extendJS[ $key ], $arrExtendJS['extend'][ $key ][ RESOURCE ]);
      }
      
      return $this;
    }
    
    public function render($assets)
    {
      if (!$assets instanceof \Phalcon\Assets\Manager) {
        throw new \Exception('Trời đụ');
      }
      
      // #compile
      $resourceCompileURL = RESOURCE_URL . '/' . COMPILE_DIR;
      
      $arrCompileDefaultCSS = $this->_defaultCSS['compile'];
      $arrCompileDefaultJS  = $this->_defaultJS['compile'];
      $arrCompileExtendCSS  = $this->_extendCSS['compile'];
      $arrCompileExtendJS   = $this->_extendJS['compile'];
      
      if (!empty($arrCompileDefaultCSS)) {
        $assets->addCss($resourceCompileURL . '/css/??' . implode(',', $arrCompileDefaultCSS) . '?v=' . VERSION);
      }
      
      if (!empty($arrCompileDefaultJS)) {
        $assets->addJs($resourceCompileURL . '/js/??' . implode(',', $arrCompileDefaultJS) . '?v=' . VERSION, true, false);
      }
      
      foreach ($arrCompileExtendCSS as $css) {
        $assets->addCss($resourceCompileURL . '/css/' . $css . '?v=' . VERSION);
      }
      
      foreach ($arrCompileExtendJS as $js) {
        $assets->addJs($resourceCompileURL . '/js/' . $js . '?v=' . VERSION, true, false);
      }
      
      unset($arrCompileDefaultCSS);
      unset($arrCompileDefaultJS);
      unset($arrCompileExtendCSS);
      unset($arrCompileExtendJS);
      
      // #vendor
      $resourceVendorURL = RESOURCE_URL . '/' . 'vendor';
      
      $arrVendorCSS = $this->_vendorCSS['global'];
      $arrVendorJS  = $this->_vendorJS['global'];
      
      foreach ($arrVendorCSS as $file) {
        $filePath = $resourceVendorURL . '/' . $file['dir'] . '/css/' . $file['name'];
        $assets->addCss($filePath . '?v=' . VERSION);
      }
      
      foreach ($arrVendorJS as $file) {
        $strAttr = !empty($file['attr']) ? $file['attr'] : 'defer';
        
        $filePath = $resourceVendorURL . '/' . $file['dir'] . '/js/' . $file['name'];
        $assets->addJs($filePath . '?v=' . VERSION, true, false);
      }
      
      unset($arrVendorCSS);
      unset($arrVendorJS);
      
      // #global
      $resourceURL = RESOURCE_URL;
      
      $arrDefaultCSS = $this->_defaultCSS['global'];
      $arrDefaultJS  = $this->_defaultJS['global'];
      $arrExtendCSS  = $this->_extendCSS['global'];
      $arrExtendJS   = $this->_extendJS['global'];
      
      foreach ($arrDefaultCSS as $file) {
        $assets->addCss($resourceURL . '/' . $file['dir'] . '/css/' . $file['name'] . '?v=' . VERSION);
      }
      
      foreach ($arrDefaultJS as $file) {
        $assets->addJs($resourceURL . '/' . $file['dir'] . '/js/' . $file['name'] . '?v=' . VERSION, true, false);
      }
      
      foreach ($arrExtendCSS as $file) {
        $assets->addCss($resourceURL . '/' . $file['dir'] . '/css/' . $file['name'] . '?v=' . VERSION);
      }
      
      foreach ($arrExtendJS as $file) {
        $assets->addJs($resourceURL . '/' . $file['dir'] . '/js/' . $file['name'] . '?v=' . VERSION, true, false);
      }
      
      unset($arrDefaultCSS);
      unset($arrDefaultJS);
      unset($arrExtendCSS);
      unset($arrExtendJS);
    }
  }
