<?php
  
  use Phalcon\Mvc\Router\Group as RouterGroup;
  
  $router = $di->getRouter();
  
  $frontend = require_once MODULE_PATH . '/frontend/config/router.php';
  $backend  = require_once MODULE_PATH . '/backend/config/router.php';
  
  $router->mount($frontend);
  $router->mount($backend);
