<?php
  
  /**
   * Register Autoloader
   */
  
  $loader = new Phalcon\Loader();
  
  $loader->registerFiles([
    DIR_PATH . '/vendor/autoload.php',
  ]);
  
  $loader->registerNamespaces([
    'Cache'                 => BASE_PATH . '/cache',
    'Base\Enum'             => BASE_PATH . '/enum',
    'Base\Controller'       => BASE_PATH . '/controller',
    'Base\Permission'       => BASE_PATH . '/permission',
    'Base\StaticManager'    => BASE_PATH . '/static-manager',
    'Base\Social'           => BASE_PATH . '/social',
    'Base\Schema'           => BASE_PATH . '/schema',
    'Model'                 => COMMON_PATH . '/model',
    'Storage'               => COMMON_PATH . '/storage',
    'Util'                  => COMMON_PATH . '/util',
    'Schema'                => COMMON_PATH . '/schema',
    'Validation'            => COMMON_PATH . '/util/validation',
    'Module\Api\Controller' => MODULE_PATH . '/api/controller',
  ]);
  
  $loader->register();

