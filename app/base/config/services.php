<?php
  
  /**
   * Services are globally registered in this file
   */
  
  use
    Phalcon\DI\FactoryDefault,
    Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter,
    Phalcon\Http\Response\Cookies,
    Phalcon\Mvc\Router,
    Phalcon\Url as UrlResolver,
    Phalcon\Session\Manager,
    Phalcon\Session\Adapter\Stream,
    Phalcon\Security,
    PhpAmqpLib\Connection\AMQPStreamConnection,
    Phalcon\Translate\InterpolatorFactory,
    Phalcon\Translate\TranslateFactory;
  
  /**
   * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
   */
  $di = new FactoryDefault();
  
  
  /**
   * Registering a router
   */
  $di['router'] = function () {
    $router = new Router();
    $router->setDefaultModule("backend");
    $router->setDefaultController("index");
    $router->setDefaultAction("index");
    $router->notFound(["controller" => "index", "action" => "index"]);
    
    $router->handle($_SERVER["REQUEST_URI"]);
    $router->removeExtraSlashes(true);
    
    return $router;
  };
  
  /**
   * Shared configuration service
   */
  $di->setShared('config', function () {
    return include BASE_PATH . "/config/config.php";
  });
  
  /**
   * The URL component is used to generate all kind of urls in the application
   */
  $di['url'] = function () {
    $url = new UrlResolver();
    $url->setBaseUri('/');
    
    return $url;
  };
  
  /**
   * Start the session the first time some component request the session service
   */
  $di['session'] = function () {
    $session = new Manager();
    $files   = new Stream(
      [
        'savePath' => '/data/www/thuenha/session',
      ]
    );
    $session->setAdapter($files);

//    $session->start();
//    $session = new SessionAdapter([
//      "uniqueId" => SECRET_KEY,
//    ]);
    $session->start();
    
    return $session;
  };
  
  $di['cookies'] = function () {
    $cookies = new Cookies();
    $cookies->useEncryption(false);
    
    return $cookies;
  };
  
  $di['template'] = function () {
    $mobileDetect = new \Util\MobileDetect();
    define('DEVICE', $mobileDetect->isMobile() ? 'mobile' : 'desktop');
    
    return 'desktop';
  };
  
  $di->setShared('config', function () {
    return require_once BASE_PATH . "/config/config.php";
  });
  
  $di->setShared('db', function () {
    $config     = $this->getConfig();
    $params     = [
      'host'     => $config->database->host,
      'username' => $config->database->username,
      'password' => $config->database->password,
      'dbname'   => $config->database->dbname,
      'charset'  => $config->database->charset,
    ];
    $connection = new DbAdapter($params);
    
    return $connection;
  });
  
  $di->setShared('translate', function () use ($di) {
    $translate = require_once BASE_PATH . "/config/translate.php";
  
    $interpolator = new InterpolatorFactory();
    $factory      = new TranslateFactory($interpolator);
  
    $translator = $factory->newInstance('array', [
      "content" => $translate,
    ]);
    
    return $translator;
  });
  
  $di->setShared('filterUtil', function () use ($di) {
    return new \Util\FilterUtil();
  });
  
  $di->setShared('translateUtil', function () use ($di) {
    return new \Util\TranslateUtil();
  });
  
  $di->set(
    'security',
    function () {
      $security = new Security();
      
      
      return $security;
    },
    true
  );
  
  $di['seo'] = function () {
    return new \Util\SEO();
  };
  
  
  $di['cache'] = function () {
    $instanceCache = new \Memcached();
    $instanceCache->addServer('127.0.0.1', 11211);
    $instanceCache->setOption(\Memcached::OPT_HASH, \Memcached::HASH_MD5);
    $instanceCache->setOption(\Memcached::OPT_PREFIX_KEY, PREFIX);
    
    return $instanceCache;
  };
