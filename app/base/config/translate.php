<?php
  
  return [
    'SIDE_NAV_INDEX_TITLE' => 'Danh sách',
    
    'SIDE_NAV_USER_INDEX_TITLE' => 'Người dùng',
    'SIDE_NAV_USER_ADD_TITLE'   => 'Tạo Người dùng',
    'SIDE_NAV_USER_EDIT_TITLE'  => 'Cập nhật người dùng',
    
    'SIDE_NAV_ROLE_INDEX_TITLE' => 'Nhóm',
    'SIDE_NAV_ROLE_ADD_TITLE'   => 'Tạo nhóm',
    'SIDE_NAV_ROLE_EDIT_TITLE'  => 'Cập nhật nhóm',
    
    'SIDE_NAV_ITEM_INDEX_TITLE' => 'Sản phẩm',
    'SIDE_NAV_ITEM_ADD_TITLE'   => 'Đăng sản phẩm',
    'SIDE_NAV_ITEM_EDIT_TITLE'  => 'Cập nhật sản phẩm',
    
    'METHOD_NOT_SUPPORT' => 'Method is not supported !',
    'PARAM_EMPTY'        => 'Quý khách vui lòng nhập đầy đủ thông tin !',
    'SYSTEM_ERROR'       => 'Hệ thống đang bảo trì, quý khách vui lòng thử lại sau !',
    
    'USER'             => 'Người dùng',
    'FULLNAME'         => 'Họ tên',
    'PHONE'            => 'SĐT',
    'EMAIL'            => 'Email',
    'PASSWORD'         => 'Mật khẩu',
    'REPASSWORD'       => 'Mật khẩu lặp lại',
    'ITEM_NAME'        => 'Tiêu đề tin',
    'ITEM_DESCRIPTION' => 'Mô tả',
    'CITY'             => 'Tỉnh/thành phố',
    'DISTRICT'         => 'Quận/huyện phố',
    'FLOOR_AREA'       => 'Diện tích',
    'ADDRESS'          => 'Địa chỉ',
    'PRICE'            => 'Giá',
    'HOST_NAME'        => 'Tên người liên hệ',
    'HOST_PHONE'       => 'SĐT liên hệ',
    'HOST_ADDRESS'     => 'Địa chỉ',
    
    'PROCESS_DATA'      => 'Xử lý dữ liệu',
    'EMAIL_OR_PASSWORD' => 'Email hoặc mật khẩu',
    
    'EMPTY'     => 'không được bỏ trống !',
    'NOT_VALID' => 'không đúng định dạng !',
    'NOT_MATCH' => 'không trùng khớp',
    'NOT_FOUND' => 'không tìm thấy !',
    'NOT_EXIST' => 'không tồn tại !',
    'NOT_EXACT' => 'không chính xác !',
    'SUCCESS'   => 'thành công !',
  ];
