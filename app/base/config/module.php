<?php
  
  /**
   * Register application modules
   */

  $application->registerModules([
    'frontend' => [
      'className' => 'Module\Frontend\Module',
      'path'      => MODULE_PATH . '/frontend/Module.php',
    ],
    'backend'  => [
      'className' => 'Module\Backend\Module',
      'path'      => MODULE_PATH . '/backend/Module.php',
    ],
    'apis'     => [
      'className' => 'Module\Apis\Module',
      'path'      => MODULE_PATH . '/apis/Module.php',
    ]
  ]);
