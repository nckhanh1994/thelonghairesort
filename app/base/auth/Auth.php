<?php
  
  namespace Library\Auth;
  
  class Auth
  {
    
    private $module;
    
    public function __construct($strModuleName)
    {
      $this->module = $strModuleName;
    }
    
    public function saveLoginSession(array $arrParams = [])
    {
      if (empty($arrParams) || !is_array($arrParams)) {
        return false;
      }
    }
    
    public function setIdentity(array $arrParams = [])
    {
    
    }
    
    /**
     * Returns the current identity.
     *
     * @return array
     */
    public function getIdentity()
    {
      return $this->session->get('auth-identity');
    }
    
  }
