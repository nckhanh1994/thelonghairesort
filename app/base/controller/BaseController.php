<?php
  
  namespace Base\Controller;
  
  use Phalcon\Http\Response;
  use Phalcon\Mvc\Controller;
  
  use
    Base\Enum\ResponseEnum;
  
  
  class BaseController extends Controller
  {
    private $_response = null;
    
    protected $module;
    protected $controller;
    protected $action;
    
    public function onConstruct()
    {
      
      $this->_response = new Response();
      
      $router           = $this->router;
      $this->module     = $router->getModuleName();
      $this->controller = $router->getControllerName();
      $this->action     = $router->getActionName();
      
//      header('Access-Control-Allow-Origin: ' . BASE_URL);
//      header('Access-Control-Allow-Credentials: true');
      
      $cacheTime = 24 * 60 * 60;
      $this->_response->setHeader('Cache-Control', 'max-age=' . $cacheTime);
      $this->_response->setContentType('application/json');
    }
    
    /**
     * @param type array $message
     * return @response
     */
    public function createResponse($params = [])
    {
      $this->_response->setJsonContent($params);
      $this->_response->send();
    }
    
    /**
     * @param type string $message
     * @param type array $payload
     * return @response
     */
    public function createResponseErrorParam($message = '', $payload = [])
    {
      $this->_response->setJsonContent(['success' => ResponseEnum::INT_CODE_ERROR, 'code' => ResponseEnum::INT_CODE_ERROR_REQUEST_PARAM, 'message' => (!empty($message) ? $message : self::getMessageFromCode(ResponseEnum::INT_CODE_ERROR_REQUEST_PARAM)), 'payload' => (!empty($payload) ? $payload : [])]);
      $this->_response->send();
    }
    
    /**
     * @param type string $message
     * @param type array $payload
     * return @response
     */
    public function createResponseErrorAPI($message = '', $payload = [])
    {
      $this->_response->setJsonContent(['success' => ResponseEnum::INT_CODE_ERROR, 'code' => ResponseEnum::INT_CODE_ERROR_API, 'message' => (!empty($message) ? $message : self::getMessageFromCode(ResponseEnum::INT_CODE_ERROR_API)), 'payload' => (!empty($payload) ? $payload : [])]);
      $this->_response->send();
    }
    
    /**
     * @param type string $message
     * @param type array $payload
     * return @response
     */
    public function createResponseSuccess($message = '', $payload = [])
    {
      $this->_response->setJsonContent(['success' => ResponseEnum::INT_CODE_SUCCESS, 'code' => ResponseEnum::INT_CODE_SUCCESS_API, 'message' => (!empty($message) ? $message : self::getMessageFromCode(ResponseEnum::INT_CODE_SUCCESS_API)), 'payload' => (!empty($payload) ? $payload : [])]);
      $this->_response->send();
    }
    
    /**
     * @param type string $viewPath
     * @param type array $arrParam
     * return @this
     */
    public function renderView($viewPath = '', $arrParam = [])
    {
      $simpleView = new Simple();
      
      return $simpleView->render($viewPath, $arrParam);
    }
    
    /**
     * @param type string $url
     * return @bool
     */
    public function redirect($url = ''): bool
    {
      if (empty($url)) {
        return false;
      }
      
      $this->_response->redirect($url);
      $this->_response->send();
      
      return false;
    }
    
    /**
     * @param type integer $code
     * return @bool
     */
    public static function isValidCode($code): bool
    {
      if (empty($code) && !is_int($code)) {
        return false;
      }
      
      return array_key_exists($code, ResponseEnum::ARR_MESSAGE_FROM_CODE);
    }
    
    /**
     * @param integer $code
     * return @string
     */
    public static function getMessageFromCode($code): string
    {
      if (!self::isValidCode($code)) {
        return false;
      }
      
      return ResponseEnum::ARR_MESSAGE_FROM_CODE[ $code ];
    }
  }
