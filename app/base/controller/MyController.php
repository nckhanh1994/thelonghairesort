<?php
  
  namespace Base\Controller;
  
  use Phalcon\Mvc\Controller;
  use Phalcon\Mvc\View;
  
  use Base\Permission\MyAcl;
  use Base\StaticManager\StaticManager;
  use Base\Enum\ResponseEnum;
  use Base\Schema\Schema;
  
  use
    Model\Role,
    Model\RolePermission,
    Model\City,
    Model\District,
    Model\Street;
  
  use
    Util\GeneralUtil,
    Util\SchemaUtil;
  
  class MyController extends Controller
  {
    protected $module;
    protected $controller;
    protected $action;
    protected $identityKey;
    protected $_rolePermissions;
    protected $arrCategoryTree;
    protected $CDATA;
    
    public function initialize()
    {
      $router            = $this->router;
      $this->module      = $router->getModuleName();
      $this->controller  = $router->getControllerName();
      $this->action      = $router->getActionName();
      $this->identityKey = "$this->module:auth";

//      var_dump($this->controller, $this->action);die;
      
      $resource = strtolower($this->module) . ':' . strtolower($this->controller) . ':' . strtolower($this->action);
      
      $templateVersion = $this->config->template->backend->version;
      $resourceURL     = STATIC_URL . '/' . $this->module . '/' . $this->config->template->backend->version . '/' . '_partial';
      if ($this->module == 'frontend') {
        $templateVersion = $this->config->template->frontend->version;
        $resourceURL     = STATIC_URL . '/' . $this->module . '/' . $this->config->template->frontend->version . '/' . '_partial';
      }
      
      // ## <editor-fold desc="Ds Tỉnh/thành + Quận/huyện + Phường/xã">
      $conditions = [
        'is_deleted' => 0,
      ];
      
      $instanceCity = new City();
      $arrCities    = $instanceCity->getListLimit($conditions, 1, 9999);
      
      $instanceDistrict = new District();
      $arrDistricts     = $instanceDistrict->getListLimit($conditions, 1, 9999);
      
      $conditions = [
        'city_id'    => 29,
        'is_deleted' => 0,
        'group_by'   => 'street_keyword'
      ];
      
      $instnaceStreet = new Street();
      $arrStreets     = $instnaceStreet->getListLimit($conditions, 1, 9999, null, ['street_id as id', 'street_keyword as name', 'street_name', 'district_keyword']);
      // ## </editor-fold>
      
      define('MODULE', $this->module);
      define('RESOURCE', $resource);
      define('RESOURCE_URL', $resourceURL);
      define('VERSION', APPLICATION_ENV == 'production' ? '1.0.21' : time());
      define('TEMPLATE_VERSION', $templateVersion);
      
      $instanceStaticManager = (new StaticManager())->render($this->assets);
      
      $this
        ->authenticate()
        ->setMeta();
      
      
      $this->CDATA = [
        'common'     => [
          'controller' => $this->controller,
          'action'     => $this->action,
          'categories' => GeneralUtil::$arrCategories,
          'cities'     => $arrCities,
          'districts'  => $arrDistricts,
          'streets'    => $arrStreets,
        ],
        'login'      => [
          'UID'       => defined('UID') ? UID : '',
          'ROLE_ID'   => defined('UID') ? ROLE_ID : 0,
          'fullName'  => defined('FULLNAME') ? FULLNAME : '',
          'MAX_PRICE' => defined('MAX_PRICE') ? MAX_PRICE : 0,
        ],
        'url'        => [
          'baseURL'     => BASE_URL,
          'apiURL'      => API_URL,
          'resourceURL' => RESOURCE_URL,
          'logoutURL'   => $this->url->get(['for' => 'backend:auth:logout']),
        ],
        'permission' => [
          'isACP'           => defined('IS_ACP') ? IS_ACP : 0,
          'groupResource'   => $this->module . ':' . $this->controller,
          'rolePermissions' => $this->_rolePermissions,
        ],
      ];
      
      $this->view->setVars([
        'CDATA'     => $this->CDATA,
        'PAGE_DATA' => [],
        'schema'    => (new Schema(SchemaUtil::ORGANIZATION))->build()
      ]);
    }
    
    private function setMeta()
    {
      $strTitle = $this->config->site->name;
      $strDesc  = $this->config->site->desc;
      $resource = $this->module . ':' . $this->controller . ':' . $this->action;
      
      switch ($resource) {
        case '':
          break;
        default:
          $this->seo->setTitle($strTitle);
          $this->seo->setByName("description", $strDesc);
          $this->seo->setByName("keywords", '');
          
          $this->seo->setByProperty("og:locate", 'en_US');
          $this->seo->setByProperty("og:type", 'website');
          $this->seo->setByProperty("og:image", STATIC_URL . '/images/logo.png');
          $this->seo->setByProperty("og:title", $strTitle);
          $this->seo->setByProperty("og:description", $strDesc);
          $this->seo->setByProperty("og:url", BASE_URL);
          $this->seo->setByProperty("og:site_name", $this->config->site->name);
          break;
      }
    }
    
    private function authenticate()
    {
      // Get info user login
      $arrLogin = $this->session->get($this->identityKey);
      switch ($this->module) {
        case 'backend':
          if (empty($arrLogin)) {
            if ($this->controller != 'auth') {
              $this->redirect($this->url->get(['for' => 'backend:auth:login']));
            }
            
            return $this;
          }
          define('UID', $this->filterUtil->_int($arrLogin['user_id']));
          define('FULLNAME', $this->filterUtil->_string($arrLogin['fullname']));
          define('EMAIL', $this->filterUtil->_string($arrLogin['email']));
          define('PHONE', $this->filterUtil->_string($arrLogin['phone']));
          define('ROLE_ID', $this->filterUtil->_int($arrLogin['role_id']));
          define('MAX_PRICE', $this->filterUtil->_string($arrLogin['max_price']));
          
          $arrLogin['module']     = $this->module;
          $arrLogin['controller'] = $this->controller;
          $arrLogin['action']     = $this->action;
          
          if (!$this->permission($arrLogin)) {
            $this->view->setLayout('/error/access-deny');
            $this->view->pick('error/access-deny');
            
            return $this;
          }
          break;
        case 'frontend':
          if (!empty($arrLogin)) {
            define('UID', $this->filterUtil->_int($arrLogin['user_id']));
            define('FULLNAME', $this->filterUtil->_string($arrLogin['fullname']));
            define('EMAIL', $this->filterUtil->_string($arrLogin['email']));
            define('PHONE', $this->filterUtil->_string($arrLogin['phone']));
            define('ROLE_ID', $this->filterUtil->_int($arrLogin['role_id']));
            define('MAX_PRICE', $this->filterUtil->_string($arrLogin['max_price']));
          }
          
          break;
        default:
          # code...
          break;
      }
      
      return $this;
    }
    
    public function permission($params)
    {
      $conditions = ['role_id' => ROLE_ID];
      
      $instanceGroup  = new Role();
      $arrGroupDetail = $instanceGroup->getDetail($conditions);
      // Check can access CPanel
      if (empty($arrGroupDetail)) {
        return false;
      }
      
      // Check use in fullaccess role
      $isACP = (int)$arrGroupDetail['is_fullaccess'];
      define('IS_ACP', $isACP);
      if ($isACP) {
        return true;
      }
      
      $instanceRolePermission = new RolePermission();
      $arrRolePermissionList  = $instanceRolePermission->getResourceList($conditions, 1, 9999);
      
      $strAction = strtolower(str_replace('-', '', $params['action']));
      
      $instanceMyAcl = new MyAcl();
      $isAccess      = $instanceMyAcl->checkPermission($arrRolePermissionList, UID, $params['module'], str_replace('-', '', $params['controller']), $strAction);
      
      $this->_rolePermissions = $arrRolePermissionList; // #set
      unset($arrRolePermissionList);
      
      return $isAccess;
    }
    
    public function renderEmailTemplate($template, $params)
    {
      $this->view->disableLevel(\Phalcon\Mvc\View::LEVEL_LAYOUT);
      if (!is_array($params)) {
        return false;
      }
      $view = $this->view;
      $view->setVars($params);
      $view->start();
      $view->setRenderLevel(View::LEVEL_ACTION_VIEW);
      $view->render(APP_PATH . 'static/emails', $template);
      $view->finish();
      
      return $view->getContent();
      
    }
    
    public function renderPrintTemplate($template, $params)
    {
      $this->view->disableLevel(\Phalcon\Mvc\View::LEVEL_LAYOUT);
      if (!is_array($params)) {
        return false;
      }
      $view = $this->view;
      $view->setVars($params);
      $view->start();
      $view->setRenderLevel(View::LEVEL_ACTION_VIEW);
      $view->render(APP_PATH . 'static/print', $template);
      $view->finish();
      
      return $view->getContent();
      
    }
    
    public function redirect($url = '', $code = 0)
    {
      if (empty($url)) {
        return false;
      }
      
      if (!empty($code)) {
        $this->response->setStatusCode($code);
      }
      $this->response->redirect($url);
      $this->response->send();
      
      return false;
    }
    
    public function responseJSON($arrParams)
    {
      return $this->response->setContent(json_encode($arrParams));
    }
    
    public function renderTemplate($path = '', $arrParam = [])
    {
      $view = new \Phalcon\Mvc\View\Simple();
      return $view->render(
        $path,
        $arrParam
      );
    }
    
    /**
     * @param type array $message
     * return @response
     */
    public function createResponse($params = [])
    {
      return $this->response->setJsonContent($params);
    }
    
    /**
     * @param type string $message
     * @param type array $payload
     * return @response
     */
    public function createResponseErrorParam($message = '', $payload = [])
    {
      return $this->response->setJsonContent(['success' => ResponseEnum::INT_CODE_ERROR, 'code' => ResponseEnum::INT_CODE_ERROR_REQUEST_PARAM, 'message' => (!empty($message) ? $message : self::getMessageFromCode(ResponseEnum::INT_CODE_ERROR_REQUEST_PARAM)), 'payload' => (!empty($payload) ? $payload : [])]);
    }
    
    /**
     * @param type string $message
     * @param type array $payload
     * return @response
     */
    public function createResponseErrorAPI($message = '', $payload = [])
    {
      return $this->response->setJsonContent(['success' => ResponseEnum::INT_CODE_ERROR, 'code' => ResponseEnum::INT_CODE_ERROR_API, 'message' => (!empty($message) ? $message : self::getMessageFromCode(ResponseEnum::INT_CODE_ERROR_API)), 'payload' => (!empty($payload) ? $payload : [])]);
    }
    
    /**
     * @param type string $message
     * @param type array $payload
     * return @response
     */
    public function createResponseSuccess($message = '', $payload = [])
    {
      return $this->response->setJsonContent(['success' => ResponseEnum::INT_CODE_SUCCESS, 'code' => ResponseEnum::INT_CODE_SUCCESS_API, 'message' => (!empty($message) ? $message : self::getMessageFromCode(ResponseEnum::INT_CODE_SUCCESS_API)), 'payload' => (!empty($payload) ? $payload : [])]);
    }
    
    /**
     * @param type integer $code
     * return @bool
     */
    public static function isValidCode($code): bool
    {
      if (empty($code) && !is_int($code)) {
        return false;
      }
      
      return array_key_exists($code, ResponseEnum::ARR_MESSAGE_FROM_CODE);
    }
    
    /**
     * @param integer $code
     * return @string
     */
    public static function getMessageFromCode($code): string
    {
      if (!self::isValidCode($code)) {
        return false;
      }
      
      return ResponseEnum::ARR_MESSAGE_FROM_CODE[ $code ];
    }
  }
