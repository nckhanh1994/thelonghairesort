<?php
  
  namespace Base\Enum;
  
  class ResponseEnum
  {
    const INT_CODE_SUCCESS = 1; // Mã thành công chung
    const INT_CODE_SUCCESS_API = 200; // Mã thành công chung của API
    const INT_CODE_ERROR = -1; // Mã lỗi chung
    // Đối với các mã -99 <= code <= -2 đều là lỗi từ phía sử dụng API
    const INT_CODE_ERROR_REQUEST_PARAM = -10; // Thông báo param truyền từ phía request API có vấn đề. VD: thiếu tham số, tham số sai,....
    // Đối với mã -199 <= code <= -100 đều là lỗi từ phía cung cấp API.
    const INT_CODE_ERROR_API = -100; // Mã lỗi chung phía cung cấp API. VD: curl lỗi, requet API từ nơi khác lỗi, data bot trả ra lỗi, database lỗi, server sập...
    // Đối với mã -299 <= code <= -200 đều là lỗi liên quan vấn đề xác thực thông tin
    const INT_CODE_ERROR_AUTH = -200; // lỗi chung liên quan vấn đề xác thực thông tin. VD: chưa đăng nhập, bla bla,...
    // Các message mặc định tương ứng từng mã lỗi
    const ARR_MESSAGE_FROM_CODE = [
      self::INT_CODE_SUCCESS             => 'Dữ liệu đã được xử lý thành công',
      self::INT_CODE_SUCCESS_API         => 'Dữ liệu đã được xử lý thành công',
      self::INT_CODE_ERROR               => 'Có lỗi xảy ra',
      self::INT_CODE_ERROR_REQUEST_PARAM => 'Tham số request không hợp lệ',
      self::INT_CODE_ERROR_API           => 'Lỗi api',
      self::INT_CODE_ERROR_AUTH          => 'Lỗi xác thực thông tin',
    ];
    
    const MSG_AUTH_LOGIN_SUCCESS = 'Chúc mừng quý khách đã đăng nhập thành công !';
  }
