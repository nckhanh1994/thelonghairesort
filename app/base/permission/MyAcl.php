<?php
  
  namespace Base\Permission;
  
  use Phalcon\Acl as Acl;
  use Phalcon\Acl\Adapter\Memory as AclMemory;
  use Phalcon\Acl\Resource as AclResource;
  use Phalcon\Acl\Role as AclRole;
  
  class MyAcl
  {
    public static $_acl = null;
    
    public function __construct()
    {
      self::getInstance();
    }
    
    public static function getInstance()
    {
      if (self::$_acl == null) {
        self::$_acl = new AclMemory();
      }
      
      return self::$_acl;
    }
    
    public function checkPermission($role_permissions, $role_id, $module, $controller, $action = '')
    {
      if (
        is_array($role_permissions)
        && !empty($role_permissions)
      ) {
        $roleID           = self::formatRole(UID);
        $strResourceTrack = trim(strtolower($module . ':' . $controller));
        
        self::$_acl->setDefaultAction(Acl::DENY);
        foreach ($role_permissions as $permission) {
          list($module, $controller, $action) = explode(':', $permission);
          $strResource = $module . ':' . $controller;
          
          self::$_acl->addResource(new AclResource($strResource), $action);
          self::$_acl->addRole(new AclRole($roleID));
          self::$_acl->allow($roleID, $strResource, $action);
        }
        
        if (self::$_acl->isAllowed($roleID, $strResourceTrack, $action))
          return true;
        
        return false;
      }
      
      return false;
    }
    
    public function isAllowed($resource, $action)
    {
      if (IS_ACP)
        return true;
      
      $roleID = $this->formatRole(UID);
      
      if (self::$_acl->isAllowed($roleID, $resource, $action))
        return true;
      
      return false;
    }
    
    private function formatRole($role_id)
    {
      return 'role_' . $role_id;
    }
    
  }
