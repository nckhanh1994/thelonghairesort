<?php
  
  namespace Util;
  
  /**
   * Register Service
   * $di->set('metatag', function() {
   * return new MetaTags();
   * });
   *
   * In Controller
   * $this->metatag->setTitle("Phalcon MetaTags Service");
   * $this->metatag->setCustom("charset", ['charset' => 'UTF-8']);
   * $this->metatag->setCustom("http", ['http-equiv' => 'content-type', 'content' => 'text/html; charset=UTF-8']);
   * $this->metatag->setByName("description", "phalcon php metatags");
   * $this->metatag->setByProperty("og:description", "When Great Minds Don’t Think Alike");
   *
   * In View
   * <?php echo $this->metatag->getTitle();?>
   * <?php echo $this->metatag->getMeta();?>
   *
   */
  class SEO
  {
    
    public $title = '';
    public $meta_custom = [];
    public $meta_name = [];
    public $meta_property = [];
    public $canonical = '';
    
    public function setTitle($title)
    {
      $this->title = $title;
    }
    
    public function getTitle()
    {
      return $this->title ? '<title>' . $this->title . '</title>' : '';
    }
    
    public function setCanonical($url)
    {
      $this->canonical = '<link rel="canonical" href="' . $url . '" />' . "\n";
    }
    
    public function getCanonical()
    {
      return $this->canonical;
    }
    
    public function setByProperty($property, $content)
    {
      $this->meta_property[ $property ] = $content;
    }
    
    public function unsetByProperty($properties)
    {
      if (is_array($properties)) {
        foreach ($properties as $property) {
          if (isset($this->meta_property[ $property ])) {
            unset($this->meta_property[ $property ]);
          }
        }
      } else {
        if (isset($this->meta_property[ $properties ])) {
          unset($this->meta_property[ $properties ]);
        }
      }
    }
    
    public function setByName($name, $content)
    {
      $this->meta_name[ $name ] = $content;
    }
    
    public function unsetByName($names)
    {
      if (is_array($names)) {
        foreach ($names as $name) {
          if (isset($this->meta_name[ $name ])) {
            unset($this->meta_name[ $name ]);
          }
        }
      } else {
        if (isset($this->meta_name[ $names ])) {
          unset($this->meta_name[ $names ]);
        }
      }
    }
    
    public function setCustom($key, $attributes)
    {
      foreach ($attributes as &$value) {
        $value = $value;
      }
      $this->meta_custom[ $key ] = $attributes;
    }
    
    public function unsetCustom($keys)
    {
      if (is_array($keys)) {
        foreach ($keys as $key) {
          if (isset($this->meta_custom[ $key ])) {
            unset($this->meta_custom[ $key ]);
          }
        }
      } else {
        if (isset($this->meta_custom[ $keys ])) {
          unset($this->meta_custom[ $keys ]);
        }
      }
    }
    
    public function getMeta()
    {
      $result = "";
      foreach ($this->meta_name as $name => $content) {
        $result .= '<meta name="' . $name . '" content="' . $content . '"/>' . "\n";
      }
      foreach ($this->meta_property as $property => $content) {
        $result .= '<meta property="' . $property . '" content="' . $content . '"/>' . "\n";
      }
      foreach ($this->meta_custom as $meta) {
        $tag = "<meta";
        foreach ($meta as $attr => $value) {
          $tag .= ' ' . $attr . '="' . $value . '"';
        }
        $tag    .= "/>";
        $result .= $tag;
      }
      
      return $result;
    }
    
  }
