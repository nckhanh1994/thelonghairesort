<?php
  
  namespace Util;
  
  use \Library\General;
  
  class Paypal
  {
    private $_url = '';
    private $_env = 'sandbox';
    
    private $_clientId = '';
    private $_clientSecret = '';
    
    private $_accessToken = '';
    
    private $_paymentId = '';
    private $_paymentToken = '';
    private $_payerId = '';
    
    /**
     * All default curl options are stored in the array inside the PayPalHttpConfig class. To make changes to those settings
     * for your specific environments, feel free to add them using the code shown below
     * Uncomment below line to override any default curl options.
     */
    public function __construct()
    {
      switch ($this->_env) {
        case 'sandbox':
          $this->_url          = 'https://api.sandbox.paypal.com/v1';
          $this->_clientId     = 'ARjvs0O18FFxm31_SE4z8k3HY7qYOnerltURRv33o8z6K84IzCFdpHMk_xkwgXBLNn726Y9wmd7wy2eY';
          $this->_clientSecret = 'EHEIu7Hcapks1jlgLgS3AG9De_jKrzrNttL6sSgjZpEZsSD3LQyYBRkfmHMJETHWlJeKtynsgZGRwLL1';
          break;
        default:
          $this->_clientId     = 'AaoinWHe04ZiRTCr3Q2VJ4KOyitFSngBM1TgbWjSDaC4xoy1AX7_8FxqWGJ_y8CFWsIbR8PG19i1KzEQ';
          $this->_clientSecret = 'EC_GtVVfPUbptGL_fIB06cBPE9KWMgqA95y_c545Oo9zpJwZK7JbjEGe1Soqt-XhZAJ6fj_cNS-maU0h';
          $this->_url          = 'https://api.paypal.com/v1';
          break;
      }
    }
    
    /**
     * @param string $_paymentId
     * @return $this;
     */
    public function setPaymentId($_paymentId = '')
    {
      $this->_paymentId = $_paymentId;
      
      return $this;
    }
    
    /**
     * @param string $_paymentToken
     * @return $this;
     */
    public function setPaymentToken($_paymentToken = '')
    {
      $this->_paymentToken = $_paymentToken;
      
      return $this;
    }
    
    /**
     * @param string $_payerId
     * @return $this;
     */
    public function setPayerId($_payerId = '')
    {
      $this->_payerId = $_payerId;
      
      return $this;
    }
    
    /**
     * @return this;
     */
    public function getAccessToken()
    {
      $url = $this->_url . '/oauth2/token';
      
      $response = General::crawler($url, 'grant_type=client_credentials', [], false, $this->_clientId . ':' . $this->_clientSecret);
      
      $this->_accessToken = json_decode($response, true)['access_token'];
      
      return $this;
    }
    
    /**
     * @return boolean;
     */
    public function executePayment()
    {
      if (!$this->validatePayment())
        return false;
      
      $url       = $this->_url . '/payments/payment/' . $this->_paymentId . '/execute';
      $arrData   = [
        'payer_id' => $this->_payerId
      ];
      $arrHeader = [
        'Content-Type: application/json',
        'Authorization: Bearer ' . $this->_accessToken,
      ];
      
      $response = General::crawler($url, json_encode($arrData), $arrHeader);
      
      return json_decode($response, true);
    }
    
    /**
     * @return boolean
     */
    public function validatePayment()
    {
      return (empty($this->_accessToken) || empty($this->_paymentId) || empty($this->_paymentToken) || empty($this->_payerId)) ? false : true;
    }
    
    /**
     * Helper method for getting an APIContext for all calls
     * @return PayPal\Rest\ApiContext
     */
    function getApiContext()
    {
      // #### SDK configuration
      // Register the sdk_config.ini file in current directory
      // as the configuration source.
      /*
      if(!defined("PP_CONFIG_PATH")) {
          define("PP_CONFIG_PATH", __DIR__);
      }
      */
      // ### Api context
      // Use an ApiContext object to authenticate
      // API calls. The clientId and clientSecret for the
      // OAuthTokenCredential class can be retrieved from
      // developer.paypal.com
      $apiContext = new ApiContext(
        new OAuthTokenCredential(
          $this->_clientId,
          $this->_clientSecret
        )
      );
      // Comment this line out and uncomment the PP_CONFIG_PATH
      // 'define' block if you want to use static file
      // based configuration
      $apiContext->setConfig(
        [
          'mode'           => 'sandbox',
          'log.LogEnabled' => true,
          'log.FileName'   => '../PayPal.log',
          'log.LogLevel'   => 'DEBUG', // PLEASE USE `INFO` LEVEL FOR LOGGING IN LIVE ENVIRONMENTS
          'cache.enabled'  => true,
          //'cache.FileName' => '/PaypalCache' // for determining paypal cache directory
          // 'http.CURLOPT_CONNECTTIMEOUT' => 30
          // 'http.headers.PayPal-Partner-Attribution-Id' => '123123123'
          //'log.AdapterFactory' => '\PayPal\Log\DefaultLogFactory' // Factory class implementing \PayPal\Log\PayPalLogFactory
        ]
      );
      // Partner Attribution Id
      // Use this header if you are a PayPal partner. Specify a unique BN Code to receive revenue attribution.
      // To learn more or to request a BN Code, contact your Partner Manager or visit the PayPal Partner Portal
      // $apiContext->addRequestHeader('PayPal-Partner-Attribution-Id', '123123123');
      return $apiContext;
    }
  }
