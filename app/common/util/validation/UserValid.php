<?php
  
  namespace Validation;
  
  use
    Util\FilterUtil,
    Util\TranslateUtil;
  
  class UserValid
  {
    static $_instanceTranslate = null;
    
    public function __construct()
    {
      self::getInstance();
    }
    
    public static function getInstance()
    {
      if (is_null(self::$_instanceTranslate)) {
        self::$_instanceTranslate = new TranslateUtil();
      }
      
      return self::$_instanceTranslate;
    }
    
    // #reg
    public static function jsValidationRulesReg()
    {
      self::getInstance();
      
      return [
        'rules'    => [
          'name'   => [
            'required' => true
          ],
          'phone'  => [
            'required' => true
          ],
          'email'  => [
            'required' => true,
            'email'    => true,
          ],
          'message'   => [
            'required' => true
          ],
        ],
        'messages' => [
          'name'   => [
            'required' => '(*)'
          ],
          'phone'  => [
            'required' => '(*)'
          ],
          'email'  => [
            'required' => '(*)',
            'email'    => '(*)'
          ],
          'message'   => [
            'required' => '(*)'
          ],
        ]
      ];;
    }
    
    // #login
    public function jsValidationRulesLogin()
    {
      self::getInstance();
      
      return [
        'rules'    => [
          'user_email' => [
            'required' => true,
            'email'    => true,
          ],
          'user_pass'  => [
            'required' => true
          ],
        ],
        'messages' => [
          'user_email' => [
            'required' => self::$_instanceTranslate->_('EMAIL', 'EMPTY'),
            'email'    => self::$_instanceTranslate->_('EMAIL', 'NOT_VALID')
          ],
          'user_pass'  => [
            'required' => self::$_instanceTranslate->_('PASSWORD', 'EMPTY')
          ],
        ]
      ];
    }
    
    // #login
    public static function jsValidationRulesRent()
    {
      self::getInstance();
      
      return [
        'rules'    => [
          'item_name'        => [
            'required' => true,
          ],
          'item_description' => [
            'required' => true
          ],
          'city_id'          => [
            'required' => true
          ],
          'district_id'      => [
            'required' => true
          ],
          'floor_area_from'  => [
            'required' => true
          ],
          'floor_area_to'    => [
            'required' => true
          ],
          'address'          => [
            'required' => true
          ],
          'price_from'       => [
            'required' => true
          ],
          'price_to'         => [
            'required' => true
          ],
          'host_name'        => [
            'required' => true
          ],
          'host_phone'       => [
            'required' => true
          ],
          'host_address'     => [
            'required' => true
          ],
        ],
        'messages' => [
          'item_name'        => [
            'required' => self::$_instanceTranslate->_('ITEM_NAME', 'EMPTY'),
          ],
          'item_description' => [
            'required' => self::$_instanceTranslate->_('ITEM_DESCRIPTION', 'EMPTY'),
          ],
          'city_id'          => [
            'required' => self::$_instanceTranslate->_('CITY', 'EMPTY')
          ],
          'district_id'      => [
            'required' => self::$_instanceTranslate->_('DISTRICT', 'EMPTY')
          ],
          'floor_area_from'  => [
            'required' => self::$_instanceTranslate->_('FLOOR_AREA', 'EMPTY')
          ],
          'floor_area_to'    => [
            'required' => self::$_instanceTranslate->_('FLOOR_AREA', 'EMPTY')
          ],
          'address'          => [
            'required' => self::$_instanceTranslate->_('ADDRESS', 'EMPTY')
          ],
          'price_from'       => [
            'required' => self::$_instanceTranslate->_('PRICE', 'EMPTY')
          ],
          'price_to'         => [
            'required' => self::$_instanceTranslate->_('PRICE', 'EMPTY')
          ],
          'host_name'        => [
            'required' => self::$_instanceTranslate->_('HOST_NAME', 'EMPTY')
          ],
          'host_phone'       => [
            'required' => self::$_instanceTranslate->_('HOST_PHONE', 'EMPTY')
          ],
          'host_address'     => [
            'required' => self::$_instanceTranslate->_('HOST_ADDRESS', 'EMPTY')
          ],
        ]
      ];
    }
    
    public static function validate($params)
    {
      $fullName = FilterUtil::string($params['fullname']);
      if (empty($fullName))
        return false;
      
      $email = FilterUtil::string($params['email']);
      if (empty($email))
        return false;
      
      $phone = FilterUtil::string($params['phone']);
      if (empty($phone))
        return false;
      
      return array_merge($params, [
        'fullname' => $fullName,
        'email'    => $email,
        'phone'    => $phone,
        'address'  => $address,
      ]);
    }
  }
