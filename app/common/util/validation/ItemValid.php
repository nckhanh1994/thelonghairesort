<?php
  
  namespace Validation;
  
  use
    Util\FilterUtil,
    Util\TranslateUtil;
  
  class ItemValid
  {
    static $_instanceTranslate = null;
    
    public function __construct()
    {
      self::getInstance();
    }
    
    public static function getInstance()
    {
      if (is_null(self::$_instanceTranslate)) {
        self::$_instanceTranslate = new TranslateUtil();
      }
      
      return self::$_instanceTranslate;
    }
    
    /**
     * #rent
     */
    public static function validateRent(&$params)
    {
      $categoryID = FilterUtil::int($params['category_id']);
      if (empty($categoryID))
        return false;
      
      $itemName = FilterUtil::string($params['item_name']);
      if (empty($itemName))
        return false;
      
      $itemDescsription = FilterUtil::string($params['item_description']);
      if (empty($itemDescsription))
        return false;
      
      $cityID = FilterUtil::int($params['city_id']);
      if (empty($cityID))
        return false;
      
      $districtID = FilterUtil::int($params['district_id']);
      if (empty($districtID))
        return false;
      
      $strAddress = FilterUtil::string($params['address']);
      if (empty($strAddress))
        return false;
      
      $hostName = FilterUtil::string($params['host_name']);
      if (empty($hostName))
        return false;
      
      $hostPhone = FilterUtil::string($params['host_phone']);
      if (empty($hostPhone))
        return false;
      
      $hostAddress = FilterUtil::string($params['host_address']);
      if (empty($hostAddress))
        return false;
      
      $floorAreaFrom = FilterUtil::float($params['floor_area_from']);
      $floorAreaTo   = FilterUtil::float($params['floor_area_to']);
      
      $priceFrom = FilterUtil::float($params['price_from']);
      $priceTo   = FilterUtil::float($params['price_to']);
      
      $params['category_id']      = $categoryID;
      $params['item_name']        = $itemName;
      $params['item_description'] = $itemDescsription;
      $params['city_id']          = $cityID;
      $params['district_id']      = $districtID;
      $params['floor_area_from']  = $floorAreaFrom;
      $params['floor_area_to']    = $floorAreaTo;
      $params['address']          = $strAddress;
      $params['price_from']       = $priceFrom;
      $params['price_to']         = $priceTo;
      $params['host_name']        = $hostName;
      $params['host_phone']       = $hostPhone;
      $params['host_address']     = $hostAddress;
      
      return true;
    }
  }
