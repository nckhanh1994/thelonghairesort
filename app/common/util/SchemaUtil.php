<?php
  
  namespace Util;
  
  class SchemaUtil
  {
    const ORGANIZATION = 'Organization';
    const IMAGE = 'ImageObject';
    const WEBSITE = 'WebSite';
    const WEBPAGE = 'WebPage';
    const BREADCRUMB = 'BreadcrumbList';
    const ARTICLE = 'NewsArticle';
  }
