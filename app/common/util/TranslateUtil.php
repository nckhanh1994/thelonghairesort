<?php
  
  namespace Util;
  
  class TranslateUtil
  {
    private $_translate = null;
    
    public function __construct()
    {
      $di = \Phalcon\DI::getDefault();
      
      $this->_translate = $di->get('translate');
    }
    
    public function _($fk, $lk = '')
    {
      return $this->_translate->_($fk) . ' ' . $this->_translate->_($lk);
    }
  }
