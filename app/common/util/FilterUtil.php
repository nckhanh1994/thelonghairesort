<?php
  
  namespace Util;
  
  class FilterUtil
  {
    public static function int($value = null, $def = 0)
    {
      $value = (int)$value;
      
      return $value !== 0 ? $value : $def;
    }
    
    public static function float($value = null, $def = 0)
    {
      $value = (float)$value;
      
      return $value !== 0 ? $value : $def;
    }
    
    public static function string($value = null, $def = '')
    {
      $value = (string)trim($value);
      
      return !empty($value) ? $value : $def;
    }
    
    public static function array($value = null, $def = [])
    {
      $value = (array)$value;
      
      return !empty($value) ? $value : $def;
    }
  
    public function _int($value = null, $def = 0)
    {
      $value = (int)$value;
    
      return $value !== 0 ? $value : $def;
    }
  
    public function _string($value = null, $def = '')
    {
      $value = (string)trim($value);
    
      return !empty($value) ? $value : $def;
    }
  }
