<?php
  
  namespace Util;
  
  use Util\FilterUtil;
  
  class HttpUtil
  {
    public static function getUserIP()
    {
      // Get real visitor IP behind CloudFlare network
      if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
        $_SERVER['REMOTE_ADDR']    = $_SERVER["HTTP_CF_CONNECTING_IP"];
        $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
      }
      
      $client  = @$_SERVER['HTTP_CLIENT_IP'];
      $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
      $remote  = $_SERVER['REMOTE_ADDR'];
      
      if (filter_var($client, FILTER_VALIDATE_IP)) {
        $ip = $client;
      } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
        $ip = $forward;
      } else {
        $ip = $remote;
      }
      
      return $ip;
    }
    
    /**
     * $paramData vừa có thể là array, vừa có thể là string
     */
    public static function sendRequest($method = 'GET', $requestURL, $requestParam = [], $arrHeader = [], $cookiePath = false)
    {
      if (!extension_loaded('curl')) {
        throw new \Exception('The cURL extension must be loaded in order to use this function.');
      }
      
      $strRequestURL = FilterUtil::string($requestURL);
      if (empty($strRequestURL)) {
        return false;
      }
      
      $curl   = null;
      
      $method = strtoupper($method);
      switch ($method) {
        case 'GET':
          if (is_array($requestParam) && !empty($requestParam)) {
            $requestURL = UrlUtil::appendParamsToUrl($requestURL, $requestParam);
          }
          
          $curl = curl_init($requestURL);
        case 'POST':
          $curl = curl_init($requestURL);
          
          curl_setopt($curl, CURLOPT_POST, 1);
          curl_setopt($curl, CURLOPT_POSTFIELDS, $requestParam);
          
          break;
        default:
          return false;
      }
      
      if (!$curl) {
        return false;
      }
      
      if (!empty($arrHeader)) {
        curl_setopt($curl, CURLOPT_HTTPHEADER, $arrHeader);
      }
      
      if ($cookiePath) {
        curl_setopt($curl, CURLOPT_COOKIESESSION, true);
        curl_setopt($curl, CURLOPT_COOKIEJAR, $cookiePath);
        curl_setopt($curl, CURLOPT_COOKIEFILE, $cookiePath);
      }
      
      curl_setopt($curl, CURLOPT_TIMEOUT, 60);
      curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 60);
      curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
      curl_setopt($curl, CURLOPT_DNS_CACHE_TIMEOUT, 0);
      curl_setopt($curl, CURLOPT_MAXREDIRS, 10);
      curl_setopt($curl, CURLOPT_ENCODING, "");
      curl_setopt($curl, CURLOPT_FRESH_CONNECT, true);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
      
      // Make request
      $rawDataResponse = curl_exec($curl);
      
      curl_close($curl);
      
      $dataResponseDecoded = null;
      if ($rawDataResponse !== false) {
        $dataResponseDecoded = json_decode($rawDataResponse, true);
      }
      
      return [
        'dataResponse' => $dataResponseDecoded,
        'requestDebug' => [
          'method'          => $method,
          'requestUrl'      => $requestURL,
          'requestParam'    => $requestParam,
          'requestHeader'   => $arrHeader,
          'rawDataResponse' => $rawDataResponse,
        ],
      ];
    }
  }
