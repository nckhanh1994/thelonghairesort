<?php
  
  namespace Util;
  
  class Pagination
  {
    
    private $totalRows = 0;
    private $rowsPerPage = 5;
    private $currentPage = 1;
    private $queryString = '';
    
    public function __construct()
    {
    
    }
    
    public function setTotalRows($totalRows)
    {
      $this->totalRows = $totalRows;
      return $this;
    }
    
    public function setcurrentPage($currentPage)
    {
      $this->currentPage = $currentPage;
      return $this;
    }
    
    public function setrowsPerPage($rowsPerPage)
    {
      $this->rowsPerPage = $rowsPerPage;
      return $this;
    }
    
    public function setQueryString($queryString)
    {
      $this->queryString = $queryString;
      return $this;
    }
    
    public function toArray()
    {
      $arr = [];
      // calculate last page
      $lastPage = ceil($this->totalRows / $this->rowsPerPage);
      // make sure we are within limits
      $this->currentPage = (int)$this->currentPage;
      if ($this->currentPage < 1) {
        $this->currentPage = 1;
      } elseif ($this->currentPage > $lastPage) {
        $this->currentPage = $lastPage;
      }
      $upto           = ($this->currentPage - 1) * $this->rowsPerPage;
      $arr['limit']   = 'LIMIT ' . $upto . ',' . $this->rowsPerPage;
      $arr['current'] = $this->currentPage;
      if ($this->currentPage == 1)
        $arr['previous'] = $this->currentPage;
      else
        $arr['previous'] = $this->currentPage - 1;
      if ($this->currentPage == $lastPage)
        $arr['next'] = $lastPage;
      else
        $arr['next'] = $this->currentPage + 1;
      $arr['last']  = $lastPage;
      $arr['info']  = 'Page (' . $this->currentPage . ' of ' . $lastPage . ')';
      $arr['pages'] = $this->getSurroundingPages($lastPage, $arr['next']);
      return $arr;
    }
    
    function getSurroundingPages($lastPage, $next)
    {
      $arr  = [];
      $show = 3; // how many boxes
      // at first
      if ($this->currentPage == 1) {
        // case of 1 page only
        if ($next == $this->currentPage)
          return [1];
        for ($i = 0; $i < $show; $i++) {
          if ($i == $lastPage)
            break;
          array_push($arr, $i + 1);
        }
        return $arr;
      }
      // at last
      if ($this->currentPage == $lastPage) {
        $start = $lastPage - $show;
        if ($start < 1)
          $start = 0;
        for ($i = $start; $i < $lastPage; $i++) {
          array_push($arr, $i + 1);
        }
        return $arr;
      }
      // at middle
      $start = $this->currentPage - $show;
      if ($start < 1)
        $start = 0;
      for ($i = $start; $i < $this->currentPage; $i++) {
        array_push($arr, $i + 1);
      }
      for ($i = ($this->currentPage + 1); $i < ($this->currentPage + $show); $i++) {
        if ($i == ($lastPage + 1))
          break;
        array_push($arr, $i);
      }
      return $arr;
    }
    
    public function toHtml($url, $arrRouter)
    {
      $arrPage = $this->toArray();
      $html    = '<ul class="pagination">';
      if ($arrPage['previous'] > 1) {
        $html .= '<li class="paginate_button page-item previous"><a class="page-link" href="' . $url->get($arrRouter + ['p' => $arrPage['previous']], $this->queryString) . '" rel="prev">Quay lại</a></li>';
      }
      
      if ($arrPage['previous'] > 1 && $arrPage['last'] > 5) {
        $html .= '<li class="paginate_button page-item previous disabled"><a class="page-link">…</a></li>';
      }
      
      foreach ($arrPage['pages'] as $page) {
        $html .= '<li ' . ($arrPage['current'] == $page ? 'class="paginate_button page-item active"' : 'class="paginate_button page-item "') . ' ><a class="page-link" href="' . $url->get($arrRouter + ['p' => $page], $this->queryString) . '">' . $page . '</a></li>';
      }
      if ($arrPage['current'] + 2 < $arrPage['last']) {
        $html .= '<li class="paginate_button page-item previous disabled"><a class="page-link">…</a></li>';
      }
      if ($arrPage['next'] < $arrPage['last']) {
        $html .= '<li class="paginate_button page-item next"><a class="page-link" href="' . $url->get($arrRouter + ['p' => $arrPage['next']], $this->queryString) . '" rel="next">Xem thêm</a></li>';
      }
      $html .= '</ul>';
      
      return $html;
    }
    
    public function toHtmlV2($url, $arrRouter)
    {
      $arrPage = $this->toArray();
      if (empty($arrPage['pages'])) {
        return '';
      }
      $html = '<ul class="pagination b-pagination pagination-md">';
      if ($arrPage['current'] == 1) {
        $html .= '<li class="page-item disabled"><span class="page-link">«</span></li>';
        $html .= '<li class="page-item disabled"><span class="page-link">‹</span></li>';
      } else if ($arrPage['current'] > 1) {
        $html .= '<li class="page-item"><a class="page-link" href="' . $url->get($arrRouter + ['page' => $arrPage['previous']]) . $this->queryString . '" tabindex="1">‹</a></li>';
      }
      if ($arrPage['previous'] > 1 && $arrPage['last'] > 5) {
        $html .= '<li class="page-item disabled"><span class="page-link">...</span></li>';
      }
      
      foreach ($arrPage['pages'] as $page) {
        $html .= '<li ' . ($arrPage['current'] == $page ? 'class="page-item active"' : 'class="page-item "') . ' ><a class="page-link" href="' . $url->get($arrRouter + ['page' => $page]) . $this->queryString . '" tabindex="' . $page . '">' . $page . '</a></li>';
      }
      if ($arrPage['current'] + 2 < $arrPage['last']) {
        $html .= '<li class="page-item disabled"><span class="page-link">...</span></li>';
      }
      
      if ($arrPage['current'] == $arrPage['next']) {
        $html .= '<li class="page-item disabled"><span class="page-link">›</span></li>';
        $html .= '<li class="page-item disabled"><span class="page-link">»</span></li>';
      } else if ($arrPage['current'] < $arrPage['next']) {
        $html .= '<li class="page-item"><a class="page-link" href="' . $url->get($arrRouter + ['page' => $arrPage['next']]) . $this->queryString . '" tabindex="' . $arrPage['next'] . '">›</a></li>';
      }
      $html .= '</ul>';
      return $html;
    }
    
    /**
     * @param string $url
     * @param array $arrRouter
     */
    public function toHtmlFE($url = '', $arrRouter = [])
    {
      $arrPage = $this->toArray();
      if (empty($arrPage['pages'])) {
        return '';
      }
      $html = '<ul class="nav-pagination">';
      if ($arrPage['current'] == 1) {
        $html .= '';
      } else if ($arrPage['current'] > 1) {
        $html .= '<li class="pagination_previous"><a title="« Previous" href="' . $url->get($arrRouter + ['page' => $arrPage['previous']]) . $this->queryString . '">«</a></li>';
      }
      if ($arrPage['previous'] > 1 && $arrPage['last'] > 5) {
        $html .= '<li><span>…</span></li>';
      }
      
      foreach ($arrPage['pages'] as $page) {
        if ($arrPage['current'] == $page)
          $html .= '<li class="current"><span>' . $page . '</span></li>';
        else
          $html .= '<li><a href="' . $url->get($arrRouter + ['page' => $page]) . $this->queryString . '" title="">' . $page . '</a></li>';
      }
      if ($arrPage['current'] + 2 < $arrPage['last']) {
        $html .= '<li><span>…</span></li>';
      }
      
      if ($arrPage['current'] == $arrPage['next']) {
        $html .= '';
      } else if ($arrPage['current'] < $arrPage['next']) {
        $html .= '<li class="pagination_next"><a href="' . $url->get($arrRouter + ['page' => $arrPage['next']]) . $this->queryString . '" title="Next »">»</a></li>';
      }
      $html .= '</ul>';
      return $html;
    }
    
    /**
     * @param string $url
     * @param array $arrRouter
     */
    public function toHtmlTheme1($url = '', $arrRouter = [])
    {
      $arrPage = $this->toArray();
      if (empty($arrPage['pages'])) {
        return '';
      }
      
      if (count($arrPage['pages']) < 2) {
        return '';
      }
      
      $html = '<div class="paging_content">';
      if ($arrPage['current'] == 1) {
        $html .= '';
      } else if ($arrPage['current'] > 1) {
        $html .= '<a title="« Previous" href="' . $url->get($arrRouter + ['page' => $arrPage['previous']]) . $this->queryString . '" class="item_paging paging_normal">&lt;</a>';
      }
      if ($arrPage['previous'] > 1 && $arrPage['last'] > 5) {
        $html .= '<a href="" class="paging_normal">..</a>';
      }
      
      foreach ($arrPage['pages'] as $page) {
        if ($arrPage['current'] == $page)
          $html .= '<font class="item_paging paging_active">' . $page . '</font>';
        else
          $html .= '<a href="' . $url->get($arrRouter + ['page' => $page]) . $this->queryString . '" title="" class="item_paging paging_normal">' . $page . '</a>';
      }
      if ($arrPage['current'] + 2 < $arrPage['last']) {
        $html .= '<a href="" class="paging_normal">..</a>';
      }
      
      if ($arrPage['current'] == $arrPage['next']) {
        $html .= '';
      } else if ($arrPage['current'] < $arrPage['next']) {
        $html .= '<a href="' . $url->get($arrRouter + ['page' => $arrPage['next']]) . $this->queryString . '" title="Next »" class="item_paging paging_normal">&gt;</a>';
      }
      $html .= '</div>';
      return $html;
    }
    
    /**
     * @param string $url
     * @param array $arrRouter
     */
    public function toHtmlTheme2($url = '', $arrRouter = [])
    {
      $arrPage = $this->toArray();
      if (empty($arrPage['pages'])) {
        return '';
      }
      
      if (count($arrPage['pages']) < 2) {
        return '';
      }
      
      $html = '<ul class="pagination">';
      if ($arrPage['current'] == 1) {
        $html .= '';
      } else if ($arrPage['current'] > 1) {
        $html .= '<li><a title="« Previous" href="' . $url->get($arrRouter + ['page' => $arrPage['previous']]) . $this->queryString . '" class="">&lt;</a></li>';
      }
      if ($arrPage['previous'] > 1 && $arrPage['last'] > 5) {
        $html .= '<li><a href="' . $url->get($arrRouter + ['page' => current($arrPage['pages']) - 1]) . $this->queryString . '" title="Next »" class="">...</a></li>';
      }
      
      foreach ($arrPage['pages'] as $page) {
        if ($arrPage['current'] == $page)
          $html .= '<li class="active"><a class="">' . $page . '</a></li>';
        else
          $html .= '<li><a href="' . $url->get($arrRouter + ['page' => $page]) . $this->queryString . '" title="" class="">' . $page . '</a></li>';
      }
      if ($arrPage['current'] + 2 < $arrPage['last']) {
        $html .= '<li><a href="' . $url->get($arrRouter + ['page' => end($arrPage['pages']) + 1]) . $this->queryString . '" title="Next »" class="">...</a></li>';
      }
      
      if ($arrPage['current'] == $arrPage['next']) {
        $html .= '';
      } else if ($arrPage['current'] < $arrPage['next']) {
        $html .= '<li><a href="' . $url->get($arrRouter + ['page' => $arrPage['next']]) . $this->queryString . '" title="Next »" class="">&gt;</a></li>';
      }
      $html .= '</ul>';
      return $html;
    }
  }
