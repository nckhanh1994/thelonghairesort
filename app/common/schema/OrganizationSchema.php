<?php
  
  namespace Schema;
  
  class OrganizationSchema
  {
    public function __construct()
    {
    }
    
    public function build()
    {
      return '
      <script type = "application/ld+json" >
        {
          "@context": "https://schema.org",
          "@type": "Organization",
          "url": "' . BASE_URL . '",
          "logo": "' . RESOURCE_URL . '/global/image/logo.png",
          "email": "' . SUPPORT_EMAIL . '",
          "sameAs": [
          ],
          "contactPoint": [{
            "@type": "ContactPoint",
            "telephone": "+' . SUPPORT_PHONE . '",
            "contactType": "customer service",
            "areaServed": "VI"
          }],
          "address": {
            "@type": "PostalAddress",
            "addressLocality": "Ho Chi Minh City",
            "addressRegion": "HCM",
            "postalCode": "70000",
            "streetAddress": "309/22 Nguyễn Văn Trỗi Phường 1 Tân Bình"
          },
        }
        </script>
      ';
    }
  }
