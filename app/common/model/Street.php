<?php
  
  namespace Model;
  
  use Util\FilterUtil;
  
  class Street extends ModelAbstract
  {
    private $_storage = null;
    
    /**
     * @construct
     */
    public function onConstruct()
    {
      $this->_keyCache = $this->setKeyCache('cacheStreet');
      
      $this->_storage = new \Storage\StorageStreet();
      
      return parent::onConstruct();
    }
    
    /**
     * return @array
     */
    public function getList(array $conditions = [], $page = 1, $limit = 15, $order = '', $fields = ['*'])
    {
      return $this->_storage->getListLimit($conditions, $page, $limit, $order, $fields);
    }
    
    /**
     * return @array
     */
    public function getListLimit(array $conditions = [], $page = 1, $limit = 15, $order = '', $fields = ['*'])
    {
      $strKeyCache = $this->generateKeyCache($conditions + ['page' => $page, 'limit' => $limit, 'strOrder' => $order] + $fields, __METHOD__);
      $arrList     = $this->_cache->read($strKeyCache);
      if (empty($arrList)) {
        $arrList = $this->_storage->getListLimit($conditions, $page, $limit, $order, $fields);
        if (!empty($arrList)) {
          $this->_cache->add($strKeyCache, $arrList, $this->_cacheLifeTime);
        }
      }
      
      return $arrList;
    }
    
    /**
     * return @integer
     */
    public function getTotal($conditions = []): int
    {
      if (!is_array($conditions)) {
        return false;
      }
      
      $strKeyCache = $this->generateKeyCache($conditions, __METHOD__);
      $total       = $this->_cache->read($strKeyCache);
      if (empty($total)) {
        $total = $this->_storage->getTotal($conditions);
        if (!empty($total)) {
          $this->_cache->add($strKeyCache, $total, $this->_cacheLifeTime);
        }
      }
      
      return $total;
    }
    
    /**
     * return @array
     */
    public function getDetail($conditions = [], $fields = ['*'])
    {
      $strKeyCache = $this->generateKeyCache($conditions + $fields, __METHOD__);
      $arrDetail   = $this->_cache->read($strKeyCache);
      if (empty($arrDetail)) {
        $arrDetail = $this->_storage->getDetail($conditions, $fields);
        if (!empty($arrDetail)) {
          $this->_cache->add($strKeyCache, $arrDetail, $this->_cacheLifeTime);
        }
      }
      
      return $arrDetail;
    }
    
    /**
     * @param type array $params
     * @param type array $conditions
     * return @integer
     */
    public function add(array $params = [], array $conditions = [])
    {
      $conditions['config'] = isset($conditions['config']) ? $conditions['config'] : [];
      if (empty($params)) {
        return false;
      }
      
      $result = $this->_storage->add($params, $conditions);
      if ($result) {
        $this->_cache->increase($this->_keyCache, 1);
      }
      
      return $result;
    }
    
    /**
     * @param type array $params
     * @param type array $conditions
     * return @boolean
     */
    public function edit(array $params = [], array $conditions = [])
    {
      $conditions['config'] = isset($conditions['config']) ? $conditions['config'] : [];
      if (empty($params) || empty($conditions)) {
        return false;
      }
      
      $result = $this->_storage->edit($params, $conditions);
      if ($result) {
        $this->_cache->increase($this->_keyCache, 1);
      }
      
      return $result;
    }
  }
