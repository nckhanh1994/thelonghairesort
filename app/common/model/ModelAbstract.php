<?php
  
  namespace Model;
  
  use Phalcon\Mvc\Model;
  
  use Cache\Memcached;
  
  class ModelAbstract extends Model
  {
    protected $_action;
    protected $_table;
    protected $_model;
    protected $_primaryKey;
    protected $_cache;
    protected $_keyCache;
    protected $_cacheLifeTime = 7 * 24 * 60 * 60;
    protected $arrIgnoreAction = ['onConstruct', 'initialize', '__construct', 'setDI', 'getDI', 'setEventsManager', 'getEventsManager', '__get', 'permission', 'checkPermission', 'renderTemplate', 'responseJSON', 'strFilters', 'renderEmailTemplate', 'renderPrintTemplate', 'redirect', 'accessDenied', 'isValid'];
    
    /**
     * @construct
     */
    public function onConstruct()
    {
      $this->_cache = new Memcached();
      if (!empty($this->_keyCache)) {
        if (empty($this->_cache->read($this->_keyCache))) {
          $this->_cache->add($this->_keyCache, 1, $this->_cacheLifeTime);
        }
      }
    }
    
    /**
     * @param field_type $keyCache
     */
    public function setKeyCache($keyCache)
    {
      $this->_keyCache = PREFIX . $keyCache;
      
      return $this->_keyCache;
    }
    
    /**
     * @generate
     */
    public function generateKeyCache($conditions = [], $strMethod = '')
    {
      $conditions = array_filter($conditions, function ($value) {
        return ($value !== null && $value !== false && $value !== '');
      });
      if (empty($conditions) || empty($strMethod) || !is_array($conditions)) {
        $conditions = ['is_deleted' => 0];
      }
      $strKeyCache = "$strMethod:" . http_build_query($conditions, '', ':');
      
      return APPLICATION_ENV . ':' . crc32($strKeyCache) . ':' . $this->_cache->read($this->_keyCache);
    }
    
    /**
     * @queryBuilder
     */
    public function createBuilder()
    {
      $this->_action = $this->modelsManager->createBuilder()->from([$this->_table => $this->_model]);
      
      return $this;
    }
    
    /**
     * @column
     */
    public function columns($fields = ['*'])
    {
      $this->_action->columns(!empty($fields) ? $fields : ['*']);
      
      return $this;
    }
    
    /**
     * @join table
     */
    public function join($conditions = [])
    {
      if (empty($conditions)) {
        return;
      }
      
      list($modelTarget, $table, $condition, $tableTarget, $conditionTarget, $typeJoin) = [$conditions['modelTarget'], $conditions['table'], $conditions['condition'], $conditions['tableTarget'], $conditions['conditionTarget'], $conditions['type']];
      $this->_action->join($modelTarget, "[$table].$condition = [$tableTarget].$conditionTarget", $tableTarget, $typeJoin);
      
      return $this;
    }
    
    /**
     * @where
     */
    public function buildWhere($conditions, $fields = [])
    {
      if (!empty($conditions['join'])) {
        $this->buildJoin($conditions['join'], $fields);
      }
      
      if (!empty($conditions['group_by'])) {
        $this->_action->groupBy("[$this->_table]." . $conditions['group_by']);
      }
      
      return $this;
    }
    
    
    /**
     * @orderBy
     */
    public function orderBy($strOrderBy = '')
    {
      $this->_action->orderBy($strOrderBy);
      
      return $this;
    }
    
    /**
     * @limit
     */
    public function limit($intLimit = 15, $offset = 0)
    {
      $this->_action->limit($intLimit, $offset);
      
      return $this;
    }
    
    /**
     * @execute
     */
    public function execute()
    {
      return $this->_action->getQuery()->execute();
    }
    
    /**
     * return @array
     */
    public function buildJoin($conditions, $fields)
    {
      $arrTmpColumns = [];
      foreach ($conditions as $condition) {
        $this->join($condition);
        // Columns khi join
        $arrTmpColumns = array_merge(($condition['columns']) ? $this->buildColumn($condition['columns'], $condition['tableTarget']) : [], $arrTmpColumns);
      }
      
      // Merge với cột đã chọn
      $fields = array_merge($arrTmpColumns, !empty($fields) ? $this->buildColumn($fields, $this->_table) : $this->buildColumn($this->getColumns(), $this->_table));
      $this->_action->columns($fields);
      
      return $this;
    }
    
    /**
     * @build columns
     */
    public function buildColumn($fields = [], $table = "")
    {
      if (empty($fields) || empty($table)) {
        return;
      }
      
      array_walk($fields, function (&$value, $key) use ($table) {
        $value = "[$table].$value";
      });
      
      return $fields;
    }
    
    /**
     * @columns table
     */
    public function getColumns()
    {
      // Get Phalcon\Mvc\Model\Metadata instance
      $metadata = $this->getModelsMetaData();
      
      // Get fields models
      return $metadata->getAttributes($this);
    }
    
    /**
     * @purifier
     */
    public function purifier($arrParams, $arrConfig = [])
    {
      require_once DIR_PATH . '/vendor/ezyang/htmlpurifier/library/HTMLPurifier.auto.php';
      
      $config = \HTMLPurifier_Config::createDefault();
      if (!empty($arrConfig)) {
        foreach ($arrConfig as $key => $value) {
          $config->set($key, $value);
        }
      }
      
      $purifier = new \HTMLPurifier($config);
      foreach ($arrParams as $key => $param) {
        $arrParams[ $key ] = $purifier->purify(trim($param));
      }
      
      return $arrParams;
    }
    
    /**
     * @getPhql - @debug
     */
    public function getQueryDebug($conditions)
    {
      $strPhql = $this->_action->getPhql();
      $strPhql = str_replace("[$this->_table]", "`$this->_table`", $strPhql);
      $strPhql = str_replace("[$this->_model]", "`$this->_table`", $strPhql);
      foreach ($conditions as $key => $value) {
        $strPhql = str_replace(":$key:", "'$value'", $strPhql);
      }
      if (!empty($conditions['join'])) {
        $tmp = $conditions['join'];
        foreach ($tmp as $condition) {
          list($model, $table) = [$condition['modelTarget'], $condition['tableTarget']];
          $strPhql = str_replace("[$model]", "`$table`", $strPhql);
          $strPhql = str_replace("[$table]", "`$table`", $strPhql);
        }
      }
      $strPhql = str_replace('LIMIT :APL0: OFFSET :APL1:', 'LIMIT 1', $strPhql);
      echo $strPhql;
      die;
    }
    
    public function rebuildArrayByKey($newKey, $_arrParam = [])
    {
      if (!is_array($_arrParam)) {
        return [];
      }
      
      $arrReturn = [];
      foreach ($_arrParam as $key => $_arrParam) {
        $arrReturn[ $_arrParam[ $newKey ] ] = $_arrParam;
      }
      
      return $arrReturn;
    }
  }
