<?php
  
  namespace Models;
  
  class GroupPermission extends ModelAbstract
  {
    protected $arrIgnoreAction = ['onConstruct', 'initialize', '__construct', 'setDI', 'getDI', 'setEventsManager', 'getEventsManager', '__get', 'permission', 'checkPermission', 'renderTemplate', 'responseJSON', 'strFilters', 'renderEmailTemplate', 'renderPrintTemplate', 'redirect', 'accessDenied'];
    
    /**
     * @construct
     */
    public function onConstruct()
    {
      $this->_keyCache = $this->setKeyCache('cacheGroupPermission');
      $this->_table    = 'group_permissions';
      $this->_model    = 'Models\GroupPermission';
      
      return parent::onConstruct();
    }
    
    /**
     * @return type @array
     */
    public function getListLimit($_arrCondition = [], $_page = 1, $_limit = 15, $_order = "id DESC", $_arrColumn = ['*'])
    {
      try {
        if (!is_array($_arrCondition) || !is_array($_arrColumn)) {
          return false;
        }
        
        $strKeyCache = $this->generateKeyCache($_arrCondition + ['page' => $_page, 'limit' => $_limit, 'strOrder' => $_order] + $_arrColumn, __METHOD__);
        $arrList     = $this->_cache->read($strKeyCache);
        if (empty($arrList)) {
          $arrList = $this->createBuilder()
            ->columns($_arrColumn)
            ->buildWhere($_arrCondition)
            ->orderBy("$this->_table.$_order")
            ->limit($_limit, $_limit * ($_page - 1))
            ->execute()
            ->toArray();
          
          if (!empty($arrList)) {
            if (!empty($_arrCondition['key_by_field'])) {
              $arrList = $this->reBuildKeyArray($_arrCondition['key_by_field'], $arrList);
            }
            // Add cache
            $this->_cache->add($strKeyCache, $arrList, $this->_cacheLifeTime);
          }
        }
        
        return $arrList;
      } catch (\Exception $ex) {
        if (APPLICATION_ENV == 'dev') {
          var_dump("\r\n" . $ex->getMessage() . "\r\n" . $ex->getFile() . ":" . $ex->getLine());
        }
        
        return false;
      }
    }
    
    /**
     * @return type @array
     */
    public function getResourceList($_arrCondition = [], $_page = 1, $_limit = 15, $_order = "id DESC", $_arrColumn = ['*'])
    {
      try {
        if (!is_array($_arrCondition) || !is_array($_arrColumn)) {
          return false;
        }
        
        $strKeyCache = $this->generateKeyCache($_arrCondition + ['page' => $_page, 'limit' => $_limit, 'strOrder' => $_order] + $_arrColumn, __METHOD__);
        $arrList     = $this->_cache->read($strKeyCache);
        if (empty($arrList)) {
          $arrList = $this->createBuilder()
            ->columns($_arrColumn)
            ->buildWhere($_arrCondition)
            ->orderBy("$this->_table.$_order")
            ->limit($_limit, $_limit * ($_page - 1))
            ->execute()
            ->toArray();
          
          if (!empty($arrList)) {
            $arrTmp = [];
            foreach ($arrList as $value) {
              $arrTmp[] = $value['module'] . ':' . $value['controller'] . ':' . $value['action'];
            }
            $arrList = $arrTmp;
            unset($arrTmp);
            // Add cache
            $this->_cache->add($strKeyCache, $arrList, $this->_cacheLifeTime);
          }
        }
        
        return $arrList;
      } catch (\Exception $ex) {
        if (APPLICATION_ENV == 'dev') {
          var_dump("\r\n" . $ex->getMessage() . "\r\n" . $ex->getFile() . ":" . $ex->getLine());
        }
        
        return false;
      }
    }
    
    public function getAllResource($_module = 'Backend')
    {
      //module frontend
      $files       = scandir(BASE_PATH . '/module/' . $_module . '/controllers/');
      $arrData     = [];
      $controllers = [];
      unset($files[ array_search('.', $files) ]);
      unset($files[ array_search('..', $files) ]);
      $router = $this->getDI()->get('router');
      
      foreach ($files as $file) {
        if ($controller = $this->extractController($file)) {
          $controllerName = str_replace('Controller', '', $controller);
          $arrAction      = [];
          
          foreach (get_class_methods('\\Module\\' . $_module . '\\Controllers\\' . $controller) as $action) {
            if (!in_array($action, $this->arrIgnoreAction)) {
              $arrAction[] = str_replace('Action', '', $action);
            }
          }
          $arrData[] = ['module' => '' . $_module . '', 'controller' => $controllerName, 'action' => $arrAction];
        }
      }
      
      return $arrData;
    }
    
    /**
     * @return type @integer
     */
    public function getTotal($_arrCondition = [])
    {
      try {
        if (!is_array($_arrCondition)) {
          return false;
        }
        
        $strKeyCache = $this->generateKeyCache($_arrCondition, __METHOD__);
        $arrTotal    = $this->_cache->read($strKeyCache);
        if (empty($arrTotal)) {
          $arrTotal = current($this->createBuilder()
            ->columns(['total' => 'COUNT(*)'])
            ->buildWhere($_arrCondition)
            ->execute()
            ->toArray());
          
          if (!empty($arrTotal)) {
            // Add cache
            $this->_cache->add($strKeyCache, $arrTotal, $this->_cacheLifeTime);
          }
        }
        
        return $arrTotal['total'];
      } catch (\Exception $ex) {
        if (APPLICATION_ENV == 'dev') {
          var_dump("\r\n" . $ex->getMessage() . "\r\n" . $ex->getFile() . ":" . $ex->getLine());
        }
        
        return false;
      }
    }
    
    /**
     * @return type @array
     */
    public function getDetail($_arrCondition = [], $_arrColumn = ['*'])
    {
      try {
        if (!is_array($_arrCondition) || !is_array($_arrColumn)) {
          return false;
        }
        
        $strKeyCache = $this->generateKeyCache($_arrCondition + $_arrColumn, __METHOD__);
        $arrDetail   = $this->_cache->read($strKeyCache);
        if (empty($arrDetail)) {
          $arrDetail = current($this->createBuilder()
            ->columns($_arrColumn)
            ->buildWhere($_arrCondition)
            ->execute()
            ->toArray());
          
          if (!empty($arrDetail)) {
            // Add cache
            $this->_cache->add($strKeyCache, $arrDetail, $this->_cacheLifeTime);
          }
        }
        
        return $arrDetail;
      } catch (\Exception $ex) {
        if (APPLICATION_ENV == 'dev') {
          var_dump("\r\n" . $ex->getMessage() . "\r\n" . $ex->getFile() . ":" . $ex->getLine());
        }
        
        return false;
      }
    }
    
    /**
     * @param type array $params
     * @param type array $conditions
     * return @integer
     */
    public function add($_arrParam = [], $_arrCondition = [])
    {
      try {
        if (empty($_arrParam) || !is_array($_arrParam)) {
          return false;
        }
        
        $result = self::save(self::purifier($_arrParam, $_arrCondition['config']));
        if ($result) {
          $intInsertId = $this->getWriteConnection()->lastInsertId();
          if ($intInsertId) {
            $this->_cache->increase($this->_tmpKeyCache, 1);
          }
          
          return $intInsertId;
        }
        
        
        return false;
      } catch (\Exception $ex) {
        if (APPLICATION_ENV == 'dev') {
          var_dump("\r\n" . $ex->getMessage() . "\r\n" . $ex->getFile() . ":" . $ex->getLine());
        }
        
        return false;
      }
    }
    
    /**
     * @param type array $params
     * @param type array $conditions
     * return @boolean
     */
    public function edit($_arrParam = [], $_arrCondition = [])
    {
      try {
        if (empty($_arrParam) || !is_array($_arrParam) || empty($_arrCondition) || !is_array($_arrCondition)) {
          return false;
        }
        
        $builder = $this->createBuilder()
          ->buildWhere($_arrCondition)
          ->execute();
        
        if (!empty($builder->count())) {
          $builder->update(self::purifier($_arrParam, $_arrCondition['config']));
          if (!empty($builder->getMessages())) {
            if (APPLICATION_ENV == 'dev') {
              var_dump($builder->getMessages());
            }
            
            return false;
          }
          $this->_cache->increase($this->_tmpKeyCache, 1);
          
          return $result;
          
        }
        
        return false;
      } catch (\Exception $ex) {
        if (APPLICATION_ENV == 'dev') {
          var_dump("\r\n" . $ex->getMessage() . "\r\n" . $ex->getFile() . ":" . $ex->getLine());
        }
        
        return false;
      }
    }
    
    public function delete($_arrCondition)
    {
      try {
        if (empty($_arrCondition)) {
          return false;
        }
        $rawQuery = 'DELETE FROM ' . $this->_table . ' WHERE 1=1 AND group_id = ' . $_arrCondition['group_id'] . ' AND module = "' . $_arrCondition['module'] . '" AND controller = "' . $_arrCondition['controller'] . '" AND action = "' . $_arrCondition['action'] . '"';
        
        $result = $this->getReadConnection()->query($rawQuery);
        
        $this->_cache->increase($this->_tmpKeyCache, 1);
        
        return true;
      } catch (\Exception $ex) {
        if (APPLICATION_ENV == 'dev') {
          var_dump("\r\n" . $ex->getMessage() . "\r\n" . $ex->getFile() . ":" . $ex->getLine());
        }
        
        return false;
      }
    }
    
    /**
     * @build conditions
     */
    public function buildWhere($_arrCondition)
    {
      if (!is_array($_arrCondition) || empty($_arrCondition)) {
        return $this;
      }
      
      /* @parent */
      parent::buildWhere($_arrCondition);
      /* @this */
      if (!empty($_arrCondition['group_id']))
        $this->_action->andWhere("[$this->_table].group_id = :group_id:", ['group_id' => $_arrCondition['group_id']]);
      
      if (!empty($_arrCondition['module']))
        $this->_action->andWhere("[$this->_table].module = :module:", ['module' => $_arrCondition['module']]);
      
      if (!empty($_arrCondition['controller']))
        $this->_action->andWhere("[$this->_table].controller = :controller:", ['controller' => $_arrCondition['controller']]);
      
      if (!empty($_arrCondition['action']))
        $this->_action->andWhere("[$this->_table].action = :action:", ['action' => $_arrCondition['action']]);
      
      return $this;
    }
    
    public function reBuildKeyArray($newKey, $_arrParam = [])
    {
      if (!is_array($_arrParam) || empty($newKey)) {
        return [];
      }
      
      $arrReturn = [];
      foreach ($_arrParam as $key => $_arrParam) {
        $arrReturn[ $_arrParam[ $newKey ] ] = $_arrParam;
      }
      
      return $arrReturn;
    }
    
    protected function extractController($name)
    {
      $filename = explode('.php', $name);
      if (count(explode('Controller.php', $name)) > 1) {
        if (count($filename) > 1) {
          return $filename[0];
        }
      }
      
      return false;
    }
  }
  
  ?>