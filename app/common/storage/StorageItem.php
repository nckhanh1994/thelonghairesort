<?php
  
  namespace Storage;
  
  use Model\ModelAbstract;
  use Util\GeneralUtil;
  
  class StorageItem extends ModelAbstract
  {
    public function onConstruct()
    {
      $this->_table      = 'items';
      $this->_model      = 'Storage\StorageItem';
      $this->_primaryKey = 'item_id';
      
      $this->setSource($this->_table);
    }
    
    
    /**
     * return @array
     */
    public function getListLimit($conditions, $page, $limit, $order, $fields)
    {
      try {
        $strOrder = !empty($order) ? "$this->_table.$order" : $this->_primaryKey . ' DESC';
        
        $arrList = $this->createBuilder()
          ->columns($fields)
          ->orderBy($strOrder)
          ->buildWhere($conditions, $fields)
          ->limit($limit, $limit * ($page - 1))
          ->execute()
          ->toArray();
        
        if (!empty($conditions['key_by_field'])) {
          $arrList = $this->rebuildArrayByKey($conditions['key_by_field'], $arrList);
        }
        
        return $arrList;
      } catch (\Exception $ex) {
        if (APPLICATION_ENV == 'dev') {
          var_dump("\r\n" . $ex->getMessage() . "\r\n" . $ex->getFile() . ":" . $ex->getLine());
          die;
        }
        
        return false;
      }
    }
    
    /**
     * return @integer
     */
    public function getTotal($conditions)
    {
      try {
        $arrTotal = current($this->createBuilder()
          ->columns(['total' => 'COUNT(*)'])
          ->buildWhere($conditions)
          ->execute()
          ->toArray());
        
        return $arrTotal['total'];
      } catch (\Exception $ex) {
        if (APPLICATION_ENV == 'dev') {
          var_dump("\r\n" . $ex->getMessage() . "\r\n" . $ex->getFile() . ":" . $ex->getLine());
          die;
        }
        
        return false;
      }
    }
    
    /**
     * return @array
     */
    public function getDetail($conditions, $fields)
    {
      try {
        $arrDetail = current($this->createBuilder()
          ->columns($fields)
          ->buildWhere($conditions)
          ->execute()
          ->toArray()
        );
        
        return $arrDetail;
      } catch (\Exception $ex) {
        if (APPLICATION_ENV == 'dev') {
          
          var_dump("\r\n" . $ex->getMessage() . "\r\n" . $ex->getFile() . ":" . $ex->getLine());
          die;
        }
        
        return false;
      }
    }
    
    /**
     * Lấy danh sách items theo danh mục
     * @param int $page
     * @param int $limit
     * return @array
     */
    public function getItemsByCat($conditions, $page, $limit, $order, $fields)
    {
      $arrCategories = GeneralUtil::$arrCategories;
      
      $strWhere = '';
      if (!empty($conditions['not_item_id_list']) && is_array($conditions['not_item_id_list'])) {
        $strWhere .= ' AND item_id NOT IN(' . implode(',', $conditions['not_item_id_list']) . ')';
      }
      
      $rawQuery = '';
      foreach ($arrCategories as $id => $category) {
        $rawQuery .= '(
          SELECT ' . implode(',', $fields) . '
          FROM items
          WHERE 1=1
          AND category_id = ' . $id . '
          AND is_cart = 0
          AND is_private = 0
          ' . $strWhere . '
          ORDER BY ' . $order . '
          LIMIT ' . (($page - 1) * $limit) . ', ' . $limit . '
        ) UNION ALL';
      }
      
      $rawQuery = rtrim($rawQuery, ' UNION ALL');
      
      $result = $this->getReadConnection()->query($rawQuery);
      $result->setFetchMode(\Phalcon\Db\Enum::FETCH_ASSOC);
      
      $arrList = $result->fetchAll();
      
      $arrTemp = []; // [category_id] => []
      foreach ($arrList as $arrValue) {
        $arrTemp[ $arrValue['category_id'] ][] = $arrValue;
      }
      $arrList = $arrTemp;
      
      unset($arrTemp);
      
      return $arrList;
    }
    
    /**
     * @param type array $params
     * @param type array $conditions
     * return @integer
     */
    public function add($params, $conditions)
    {
      try {
        if (empty($params)) {
          return false;
        }
        
        $result = self::save(self::purifier($params, $conditions['config']));
        if ($result) {
          $intInsertId = $this->getWriteConnection()->lastInsertId();
          
          return $intInsertId;
        }
        
        
        return false;
      } catch (\Exception $ex) {
        if (APPLICATION_ENV == 'dev') {
          var_dump("\r\n" . $ex->getMessage() . "\r\n" . $ex->getFile() . ":" . $ex->getLine());
          die;
        }
        
        return false;
      }
    }
    
    /**
     * @param type array $params
     * @param type array $conditions
     * return @boolean
     */
    public function edit($params, $conditions)
    {
      try {
        if (empty($params) || empty($conditions)) {
          return false;
        }
        
        $builder = $this->createBuilder()
          ->buildWhere($conditions)
          ->execute();
        
        if (!empty($builder->count())) {
          $builder->update(self::purifier($params, $conditions['config']));
          if (!empty($builder->getMessages())) {
            return false;
          }
          
          return true;
        }
        
        return false;
      } catch (\Exception $ex) {
        if (APPLICATION_ENV == 'dev') {
          var_dump("\r\n" . $ex->getMessage() . "\r\n" . $ex->getFile() . ":" . $ex->getLine());
          die;
        }
        
        return false;
      }
    }
    
    /**
     * build @conditions
     */
    public function buildWhere($conditions, $fields = [])
    {
      if (empty($conditions)) {
        return $this;
      }
      /* @parent */
      parent::buildWhere($conditions, $fields);
      /* @this */
      if (!empty($conditions['item_id'])) {
        $this->_action->andWhere("[$this->_table].item_id = :item_id:", ['item_id' => $conditions['item_id']]);
      }
      
      if (!empty($conditions['keyword'])) {
        $value = trim($conditions['keyword']);
        
        $this->_action->andWhere("([$this->_table].item_name LIKE :keyword: OR [$this->_table].host_phone LIKE :keyword:)", ['keyword' => "%$value%"]);
      }
      
      if (!empty($conditions['item_name_like'])) {
        $value = trim($conditions['item_name_like']);
        
        $this->_action->andWhere("[$this->_table].item_name LIKE :item_name_like:", ['item_name_like' => "%$value%"]);
      }
      
      if (!empty($conditions['street_name_like'])) {
        $value = trim($conditions['street_name_like']);
        
        $this->_action->andWhere(
          "([$this->_table].full_address_keyword LIKE :street_name_like:) OR ([$this->_table].address LIKE :street_name_like:)"
          , ['street_name_like' => "%$value%"]);
      }
      
      if (!empty($conditions['not_item_id'])) {
        $this->_action->andWhere("[$this->_table].item_id != :not_item_id:", ['not_item_id' => $conditions['not_item_id']]);
      }
      
      if (isset($conditions['status'])) {
        $this->_action->andWhere("[$this->_table].status = :status:", ['status' => $conditions['status']]);
      }
      
      if (isset($conditions['is_deleted'])) {
        $this->_action->andWhere("[$this->_table].is_deleted = :is_deleted:", ['is_deleted' => $conditions['is_deleted']]);
      }
      
      if (isset($conditions['is_cart'])) {
        $this->_action->andWhere("[$this->_table].is_cart = :is_cart:", ['is_cart' => $conditions['is_cart']]);
      }
      
      if (!empty($conditions['district_id_list'])) {
        $this->_action->inWhere("[$this->_table].district_id", $conditions['district_id_list']);
      }
      
      if (!empty($conditions['category_id_list'])) {
        $this->_action->inWhere("[$this->_table].category_id", $conditions['category_id_list']);
      }
      
      if (!empty($conditions['created_at_start']) && !empty($conditions['created_at_end'])) {
        $this->_action->betweenWhere('created_at', $conditions['created_at_start'], $conditions['created_at_end']);
      }
      
      if (isset($conditions['price_less_than'])) {
        $this->_action->andWhere("[$this->_table].price <= :price_less_than:", ['price_less_than' => $conditions['price_less_than']]);
      }
      
      if (!empty($conditions['or_user_created_id'])) {
        $this->_action->orWhere("[$this->_table].user_created_id = :or_user_created_id:", ['or_user_created_id' => $conditions['or_user_created_id']]);
        $this->_action->andWhere("[$this->_table].is_deleted = :is_deleted:", ['is_deleted' => $conditions['is_deleted']]);
      }
      
      if (isset($conditions['is_favorite'])) {
        $this->_action->andWhere("[$this->_table].is_favorite = :is_favorite:", ['is_favorite' => $conditions['is_favorite']]);
      }
      
      if (isset($conditions['is_private'])) {
        $this->_action->andWhere("[$this->_table].is_private = :is_private:", ['is_private' => $conditions['is_private']]);
      }
      
      if (!empty($conditions['price_vnd_from']) && !empty($conditions['price_vnd_to'])) {
        $this->_action->betweenWhere('price_vnd', $conditions['price_vnd_from'], $conditions['price_vnd_to']);
      }
      
      if (!empty($conditions['sort'])) {
        $this->_action->orderBy($conditions['sort']);
      }
      
      if (isset($conditions['is_promotion'])) {
        $this->_action->andWhere("[$this->_table].is_promotion = :is_promotion:", ['is_promotion' => $conditions['is_promotion']]);
      }
      
      if (!empty($conditions['not_item_id_list']) && is_array($conditions['not_item_id_list'])) {
        $this->_action->notInWhere("[$this->_table].item_id", $conditions['not_item_id_list']);
      }
      
      if (!empty($conditions['query'])) {
        $value = trim($conditions['query']);
        
        $this->_action->andWhere(
          "([$this->_table].full_address_keyword LIKE :query:)"
          , ['query' => "%$value%"]);
      }
      
      if (!empty($conditions['category_id'])) {
        $this->_action->andWhere("[$this->_table].category_id = :category_id:", ['category_id' => $conditions['category_id']]);
      }
      
      if (!empty($conditions['city_id'])) {
        $this->_action->andWhere("[$this->_table].city_id = :city_id:", ['city_id' => $conditions['city_id']]);
      }
      
      if (!empty($conditions['price_from']) && !empty($conditions['price_to'])) {
        $this->_action->betweenWhere('price', $conditions['price_from'], $conditions['price_to']);
      }
      
      if (!empty($conditions['floor_area_from']) && !empty($conditions['floor_area_to'])) {
        $this->_action->betweenWhere('floor_area', $conditions['floor_area_from'], $conditions['floor_area_to']);
      }
      
      if (!empty($conditions['user_created_id'])) {
        $this->_action->andWhere("[$this->_table].user_created_id = :user_created_id:", ['user_created_id' => $conditions['user_created_id']]);
      }
      
      return $this;
    }
  }
