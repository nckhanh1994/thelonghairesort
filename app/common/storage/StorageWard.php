<?php
  
  namespace Storage;
  
  use Model\ModelAbstract;
  
  class StorageWard extends ModelAbstract
  {
    public function onConstruct()
    {
      $this->_table      = 'wards';
      $this->_model      = 'Storage\StorageWard';
      $this->_primaryKey = 'ward_id';
  
      $this->setSource($this->_table);
    }
    
    
    
    /**
     * return @array
     */
    public function getListLimit($conditions, $page, $limit, $order, $fields)
    {
      try {
        $strOrder = !empty($order) ? "$this->_table.$order" : $this->_primaryKey . ' DESC';
        
        $arrList = $this->createBuilder()
          ->columns($fields)
          ->buildWhere($conditions, $fields)
          ->orderBy($strOrder)
          ->limit($limit, $limit * ($page - 1))
          ->execute()
          ->toArray();
        
        if (!empty($conditions['key_by_field'])) {
          $arrList = $this->rebuildArrayByKey($conditions['key_by_field'], $arrList);
        }
        
        return $arrList;
      } catch (\Exception $ex) {
        if (APPLICATION_ENV == 'dev') {
          var_dump("\r\n" . $ex->getMessage() . "\r\n" . $ex->getFile() . ":" . $ex->getLine());
          die;
        }
        
        return false;
      }
    }
    
    /**
     * return @integer
     */
    public function getTotal($conditions)
    {
      try {
        $arrTotal = current($this->createBuilder()
          ->columns(['total' => 'COUNT(*)'])
          ->buildWhere($conditions)
          ->execute()
          ->toArray());
        
        return $arrTotal['total'];
      } catch (\Exception $ex) {
        if (APPLICATION_ENV == 'dev') {
          var_dump("\r\n" . $ex->getMessage() . "\r\n" . $ex->getFile() . ":" . $ex->getLine());
          die;
        }
        
        return false;
      }
    }
    
    /**
     * return @array
     */
    public function getDetail($conditions, $fields)
    {
      try {
        $arrDetail = current($this->createBuilder()
          ->columns($fields)
          ->buildWhere($conditions)
          ->execute()
          ->toArray()
        );
        
        return $arrDetail;
      } catch (\Exception $ex) {
        if (APPLICATION_ENV == 'dev') {
          
          var_dump("\r\n" . $ex->getMessage() . "\r\n" . $ex->getFile() . ":" . $ex->getLine());
          die;
        }
        
        return false;
      }
    }
    
    /**
     * @param type array $params
     * @param type array $conditions
     * return @integer
     */
    public function add($params, $conditions)
    {
      try {
        if (empty($params)) {
          return false;
        }
        
        $result = self::save(self::purifier($params, $conditions['config']));
        if ($result) {
          $intInsertId = $this->getWriteConnection()->lastInsertId();
          
          return $intInsertId;
        }
        
        
        return false;
      } catch (\Exception $ex) {
        if (APPLICATION_ENV == 'dev') {
          var_dump("\r\n" . $ex->getMessage() . "\r\n" . $ex->getFile() . ":" . $ex->getLine());
          die;
        }
        
        return false;
      }
    }
    
    /**
     * @param type array $params
     * @param type array $conditions
     * return @boolean
     */
    public function edit($params, $conditions)
    {
      try {
        if (empty($params) || empty($conditions)) {
          return false;
        }
        
        $builder = $this->createBuilder()
          ->buildWhere($conditions)
          ->execute();
        
        if (!empty($builder->count())) {
          $builder->update(self::purifier($params, $conditions['config']));
          if (!empty($builder->getMessages())) {
            return false;
          }
          
          return true;
        }
        
        return false;
      } catch (\Exception $ex) {
        if (APPLICATION_ENV == 'dev') {
          var_dump("\r\n" . $ex->getMessage() . "\r\n" . $ex->getFile() . ":" . $ex->getLine());
          die;
        }
        
        return false;
      }
    }
    
    /**
     * @build conditions
     */
    public function buildWhere($conditions, $fields = [])
    {
      if (empty($conditions)) {
        return $this;
      }
      /* @parent */
      parent::buildWhere($conditions, $fields);
      /* @this */
      if (!empty($conditions['ward_id'])) {
        $this->_action->andWhere("[$this->_table].ward_id = :ward_id:", ['ward_id' => $conditions['ward_id']]);
      }
      
      if (!empty($conditions['not_ward_id'])) {
        $this->_action->andWhere("[$this->_table].ward_id != :not_ward_id:", ['not_ward_id' => $conditions['not_ward_id']]);
      }
      
      if (isset($conditions['is_deleted'])) {
        $this->_action->andWhere("[$this->_table].is_deleted = :is_deleted:", ['is_deleted' => $conditions['is_deleted']]);
      }
      
      if (!empty($conditions['ward_id_list'])) {
        $this->_action->inWhere("[$this->_table].ward_id", $conditions['ward_id_list']);
      }
      
      if (!empty($conditions['district_id'])) {
        $this->_action->andWhere("[$this->_table].district_id = :district_id:", ['district_id' => $conditions['district_id']]);
      }
      
      return $this;
    }
  }
