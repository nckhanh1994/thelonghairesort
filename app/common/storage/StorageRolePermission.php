<?php
  
  namespace Storage;
  
  use Model\ModelAbstract;
  
  class StorageRolePermission extends ModelAbstract
  {
    public function onConstruct()
    {
      $this->_table      = 'role_permissions';
      $this->_model      = 'Storage\StorageRolePermission';
      $this->_primaryKey = 'role_permission_id';
  
      $this->setSource($this->_table);
    }
    
    
    
    /**
     * return @array
     */
    public function getListLimit($conditions, $page, $limit, $order, $fields)
    {
      try {
        $strOrder = !empty($order) ? "$this->_table.$order" : $this->_primaryKey . ' DESC';
        
        $arrList = $this->createBuilder()
          ->columns($fields)
          ->buildWhere($conditions, $fields)
          ->orderBy($strOrder)
          ->limit($limit, $limit * ($page - 1))
          ->execute()
          ->toArray();
        
        if (!empty($conditions['key_by_field'])) {
          $arrList = $this->rebuildArrayByKey($conditions['key_by_field'], $arrList);
        }
        
        return $arrList;
      } catch (\Exception $ex) {
        if (APPLICATION_ENV == 'dev') {
          var_dump("\r\n" . $ex->getMessage() . "\r\n" . $ex->getFile() . ":" . $ex->getLine());
          die;
        }
        
        return false;
      }
    }
    
    /**
     * return @integer
     */
    public function getTotal($conditions)
    {
      try {
        $arrTotal = current($this->createBuilder()
          ->columns(['total' => 'COUNT(*)'])
          ->buildWhere($conditions)
          ->execute()
          ->toArray());
        
        return $arrTotal['total'];
      } catch (\Exception $ex) {
        if (APPLICATION_ENV == 'dev') {
          var_dump("\r\n" . $ex->getMessage() . "\r\n" . $ex->getFile() . ":" . $ex->getLine());
          die;
        }
        
        return false;
      }
    }
    
    /**
     * return @array
     */
    public function getDetail($conditions, $fields)
    {
      try {
        $arrDetail = current($this->createBuilder()
          ->columns($fields)
          ->buildWhere($conditions)
          ->execute()
          ->toArray()
        );
        
        return $arrDetail;
      } catch (\Exception $ex) {
        if (APPLICATION_ENV == 'dev') {
          
          var_dump("\r\n" . $ex->getMessage() . "\r\n" . $ex->getFile() . ":" . $ex->getLine());
          die;
        }
        
        return false;
      }
    }
    
    /**
     * @param type array $params
     * @param type array $conditions
     * return @integer
     */
    public function add($params, $conditions)
    {
      try {
        if (empty($params)) {
          return false;
        }
        
        $result = self::save(self::purifier($params, $conditions['config']));
        if ($result) {
          $intInsertId = $this->getWriteConnection()->lastInsertId();
          
          return $intInsertId;
        }
        
        
        return false;
      } catch (\Exception $ex) {
        if (APPLICATION_ENV == 'dev') {
          var_dump("\r\n" . $ex->getMessage() . "\r\n" . $ex->getFile() . ":" . $ex->getLine());
          die;
        }
        
        return false;
      }
    }
    
    /**
     * @param type array $params
     * @param type array $conditions
     * return @boolean
     */
    public function edit($params, $conditions)
    {
      try {
        if (empty($params) || empty($conditions)) {
          return false;
        }
        
        $builder = $this->createBuilder()
          ->buildWhere($conditions)
          ->execute();
        
        if (!empty($builder->count())) {
          $builder->update(self::purifier($params, $conditions['config']));
          if (!empty($builder->getMessages())) {
            return false;
          }
          
          return true;
        }
        
        return false;
      } catch (\Exception $ex) {
        if (APPLICATION_ENV == 'dev') {
          var_dump("\r\n" . $ex->getMessage() . "\r\n" . $ex->getFile() . ":" . $ex->getLine());
          die;
        }
        
        return false;
      }
    }
    
    /**
     * @param type array $conditions
     * return @boolean
     */
    public function delete($conditions)
    {
      try {
        if (empty($conditions)) {
          return false;
        }
        
        $strWhere = 'WHERE 1=1';
        if (!empty($conditions['role_id'])) {
          $strWhere .= ' AND role_id = "' . $conditions['role_id'] . '"';
        }
        if (!empty($conditions['module'])) {
          $strWhere .= ' AND module = "' . $conditions['module'] . '"';
        }
        if (!empty($conditions['controller'])) {
          $strWhere .= ' AND controller = "' . $conditions['controller'] . '"';
        }
        if (!empty($conditions['action'])) {
          $strWhere .= ' AND action = "' . $conditions['action'] . '"';
        }
        
        $rawQuery = "DELETE FROM $this->_table $strWhere";
        
        $result = $this->getReadConnection()->query($rawQuery);
        
        return $result->numRows();
        
        return false;
      } catch (\Exception $ex) {
        if (APPLICATION_ENV == 'dev') {
          var_dump("\r\n" . $ex->getMessage() . "\r\n" . $ex->getFile() . ":" . $ex->getLine());
          die;
        }
        
        return false;
      }
    }
  
    /**
     * return @array
     */
    public function getResourceList($conditions, $page, $limit, $order, $fields)
    {
      try {
        $strOrder = !empty($order) ? "$this->_table.$order" : $this->_primaryKey . ' DESC';
      
        $arrList = $this->createBuilder()
          ->columns($fields)
          ->buildWhere($conditions, $fields)
          ->orderBy($strOrder)
          ->limit($limit, $limit * ($page - 1))
          ->execute()
          ->toArray();
  
        if (!empty($arrList)) {
          $arrTmp = [];
          foreach ($arrList as $value) {
            $arrTmp[] = $value['module'] . ':' . $value['controller'] . ':' . $value['action'];
          }
          $arrList = $arrTmp;
          unset($arrTmp);
        }
      
        return $arrList;
      } catch (\Exception $ex) {
        if (APPLICATION_ENV == 'dev') {
          var_dump("\r\n" . $ex->getMessage() . "\r\n" . $ex->getFile() . ":" . $ex->getLine());
          die;
        }
      
        return false;
      }
    }
    
    public function getAllResource($module)
    {
      $arrMappingActioInfo = self::getMappingActionInfo();
      //module frontend
      $files       = scandir(APP_PATH . '/module/' . $module . '/controller/');
      $arrList     = [];
      $controllers = [];
      unset($files[ array_search('.', $files) ]);
      unset($files[ array_search('..', $files) ]);
      
      foreach ($files as $file) {
        if ($controller = $this->_extractController($file)) {
          $controllerName = str_replace('Controller', '', $controller);
          $arrAction      = [];
          
          $arrActions = get_class_methods('\\Module\\' . ucfirst($module) . '\\Controller\\' . $controller);
          foreach ($arrActions as $action) {
            if (!in_array($action, $this->arrIgnoreAction)) {
              $action = str_replace('Action', '', $action);
              
              if (!isset($arrMappingActioInfo[ $action ]))
                continue;
              
              $arrAction[ $action ] = $arrMappingActioInfo[ $action ];
            }
          }
          
          $arrList[] = ['module' => '' . $module . '', 'controller' => $controllerName, 'action' => $arrAction];
        }
      }
      
      return $arrList;
    }
    
    private function _extractController($name)
    {
      $filename = explode('.php', $name);
      if (count(explode('Controller.php', $name)) > 1) {
        if (count($filename) > 1) {
          return $filename[0];
        }
      }
      
      return false;
    }
    
    public static function getMappingActionInfo()
    {
      return [
        'index'  => [
          'icon' => 'ti-view-grid',
          'text' => 'Xem'
        ],
        'add'    => [
          'icon' => 'ti-plus',
          'text' => 'Thêm'
        ],
        'edit'   => [
          'icon' => 'ti-pencil',
          'text' => 'Cập nhật'
        ],
        'delete' => [
          'icon' => 'ti-close',
          'text' => 'Xóa'
        ],
        'grant'  => [
          'icon' => 'ti-lock',
          'text' => 'grant'
        ],
        'login'  => [
          'icon' => 'ti-key',
          'text' => 'Đăng nhập'
        ],
        'logout' => [
          'icon' => 'ti-key',
          'text' => 'Đăng xuất'
        ],
      ];
    }
    
    /**
     * @build conditions
     */
    public function buildWhere($conditions, $fields = [])
    {
      if (empty($conditions)) {
        return $this;
      }
      /* @parent */
      parent::buildWhere($conditions, $fields);
      /* @this */
      if (!empty($conditions['role_permission_id'])) {
        $this->_action->andWhere("[$this->_table].role_permission_id = :role_permission_id:", ['role_permission_id' => $conditions['role_permission_id']]);
      }
      if (!empty($conditions['role_id'])) {
        $this->_action->andWhere("[$this->_table].role_id = :role_id:", ['role_id' => $conditions['role_id']]);
      }
      if (!empty($conditions['not_role_permission_id'])) {
        $this->_action->andWhere("[$this->_table].role_permission_id != :not_role_permission_id:", ['not_role_permission_id' => $conditions['not_role_permission_id']]);
      }
      if (!empty($conditions['module'])) {
        $this->_action->andWhere("[$this->_table].module = :module:", ['module' => $conditions['module']]);
      }
      if (!empty($conditions['controller'])) {
        $this->_action->andWhere("[$this->_table].controller = :controller:", ['controller' => $conditions['controller']]);
      }
      if (!empty($conditions['action'])) {
        $this->_action->andWhere("[$this->_table].action = :action:", ['action' => $conditions['action']]);
      }
      if (isset($conditions['is_deleted'])) {
        $this->_action->andWhere("[$this->_table].is_deleted = :is_deleted:", ['is_deleted' => $conditions['is_deleted']]);
      }
      
      return $this;
    }
  }
