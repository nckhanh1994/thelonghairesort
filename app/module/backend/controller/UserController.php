<?php
  
  namespace Module\Backend\Controller;
  
  use Base\Controller\MyController;
  use Util\Pagination;
  
  class UserController extends MyController
  {
    
    public function indexAction()
    {
      $this->seo->setTitle($this->translate->_('SIDE_NAV_USER_INDEX_TITLE'));
      $this->view->setVars([]);
    }
    
    public function addAction()
    {
      // #seo
      $this->seo->setTitle($this->translate->_('SIDE_NAV_USER_ADD_TITLE'));
      $this->view->setVars([]);
      
      $this->view->setVars([]);
    }
    
    public function editAction()
    {
      // #seo
      $this->seo->setTitle($this->translate->_('SIDE_NAV_USER_EDIT_TITLE'));
      
      $this->view->setVars([]);
    }
  }
