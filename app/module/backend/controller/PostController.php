<?php
  
  namespace Module\Backend\Controller;
  
  use Base\Controller\MyController;
  use Util\Pagination;
  
  class PostController extends MyController
  {
    /**
     * @index
     */
    public function indexAction()
    {
      $intPage  = ($this->dispatcher->getParam('page'));
      $intPage  = $intPage ? $intPage : 1;
      $intLimit = 20;
      
      $arrCondition = ['is_deleted' => 0];
      $instancePost = new \Models\Post();
      $arrPostList  = $instancePost->getListLimit($arrCondition, $intPage, $intLimit);
      
      $intTotal = $instancePost->getTotal($arrCondition);
      
      $paginationHTML = (new Pagination())
        ->setTotalRows($intTotal)
        ->setrowsPerPage($intLimit)
        ->setcurrentPage($intPage)
        ->setQueryString($strFilters)
        ->toHtmlV2($this->url, ["for" => "backend:post:index"]);
      
      // #seo
      $this->seo->setTitle("Danh sách bài viết");
      
      $this->view->setVars([
        'intPage'        => $intPage,
        'arrPostList'    => $arrPostList,
        'paginationHTML' => $paginationHTML
      ]);
    }
    
    /**
     * @param type array $params
* @param type array $conditions
* return @integer
     */
    public function addAction()
    {
      /** @prepare */
      $intPage = (int)$this->dispatcher->getParam('page');
      $intPage = $intPage ? $intPage : 1;
      
      if ($this->request->isPost()) {
        $params = $this->request->getPost();
        
        if ($this->isValid($params)) {
          $instancePost = new \Models\Post();
          $arrAdd       = [
            'post_title'       => $params['post_title'],
            'post_description' => $params['post_description'],
            'post_content'     => $params['post_content'],
            'post_slug'        => \Library\General::getSlug($params['post_title']),
            'post_image'       => json_encode(array_values(array_filter($params['post_image']))),
            'meta_title'       => !empty($params['meta_title']) ? $params['meta_title'] : $params['post_title'],
            'meta_description' => !empty($params['meta_description']) ? $params['meta_description'] : $params['post_description'],
            'meta_keyword'     => !empty($params['meta_keyword']) ? $params['meta_keyword'] : $params['post_title'],
            'is_home_page'     => isset($params['is_home_page']) ? 1 : 0,
            'status'           => isset($params['status']) ? 0 : 1,
            'created_at'       => time(),
            'updated_at'       => time(),
          ];
          
          $result = $instancePost->add($arrAdd, ['config' => ['HTML.Trusted' => 'true', 'CSS.Trusted' => 'true', 'Filter.YouTube' => 'true']]);
          if (empty($result)) {
            $this->flashSession->error($this->translate->_('Hệ thống đang xảy ra sự cố, vui lòng thử lại sau !'));
            
            return $this->response->redirect(['for' => 'backend:post:index', 'page' => $intPage]);
          }
          $this->flashSession->success($this->translate->_('Thêm Post thành công.'));
          
          return $this->response->redirect(['for' => 'backend:post:add', 'page' => $intPage]);
        }
      }
      
      // #seo
      $this->seo->setTitle("Tạo bài viết");
      
      $this->view->setVars([
        'intPage' => $intPage,
        'params'  => $params
      ]);
    }
    
    /**
     * @param type array $params
* @param type array $conditions
* return @boolean
     */
    public function editAction()
    {
      /** @prepare */
      $intID   = (int)$this->dispatcher->getParam('id', 'int');
      $intPage = (int)$this->dispatcher->getParam('page');
      $intPage = $intPage ? $intPage : 1;
      
      if (empty($intID)) {
        $this->flashSession->error($this->translate->_('Post này không tồn tại.'));
        
        return $this->response->redirect(['for' => 'backend:post:index', 'page' => $intPage]);
      }
      
      $instancePost  = new \Models\Post();
      $arrPostDetail = $instancePost->getDetail(['post_id' => $intID, 'is_deleted' => 0]);
      if (empty($arrPostDetail)) {
        $this->flashSession->error($this->translate->_('Post này không tồn tại.'));
        
        return $this->response->redirect(['for' => 'backend:post:index']);
      }
      
      if ($this->request->isPost()) {
        $params = $this->request->getPost();
        
        error_reporting(-1);
        ini_set('display_errors', 'On');
        
        if ($this->isValid($params)) {
          $arrEdit = [
            'post_title'       => $params['post_title'],
            'post_description' => $params['post_description'],
            'post_content'     => $params['post_content'],
            'post_slug'        => \Library\General::getSlug($params['post_title']),
            'post_image'       => json_encode(array_values(array_filter($params['post_image']))),
            'meta_title'       => !empty($params['meta_title']) ? $params['meta_title'] : $params['post_title'],
            'meta_description' => !empty($params['meta_description']) ? $params['meta_description'] : $params['post_description'],
            'meta_keyword'     => !empty($params['meta_keyword']) ? $params['meta_keyword'] : $params['post_title'],
            'is_home_page'     => isset($params['is_home_page']) ? 1 : 0,
            'status'           => isset($params['status']) ? 0 : 1,
            'updated_at'       => time()
          ];
          
          $result = $instancePost->edit($arrEdit, ['post_id' => $arrPostDetail['post_id'], 'config' => ['HTML.Trusted' => 'true', 'CSS.Trusted' => 'true', 'Filter.YouTube' => 'true']]);
          if (empty($result)) {
            $this->flashSession->error($this->translate->_('Hệ thống đang xảy ra sự cố, vui lòng thử lại sau !'));
            
            return $this->response->redirect(['for' => 'backend:post:index', 'page' => $intPage]);
          }
          $this->flashSession->success($this->translate->_('Cập nhật post thành công.'));
          
          return $this->response->redirect(['for' => 'backend:post:edit', 'page' => $intPage, 'id' => $intID]);
        }
      }
      
      // #seo
      $this->seo->setTitle("Cập nhật bài viết");
      
      $this->view->setVars([
        'intPage'        => $intPage,
        'params'         => $params,
        'arrPostDetail'  => $arrPostDetail,
        'arrProductList' => $arrProductList
      ]);
    }
    
    public function deleteAction()
    {
      /** @prepare */
      $intPostID = (int)$this->dispatcher->getParam('id', 'int');
      $intPage   = ($this->dispatcher->getParam('page'));
      $intPage   = $intPage ? $intPage : 1;
      
      if (empty($intPostID)) {
        $this->flashSession->error($this->translate->_('Bài viết này không tồn tại.'));
        
        return $this->response->redirect(['for' => 'backend:user:index', 'page' => $intPage]);
      }
      
      $instancePost = new \Models\Post();
      $result       = $instancePost->edit(['is_deleted' => 1], ['post_id' => $intPostID]);
      if (empty($result)) {
        $this->flashSession->error($this->translate->_('Hệ thống đang xảy ra sự cố, vui lòng thử lại sau !'));
        
        return $this->response->redirect(['for' => 'backend:post:index', 'page' => $intPage]);
      }
      
      $this->flashSession->success($this->translate->_('Xóa bài viết thành công.'));
      
      return $this->response->redirect(['for' => 'backend:post:index', 'page' => $intPage]);
    }
    
    /**
     * @param array $_arrParam
     */
    public function isValid($_arrParam = [])
    {
      if (empty($_arrParam['post_title'])) {
        $this->flashSession->message('error', $this->translate->_('Vui lòng post title.'));
        return false;
      }
      
      return true;
    }
  }
