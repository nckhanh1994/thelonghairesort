<?php
  
  namespace Module\Backend\Controller;
  
  use Base\Controller\MyController;
  use Util\Pagination;
  
  use
    Model\Street,
    Model\Item;
  
  class ItemController extends MyController
  {
    
    public function indexAction()
    {
      $conditions     = [
        'city_id'    => 29,
        'is_deleted' => 0
      ];
      $instnaceStreet = new Street();
      $arrStreetList  = $instnaceStreet->getListLimit($conditions, 1, 9999, null, ['street_id as id', 'CONCAT(street_keyword, " - ", street_name) as name', 'street_name', 'district_keyword']);
      
      $this->seo->setTitle($this->translate->_('SIDE_NAV_ITEM_INDEX_TITLE'));
      $this->view->setVars([
      ]);
    }
    
    public function addAction()
    {
      $CDATA = $this->view->getVar('CDATA');
      
      // #seo
      $this->seo->setTitle($this->translate->_('SIDE_NAV_ITEM_ADD_TITLE'));
      $this->view->setVars([]);
      
      $this->view->setVars([
        'CDATA' => $CDATA
      ]);
    }
    
    public function editAction()
    {
      $CDATA = $this->view->getVar('CDATA');
      
      // #seo
      $this->seo->setTitle($this->translate->_('SIDE_NAV_ITEM_EDIT_TITLE'));
      
      $this->view->setVars([
        'CDATA' => $CDATA
      ]);
    }
    
    public function viewAction()
    {
      // #seo
      $this->seo->setTitle($this->translate->_('SIDE_NAV_ITEM_EDIT_TITLE'));
      
      $this->view->setVars([
      
      ]);
    }
  }
