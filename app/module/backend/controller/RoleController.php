<?php
  
  namespace Module\Backend\Controller;
  
  use Base\Controller\MyController;
  
  use Model\RolePermission;
  
  use Util\Pagination;
  
  class RoleController extends MyController
  {
    public function initialize()
    {
      parent::initialize();
      
      $instanceRolePermission = new RolePermission();
      
      $arrResources = $instanceRolePermission->getAllResource();
      
      $PAGE_DATA = $this->view->getVar('PAGE_DATA');
      
      $this->view->setVars([
        'PAGE_DATA' => array_merge($PAGE_DATA, [
          'arrResources' => $arrResources
        ])
      ]);
    }
    
    public function indexAction()
    {
      $this->seo->setTitle("Danh sách người dùng");
    }
    
    public function addAction()
    {
      $this->seo->setTitle("Danh sách người dùng");
    }
    
    public function editAction()
    {
      $this->seo->setTitle("Danh sách người dùng");
    }
    
    public function deleteAction()
    {
      $this->seo->setTitle("Danh sách người dùng");
    }
    
    public function grantAction()
    {
      $this->seo->setTitle("Danh sách người dùng");
    }
  }
