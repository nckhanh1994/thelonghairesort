<?php
  
  namespace Module\Backend\Controller;
  
  use Base\Controller\MyController;
  use
    Util\FilterUtil,
    Util\General;
  
  use Model\User,
    Model\Role;
  
  class AuthController extends MyController
  {
    public function onConstruct()
    {
      $this->view->setLayout('auth');
    }
    
    public function indexAction()
    {
      $strURL = $this->url->get(['for' => 'backend-id', 'controller' => 'auth', 'action' => 'login']);
      
      return $this->response->redirect($strURL);
    }
    
    public function loginAction()
    {
      $this->isLogged();
      
      $params = [];
      if ($this->request->isPost()) {
        $params  = $this->request->getPost();
        $isError = 0;
        
        $strEmail = FilterUtil::string($params['email']);
        if (empty($strEmail)) {
          $isError = 1;
          $this->flashSession->error($this->translate->_('EMAIL_EMPTY'));
        }
        
        $strPassword = FilterUtil::string($params['password']);
        if (empty($strPassword)) {
          $isError = 1;
          $this->flashSession->error($this->translate->_('PASSWORD_EMPTY'));
        }
        
        $strEmail = filter_var($strEmail, FILTER_VALIDATE_EMAIL);
        if (empty($strEmail)) {
          $isError == 1;
          $this->flashSession->error($this->translate->_('EMAIL_NOT_VALID'));
        }
        
        /* Thông tin người dùng */
        $arrCondition = [
          'is_deleted' => 0,
          'email'      => $strEmail,
        ];
        
        $instanceUser  = new User();
        $arrUserDetail = $instanceUser->getDetail($arrCondition);
        
        if (empty($arrUserDetail)) {
          $isError = 1;
          $this->flashSession->error($this->translate->_('USER_NOT_FOUND'));
        }
        /*  */
        
        /* Check password */
        if (!$this->security->checkHash($strPassword, $arrUserDetail['password'])) {
          $isError = 1;
          $this->flashSession->error($this->translate->_('PASSWORD_NOT_CORRECT'));
        }
        
        /*  */
        
        $instanceRole  = new Role();
        $arrRoleDetail = $instanceRole->getDetail([
          'is_deleted' => 0,
          'role_id'    => $arrUserDetail['role_id']
        ]);
        
        if (empty($isError)) {
          $this->session->set($this->identityKey, [
            'user_id'   => $arrUserDetail['user_id'],
            'fullname'  => $arrUserDetail['fullname'],
            'email'     => $arrUserDetail['email'],
            'role_id'   => $arrUserDetail['role_id'],
            'max_price' => $arrRoleDetail['max_price']
          ]);
          
          return $this->response->redirect($this->url->get(['for' => 'backend', 'controller' => 'index', 'action' => 'index']));
        }
      }
      
      $this->view->setVars([
        'params' => $params
      ]);
    }
    
    private function isLogged()
    {
      $isLogged = $this->session->get($this->identityKey);
      if ($isLogged) {
        $strURL = $this->url->get('/backend');
        
        return $this->response->redirect($strURL);
      }
    }
    
    public function logoutAction()
    {
      // Remove a session variable
      $this->session->remove($this->identityKey);
      
      return $this->response->redirect($this->url->get(['for' => 'backend:auth:login']));
    }
    
    public function accessDeniedAction()
    {
      $this->view->setLayout('/error/access-deny');
      $this->view->pick('error/access-deny');
    }
    
  }
