<?php
  
  namespace Module\Backend\Controller;
  
  use Base\Controller\MyController;
  
  class IndexController extends MyController
  {
    public function indexAction()
    {
      $this->response->redirect($this->url->get(['for' => 'backend:item:index', 'page' => 1]));
      
      $this->view->setVars([]);
    }
  }
