<?php
  
  namespace Module\Backend\Controller;
  
  use Base\Controller\MyController;
  
  class GeneralController extends MyController
  {
    /**
     * @index
     */
    public function indexAction()
    {
      
    }
    
    // ! Lấy ra danh sách sản phẩm
    public function xhrGetProductListAction()
    {
      $params = $this->request->getQuery();
      if (empty($params['term'])) {
        return $this->responseJSON(['code' => 01, 'msg' => 'Term not found...', 'data' => []]);
      }
      
      $arrCondition = ['is_deleted' => 0, 'status' => 1, 'prod_id_or_prod_name_like' => trim($params['term'])];
      // Lấy danh sách template theo term
      $instanceProduct = new \Models\Product();
      $arrProductList  = $instanceProduct->getListLimit($arrCondition, 1, 20, 'prod_id DESC', ['prod_id', 'prod_name', 'prod_image']);
      $arrTmp          = [];
      foreach ($arrProductList as $product)
        $arrTmp[] = ['id' => $product['prod_id'], 'text' => '<div style="display: inline-block;margin: 5px 5px;"><img src="' . current(json_decode($product['prod_image'], true)) . '" alt="" width="32" height="32"><span style="margin: 5px;">' . $product['prod_name'] . '</span></div>'];
      $arrProductList = $arrTmp;
      unset($arrTmp);
      
      return $this->responseJSON(['code' => 200, 'msg' => 'Successfully !', 'data' => $arrProductList]);
    }
    
    // ! Upload hình ảnh
    public function xhrUploadImagesAction()
    {
      $params = $this->dispatcher->getParams();
      if ($this->request->hasFiles() && $this->request->isPost()) {
        $arrFiles = $this->request->getUploadedFiles();
        $arrImage = \Library\General::uploadImage($arrFiles, $params[0]);
        
        if (empty($arrImage))
          return $this->responseJSON(['code' => '01', 'msg' => 'Image not found...']);
        
        return $this->responseJSON($arrImage);
      };
      
      return $this->responseJSON(['code' => 500, 'msg' => 'Internal Server Error...']);
    }
  }
