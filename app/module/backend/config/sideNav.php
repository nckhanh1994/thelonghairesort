<?php
  return [
    'admin' => [
      'info'     => [
        'title' => 'Hệ thống',
        'icon'  => 'mdi mdi-settings'
      ],
      'children' => [
        'backend:user:index' => [
          'info'     => [
            'title' => $this->translate->_('SIDE_NAV_USER_INDEX_TITLE'),
            'icon'  => '	la la-user'
          ],
          'children' => [
            'backend:user:index' => [
              'info' => [
                'title' => $this->translate->_('SIDE_NAV_INDEX_TITLE'),
                'icon'  => '	la la-user'
              ]
            ],
            'backend:user:add'   => [
              'info' => [
                'title' => $this->translate->_('SIDE_NAV_USER_ADD_TITLE'),
                'icon'  => '	la la-user'
              ]
            ],
          ]
        ],
        'backend:role:index' => [
          'info'     => [
            'title' => $this->translate->_('SIDE_NAV_ROLE_INDEX_TITLE'),
            'icon'  => '	la la-group',
          ],
          'children' => [
            'backend:role:index' => [
              'info' => [
                'title' => $this->translate->_('SIDE_NAV_INDEX_TITLE'),
                'icon'  => '	la la-group',
              ]
            ],
            'backend:role:add'   => [
              'info' => [
                'title' => $this->translate->_('SIDE_NAV_ROLE_ADD_TITLE'),
                'icon'  => '	la la-group',
              ]
            ],
          ]
        ],
        'backend:item:index' => [
          'info'     => [
            'title' => $this->translate->_('SIDE_NAV_ITEM_INDEX_TITLE'),
            'icon'  => 'la la-home',
          ],
          'children' => [
            'backend:item:index' => [
              'info' => [
                'title' => $this->translate->_('SIDE_NAV_INDEX_TITLE'),
                'icon'  => 'la la-home',
              ]
            ],
            'backend:item:add'   => [
              'info' => [
                'title' => $this->translate->_('SIDE_NAV_ITEM_ADD_TITLE'),
                'icon'  => 'la la-home',
              ]
            ],
          ]
        ],
      ]
    ]
  ];
