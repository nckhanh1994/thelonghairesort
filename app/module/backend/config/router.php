<?php
  
  use Phalcon\Mvc\Router\Group as RouterGroup;
  
  // Create a group with a common module and controller
  $backend = new RouterGroup([
    'namespace' => 'Module\Backend\Controller',
    'module'    => 'backend',
  ]);
  
  $backend->setPrefix('/backend');
  
  
  // #general
  $backend->add('/:params', ['controller' => 'index', 'action' => 'index'])->setName('backend:home');
  $backend->add('/:controller/:action/:params', ['controller' => 1, 'action' => 2, 'params' => 3])->setName('backend');
  // #login
  $backend->add('/auth/login', ['controller' => 'auth', 'action' => 'login'])->setName('backend:auth:login');
  $backend->add('/auth/logout', ['controller' => 'auth', 'action' => 'logout'])->setName('backend:auth:logout');
  // #user
  $backend->add('/user/index/{page:[0-9]+}', ['controller' => 'user', 'action' => 'index'])->setName('backend:user:index');
  $backend->add('/user/add/{page:[0-9]+}', ['controller' => 'user', 'action' => 'add'])->setName('backend:user:add');
  $backend->add('/user/edit/{id:[0-9]+}/{page:[0-9]+}', ['controller' => 'user', 'action' => 'edit'])->setName('backend:user:edit');
  $backend->add('/user/delete/{id:[0-9]+}/{page:[0-9]+}', ['controller' => 'user', 'action' => 'delete'])->setName('backend:user:delete');
  // #role
  $backend->add('/role/index/{page:[0-9]+}', ['controller' => 'role', 'action' => 'index'])->setName('backend:role:index');
  $backend->add('/role/add/{page:[0-9]+}', ['controller' => 'role', 'action' => 'add'])->setName('backend:role:add');
  $backend->add('/role/edit/{id:[0-9]+}/{page:[0-9]+}', ['controller' => 'role', 'action' => 'edit'])->setName('backend:role:edit');
  $backend->add('/role/delete/{id:[0-9]+}/{page:[0-9]+}', ['controller' => 'role', 'action' => 'delete'])->setName('backend:role:delete');
  $backend->add('/role/grant/{id:[0-9]+}/{page:[0-9]+}', ['controller' => 'role', 'action' => 'grant'])->setName('backend:role:grant');
  // #item
  $backend->add('/item/index/{page:[0-9]+}', ['controller' => 'item', 'action' => 'index'])->setName('backend:item:index');
  $backend->add('/item/add/{page:[0-9]+}', ['controller' => 'item', 'action' => 'add'])->setName('backend:item:add');
  $backend->add('/item/view/{id:[0-9]+}/{page:[0-9]+}', ['controller' => 'item', 'action' => 'view'])->setName('backend:item:view');
  $backend->add('/item/edit/{id:[0-9]+}/{page:[0-9]+}', ['controller' => 'item', 'action' => 'edit'])->setName('backend:item:edit');
  $backend->add('/item/delete/{id:[0-9]+}/{page:[0-9]+}', ['controller' => 'item', 'action' => 'delete'])->setName('backend:item:delete');
  
  return $backend;
