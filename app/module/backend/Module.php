<?php
  
  namespace Module\Backend;
  
  use Phalcon\Loader;
  use Phalcon\Di\DiInterface;
  use Phalcon\Mvc\Dispatcher;
  use Phalcon\Mvc\ModuleDefinitionInterface;
  use Phalcon\Mvc\View;
  
  class Module implements ModuleDefinitionInterface
  {
    public function registerAutoloaders(DiInterface $container = null)
    {
      $loader = new Loader();
      $loader->registerNamespaces([
        'Module\Backend\Controller'      => MODULE_PATH . '/backend/controller',
        'Module\Backend\Controller\Ajax' => MODULE_PATH . '/backend/controller/ajax',
      ]);
      $loader->register();
    }
    
    /**
     * Register the services here to make them general or register in the ModuleDefinition to make them module-specific
     */
    public function registerServices(DiInterface $di)
    {
      //Registering a dispatcher
      $di->set('dispatcher', function () {
        $dispatcher = new Dispatcher();
        $dispatcher->setDefaultNamespace("Module\Backend\Controller\\");
        
        return $dispatcher;
      });
      
      //Registering the view component
      $di['view'] = function () {
        $view = new View();
        
        $viewPath = MODULE_PATH . '/backend/view';
        $view->setViewsDir($viewPath);
        $view->setLayoutsDir('_layout/');
        $view->setPartialsDir('_partial/');
        $view->setLayout('main');
        
        return $view;
      };
      
    }
    
  }
