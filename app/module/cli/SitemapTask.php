<?php
  
  use Library\General;
  
  class SitemapTask extends \Phalcon\Cli\Task
  {
    /**
     * @construct
     */
    public final function onConstruct()
    {
      
    }
    
    /**
     * @param $_params
     * @return boolean;
     */
    public function runAction()
    {
      $dom          = new \DOMDocument("1.0", "UTF-8");
      $sitemapindex = $dom->createElement('sitemapindex');
      $sitemapindex->setAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
      $sitemapindex->setAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
      
      
      $file   = PUBLIC_PATH . '/sitemap.xml';
      $handle = fopen($file, 'W+');
      file_put_contents($file, $dom->saveXML());
      fclose($handle);
      
      $code = $this->ping('https://www.google.com/webmasters/sitemaps/ping?sitemap=https://thenewsfeed.today/sitemap.xml');
      $this->fecho(General::getColoredString("Ping sitemap to Google successfully - code=$code", 'white', 'blue'));
      
      $code = $this->ping('http://www.bing.com/webmaster/ping.aspx?siteMap=https://thenewsfeed.today/sitemap.xml');
      $this->fecho(General::getColoredString("Ping sitemap to Bing successfully - code=$code", 'white', 'blue'));
    }
    
    public function postAction(array $params)
    {
      $instancePost = new \Models\Post();
      $total        = $instancePost->getTotal(['is_deleted' => 0]);
      
      if (empty($total)) {
        exit;
      }
      
      $isBackground = (int)$params['background'];
      if (empty($isBackground)) {
        $command = 'nohup php ' . PUBLIC_PATH . '/cli.php sitemap post --background=1' . ' >> ' . $this->logPath . ' 2>&1 & echo $!';
        return shell_exec($command);
      }
      
      $path = PUBLIC_PATH . '/sitemaps/post';
      if (!is_readable($path) || !is_writable($path)) {
        exit;
      }
      
      $intLimit     = 10000;
      $arrCondition = [
        'is_deleted' => 0,
        'status'     => 1
      ];
      
      $arrPostList = $instancePost->getList($arrCondition, 1, $intLimit, 'post_id DESC', ['post_id', 'post_title', 'post_slug', 'post_description', 'post_image', 'created_at', 'updated_at']);
      if (empty($arrPostList)) {
        $this->fecho(General::getColoredString("empty post or post list less than limit.", 'red'));
        exit;
      }
      $sitemap = new \DOMDocument("1.0", "UTF-8");
      $urlset  = $sitemap->createElement('urlset');
      $urlset->setAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
      $urlset->setAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
      
      foreach ($arrPostList as $post) {
        $intID = (int)$post['post_id'];
        
        $href = BASE_URL . '/bai-viet/' . $post['post_slug'] . '-' . $intID . '.html';
        
        $url = $sitemap->createElement('url');
        $url->appendChild($sitemap->createElement('loc', $href));
        $url->appendChild($sitemap->createElement('changefreq', 'daily'));
        $url->appendChild($sitemap->createElement('lastmod', date('c', $post['created_at'])));
        $url->appendChild($sitemap->createElement('priority', 0.8));
        $urlset->appendChild($url);
        
        $strQuery .= "post_id=$intID OR ";
      }
      
      $sitemap->appendChild($urlset);
      
      $file   = "$path/posts.xml";
      $handle = fopen($file, 'W+');
      file_put_contents($file, $sitemap->saveXML());
      fclose($handle);

//      $command = 'nohup php ' . PUBLIC_PATH . '/cli.php sitemap post --background=1' . ' >> ' . $this->logPath . ' 2>&1 & echo $!';
      $this->fecho(General::getColoredString("Generated $file", 'green'));
      return shell_exec($command);
    }
    
    /**
     * @param string $_message
     * @void
     */
    private function fecho($_message = '')
    {
      echo $_message;
      flush();
    }
  }
