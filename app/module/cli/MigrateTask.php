<?php
  
  use library\General;
  use library\jobs\JobExportExcel;
  
  class MigrateTask extends \Phalcon\Cli\Task
  {
    /**
     * @construct
     */
    public final function onConstruct()
    {
      
    }
    
    /**
     * @param $_params
     * @return boolean;
     */
    public function migrateProductFromFileAction($_params = [])
    {
      if (empty($_params['name'])) {
        \Library\General::echoDanger('File name not found...');
      }
      
      $fileName = trim($_params['name']);
      
      $fh = fopen('./feeds/' . $fileName, 'r');
      while ($line = fgets($fh)) {
        $arrData = explode('|||', $line);
        iF (empty($arrData[0]) || empty($arrData[1]))
          continue;
        
        $arrAdd = [
          'sku'        => trim($arrData[0]),
          'title'      => trim($arrData[1]),
          'slug'       => \Library\General::getSlug(trim($arrData[1])),
          'status'     => 1,
          'created_at' => time()
        ];
        
        $instanceProduct = new \Models\Product();
        $result          = $instanceProduct->add($arrAdd);
        if (!empty($result))
          \Library\General::echoSuccess($result . ' created successfully !');
        else
          \Library\General::echoDanger($result . ' not created !');
      }
      fclose($fh);
      \Library\General::echoInfo('== DONE ==');
    }
    
    /**
     * @param $_params
     * @return boolean;
     */
    public function migrateTagsFromProductAction()
    {
      $instanceProduct = new \Models\Product();
      $arrProductList  = $instanceProduct->getListLimit([], 1, 9999);
      
      $instanceTag = new \Models\Tag();
      $arrMultiAdd = [];
      foreach ($arrProductList as $product) {
        if (empty($product['title']))
          continue;
        
        $arrTag = $this->createTags($product['title']);
        foreach ($arrTag as $tag) {
          $arrMultiAdd[] = ['tag' => trim(strip_tags($tag)), 'slug' => \Library\General::getSlug($tag), 'created_at' => time()];
        }
      }
      
      $instanceTag->addMultiOnDuplicate($arrMultiAdd);
      
      \Library\General::echoInfo('== DONE ==');
    }
    
    /**
     * @param string $_message
     * @void
     */
    private function fecho($_message = '')
    {
      echo $_message = '';
      flush();
    }
    
    function createTags($string, $minkeys = 2, $maxkeys = 4)
    {
      $string = str_replace('-', ' ', $string);
      $string = str_replace(',', ' ', $string);
      $string = str_replace('  ', ' ', $string);
      
      $words = explode(' ', $string);
      //$minkeys = 2;
      //$maxkeys = 4;
      $listkeywords = [];
      for ($i = 0; $i < count($words); $i++) {
        $list = [];
        for ($j = $i; $j < count($words); $j++) {
          //echo $words[$i]." ".$words[$j].",";
          array_push($list, $words[ $j ]);
          if (count($list) >= $minkeys and count($list) < $maxkeys) {
            array_push($listkeywords, implode(" ", $list));
          }
        }
      }
      return $listkeywords;
    }
  }
