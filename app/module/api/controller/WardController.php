<?php
  
  namespace Module\Api\Controller;
  
  use Model\Ward;
  
  use Base\Controller\BaseController;
  
  use Util\FilterUtil;
  
  class WardController extends BaseController
  {
    public function read()
    {
      $params = $this->request->getPost();
      
      $intDistrictID = FilterUtil::int($params['districtID']);
      if (empty($intDistrictID)) {
        return $this->createResponseErrorParam();
      }
      
      $intPage  = 1;
      $intLimit = 9999;
      
      $conditions = [
        'district_id' => $intDistrictID,
        'is_deleted'  => 0
      ];
      
      $instanceWard = new Ward();
      $arrWardList  = $instanceWard->getListLimit($conditions, $intPage, $intLimit, 'ward_id ASC');
      
      return $this->createResponseSuccess(null, ['arrWardList' => $arrWardList]);
    }
  }
