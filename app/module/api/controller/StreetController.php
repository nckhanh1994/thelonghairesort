<?php
  
  namespace Module\Api\Controller;
  
  use Model\Street;
  
  use Base\Controller\BaseController;
  
  use Util\FilterUtil;
  
  class StreetController extends BaseController
  {
    public function read()
    {
      $params = $this->request->getPost();
      
      $intDistrictID = FilterUtil::int($params['districtID']);
      if (empty($intDistrictID)) {
        return $this->createResponseErrorParam();
      }
      
      $intPage  = 1;
      $intLimit = 9999;
      
      $conditions = [
        'district_id' => $intDistrictID,
        'is_deleted'  => 0
      ];
      
      $instanceStreet = new Street();
      $arrStreetList  = $instanceStreet->getListLimit($conditions, $intPage, $intLimit, 'street_id ASC');
      
      return $this->createResponseSuccess(null, ['arrStreetList' => $arrStreetList]);
    }
  }
