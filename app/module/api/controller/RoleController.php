<?php
  
  namespace Module\Api\Controller;
  
  use
    Model\Role,
    Model\RolePermission;
  
  use Base\Controller\BaseController;
  
  use Util\FilterUtil;
  
  class RoleController extends BaseController
  {
    public function get()
    {
      if ($this->request->isPost()) {
        $params = $this->request->getPost();
        
        $intID = FilterUtil::int($params['id']);
        if (empty($intID))
          return $this->createResponseErrorParam();
        
        $conditions = [
          'role_id'    => $intID,
          'is_deleted' => 0
        ];
        
        $instanceRole  = new Role();
        $arrRoleDetail = $instanceRole->getDetail($conditions);
        
        return $this->createResponseSuccess(null, ['arrRoleDetail' => $arrRoleDetail]);
      }
      
      return $this->createResponseErrorParam('Method is not supported !');;
    }
    
    public function read($page = 1, $limit = 15)
    {
      $intPage  = FilterUtil::int($page, 1);
      $intLimit = FilterUtil::int($limit, 15);
      
      $conditions = [
        'is_deleted' => 0
      ];
      
      $instanceRole = new Role();
      $arrRoleList  = $instanceRole->getListLimit($conditions, $intPage, $intLimit);
      
      return $this->createResponseSuccess(null, ['roleList' => $arrRoleList]);
    }
    
    public function create()
    {
      if ($this->request->isPost()) {
        $params = $this->request->getPost();
        /* #Validate */
        $arrParam = $this->_validate($params);
        if (empty($arrParam))
          return $this->createResponseErrorParam();
        /* */
        
        $payload = [
          'role_name'     => $arrParam['role_name'],
          'max_price'     => FilterUtil::float($arrParam['max_price']),
          'is_fullaccess' => FilterUtil::int($arrParam['is_fullaccess']),
          'created_at'    => time()
        ];
        
        $instanceRole = new Role();
        $result       = $instanceRole->add($payload);
        if (empty($result)) {
          return $this->createResponseErrorAPI('Hệ thống đang gặp sự cố, Vui lòng thử lại !');
        }
        
        $conditions    = [
          'role_id'    => $result,
          'is_deleted' => 0
        ];
        $arrRoleDetail = $instanceRole->getDetail($conditions);
        
        return $this->createResponseSuccess(null, ['arrRoleDetail' => $arrRoleDetail]);
      }
      
      return $this->createResponseErrorParam('Method is not supported !');
    }
    
    public function update()
    {
      if ($this->request->isPost()) {
        $params = $this->request->getPost();
        $intID  = FilterUtil::int($params['id']);
        if (empty($intID))
          return $this->createResponseErrorParam();
        
        $conditions = [
          'role_id'    => $intID,
          'is_deleted' => 0,
        ];
        
        $instanceRole  = new Role();
        $arrRoleDetail = $instanceRole->getDetail($conditions);
        if (empty($arrRoleDetail)) {
          return $this->createResponseErrorAPI();
        }
        
        /* #Validate */
        $arrParam = $this->_validate($params);
        if (empty($arrParam))
          return $this->createResponseErrorParam();
        /* */
        
        
        $payload = [
          'role_name'     => $arrParam['role_name'],
          'max_price'     => FilterUtil::float($arrParam['max_price']),
          'is_fullaccess' => FilterUtil::int($arrParam['is_fullaccess']),
          'created_at'    => time(),
          'updated_at'    => time()
        ];
        
        $conditions = [
          'role_id'    => $intID,
          'is_deleted' => 0,
        ];
        $result     = $instanceRole->edit($payload, $conditions);
        if (empty($result)) {
          return $this->createResponseErrorAPI('Hệ thống đang gặp sự cố, Vui lòng thử lại !');
        }
        
        $arrRoleDetail = $instanceRole->getDetail($conditions);
        
        return $this->createResponseSuccess(null, ['arrRoleDetail' => $arrRoleDetail]);
      }
      
      return $this->createResponseErrorParam('Method is not supported !');
    }
    
    public function delete()
    {
      if ($this->request->isPost()) {
        $params = $this->request->getPost();
        $intID  = FilterUtil::int($params['id']);
        if (empty($intID))
          return $this->createResponseErrorParam();
        
        $conditions = [
          'role_id'    => $intID,
          'is_deleted' => 0,
        ];
        
        $instanceRole  = new Role();
        $arrRoleDetail = $instanceRole->getDetail($conditions);
        if (empty($arrRoleDetail)) {
          return $this->createResponseErrorAPI('Người dùng không tồn tại !');
        }
        
        $payload = [
          'is_deleted' => 1,
          'updated_at' => time(),
        ];
        
        $conditions = [
          'role_id' => $intID
        ];
        
        $result = $instanceRole->edit($payload, $conditions);
        if (empty($result)) {
          return $this->createResponseErrorAPI('Hệ thống đang gặp sự cố, Vui lòng thử lại !');
        }
        
        return $this->createResponseSuccess(null, ['id' => $intID]);
      }
      
      return $this->createResponseErrorParam('Method is not supported !');
    }
    
    public function createRolePermission()
    {
      if ($this->request->isPost()) {
        $params = $this->request->getPost();
        
        $intID = $params['id'];
        if (empty($intID))
          return $this->createResponseErrorParam();
        
        $strResource = $params['resource'];
        if (empty($strResource))
          return $this->createResponseErrorParam();
        
        $arrResource = is_array($params['resource']) ? $params['resource'] : [$params['resource']];
        foreach ($arrResource as $resource) {
          // analys resource
          list($module, $controller, $action) = explode(':', $resource);
          
          $payload = [
            'role_id'    => $intID,
            'module'     => $module,
            'controller' => $controller,
            'action'     => $action,
          ];
          
          $instanceRolePermission = new RolePermission();
          $result                 = $instanceRolePermission->add($payload);
        }
        
        $arrRolePermissionDetail = current($instanceRolePermission->getList(['role_id' => $intID]));
        
        return $this->createResponseSuccess(null, ['rolePermission' => $arrRolePermissionDetail]);
      }
      
      return $this->createResponseErrorParam('Method is not supported !');
    }
    
    public function deleteRolePermission()
    {
      if ($this->request->isPost()) {
        $params = $this->request->getPost();
        
        $intID = $params['id'];
        if (empty($intID))
          return $this->createResponseErrorParam();
        
        $strResource = $params['resource'];
        if (empty($strResource))
          return $this->createResponseErrorParam();
        
        $arrResource = is_array($params['resource']) ? $params['resource'] : [$params['resource']];
        
        foreach ($arrResource as $resource) {
          // analys resource
          list($module, $controller, $action) = explode(':', $resource);
          
          $conditions = [
            'role_id'    => $intID,
            'module'     => $module,
            'controller' => $controller,
            'action'     => $action,
          ];
          
          $instanceRolePermission = new RolePermission();
          $instanceRolePermission->delete($conditions);
        }
        
        return $this->createResponseSuccess();
      }
    }
    
    public function getRolePermission()
    {
      if ($this->request->isPost()) {
        $params = $this->request->getPost();
        
        $intID = $params['id'];
        if (empty($intID))
          return $this->createResponseErrorParam();
        
        $conditions = [
          'role_id' => $intID
        ];
        
        $instanceRolePermission = new RolePermission();
        $arrRolePermissionList  = $instanceRolePermission->getListLimit($conditions, 1, 9999);
        
        $arrTemp = [];
        foreach ($arrRolePermissionList as $arrRolePermission) {
          $arrTemp[ $arrRolePermission['controller'] ][ $arrRolePermission['action'] ] = $arrRolePermission;
        }
        $arrRolePermissionList = $arrTemp;
        unset($arrTemp);
        
        return $this->createResponseSuccess(null, ['rolePermissionList' => $arrRolePermissionList]);
      }
      
      return $this->createResponseErrorParam('Method is not supported !');
    }
    
    private function _validate($params)
    {
      $roleName = FilterUtil::string($params['role_name']);
      if (empty($roleName))
        return false;
      
      return array_merge($params, [
        'role_name' => $roleName,
      ]);
    }
  }
