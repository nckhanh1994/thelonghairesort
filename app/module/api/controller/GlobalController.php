<?php
  
  namespace Module\Api\Controller;
  
  use Base\Controller\BaseController;
  
  use Util\GeneralUtil;
  use Util\FilterUtil;
  
  class GlobalController extends BaseController
  {
    public function upload()
    {
      if ($this->request->isPost()) {
        if ($this->request->hasFiles()) {
          $params        = $this->request->getPost();
          $strController = FilterUtil::string($params['controller'], 'global');
          
          $arrFiles = $this->request->getUploadedFiles();
          $arrImage = GeneralUtil::uploadImage($arrFiles, $strController);
          
          return $this->createResponseSuccess(null, ['image_info' => $arrImage]);
        };
      }
      
      return $this->createResponseErrorParam('Method is not supported !');
    }
  }
