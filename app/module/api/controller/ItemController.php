<?php
  
  namespace Module\Api\Controller;
  
  use Base\Controller\BaseController;
  
  use
    Model\Item,
    Model\ItemLog;
  
  use Util\GeneralUtil;
  use Util\FilterUtil;
  
  class ItemController extends BaseController
  {
    public function get()
    {
      if ($this->request->isPost()) {
        $params = $this->request->getPost();
        
        $intID = FilterUtil::int($params['id']);
        if (empty($intID))
          return $this->createResponseErrorParam();
        
        $conditions = [
          'item_id'    => $intID,
          'is_deleted' => 0
        ];
        
        $instanceItem  = new Item();
        $arrItemDetail = $instanceItem->getDetail($conditions);
        
        return $this->createResponseSuccess(null, ['arrItemDetail' => $arrItemDetail]);
      }
      
      return $this->createResponseErrorParam('Method is not supported !');;
    }
    
    public function read()
    {
      $params = $this->request->getPost();
      
      $intPage  = isset($params['page']) ? FilterUtil::int($params['page'], 1) : 1;
      $intLimit = 20;
      
      $conditions = [
        'is_deleted' => 0,
        'is_cart'    => 1,
      ];
      $intRoleID  = FilterUtil::int($params['roleID'], 0);
      if ($intRoleID != 2) {
        $userCreatedID = FilterUtil::int($params['userCreatedID']);
        $maxPrice      = FilterUtil::float($params['maxPrice']);
        
        $conditions = $conditions + [
            'is_private'         => 0,
            'or_user_created_id' => $userCreatedID,
            'price_less_than'    => $maxPrice,
          ];
      }
      
      $arrQuery = [];
      parse_str($params['queryString'], $arrQuery);
      $strFilter = $this->_filter($arrQuery, $conditions);
      if (!empty(trim($strFilter, '?'))) {
        unset($conditions['or_user_created_id']);
      }
      
      $instanceItem = new Item();
      $arrItemList  = $instanceItem->getListLimit($conditions, $intPage, $intLimit, 'created_at DESC');
      
      $totalRecord = $instanceItem->getTotal($conditions);
      
      return $this->createResponseSuccess(null, [
        'page'        => $intPage,
        'totalRecord' => $totalRecord,
        'itemList'    => $arrItemList
      ]);
    }
    
    public function create()
    {
      if ($this->request->isPost()) {
        $params = $this->request->getPost();
        
        /* #validate */
        $arrParams = $this->_validate($params);
        if (empty($arrParams))
          return $this->createResponseErrorParam();
        /* */
        
        $price    = FilterUtil::float($arrParams['price']);
        $currency = FilterUtil::string($arrParams['currency']);
        $priceVND = GeneralUtil::convertPrice2VND($price, $currency);
        
        /* #is_promotion */
        $isPromotion    = FilterUtil::int($arrParams['is_promotion']);
        $promotionPrice = FilterUtil::float($arrParams['promotion_price']);
        if ($isPromotion) {
          $promotionPriceVND = GeneralUtil::convertPrice2VND($promotionPrice, $currency);
        }
        /* ## */
        $arrImageList = array_filter(array_values($arrParams['item_image_list']));
        
        $payload = [
          'item_feature_image'  => current($arrImageList),
          'item_image_list'     => json_encode($arrImageList),
          'item_description'    => FilterUtil::string($arrParams['item_description']),
          'item_content'        => FilterUtil::string($arrParams['item_content']),
          'floor_area'          => FilterUtil::int($arrParams['floor_area']),
          'floor_area_unit'     => 'm2',
          'host_phone'          => FilterUtil::string($arrParams['host_phone']),
          'price'               => $price,
          'price_vnd'           => $priceVND,
          'currency'            => $currency,
          'promotion_price'     => $promotionPrice,
          'promotion_price_vnd' => $promotionPriceVND,
          'commission'          => FilterUtil::float($arrParams['commission']),
          'category_id'         => FilterUtil::int($arrParams['category_id']),
          'city_id'             => FilterUtil::int($arrParams['city_id']),
          'district_id'         => FilterUtil::int($arrParams['district_id']),
          'ward_id'             => FilterUtil::int($arrParams['ward_id']),
          'street_id'           => FilterUtil::int($arrParams['street_id']),
          'city_name'           => FilterUtil::string($arrParams['city_name']),
          'district_name'       => FilterUtil::string($arrParams['district_name']),
          'ward_name'           => FilterUtil::string($arrParams['ward_name']),
          'street_name'         => FilterUtil::string($arrParams['street_name']),
          'address'             => FilterUtil::string($arrParams['address']),
          'is_cart'             => FilterUtil::int($arrParams['is_cart']),
          'status'              => FilterUtil::int($arrParams['status']),
          'created_at'          => time(),
          'is_deleted'          => FilterUtil::int($arrParams['is_deleted']),
          'meta_title'          => FilterUtil::string($arrParams['meta_title']),
          'meta_description'    => FilterUtil::string($arrParams['meta_description']),
          'meta_keyword'        => FilterUtil::string($arrParams['meta_keyword']),
          'user_created_id'     => FilterUtil::int($arrParams['user_created_id']),
          'is_favorite'         => FilterUtil::int($arrParams['is_favorite']),
          'is_private'          => FilterUtil::int($arrParams['is_private']),
          'is_promotion'        => $isPromotion,
        ];
        
        $itemName = $payload['address'] . ' - ' . $payload['street_name'] . ' - ' . $payload['ward_name'] . ' - ' . $payload['district_name'] . ' - ' . $payload['city_name'];
        $payload  = $payload + [
            'item_name'            => $itemName,
            'full_address_keyword' => str_replace('-', ' ', GeneralUtil::getSlug($itemName))
          ];
        
        $instanceItem = new Item();
        $result       = $instanceItem->add($payload);
        if (empty($result)) {
          return $this->createResponseErrorAPI('Hệ thống đang gặp sự cố, Vui lòng thử lại !');
        }
        
        $conditions    = [
          'item_id'    => $result,
          'is_deleted' => 0
        ];
        $arrItemDetail = $instanceItem->getDetail($conditions);
        
        return $this->createResponseSuccess(null, ['arrItemDetail' => $arrItemDetail]);
      }
      
      return $this->createResponseErrorParam('Method is not supported !');
    }
    
    public function update()
    {
      if ($this->request->isPost()) {
        $params = $this->request->getPost();
        $intID  = FilterUtil::int($params['id']);
        if (empty($intID))
          return $this->createResponseErrorParam();
        
        $conditions = [
          'item_id'    => $intID,
          'is_deleted' => 0,
        ];
        
        $instanceItem  = new Item();
        $arrItemDetail = $instanceItem->getDetail($conditions);
        if (empty($arrItemDetail)) {
          return $this->createResponseErrorAPI();
        }
        
        /* #Validate */
        $arrParams = $this->_validate($params);
        if (empty($arrParams))
          return $this->createResponseErrorParam();
        /* */
        
        $price    = FilterUtil::float($arrParams['price']);
        $currency = FilterUtil::string($arrParams['currency']);
        $priceVND = GeneralUtil::convertPrice2VND($price, $currency);
        
        /* #is_promotion */
        $isPromotion    = FilterUtil::int($arrParams['is_promotion']);
        $promotionPrice = FilterUtil::float($arrParams['promotion_price']);
        if ($isPromotion) {
          $promotionPriceVND = GeneralUtil::convertPrice2VND($promotionPrice, $currency);
        }
        /* ## */
        
        $arrImageList = array_filter(array_values($arrParams['item_image_list']));
        
        $payload = [
          'item_feature_image'  => current($arrImageList),
          'item_image_list'     => json_encode($arrImageList),
          'item_description'    => FilterUtil::string($arrParams['item_description']),
          'item_content'        => FilterUtil::string($arrParams['item_content']),
          'floor_area'          => FilterUtil::int($arrParams['floor_area']),
          'floor_area_unit'     => 'm2',
          'host_phone'          => FilterUtil::string($arrParams['host_phone']),
          'price'               => $price,
          'price_vnd'           => $priceVND,
          'currency'            => $currency,
          'promotion_price'     => $promotionPrice,
          'promotion_price_vnd' => $promotionPriceVND,
          'commission'          => FilterUtil::float($arrParams['commission']),
          'category_id'         => FilterUtil::int($arrParams['category_id']),
          'city_id'             => FilterUtil::int($arrParams['city_id']),
          'district_id'         => FilterUtil::int($arrParams['district_id']),
          'ward_id'             => FilterUtil::int($arrParams['ward_id']),
          'street_id'           => FilterUtil::int($arrParams['street_id']),
          'city_name'           => FilterUtil::string($arrParams['city_name']),
          'district_name'       => FilterUtil::string($arrParams['district_name']),
          'ward_name'           => FilterUtil::string($arrParams['ward_name']),
          'street_name'         => FilterUtil::string($arrParams['street_name']),
          'address'             => FilterUtil::string($arrParams['address']),
          'is_cart'             => FilterUtil::int($arrParams['is_cart']),
          'status'              => FilterUtil::int($arrParams['status']),
          'updated_at'          => time(),
          'is_deleted'          => FilterUtil::int($arrParams['is_deleted']),
          'meta_title'          => FilterUtil::string($arrParams['meta_title']),
          'meta_description'    => FilterUtil::string($arrParams['meta_description']),
          'meta_keyword'        => FilterUtil::string($arrParams['meta_keyword']),
          'user_updated_id'     => FilterUtil::int($arrParams['user_updated_id']),
          'is_favorite'         => FilterUtil::int($arrParams['is_favorite']),
          'is_private'          => FilterUtil::int($arrParams['is_private']),
          'is_promotion'        => $isPromotion,
        ];
        
        $itemName = $payload['address'] . ' - ' . $payload['street_name'] . ' - ' . $payload['ward_name'] . ' - ' . $payload['district_name'] . ' - ' . $payload['city_name'];
        $payload  = $payload + [
            'item_name'            => $itemName,
            'full_address_keyword' => str_replace('-', ' ', GeneralUtil::getSlug($itemName))
          ];
        
        $conditions = [
          'item_id' => $intID,
          'is_deleted'
        ];
        
        $result = $instanceItem->edit($payload, $conditions);
        if (empty($result)) {
          return $this->createResponseErrorAPI('Hệ thống đang gặp sự cố, Vui lòng thử lại !');
        }
        
        $arrItemDetail = $instanceItem->getDetail($conditions);
        
        $instanceItemLog = new ItemLog();
        $instanceItemLog->add([
          'item_id'         => $arrItemDetail['item_id'],
          'note'            => FilterUtil::string($arrParams['note']),
          'user_created_id' => $arrItemDetail['user_updated_id'],
          'created_at'      => time(),
        ]);
        
        return $this->createResponseSuccess(null, ['arrItemDetail' => $arrItemDetail]);
      }
      
      return $this->createResponseErrorParam('Method is not supported !');
    }
    
    public function delete()
    {
      if ($this->request->isPost()) {
        $params = $this->request->getPost();
        $intID  = FilterUtil::int($params['id']);
        if (empty($intID))
          return $this->createResponseErrorParam();
        
        $conditions = [
          'item_id'    => $intID,
          'is_deleted' => 0,
        ];
        
        $instanceItem  = new Item();
        $arrItemDetail = $instanceItem->getDetail($conditions);
        if (empty($arrItemDetail)) {
          return $this->createResponseErrorAPI('Người dùng không tồn tại !');
        }
        
        $payload = [
          'is_deleted' => 1,
          'updated_at' => time(),
        ];
        
        $conditions = [
          'item_id' => $intID
        ];
        
        $result = $instanceItem->edit($payload, $conditions);
        if (empty($result)) {
          return $this->createResponseErrorAPI('Hệ thống đang gặp sự cố, Vui lòng thử lại !');
        }
        
        return $this->createResponseSuccess(null, ['id' => $intID]);
      }
      
      return $this->createResponseErrorParam('Method is not supported !');
    }
    
    private function _filter($params, &$conditions)
    {
      $strFilter = '?';
      
      $strKeyword = FilterUtil::string($params['s']);
      if (!empty($strKeyword)) {
        $conditions['keyword'] = $strKeyword;
        
        $strFilter .= 's=' . $strKeyword;
      }
      
      if (!empty($params['district_id'])) {
        $districtID = FilterUtil::string($params['district_id']);
        
        $conditions['district_id_list'] = explode(',', $districtID);
        
        $strFilter .= 'districtID=' . $districtID;
      }
      
      if (!empty($params['category_id'])) {
        $categoryID = FilterUtil::string($params['category_id']);
        
        $conditions['category_id_list'] = explode(',', $categoryID);
        
        $strFilter .= 'categoryID=' . $categoryID;
      }
      
      if (!empty($params['startCreatedAt']) && !empty($params['endCreatedAt'])) {
        $startCreatedAt = FilterUtil::string($params['startCreatedAt']);
        $endCreatedAt   = FilterUtil::string($params['endCreatedAt']);
        
        $conditions['created_at_start_format'] = $startCreatedAt;
        $conditions['created_at_end_format']   = $endCreatedAt;
        
        $conditions['created_at_start'] = strtotime($startCreatedAt);
        $conditions['created_at_end']   = strtotime($endCreatedAt);
        
        $strFilter .= 'startCreatedAt=' . $startCreatedAt . '&endCreatedAt=' . $endCreatedAt;
      }
      
      $strStreetName = FilterUtil::string($params['s_like']);
      if (!empty($strStreetName)) {
        $conditions['street_name_like'] = $strStreetName;
        
        $strFilter .= 's_like=' . $strStreetName;
      }
      
      $isFavorite = FilterUtil::int($params['is_favorite']);
      if (!empty($isFavorite)) {
        $conditions['is_favorite'] = $isFavorite;
        
        $strFilter .= 'is_favorite=' . $isFavorite;
      }
      
      $isPrivate = FilterUtil::int($params['is_private']);
      if (!empty($isPrivate)) {
        $conditions['is_private'] = $isPrivate;
        
        $strFilter .= 'is_private=' . $isPrivate;
      }
      
      if (!empty($params['price_vnd_from']) && !empty($params['price_vnd_to'])) {
        $currency = FilterUtil::string($params['currency']);
        
        $priceVNDFrom = FilterUtil::int(str_replace(',', '', $params['price_vnd_from']));
        $priceVNDTo   = FilterUtil::int(str_replace(',', '', $params['price_vnd_to']));
        
        $priceVNDFrom = GeneralUtil::convertPrice2VND($priceVNDFrom, $currency);
        $priceVNDTo   = GeneralUtil::convertPrice2VND($priceVNDTo, $currency);
        
        $conditions['price_vnd_from'] = $priceVNDFrom;
        $conditions['price_vnd_to']   = $priceVNDTo;
        
        $strFilter .= 'price_vnd_from=' . $priceFrom . '&price_vnd_to=' . $priceTo;
      }
      
      if (!empty($params['sort'])) {
        $sort = FilterUtil::string($params['sort']);
        
        $conditions['sort'] = $sort;
      }
      
      /* #is_promotion */
      $isPromotion = FilterUtil::int($params['is_promotion']);
      if (!empty($isPromotion)) {
        $conditions['is_promotion'] = $isPromotion;
        
        $strFilter .= 'is_promotion=' . $isPromotion;
      }
      /* ## */
      
      return $strFilter;
    }
    
    private function _validate($params)
    {
      $itemName = FilterUtil::string($params['item_name']);
//      if (empty($itemName))
//        return false;
      
      return array_merge($params, [
        'item_name' => $itemName,
      ]);
    }
  }
