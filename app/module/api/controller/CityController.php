<?php
  
  namespace Module\Api\Controller;
  
  use Model\City;
  
  use Base\Controller\BaseController;
  
  use Util\FilterUtil;
  
  class CityController extends BaseController
  {
    public function read()
    {
      $intPage  = 1;
      $intLimit = 9999;
      
      $conditions = [
        'is_deleted' => 0,
        'city_id'    => 29
      ];
      
      $instanceCity = new City();
      $arrCityList  = $instanceCity->getListLimit($conditions, $intPage, $intLimit, 'city_id ASC');
      
      return $this->createResponseSuccess(null, ['arrCityList' => $arrCityList]);
    }
  }
