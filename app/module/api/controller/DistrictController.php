<?php
  
  namespace Module\Api\Controller;
  
  use Model\District;
  
  use Base\Controller\BaseController;
  
  use Util\FilterUtil;
  
  class DistrictController extends BaseController
  {
    public function read()
    {
      $params = $this->request->getPost();
      
      $intCityID = FilterUtil::int($params['cityID']);
      if (empty($intCityID)) {
        return $this->createResponseErrorParam();
      }
      
      $intPage  = 1;
      $intLimit = 9999;
      
      $conditions = [
        'city_id'    => $intCityID,
        'is_deleted' => 0
      ];
      
      $instanceDistrict = new District();
      $arrDistrictList  = $instanceDistrict->getListLimit($conditions, $intPage, $intLimit, 'district_id ASC');
      
      return $this->createResponseSuccess(null, ['arrDistrictList' => $arrDistrictList]);
    }
  }
