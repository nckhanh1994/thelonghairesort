<?php
  
  namespace Module\Api\Controller;
  
  use Base\Controller\BaseController;
  
  use
    Model\User,
    Model\Item,
    Model\Contact;
  
  use
    Util\GeneralUtil,
    Util\FilterUtil;
  
  use
    Validation\UserValid,
    Validation\ItemValid;
  
  class UserController extends BaseController
  {
    public function get()
    {
      if ($this->request->isPost()) {
        $params = $this->request->getPost();
        
        $intID = FilterUtil::int($params['id']);
        if (empty($intID))
          return $this->createResponseErrorParam();
        
        $conditions = [
          'user_id'    => $intID,
          'is_deleted' => 0
        ];
        
        $instanceUser = new User();
        $arrUser      = $instanceUser->getDetail($conditions);
        
        return $this->createResponseSuccess(null, ['arrUserDetail' => $arrUser]);
      }
      
      return $this->createResponseErrorParam('Method is not supported !');;
    }
    
    public function read()
    {
      $params = $this->request->getPost();
      
      $intPage  = isset($params['page']) ? FilterUtil::int($params['page'], 1) : 1;
      $intLimit = isset($params['limit']) ? FilterUtil::int($params['limit'], 15) : 15;
      
      $conditions = [
        'is_deleted' => 0,
      ];
      
      if (!empty($params['role_id_more_than'])) {
        $conditions['role_id_more_than'] = FilterUtil::int($params['role_id_more_than']);
      }
      
      $instanceUser = new User();
      $arrUsers     = $instanceUser->getListLimit($conditions, $intPage, $intLimit);
      
      return $this->createResponseSuccess(null, ['userList' => $arrUsers]);
    }
    
    public function create()
    {
      if (!$this->request->isPost()) {
        return $this->createResponseErrorParam('Method is not supported !');
      }
      
      $params = $this->request->getPost();
      
      /* ## Validate */
      $arrParam = $this->_validate($params);
      if (empty($arrParam))
        return $this->createResponseErrorParam();
      /* ## */
      
      return $this->_createAct($params);
    }
    
    /**
     * @param array $param
     */
    private function _createAct($params)
    {
      /* ## Check email exists */
      $strEmail   = FilterUtil::string($params['email']);
      $conditions = [
        'email'      => $strEmail,
        'is_deleted' => 0
      ];
      
      $instanceUser = new User();
      $isExist      = $instanceUser->getDetail($conditions);
//      if (!empty($isExist))
//        return $this->createResponseErrorAPI('Email này đã được sử dụng. Vui lòng chọn Email khác !');
      /* ## */
      
      $payload = [
        'profile_image' => FilterUtil::string($params['profile_image']),
        'fullname'      => $params['fullname'],
        'email'         => $params['email'],
        'phone'         => $params['phone'],
        'address'       => $params['address'],
        'group_id'      => !empty($params['group_id']) ? $params['group_id'] : 1,
        'is_actived'    => FilterUtil::int($params['is_actived']),
        'created_at'    => time(),
        'role_id'       => FilterUtil::int($params['role_id']),
      ];
      
      if (!empty($params['password'])) {
        $payload['password'] = $this->security->hash($params['password']);
      }
      
      $result = $instanceUser->add($payload);
      if (empty($result)) {
        return $this->createResponseErrorAPI('Hệ thống đang gặp sự cố, Vui lòng thử lại !');
      }
      
      $conditions = [
        'user_id'    => $result,
        'is_deleted' => 0
      ];
      $arrUser    = $instanceUser->getDetail($conditions);
      
      return $this->createResponseSuccess(null, ['arrUserDetail' => $arrUser]);
    }
    
    public function update()
    {
      if ($this->request->isPost()) {
        $params = $this->request->getPost();
        $intID  = FilterUtil::int($params['id']);
        if (empty($intID))
          return $this->createResponseErrorParam();
        
        $conditions = [
          'user_id'    => $intID,
          'is_deleted' => 0,
        ];
        
        $instanceUser = new User();
        $arrUser      = $instanceUser->getDetail($conditions);
        if (empty($arrUser)) {
          return $this->createResponseErrorAPI();
        }
        
        /* #Validate */
        $arrParam = UserValid::validate($params);
        if (empty($arrParam))
          return $this->createResponseErrorParam();
        /* */
        
        /* ! Check exist email */
        $strEmail   = FilterUtil::string($params['email']);
        $conditions = [
          'email'       => $strEmail,
          'is_deleted'  => 0,
          'not_user_id' => $intID
        ];
        $isExist    = $instanceUser->getDetail($conditions);
        if (!empty($isExist))
          return $this->createResponseErrorAPI('Email này đã được sử dụng. Vui lòng chọn Email khác !');
        /*  */
        
        $payload = [
          'profile_image' => $arrParam['profile_image'],
          'fullname'      => $arrParam['fullname'],
          'email'         => $arrParam['email'],
          'phone'         => $arrParam['phone'],
          'address'       => $arrParam['address'],
          'group_id'      => !empty($arrParam['group_id']) ? $arrParam['group_id'] : 1,
          'is_actived'    => FilterUtil::int($arrParam['is_actived']),
          'updated_at'    => time(),
          'role_id'       => FilterUtil::int($arrParam['role_id']),
        ];
        
        if (!empty($arrParam['password'])) {
          $payload['password'] = $this->security->hash($arrParam['password']);
        }
        
        $conditions = [
          'user_id' => $intID,
          'is_deleted'
        ];
        $result     = $instanceUser->edit($payload, $conditions);
        if (empty($result)) {
          return $this->createResponseErrorAPI('Hệ thống đang gặp sự cố, Vui lòng thử lại !');
        }
        
        $arrUser = $instanceUser->getDetail($conditions);
        
        return $this->createResponseSuccess(null, ['arrUserDetail' => $arrUser]);
      }
      
      return $this->createResponseErrorParam('Method is not supported !');
    }
    
    public function delete()
    {
      if ($this->request->isPost()) {
        $params = $this->request->getPost();
        $intID  = FilterUtil::int($params['id']);
        if (empty($intID))
          return $this->createResponseErrorParam();
        
        $conditions = [
          'user_id'    => $intID,
          'is_deleted' => 0,
        ];
        
        $instanceUser = new User();
        $arrUser      = $instanceUser->getDetail($conditions);
        if (empty($arrUser)) {
          return $this->createResponseErrorAPI('Người dùng không tồn tại !');
        }
        
        $payload = [
          'is_deleted' => 1,
          'updated_at' => time(),
        ];
        
        $conditions = [
          'user_id' => $intID
        ];
        
        $result = $instanceUser->edit($payload, $conditions);
        if (empty($result)) {
          return $this->createResponseErrorAPI('Hệ thống đang gặp sự cố, Vui lòng thử lại !');
        }
        
        return $this->createResponseSuccess(null, ['id' => $intID]);
      }
      
      return $this->createResponseErrorParam('Method is not supported !');
    }
    
    /* ********************************
     * #reg - ! Đăng ký tài khoản
     ********************************** */
    public function reg()
    {
      if (!$this->request->isPost()) {
        return $this->createResponseErrorParam('Method is not supported !');
      }
      
      $params = $this->request->getPost();
      
      $params = [
        'fullname' => $params['user_name'],
        'email'    => $params['user_email'],
        'phone'    => $params['user_phone'],
        'password' => $params['user_pass']
      ]; // reset
      
      /* ## Validate */
      $arrParams = UserValid::validate($params);
      if (empty($arrParams))
        return $this->createResponseErrorParam();
      /* ## */
      
      return $this->_createAct($arrParams);
    }
    /* ////////////////////////////// */
    
    /* ********************************
     #login - ! Đăng nhập tài khoản
     ********************************** */
    public function login()
    {
      if (!$this->request->isPost()) {
        return $this->createResponseErrorParam($this->translateUtil->_('METHOD_NOT_SUPPORT'));
      }
      
      $params = $this->request->getPost();
      
      $strEmail = FilterUtil::string($params['user_email']);
      if (empty($strEmail)) {
        return $this->createResponseErrorParam($this->translateUtil->_('EMAIL', 'EMPTY'));
      }
      
      $strPassword = FilterUtil::string($params['user_pass']);
      if (empty($strPassword)) {
        return $this->createResponseErrorParam($this->translateUtil->_('PASSWORD', 'EMPTY'));
      }
      
      /* ## Kiểm tra thông tin người dùng */
      $conditions = [
        'email'      => $strEmail,
        'is_actived' => 1,
        'is_deleted' => 0,
      ];
      
      $instanceUser = new User();
      $arrUser      = $instanceUser->getDetail($conditions);
      
      if (empty($arrUser)) {
        return $this->createResponseErrorParam($this->translateUtil->_('EMAIL', 'NOT_EXIST'));
      }
      /* ## */
      
      /* ## Kiểm tra mật khẩu */
//      if (!$this->security->checkHash($strPassword, $arrUser['password'])) {
//        return $this->createResponseErrorParam($this->translateUtil->_('EMAIL_OR_PASSWORD', 'NOT_EXACT'));
//      }
      /* ## */
      
      $arrPayload = [
        'user_info' => $arrUser
      ];
      
      return $this->createResponseSuccess($this->translateUtil->_('PROCESS_DATA', 'SUCCESS'), $arrPayload);
    }
    /* ////////////////////////////// */
    
    /* ********************************
     #logout - ! Đăng xuất tài khoản
     ********************************** */
    public function logout()
    {
      // Remove a session variable
      $this->session->remove($this->identityKey);
      
      return $this->response->redirect('/');
    }
    /* ////////////////////////////// */
    
    /* ********************************
     #rent - ! Đăng tin cho thuê
     ********************************** */
    public function rent()
    {
      if (!$this->request->isPost()) {
        return $this->createResponseErrorParam($this->translateUtil->_('METHOD_NOT_SUPPORT'));
      }
      
      $params      = $this->request->getPost();
      $strFormData = FilterUtil::string($params['form_data']);
      if (empty($strFormData)) {
        return $this->createResponseErrorParam($this->translateUtil->_('PARAM_EMPTY'));
      }
      
      $arrParams = [];
      parse_str($strFormData, $arrParams);
      
      if (!ItemValid::validateRent($arrParams)) {
        return $this->createResponseErrorParam($this->translateUtil->_('PARAM_EMPTY'));
      }
      
      /* ## Thêm sản phẩm */
      $arrHostInfo = [
        'host_name'    => $arrParams['host_name'],
        'host_phone'   => $arrParams['host_phone'],
        'host_address' => $arrParams['host_address']
      ];
      
      $arrPhotoImg = FilterUtil::array($arrParams['photoimg']);
      
      $payload = [
        'item_name'           => $arrParams['item_name'],
        'item_feature_image'  => current($arrPhotoImg),
        'item_image_list'     => json_encode($arrPhotoImg),
        'item_description'    => $arrParams['item_description'],
        'item_content'        => $arrParams['item_description'],
        'floor_area'          => $arrParams['floor_area_from'],
        'floor_area_unit'     => 'm2',
        'host_phone'          => $arrHostInfo['host_phone'],
        'price'               => $arrParams['price_from'],
        'price_vnd'           => $arrParams['price_from'],
        'currency'            => 'vnd',
        'promotion_price'     => 0,
        'promotion_price_vnd' => 0,
        'commission'          => 0,
        'category_id'         => $arrParams['category_id'],
        'city_id'             => $arrParams['city_id'],
        'district_id'         => $arrParams['district_id'],
        'ward_id'             => 0,
        'street_id'           => 0,
        'city_name'           => $arrParams['city_name'],
        'district_name'       => $arrParams['district_name'],
        'ward_name'           => $arrParams['ward_name'],
        'street_name'         => $arrParams['street_name'],
        'address'             => $arrParams['address'],
        'is_cart'             => 0,
        'status'              => 1,
        'created_at'          => time(),
        'is_deleted'          => 0,
        'meta_title'          => $arrParams['item_name'],
        'meta_description'    => $arrParams['item_name'],
        'meta_keyword'        => $arrParams['item_name'],
        'user_created_id'     => $arrParams['user_created_id'],
        'is_favorite'         => 0,
        'is_private'          => 0,
        'is_promotion'        => 0,
        'host_info'           => json_encode($arrHostInfo)
      ];
      
      $itemName = $payload['item_name'] . ' - ' . $payload['address'] . ' - ' . $payload['district_name'] . ' - ' . $payload['city_name'];
      $payload  = $payload + [
          'full_address_keyword' => str_replace('-', ' ', GeneralUtil::getSlug($itemName))
        ];
      
      $strType = FilterUtil::string($params['type']);
      
      $itemID = 0;
      
      $instanceItem = new Item();
      if ($strType == 'editRent') {
        $itemID = FilterUtil::float($arrParams['item_id']);
        
        $conditions = [
          'item_id'    => $itemID,
          'is_deleted' => 0,
          'is_private' => 0,
          'is_cart'    => 0,
        ];
        
        $result = $instanceItem->edit($payload, $conditions);
      } else {
        $itemID = $result = $instanceItem->add($payload);
      }
      
      if (empty($result)) {
        return $this->createResponseErrorAPI($this->translateUtil->_('SYSTEM_ERROR'));
      }
      /* ## */
      
      $arrResponse = [
        'id'   => $itemID,
        'slug' => GeneralUtil::getSlug($payload['item_name']),
      ];
      
      return $this->createResponseSuccess(null, $arrResponse);
    }
    /* ////////////////////////////// */
    
    /* ********************************
     #contact - ! Liên hệ
     ********************************** */
    public function contact()
    {
      if (!$this->request->isPost()) {
        return $this->createResponseErrorParam($this->translateUtil->_('METHOD_NOT_SUPPORT'));
      }
      
      $params      = $this->request->getPost();
      $strFormData = FilterUtil::string($params['form_data']);
      if (empty($strFormData)) {
        return $this->createResponseErrorParam($this->translateUtil->_('PARAM_EMPTY'));
      }
      
      $arrParams = [];
      parse_str($strFormData, $arrParams);
      
      /* ## Thêm sản phẩm */
      $payload = [
        'info' => json_encode($arrParams)
      ];
      
      $instanceContact = new Contact();
      $instanceContact->add($payload);
      
      $arrResData = [
        'redirect_url' => $_SERVER['HTTP_REFERER']
      ];
      
      return $this->createResponseSuccess(null, $arrResData);
    }
    /* ////////////////////////////// */
  }
