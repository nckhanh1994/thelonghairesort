<?php
  
  use Phalcon\Mvc\Router\Group as RouterGroup;
  
  /**
   * ======================================================================================
   * ================================== FRONTEND ==========================================
   * ======================================================================================
   */
  $frontend = new RouterGroup([
    'namespace' => 'Module\Frontend\Controller',
    'module'    => 'frontend',
  ]);
  
  $frontend->setPrefix('');
  
  $frontend->add('/:params', ['module' => 'frontend', 'controller' => 'index', 'action' => 'index'])->setName('frontend-home');
  $frontend->add('/:controller/:action/:params', ['module' => 'frontend', 'controller' => 1, 'action' => 2, 'params' => 3])->setName('frontend');
  
  $frontend->add('/tim-kiem.html', ['module' => 'frontend', 'controller' => 'item', 'action' => 'search'])->setName('frontend:item:search');
  $frontend->add('/tim-kiem/trang-{page:[0-9]+}.html', ['module' => 'frontend', 'controller' => 'item', 'action' => 'search'])->setName('frontend:item:searchpaging');
  $frontend->add('/{slug:[a-zA-Z0-9-]+}-c{id:[0-9]+}.html', ['module' => 'frontend', 'controller' => 'item', 'action' => 'index'])->setName('frontend:item:index');
  $frontend->add('/{slug:[a-zA-Z0-9-]+}-c{id:[0-9]+}-trang{page:[0-9]+}.html', ['module' => 'frontend', 'controller' => 'item', 'action' => 'index'])->setName('frontend:item:indexpaging');
  $frontend->add('/{slug:[a-zA-Z0-9-]+}-{id:[0-9]+}.html', ['module' => 'frontend', 'controller' => 'item', 'action' => 'view'])->setName('frontend:item:view');
  
  $frontend->add('/dang-ky.html', ['module' => 'frontend', 'controller' => 'user', 'action' => 'reg'])->setName('frontend:user:reg');
  $frontend->add('/dang-nhap.html', ['module' => 'frontend', 'controller' => 'user', 'action' => 'login'])->setName('frontend:user:login');
  $frontend->add('/dang-xuat.html', ['module' => 'frontend', 'controller' => 'user', 'action' => 'logout'])->setName('frontend:user:logout');
  $frontend->add('/chon-danh-muc-dang-tin.html', ['module' => 'frontend', 'controller' => 'user', 'action' => 'rentCategory'])->setName('frontend:user:rentcategory');
  $frontend->add('/dang-tin-cho-thue-{id:[0-9]+}.html', ['module' => 'frontend', 'controller' => 'user', 'action' => 'rent'])->setName('frontend:user:rent');
  $frontend->add('/dang-tin-thanh-cong.html', ['module' => 'frontend', 'controller' => 'user', 'action' => 'rentComplete'])->setName('frontend:user:rentComplete');
  $frontend->add('/dang-nhap-mang-xa-hoi-{provider:[a-z]+}', ['controller' => 'user', 'action' => 'loginSocial',])->setName('frontend:user:loginsocial');
  $frontend->add('/quan-ly-tin-dang.html', ['module' => 'frontend', 'controller' => 'user', 'action' => 'manageItem'])->setName('frontend:user:manageitem');
  $frontend->add('/quan-ly-tin-dang-trang-{page:[0-9]+}.html', ['module' => 'frontend', 'controller' => 'user', 'action' => 'manageItem'])->setName('frontend:user:manageitempaging');
  $frontend->add('/sua-tin-{id:[0-9]+}.html', ['module' => 'frontend', 'controller' => 'user', 'action' => 'editRent'])->setName('frontend:user:editrent');
  
  $frontend->add('/quan-go-vap.html', ['module' => 'frontend', 'controller' => 'item', 'action' => 'category', 'district_id' => 1])->setName('frontend:item:district1');
  $frontend->add('/quan-tan-binh.html', ['module' => 'frontend', 'controller' => 'item', 'action' => 'category', 'district_id' => 2])->setName('frontend:item:district2');
  $frontend->add('/quan-phu-nhuan.html', ['module' => 'frontend', 'controller' => 'item', 'action' => 'category', 'district_id' => 3])->setName('frontend:item:district3');
  $frontend->add('/quan-tan-phu.html', ['module' => 'frontend', 'controller' => 'item', 'action' => 'category', 'district_id' => 4])->setName('frontend:item:district4');
  $frontend->add('/quan-10.html', ['module' => 'frontend', 'controller' => 'item', 'action' => 'category', 'district_id' => 5])->setName('frontend:item:district5');
  
  
  return $frontend;
