<?php
  
  namespace Module\Frontend;

  use Phalcon\Loader;
  use Phalcon\Di\DiInterface;
  use Phalcon\Mvc\Dispatcher;
  use Phalcon\Mvc\ModuleDefinitionInterface;
  use Phalcon\Mvc\View;
  
  class Module implements ModuleDefinitionInterface
  {
    public function registerAutoloaders(DiInterface $container = null)
    {
      $loader = new Loader();
      $loader->registerNamespaces([
        'Module\Frontend\Controller'      => MODULE_PATH . '/frontend/controller',
        'Module\Frontend\Controller\Ajax' => MODULE_PATH . '/frontend/controller/ajax',
      ]);
      $loader->register();
    }
    
    /**
     * Register the services here to make them general or register in the ModuleDefinition to make them module-specific
     */
    public function registerServices(DiInterface $di)
    {
      
      //Registering a dispatcher
      $di->set('dispatcher', function () {
        $dispatcher = new Dispatcher();
        $dispatcher->setDefaultNamespace("Module\Frontend\Controllers\\");
        
        return $dispatcher;
      });
      
      //Registering the view component
      $di['view'] = function () use ($di) {
        $view   = new View();
        $config = $this->get('config');
        
        $viewPath = MODULE_PATH . '/frontend/view/template/' . $config->template->frontend->version . '/' . $di->get('template');
        
        $view->setViewsDir($viewPath);
        $view->setLayoutsDir('_layout/');
        $view->setPartialsDir('_partial/');
        $view->setLayout('main');
        
        return $view;
      };
      
    }
    
  }
