<?php
  
  namespace Module\Frontend\Controller;
  
  use Phalcon\Mvc\View\Simple as View;
  
  use Base\Controller\MyController;
  
  use
    Model\Item;
  
  use Util\Pagination;
  
  use
    Util\GeneralUtil,
    Util\FilterUtil;
  
  class IndexController extends MyController
  {
    public function initialize()
    {
      parent::initialize();
    
      $this->CDATA['url']['refererURL'] = !empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '/';
    
      $this->view->setVars([
        'CDATA'         => $this->CDATA,
        'arrCategories' => $this->CDATA['common']['categories'],
        'arrCities'     => $this->CDATA['common']['cities'],
        'arrDistricts'  => $this->CDATA['common']['districts']
      ]);
    }
    
    public function indexAction()
    {
      /* ## Items theo danh mục */
      $arrColumns    = ['item_id', 'item_name', 'item_feature_image', 'item_description', 'host_phone', 'price_vnd', 'category_id', 'floor_area', 'floor_area_unit', 'city_name', 'district_name', 'created_at', 'address'];
      $instanceItem  = new Item();
      $arrItemsByCat = $instanceItem->getItemsByCat([], 1, 12, 'item_id DESC', $arrColumns);
      /* ## */
      
      /* ## Items đăng gần đây */
      $conditions = [
        'is_deleted' => 0,
        'is_private' => 0,
        'is_cart'    => 0,
      ];
      
      $instanceItem   = new Item();
      $arrRecentItems = $instanceItem->getListLimit($conditions, 1, 5, '', $arrColumns);
      /* ## */
      
      $this->view->setVars([
        'arrItemsByCat'  => $arrItemsByCat,
        'arrRecentItems' => $arrRecentItems
      ]);
    }
  }
