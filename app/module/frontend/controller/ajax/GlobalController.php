<?php
  
  namespace Module\Frontend\Controller\Ajax;
  
  use Base\Controller\MyAjaxController;
  
  use Model\RequireBooking;
  
  use Util\Safe;
  
  class GlobalController extends MyAjaxController
  {
    public function sendRequireAction()
    {
      if ($this->request->isPost()) {
        $params = $this->request->getPost();
        
        if (empty($params['formData'])) {
          return $this->createResponseErrorParam();
        }
        
        $arrParam = [];
        foreach ($params['formData'] as $param) {
          $arrParam[ $param['name'] ] = Safe::_string($param['value']);
        }
        
        $arrAdd = [
          'startDate'    => strtotime($arrParam['startDate']),
          'endDate'      => strtotime($arrParam['endDate']),
          'qty'          => Safe::_int($arrParam['qty'], 2),
          'phone'        => $arrParam['phone'],
          'created_date' => time(),
          'updated_date' => time()
        ];
        
        $instanceRequireBooking = new RequireBooking();
        $instanceRequireBooking->add($arrAdd);
        
        return $this->createResponseSuccess('Yêu cầu của quý khách đã được gửi đi, chúng tôi sẽ liên hệ sớm đến quý khách xin chân thành cảm ơn !');
      }
      
      return $this->createResponseErrorAPI('Method is not supported !');
    }
  }
