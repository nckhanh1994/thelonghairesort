<?php
  
  namespace Module\Frontend\Controller;
  
  use Model\User;
  use Phalcon\Mvc\View\Simple as View;
  
  use Base\Controller\MyController;
  
  use
    Model\Item,
    Model\City,
    Model\District;
  
  use Util\Pagination;
  
  use
    Util\GeneralUtil,
    Util\FilterUtil,
    Util\HttpUtil;
  
  use
    Validation\UserValid;
  
  class UserController extends MyController
  {
    public function initialize()
    {
      parent::initialize();
      
      $this->CDATA['url']['refererURL'] = !empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '/';
      switch ($this->controller) {
        case 'user':
          $this->CDATA['url']['regURL']   = $this->url->get(['for' => 'frontend:user:reg']);
          $this->CDATA['url']['loginURL'] = $this->url->get(['for' => 'frontend:user:login']);
          break;
      }
      
      $this->view->setVars([
        'CDATA'         => $this->CDATA,
        'arrCategories' => $this->CDATA['common']['categories'],
        'arrCities'     => $this->CDATA['common']['cities'],
        'arrDistricts'  => $this->CDATA['common']['districts']
      ]);
    }
    
    /* ********************************
     * #reg - ! Đăng ký tài khoản
     ********************************** */
    public function regAction()
    {
      if (defined('UID')) {
        return $this->redirect('/');
      }
      
      if ($this->request->isAjax()) {
        $params = $this->request->getPost();
        
        $strFormData = FilterUtil::string($params['form_data']);
        if (empty($strFormData)) {
          return $this->createResponseErrorParam($this->translateUtil->_('PARAM_EMPTY'));
        }
        
        parse_str($strFormData, $arrReqParams);
        
        /* ## Tạo thông tin người dùng */
        $strReqURL        = API_URL . '/user/reg';
        $httpResponseData = HttpUtil::sendRequest('POST', $strReqURL, $arrReqParams);
        if (empty($httpResponseData['dataResponse']))
          return $this->createResponseErrorParam($this->translateUtil->_('SYSTEM_ERROR'));
        
        $dataResponse = $httpResponseData['dataResponse'];
        if ($dataResponse['success'] !== 1) {
          return $this->createResponseErrorParam($dataResponse['message'], $dataResponse['payload']);
        }
        /* ## */
        
        /* ## Lưu thông tin đăng nhập */
        $arrUser  = $dataResponse['payload']['arrUserDetail'];
        $arrLogin = [
          'user_id'   => $arrUser['user_id'],
          'fullname'  => $arrUser['fullname'],
          'email'     => $arrUser['email'],
          'role_id'   => $arrUser['role_id'],
          'max_price' => 0
        ];
        
        $this->session->set('frontend:auth', $arrLogin);
        /* ## */
        
        return $this->createResponseSuccess($this->translateUtil->_('PROCESS_DATA', 'SUCCESS'));
      }
      
      $this->view->setVars([
        'arrValidationRules' => UserValid::jsValidationRulesReg()
      ]);
    }
    /* ////////////////////////////// */
    
    /* ********************************
     * #login - ! Đăng nhập
     ********************************** */
    public function loginAction()
    {
      if (defined('UID')) {
        return $this->redirect('/');
      }
      
      if ($this->request->isAjax()) {
        $params = $this->request->getPost();
        
        $strFormData = FilterUtil::string($params['form_data']);
        if (empty($strFormData)) {
          return $this->createResponseErrorParam($this->translateUtil->_('PARAM_EMPTY'));
        }
        
        parse_str($strFormData, $arrReqParams);
        
        /* ## Kiểm tra thông tin đăng nhập */
        $strReqURL        = API_URL . '/user/login';
        $httpResponseData = HttpUtil::sendRequest('POST', $strReqURL, $arrReqParams);
        
        if (empty($httpResponseData['dataResponse']))
          return $this->createResponseErrorParam($this->translateUtil->_('SYSTEM_ERROR'));
        
        $dataResponse = $httpResponseData['dataResponse'];
        if ($dataResponse['success'] !== 1) {
          return $this->createResponseErrorParam($dataResponse['message'], $dataResponse['payload']);
        }
        /* ## */
        
        /* ## Lưu thông tin đăng nhập */
        $isRemember = FilterUtil::int($arrReqParams['is_remember']);
        if (!empty($isRemember)) {
          $this->cookies->set('login_encrypt', openssl_encrypt(json_encode($arrUser), CIPHER_METHOD, SECRET_KEY));
        }
        
        $arrUser  = $dataResponse['payload']['user_info'];
        $arrLogin = [
          'user_id'   => $arrUser['user_id'],
          'fullname'  => $arrUser['fullname'],
          'email'     => $arrUser['email'],
          'role_id'   => $arrUser['role_id'],
          'phone'     => $arrUser['phone'],
          'max_price' => 0
        ];
        
        $this->session->set('frontend:auth', $arrLogin);
        /* ## */
        
        return $this->createResponseSuccess($this->translateUtil->_('PROCESS_DATA', 'SUCCESS'));
      }
      
      $this->view->setVars([
        'arrValidationRules' => UserValid::jsValidationRulesLogin()
      ]);
    }
    /* ////////////////////////////// */
    
    /* ********************************
     #logout - ! Đăng xuất tài khoản
     ********************************** */
    public function logoutAction()
    {
      $refererURL = $_SERVER['HTTP_REFERER'];
      // Remove a session variable
      $this->session->remove($this->identityKey);
      
      return $this->response->redirect($refererURL);
    }
    /* ////////////////////////////// */
    
    /* ********************************
    #rent - ! Đăng tin cho/cần thuê
    ********************************** */
    public function rentCategoryAction()
    {
      $this->view->setVars([]);
    }
    
    public function rentAction()
    {
      if (!defined('UID')) {
        return $this->redirect($this->url->get(['for' => 'frontend:user:login']));
      }
      
      $params = $this->dispatcher->getParams();
      
      $categoryID = FilterUtil::int($params['id']);
      if (empty($categoryID)) {
        return $this->redirect($this->url->get(['frontend:user:rentcategory']));
      }
      
      $this->view->setVars([
        'categoryID'         => $categoryID,
        'arrValidationRules' => UserValid::jsValidationRulesRent(),
      ]);
    }
    
    public function editRentAction()
    {
      $params = $this->dispatcher->getParams();
      
      $refererURL = $this->CDATA['url']['refererURL'];
      
      $intID = FilterUtil::int($params['id']);
      if (empty($intID)) {
        return $this->redirect($refererURL);
      }
      
      /* ## Thông tin item */
      $conditions = [
        'item_id'    => $intID,
        'is_deleted' => 0,
        'is_private' => 0,
        'is_cart'    => 0,
      ];
      
      $instanceItem = new Item();
      $arrItem      = $instanceItem->getDetail($conditions);
      
      if (empty($arrItem)) {
        return $this->redirect($refererURL);
      }
      
      if ($arrItem['user_created_id'] != UID) {
        return $this->redirect($refererURL);
      }
      /* ## */
      
      $this->view->setVars([
        'categoryID'         => $arrItem['category_id'],
        'arrValidationRules' => UserValid::jsValidationRulesRent(),
        'arrItem'            => $arrItem
      ]);
    }
    
    public function rentCompleteAction()
    {
      $this->view->setVars([]);
    }
    /* ////////////////////////////// */
    
    /* ********************************
    #manage-item - ! Quản lý tin đăng
    ********************************** */
    public function manageItemAction()
    {
      if (!defined('UID')) {
        return $this->redirect('/');
      }
      
      $page  = FilterUtil::int($this->dispatcher->getParam('page'), 1);
      $limit = 5;
      
      /* ## Ds sản phẩm */
      $arrColumns = ['item_id', 'item_name', 'item_feature_image', 'item_description', 'host_phone', 'price', 'price_vnd', 'category_id', 'floor_area', 'floor_area_unit', 'city_name', 'district_name', 'created_at', 'address'];
      
      $conditions = [
        'is_deleted'      => 0,
        'is_private'      => 0,
        'is_cart'         => 0,
        'user_created_id' => UID,
      ];
      
      $instanceItem = new Item();
      $arrItems     = $instanceItem->getListLimit($conditions, $page, $limit, 'item_id DESC', $arrColumns);
      
      /* ## ## paging */
      $totalRow = $instanceItem->getTotal($conditions);
      
      $arrRouter = [
        'for' => "frontend:user:manageitempaging",
      ];
      
      $paginationHTML = (new Pagination())
        ->setTotalRows($totalRow)
        ->setrowsPerPage($limit)
        ->setcurrentPage($page)
        ->setQueryString('')
        ->toHtmlTheme1($this->url, $arrRouter);
      
      if (empty($arrItems))
        return $this->redirect('/');
      /* ## ## */
      
      /* ## */
      
      $this->view->setVars([
        'arrItems'       => $arrItems,
        'paginationHTML' => $paginationHTML
      ]);
    }
    /* ////////////////////////////// */
    
  }
