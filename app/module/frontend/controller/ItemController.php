<?php
  
  namespace Module\Frontend\Controller;
  
  use Phalcon\Mvc\View\Simple as View;
  
  use Base\Controller\MyController;
  
  use
    Model\Item;
  
  use
    Util\GeneralUtil,
    Util\FilterUtil,
    Util\Pagination;
  
  use
    Validation\UserValid;
  
  class ItemController extends MyController
  {
    public function initialize()
    {
      parent::initialize();
      
      $this->CDATA['url']['refererURL'] = !empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
      
      $this->view->setVars([
        'CDATA'         => $this->CDATA,
        'arrCategories' => $this->CDATA['common']['categories'],
        'arrCities'     => $this->CDATA['common']['cities'],
        'arrDistricts'  => $this->CDATA['common']['districts']
      ]);
    }
    
    /**
     * @index
     */
    public function indexAction()
    {
      $params = $this->dispatcher->getParams();
      
      $categoryID = FilterUtil::int($params['id']);
      if (empty($categoryID)) {
        return $this->redirect('/');
      }
      
      /* ## Items đăng gần đây */
      $page  = FilterUtil::int($params['page'], 1);
      $limit = 5;
      
      $arrColumns = ['item_id', 'item_name', 'item_feature_image', 'item_description', 'host_phone', 'price', 'price_vnd', 'category_id', 'floor_area', 'floor_area_unit', 'city_name', 'district_name', 'created_at', 'address'];
      
      $conditions = [
        'category_id_list' => [$categoryID],
        'is_deleted'       => 0,
        'is_private'       => 0,
        'is_cart'          => 0,
      ];
      
      $instanceItem = new Item();
      $arrItems     = $instanceItem->getListLimit($conditions, $page, $limit, 'item_id DESC', $arrColumns);
      if (empty($arrItems)) {
        return $this->redirect('/');
      }
      
      /* ## ## paging */
      $totalRow = $instanceItem->getTotal($conditions);
      
      $arrRouter = [
        'for'  => "frontend:item:indexpaging",
        'slug' => GeneralUtil::getSlug($this->CDATA['common']['categories'][ $categoryID ]),
        'id'   => $categoryID,
      ];
      
      $paginationHTML = (new Pagination())
        ->setTotalRows($totalRow)
        ->setrowsPerPage($limit)
        ->setcurrentPage($page)
        ->setQueryString('')
        ->toHtmlTheme1($this->url, $arrRouter);
      /* ## ## */
      
      /* ## */
      
      /* ## Items đăng gần đây */
      $conditions = [
        'is_deleted' => 0,
        'is_private' => 0,
        'is_cart'    => 0,
      ];
      
      $instanceItem   = new Item();
      $arrRecentItems = $instanceItem->getListLimit($conditions, 1, 5, '', $arrColumns);
      /* ## */
      
      $this->view->setVars([
        'arrItems'       => $arrItems,
        'arrRecentItems' => $arrRecentItems,
        'paginationHTML' => $paginationHTML
      ]);
    }
    
    /**
     * @view
     */
    public function viewAction()
    {
      $params = $this->dispatcher->getParams();
      
      $itemID = FilterUtil::int($params['id']);
      if (empty($itemID)) {
        return $this->redirect('/');
      }
      
      /* ## Chi tiết sản phẩm */
      $conditions   = [
        'item_id'    => $itemID,
        'is_deleted' => 0,
        'is_private' => 0,
        'is_cart'    => 0,
      ];
      $instanceItem = new Item();
      $arrItem      = $instanceItem->getDetail($conditions);
      if (empty($arrItem)) {
        return $this->redirect('/');
      }
      /* ## */
      
      /* ## Check slug tránh bị duplicate content */
      $strCurrentSlug = GeneralUtil::getSlug($arrItem['item_name']);
      $strSlug        = FilterUtil::string($params['slug']);
      if ($strCurrentSlug !== $strSlug) {
        return $this->redirect($this->url->get(['for' => 'frontend:item:view', 'slug' => $strCurrentSlug, 'id' => $arrItem['item_id']]), 301);
      }
      /* ## */
      
      /* ## Items theo danh mục */
      $conditions = [
        'not_item_id_list' => [$arrItem['item_id']]
      ];
      $arrColumns = ['item_id', 'item_name', 'item_feature_image', 'item_description', 'host_phone', 'price_vnd', 'category_id', 'floor_area', 'floor_area_unit', 'city_name', 'district_name', 'created_at', 'address'];
//      $instanceItem  = new Item();
//      $arrItemsByCat = $instanceItem->getItemsByCat($conditions, 1, 20, 'item_id DESC', $arrColumns);
      /* ## */
      
      /* ## Items đăng gần đây */
      $conditions = [
        'is_deleted'       => 0,
        'is_private'       => 0,
        'is_cart'          => 0,
        'not_item_id_list' => [$arrItem['item_id']],
      ];
      
      $instanceItem   = new Item();
      $arrRecentItems = $instanceItem->getListLimit($conditions, 1, 5, 'item_id DESC', $arrColumns);
      /* ## */
      
      $this->view->setVars([
        'arrItem'            => $arrItem,
        'arrRecentItems'     => $arrRecentItems,
        'arrValidationRules' => UserValid::jsValidationRulesReg()
      ]);
    }
    
    /**
     * @search
     */
    public function searchAction()
    {
      $params = $this->request->getQuery();
      unset($params['q']);
      
      $searchConditions = $this->_isSearched($params);
      if (empty($searchConditions)) {
        return $this->redirect('/');
      }
      
      $page  = FilterUtil::int($this->dispatcher->getParam('page'), 1);
      $limit = 5;
      
      // ## <editor-fold desc="item list">
      $arrColumns = ['item_id', 'item_name', 'item_feature_image', 'item_description', 'host_phone', 'price', 'price_vnd', 'category_id', 'floor_area', 'floor_area_unit', 'city_name', 'district_name', 'created_at', 'address'];
      
      $searchConditions = $searchConditions + [
          'is_deleted' => 0,
          'is_private' => 0,
          'is_cart'    => 0
        ];
      
      $instanceItem = new Item();
      $arrItems     = $instanceItem->getListLimit($searchConditions, $page, $limit, 'item_id DESC', $arrColumns);
      
      // == ## <editor-fold desc="paging">
      $totalRow = $instanceItem->getTotal($searchConditions);
      
      $arrRouter = [
        'for' => "frontend:item:searchpaging",
      ];
      
      $paginationHTML = (new Pagination())
        ->setTotalRows($totalRow)
        ->setrowsPerPage($limit)
        ->setcurrentPage($page)
        ->setQueryString('?' . http_build_query($params))
        ->toHtmlTheme2($this->url, $arrRouter);
      // == ## </editor-fold>
      
      // ## </editor-fold>
      
      /* ## Items đăng gần đây */
      $conditions = [
        'is_deleted' => 0,
        'is_private' => 0,
        'is_cart'    => 0,
      ];
      
      $instanceItem   = new Item();
      $arrRecentItems = $instanceItem->getListLimit($conditions, 1, 5, '', $arrColumns);
      /* ## */
      
      $this->view->setVars([
        'params'           => $params,
        'searchConditions' => $searchConditions,
        'arrItems'         => $arrItems,
        'totalRow'         => $totalRow,
        'arrRecentItems'   => $arrRecentItems,
        'paginationHTML'   => $paginationHTML
      ]);
    }
    
    private function _isSearched($params)
    {
      if (empty($params))
        return false;
      
      $defaults = [
        'k'                => null,
        'category_id'      => null,
        'city_id'          => null,
        'price_range'      => null,
        'floor_area_range' => null,
      ];
      
      $params = array_merge($defaults, $params);
      
      $conditions = [];
      $keyword    = FilterUtil::string($params['k']);
      if (!empty($keyword)) {
        $conditions['query'] = $keyword;
      }
      
      $categoryID = FilterUtil::int($params['category_id']);
      if (!empty($categoryID)) {
        $conditions['category_id'] = $categoryID;
      }
      
      $cityID = FilterUtil::int($params['city_id']);
      if (!empty($cityID)) {
        $conditions['city_id'] = $cityID;
      }
      
      $priceRange = FilterUtil::string($params['price']);
      if (!empty($priceRange)) {
        list($from, $to) = explode('-', $priceRange);
        
        $conditions['price_from'] = FilterUtil::int($from) * 1000000;
        $conditions['price_to']   = FilterUtil::int($to) * 1000000;
      }
      
      $floorAreaRange = FilterUtil::string($params['floor_area']);
      if (!empty($floorAreaRange)) {
        list($from, $to) = explode('-', $floorAreaRange);
        
        $conditions['floor_area_from'] = FilterUtil::int($from);
        $conditions['floor_area_to']   = FilterUtil::int($to);
      }
      
      return $conditions;
    }
    
    /**
     * @category
     */
    public function categoryAction()
    {
      $params = $this->dispatcher->getParams();
      unset($params['q']);
      
      if (empty($params['district_id'])) {
        return $this->redirect('/');
      }
      
      $page  = FilterUtil::int($this->dispatcher->getParam('page'), 1);
      $limit = 5;
      
      // ## <editor-fold desc="item list">
      $districtID = FilterUtil::int($params['district_id']);
      $arrColumns = ['item_id', 'item_name', 'item_feature_image', 'item_description', 'host_phone', 'price', 'price_vnd', 'category_id', 'floor_area', 'floor_area_unit', 'city_name', 'district_name', 'created_at', 'address'];
      
      $conditions = [
        'is_deleted'  => 0,
        'is_private'  => 0,
        'is_cart'     => 0,
        'district_id' => $districtID
      ];
      
      $instanceItem = new Item();
      $arrItems     = $instanceItem->getListLimit($conditions, $page, $limit, 'item_id DESC', $arrColumns);
      
      // == ## <editor-fold desc="paging">
      $totalRow = $instanceItem->getTotal($searchConditions);
      
      $arrRouter = [
        'for' => "frontend:item:searchpaging",
      ];
      
      $paginationHTML = (new Pagination())
        ->setTotalRows($totalRow)
        ->setrowsPerPage($limit)
        ->setcurrentPage($page)
        ->setQueryString('?' . http_build_query($params))
        ->toHtmlTheme2($this->url, $arrRouter);
      // == ## </editor-fold>
      
      // ## </editor-fold>
      
      /* ## Items đăng gần đây */
      $conditions = [
        'is_deleted' => 0,
        'is_private' => 0,
        'is_cart'    => 0,
      ];
      
      $instanceItem   = new Item();
      $arrRecentItems = $instanceItem->getListLimit($conditions, 1, 5, '', $arrColumns);
      /* ## */
      
      $this->view->setVars([
        'params'           => $params,
        'searchConditions' => $searchConditions,
        'arrItems'         => $arrItems,
        'totalRow'         => $totalRow,
        'arrRecentItems'   => $arrRecentItems,
        'paginationHTML'   => $paginationHTML
      ]);
    }
  }
