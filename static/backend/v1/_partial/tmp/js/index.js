/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(Object.prototype.hasOwnProperty.call(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"index": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push([0,"vendor"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/global/App.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--1!./node_modules/vue-loader/lib??vue-loader-options!./src/vue/partial/global/App.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var $vuePartialPath_global_TheBreadcrumb__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! $vuePartialPath/global/TheBreadcrumb */ "./src/vue/partial/global/TheBreadcrumb.vue");
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'user-app',
  components: {
    'breadcrumb': $vuePartialPath_global_TheBreadcrumb__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {};
  },
  computed: {},
  created: function created() {},
  methods: {}
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/global/TheBreadcrumb.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--1!./node_modules/vue-loader/lib??vue-loader-options!./src/vue/partial/global/TheBreadcrumb.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var $jsBasePath_define__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! $jsBasePath/define */ "./src/js/base/define.js");
/* harmony import */ var $jsBasePath_EventBus__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! $jsBasePath/EventBus */ "./src/js/base/EventBus.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'the-bread-crumb',
  components: {},
  data: function data() {
    return {
      CDATA: CDATA,
      content: "",
      pagingState: {
        page: 1,
        limit: 20,
        totalRecord: 0,
        totalPage: 1
      }
    };
  },
  computed: {},
  watch: {
    $route: function $route(to, from) {
      var pathFrom = from.path;
      var routeName = to.name;
      this.getBreadcrumbContent(routeName, pathFrom);
    }
  },
  beforeCreate: function beforeCreate() {
    var _this = this;

    $jsBasePath_EventBus__WEBPACK_IMPORTED_MODULE_1__["default"].on($jsBasePath_define__WEBPACK_IMPORTED_MODULE_0__["EVENT_BUS_KEY"].PAGING.GET_TOTAL_RECORD, function (payload) {
      var _payload = _objectSpread({}, payload),
          page = _payload.page,
          totalRecord = _payload.totalRecord;

      _this.pagingState.page = page;
      _this.pagingState.totalRecord = totalRecord;
      var tempNotRound = totalRecord / _this.pagingState.limit;
      var temp = Math.round(tempNotRound);
      _this.pagingState.totalPage = tempNotRound > temp ? temp + 1 : temp;
    });
  },
  created: function created() {
    var path = window.location.href.replace(CDATA.url.baseURL, '');
    var resolve = this.$router.resolve(path);
    this.getBreadcrumbContent(resolve.resolved.name);
  },
  methods: {
    getBreadcrumbContent: function getBreadcrumbContent(routeName, pathFrom) {
      this.content = "\n        <a href=\"/backend\" class=\"kt-subheader__breadcrumbs-home\"><i class=\"flaticon2-shelter\"></i></a>\n        <span class=\"kt-subheader__breadcrumbs-separator\"></span>\n      ";

      switch (routeName) {
        case 'user:index':
          this.content += "\n            <span class=\"kt-subheader__breadcrumbs-link\">\n              Ng\u01B0\u1EDDi d\xF9ng </span>\n            <span class=\"kt-subheader__breadcrumbs-separator\"></span>\n            <span class=\"kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active\">Danh s\xE1ch</span>\n          ";
          break;

        case 'user:add':
          this.content += "\n            <span class=\"kt-subheader__breadcrumbs-link\">Ng\u01B0\u1EDDi d\xF9ng </span>\n            <span class=\"kt-subheader__breadcrumbs-separator\"></span>\n            <a\n                href=\"#\"\n                class=\"kt-subheader__breadcrumbs-link\"\n                @click.prevent=\"goRead\"\n            >\n              Danh s\xE1ch\n            </a>\n            <span class=\"kt-subheader__breadcrumbs-separator\"></span>\n            <span class=\"kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active\">T\u1EA1o ng\u01B0\u1EDDi d\xF9ng</span>\n          ";
          break;

        case 'user:edit':
          this.content += "\n            <span class=\"kt-subheader__breadcrumbs-link\">Ng\u01B0\u1EDDi d\xF9ng </span>\n            <span class=\"kt-subheader__breadcrumbs-separator\"></span>\n            <a\n                href=\"#\"\n                class=\"kt-subheader__breadcrumbs-link\"\n                @click.prevent=\"goRead\"\n            >\n              Danh s\xE1ch\n            </a>\n            <span class=\"kt-subheader__breadcrumbs-separator\"></span>\n            <span class=\"kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active\">C\u1EADp nh\u1EADt ng\u01B0\u1EDDi d\xF9ng</span>\n          ";
          break;

        case 'role:index':
          this.content += "\n            <span class=\"kt-subheader__breadcrumbs-link\">Nh\xF3m </span>\n            <span class=\"kt-subheader__breadcrumbs-separator\"></span>\n            <span class=\"kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active\">Danh s\xE1ch</span>\n          ";
          break;

        case 'role:add':
          this.content += "\n            <span class=\"kt-subheader__breadcrumbs-link\">Nh\xF3m </span>\n            <span class=\"kt-subheader__breadcrumbs-separator\"></span>\n            <a\n                href=\"#\"\n                class=\"kt-subheader__breadcrumbs-link\"\n                @click.prevent=\"goRead\"\n            >\n              Danh s\xE1ch\n            </a>\n            <span class=\"kt-subheader__breadcrumbs-separator\"></span>\n            <span class=\"kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active\">T\u1EA1o nh\xF3m</span>\n          ";
          break;

        case 'role:edit':
          this.content += "\n            <span class=\"kt-subheader__breadcrumbs-link\">Nh\xF3m </span>\n            <span class=\"kt-subheader__breadcrumbs-separator\"></span>\n            <a\n                href=\"#\"\n                class=\"kt-subheader__breadcrumbs-link\"\n                @click.prevent=\"goRead\"\n            >\n              Danh s\xE1ch\n            </a>\n            <span class=\"kt-subheader__breadcrumbs-separator\"></span>\n            <span class=\"kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active\">C\u1EADp nh\u1EADt nh\xF3m</span>\n          ";
          break;

        case 'role:grant':
          this.content += "\n            <span class=\"kt-subheader__breadcrumbs-link\">Nh\xF3m </span>\n            <span class=\"kt-subheader__breadcrumbs-separator\"></span>\n            <a\n                href=\"#\"\n                class=\"kt-subheader__breadcrumbs-link\"\n                @click.prevent=\"goRead\"\n            >\n              Danh s\xE1ch\n            </a>\n            <span class=\"kt-subheader__breadcrumbs-separator\"></span>\n            <span class=\"kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active\">Ph\xE2n quy\u1EC1n</span>\n          ";
          break;

        case 'item:index':
          this.content += "\n            <span class=\"kt-subheader__breadcrumbs-link\">S\u1EA3n ph\u1EA9m </span>\n            <span class=\"kt-subheader__breadcrumbs-separator\"></span>\n            <span class=\"kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active\">Danh s\xE1ch</span>\n          ";
          break;

        case 'item:add':
          this.content += "\n            <span class=\"kt-subheader__breadcrumbs-link\">S\u1EA3n ph\u1EA9m </span>\n            <span class=\"kt-subheader__breadcrumbs-separator\"></span>\n            <a\n                href=\"#\"\n                class=\"kt-subheader__breadcrumbs-link\"\n                @click.prevent=\"goRead\"\n            >\n              Danh s\xE1ch\n            </a>\n            <span class=\"kt-subheader__breadcrumbs-separator\"></span>\n            <span class=\"kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active\">\u0110\u0103ng s\u1EA3n ph\u1EA9m</span>\n          ";
          break;

        case 'item:edit':
          this.content += "\n            <span class=\"kt-subheader__breadcrumbs-link\">S\u1EA3n ph\u1EA9m </span>\n            <span class=\"kt-subheader__breadcrumbs-separator\"></span>\n            <a\n                href=\"#\"\n                class=\"kt-subheader__breadcrumbs-link\"\n                @click.prevent=\"goRead\"\n            >\n              Danh s\xE1ch\n            </a>\n            <span class=\"kt-subheader__breadcrumbs-separator\"></span>\n            <span class=\"kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active\">C\u1EADp nh\u1EADt s\u1EA3n ph\u1EA9m</span>\n          ";
          break;

        default:
          this.content += "\n            <span class=\"kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active\">Dashboard</span>\n          ";
          break;
      }
    },
    changePage: function changePage(page) {
      page = page >= 1 ? page : 1;
      $jsBasePath_EventBus__WEBPACK_IMPORTED_MODULE_1__["default"].emit($jsBasePath_define__WEBPACK_IMPORTED_MODULE_0__["EVENT_BUS_KEY"].PAGING.CHANGE_PAGE, {
        page: page
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/index/IndexSection.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--1!./node_modules/vue-loader/lib??vue-loader-options!./src/vue/partial/index/IndexSection.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var $jsCommonPath_util_GeneralUtil__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! $jsCommonPath/util/GeneralUtil */ "./src/js/common/util/GeneralUtil.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'index-section',
  components: {},
  data: function data() {
    return {
      CDATA: CDATA,
      locOption: {
        isReading: false
      }
    };
  },
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapState"])(['option'])),
  created: function created() {
    console.log('xxxxx');
  },
  mounted: function mounted() {
    Object($jsCommonPath_util_GeneralUtil__WEBPACK_IMPORTED_MODULE_1__["_showContainer"])();
  },
  methods: {}
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/global/App.vue?vue&type=template&id=f7c6b9b6&":
/*!*****************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./src/vue/partial/global/App.vue?vue&type=template&id=f7c6b9b6& ***!
  \*****************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass:
        "kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor"
    },
    [_c("breadcrumb"), _vm._v(" "), _c("router-view")],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/global/TheBreadcrumb.vue?vue&type=template&id=565653d8&":
/*!***************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./src/vue/partial/global/TheBreadcrumb.vue?vue&type=template&id=565653d8& ***!
  \***************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass: "kt-subheader   kt-grid__item",
      attrs: { id: "kt_subheader" }
    },
    [
      _c("div", { staticClass: "kt-container  kt-container--fluid " }, [
        _c("div", { staticClass: "kt-subheader__main" }, [
          _c("span", { staticClass: "kt-subheader__separator kt-hidden" }),
          _vm._v(" "),
          _c(
            "div",
            {
              staticClass: "kt-subheader__breadcrumbs",
              domProps: { innerHTML: _vm._s(_vm.content) }
            },
            [_vm._v("\n        " + _vm._s(_vm.content) + "\n      ")]
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "kt-subheader__toolbar" }),
        _vm._v(" "),
        _c("div", { staticClass: "kt-header__topbar" }, [
          _c("div", { staticClass: "kt-pagination kt-pagination--brand" }, [
            _c("div", { staticClass: "kt-pagination__toolbar _m-r-10" }, [
              _c("span", { staticClass: "pagination__desc" }, [
                _vm._v(
                  "\n              " +
                    _vm._s(
                      (_vm.pagingState.page - 1) * _vm.pagingState.limit +
                        " - " +
                        (_vm.pagingState.page * _vm.pagingState.limit <
                        _vm.pagingState.totalRecord
                          ? _vm.pagingState.page * _vm.pagingState.limit
                          : _vm.pagingState.totalRecord)
                    ) +
                    " trong số " +
                    _vm._s(_vm.pagingState.totalRecord) +
                    "\n          "
                )
              ])
            ]),
            _vm._v(" "),
            _c("ul", { staticClass: "kt-pagination__links" }, [
              _c("li", { staticClass: "kt-pagination__link--first" }, [
                _c(
                  "a",
                  {
                    class: { "is-disabled": _vm.pagingState.page == 1 },
                    attrs: { href: "#" },
                    on: {
                      click: function($event) {
                        $event.preventDefault()
                        return _vm.changePage(1)
                      }
                    }
                  },
                  [
                    _c("i", {
                      staticClass: "fa fa-angle-double-left kt-font-brand"
                    })
                  ]
                )
              ]),
              _vm._v(" "),
              _c("li", { staticClass: "kt-pagination__link--prev" }, [
                _c(
                  "a",
                  {
                    class: { "is-disabled": _vm.pagingState.page == 1 },
                    attrs: { href: "#" },
                    on: {
                      click: function($event) {
                        $event.preventDefault()
                        return _vm.changePage(_vm.pagingState.page - 1)
                      }
                    }
                  },
                  [_c("i", { staticClass: "fa fa-angle-left kt-font-brand" })]
                )
              ]),
              _vm._v(" "),
              _c("li", { staticClass: "kt-pagination__link--next" }, [
                _c(
                  "a",
                  {
                    class: {
                      "is-disabled":
                        _vm.pagingState.totalRecord < _vm.pagingState.limit ||
                        _vm.pagingState.page == _vm.pagingState.totalPage
                    },
                    attrs: { href: "#" },
                    on: {
                      click: function($event) {
                        $event.preventDefault()
                        return _vm.changePage(_vm.pagingState.page + 1)
                      }
                    }
                  },
                  [_c("i", { staticClass: "fa fa-angle-right kt-font-brand" })]
                )
              ]),
              _vm._v(" "),
              _c("li", { staticClass: "kt-pagination__link--last" }, [
                _c(
                  "a",
                  {
                    class: {
                      "is-disabled":
                        _vm.pagingState.totalRecord < _vm.pagingState.limit ||
                        _vm.pagingState.page == _vm.pagingState.totalPage
                    },
                    attrs: { href: "#" },
                    on: {
                      click: function($event) {
                        $event.preventDefault()
                        return _vm.changePage(_vm.pagingState.totalPage)
                      }
                    }
                  },
                  [
                    _c("i", {
                      staticClass: "fa fa-angle-double-right kt-font-brand"
                    })
                  ]
                )
              ])
            ])
          ]),
          _vm._v(" "),
          _c(
            "div",
            {
              staticClass: "kt-header__topbar-item kt-header__topbar-item--user"
            },
            [
              _c(
                "div",
                {
                  staticClass: "kt-header__topbar-wrapper",
                  attrs: { "data-toggle": "dropdown", "data-offset": "0px,0px" }
                },
                [
                  _c("div", { staticClass: "kt-header__topbar-user" }, [
                    _c(
                      "span",
                      {
                        staticClass:
                          "kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold",
                        staticStyle: { width: "100%", padding: "10px" }
                      },
                      [_vm._v(_vm._s(_vm.CDATA.login.fullName))]
                    )
                  ])
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass:
                    "dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl"
                },
                [
                  _c("div", { staticClass: "kt-notification" }, [
                    _c(
                      "div",
                      {
                        staticClass: "kt-notification__custom kt-space-between"
                      },
                      [
                        _c(
                          "a",
                          {
                            staticClass:
                              "btn btn-label btn-label-brand btn-sm btn-bold",
                            attrs: { href: _vm.CDATA.url.logoutURL }
                          },
                          [_vm._v("Sign Out")]
                        )
                      ]
                    )
                  ])
                ]
              )
            ]
          )
        ])
      ])
    ]
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/index/IndexSection.vue?vue&type=template&id=e824a800&":
/*!*************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./src/vue/partial/index/IndexSection.vue?vue&type=template&id=e824a800& ***!
  \*************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass:
        "kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor"
    },
    [
      _c(
        "div",
        {
          staticClass: "kt-subheader   kt-grid__item",
          attrs: { id: "kt_subheader" }
        },
        [
          _c("div", { staticClass: "kt-container  kt-container--fluid " }, [
            _vm._m(0),
            _vm._v(" "),
            _c("div", { staticClass: "kt-subheader__toolbar" }),
            _vm._v(" "),
            _c("div", { staticClass: "kt-header__topbar" }, [
              _c(
                "div",
                {
                  staticClass:
                    "kt-header__topbar-item kt-header__topbar-item--user"
                },
                [
                  _c(
                    "div",
                    {
                      staticClass: "kt-header__topbar-wrapper",
                      attrs: {
                        "data-toggle": "dropdown",
                        "data-offset": "0px,0px"
                      }
                    },
                    [
                      _c("div", { staticClass: "kt-header__topbar-user" }, [
                        _c(
                          "span",
                          {
                            staticClass:
                              "kt-header__topbar-welcome kt-hidden-mobile"
                          },
                          [_vm._v("Hi,")]
                        ),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            staticClass:
                              "kt-header__topbar-username kt-hidden-mobile"
                          },
                          [_vm._v(_vm._s(_vm.CDATA.login.fullName))]
                        ),
                        _vm._v(" "),
                        _c("img", {
                          staticClass: "kt-hidden",
                          attrs: { alt: "Pic", src: "" }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            staticClass:
                              "kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold"
                          },
                          [_vm._v("S")]
                        )
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass:
                        "dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl"
                    },
                    [
                      _c("div", { staticClass: "kt-notification" }, [
                        _c(
                          "div",
                          {
                            staticClass:
                              "kt-notification__custom kt-space-between"
                          },
                          [
                            _c(
                              "a",
                              {
                                staticClass:
                                  "btn btn-label btn-label-brand btn-sm btn-bold",
                                attrs: { href: _vm.CDATA.url.logoutURL }
                              },
                              [_vm._v("Sign Out")]
                            )
                          ]
                        )
                      ])
                    ]
                  )
                ]
              )
            ])
          ])
        ]
      )
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "kt-subheader__main" }, [
      _c("span", { staticClass: "kt-subheader__separator kt-hidden" }),
      _vm._v(" "),
      _c("div", { staticClass: "kt-subheader__breadcrumbs" }, [
        _c(
          "a",
          {
            staticClass: "kt-subheader__breadcrumbs-home",
            attrs: { href: "/backend" }
          },
          [_c("i", { staticClass: "flaticon2-shelter" })]
        ),
        _vm._v(" "),
        _c("span", { staticClass: "kt-subheader__breadcrumbs-separator" }),
        _vm._v(" "),
        _c("span", { staticClass: "kt-subheader__breadcrumbs-link" }, [
          _vm._v("\n              Dashboard\n            ")
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./src/js/partial/index/index.js":
/*!***************************************!*\
  !*** ./src/js/partial/index/index.js ***!
  \***************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.runtime.esm.js");
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var vue_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vue-router */ "./node_modules/vue-router/dist/vue-router.esm.js");
/* harmony import */ var $vuePartialPath_global_App__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! $vuePartialPath/global/App */ "./src/vue/partial/global/App.vue");
/* harmony import */ var $vuePartialPath_index_IndexSection__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! $vuePartialPath/index/IndexSection */ "./src/vue/partial/index/IndexSection.vue");





vue__WEBPACK_IMPORTED_MODULE_0__["default"].use(vuex__WEBPACK_IMPORTED_MODULE_1__["default"]);
vue__WEBPACK_IMPORTED_MODULE_0__["default"].use(vue_router__WEBPACK_IMPORTED_MODULE_2__["default"]); // const store = new Vuex.Store(Store)

var router = new vue_router__WEBPACK_IMPORTED_MODULE_2__["default"]({
  mode: 'history',
  routes: [{
    name: 'index',
    path: '/backend*',
    component: $vuePartialPath_index_IndexSection__WEBPACK_IMPORTED_MODULE_4__["default"]
  }]
});

var Object = function () {
  var _render = function _render() {
    var $el = $('#content-section');
    new vue__WEBPACK_IMPORTED_MODULE_0__["default"]({
      router: router,
      // store,
      render: function render(h) {
        return h($vuePartialPath_global_App__WEBPACK_IMPORTED_MODULE_3__["default"]);
      }
    }).$mount($el[0]);
  };

  return {
    init: function init() {
      _render();
    }
  };
}();

document.addEventListener("DOMContentLoaded", function () {
  Object.init();
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js?fe57")))

/***/ }),

/***/ "./src/vue/partial/global/App.vue":
/*!****************************************!*\
  !*** ./src/vue/partial/global/App.vue ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _App_vue_vue_type_template_id_f7c6b9b6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./App.vue?vue&type=template&id=f7c6b9b6& */ "./src/vue/partial/global/App.vue?vue&type=template&id=f7c6b9b6&");
/* harmony import */ var _App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./App.vue?vue&type=script&lang=js& */ "./src/vue/partial/global/App.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _App_vue_vue_type_template_id_f7c6b9b6___WEBPACK_IMPORTED_MODULE_0__["render"],
  _App_vue_vue_type_template_id_f7c6b9b6___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "src/vue/partial/global/App.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./src/vue/partial/global/App.vue?vue&type=script&lang=js&":
/*!*****************************************************************!*\
  !*** ./src/vue/partial/global/App.vue?vue&type=script&lang=js& ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_1_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--1!../../../../node_modules/vue-loader/lib??vue-loader-options!./App.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/global/App.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_1_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./src/vue/partial/global/App.vue?vue&type=template&id=f7c6b9b6&":
/*!***********************************************************************!*\
  !*** ./src/vue/partial/global/App.vue?vue&type=template&id=f7c6b9b6& ***!
  \***********************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_template_id_f7c6b9b6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./App.vue?vue&type=template&id=f7c6b9b6& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/global/App.vue?vue&type=template&id=f7c6b9b6&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_template_id_f7c6b9b6___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_template_id_f7c6b9b6___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./src/vue/partial/global/TheBreadcrumb.vue":
/*!**************************************************!*\
  !*** ./src/vue/partial/global/TheBreadcrumb.vue ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _TheBreadcrumb_vue_vue_type_template_id_565653d8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./TheBreadcrumb.vue?vue&type=template&id=565653d8& */ "./src/vue/partial/global/TheBreadcrumb.vue?vue&type=template&id=565653d8&");
/* harmony import */ var _TheBreadcrumb_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./TheBreadcrumb.vue?vue&type=script&lang=js& */ "./src/vue/partial/global/TheBreadcrumb.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _TheBreadcrumb_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _TheBreadcrumb_vue_vue_type_template_id_565653d8___WEBPACK_IMPORTED_MODULE_0__["render"],
  _TheBreadcrumb_vue_vue_type_template_id_565653d8___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "src/vue/partial/global/TheBreadcrumb.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./src/vue/partial/global/TheBreadcrumb.vue?vue&type=script&lang=js&":
/*!***************************************************************************!*\
  !*** ./src/vue/partial/global/TheBreadcrumb.vue?vue&type=script&lang=js& ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_1_node_modules_vue_loader_lib_index_js_vue_loader_options_TheBreadcrumb_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--1!../../../../node_modules/vue-loader/lib??vue-loader-options!./TheBreadcrumb.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/global/TheBreadcrumb.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_1_node_modules_vue_loader_lib_index_js_vue_loader_options_TheBreadcrumb_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./src/vue/partial/global/TheBreadcrumb.vue?vue&type=template&id=565653d8&":
/*!*********************************************************************************!*\
  !*** ./src/vue/partial/global/TheBreadcrumb.vue?vue&type=template&id=565653d8& ***!
  \*********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TheBreadcrumb_vue_vue_type_template_id_565653d8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./TheBreadcrumb.vue?vue&type=template&id=565653d8& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/global/TheBreadcrumb.vue?vue&type=template&id=565653d8&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TheBreadcrumb_vue_vue_type_template_id_565653d8___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TheBreadcrumb_vue_vue_type_template_id_565653d8___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./src/vue/partial/index/IndexSection.vue":
/*!************************************************!*\
  !*** ./src/vue/partial/index/IndexSection.vue ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _IndexSection_vue_vue_type_template_id_e824a800___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./IndexSection.vue?vue&type=template&id=e824a800& */ "./src/vue/partial/index/IndexSection.vue?vue&type=template&id=e824a800&");
/* harmony import */ var _IndexSection_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./IndexSection.vue?vue&type=script&lang=js& */ "./src/vue/partial/index/IndexSection.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _IndexSection_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _IndexSection_vue_vue_type_template_id_e824a800___WEBPACK_IMPORTED_MODULE_0__["render"],
  _IndexSection_vue_vue_type_template_id_e824a800___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "src/vue/partial/index/IndexSection.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./src/vue/partial/index/IndexSection.vue?vue&type=script&lang=js&":
/*!*************************************************************************!*\
  !*** ./src/vue/partial/index/IndexSection.vue?vue&type=script&lang=js& ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_1_node_modules_vue_loader_lib_index_js_vue_loader_options_IndexSection_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--1!../../../../node_modules/vue-loader/lib??vue-loader-options!./IndexSection.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/index/IndexSection.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_1_node_modules_vue_loader_lib_index_js_vue_loader_options_IndexSection_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./src/vue/partial/index/IndexSection.vue?vue&type=template&id=e824a800&":
/*!*******************************************************************************!*\
  !*** ./src/vue/partial/index/IndexSection.vue?vue&type=template&id=e824a800& ***!
  \*******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_IndexSection_vue_vue_type_template_id_e824a800___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./IndexSection.vue?vue&type=template&id=e824a800& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/index/IndexSection.vue?vue&type=template&id=e824a800&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_IndexSection_vue_vue_type_template_id_e824a800___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_IndexSection_vue_vue_type_template_id_e824a800___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ 0:
/*!****************************************************************!*\
  !*** multi ./src/js/vendor.js ./src/js/partial/index/index.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! D:\Project\share\outsouce_thuenha\html\static\backend\v1\src/js/vendor.js */"./src/js/vendor.js");
module.exports = __webpack_require__(/*! D:\Project\share\outsouce_thuenha\html\static\backend\v1\src/js/partial/index/index.js */"./src/js/partial/index/index.js");


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vL3NyYy92dWUvcGFydGlhbC9nbG9iYWwvQXBwLnZ1ZSIsIndlYnBhY2s6Ly8vc3JjL3Z1ZS9wYXJ0aWFsL2dsb2JhbC9UaGVCcmVhZGNydW1iLnZ1ZSIsIndlYnBhY2s6Ly8vc3JjL3Z1ZS9wYXJ0aWFsL2luZGV4L0luZGV4U2VjdGlvbi52dWUiLCJ3ZWJwYWNrOi8vLy4vc3JjL3Z1ZS9wYXJ0aWFsL2dsb2JhbC9BcHAudnVlP2Y5YWEiLCJ3ZWJwYWNrOi8vLy4vc3JjL3Z1ZS9wYXJ0aWFsL2dsb2JhbC9UaGVCcmVhZGNydW1iLnZ1ZT9mNDcwIiwid2VicGFjazovLy8uL3NyYy92dWUvcGFydGlhbC9pbmRleC9JbmRleFNlY3Rpb24udnVlPzFjNTQiLCJ3ZWJwYWNrOi8vLy4vc3JjL2pzL3BhcnRpYWwvaW5kZXgvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3Z1ZS9wYXJ0aWFsL2dsb2JhbC9BcHAudnVlIiwid2VicGFjazovLy8uL3NyYy92dWUvcGFydGlhbC9nbG9iYWwvQXBwLnZ1ZT8yZDVlIiwid2VicGFjazovLy8uL3NyYy92dWUvcGFydGlhbC9nbG9iYWwvQXBwLnZ1ZT8yMjA1Iiwid2VicGFjazovLy8uL3NyYy92dWUvcGFydGlhbC9nbG9iYWwvVGhlQnJlYWRjcnVtYi52dWUiLCJ3ZWJwYWNrOi8vLy4vc3JjL3Z1ZS9wYXJ0aWFsL2dsb2JhbC9UaGVCcmVhZGNydW1iLnZ1ZT9iNjBmIiwid2VicGFjazovLy8uL3NyYy92dWUvcGFydGlhbC9nbG9iYWwvVGhlQnJlYWRjcnVtYi52dWU/MzI1MiIsIndlYnBhY2s6Ly8vLi9zcmMvdnVlL3BhcnRpYWwvaW5kZXgvSW5kZXhTZWN0aW9uLnZ1ZSIsIndlYnBhY2s6Ly8vLi9zcmMvdnVlL3BhcnRpYWwvaW5kZXgvSW5kZXhTZWN0aW9uLnZ1ZT8xNDllIiwid2VicGFjazovLy8uL3NyYy92dWUvcGFydGlhbC9pbmRleC9JbmRleFNlY3Rpb24udnVlPzZmOTkiXSwibmFtZXMiOlsiVnVlIiwidXNlIiwiVnVleCIsIlZ1ZVJvdXRlciIsInJvdXRlciIsIm1vZGUiLCJyb3V0ZXMiLCJuYW1lIiwicGF0aCIsImNvbXBvbmVudCIsIkluZGV4U2VjdGlvbiIsIk9iamVjdCIsIl9yZW5kZXIiLCIkZWwiLCIkIiwicmVuZGVyIiwiaCIsIkFwcCIsIiRtb3VudCIsImluaXQiLCJkb2N1bWVudCIsImFkZEV2ZW50TGlzdGVuZXIiXSwibWFwcGluZ3MiOiI7UUFBQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLFFBQVEsb0JBQW9CO1FBQzVCO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EsaUJBQWlCLDRCQUE0QjtRQUM3QztRQUNBO1FBQ0Esa0JBQWtCLDJCQUEyQjtRQUM3QztRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBOzs7UUFHQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMENBQTBDLGdDQUFnQztRQUMxRTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLHdEQUF3RCxrQkFBa0I7UUFDMUU7UUFDQSxpREFBaUQsY0FBYztRQUMvRDs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EseUNBQXlDLGlDQUFpQztRQUMxRSxnSEFBZ0gsbUJBQW1CLEVBQUU7UUFDckk7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwyQkFBMkIsMEJBQTBCLEVBQUU7UUFDdkQsaUNBQWlDLGVBQWU7UUFDaEQ7UUFDQTtRQUNBOztRQUVBO1FBQ0Esc0RBQXNELCtEQUErRDs7UUFFckg7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLGdCQUFnQix1QkFBdUI7UUFDdkM7OztRQUdBO1FBQ0E7UUFDQTtRQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDL0lBO0FBRUE7QUFDQSxrQkFEQTtBQUVBO0FBQ0E7QUFEQSxHQUZBO0FBS0EsTUFMQSxrQkFLQTtBQUNBO0FBQ0EsR0FQQTtBQVFBLGNBUkE7QUFTQSxTQVRBLHFCQVNBLENBQ0EsQ0FWQTtBQVdBO0FBWEEsRzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3NFQTtBQUNBO0FBRUE7QUFDQSx5QkFEQTtBQUVBLGdCQUZBO0FBR0EsTUFIQSxrQkFHQTtBQUNBO0FBQ0Esa0JBREE7QUFFQSxpQkFGQTtBQUdBO0FBQ0EsZUFEQTtBQUVBLGlCQUZBO0FBR0Esc0JBSEE7QUFJQTtBQUpBO0FBSEE7QUFVQSxHQWRBO0FBZUEsY0FmQTtBQWdCQTtBQUNBLFVBREEsa0JBQ0EsRUFEQSxFQUNBLElBREEsRUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBTkEsR0FoQkE7QUF3QkEsY0F4QkEsMEJBd0JBO0FBQUE7O0FBQ0E7QUFBQSx1Q0FDQSxPQURBO0FBQUEsVUFDQSxJQURBLFlBQ0EsSUFEQTtBQUFBLFVBQ0EsV0FEQSxZQUNBLFdBREE7O0FBR0E7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBLEtBVkE7QUFXQSxHQXBDQTtBQXFDQSxTQXJDQSxxQkFxQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQSxHQTFDQTtBQTJDQTtBQUNBLHdCQURBLGdDQUNBLFNBREEsRUFDQSxRQURBLEVBQ0E7QUFDQTs7QUFLQTtBQUNBO0FBQ0E7QUFNQTs7QUFDQTtBQUNBO0FBYUE7O0FBQ0E7QUFDQTtBQWFBOztBQUNBO0FBQ0E7QUFLQTs7QUFDQTtBQUNBO0FBYUE7O0FBQ0E7QUFDQTtBQWFBOztBQUNBO0FBQ0E7QUFhQTs7QUFDQTtBQUNBO0FBS0E7O0FBQ0E7QUFDQTtBQWFBOztBQUNBO0FBQ0E7QUFhQTs7QUFDQTtBQUNBO0FBR0E7QUFwSUE7QUFzSUEsS0E3SUE7QUE4SUEsY0E5SUEsc0JBOElBLElBOUlBLEVBOElBO0FBQ0E7QUFFQTtBQUFBO0FBQUE7QUFDQTtBQWxKQTtBQTNDQSxHOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3pDQTtBQUVBO0FBRUE7QUFDQSx1QkFEQTtBQUVBLGdCQUZBO0FBS0EsTUFMQSxrQkFLQTtBQUNBO0FBQ0Esa0JBREE7QUFFQTtBQUNBO0FBREE7QUFGQTtBQU1BLEdBWkE7QUFhQSw4QkFDQSxpRUFEQSxDQWJBO0FBZ0JBLFNBaEJBLHFCQWdCQTtBQUNBO0FBQ0EsR0FsQkE7QUFtQkEsU0FuQkEscUJBbUJBO0FBQ0E7QUFDQSxHQXJCQTtBQXNCQTtBQXRCQSxHOzs7Ozs7Ozs7Ozs7QUM5Q0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7QUNmQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsY0FBYztBQUNkLEtBQUs7QUFDTDtBQUNBLGlCQUFpQixvREFBb0Q7QUFDckUsbUJBQW1CLG9DQUFvQztBQUN2RCxzQkFBc0IsbURBQW1EO0FBQ3pFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx5QkFBeUI7QUFDekIsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CLHVDQUF1QztBQUMxRDtBQUNBLG1CQUFtQixtQ0FBbUM7QUFDdEQscUJBQXFCLG9EQUFvRDtBQUN6RSx1QkFBdUIsZ0RBQWdEO0FBQ3ZFLDBCQUEwQixrQ0FBa0M7QUFDNUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNCQUFzQixzQ0FBc0M7QUFDNUQsd0JBQXdCLDRDQUE0QztBQUNwRTtBQUNBO0FBQ0E7QUFDQSw0QkFBNEIsMkNBQTJDO0FBQ3ZFLDRCQUE0QixZQUFZO0FBQ3hDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQjtBQUNuQjtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckI7QUFDQTtBQUNBO0FBQ0E7QUFDQSx3QkFBd0IsMkNBQTJDO0FBQ25FO0FBQ0E7QUFDQTtBQUNBLDRCQUE0QiwyQ0FBMkM7QUFDdkUsNEJBQTRCLFlBQVk7QUFDeEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CO0FBQ25CLDRCQUE0QixnREFBZ0Q7QUFDNUU7QUFDQTtBQUNBO0FBQ0Esd0JBQXdCLDJDQUEyQztBQUNuRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQiw0QkFBNEIsWUFBWTtBQUN4QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUI7QUFDbkIsNEJBQTRCLGlEQUFpRDtBQUM3RTtBQUNBO0FBQ0E7QUFDQSx3QkFBd0IsMkNBQTJDO0FBQ25FO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCLDRCQUE0QixZQUFZO0FBQ3hDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQjtBQUNuQjtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDBCQUEwQjtBQUMxQixpQkFBaUI7QUFDakI7QUFDQSw2QkFBNkIsd0NBQXdDO0FBQ3JFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxzQ0FBc0M7QUFDdEMsdUJBQXVCO0FBQ3ZCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQSw2QkFBNkIsaUNBQWlDO0FBQzlEO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdUJBQXVCO0FBQ3ZCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG9DQUFvQztBQUNwQywyQkFBMkI7QUFDM0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7O0FDdE1BO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esa0JBQWtCO0FBQ2xCLFNBQVM7QUFDVDtBQUNBLHFCQUFxQixvREFBb0Q7QUFDekU7QUFDQTtBQUNBLHVCQUF1Qix1Q0FBdUM7QUFDOUQ7QUFDQSx1QkFBdUIsbUNBQW1DO0FBQzFEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCO0FBQ0EsaUNBQWlDLHdDQUF3QztBQUN6RTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMkJBQTJCO0FBQzNCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwyQkFBMkI7QUFDM0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGtDQUFrQztBQUNsQyx5QkFBeUI7QUFDekI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMkJBQTJCO0FBQzNCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckI7QUFDQSxpQ0FBaUMsaUNBQWlDO0FBQ2xFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwyQkFBMkI7QUFDM0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esd0NBQXdDO0FBQ3hDLCtCQUErQjtBQUMvQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxzQkFBc0Isb0NBQW9DO0FBQzFELGtCQUFrQixtREFBbUQ7QUFDckU7QUFDQSxpQkFBaUIsMkNBQTJDO0FBQzVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esb0JBQW9CO0FBQ3BCLFdBQVc7QUFDWCxvQkFBb0IsbUNBQW1DO0FBQ3ZEO0FBQ0E7QUFDQSxvQkFBb0IscURBQXFEO0FBQ3pFO0FBQ0Esb0JBQW9CLGdEQUFnRDtBQUNwRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7QUM5SUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUVBQSwyQ0FBRyxDQUFDQyxHQUFKLENBQVFDLDRDQUFSO0FBQ0FGLDJDQUFHLENBQUNDLEdBQUosQ0FBUUUsa0RBQVIsRSxDQUVBOztBQUNBLElBQU1DLE1BQU0sR0FBRyxJQUFJRCxrREFBSixDQUFjO0FBQzNCRSxNQUFJLEVBQUUsU0FEcUI7QUFFM0JDLFFBQU0sRUFBRSxDQUNOO0FBQUVDLFFBQUksRUFBRSxPQUFSO0FBQWlCQyxRQUFJLEVBQUUsV0FBdkI7QUFBb0NDLGFBQVMsRUFBRUMsMEVBQVlBO0FBQTNELEdBRE07QUFGbUIsQ0FBZCxDQUFmOztBQU9BLElBQU1DLE1BQU0sR0FBSSxZQUFNO0FBQ3BCLE1BQU1DLE9BQU8sR0FBRyxTQUFWQSxPQUFVLEdBQU07QUFDcEIsUUFBSUMsR0FBRyxHQUFHQyxDQUFDLENBQUMsa0JBQUQsQ0FBWDtBQUVBLFFBQUlkLDJDQUFKLENBQVE7QUFDTkksWUFBTSxFQUFOQSxNQURNO0FBRU47QUFDQVcsWUFBTSxFQUFFLGdCQUFBQyxDQUFDO0FBQUEsZUFBSUEsQ0FBQyxDQUFDQyxrRUFBRCxDQUFMO0FBQUE7QUFISCxLQUFSLEVBSUdDLE1BSkgsQ0FJVUwsR0FBRyxDQUFFLENBQUYsQ0FKYjtBQUtELEdBUkQ7O0FBVUEsU0FBTztBQUNMTSxRQURLLGtCQUNFO0FBQ0xQLGFBQU87QUFDUjtBQUhJLEdBQVA7QUFLRCxDQWhCYyxFQUFmOztBQWtCQVEsUUFBUSxDQUFDQyxnQkFBVCxDQUEwQixrQkFBMUIsRUFBOEMsWUFBWTtBQUN4RFYsUUFBTSxDQUFDUSxJQUFQO0FBQ0QsQ0FGRCxFOzs7Ozs7Ozs7Ozs7O0FDcENBO0FBQUE7QUFBQTtBQUFBO0FBQWtGO0FBQzNCO0FBQ0w7OztBQUdsRDtBQUNnRztBQUNoRyxnQkFBZ0IsMkdBQVU7QUFDMUIsRUFBRSx5RUFBTTtBQUNSLEVBQUUsOEVBQU07QUFDUixFQUFFLHVGQUFlO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0EsSUFBSSxLQUFVLEVBQUUsWUFpQmY7QUFDRDtBQUNlLGdGOzs7Ozs7Ozs7Ozs7QUN0Q2Y7QUFBQTtBQUFBLHdDQUF1TCxDQUFnQiw2T0FBRyxFQUFDLEM7Ozs7Ozs7Ozs7OztBQ0EzTTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7Ozs7Ozs7Ozs7QUNBQTtBQUFBO0FBQUE7QUFBQTtBQUE0RjtBQUMzQjtBQUNMOzs7QUFHNUQ7QUFDZ0c7QUFDaEcsZ0JBQWdCLDJHQUFVO0FBQzFCLEVBQUUsbUZBQU07QUFDUixFQUFFLHdGQUFNO0FBQ1IsRUFBRSxpR0FBZTtBQUNqQjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBLElBQUksS0FBVSxFQUFFLFlBaUJmO0FBQ0Q7QUFDZSxnRjs7Ozs7Ozs7Ozs7O0FDdENmO0FBQUE7QUFBQSx3Q0FBaU0sQ0FBZ0IsdVBBQUcsRUFBQyxDOzs7Ozs7Ozs7Ozs7QUNBck47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOzs7Ozs7Ozs7Ozs7O0FDQUE7QUFBQTtBQUFBO0FBQUE7QUFBMkY7QUFDM0I7QUFDTDs7O0FBRzNEO0FBQ2dHO0FBQ2hHLGdCQUFnQiwyR0FBVTtBQUMxQixFQUFFLGtGQUFNO0FBQ1IsRUFBRSx1RkFBTTtBQUNSLEVBQUUsZ0dBQWU7QUFDakI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQSxJQUFJLEtBQVUsRUFBRSxZQWlCZjtBQUNEO0FBQ2UsZ0Y7Ozs7Ozs7Ozs7OztBQ3RDZjtBQUFBO0FBQUEsd0NBQWdNLENBQWdCLHNQQUFHLEVBQUMsQzs7Ozs7Ozs7Ozs7O0FDQXBOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSIsImZpbGUiOiJqcy9pbmRleC5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIGluc3RhbGwgYSBKU09OUCBjYWxsYmFjayBmb3IgY2h1bmsgbG9hZGluZ1xuIFx0ZnVuY3Rpb24gd2VicGFja0pzb25wQ2FsbGJhY2soZGF0YSkge1xuIFx0XHR2YXIgY2h1bmtJZHMgPSBkYXRhWzBdO1xuIFx0XHR2YXIgbW9yZU1vZHVsZXMgPSBkYXRhWzFdO1xuIFx0XHR2YXIgZXhlY3V0ZU1vZHVsZXMgPSBkYXRhWzJdO1xuXG4gXHRcdC8vIGFkZCBcIm1vcmVNb2R1bGVzXCIgdG8gdGhlIG1vZHVsZXMgb2JqZWN0LFxuIFx0XHQvLyB0aGVuIGZsYWcgYWxsIFwiY2h1bmtJZHNcIiBhcyBsb2FkZWQgYW5kIGZpcmUgY2FsbGJhY2tcbiBcdFx0dmFyIG1vZHVsZUlkLCBjaHVua0lkLCBpID0gMCwgcmVzb2x2ZXMgPSBbXTtcbiBcdFx0Zm9yKDtpIDwgY2h1bmtJZHMubGVuZ3RoOyBpKyspIHtcbiBcdFx0XHRjaHVua0lkID0gY2h1bmtJZHNbaV07XG4gXHRcdFx0aWYoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKGluc3RhbGxlZENodW5rcywgY2h1bmtJZCkgJiYgaW5zdGFsbGVkQ2h1bmtzW2NodW5rSWRdKSB7XG4gXHRcdFx0XHRyZXNvbHZlcy5wdXNoKGluc3RhbGxlZENodW5rc1tjaHVua0lkXVswXSk7XG4gXHRcdFx0fVxuIFx0XHRcdGluc3RhbGxlZENodW5rc1tjaHVua0lkXSA9IDA7XG4gXHRcdH1cbiBcdFx0Zm9yKG1vZHVsZUlkIGluIG1vcmVNb2R1bGVzKSB7XG4gXHRcdFx0aWYoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG1vcmVNb2R1bGVzLCBtb2R1bGVJZCkpIHtcbiBcdFx0XHRcdG1vZHVsZXNbbW9kdWxlSWRdID0gbW9yZU1vZHVsZXNbbW9kdWxlSWRdO1xuIFx0XHRcdH1cbiBcdFx0fVxuIFx0XHRpZihwYXJlbnRKc29ucEZ1bmN0aW9uKSBwYXJlbnRKc29ucEZ1bmN0aW9uKGRhdGEpO1xuXG4gXHRcdHdoaWxlKHJlc29sdmVzLmxlbmd0aCkge1xuIFx0XHRcdHJlc29sdmVzLnNoaWZ0KCkoKTtcbiBcdFx0fVxuXG4gXHRcdC8vIGFkZCBlbnRyeSBtb2R1bGVzIGZyb20gbG9hZGVkIGNodW5rIHRvIGRlZmVycmVkIGxpc3RcbiBcdFx0ZGVmZXJyZWRNb2R1bGVzLnB1c2guYXBwbHkoZGVmZXJyZWRNb2R1bGVzLCBleGVjdXRlTW9kdWxlcyB8fCBbXSk7XG5cbiBcdFx0Ly8gcnVuIGRlZmVycmVkIG1vZHVsZXMgd2hlbiBhbGwgY2h1bmtzIHJlYWR5XG4gXHRcdHJldHVybiBjaGVja0RlZmVycmVkTW9kdWxlcygpO1xuIFx0fTtcbiBcdGZ1bmN0aW9uIGNoZWNrRGVmZXJyZWRNb2R1bGVzKCkge1xuIFx0XHR2YXIgcmVzdWx0O1xuIFx0XHRmb3IodmFyIGkgPSAwOyBpIDwgZGVmZXJyZWRNb2R1bGVzLmxlbmd0aDsgaSsrKSB7XG4gXHRcdFx0dmFyIGRlZmVycmVkTW9kdWxlID0gZGVmZXJyZWRNb2R1bGVzW2ldO1xuIFx0XHRcdHZhciBmdWxmaWxsZWQgPSB0cnVlO1xuIFx0XHRcdGZvcih2YXIgaiA9IDE7IGogPCBkZWZlcnJlZE1vZHVsZS5sZW5ndGg7IGorKykge1xuIFx0XHRcdFx0dmFyIGRlcElkID0gZGVmZXJyZWRNb2R1bGVbal07XG4gXHRcdFx0XHRpZihpbnN0YWxsZWRDaHVua3NbZGVwSWRdICE9PSAwKSBmdWxmaWxsZWQgPSBmYWxzZTtcbiBcdFx0XHR9XG4gXHRcdFx0aWYoZnVsZmlsbGVkKSB7XG4gXHRcdFx0XHRkZWZlcnJlZE1vZHVsZXMuc3BsaWNlKGktLSwgMSk7XG4gXHRcdFx0XHRyZXN1bHQgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IGRlZmVycmVkTW9kdWxlWzBdKTtcbiBcdFx0XHR9XG4gXHRcdH1cblxuIFx0XHRyZXR1cm4gcmVzdWx0O1xuIFx0fVxuXG4gXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBvYmplY3QgdG8gc3RvcmUgbG9hZGVkIGFuZCBsb2FkaW5nIGNodW5rc1xuIFx0Ly8gdW5kZWZpbmVkID0gY2h1bmsgbm90IGxvYWRlZCwgbnVsbCA9IGNodW5rIHByZWxvYWRlZC9wcmVmZXRjaGVkXG4gXHQvLyBQcm9taXNlID0gY2h1bmsgbG9hZGluZywgMCA9IGNodW5rIGxvYWRlZFxuIFx0dmFyIGluc3RhbGxlZENodW5rcyA9IHtcbiBcdFx0XCJpbmRleFwiOiAwXG4gXHR9O1xuXG4gXHR2YXIgZGVmZXJyZWRNb2R1bGVzID0gW107XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cbiBcdHZhciBqc29ucEFycmF5ID0gd2luZG93W1wid2VicGFja0pzb25wXCJdID0gd2luZG93W1wid2VicGFja0pzb25wXCJdIHx8IFtdO1xuIFx0dmFyIG9sZEpzb25wRnVuY3Rpb24gPSBqc29ucEFycmF5LnB1c2guYmluZChqc29ucEFycmF5KTtcbiBcdGpzb25wQXJyYXkucHVzaCA9IHdlYnBhY2tKc29ucENhbGxiYWNrO1xuIFx0anNvbnBBcnJheSA9IGpzb25wQXJyYXkuc2xpY2UoKTtcbiBcdGZvcih2YXIgaSA9IDA7IGkgPCBqc29ucEFycmF5Lmxlbmd0aDsgaSsrKSB3ZWJwYWNrSnNvbnBDYWxsYmFjayhqc29ucEFycmF5W2ldKTtcbiBcdHZhciBwYXJlbnRKc29ucEZ1bmN0aW9uID0gb2xkSnNvbnBGdW5jdGlvbjtcblxuXG4gXHQvLyBhZGQgZW50cnkgbW9kdWxlIHRvIGRlZmVycmVkIGxpc3RcbiBcdGRlZmVycmVkTW9kdWxlcy5wdXNoKFswLFwidmVuZG9yXCJdKTtcbiBcdC8vIHJ1biBkZWZlcnJlZCBtb2R1bGVzIHdoZW4gcmVhZHlcbiBcdHJldHVybiBjaGVja0RlZmVycmVkTW9kdWxlcygpO1xuIiwiPHRlbXBsYXRlPlxyXG4gIDxkaXYgY2xhc3M9XCJrdC1jb250ZW50ICBrdC1ncmlkX19pdGVtIGt0LWdyaWRfX2l0ZW0tLWZsdWlkIGt0LWdyaWQga3QtZ3JpZC0taG9yXCI+XHJcbiAgICA8YnJlYWRjcnVtYj48L2JyZWFkY3J1bWI+XHJcbiAgICA8cm91dGVyLXZpZXc+PC9yb3V0ZXItdmlldz5cclxuICA8L2Rpdj5cclxuPC90ZW1wbGF0ZT5cclxuXHJcbjxzY3JpcHQ+XHJcbiAgaW1wb3J0IFRoZUJyZWFkY3J1bWIgZnJvbSAnJHZ1ZVBhcnRpYWxQYXRoL2dsb2JhbC9UaGVCcmVhZGNydW1iJ1xyXG5cclxuICBleHBvcnQgZGVmYXVsdCB7XHJcbiAgICBuYW1lOiAndXNlci1hcHAnLFxyXG4gICAgY29tcG9uZW50czoge1xyXG4gICAgICAnYnJlYWRjcnVtYic6IFRoZUJyZWFkY3J1bWIsXHJcbiAgICB9LFxyXG4gICAgZGF0YSgpIHtcclxuICAgICAgcmV0dXJuIHt9XHJcbiAgICB9LFxyXG4gICAgY29tcHV0ZWQ6IHt9LFxyXG4gICAgY3JlYXRlZCgpIHtcclxuICAgIH0sXHJcbiAgICBtZXRob2RzOiB7fSxcclxuICB9XHJcbjwvc2NyaXB0PlxyXG5cclxuIiwiPHRlbXBsYXRlPlxyXG4gIDxkaXYgY2xhc3M9XCJrdC1zdWJoZWFkZXIgICBrdC1ncmlkX19pdGVtXCIgaWQ9XCJrdF9zdWJoZWFkZXJcIj5cclxuICAgIDxkaXYgY2xhc3M9XCJrdC1jb250YWluZXIgIGt0LWNvbnRhaW5lci0tZmx1aWQgXCI+XHJcbiAgICAgIDxkaXYgY2xhc3M9XCJrdC1zdWJoZWFkZXJfX21haW5cIj5cclxuICAgICAgICA8c3BhbiBjbGFzcz1cImt0LXN1YmhlYWRlcl9fc2VwYXJhdG9yIGt0LWhpZGRlblwiPjwvc3Bhbj5cclxuICAgICAgICA8ZGl2XHJcbiAgICAgICAgICAgIGNsYXNzPVwia3Qtc3ViaGVhZGVyX19icmVhZGNydW1ic1wiXHJcbiAgICAgICAgICAgIHYtaHRtbD1cImNvbnRlbnRcIlxyXG4gICAgICAgID5cclxuICAgICAgICAgIHt7Y29udGVudH19XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgICA8ZGl2IGNsYXNzPVwia3Qtc3ViaGVhZGVyX190b29sYmFyXCI+PC9kaXY+XHJcbiAgICAgIDxkaXYgY2xhc3M9XCJrdC1oZWFkZXJfX3RvcGJhclwiPlxyXG4gICAgICAgIDwhLS0gcGFnaW5hdGlvbiAtLT5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwia3QtcGFnaW5hdGlvbiBrdC1wYWdpbmF0aW9uLS1icmFuZFwiPlxyXG4gICAgICAgICAgPGRpdiBjbGFzcz1cImt0LXBhZ2luYXRpb25fX3Rvb2xiYXIgX20tci0xMFwiPlxyXG4gICAgICAgICAgICA8c3BhbiBjbGFzcz1cInBhZ2luYXRpb25fX2Rlc2NcIj5cclxuICAgICAgICAgICAgICAgIHt7KChwYWdpbmdTdGF0ZS5wYWdlIC0gMSkgKiBwYWdpbmdTdGF0ZS5saW1pdCkgKyAnIC0gJyArICggKHBhZ2luZ1N0YXRlLnBhZ2UgKiBwYWdpbmdTdGF0ZS5saW1pdCkgPCBwYWdpbmdTdGF0ZS50b3RhbFJlY29yZCA/IChwYWdpbmdTdGF0ZS5wYWdlICogcGFnaW5nU3RhdGUubGltaXQpIDogcGFnaW5nU3RhdGUudG90YWxSZWNvcmQgKX19IHRyb25nIHPhu5Ege3twYWdpbmdTdGF0ZS50b3RhbFJlY29yZH19XHJcbiAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgPHVsIGNsYXNzPVwia3QtcGFnaW5hdGlvbl9fbGlua3NcIj5cclxuICAgICAgICAgICAgPGxpIGNsYXNzPVwia3QtcGFnaW5hdGlvbl9fbGluay0tZmlyc3RcIj5cclxuICAgICAgICAgICAgICA8YVxyXG4gICAgICAgICAgICAgICAgICBocmVmPVwiI1wiXHJcbiAgICAgICAgICAgICAgICAgIDpjbGFzcz1cInsnaXMtZGlzYWJsZWQnOiBwYWdpbmdTdGF0ZS5wYWdlID09IDF9XCJcclxuICAgICAgICAgICAgICAgICAgQGNsaWNrLnByZXZlbnQ9XCJjaGFuZ2VQYWdlKDEpXCJcclxuICAgICAgICAgICAgICA+PGkgY2xhc3M9XCJmYSBmYS1hbmdsZS1kb3VibGUtbGVmdCBrdC1mb250LWJyYW5kXCI+PC9pPjwvYT5cclxuICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgPGxpIGNsYXNzPVwia3QtcGFnaW5hdGlvbl9fbGluay0tcHJldlwiPlxyXG4gICAgICAgICAgICAgIDxhXHJcbiAgICAgICAgICAgICAgICAgIGhyZWY9XCIjXCJcclxuICAgICAgICAgICAgICAgICAgOmNsYXNzPVwieydpcy1kaXNhYmxlZCc6IHBhZ2luZ1N0YXRlLnBhZ2UgPT0gMX1cIlxyXG4gICAgICAgICAgICAgICAgICBAY2xpY2sucHJldmVudD1cImNoYW5nZVBhZ2UocGFnaW5nU3RhdGUucGFnZSAtIDEpXCJcclxuICAgICAgICAgICAgICA+PGkgY2xhc3M9XCJmYSBmYS1hbmdsZS1sZWZ0IGt0LWZvbnQtYnJhbmRcIj48L2k+PC9hPlxyXG4gICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICA8bGkgY2xhc3M9XCJrdC1wYWdpbmF0aW9uX19saW5rLS1uZXh0XCI+XHJcbiAgICAgICAgICAgICAgPGFcclxuICAgICAgICAgICAgICAgICAgaHJlZj1cIiNcIlxyXG4gICAgICAgICAgICAgICAgICA6Y2xhc3M9XCJ7J2lzLWRpc2FibGVkJzogcGFnaW5nU3RhdGUudG90YWxSZWNvcmQgPCBwYWdpbmdTdGF0ZS5saW1pdCB8fCBwYWdpbmdTdGF0ZS5wYWdlID09IHBhZ2luZ1N0YXRlLnRvdGFsUGFnZX1cIlxyXG4gICAgICAgICAgICAgICAgICBAY2xpY2sucHJldmVudD1cImNoYW5nZVBhZ2UocGFnaW5nU3RhdGUucGFnZSArIDEpXCJcclxuICAgICAgICAgICAgICA+PGkgY2xhc3M9XCJmYSBmYS1hbmdsZS1yaWdodCBrdC1mb250LWJyYW5kXCI+PC9pPjwvYT5cclxuICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgPGxpIGNsYXNzPVwia3QtcGFnaW5hdGlvbl9fbGluay0tbGFzdFwiPlxyXG4gICAgICAgICAgICAgIDxhXHJcbiAgICAgICAgICAgICAgICAgIGhyZWY9XCIjXCJcclxuICAgICAgICAgICAgICAgICAgOmNsYXNzPVwieydpcy1kaXNhYmxlZCc6IHBhZ2luZ1N0YXRlLnRvdGFsUmVjb3JkIDwgcGFnaW5nU3RhdGUubGltaXQgfHwgcGFnaW5nU3RhdGUucGFnZSA9PSBwYWdpbmdTdGF0ZS50b3RhbFBhZ2V9XCJcclxuICAgICAgICAgICAgICAgICAgQGNsaWNrLnByZXZlbnQ9XCJjaGFuZ2VQYWdlKHBhZ2luZ1N0YXRlLnRvdGFsUGFnZSlcIlxyXG4gICAgICAgICAgICAgID48aSBjbGFzcz1cImZhIGZhLWFuZ2xlLWRvdWJsZS1yaWdodCBrdC1mb250LWJyYW5kXCI+PC9pPjwvYT5cclxuICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgIDwvdWw+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPCEtLSBwYWdpbmF0aW9uIGVuZCAtLT5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwia3QtaGVhZGVyX190b3BiYXItaXRlbSBrdC1oZWFkZXJfX3RvcGJhci1pdGVtLS11c2VyXCI+XHJcbiAgICAgICAgICA8ZGl2IGNsYXNzPVwia3QtaGVhZGVyX190b3BiYXItd3JhcHBlclwiIGRhdGEtdG9nZ2xlPVwiZHJvcGRvd25cIiBkYXRhLW9mZnNldD1cIjBweCwwcHhcIj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cImt0LWhlYWRlcl9fdG9wYmFyLXVzZXJcIj5cclxuICAgICAgICAgICAgICA8IS0tdXNlIGJlbG93IGJhZGdlIGVsZW1lbnQgaW5zdGVhZCB0aGUgdXNlciBhdmF0YXIgdG8gZGlzcGxheSB1c2VybmFtZSdzIGZpcnN0IGxldHRlcihyZW1vdmUga3QtaGlkZGVuIGNsYXNzIHRvIGRpc3BsYXkgaXQpIC0tPlxyXG4gICAgICAgICAgICAgIDxzcGFuXHJcbiAgICAgICAgICAgICAgICAgIGNsYXNzPVwia3QtYmFkZ2Uga3QtYmFkZ2UtLXVzZXJuYW1lIGt0LWJhZGdlLS11bmlmaWVkLXN1Y2Nlc3Mga3QtYmFkZ2UtLWxnIGt0LWJhZGdlLS1yb3VuZGVkIGt0LWJhZGdlLS1ib2xkXCJcclxuICAgICAgICAgICAgICAgICAgc3R5bGU9XCJ3aWR0aDogMTAwJTsgcGFkZGluZzogMTBweDtcIlxyXG4gICAgICAgICAgICAgID57e0NEQVRBLmxvZ2luLmZ1bGxOYW1lfX08L3NwYW4+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgPGRpdiBjbGFzcz1cImRyb3Bkb3duLW1lbnUgZHJvcGRvd24tbWVudS1maXQgZHJvcGRvd24tbWVudS1yaWdodCBkcm9wZG93bi1tZW51LWFuaW0gZHJvcGRvd24tbWVudS10b3AtdW5yb3VuZCBkcm9wZG93bi1tZW51LXhsXCI+XHJcbiAgICAgICAgICAgIDwhLS1iZWdpbjogTmF2aWdhdGlvbiAtLT5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cImt0LW5vdGlmaWNhdGlvblwiPlxyXG4gICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJrdC1ub3RpZmljYXRpb25fX2N1c3RvbSBrdC1zcGFjZS1iZXR3ZWVuXCI+XHJcbiAgICAgICAgICAgICAgICA8YSA6aHJlZj1cIkNEQVRBLnVybC5sb2dvdXRVUkxcIiBjbGFzcz1cImJ0biBidG4tbGFiZWwgYnRuLWxhYmVsLWJyYW5kIGJ0bi1zbSBidG4tYm9sZFwiPlNpZ24gT3V0PC9hPlxyXG4gICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPCEtLWVuZDogTmF2aWdhdGlvbiAtLT5cclxuICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICA8L2Rpdj5cclxuICAgIDwvZGl2PlxyXG4gIDwvZGl2PlxyXG48L3RlbXBsYXRlPlxyXG5cclxuPHNjcmlwdD5cclxuICBpbXBvcnQgeyBFVkVOVF9CVVNfS0VZIH0gZnJvbSAnJGpzQmFzZVBhdGgvZGVmaW5lJ1xyXG4gIGltcG9ydCBFdmVudEJ1cyBmcm9tICckanNCYXNlUGF0aC9FdmVudEJ1cydcclxuXHJcbiAgZXhwb3J0IGRlZmF1bHQge1xyXG4gICAgbmFtZTogJ3RoZS1icmVhZC1jcnVtYicsXHJcbiAgICBjb21wb25lbnRzOiB7fSxcclxuICAgIGRhdGEoKSB7XHJcbiAgICAgIHJldHVybiB7XHJcbiAgICAgICAgQ0RBVEEsXHJcbiAgICAgICAgY29udGVudDogYGAsXHJcbiAgICAgICAgcGFnaW5nU3RhdGU6IHtcclxuICAgICAgICAgIHBhZ2U6IDEsXHJcbiAgICAgICAgICBsaW1pdDogMjAsXHJcbiAgICAgICAgICB0b3RhbFJlY29yZDogMCxcclxuICAgICAgICAgIHRvdGFsUGFnZTogMSxcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH0sXHJcbiAgICBjb21wdXRlZDoge30sXHJcbiAgICB3YXRjaDoge1xyXG4gICAgICAkcm91dGUodG8sIGZyb20pIHtcclxuICAgICAgICBsZXQgcGF0aEZyb20gPSBmcm9tLnBhdGg7XHJcbiAgICAgICAgbGV0IHJvdXRlTmFtZSA9IHRvLm5hbWU7XHJcblxyXG4gICAgICAgIHRoaXMuZ2V0QnJlYWRjcnVtYkNvbnRlbnQocm91dGVOYW1lLCBwYXRoRnJvbSk7XHJcbiAgICAgIH1cclxuICAgIH0sXHJcbiAgICBiZWZvcmVDcmVhdGUoKSB7XHJcbiAgICAgIEV2ZW50QnVzLm9uKEVWRU5UX0JVU19LRVkuUEFHSU5HLkdFVF9UT1RBTF9SRUNPUkQsIHBheWxvYWQgPT4ge1xyXG4gICAgICAgIGxldCB7IHBhZ2UsIHRvdGFsUmVjb3JkIH0gPSB7IC4uLnBheWxvYWQgfTtcclxuXHJcbiAgICAgICAgdGhpcy5wYWdpbmdTdGF0ZS5wYWdlID0gcGFnZTtcclxuICAgICAgICB0aGlzLnBhZ2luZ1N0YXRlLnRvdGFsUmVjb3JkID0gdG90YWxSZWNvcmQ7XHJcblxyXG4gICAgICAgIGxldCB0ZW1wTm90Um91bmQgPSB0b3RhbFJlY29yZCAvIHRoaXMucGFnaW5nU3RhdGUubGltaXQ7XHJcbiAgICAgICAgbGV0IHRlbXAgPSBNYXRoLnJvdW5kKHRlbXBOb3RSb3VuZCk7XHJcblxyXG4gICAgICAgIHRoaXMucGFnaW5nU3RhdGUudG90YWxQYWdlID0gdGVtcE5vdFJvdW5kID4gdGVtcCA/IHRlbXAgKyAxIDogdGVtcDtcclxuICAgICAgfSlcclxuICAgIH0sXHJcbiAgICBjcmVhdGVkKCkge1xyXG4gICAgICBsZXQgcGF0aCA9IHdpbmRvdy5sb2NhdGlvbi5ocmVmLnJlcGxhY2UoQ0RBVEEudXJsLmJhc2VVUkwsICcnKTtcclxuICAgICAgbGV0IHJlc29sdmUgPSB0aGlzLiRyb3V0ZXIucmVzb2x2ZShwYXRoKTtcclxuXHJcbiAgICAgIHRoaXMuZ2V0QnJlYWRjcnVtYkNvbnRlbnQocmVzb2x2ZS5yZXNvbHZlZC5uYW1lKTtcclxuICAgIH0sXHJcbiAgICBtZXRob2RzOiB7XHJcbiAgICAgIGdldEJyZWFkY3J1bWJDb250ZW50KHJvdXRlTmFtZSwgcGF0aEZyb20pIHtcclxuICAgICAgICB0aGlzLmNvbnRlbnQgPSBgXHJcbiAgICAgICAgICA8YSBocmVmPVwiL2JhY2tlbmRcIiBjbGFzcz1cImt0LXN1YmhlYWRlcl9fYnJlYWRjcnVtYnMtaG9tZVwiPjxpIGNsYXNzPVwiZmxhdGljb24yLXNoZWx0ZXJcIj48L2k+PC9hPlxyXG4gICAgICAgICAgPHNwYW4gY2xhc3M9XCJrdC1zdWJoZWFkZXJfX2JyZWFkY3J1bWJzLXNlcGFyYXRvclwiPjwvc3Bhbj5cclxuICAgICAgICBgO1xyXG5cclxuICAgICAgICBzd2l0Y2ggKHJvdXRlTmFtZSkge1xyXG4gICAgICAgICAgY2FzZSAndXNlcjppbmRleCc6XHJcbiAgICAgICAgICAgIHRoaXMuY29udGVudCArPSBgXHJcbiAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJrdC1zdWJoZWFkZXJfX2JyZWFkY3J1bWJzLWxpbmtcIj5cclxuICAgICAgICAgICAgICAgIE5nxrDhu51pIGTDuW5nIDwvc3Bhbj5cclxuICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImt0LXN1YmhlYWRlcl9fYnJlYWRjcnVtYnMtc2VwYXJhdG9yXCI+PC9zcGFuPlxyXG4gICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwia3Qtc3ViaGVhZGVyX19icmVhZGNydW1icy1saW5rIGt0LXN1YmhlYWRlcl9fYnJlYWRjcnVtYnMtbGluay0tYWN0aXZlXCI+RGFuaCBzw6FjaDwvc3Bhbj5cclxuICAgICAgICAgICAgYDtcclxuICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICBjYXNlICd1c2VyOmFkZCc6XHJcbiAgICAgICAgICAgIHRoaXMuY29udGVudCArPSBgXHJcbiAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJrdC1zdWJoZWFkZXJfX2JyZWFkY3J1bWJzLWxpbmtcIj5OZ8aw4budaSBkw7luZyA8L3NwYW4+XHJcbiAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJrdC1zdWJoZWFkZXJfX2JyZWFkY3J1bWJzLXNlcGFyYXRvclwiPjwvc3Bhbj5cclxuICAgICAgICAgICAgICA8YVxyXG4gICAgICAgICAgICAgICAgICBocmVmPVwiI1wiXHJcbiAgICAgICAgICAgICAgICAgIGNsYXNzPVwia3Qtc3ViaGVhZGVyX19icmVhZGNydW1icy1saW5rXCJcclxuICAgICAgICAgICAgICAgICAgQGNsaWNrLnByZXZlbnQ9XCJnb1JlYWRcIlxyXG4gICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgIERhbmggc8OhY2hcclxuICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJrdC1zdWJoZWFkZXJfX2JyZWFkY3J1bWJzLXNlcGFyYXRvclwiPjwvc3Bhbj5cclxuICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImt0LXN1YmhlYWRlcl9fYnJlYWRjcnVtYnMtbGluayBrdC1zdWJoZWFkZXJfX2JyZWFkY3J1bWJzLWxpbmstLWFjdGl2ZVwiPlThuqFvIG5nxrDhu51pIGTDuW5nPC9zcGFuPlxyXG4gICAgICAgICAgICBgO1xyXG4gICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgIGNhc2UgJ3VzZXI6ZWRpdCc6XHJcbiAgICAgICAgICAgIHRoaXMuY29udGVudCArPSBgXHJcbiAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJrdC1zdWJoZWFkZXJfX2JyZWFkY3J1bWJzLWxpbmtcIj5OZ8aw4budaSBkw7luZyA8L3NwYW4+XHJcbiAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJrdC1zdWJoZWFkZXJfX2JyZWFkY3J1bWJzLXNlcGFyYXRvclwiPjwvc3Bhbj5cclxuICAgICAgICAgICAgICA8YVxyXG4gICAgICAgICAgICAgICAgICBocmVmPVwiI1wiXHJcbiAgICAgICAgICAgICAgICAgIGNsYXNzPVwia3Qtc3ViaGVhZGVyX19icmVhZGNydW1icy1saW5rXCJcclxuICAgICAgICAgICAgICAgICAgQGNsaWNrLnByZXZlbnQ9XCJnb1JlYWRcIlxyXG4gICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgIERhbmggc8OhY2hcclxuICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJrdC1zdWJoZWFkZXJfX2JyZWFkY3J1bWJzLXNlcGFyYXRvclwiPjwvc3Bhbj5cclxuICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImt0LXN1YmhlYWRlcl9fYnJlYWRjcnVtYnMtbGluayBrdC1zdWJoZWFkZXJfX2JyZWFkY3J1bWJzLWxpbmstLWFjdGl2ZVwiPkPhuq1wIG5o4bqtdCBuZ8aw4budaSBkw7luZzwvc3Bhbj5cclxuICAgICAgICAgICAgYDtcclxuICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICBjYXNlICdyb2xlOmluZGV4JzpcclxuICAgICAgICAgICAgdGhpcy5jb250ZW50ICs9IGBcclxuICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImt0LXN1YmhlYWRlcl9fYnJlYWRjcnVtYnMtbGlua1wiPk5ow7NtIDwvc3Bhbj5cclxuICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImt0LXN1YmhlYWRlcl9fYnJlYWRjcnVtYnMtc2VwYXJhdG9yXCI+PC9zcGFuPlxyXG4gICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwia3Qtc3ViaGVhZGVyX19icmVhZGNydW1icy1saW5rIGt0LXN1YmhlYWRlcl9fYnJlYWRjcnVtYnMtbGluay0tYWN0aXZlXCI+RGFuaCBzw6FjaDwvc3Bhbj5cclxuICAgICAgICAgICAgYDtcclxuICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICBjYXNlICdyb2xlOmFkZCc6XHJcbiAgICAgICAgICAgIHRoaXMuY29udGVudCArPSBgXHJcbiAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJrdC1zdWJoZWFkZXJfX2JyZWFkY3J1bWJzLWxpbmtcIj5OaMOzbSA8L3NwYW4+XHJcbiAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJrdC1zdWJoZWFkZXJfX2JyZWFkY3J1bWJzLXNlcGFyYXRvclwiPjwvc3Bhbj5cclxuICAgICAgICAgICAgICA8YVxyXG4gICAgICAgICAgICAgICAgICBocmVmPVwiI1wiXHJcbiAgICAgICAgICAgICAgICAgIGNsYXNzPVwia3Qtc3ViaGVhZGVyX19icmVhZGNydW1icy1saW5rXCJcclxuICAgICAgICAgICAgICAgICAgQGNsaWNrLnByZXZlbnQ9XCJnb1JlYWRcIlxyXG4gICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgIERhbmggc8OhY2hcclxuICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJrdC1zdWJoZWFkZXJfX2JyZWFkY3J1bWJzLXNlcGFyYXRvclwiPjwvc3Bhbj5cclxuICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImt0LXN1YmhlYWRlcl9fYnJlYWRjcnVtYnMtbGluayBrdC1zdWJoZWFkZXJfX2JyZWFkY3J1bWJzLWxpbmstLWFjdGl2ZVwiPlThuqFvIG5ow7NtPC9zcGFuPlxyXG4gICAgICAgICAgICBgO1xyXG4gICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgIGNhc2UgJ3JvbGU6ZWRpdCc6XHJcbiAgICAgICAgICAgIHRoaXMuY29udGVudCArPSBgXHJcbiAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJrdC1zdWJoZWFkZXJfX2JyZWFkY3J1bWJzLWxpbmtcIj5OaMOzbSA8L3NwYW4+XHJcbiAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJrdC1zdWJoZWFkZXJfX2JyZWFkY3J1bWJzLXNlcGFyYXRvclwiPjwvc3Bhbj5cclxuICAgICAgICAgICAgICA8YVxyXG4gICAgICAgICAgICAgICAgICBocmVmPVwiI1wiXHJcbiAgICAgICAgICAgICAgICAgIGNsYXNzPVwia3Qtc3ViaGVhZGVyX19icmVhZGNydW1icy1saW5rXCJcclxuICAgICAgICAgICAgICAgICAgQGNsaWNrLnByZXZlbnQ9XCJnb1JlYWRcIlxyXG4gICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgIERhbmggc8OhY2hcclxuICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJrdC1zdWJoZWFkZXJfX2JyZWFkY3J1bWJzLXNlcGFyYXRvclwiPjwvc3Bhbj5cclxuICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImt0LXN1YmhlYWRlcl9fYnJlYWRjcnVtYnMtbGluayBrdC1zdWJoZWFkZXJfX2JyZWFkY3J1bWJzLWxpbmstLWFjdGl2ZVwiPkPhuq1wIG5o4bqtdCBuaMOzbTwvc3Bhbj5cclxuICAgICAgICAgICAgYDtcclxuICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICBjYXNlICdyb2xlOmdyYW50JzpcclxuICAgICAgICAgICAgdGhpcy5jb250ZW50ICs9IGBcclxuICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImt0LXN1YmhlYWRlcl9fYnJlYWRjcnVtYnMtbGlua1wiPk5ow7NtIDwvc3Bhbj5cclxuICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImt0LXN1YmhlYWRlcl9fYnJlYWRjcnVtYnMtc2VwYXJhdG9yXCI+PC9zcGFuPlxyXG4gICAgICAgICAgICAgIDxhXHJcbiAgICAgICAgICAgICAgICAgIGhyZWY9XCIjXCJcclxuICAgICAgICAgICAgICAgICAgY2xhc3M9XCJrdC1zdWJoZWFkZXJfX2JyZWFkY3J1bWJzLWxpbmtcIlxyXG4gICAgICAgICAgICAgICAgICBAY2xpY2sucHJldmVudD1cImdvUmVhZFwiXHJcbiAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgRGFuaCBzw6FjaFxyXG4gICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImt0LXN1YmhlYWRlcl9fYnJlYWRjcnVtYnMtc2VwYXJhdG9yXCI+PC9zcGFuPlxyXG4gICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwia3Qtc3ViaGVhZGVyX19icmVhZGNydW1icy1saW5rIGt0LXN1YmhlYWRlcl9fYnJlYWRjcnVtYnMtbGluay0tYWN0aXZlXCI+UGjDom4gcXV54buBbjwvc3Bhbj5cclxuICAgICAgICAgICAgYDtcclxuICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICBjYXNlICdpdGVtOmluZGV4JzpcclxuICAgICAgICAgICAgdGhpcy5jb250ZW50ICs9IGBcclxuICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImt0LXN1YmhlYWRlcl9fYnJlYWRjcnVtYnMtbGlua1wiPlPhuqNuIHBo4bqpbSA8L3NwYW4+XHJcbiAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJrdC1zdWJoZWFkZXJfX2JyZWFkY3J1bWJzLXNlcGFyYXRvclwiPjwvc3Bhbj5cclxuICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImt0LXN1YmhlYWRlcl9fYnJlYWRjcnVtYnMtbGluayBrdC1zdWJoZWFkZXJfX2JyZWFkY3J1bWJzLWxpbmstLWFjdGl2ZVwiPkRhbmggc8OhY2g8L3NwYW4+XHJcbiAgICAgICAgICAgIGA7XHJcbiAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgY2FzZSAnaXRlbTphZGQnOlxyXG4gICAgICAgICAgICB0aGlzLmNvbnRlbnQgKz0gYFxyXG4gICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwia3Qtc3ViaGVhZGVyX19icmVhZGNydW1icy1saW5rXCI+U+G6o24gcGjhuqltIDwvc3Bhbj5cclxuICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImt0LXN1YmhlYWRlcl9fYnJlYWRjcnVtYnMtc2VwYXJhdG9yXCI+PC9zcGFuPlxyXG4gICAgICAgICAgICAgIDxhXHJcbiAgICAgICAgICAgICAgICAgIGhyZWY9XCIjXCJcclxuICAgICAgICAgICAgICAgICAgY2xhc3M9XCJrdC1zdWJoZWFkZXJfX2JyZWFkY3J1bWJzLWxpbmtcIlxyXG4gICAgICAgICAgICAgICAgICBAY2xpY2sucHJldmVudD1cImdvUmVhZFwiXHJcbiAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgRGFuaCBzw6FjaFxyXG4gICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImt0LXN1YmhlYWRlcl9fYnJlYWRjcnVtYnMtc2VwYXJhdG9yXCI+PC9zcGFuPlxyXG4gICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwia3Qtc3ViaGVhZGVyX19icmVhZGNydW1icy1saW5rIGt0LXN1YmhlYWRlcl9fYnJlYWRjcnVtYnMtbGluay0tYWN0aXZlXCI+xJDEg25nIHPhuqNuIHBo4bqpbTwvc3Bhbj5cclxuICAgICAgICAgICAgYDtcclxuICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICBjYXNlICdpdGVtOmVkaXQnOlxyXG4gICAgICAgICAgICB0aGlzLmNvbnRlbnQgKz0gYFxyXG4gICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwia3Qtc3ViaGVhZGVyX19icmVhZGNydW1icy1saW5rXCI+U+G6o24gcGjhuqltIDwvc3Bhbj5cclxuICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImt0LXN1YmhlYWRlcl9fYnJlYWRjcnVtYnMtc2VwYXJhdG9yXCI+PC9zcGFuPlxyXG4gICAgICAgICAgICAgIDxhXHJcbiAgICAgICAgICAgICAgICAgIGhyZWY9XCIjXCJcclxuICAgICAgICAgICAgICAgICAgY2xhc3M9XCJrdC1zdWJoZWFkZXJfX2JyZWFkY3J1bWJzLWxpbmtcIlxyXG4gICAgICAgICAgICAgICAgICBAY2xpY2sucHJldmVudD1cImdvUmVhZFwiXHJcbiAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgRGFuaCBzw6FjaFxyXG4gICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImt0LXN1YmhlYWRlcl9fYnJlYWRjcnVtYnMtc2VwYXJhdG9yXCI+PC9zcGFuPlxyXG4gICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwia3Qtc3ViaGVhZGVyX19icmVhZGNydW1icy1saW5rIGt0LXN1YmhlYWRlcl9fYnJlYWRjcnVtYnMtbGluay0tYWN0aXZlXCI+Q+G6rXAgbmjhuq10IHPhuqNuIHBo4bqpbTwvc3Bhbj5cclxuICAgICAgICAgICAgYDtcclxuICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICB0aGlzLmNvbnRlbnQgKz0gYFxyXG4gICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwia3Qtc3ViaGVhZGVyX19icmVhZGNydW1icy1saW5rIGt0LXN1YmhlYWRlcl9fYnJlYWRjcnVtYnMtbGluay0tYWN0aXZlXCI+RGFzaGJvYXJkPC9zcGFuPlxyXG4gICAgICAgICAgICBgXHJcbiAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIH1cclxuICAgICAgfSxcclxuICAgICAgY2hhbmdlUGFnZShwYWdlKSB7XHJcbiAgICAgICAgcGFnZSA9IHBhZ2UgPj0gMSA/IHBhZ2UgOiAxO1xyXG5cclxuICAgICAgICBFdmVudEJ1cy5lbWl0KEVWRU5UX0JVU19LRVkuUEFHSU5HLkNIQU5HRV9QQUdFLCB7IHBhZ2U6IHBhZ2UgfSk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbjwvc2NyaXB0PlxyXG5cclxuIiwiPHRlbXBsYXRlPlxyXG4gIDxkaXYgY2xhc3M9XCJrdC1jb250ZW50ICBrdC1ncmlkX19pdGVtIGt0LWdyaWRfX2l0ZW0tLWZsdWlkIGt0LWdyaWQga3QtZ3JpZC0taG9yXCI+XHJcbiAgICA8ZGl2IGNsYXNzPVwia3Qtc3ViaGVhZGVyICAga3QtZ3JpZF9faXRlbVwiIGlkPVwia3Rfc3ViaGVhZGVyXCI+XHJcbiAgICAgIDxkaXYgY2xhc3M9XCJrdC1jb250YWluZXIgIGt0LWNvbnRhaW5lci0tZmx1aWQgXCI+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cImt0LXN1YmhlYWRlcl9fbWFpblwiPlxyXG4gICAgICAgICAgPHNwYW4gY2xhc3M9XCJrdC1zdWJoZWFkZXJfX3NlcGFyYXRvciBrdC1oaWRkZW5cIj48L3NwYW4+XHJcbiAgICAgICAgICA8ZGl2IGNsYXNzPVwia3Qtc3ViaGVhZGVyX19icmVhZGNydW1ic1wiPlxyXG4gICAgICAgICAgICA8YSBocmVmPVwiL2JhY2tlbmRcIiBjbGFzcz1cImt0LXN1YmhlYWRlcl9fYnJlYWRjcnVtYnMtaG9tZVwiPjxpIGNsYXNzPVwiZmxhdGljb24yLXNoZWx0ZXJcIj48L2k+PC9hPlxyXG4gICAgICAgICAgICA8c3BhbiBjbGFzcz1cImt0LXN1YmhlYWRlcl9fYnJlYWRjcnVtYnMtc2VwYXJhdG9yXCI+PC9zcGFuPlxyXG4gICAgICAgICAgICA8c3BhbiBjbGFzcz1cImt0LXN1YmhlYWRlcl9fYnJlYWRjcnVtYnMtbGlua1wiPlxyXG4gICAgICAgICAgICAgIERhc2hib2FyZFxyXG4gICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwia3Qtc3ViaGVhZGVyX190b29sYmFyXCI+PC9kaXY+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cImt0LWhlYWRlcl9fdG9wYmFyXCI+XHJcbiAgICAgICAgICA8ZGl2IGNsYXNzPVwia3QtaGVhZGVyX190b3BiYXItaXRlbSBrdC1oZWFkZXJfX3RvcGJhci1pdGVtLS11c2VyXCI+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJrdC1oZWFkZXJfX3RvcGJhci13cmFwcGVyXCIgZGF0YS10b2dnbGU9XCJkcm9wZG93blwiIGRhdGEtb2Zmc2V0PVwiMHB4LDBweFwiPlxyXG4gICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJrdC1oZWFkZXJfX3RvcGJhci11c2VyXCI+XHJcbiAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cImt0LWhlYWRlcl9fdG9wYmFyLXdlbGNvbWUga3QtaGlkZGVuLW1vYmlsZVwiPkhpLDwvc3Bhbj5cclxuICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwia3QtaGVhZGVyX190b3BiYXItdXNlcm5hbWUga3QtaGlkZGVuLW1vYmlsZVwiPnt7Q0RBVEEubG9naW4uZnVsbE5hbWV9fTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgIDxpbWcgY2xhc3M9XCJrdC1oaWRkZW5cIiBhbHQ9XCJQaWNcIiBzcmM9XCJcIj5cclxuICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwia3QtYmFkZ2Uga3QtYmFkZ2UtLXVzZXJuYW1lIGt0LWJhZGdlLS11bmlmaWVkLXN1Y2Nlc3Mga3QtYmFkZ2UtLWxnIGt0LWJhZGdlLS1yb3VuZGVkIGt0LWJhZGdlLS1ib2xkXCI+Uzwvc3Bhbj5cclxuICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZHJvcGRvd24tbWVudSBkcm9wZG93bi1tZW51LWZpdCBkcm9wZG93bi1tZW51LXJpZ2h0IGRyb3Bkb3duLW1lbnUtYW5pbSBkcm9wZG93bi1tZW51LXRvcC11bnJvdW5kIGRyb3Bkb3duLW1lbnUteGxcIj5cclxuICAgICAgICAgICAgICA8IS0tYmVnaW46IE5hdmlnYXRpb24gLS0+XHJcbiAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImt0LW5vdGlmaWNhdGlvblwiPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImt0LW5vdGlmaWNhdGlvbl9fY3VzdG9tIGt0LXNwYWNlLWJldHdlZW5cIj5cclxuPGEgOmhyZWY9XCJDREFUQS51cmwubG9nb3V0VVJMXCIgY2xhc3M9XCJidG4gYnRuLWxhYmVsIGJ0bi1sYWJlbC1icmFuZCBidG4tc20gYnRuLWJvbGRcIj5TaWduIE91dDwvYT48L2Rpdj5cclxuICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICA8IS0tZW5kOiBOYXZpZ2F0aW9uIC0tPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICA8L2Rpdj5cclxuICAgIDwvZGl2PlxyXG4gIDwvZGl2PlxyXG48L3RlbXBsYXRlPlxyXG5cclxuPHNjcmlwdD5cclxuICBpbXBvcnQgeyBtYXBTdGF0ZSwgbWFwR2V0dGVycywgbWFwTXV0YXRpb25zLCBtYXBBY3Rpb25zIH0gZnJvbSAndnVleCc7XHJcblxyXG4gIGltcG9ydCB7IF9zaG93Q29udGFpbmVyIH0gZnJvbSBcIiRqc0NvbW1vblBhdGgvdXRpbC9HZW5lcmFsVXRpbFwiXHJcblxyXG4gIGV4cG9ydCBkZWZhdWx0IHtcclxuICAgIG5hbWU6ICdpbmRleC1zZWN0aW9uJyxcclxuICAgIGNvbXBvbmVudHM6IHtcclxuXHJcbiAgICB9LFxyXG4gICAgZGF0YSgpIHtcclxuICAgICAgcmV0dXJuIHtcclxuICAgICAgICBDREFUQSxcclxuICAgICAgICBsb2NPcHRpb246IHtcclxuICAgICAgICAgIGlzUmVhZGluZzogZmFsc2VcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH0sXHJcbiAgICBjb21wdXRlZDoge1xyXG4gICAgICAuLi5tYXBTdGF0ZShbICdvcHRpb24nIF0pLFxyXG4gICAgfSxcclxuICAgIGNyZWF0ZWQoKSB7XHJcbiAgICAgIGNvbnNvbGUubG9nKCd4eHh4eCcpXHJcbiAgICB9LFxyXG4gICAgbW91bnRlZCgpIHtcclxuICAgICAgX3Nob3dDb250YWluZXIoKVxyXG4gICAgfSxcclxuICAgIG1ldGhvZHM6IHtcclxuICAgIH1cclxuICB9XHJcbjwvc2NyaXB0PlxyXG5cclxuIiwidmFyIHJlbmRlciA9IGZ1bmN0aW9uKCkge1xuICB2YXIgX3ZtID0gdGhpc1xuICB2YXIgX2ggPSBfdm0uJGNyZWF0ZUVsZW1lbnRcbiAgdmFyIF9jID0gX3ZtLl9zZWxmLl9jIHx8IF9oXG4gIHJldHVybiBfYyhcbiAgICBcImRpdlwiLFxuICAgIHtcbiAgICAgIHN0YXRpY0NsYXNzOlxuICAgICAgICBcImt0LWNvbnRlbnQgIGt0LWdyaWRfX2l0ZW0ga3QtZ3JpZF9faXRlbS0tZmx1aWQga3QtZ3JpZCBrdC1ncmlkLS1ob3JcIlxuICAgIH0sXG4gICAgW19jKFwiYnJlYWRjcnVtYlwiKSwgX3ZtLl92KFwiIFwiKSwgX2MoXCJyb3V0ZXItdmlld1wiKV0sXG4gICAgMVxuICApXG59XG52YXIgc3RhdGljUmVuZGVyRm5zID0gW11cbnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuXG5leHBvcnQgeyByZW5kZXIsIHN0YXRpY1JlbmRlckZucyB9IiwidmFyIHJlbmRlciA9IGZ1bmN0aW9uKCkge1xuICB2YXIgX3ZtID0gdGhpc1xuICB2YXIgX2ggPSBfdm0uJGNyZWF0ZUVsZW1lbnRcbiAgdmFyIF9jID0gX3ZtLl9zZWxmLl9jIHx8IF9oXG4gIHJldHVybiBfYyhcbiAgICBcImRpdlwiLFxuICAgIHtcbiAgICAgIHN0YXRpY0NsYXNzOiBcImt0LXN1YmhlYWRlciAgIGt0LWdyaWRfX2l0ZW1cIixcbiAgICAgIGF0dHJzOiB7IGlkOiBcImt0X3N1YmhlYWRlclwiIH1cbiAgICB9LFxuICAgIFtcbiAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwia3QtY29udGFpbmVyICBrdC1jb250YWluZXItLWZsdWlkIFwiIH0sIFtcbiAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJrdC1zdWJoZWFkZXJfX21haW5cIiB9LCBbXG4gICAgICAgICAgX2MoXCJzcGFuXCIsIHsgc3RhdGljQ2xhc3M6IFwia3Qtc3ViaGVhZGVyX19zZXBhcmF0b3Iga3QtaGlkZGVuXCIgfSksXG4gICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICBfYyhcbiAgICAgICAgICAgIFwiZGl2XCIsXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImt0LXN1YmhlYWRlcl9fYnJlYWRjcnVtYnNcIixcbiAgICAgICAgICAgICAgZG9tUHJvcHM6IHsgaW5uZXJIVE1MOiBfdm0uX3MoX3ZtLmNvbnRlbnQpIH1cbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBbX3ZtLl92KFwiXFxuICAgICAgICBcIiArIF92bS5fcyhfdm0uY29udGVudCkgKyBcIlxcbiAgICAgIFwiKV1cbiAgICAgICAgICApXG4gICAgICAgIF0pLFxuICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImt0LXN1YmhlYWRlcl9fdG9vbGJhclwiIH0pLFxuICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImt0LWhlYWRlcl9fdG9wYmFyXCIgfSwgW1xuICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwia3QtcGFnaW5hdGlvbiBrdC1wYWdpbmF0aW9uLS1icmFuZFwiIH0sIFtcbiAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwia3QtcGFnaW5hdGlvbl9fdG9vbGJhciBfbS1yLTEwXCIgfSwgW1xuICAgICAgICAgICAgICBfYyhcInNwYW5cIiwgeyBzdGF0aWNDbGFzczogXCJwYWdpbmF0aW9uX19kZXNjXCIgfSwgW1xuICAgICAgICAgICAgICAgIF92bS5fdihcbiAgICAgICAgICAgICAgICAgIFwiXFxuICAgICAgICAgICAgICBcIiArXG4gICAgICAgICAgICAgICAgICAgIF92bS5fcyhcbiAgICAgICAgICAgICAgICAgICAgICAoX3ZtLnBhZ2luZ1N0YXRlLnBhZ2UgLSAxKSAqIF92bS5wYWdpbmdTdGF0ZS5saW1pdCArXG4gICAgICAgICAgICAgICAgICAgICAgICBcIiAtIFwiICtcbiAgICAgICAgICAgICAgICAgICAgICAgIChfdm0ucGFnaW5nU3RhdGUucGFnZSAqIF92bS5wYWdpbmdTdGF0ZS5saW1pdCA8XG4gICAgICAgICAgICAgICAgICAgICAgICBfdm0ucGFnaW5nU3RhdGUudG90YWxSZWNvcmRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPyBfdm0ucGFnaW5nU3RhdGUucGFnZSAqIF92bS5wYWdpbmdTdGF0ZS5saW1pdFxuICAgICAgICAgICAgICAgICAgICAgICAgICA6IF92bS5wYWdpbmdTdGF0ZS50b3RhbFJlY29yZClcbiAgICAgICAgICAgICAgICAgICAgKSArXG4gICAgICAgICAgICAgICAgICAgIFwiIHRyb25nIHPhu5EgXCIgK1xuICAgICAgICAgICAgICAgICAgICBfdm0uX3MoX3ZtLnBhZ2luZ1N0YXRlLnRvdGFsUmVjb3JkKSArXG4gICAgICAgICAgICAgICAgICAgIFwiXFxuICAgICAgICAgIFwiXG4gICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgXSksXG4gICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgX2MoXCJ1bFwiLCB7IHN0YXRpY0NsYXNzOiBcImt0LXBhZ2luYXRpb25fX2xpbmtzXCIgfSwgW1xuICAgICAgICAgICAgICBfYyhcImxpXCIsIHsgc3RhdGljQ2xhc3M6IFwia3QtcGFnaW5hdGlvbl9fbGluay0tZmlyc3RcIiB9LCBbXG4gICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICBcImFcIixcbiAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgY2xhc3M6IHsgXCJpcy1kaXNhYmxlZFwiOiBfdm0ucGFnaW5nU3RhdGUucGFnZSA9PSAxIH0sXG4gICAgICAgICAgICAgICAgICAgIGF0dHJzOiB7IGhyZWY6IFwiI1wiIH0sXG4gICAgICAgICAgICAgICAgICAgIG9uOiB7XG4gICAgICAgICAgICAgICAgICAgICAgY2xpY2s6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgJGV2ZW50LnByZXZlbnREZWZhdWx0KClcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBfdm0uY2hhbmdlUGFnZSgxKVxuICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgICAgICAgX2MoXCJpXCIsIHtcbiAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJmYSBmYS1hbmdsZS1kb3VibGUtbGVmdCBrdC1mb250LWJyYW5kXCJcbiAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICBfYyhcImxpXCIsIHsgc3RhdGljQ2xhc3M6IFwia3QtcGFnaW5hdGlvbl9fbGluay0tcHJldlwiIH0sIFtcbiAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgIFwiYVwiLFxuICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBjbGFzczogeyBcImlzLWRpc2FibGVkXCI6IF92bS5wYWdpbmdTdGF0ZS5wYWdlID09IDEgfSxcbiAgICAgICAgICAgICAgICAgICAgYXR0cnM6IHsgaHJlZjogXCIjXCIgfSxcbiAgICAgICAgICAgICAgICAgICAgb246IHtcbiAgICAgICAgICAgICAgICAgICAgICBjbGljazogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkZXZlbnQucHJldmVudERlZmF1bHQoKVxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIF92bS5jaGFuZ2VQYWdlKF92bS5wYWdpbmdTdGF0ZS5wYWdlIC0gMSlcbiAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICBbX2MoXCJpXCIsIHsgc3RhdGljQ2xhc3M6IFwiZmEgZmEtYW5nbGUtbGVmdCBrdC1mb250LWJyYW5kXCIgfSldXG4gICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICBdKSxcbiAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgX2MoXCJsaVwiLCB7IHN0YXRpY0NsYXNzOiBcImt0LXBhZ2luYXRpb25fX2xpbmstLW5leHRcIiB9LCBbXG4gICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICBcImFcIixcbiAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgY2xhc3M6IHtcbiAgICAgICAgICAgICAgICAgICAgICBcImlzLWRpc2FibGVkXCI6XG4gICAgICAgICAgICAgICAgICAgICAgICBfdm0ucGFnaW5nU3RhdGUudG90YWxSZWNvcmQgPCBfdm0ucGFnaW5nU3RhdGUubGltaXQgfHxcbiAgICAgICAgICAgICAgICAgICAgICAgIF92bS5wYWdpbmdTdGF0ZS5wYWdlID09IF92bS5wYWdpbmdTdGF0ZS50b3RhbFBhZ2VcbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgYXR0cnM6IHsgaHJlZjogXCIjXCIgfSxcbiAgICAgICAgICAgICAgICAgICAgb246IHtcbiAgICAgICAgICAgICAgICAgICAgICBjbGljazogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkZXZlbnQucHJldmVudERlZmF1bHQoKVxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIF92bS5jaGFuZ2VQYWdlKF92bS5wYWdpbmdTdGF0ZS5wYWdlICsgMSlcbiAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICBbX2MoXCJpXCIsIHsgc3RhdGljQ2xhc3M6IFwiZmEgZmEtYW5nbGUtcmlnaHQga3QtZm9udC1icmFuZFwiIH0pXVxuICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgXSksXG4gICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgIF9jKFwibGlcIiwgeyBzdGF0aWNDbGFzczogXCJrdC1wYWdpbmF0aW9uX19saW5rLS1sYXN0XCIgfSwgW1xuICAgICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgICAgXCJhXCIsXG4gICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGNsYXNzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgXCJpcy1kaXNhYmxlZFwiOlxuICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLnBhZ2luZ1N0YXRlLnRvdGFsUmVjb3JkIDwgX3ZtLnBhZ2luZ1N0YXRlLmxpbWl0IHx8XG4gICAgICAgICAgICAgICAgICAgICAgICBfdm0ucGFnaW5nU3RhdGUucGFnZSA9PSBfdm0ucGFnaW5nU3RhdGUudG90YWxQYWdlXG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgIGF0dHJzOiB7IGhyZWY6IFwiI1wiIH0sXG4gICAgICAgICAgICAgICAgICAgIG9uOiB7XG4gICAgICAgICAgICAgICAgICAgICAgY2xpY2s6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgJGV2ZW50LnByZXZlbnREZWZhdWx0KClcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBfdm0uY2hhbmdlUGFnZShfdm0ucGFnaW5nU3RhdGUudG90YWxQYWdlKVxuICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgICAgICAgX2MoXCJpXCIsIHtcbiAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJmYSBmYS1hbmdsZS1kb3VibGUtcmlnaHQga3QtZm9udC1icmFuZFwiXG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgXSlcbiAgICAgICAgICBdKSxcbiAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgIF9jKFxuICAgICAgICAgICAgXCJkaXZcIixcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwia3QtaGVhZGVyX190b3BiYXItaXRlbSBrdC1oZWFkZXJfX3RvcGJhci1pdGVtLS11c2VyXCJcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBbXG4gICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgIFwiZGl2XCIsXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwia3QtaGVhZGVyX190b3BiYXItd3JhcHBlclwiLFxuICAgICAgICAgICAgICAgICAgYXR0cnM6IHsgXCJkYXRhLXRvZ2dsZVwiOiBcImRyb3Bkb3duXCIsIFwiZGF0YS1vZmZzZXRcIjogXCIwcHgsMHB4XCIgfVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgW1xuICAgICAgICAgICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJrdC1oZWFkZXJfX3RvcGJhci11c2VyXCIgfSwgW1xuICAgICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgICBcInNwYW5cIixcbiAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgXCJrdC1iYWRnZSBrdC1iYWRnZS0tdXNlcm5hbWUga3QtYmFkZ2UtLXVuaWZpZWQtc3VjY2VzcyBrdC1iYWRnZS0tbGcga3QtYmFkZ2UtLXJvdW5kZWQga3QtYmFkZ2UtLWJvbGRcIixcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY1N0eWxlOiB7IHdpZHRoOiBcIjEwMCVcIiwgcGFkZGluZzogXCIxMHB4XCIgfVxuICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgW192bS5fdihfdm0uX3MoX3ZtLkNEQVRBLmxvZ2luLmZ1bGxOYW1lKSldXG4gICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICApLFxuICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICBcImRpdlwiLFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOlxuICAgICAgICAgICAgICAgICAgICBcImRyb3Bkb3duLW1lbnUgZHJvcGRvd24tbWVudS1maXQgZHJvcGRvd24tbWVudS1yaWdodCBkcm9wZG93bi1tZW51LWFuaW0gZHJvcGRvd24tbWVudS10b3AtdW5yb3VuZCBkcm9wZG93bi1tZW51LXhsXCJcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwia3Qtbm90aWZpY2F0aW9uXCIgfSwgW1xuICAgICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgICBcImRpdlwiLFxuICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImt0LW5vdGlmaWNhdGlvbl9fY3VzdG9tIGt0LXNwYWNlLWJldHdlZW5cIlxuICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgW1xuICAgICAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgIFwiYVwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImJ0biBidG4tbGFiZWwgYnRuLWxhYmVsLWJyYW5kIGJ0bi1zbSBidG4tYm9sZFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF0dHJzOiB7IGhyZWY6IF92bS5DREFUQS51cmwubG9nb3V0VVJMIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgW192bS5fdihcIlNpZ24gT3V0XCIpXVxuICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICAgIClcbiAgICAgICAgICAgIF1cbiAgICAgICAgICApXG4gICAgICAgIF0pXG4gICAgICBdKVxuICAgIF1cbiAgKVxufVxudmFyIHN0YXRpY1JlbmRlckZucyA9IFtdXG5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcblxuZXhwb3J0IHsgcmVuZGVyLCBzdGF0aWNSZW5kZXJGbnMgfSIsInZhciByZW5kZXIgPSBmdW5jdGlvbigpIHtcbiAgdmFyIF92bSA9IHRoaXNcbiAgdmFyIF9oID0gX3ZtLiRjcmVhdGVFbGVtZW50XG4gIHZhciBfYyA9IF92bS5fc2VsZi5fYyB8fCBfaFxuICByZXR1cm4gX2MoXG4gICAgXCJkaXZcIixcbiAgICB7XG4gICAgICBzdGF0aWNDbGFzczpcbiAgICAgICAgXCJrdC1jb250ZW50ICBrdC1ncmlkX19pdGVtIGt0LWdyaWRfX2l0ZW0tLWZsdWlkIGt0LWdyaWQga3QtZ3JpZC0taG9yXCJcbiAgICB9LFxuICAgIFtcbiAgICAgIF9jKFxuICAgICAgICBcImRpdlwiLFxuICAgICAgICB7XG4gICAgICAgICAgc3RhdGljQ2xhc3M6IFwia3Qtc3ViaGVhZGVyICAga3QtZ3JpZF9faXRlbVwiLFxuICAgICAgICAgIGF0dHJzOiB7IGlkOiBcImt0X3N1YmhlYWRlclwiIH1cbiAgICAgICAgfSxcbiAgICAgICAgW1xuICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwia3QtY29udGFpbmVyICBrdC1jb250YWluZXItLWZsdWlkIFwiIH0sIFtcbiAgICAgICAgICAgIF92bS5fbSgwKSxcbiAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImt0LXN1YmhlYWRlcl9fdG9vbGJhclwiIH0pLFxuICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwia3QtaGVhZGVyX190b3BiYXJcIiB9LCBbXG4gICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgIFwiZGl2XCIsXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6XG4gICAgICAgICAgICAgICAgICAgIFwia3QtaGVhZGVyX190b3BiYXItaXRlbSBrdC1oZWFkZXJfX3RvcGJhci1pdGVtLS11c2VyXCJcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgICAgICBcImRpdlwiLFxuICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwia3QtaGVhZGVyX190b3BiYXItd3JhcHBlclwiLFxuICAgICAgICAgICAgICAgICAgICAgIGF0dHJzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBcImRhdGEtdG9nZ2xlXCI6IFwiZHJvcGRvd25cIixcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiZGF0YS1vZmZzZXRcIjogXCIwcHgsMHB4XCJcbiAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImt0LWhlYWRlcl9fdG9wYmFyLXVzZXJcIiB9LCBbXG4gICAgICAgICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgICAgICAgXCJzcGFuXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwia3QtaGVhZGVyX190b3BiYXItd2VsY29tZSBrdC1oaWRkZW4tbW9iaWxlXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgW192bS5fdihcIkhpLFwiKV1cbiAgICAgICAgICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgIFwic3BhblwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImt0LWhlYWRlcl9fdG9wYmFyLXVzZXJuYW1lIGt0LWhpZGRlbi1tb2JpbGVcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICBbX3ZtLl92KF92bS5fcyhfdm0uQ0RBVEEubG9naW4uZnVsbE5hbWUpKV1cbiAgICAgICAgICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJpbWdcIiwge1xuICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJrdC1oaWRkZW5cIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgYXR0cnM6IHsgYWx0OiBcIlBpY1wiLCBzcmM6IFwiXCIgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfSksXG4gICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgIFwic3BhblwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImt0LWJhZGdlIGt0LWJhZGdlLS11c2VybmFtZSBrdC1iYWRnZS0tdW5pZmllZC1zdWNjZXNzIGt0LWJhZGdlLS1sZyBrdC1iYWRnZS0tcm91bmRlZCBrdC1iYWRnZS0tYm9sZFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgIFtfdm0uX3YoXCJTXCIpXVxuICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgIFwiZGl2XCIsXG4gICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczpcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiZHJvcGRvd24tbWVudSBkcm9wZG93bi1tZW51LWZpdCBkcm9wZG93bi1tZW51LXJpZ2h0IGRyb3Bkb3duLW1lbnUtYW5pbSBkcm9wZG93bi1tZW51LXRvcC11bnJvdW5kIGRyb3Bkb3duLW1lbnUteGxcIlxuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJrdC1ub3RpZmljYXRpb25cIiB9LCBbXG4gICAgICAgICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgICAgICAgXCJkaXZcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJrdC1ub3RpZmljYXRpb25fX2N1c3RvbSBrdC1zcGFjZS1iZXR3ZWVuXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJhXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiYnRuIGJ0bi1sYWJlbCBidG4tbGFiZWwtYnJhbmQgYnRuLXNtIGJ0bi1ib2xkXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF0dHJzOiB7IGhyZWY6IF92bS5DREFUQS51cmwubG9nb3V0VVJMIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbX3ZtLl92KFwiU2lnbiBPdXRcIildXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgXSlcbiAgICAgICAgICBdKVxuICAgICAgICBdXG4gICAgICApXG4gICAgXVxuICApXG59XG52YXIgc3RhdGljUmVuZGVyRm5zID0gW1xuICBmdW5jdGlvbigpIHtcbiAgICB2YXIgX3ZtID0gdGhpc1xuICAgIHZhciBfaCA9IF92bS4kY3JlYXRlRWxlbWVudFxuICAgIHZhciBfYyA9IF92bS5fc2VsZi5fYyB8fCBfaFxuICAgIHJldHVybiBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImt0LXN1YmhlYWRlcl9fbWFpblwiIH0sIFtcbiAgICAgIF9jKFwic3BhblwiLCB7IHN0YXRpY0NsYXNzOiBcImt0LXN1YmhlYWRlcl9fc2VwYXJhdG9yIGt0LWhpZGRlblwiIH0pLFxuICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwia3Qtc3ViaGVhZGVyX19icmVhZGNydW1ic1wiIH0sIFtcbiAgICAgICAgX2MoXG4gICAgICAgICAgXCJhXCIsXG4gICAgICAgICAge1xuICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwia3Qtc3ViaGVhZGVyX19icmVhZGNydW1icy1ob21lXCIsXG4gICAgICAgICAgICBhdHRyczogeyBocmVmOiBcIi9iYWNrZW5kXCIgfVxuICAgICAgICAgIH0sXG4gICAgICAgICAgW19jKFwiaVwiLCB7IHN0YXRpY0NsYXNzOiBcImZsYXRpY29uMi1zaGVsdGVyXCIgfSldXG4gICAgICAgICksXG4gICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgIF9jKFwic3BhblwiLCB7IHN0YXRpY0NsYXNzOiBcImt0LXN1YmhlYWRlcl9fYnJlYWRjcnVtYnMtc2VwYXJhdG9yXCIgfSksXG4gICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgIF9jKFwic3BhblwiLCB7IHN0YXRpY0NsYXNzOiBcImt0LXN1YmhlYWRlcl9fYnJlYWRjcnVtYnMtbGlua1wiIH0sIFtcbiAgICAgICAgICBfdm0uX3YoXCJcXG4gICAgICAgICAgICAgIERhc2hib2FyZFxcbiAgICAgICAgICAgIFwiKVxuICAgICAgICBdKVxuICAgICAgXSlcbiAgICBdKVxuICB9XG5dXG5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcblxuZXhwb3J0IHsgcmVuZGVyLCBzdGF0aWNSZW5kZXJGbnMgfSIsImltcG9ydCBWdWUgZnJvbSAndnVlJ1xyXG5pbXBvcnQgVnVleCBmcm9tICd2dWV4J1xyXG5pbXBvcnQgVnVlUm91dGVyIGZyb20gJ3Z1ZS1yb3V0ZXInXHJcblxyXG5pbXBvcnQgQXBwIGZyb20gJyR2dWVQYXJ0aWFsUGF0aC9nbG9iYWwvQXBwJ1xyXG5pbXBvcnQgSW5kZXhTZWN0aW9uIGZyb20gJyR2dWVQYXJ0aWFsUGF0aC9pbmRleC9JbmRleFNlY3Rpb24nXHJcblxyXG5WdWUudXNlKFZ1ZXgpXHJcblZ1ZS51c2UoVnVlUm91dGVyKVxyXG5cclxuLy8gY29uc3Qgc3RvcmUgPSBuZXcgVnVleC5TdG9yZShTdG9yZSlcclxuY29uc3Qgcm91dGVyID0gbmV3IFZ1ZVJvdXRlcih7XHJcbiAgbW9kZTogJ2hpc3RvcnknLFxyXG4gIHJvdXRlczogW1xyXG4gICAgeyBuYW1lOiAnaW5kZXgnLCBwYXRoOiAnL2JhY2tlbmQqJywgY29tcG9uZW50OiBJbmRleFNlY3Rpb24gfSxcclxuICBdXHJcbn0pO1xyXG5cclxuY29uc3QgT2JqZWN0ID0gKCgpID0+IHtcclxuICBjb25zdCBfcmVuZGVyID0gKCkgPT4ge1xyXG4gICAgbGV0ICRlbCA9ICQoJyNjb250ZW50LXNlY3Rpb24nKVxyXG5cclxuICAgIG5ldyBWdWUoe1xyXG4gICAgICByb3V0ZXIsXHJcbiAgICAgIC8vIHN0b3JlLFxyXG4gICAgICByZW5kZXI6IGggPT4gaChBcHApXHJcbiAgICB9KS4kbW91bnQoJGVsWyAwIF0pXHJcbiAgfVxyXG5cclxuICByZXR1cm4ge1xyXG4gICAgaW5pdCgpIHtcclxuICAgICAgX3JlbmRlcigpXHJcbiAgICB9XHJcbiAgfVxyXG59KSgpXHJcblxyXG5kb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKFwiRE9NQ29udGVudExvYWRlZFwiLCBmdW5jdGlvbiAoKSB7XHJcbiAgT2JqZWN0LmluaXQoKVxyXG59KTtcclxuIiwiaW1wb3J0IHsgcmVuZGVyLCBzdGF0aWNSZW5kZXJGbnMgfSBmcm9tIFwiLi9BcHAudnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPWY3YzZiOWI2JlwiXG5pbXBvcnQgc2NyaXB0IGZyb20gXCIuL0FwcC52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCJcbmV4cG9ydCAqIGZyb20gXCIuL0FwcC52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCJcblxuXG4vKiBub3JtYWxpemUgY29tcG9uZW50ICovXG5pbXBvcnQgbm9ybWFsaXplciBmcm9tIFwiIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9ydW50aW1lL2NvbXBvbmVudE5vcm1hbGl6ZXIuanNcIlxudmFyIGNvbXBvbmVudCA9IG5vcm1hbGl6ZXIoXG4gIHNjcmlwdCxcbiAgcmVuZGVyLFxuICBzdGF0aWNSZW5kZXJGbnMsXG4gIGZhbHNlLFxuICBudWxsLFxuICBudWxsLFxuICBudWxsXG4gIFxuKVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkge1xuICB2YXIgYXBpID0gcmVxdWlyZShcIkQ6XFxcXFByb2plY3RcXFxcc2hhcmVcXFxcb3V0c291Y2VfdGh1ZW5oYVxcXFxodG1sXFxcXHN0YXRpY1xcXFxiYWNrZW5kXFxcXHYxXFxcXG5vZGVfbW9kdWxlc1xcXFx2dWUtaG90LXJlbG9hZC1hcGlcXFxcZGlzdFxcXFxpbmRleC5qc1wiKVxuICBhcGkuaW5zdGFsbChyZXF1aXJlKCd2dWUnKSlcbiAgaWYgKGFwaS5jb21wYXRpYmxlKSB7XG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICAgIGlmICghYXBpLmlzUmVjb3JkZWQoJ2Y3YzZiOWI2JykpIHtcbiAgICAgIGFwaS5jcmVhdGVSZWNvcmQoJ2Y3YzZiOWI2JywgY29tcG9uZW50Lm9wdGlvbnMpXG4gICAgfSBlbHNlIHtcbiAgICAgIGFwaS5yZWxvYWQoJ2Y3YzZiOWI2JywgY29tcG9uZW50Lm9wdGlvbnMpXG4gICAgfVxuICAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiLi9BcHAudnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPWY3YzZiOWI2JlwiLCBmdW5jdGlvbiAoKSB7XG4gICAgICBhcGkucmVyZW5kZXIoJ2Y3YzZiOWI2Jywge1xuICAgICAgICByZW5kZXI6IHJlbmRlcixcbiAgICAgICAgc3RhdGljUmVuZGVyRm5zOiBzdGF0aWNSZW5kZXJGbnNcbiAgICAgIH0pXG4gICAgfSlcbiAgfVxufVxuY29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJzcmMvdnVlL3BhcnRpYWwvZ2xvYmFsL0FwcC52dWVcIlxuZXhwb3J0IGRlZmF1bHQgY29tcG9uZW50LmV4cG9ydHMiLCJpbXBvcnQgbW9kIGZyb20gXCItIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9iYWJlbC1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2luZGV4LmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi9BcHAudnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiOyBleHBvcnQgZGVmYXVsdCBtb2Q7IGV4cG9ydCAqIGZyb20gXCItIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9iYWJlbC1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2luZGV4LmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi9BcHAudnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiIiwiZXhwb3J0ICogZnJvbSBcIi0hLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2xvYWRlcnMvdGVtcGxhdGVMb2FkZXIuanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvaW5kZXguanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuL0FwcC52dWU/dnVlJnR5cGU9dGVtcGxhdGUmaWQ9ZjdjNmI5YjYmXCIiLCJpbXBvcnQgeyByZW5kZXIsIHN0YXRpY1JlbmRlckZucyB9IGZyb20gXCIuL1RoZUJyZWFkY3J1bWIudnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPTU2NTY1M2Q4JlwiXG5pbXBvcnQgc2NyaXB0IGZyb20gXCIuL1RoZUJyZWFkY3J1bWIudnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiXG5leHBvcnQgKiBmcm9tIFwiLi9UaGVCcmVhZGNydW1iLnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIlxuXG5cbi8qIG5vcm1hbGl6ZSBjb21wb25lbnQgKi9cbmltcG9ydCBub3JtYWxpemVyIGZyb20gXCIhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3J1bnRpbWUvY29tcG9uZW50Tm9ybWFsaXplci5qc1wiXG52YXIgY29tcG9uZW50ID0gbm9ybWFsaXplcihcbiAgc2NyaXB0LFxuICByZW5kZXIsXG4gIHN0YXRpY1JlbmRlckZucyxcbiAgZmFsc2UsXG4gIG51bGwsXG4gIG51bGwsXG4gIG51bGxcbiAgXG4pXG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7XG4gIHZhciBhcGkgPSByZXF1aXJlKFwiRDpcXFxcUHJvamVjdFxcXFxzaGFyZVxcXFxvdXRzb3VjZV90aHVlbmhhXFxcXGh0bWxcXFxcc3RhdGljXFxcXGJhY2tlbmRcXFxcdjFcXFxcbm9kZV9tb2R1bGVzXFxcXHZ1ZS1ob3QtcmVsb2FkLWFwaVxcXFxkaXN0XFxcXGluZGV4LmpzXCIpXG4gIGFwaS5pbnN0YWxsKHJlcXVpcmUoJ3Z1ZScpKVxuICBpZiAoYXBpLmNvbXBhdGlibGUpIHtcbiAgICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gICAgaWYgKCFhcGkuaXNSZWNvcmRlZCgnNTY1NjUzZDgnKSkge1xuICAgICAgYXBpLmNyZWF0ZVJlY29yZCgnNTY1NjUzZDgnLCBjb21wb25lbnQub3B0aW9ucylcbiAgICB9IGVsc2Uge1xuICAgICAgYXBpLnJlbG9hZCgnNTY1NjUzZDgnLCBjb21wb25lbnQub3B0aW9ucylcbiAgICB9XG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoXCIuL1RoZUJyZWFkY3J1bWIudnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPTU2NTY1M2Q4JlwiLCBmdW5jdGlvbiAoKSB7XG4gICAgICBhcGkucmVyZW5kZXIoJzU2NTY1M2Q4Jywge1xuICAgICAgICByZW5kZXI6IHJlbmRlcixcbiAgICAgICAgc3RhdGljUmVuZGVyRm5zOiBzdGF0aWNSZW5kZXJGbnNcbiAgICAgIH0pXG4gICAgfSlcbiAgfVxufVxuY29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJzcmMvdnVlL3BhcnRpYWwvZ2xvYmFsL1RoZUJyZWFkY3J1bWIudnVlXCJcbmV4cG9ydCBkZWZhdWx0IGNvbXBvbmVudC5leHBvcnRzIiwiaW1wb3J0IG1vZCBmcm9tIFwiLSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvYmFiZWwtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9pbmRleC5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4vVGhlQnJlYWRjcnVtYi52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCI7IGV4cG9ydCBkZWZhdWx0IG1vZDsgZXhwb3J0ICogZnJvbSBcIi0hLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2JhYmVsLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvaW5kZXguanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuL1RoZUJyZWFkY3J1bWIudnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiIiwiZXhwb3J0ICogZnJvbSBcIi0hLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2xvYWRlcnMvdGVtcGxhdGVMb2FkZXIuanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvaW5kZXguanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuL1RoZUJyZWFkY3J1bWIudnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPTU2NTY1M2Q4JlwiIiwiaW1wb3J0IHsgcmVuZGVyLCBzdGF0aWNSZW5kZXJGbnMgfSBmcm9tIFwiLi9JbmRleFNlY3Rpb24udnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPWU4MjRhODAwJlwiXG5pbXBvcnQgc2NyaXB0IGZyb20gXCIuL0luZGV4U2VjdGlvbi52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCJcbmV4cG9ydCAqIGZyb20gXCIuL0luZGV4U2VjdGlvbi52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCJcblxuXG4vKiBub3JtYWxpemUgY29tcG9uZW50ICovXG5pbXBvcnQgbm9ybWFsaXplciBmcm9tIFwiIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9ydW50aW1lL2NvbXBvbmVudE5vcm1hbGl6ZXIuanNcIlxudmFyIGNvbXBvbmVudCA9IG5vcm1hbGl6ZXIoXG4gIHNjcmlwdCxcbiAgcmVuZGVyLFxuICBzdGF0aWNSZW5kZXJGbnMsXG4gIGZhbHNlLFxuICBudWxsLFxuICBudWxsLFxuICBudWxsXG4gIFxuKVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkge1xuICB2YXIgYXBpID0gcmVxdWlyZShcIkQ6XFxcXFByb2plY3RcXFxcc2hhcmVcXFxcb3V0c291Y2VfdGh1ZW5oYVxcXFxodG1sXFxcXHN0YXRpY1xcXFxiYWNrZW5kXFxcXHYxXFxcXG5vZGVfbW9kdWxlc1xcXFx2dWUtaG90LXJlbG9hZC1hcGlcXFxcZGlzdFxcXFxpbmRleC5qc1wiKVxuICBhcGkuaW5zdGFsbChyZXF1aXJlKCd2dWUnKSlcbiAgaWYgKGFwaS5jb21wYXRpYmxlKSB7XG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICAgIGlmICghYXBpLmlzUmVjb3JkZWQoJ2U4MjRhODAwJykpIHtcbiAgICAgIGFwaS5jcmVhdGVSZWNvcmQoJ2U4MjRhODAwJywgY29tcG9uZW50Lm9wdGlvbnMpXG4gICAgfSBlbHNlIHtcbiAgICAgIGFwaS5yZWxvYWQoJ2U4MjRhODAwJywgY29tcG9uZW50Lm9wdGlvbnMpXG4gICAgfVxuICAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiLi9JbmRleFNlY3Rpb24udnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPWU4MjRhODAwJlwiLCBmdW5jdGlvbiAoKSB7XG4gICAgICBhcGkucmVyZW5kZXIoJ2U4MjRhODAwJywge1xuICAgICAgICByZW5kZXI6IHJlbmRlcixcbiAgICAgICAgc3RhdGljUmVuZGVyRm5zOiBzdGF0aWNSZW5kZXJGbnNcbiAgICAgIH0pXG4gICAgfSlcbiAgfVxufVxuY29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJzcmMvdnVlL3BhcnRpYWwvaW5kZXgvSW5kZXhTZWN0aW9uLnZ1ZVwiXG5leHBvcnQgZGVmYXVsdCBjb21wb25lbnQuZXhwb3J0cyIsImltcG9ydCBtb2QgZnJvbSBcIi0hLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2JhYmVsLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvaW5kZXguanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuL0luZGV4U2VjdGlvbi52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCI7IGV4cG9ydCBkZWZhdWx0IG1vZDsgZXhwb3J0ICogZnJvbSBcIi0hLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2JhYmVsLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvaW5kZXguanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuL0luZGV4U2VjdGlvbi52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCIiLCJleHBvcnQgKiBmcm9tIFwiLSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvbG9hZGVycy90ZW1wbGF0ZUxvYWRlci5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9pbmRleC5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4vSW5kZXhTZWN0aW9uLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD1lODI0YTgwMCZcIiJdLCJzb3VyY2VSb290IjoiIn0=