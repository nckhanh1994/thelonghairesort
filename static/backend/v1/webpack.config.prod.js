//Load config
const WEB_CONFIG = require('./web.config');

const glob = require('glob');

// Plugin
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const WriteFilePlugin = require('write-file-webpack-plugin');
const sass = require('node-sass');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');

const WEBPACK_CONFIG = {
  entry: WEB_CONFIG.webpackConfig.entry,
  output: {
    filename: 'js/[name].js',
    path: WEB_CONFIG.distPath,
  },
  resolve: WEB_CONFIG.webpackConfig.resolves,
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.js$/,
        exclude: /node_modules\/(?!(dom7|ssr-window|swiper|bootstrap|query-string|strict-uri-encode)\/).*/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [ '@babel/preset-env' ]
          }
        }
      },
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              url: false,
              sourceMap: true
            }
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true,
              includePaths: [ WEB_CONFIG.webPath.src.scss, WEB_CONFIG.nodeModulesPath ],
            }
          }
        ],
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: WEB_CONFIG.webPath.dist.font,
            }
          }
        ]
      },
      {
        /**
         * Loader này giúp mở các lib cho bên ngoài webpack sử dụng
         */
        test: require.resolve('jquery'),
        use: [
          {
            loader: 'expose-loader',
            options: 'jQuery',
          },
          {
            loader: 'expose-loader',
            options: '$',
          }
        ],
      }
    ], //end rules
  }, //end module
  optimization: {
    splitChunks: WEB_CONFIG.webpackConfig.optimization.splitChunks,
    minimizer: [
      new UglifyJsPlugin(),
      new OptimizeCSSAssetsPlugin({}),
    ],
  },
  devtool: 'inline-source-map',
  performance: {
    hints: false
  },
  stats: {
    all: false,
    errors: true,
    errorDetails: true,
    assets: true,
    colors: true,
    timings: true,
    warnings: true,
  },
  plugins: [
    new CleanWebpackPlugin({
      cleanOnceBeforeBuildPatterns: [
        WEB_CONFIG.distPath,
      ],
      cleanAfterEveryBuildPatterns: [
        WEB_CONFIG.webPath.dist.css + '/**/*.css.map',
        WEB_CONFIG.webPath.dist.js + '/**/*-style.js',
        WEB_CONFIG.webPath.dist.js + '/**/*.js.map',
        WEB_CONFIG.webPath.dist.js + '/**/*.js.LICENSE'
      ],
    }),

    new WriteFilePlugin({
      test: /\.(.*)$/,
      useHashIndex: true,
      log: true,
    }),

    new MiniCssExtractPlugin({
      path: WEB_CONFIG.webPath.dist.css,
      filename: 'css/[name].css',
      chunkFilename: 'css/[name]_chunk.css',
    }),

    new webpack.ProvidePlugin(WEB_CONFIG.webpackConfig.plugins.providePlugins), // Automatically load modules instead of having to 'import' or 'require' them everywhere.

    new VueLoaderPlugin()
  ], // end plugins
}; // end WEB_CONFIG

// Export to run
// ---------------------------
module.exports = WEBPACK_CONFIG;
