import { forIn as _forIn, isEmpty as _isEmpty } from 'lodash';

import * as API from '$jsCommonPath/api'
import { isErrorAPI } from "$jsCommonPath/util/GeneralUtil";
import { _int } from "$jsCommonPath/util/SafeUtil";

let _objEntity = {
  role_id: null,
  role_name: null,
  is_fullaccess: null,
  is_deleted: null,
  created_at: 0,
  updated_at: 0,
  max_price: ''
}

class Store {
  constructor() {
    return {
      state: {
        option: {
          isReady: false,
          isGetRolePermission: false
        },
        entity: {
          arrEntityList: [],
          objEntitySelect: { ..._objEntity },
        },
        permission: {
          objResources: typeof PAGE_DATA == 'undefined' ? [] : PAGE_DATA.arrResources,
          arrRolePermissionList: [],
        }
      },
      mutations: {
        resetEntity(state) {
          state.entity.objEntitySelect = { ..._objEntity }
        },
        setOptionState(state, payload) {
          state.option[ payload.field ] = payload.value
        },
        setEntities(state, payload) {
          state.entity.arrEntityList = payload.arrEntityList
        },
        setEntity(state, payload) {
          if (_isEmpty(payload.objEntitySelect))
            return

          let { objEntitySelect } = { ...payload }

          objEntitySelect.password = null

          state.entity.objEntitySelect = objEntitySelect
        },
        updateEntity(state, payload) {
          let { type, index, arrEntity } = { ...payload }

          console.log(type)
          switch (type) {
            case 'create':
              state.entity.arrEntityList.unshift(arrEntity)
              break;
            case 'update':
              state.entity.arrEntityList[ index ] = arrEntity
              state.entity.objEntitySelect = arrEntity

              break;
            case 'delete':
              console.log(state.entity.arrEntityList, index)
              state.entity.arrEntityList.splice(index, 1)
              break;
          }
        },
        setRolePermission(state, payload) {
          let strType = payload.type;
          let objRolePermission = payload.rolePermission

          if (strType == 'create') {
            if (!state.permission.arrRolePermissionList.hasOwnProperty(objRolePermission.controller)) {
              state.permission.arrRolePermissionList[ objRolePermission.controller ] = {}
            }

            if (!state.permission.arrRolePermissionList[ objRolePermission.controller ].hasOwnProperty(objRolePermission.action)) {
              state.permission.arrRolePermissionList[ objRolePermission.controller ][ objRolePermission.action ] = {}
            }

            state.permission.arrRolePermissionList[ objRolePermission.controller ][ objRolePermission.action ] = objRolePermission;
          } else {
            let [ strModule, controller, action ] = payload.resource.split(':');
            let temp = { ...state.permission.arrRolePermissionList[ controller ] };
            delete temp[ action ]

            state.permission.arrRolePermissionList[ controller ] = temp;
          }
        },
        setRolePermissionList(state, payload) {
          state.permission.arrRolePermissionList = payload.rolePermissionList
          state.option.isGetRolePermission = true
        },
      },
      actions: {
        getEntityAct({ commit }, payload) {
          return new Promise(function (resolve, reject) {
            let reqParams = { ...payload }
            API.getRoleReq(reqParams).done((res) => {
              if (isErrorAPI(res)) {
                reject(res)
                return
              }

              let { arrRoleDetail } = { ...res.payload }

              commit('setEntity', { objEntitySelect: arrRoleDetail })

              resolve(res)
            })
          })
        },
        readEntityAct({ commit }) {
          return new Promise(function (resolve, reject) {
            let reqParams = {}
            API.readRoleReq(reqParams).done((res) => {
              if (isErrorAPI(res)) {
                reject(res)
                return
              }

              let { roleList } = { ...res.payload }

              commit('setEntities', { arrEntityList: roleList })

              resolve(res)
            })
          })
        },
        uploadFileAct({ commit }, payload) {
          return new Promise(function (resolve, reject) {
            let formData = new FormData

            formData.append('controller', payload.controller)
            formData.append('file', payload.file)

            API.uploadReq(formData, {
              processData: false,
              contentType: false,
            }).done(res => {
              if (isErrorAPI(res)) {

                reject(res)
                return
              }

              resolve(res)
            })
          })
        },
        setEntityAct({ commit, getters }, payload) {
          if (!payload.hasOwnProperty('index'))
            return

          let { index } = { ...payload }

          commit('setEntity', { objEntitySelect: getters.getEntityByIndex(index)[ 0 ] })
        },
        createEntityAct({ commit, getters }, payload) {
          return new Promise(function (resolve, reject) {
            let reqParams = { ...payload }
            API.createRoleReq(reqParams).done((res) => {
              if (isErrorAPI(res)) {
                reject(res)
                return
              }

              let { arrRoleDetail } = { ...res.payload }

              commit('updateEntity', {
                type: 'create',
                arrEntity: arrRoleDetail
              })

              resolve(res)
            })
          })
        },
        updateEntityAct({ commit, getters }, payload) {
          return new Promise(function (resolve, reject) {
            let reqParams = { ...payload }

            API.updateRoleReq(reqParams).done((res) => {
              if (isErrorAPI(res)) {
                reject(res)
                return
              }

              let { arrRoleDetail } = { ...res.payload }

              commit('updateEntity', {
                type: 'update',
                index: getters.getIndexByID(arrRoleDetail.role_id),
                arrEntity: arrRoleDetail
              })

              resolve(res)
            })
          })
        },
        deleteEntityAct({ commit, getters }, payload) {
          return new Promise(function (resolve, reject) {
            let reqParams = { ...payload }

            API.deleteRoleReq(reqParams).done(res => {
              if (isErrorAPI(res)) {
                reject(res)

                return
              }

              let { id } = { ...res.payload }

              commit('updateEntity', {
                type: 'delete',
                index: getters.getIndexByID(id)
              })

              resolve(res)
            })
          })
        },
        getRolePermissionAct({ commit, getters }, payload) {
          return new Promise(function (resolve, reject) {
            let reqParams = { ...payload }

            API.getRolePermissionReq(reqParams).done(res => {
              if (isErrorAPI(res)) {
                reject(res)

                return
              }

              let { rolePermissionList } = { ...res.payload }

              commit('setRolePermissionList', { rolePermissionList })

              resolve(res)
            })
          })
        },
        createRolePermissionAct({ commit, getters }, payload) {
          return new Promise(function (resolve, reject) {
            let reqParams = {
              id: payload.id,
              resource: payload.resource
            }

            API.createRolePermissionReq(reqParams).done(res => {
              if (isErrorAPI(res)) {
                reject(res)

                return
              }

              let { rolePermission } = { ...res.payload }

              commit('setRolePermission', {
                type: 'create',
                rolePermission
              })

              resolve(res)
            })
          })
        },
        deleteRolePermissionAct({ commit, getters }, payload) {
          return new Promise(function (resolve, reject) {
            let reqParams = {
              id: payload.id,
              resource: payload.resource
            }

            API.deleteRolePermissionReq(reqParams).done(res => {
              if (isErrorAPI(res)) {
                reject(res)

                return
              }

              let { rolePermission } = { ...res.payload }

              commit('setRolePermission', {
                type: 'delete',
                resource: payload.resource
              })

              resolve(res)
            })
          })
        },
      },
      getters: {
        getEntityByIndex: state => index => {
          return state.entity.arrEntityList.filter((objEntity, currentIndex) => currentIndex == index)
        },
        getIndexByID: state => id => {
          let index = -1
          _forIn(state.entity.arrEntityList, (objEntity, currentIndex) => {
            if (objEntity.role_id != id)
              return

            index = currentIndex
            return false
          })

          return index
        }
      }
    }
  }
}

export default (new Store());


