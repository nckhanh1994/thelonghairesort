import { forIn as _forIn, isEmpty as _isEmpty } from 'lodash';

import * as API from '$jsCommonPath/api'
import { isErrorAPI, _parseQuery } from "$jsCommonPath/util/GeneralUtil";
import { _int, _isEmptyString } from "$jsCommonPath/util/SafeUtil";

let _objEntity = {
  item_id: '',
  item_name: '',
  item_feature_image: '',
  item_image_list: [],
  item_description: '',
  item_content: '',
  floor_area: '',
  floor_area_unit: '',
  host_phone: '',
  host_phone_support: '',
  price: '',
  currency: 'milionvnd',
  commission: '',
  category_id: 1,
  city_id: '29',
  district_id: '',
  ward_id: '',
  street_id: '',
  city_name: '',
  district_name: '',
  ward_name: '',
  street_name: '',
  address: '',
  is_cart: 1,
  status: 0,
  created_at: 0,
  updated_at: 0,
  is_deleted: 0,
  meta_title: '',
  meta_description: '',
  meta_keyword: '',
  user_created_id: 0,
  is_favorite: 0,
  is_promotion: 0,
  promotion_price: 0,
  is_private: 0,
  note: ''
}

class Store {
  constructor() {
    return {
      state: {
        option: {
          isReady: false
        },
        filterState: {
          queryString: '',
          arrQuery: {
            price_vnd_from: '',
            price_vnd_to: ''
          }
        },
        entity: {
          arrEntityList: [],
          objEntitySelect: { ..._objEntity },
        },
        address: {
          arrCityList: [],
          arrDistrictList: [],
          arrWardList: [],
          arrStreetList: [],
        },
        userState: {
          userList: {},
        },
      },
      mutations: {
        resetEntity(state) {
          state.entity.objEntitySelect = { ..._objEntity }
        },
        setOptionState(state, payload) {
          state.option[ payload.field ] = payload.value;
        },
        setFilterState(state, payload) {
          state.filterState[ payload.field ] = payload.value;

          if (_isEmptyString(payload.value) && payload.field == 'queryString')
            return;

          state.filterState.arrQuery = _parseQuery(payload.value);
        },
        setEntities(state, payload) {
          state.entity.arrEntityList = payload.arrEntityList
        },
        setEntity(state, payload) {
          if (_isEmpty(payload.objEntitySelect))
            return

          let { objEntitySelect } = { ...payload }

          objEntitySelect.password = null

          state.entity.objEntitySelect = objEntitySelect
        },
        updateEntity(state, payload) {
          let { type, index, arrEntity } = { ...payload }

          switch (type) {
            case 'create':
              state.entity.arrEntityList.unshift(arrEntity)
              break;
            case 'update':
              state.entity.arrEntityList[ index ] = arrEntity
              state.entity.objEntitySelect = arrEntity

              break;
            case 'delete':
              state.entity.arrEntityList.splice(index, 1)
              break;
          }
        },
        setCities(state, payload) {
          if (!payload.hasOwnProperty('arrCityList'))
            return

          state.address.arrCityList = payload.arrCityList
        },
        setDistricts(state, payload) {
          if (!payload.hasOwnProperty('arrDistrictList'))
            return

          state.address.arrDistrictList = payload.arrDistrictList
        },
        setWards(state, payload) {
          if (!payload.hasOwnProperty('arrWardList'))
            return

          state.address.arrWardList = payload.arrWardList
        },
        setStreets(state, payload) {
          state.address.arrStreetList = payload.arrStreetList
        },
        setUsers(state, payload) {
          let objTemp = {}
          let arrTemp = payload.userList

          _forIn(arrTemp, (objUser, index) => {
            objTemp[ objUser.user_id ] = objUser
          })

          state.userState.userList = objTemp
        }
      },
      actions: {
        getEntityAct({ commit }, payload) {
          return new Promise(function (resolve, reject) {
            let reqParams = { ...payload }
            API.getItemReq(reqParams).done((res) => {
              if (isErrorAPI(res)) {
                reject(res)
                return
              }

              let { arrItemDetail } = { ...res.payload }

              commit('setEntity', { objEntitySelect: arrItemDetail })

              resolve(res)
            })
          })
        },
        readEntityAct({ commit }, payload = {}) {
          return new Promise(function (resolve, reject) {
            let reqParams = {
              page: payload.page,
              queryString: payload.hasOwnProperty('queryString') ? payload.queryString : '',
              roleID: CDATA.login.ROLE_ID,
              userCreatedID: CDATA.login.UID,
              maxPrice: CDATA.login.MAX_PRICE
            }

            API.readItemReq(reqParams).done((res) => {
              if (isErrorAPI(res)) {
                reject(res)
                return
              }

              let { itemList } = { ...res.payload }

              commit('setEntities', { arrEntityList: itemList })

              resolve(res)
            })
          })
        },
        uploadFileAct({ commit }, payload) {
          return new Promise(function (resolve, reject) {
            let formData = new FormData

            formData.append('controller', payload.controller)
            formData.append('file', payload.file)

            API.uploadReq(formData, {
              processData: false,
              contentType: false,
            }).done(res => {
              if (isErrorAPI(res)) {

                reject(res)
                return
              }

              resolve(res)
            })
          })
        },
        setEntityAct({ commit, getters }, payload) {
          if (!payload.hasOwnProperty('index'))
            return

          let { index } = { ...payload }

          commit('setEntity', { objEntitySelect: getters.getEntityByIndex(index)[ 0 ] })
        },
        createEntityAct({ commit, getters }, payload) {
          return new Promise(function (resolve, reject) {
            let reqParams = { ...payload }
            API.createItemReq(reqParams).done((res) => {
              if (isErrorAPI(res)) {
                reject(res)
                return
              }

              let { arrItemDetail } = { ...res.payload }

              commit('updateEntity', {
                type: 'create',
                arrEntity: arrItemDetail
              })

              resolve(res)
            })
          })
        },
        updateEntityAct({ commit, getters }, payload) {
          return new Promise(function (resolve, reject) {
            let reqParams = { ...payload }

            API.updateItemReq(reqParams).done((res) => {
              if (isErrorAPI(res)) {
                reject(res)
                return
              }

              let { arrItemDetail } = { ...res.payload }

              commit('updateEntity', {
                type: 'update',
                index: getters.getIndexByID(arrItemDetail.item_id),
                arrEntity: arrItemDetail
              })

              resolve(res)
            })
          })
        },
        deleteEntityAct({ commit, getters }, payload) {
          return new Promise(function (resolve, reject) {
            let reqParams = { ...payload }

            API.deleteItemReq(reqParams).done(res => {
              if (isErrorAPI(res)) {
                reject(res)

                return
              }

              let { id } = { ...res.payload }

              commit('updateEntity', {
                type: 'delete',
                index: getters.getIndexByID(id)
              })

              resolve(res)
            })
          })
        },
        readCityListAct({ commit }, payload) {
          let reqParams = { ...payload }

          API.readCityReq(reqParams, {
            isDirect: true
          }).done(res => {
            if (isErrorAPI(res)) {
              return;
            }

            let { arrCityList } = { ...res.payload }

            commit('setCities', { arrCityList })
          })
        },
        readDistrictAct({ commit }, payload) {
          commit('setDistricts', [])
          commit('setWards', [])

          var cityID = _int(payload.cityID)
          if (!cityID)
            return

          let reqParams = {
            cityID: cityID
          }

          API.readDistrictReq(reqParams, {
            isDirect: true
          }).done(res => {
            if (isErrorAPI(res)) {
              commit('setDistricts', [])
              return '';
            }

            let { arrDistrictList } = { ...res.payload }

            commit('setDistricts', { arrDistrictList })
          })
        },
        readWardAct({ commit }, payload) {
          commit('setWards', [])

          var districtID = _int(payload.districtID)
          if (!districtID)
            return

          let reqParams = {
            districtID: districtID
          }

          API.readWardReq(reqParams, {
            isDirect: true
          }).done(res => {
            if (isErrorAPI(res)) {
              commit('setWards', [])

              return '';
            }

            let { arrWardList } = { ...res.payload }

            commit('setWards', { arrWardList })
          })
        },
        readStreetAct({ commit }, payload) {
          commit('setStreets', [])

          var districtID = _int(payload.districtID)
          if (!districtID)
            return

          let reqParams = {
            districtID: districtID
          }

          API.readStreetReq(reqParams, {
            isDirect: true
          }).done(res => {
            if (isErrorAPI(res)) {
              commit('setStreets', [])

              return '';
            }

            let { arrStreetList } = { ...res.payload }

            commit('setStreets', { arrStreetList })
          })
        },
        readUserAct({ commit, getters }, payload) {
          return new Promise(function (resolve, reject) {
            let reqParams = {}
            API.readUserReq(reqParams).done((res) => {
              if (isErrorAPI(res)) {
                reject(res)
                return
              }

              let { userList } = { ...res.payload }

              commit('setUsers', { userList })

              resolve(res)
            })
          })
        }
      },
      getters: {
        getEntityByIndex: state => index => {
          return state.entity.arrEntityList.filter((objEntity, currentIndex) => currentIndex == index)
        },
        getIndexByID: state => id => {
          let index = -1
          _forIn(state.entity.arrEntityList, (objEntity, currentIndex) => {
            if (objEntity.item_id != id)
              return

            index = currentIndex
            return false
          })

          return index
        }
      }
    }
  }
}

export default (new Store());


