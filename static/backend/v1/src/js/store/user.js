import { forIn as _forIn, isEmpty as _isEmpty } from 'lodash';

import * as API from '$jsCommonPath/api'
import { isErrorAPI } from "$jsCommonPath/util/GeneralUtil";
import { _int } from "$jsCommonPath/util/SafeUtil";

let _objEntity = {
  user_id: null,
  profile_image: null,
  fullname: null,
  email: null,
  phone: null,
  is_actived: 0,
  role_id: 1,
}

class Store {
  constructor() {
    return {
      state: {
        option: {
          isReady: false
        },
        entity: {
          arrEntityList: [],
          objEntitySelect: { ..._objEntity },
        },
        roleState: {
          isReady: false,
          arrRoleList: []
        }
      },
      mutations: {
        resetEntity(state) {
          state.entity.objEntitySelect = { ..._objEntity }
        },
        setOptionState(state, payload) {
          state.option[ payload.field ] = payload.value
        },
        updateRoleState(state, payload) {
          state.roleState[ payload.field ] = payload.value
        },
        setEntities(state, payload) {
          state.entity.arrEntityList = payload.arrEntityList
        },
        setRoles(state, payload) {
          state.roleState.arrRoleList = payload.roleList
        },
        setEntity(state, payload) {
          if (_isEmpty(payload.objEntitySelect))
            return

          let { objEntitySelect } = { ...payload }

          objEntitySelect.password = null

          state.entity.objEntitySelect = objEntitySelect
        },
        updateEntity(state, payload) {
          let { type, index, arrEntity } = { ...payload }

          switch (type) {
            case 'create':
              state.entity.arrEntityList.unshift(arrEntity)
              break;
            case 'update':
              state.entity.arrEntityList[ index ] = arrEntity
              state.entity.objEntitySelect = arrEntity

              break;
            case 'delete':
              state.entity.arrEntityList.splice(index, 1)
              break;
          }
        }
      },
      actions: {
        getEntityAct({ commit }, payload) {
          return new Promise(function (resolve, reject) {
            let reqParams = { ...payload }
            API.getUserReq(reqParams).done((res) => {
              if (isErrorAPI(res)) {
                reject(res)
                return
              }

              let { arrUserDetail } = { ...res.payload }

              commit('setEntity', { objEntitySelect: arrUserDetail })

              resolve(res)
            })
          })
        },
        readEntityAct({ commit }) {
          // KTUserListDatatable.init();
          return new Promise(function (resolve, reject) {
            let reqParams = {}
            API.readUserReq(reqParams).done((res) => {
              if (isErrorAPI(res)) {
                reject(res)
                return
              }

              let { userList } = { ...res.payload }

              commit('setEntities', { arrEntityList: userList })

              resolve(res)
            })
          })
        },
        readRoleAct({ commit }) {
          return new Promise(function (resolve, reject) {
            let reqParams = {}
            API.readRoleReq(reqParams).done((res) => {
              if (isErrorAPI(res)) {
                reject(res)
                return
              }

              let { roleList } = { ...res.payload }

              commit('setRoles', { roleList })

              resolve(res)
            })
          })
        },
        uploadFileAct({ commit }, payload) {
          return new Promise(function (resolve, reject) {
            let formData = new FormData

            formData.append('controller', payload.controller)
            formData.append('file', payload.file)

            API.uploadReq(formData, {
              processData: false,
              contentType: false,
            }).done(res => {
              if (isErrorAPI(res)) {

                reject(res)
                return
              }

              resolve(res)
            })
          })
        },
        setEntityAct({ commit, getters }, payload) {
          if (!payload.hasOwnProperty('index'))
            return

          let { index } = { ...payload }

          commit('setEntity', { objEntitySelect: getters.getEntityByIndex(index)[ 0 ] })
        },
        createEntityAct({ commit, getters }, payload) {
          return new Promise(function (resolve, reject) {
            let reqParams = { ...payload }
            API.createUserReq(reqParams).done((res) => {
              if (isErrorAPI(res)) {
                reject(res)
                return
              }

              let { arrUserDetail } = { ...res.payload }

              commit('updateEntity', {
                type: 'create',
                arrEntity: arrUserDetail
              })

              resolve(res)
            })
          })
        },
        updateEntityAct({ commit, getters }, payload) {
          return new Promise(function (resolve, reject) {
            let reqParams = { ...payload }

            API.updateUserReq(reqParams).done((res) => {
              if (isErrorAPI(res)) {
                reject(res)
                return
              }

              let { arrUserDetail } = { ...res.payload }

              commit('updateEntity', {
                type: 'update',
                index: getters.getIndexByID(arrUserDetail.user_id),
                arrEntity: arrUserDetail
              })

              resolve(res)
            })
          })
        },
        deleteEntityAct({ commit, getters }, payload) {
          return new Promise(function (resolve, reject) {
            let reqParams = { ...payload }

            API.deleteUserReq(reqParams).done(res => {
              if (isErrorAPI(res)) {
                reject(res)

                return
              }

              let { id } = { ...res.payload }

              console.log(id)

              commit('updateEntity', {
                type: 'delete',
                index: getters.getIndexByID(id)
              })

              resolve(res)
            })
          })
        },
      },
      getters: {
        getEntityByIndex: state => index => {
          return state.entity.arrEntityList.filter((objEntity, currentIndex) => currentIndex == index)
        },
        getIndexByID: state => id => {
          let index = -1
          _forIn(state.entity.arrEntityList, (objEntity, currentIndex) => {
            if (objEntity.user_id != id)
              return

            index = currentIndex
            return false
          })

          return index
        }
      }
    }
  }
}

export default (new Store());


