export const INT_CODE_SUCCESS_MIN_VALUE = 1; // Giá trị tối thiểu của mã thành công
export const INT_CODE_ERROR_MAX_VALUE = -1; // Giá trị tối đa của mã lỗi
export const INT_CODE_SUCCESS = 1; // Mã thành công chung
export const INT_CODE_ERROR = -1; // Mã lỗi chung
export const INT_CODE_SUCCESS_API = 200; // Mã thành công từ phía cung cấp API
// Đối với các mã -99 <= code <= -2 đều là lỗi từ phía sử dụng API
export const INT_CODE_ERROR_REQUEST_PARAM = -10; // Thông báo param truyền từ phía request API có vấn đề. VD: thiếu tham số, tham số sai,....
// Đối với mã -199 <= code <= -100 đều là lỗi từ phía cung cấp API.
export const INT_CODE_ERROR_API = -100; // Mã lỗi chung phía cung cấp API. VD: curl lỗi, requet API từ nơi khác lỗi, data bot trả ra lỗi, database lỗi, server sập...
// Đối với mã -299 <= code <= -200 đều là lỗi liên quan vấn đề xác thực thông tin
export const INT_CODE_ERROR_AUTH = -200; // lỗi chung liên quan vấn đề xác thực thông tin. VD: chưa đăng nhập, bla bla,...
export const INT_CODE_ERROR_PERMISSION = -403; // Lỗi quyền hạn người dùng