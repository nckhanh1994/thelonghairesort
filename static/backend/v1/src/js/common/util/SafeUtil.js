import { isObject as _isObject, isEmpty as _isEmpty, forEach as _forEach } from 'lodash';

export const _isEmptyString = (v) => {
  return (
      (typeof v == 'undefined') || (v == null) || (v == false) || (v.length == 0) || (v == "") || (v.replace(/\s/g, "") == "") || (!/[^\s]/.test(v)) || (/^\s*$/.test(v))
  )
}

export const _int = (v, def) => {
  v = parseInt(v)

  v = !isNaN(v) ? v : def

  return v
}

export const _float = (v, def) => {
  v = parseFloat(v)

  v = !isNaN(v) ? v : def

  return v
}

export const _objectSlice = (obj, offset, limit) => {
  let objResult = Object.keys(obj).slice(offset, limit).reduce((result, key) => {
    result[ key ] = obj[ key ];

    return result;
  }, {});

  return objResult
}