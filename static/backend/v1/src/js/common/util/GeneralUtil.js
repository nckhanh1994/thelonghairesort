import { EVENT_BUS_KEY, API_URL } from '$jsBasePath/define'
import EventBus from '$jsBasePath/EventBus';

import { INT_CODE_SUCCESS } from '$jsCommonPath/enum/response'

export const _showContainer = () => {
  $('.kt-container').fadeIn();
}

export const isErrorAPI = (result) => result.success !== INT_CODE_SUCCESS

export const createRequest = (action, data = {}, args = {}, method = 'POST') => {
  let handleURL = API_URL + '/' + action

  let setting = {
    type: method,
    url: `${handleURL}`,
    data: data,
    dataType: 'json',
    ...args
  }

  return $.ajax(setting);
}

export function _formatPrice(num) {
  if (!num)
    return ''
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}

export function _getArrByKey(arr, key, value) {
  return arr.filter((obj, index) => obj[ key ] == value)
}

export function _isJson(str) {
  try {
    return JSON.parse(str);
  } catch (e) {
    return {};
  }
  return {};
}

export function _parseQuery(queryString) {
  var query = {};
  var pairs = (queryString[ 0 ] === '?' ? queryString.substr(1) : queryString).split('&');
  for (var i = 0; i < pairs.length; i++) {
    var pair = pairs[ i ].split('=');
    query[ decodeURIComponent(pair[ 0 ]) ] = decodeURIComponent(pair[ 1 ] || '');
  }
  return query;
}

export function _limitWords(str, no_words) {
  if (typeof str === 'undefined')
    return '';

  return str.split(" ").splice(0, no_words).join(" ");
}

export function _checkACP(action) {
  try {
    let { isACP, rolePermissionList } = CDATA.permission;
    if (isACP)
      return true;

    if (!Array.isArray(rolePermissionList))
      return false;

    let resource = CDATA.permission.groupResource + ':' + action;

    return rolePermissionList.includes(resource);
  } catch (e) {
    return false;
  }
}

