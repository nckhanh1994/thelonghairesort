import * as ACTION from '$jsCommonPath/enum/action'
import { createRequest } from '$jsCommonPath/util/GeneralUtil'

export const uploadReq = (data, args) => createRequest(ACTION.UPLOAD, data, args)