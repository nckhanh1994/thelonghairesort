import * as ACTION from '$jsCommonPath/enum/action'
import { createRequest } from '$jsCommonPath/util/GeneralUtil'

export const uploadReq = (data, args) => createRequest(ACTION.UPLOAD, data, args)

export const getUserReq = (data, args) => createRequest(ACTION.GET_USER, data, args)
export const readUserReq = (data, args) => createRequest(ACTION.READ_USER, data, args)
export const createUserReq = (data, args) => createRequest(ACTION.CREATE_USER, data, args)
export const updateUserReq = (data, args) => createRequest(ACTION.UPDATE_USER, data, args)
export const deleteUserReq = (data, args) => createRequest(ACTION.DELETE_USER, data, args)

export const getRoleReq = (data, args) => createRequest(ACTION.GET_ROLE, data, args)
export const readRoleReq = (data, args) => createRequest(ACTION.READ_ROLE, data, args)
export const createRoleReq = (data, args) => createRequest(ACTION.CREATE_ROLE, data, args)
export const updateRoleReq = (data, args) => createRequest(ACTION.UPDATE_ROLE, data, args)
export const deleteRoleReq = (data, args) => createRequest(ACTION.DELETE_ROLE, data, args)
export const getRolePermissionReq = (data, args) => createRequest(ACTION.GET_ROLE_PERMISSION, data, args)
export const createRolePermissionReq = (data, args) => createRequest(ACTION.CREATE_ROLE_PERMISSION, data, args)
export const deleteRolePermissionReq = (data, args) => createRequest(ACTION.DELETE_ROLE_PERMISSION, data, args)

export const getItemReq = (data, args) => createRequest(ACTION.GET_ITEM, data, args)
export const readItemReq = (data, args) => createRequest(ACTION.READ_ITEM, data, args)
export const createItemReq = (data, args) => createRequest(ACTION.CREATE_ITEM, data, args)
export const updateItemReq = (data, args) => createRequest(ACTION.UPDATE_ITEM, data, args)
export const deleteItemReq = (data, args) => createRequest(ACTION.DELETE_ITEM, data, args)

export const getCityReq = (data, args) => createRequest(ACTION.GET_CITY, data, args)
export const readCityReq = (data, args) => createRequest(ACTION.READ_CITY, data, args)
export const createCityReq = (data, args) => createRequest(ACTION.CREATE_CITY, data, args)
export const updateCityReq = (data, args) => createRequest(ACTION.UPDATE_CITY, data, args)
export const deleteCityReq = (data, args) => createRequest(ACTION.DELETE_CITY, data, args)

export const getDistrictReq = (data, args) => createRequest(ACTION.GET_DISTRICT, data, args)
export const readDistrictReq = (data, args) => createRequest(ACTION.READ_DISTRICT, data, args)
export const createDistrictReq = (data, args) => createRequest(ACTION.CREATE_DISTRICT, data, args)
export const updateDistrictReq = (data, args) => createRequest(ACTION.UPDATE_DISTRICT, data, args)
export const deleteDistrictReq = (data, args) => createRequest(ACTION.DELETE_DISTRICT, data, args)

export const getWardReq = (data, args) => createRequest(ACTION.GET_WARD, data, args)
export const readWardReq = (data, args) => createRequest(ACTION.READ_WARD, data, args)
export const createWardReq = (data, args) => createRequest(ACTION.CREATE_WARD, data, args)
export const updateWardReq = (data, args) => createRequest(ACTION.UPDATE_WARD, data, args)
export const deleteWardReq = (data, args) => createRequest(ACTION.DELETE_WARD, data, args)

export const readStreetReq = (data, args) => createRequest(ACTION.READ_STREET, data, args)