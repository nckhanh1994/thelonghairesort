import { forIn as _forIn } from 'lodash';

export default (() => {
  const _BREAKPOINT = {
    xs: 592,
    sm: 752,
    md: 992,
    lg: 1280,
    xl: 1352,
    xxl: 1432,
  };

  let _$breakPoint = '';

  const _getBreakPointKey = () => {
    // let checkWidth = window.innerWidth||document.documentElement.clientWidth||document.body.clientWidth;
    let checkWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

    _forIn(_BREAKPOINT, (value, key) => {
      if (checkWidth < _BREAKPOINT[ key ]) {
        _$breakPoint = key;
        return false;
      } else {
        _$breakPoint = 'xxxl';
      }
    })

    return _$breakPoint;
  };

  const _getCurrentBreakPointKey = () => {
    return _$breakPoint;
  };

  return {
    init() {
    },
    getCurrentBreakPointKey: _getBreakPointKey,
  };
})();
