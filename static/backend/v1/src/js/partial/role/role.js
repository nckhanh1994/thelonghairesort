
import Vue from 'vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'

import App from '$vuePartialPath/global/App'
import RoleIndexSection from '$vuePartialPath/role/RoleIndexSection'
import RoleAddSection from '$vuePartialPath/role/RoleAddSection'
import RoleEditSection from '$vuePartialPath/role/RoleEditSection'
import RoleGrantSection from '$vuePartialPath/role/RoleGrantSection'

import RoleTable from '$vuePartialPath/role/RoleTable'

import Store from '$jsStorePath/role'

Vue.use(Vuex)
Vue.use(VueRouter)

const store = new Vuex.Store(Store)
const router = new VueRouter({
  mode: 'history',
  routes: [
    { name: 'role:index', path: '/backend/role/index/:page', component: RoleIndexSection },
    { name: 'role:add', path: '/backend/role/add/:page', component: RoleAddSection },
    { name: 'role:edit', path: '/backend/role/edit/:id/:page', component: RoleEditSection },
    { name: 'role:grant', path: '/backend/role/grant/:id/:page', component: RoleGrantSection },
  ]
});

const Object = (() => {
  const _renderRoleTable = () => {
    let $el = $('#role-section')

    new Vue({
      router,
      store,
      render: h => h(App)
    }).$mount($el[ 0 ])
  }

  return {
    init() {
      _renderRoleTable()
    }
  }
})()

document.addEventListener("DOMContentLoaded", function () {
  Object.init()
});
