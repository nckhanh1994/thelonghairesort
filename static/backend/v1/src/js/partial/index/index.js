import Vue from 'vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'

import App from '$vuePartialPath/global/App'
import IndexSection from '$vuePartialPath/index/IndexSection'

Vue.use(Vuex)
Vue.use(VueRouter)

// const store = new Vuex.Store(Store)
const router = new VueRouter({
  mode: 'history',
  routes: [
    { name: 'index', path: '/backend*', component: IndexSection },
  ]
});

const Object = (() => {
  const _render = () => {
    let $el = $('#content-section')

    new Vue({
      router,
      // store,
      render: h => h(App)
    }).$mount($el[ 0 ])
  }

  return {
    init() {
      _render()
    }
  }
})()

document.addEventListener("DOMContentLoaded", function () {
  Object.init()
});
