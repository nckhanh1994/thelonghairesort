import Vue from 'vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'

import App from '$vuePartialPath/global/App'
import ItemIndexSection from '$vuePartialPath/item/ItemIndexSection'
import ItemAddSection from '$vuePartialPath/item/ItemAddSection'
import ItemViewSection from '$vuePartialPath/item/ItemViewSection'
import ItemEditSection from '$vuePartialPath/item/ItemEditSection'

import ItemTable from '$vuePartialPath/item/ItemTable'

import Store from '$jsStorePath/item'

Vue.use(Vuex)
Vue.use(VueRouter)

const store = new Vuex.Store(Store)
const router = new VueRouter({
  mode: 'history',
  routes: [
    { name: 'item:index', path: '/backend/item/index/:page', component: ItemIndexSection },
    { name: 'item:add', path: '/backend/item/add/:page', component: ItemAddSection },
    { name: 'item:view', path: '/backend/item/view/:id/:page', component: ItemViewSection },
    { name: 'item:edit', path: '/backend/item/edit/:id/:page', component: ItemEditSection },
  ]
});

const Object = (() => {
  const _renderItemTable = () => {
    let $el = $('#item-section')

    new Vue({
      router,
      store,
      render: h => h(App)
    }).$mount($el[ 0 ])
  }

  return {
    init() {
      _renderItemTable()
    }
  }
})()

document.addEventListener("DOMContentLoaded", function () {
  Object.init()
});
