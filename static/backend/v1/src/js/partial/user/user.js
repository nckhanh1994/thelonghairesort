import Vue from 'vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'

import App from '$vuePartialPath/global/App'
import UserIndexSection from '$vuePartialPath/user/UserIndexSection'
import UserAddSection from '$vuePartialPath/user/UserAddSection'
import UserEditSection from '$vuePartialPath/user/UserEditSection'

import Store from '$jsStorePath/user'

Vue.use(Vuex)
Vue.use(VueRouter)

const store = new Vuex.Store(Store)
const router = new VueRouter({
  mode: 'history',
  routes: [
    { name: 'user:index', path: '/backend/user/index/:page', component: UserIndexSection },
    { name: 'user:add', path: '/backend/user/add/:page', component: UserAddSection },
    { name: 'user:edit', path: '/backend/user/edit/:id/:page', component: UserEditSection },
  ]
});

const Object = (() => {
  const _renderUserTable = () => {
    let $el = $('#user-section')

    new Vue({
      router,
      store,
      render: h => h(App)
    }).$mount($el[ 0 ]);
  }

  return {
    init() {
      _renderUserTable();
    }
  }
})()

document.addEventListener("DOMContentLoaded", function () {
  Object.init()
});
