const path = require('path');
const argv = require('yargs').argv;

const WEB_ROOT_PATH = path.resolve(__dirname);
const WEB_SRC_PATH = path.resolve(__dirname, 'src');
const NODE_MODULES_PATH = path.resolve(__dirname, 'node_modules');

//Folder Path
let WEB_DIST_PATH = path.resolve(__dirname, '_partial/dist');
if (argv.mode == 'development') {
  WEB_DIST_PATH = path.resolve(__dirname, '_partial/tmp');
}

const WEB_PATH = {
  dist: {
    font: WEB_DIST_PATH + '/font',
    image: WEB_DIST_PATH + '/image',
    upload: WEB_DIST_PATH + '/upload',
    css: WEB_DIST_PATH + '/css',
    js: WEB_DIST_PATH + '/js',
  },
  src: {
    font: WEB_SRC_PATH + '/font',
    image: WEB_SRC_PATH + '/image',

    // Scss
    scss: WEB_SRC_PATH + '/scss',
    scssPartial: WEB_SRC_PATH + '/scss/partial',

    // Js
    js: WEB_SRC_PATH + '/js',
    jsBase: WEB_SRC_PATH + '/js/base',
    jsPartial: WEB_SRC_PATH + '/js/partial',
    jsCommon: WEB_SRC_PATH + '/js/common',
    jsStore: WEB_SRC_PATH + '/js/store',

    // Vue
    vue: WEB_SRC_PATH + '/vue',
    vueBase: WEB_SRC_PATH + '/vue/base',
    vuePartial: WEB_SRC_PATH + '/vue/partial',
  },
}

const arrVendorJs = [
  WEB_PATH.src.js + '/vendor.js',
]
const WEB_CONFIG = {
  appPath: WEB_ROOT_PATH,
  nodeModulesPath: NODE_MODULES_PATH,
  distPath: WEB_DIST_PATH,
  srcPath: WEB_SRC_PATH,
  webPath: WEB_PATH,
  webpackConfig: {
    entry: {
      'index': [
        ...arrVendorJs,
        WEB_PATH.src.jsPartial + '/index/index.js',
      ],
      'user': [
        ...arrVendorJs,
        WEB_PATH.src.jsPartial + '/user/user.js',
      ],
      'role': [
        ...arrVendorJs,
        WEB_PATH.src.jsPartial + '/role/role.js',
      ],
      'item': [
        ...arrVendorJs,
        WEB_PATH.src.jsPartial + '/item/item.js',
      ],
    },
    resolves: {
      extensions: [ '.js', '.json', '.css', '.scss', '.vue' ],
      alias: {
        $vue: 'vue/dist/vue.esm.js',
        '$srcPath': WEB_SRC_PATH,

        // Scss
        '$scssPath': WEB_PATH.src.scss,
        '$scssPartialPath': WEB_PATH.src.scssPartial,

        // Js
        '$jsPath': WEB_PATH.src.js,
        '$jsBasePath': WEB_PATH.src.jsBase,
        '$jsPartialPath': WEB_PATH.src.jsPartial,
        '$jsCommonPath': WEB_PATH.src.jsCommon,
        '$jsStorePath': WEB_PATH.src.jsStore,

        // Vue
        '$vuePath': WEB_PATH.src.vue,
        '$vueBasePath': WEB_PATH.src.vueBase,
        '$vuePartialPath': WEB_PATH.src.vuePartial,
      },
    },
    plugins: {
      providePlugins: {
        $: 'jquery',
        jQuery: 'jquery',
        jquery: 'jquery',
      },
    },
    optimization: {
      splitChunks: {
        minChunks: 2,
        cacheGroups: {
          js: {
            test: /\.(js)$/,
            name: 'vendor',
            chunks: 'all',
            minSize: 350000,
            minChunks: 4,
          },
        },
      },
    },
  },
};

module.exports = WEB_CONFIG;