//Load config
const WEB_CONFIG = require('./web.config');

const path = require('path');

// Plugin
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const WriteFilePlugin = require('write-file-webpack-plugin');
const sass = require('node-sass');
const VueLoaderPlugin = require('vue-loader/lib/plugin');

const WEBPACK_CONFIG = {
  entry: WEB_CONFIG.webpackConfig.entry,
  output: {
    filename: 'js/[name].js',
    path: WEB_CONFIG.distPath,
  },
  resolve: WEB_CONFIG.webpackConfig.resolves,
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [ '@babel/preset-env' ],
            plugins: [
              '@babel/plugin-proposal-object-rest-spread',
              '@babel/plugin-proposal-class-properties'
            ],
          }
        }
      },
      {
        test: /\.scss$/,
        use: [
          'style-loader',
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              url: false,
              sourceMap: true
            }
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true,
              includePaths: [ WEB_CONFIG.webPath.src.scss, WEB_CONFIG.nodeModulesPath ],
            }
          },
        ],
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: WEB_CONFIG.webPath.dist.font,
            }
          }
        ]
      },
      {
        /**
         * Loader này giúp mở các lib cho bên ngoài webpack sử dụng
         */
        test: require.resolve('jquery'),
        use: [
          {
            loader: 'expose-loader',
            options: 'jQuery',
          },
          {
            loader: 'expose-loader',
            options: '$',
          }
        ],
      }
    ], //end rules
  }, //end module
  optimization: {
    splitChunks: WEB_CONFIG.webpackConfig.optimization.splitChunks,
  },
  devtool: 'source-map',
  performance: {
    hints: false
  },
  devtool: 'inline-source-map',
  devServer: {
    contentBase: WEB_CONFIG.distPath,
    port: 1236,
    disableHostCheck: true,
    hot: false,
    stats: {
      all: false,
      errors: true,
      errorDetails: true,
      assets: true,
      colors: true,
      timings: true,
      warnings: true,
    },
  },
  plugins: [
    new WriteFilePlugin({
      test: /\.(.*)$/,
      useHashIndex: true,
      log: true,
    }),

    new MiniCssExtractPlugin({
      path: WEB_CONFIG.webPath.dist.css,
      filename: 'css/[name].css',
      chunkFilename: '[name]_chunk.css',
    }),

    new webpack.ProvidePlugin(WEB_CONFIG.webpackConfig.plugins.providePlugins), // Automatically load modules instead of having to 'import' or 'require' them everywhere.

    new VueLoaderPlugin(),
  ], // end plugins
}; // end WEB_CONFIG

// Export to run
// ---------------------------
module.exports = WEBPACK_CONFIG;