$(function () {
  let EVENT

  var strKeyword = conditions.hasOwnProperty('keyword') ? conditions.keyword : ''
  var startCreatedAt = conditions.hasOwnProperty('created_at_start_format') ? conditions.created_at_start_format : ''
  var endCreatedAt = conditions.hasOwnProperty('created_at_end_format') ? conditions.created_at_end_format : ''
  var strStreetName = conditions.hasOwnProperty('street_name_like') ? conditions.street_name_like : ''
  var arrDistrictID = conditions.hasOwnProperty('district_id_list') ? conditions.district_id_list : []
  let arrCategoryID = conditions.hasOwnProperty('category_id_list') ? conditions.category_id_list : []
  let isFavorite = conditions.hasOwnProperty('is_favorite') ? conditions.is_favorite : 0;
  let isPrivate = conditions.hasOwnProperty('is_private') ? conditions.is_private : 0;

  var Object = {
    init() {
      EVENT = this.event

      this.event.init()
      this.request.init()
      this.callback.init()
      this.plugin.init()
    },
    event: {
      init() {
        this.filter.init()

      },
      filter: {
        init() {
          this.district()
          this.category()
          this.street()
          this.favorite();
          this.private();

          let options = {
            opens: 'left',
            locale: {
              format: 'DD-MM-YYYY'
            }
          }

          if (startCreatedAt.length > 0 && endCreatedAt.length > 0) {
            options.startDate = startCreatedAt
            options.endDate = endCreatedAt
          }

          $('#trigger-date-range').daterangepicker(options, function (start, end, label) {
            startCreatedAt = ''
            endCreatedAt = ''

            startCreatedAt = start.format('DD-MM-YYYY')
            endCreatedAt = end.format('DD-MM-YYYY')

            EVENT.filter.goFilter()
          });

          $('.js-filter__sumibt').on('click', (e) => {
            strKeyword = $.trim($('.js-filter-keyword').val())

            EVENT.filter.goFilter()
          })
        },
        district() {
          let nameFilter = 'district'

          let $elFilterSubmit = $('.js-filter-' + nameFilter + '__submit')
          let $elFilterRemove = $('.js-filter-' + nameFilter + '__remove')
          let $elPopupFilter = $('.js-filter-' + nameFilter + '__popup')

          $(document).on('click', (e) => {
            $elPopupFilter.hide()
          })

          $elPopupFilter.on('click', (e) => {
            e.preventDefault()
            e.stopPropagation()
          })

          let $elFilterPopups = $('.js-filter-popup')
          let $elTrigger = $('.js-filter-' + nameFilter + '')
          let $elPopup = $('.js-filter-' + nameFilter + '__popup')
          $elTrigger.on('click', (e) => {
            e.preventDefault()
            e.stopPropagation()

            $elFilterPopups.hide()
            let $self = $(e.target)

            $elPopup.toggle()

            let offset = $elTrigger.offset()
            let top = offset.top + $elTrigger.height() + 30
            let left = offset.left

            $elPopup.css({
              position: 'absolute',
              top: top,
              left: left
            })
          })

          let $elFilterLabel = $('.js-filter-' + nameFilter + '__popup-label')
          let selectorFilterInput = '.js-filter-' + nameFilter + '__popup-input';

          $elFilterLabel.on('click', function (e) {
            e.preventDefault()
            e.stopPropagation()

            let $self = $(this)

            $self.find(selectorFilterInput).toggleClass('is-checked')

            // ??
            arrDistrictID = []
            $elFilterInputs = $(selectorFilterInput + '.is-checked').find('input[type="checkbox"]')
            $elFilterInputs.each((index, el) => {
              let $el = $(el)

              arrDistrictID.push($el.val())
            })

            arrDistrictID = arrDistrictID.filter(function () {
              return true
            })

            if (arrDistrictID.length > 0) {
              $elTrigger.addClass('filtered')
            } else {
              $elTrigger.removeClass('filtered')
            }
          })

          $elFilterSubmit.on('click', (e) => {
            EVENT.filter.goFilter()
          })

          $elFilterRemove.on('click', (e) => {
            arrDistrictID = []

            EVENT.filter.goFilter()
          })
        },
        category() {
          let nameFilter = 'category'

          let $elFilterSubmit = $('.js-filter-' + nameFilter + '__submit')
          let $elFilterRemove = $('.js-filter-' + nameFilter + '__remove')
          let $elPopupFilter = $('.js-filter-' + nameFilter + '__popup')

          $(document).on('click', (e) => {
            $elPopupFilter.hide()
          })

          $elPopupFilter.on('click', (e) => {
            e.preventDefault()
            e.stopPropagation()
          })

          let $elFilterPopups = $('.js-filter-popup')
          let $elTrigger = $('.js-filter-' + nameFilter + '')
          let $elPopup = $('.js-filter-' + nameFilter + '__popup')
          $elTrigger.on('click', (e) => {
            e.preventDefault()
            e.stopPropagation()

            let $self = $(e.target)

            $elFilterPopups.hide()
            $elPopup.toggle()

            let offset = $elTrigger.offset()
            let top = offset.top + $elTrigger.height() + 30
            let left = offset.left

            $elPopup.css({
              position: 'absolute',
              top: top,
              left: left
            })
          })

          let $elFilterLabel = $('.js-filter-' + nameFilter + '__popup-label')
          let selectorFilterInput = '.js-filter-' + nameFilter + '__popup-input';

          $elFilterLabel.on('click', function (e) {
            e.preventDefault()
            e.stopPropagation()

            let $self = $(this)

            $self.find(selectorFilterInput).toggleClass('is-checked')

            // ??
            arrCategoryID = []
            $elFilterInputs = $(selectorFilterInput + '.is-checked').find('input[type="checkbox"]')
            $elFilterInputs.each((index, el) => {
              let $el = $(el)

              arrCategoryID.push($el.val())
            })

            arrCategoryID = arrCategoryID.filter(function () {
              return true
            })

            if (arrCategoryID.length > 0) {
              $elTrigger.addClass('filtered')
            } else {
              $elTrigger.removeClass('filtered')
            }
          })

          $elFilterSubmit.on('click', (e) => {
            EVENT.filter.goFilter()
          })

          $elFilterRemove.on('click', (e) => {
            arrCategoryID = []

            EVENT.filter.goFilter()
          })
        },
        street() {
          let nameFilter = 'street'

          let $elFilterSubmit = $('.js-filter-' + nameFilter + '__submit')
          let $elFilterRemove = $('.js-filter-' + nameFilter + '__remove')
          let $elPopupFilter = $('.js-filter-' + nameFilter + '__popup')

          $(document).on('click', (e) => {
            $elPopupFilter.hide()
          })

          $elPopupFilter.on('click', (e) => {
            e.preventDefault()
            e.stopPropagation()
          })

          let $elFilterPopups = $('.js-filter-popup')
          let $elTrigger = $('.js-filter-' + nameFilter + '')
          let $elPopup = $('.js-filter-' + nameFilter + '__popup')
          $elTrigger.on('click', (e) => {
            e.preventDefault()
            e.stopPropagation()

            let $self = $(e.target)

            $elFilterPopups.hide()
            $elPopup.toggle()

            let offset = $elTrigger.offset()
            let top = offset.top + $elTrigger.height() + 30
            let left = offset.left

            $elPopup.css({
              position: 'absolute',
              top: top,
              left: left
            })
          })

          $elFilterSubmit.on('click', (e) => {
            strStreetName = $.trim($('.js-filter-street__input').val())

            EVENT.filter.goFilter()
          })

          $elFilterRemove.on('click', (e) => {
            strStreetName = ''

            EVENT.filter.goFilter()
          })

          $('.js-filter-street__input').typeahead({
            items: 8,
            source: streetList,
          });
        },
        favorite() {
          let nameFilter = 'favorite'

          let $elTrigger = $('.js-filter-' + nameFilter + '')
          $elTrigger.on('click', (e) => {
            e.preventDefault()
            e.stopPropagation()

            let $self = $(e.target)

            isFavorite = 1;
            if ($self.hasClass('filtered')) {
              isFavorite = 0;
            }

            EVENT.filter.goFilter()
          })
        },
        private() {
          let nameFilter = 'private'

          let $elTrigger = $('.js-filter-' + nameFilter + '')
          $elTrigger.on('click', (e) => {
            e.preventDefault()
            e.stopPropagation()

            let $self = $(e.target)

            isPrivate = 1;
            if ($self.hasClass('filtered')) {
              isPrivate = 0;
            }


            EVENT.filter.goFilter()
          })
        },
        goFilter() {
          let strFilter = ''

          if (strKeyword.length > 0) {
            strFilter += '&s=' + strKeyword
          }

          if (arrDistrictID.length > 0) {
            let strDistrictID = arrDistrictID.join(',')

            strFilter += '&districtID=' + strDistrictID
          }

          if (arrCategoryID.length > 0) {
            let strCategoryID = arrCategoryID.join(',')

            strFilter += '&categoryID=' + strCategoryID
          }

          if (startCreatedAt.length > 0) {
            strFilter += '&startCreatedAt=' + startCreatedAt
          }

          if (endCreatedAt.length > 0) {
            let strCategoryID = arrCategoryID.join(',')

            strFilter += '&endCreatedAt=' + endCreatedAt
          }

          if (strStreetName.length > 0) {
            strFilter += '&s_like=' + strStreetName
          }

          if (isFavorite > 0) {
            strFilter += '&is_favorite=' + isFavorite
          }

          if (isPrivate > 0) {
            strFilter += '&is_private=' + isPrivate
          }

          window.location.href = CDATA.pageURL + strFilter
        }
      }
    },
    request: {
      init() {
      }
    },
    callback: {
      init() {

      }
    },
    plugin: {
      init() {

      }
    }
  };

  Object.init()
});
