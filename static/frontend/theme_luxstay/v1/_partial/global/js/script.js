$(function () {
  var Object = {
    init() {
      this.event.init()
      this.request.init()
      this.callback.init()
      this.plugin.init()
    },
    event: {
      init() {
      }
    },
    request: {
      init() {
      }
    },
    callback: {
      init() {
      }
    },
    plugin: {
      init() {
        this.slick()
      },
      slick() {
        $('#section-uu-dai__slider').slick({
          infinite: true,
          slidesToShow: 4,
          slidesToScroll: 4
        });

        $('#section-gio-vang__slider').slick({
          infinite: true,
          slidesToShow: 4,
          slidesToScroll: 4
        });

        $('#section-dia-diem__slider').slick({
          infinite: true,
          slidesToShow: 4,
          slidesToScroll: 4
        });

        $('#section-khong-gian__slider').slick({
          infinite: true,
          slidesToShow: 2,
          slidesToScroll: 2
        });

        $('#section-deal__slider').slick({
          infinite: true,
          slidesToShow: 6,
          slidesToScroll: 6
        });

        $('#section-noi-dung__slider').slick({
          infinite: true,
          slidesToShow: 6,
          slidesToScroll: 6
        });
      }
    }
  };

  Object.init()
});
