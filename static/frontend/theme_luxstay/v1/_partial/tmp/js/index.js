/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(Object.prototype.hasOwnProperty.call(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"index": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push([0,"vendor"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/global/TheModalFullScreen.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--1!./node_modules/vue-loader/lib??vue-loader-options!./src/vue/partial/global/TheModalFullScreen.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var $jsBasePath_define__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! $jsBasePath/define */ "./src/js/base/define.js");
/* harmony import */ var $jsBasePath_EventBus__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! $jsBasePath/EventBus */ "./src/js/base/EventBus.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'modal',
  components: {},
  data: function data() {
    return {
      title: 'THÔNG BÁO !',
      content: 'We are lucky to live in a glorious age that gives us everything as a human race.'
    };
  },
  computed: {},
  created: function created() {
    var self = this;
    $jsBasePath_EventBus__WEBPACK_IMPORTED_MODULE_1__["default"].on($jsBasePath_define__WEBPACK_IMPORTED_MODULE_0__["EVENT_BUS_KEY"].UPDATE_CONTENT_MODAL, function (content) {
      self.content = content;
    });
    $(document).on('keyup', function (e) {
      var keyCode = e.which;
      if (keyCode != 13 && keyCode != 27) return;
      var flag = $('#modal-fs').hasClass('show');
      if (!flag) return;
      $('#modal-fs-btn').trigger('click');
      $jsBasePath_EventBus__WEBPACK_IMPORTED_MODULE_1__["default"].emit($jsBasePath_define__WEBPACK_IMPORTED_MODULE_0__["EVENT_BUS_KEY"].CONFIRM_MODAL, true);
    });
  },
  methods: {
    confirm: function confirm() {
      $('#modal-fs-btn').trigger('click');
      $jsBasePath_EventBus__WEBPACK_IMPORTED_MODULE_1__["default"].emit($jsBasePath_define__WEBPACK_IMPORTED_MODULE_0__["EVENT_BUS_KEY"].CONFIRM_MODAL, true);
    }
  }
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js-exposed")))

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/user/UserTable.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--1!./node_modules/vue-loader/lib??vue-loader-options!./src/vue/partial/user/UserTable.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var $vuePartialPath_user_UserTableRow__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! $vuePartialPath/user/UserTableRow */ "./src/vue/partial/user/UserTableRow.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'user-table',
  components: {
    'user-table-row': $vuePartialPath_user_UserTableRow__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {};
  },
  computed: {
    arrEntityList: function arrEntityList() {
      return this.$store.state.entity.arrEntityList;
    }
  },
  created: function created() {},
  methods: {}
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/user/UserTableRow.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--1!./node_modules/vue-loader/lib??vue-loader-options!./src/vue/partial/user/UserTableRow.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var $jsCommonPath_util_SafeUtil__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! $jsCommonPath/util/SafeUtil */ "./src/js/common/util/SafeUtil.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'user-table-row',
  props: {
    index: {
      default: {},
      type: Number
    },
    objEntity: {
      default: {},
      type: Object
    }
  },
  components: {},
  data: function data() {
    return {
      url: {
        resourceURL: CDATA.url.resourceURL
      },
      flag: {
        isDeleting: false
      }
    };
  },
  computed: {
    profileImage: function profileImage() {
      if (Object($jsCommonPath_util_SafeUtil__WEBPACK_IMPORTED_MODULE_0__["_isEmptyString"])(this.objEntity.profile_image)) return this.url.resourceURL + '/global/image/notFound.png';
      return this.objEntity.profile_image;
    }
  },
  created: function created() {},
  methods: {
    goUpdate: function goUpdate() {
      this.$store.dispatch('setEntityAct', {
        index: this.index
      });
      this.$router.push({
        name: 'user:edit',
        params: {
          id: this.objEntity.user_id,
          page: 1
        }
      });
    },
    remove: function remove(id) {
      var _this = this;

      if (!id) return;
      this.flag.isDeleting = true;
      this.$store.dispatch('deleteEntityAct', {
        id: id
      }).then(function (res) {
        _this.flag.isDeleting = false;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/global/TheModalFullScreen.vue?vue&type=template&id=1e69cc63&":
/*!********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./src/vue/partial/global/TheModalFullScreen.vue?vue&type=template&id=1e69cc63& ***!
  \********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "modal fade modal-fs", attrs: { id: "modal-fs" } },
    [
      _c("div", { staticClass: "modal-dialog", attrs: { role: "document" } }, [
        _c("div", { staticClass: "modal-content" }, [
          _c("div", { staticClass: "modal-body" }, [
            _c("div", { staticClass: "container text-center" }, [
              _c("div", { staticClass: "row full-height align-items-center" }, [
                _c("div", { staticClass: "col-md-5 m-h-auto" }, [
                  _c("div", { staticClass: "p-h-30 p-b-50" }, [
                    _vm._m(0),
                    _vm._v(" "),
                    _c("h1", { staticClass: "text-thin m-v-15" }, [
                      _vm._v(_vm._s(_vm.title))
                    ]),
                    _vm._v(" "),
                    _c("p", { staticClass: "m-b-25 lead" }, [
                      _vm._v(_vm._s(_vm.content))
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "text-center" }, [
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-default btn-lg",
                          on: {
                            click: function($event) {
                              $event.preventDefault()
                              return _vm.confirm($event)
                            }
                          }
                        },
                        [_vm._v("Confirm\n                  ")]
                      )
                    ])
                  ])
                ])
              ])
            ]),
            _vm._v(" "),
            _c(
              "a",
              {
                staticClass: "modal-close",
                attrs: { href: "#", "data-dismiss": "modal" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                    return _vm.confirm($event)
                  }
                }
              },
              [_c("i", { staticClass: "ti-close" })]
            )
          ])
        ])
      ])
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "text-center font-size-70" }, [
      _c("i", {
        staticClass:
          "mdi mdi-checkbox-marked-circle-outline icon-gradient-success"
      })
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/user/UserTable.vue?vue&type=template&id=75b32def&":
/*!*********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./src/vue/partial/user/UserTable.vue?vue&type=template&id=75b32def& ***!
  \*********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "table-overflow", attrs: { id: "user-table" } },
    [
      _c(
        "table",
        { staticClass: "table table-hover table-xl", attrs: { id: "dt-opt" } },
        [
          _vm._m(0),
          _vm._v(" "),
          _c(
            "tbody",
            [
              _vm._l(_vm.arrEntityList, function(objEntity, index) {
                return [
                  _c("user-table-row", {
                    attrs: { index: index, "obj-entity": objEntity }
                  })
                ]
              })
            ],
            2
          )
        ]
      )
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [
          _c("div", { staticClass: "checkbox p-0" }, [
            _c("input", {
              staticClass: "checkAll",
              attrs: { id: "selectable1", type: "checkbox", name: "checkAll" }
            }),
            _vm._v(" "),
            _c("label", { attrs: { for: "selectable1" } })
          ])
        ]),
        _vm._v(" "),
        _c("th", [_vm._v("FULLNAME")]),
        _vm._v(" "),
        _c("th", [_vm._v("EMAIL")]),
        _vm._v(" "),
        _c("th", [_vm._v("STATUS")]),
        _vm._v(" "),
        _c("th", [_vm._v("#")]),
        _vm._v(" "),
        _c("th")
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/user/UserTableRow.vue?vue&type=template&id=42c1b89b&":
/*!************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./src/vue/partial/user/UserTableRow.vue?vue&type=template&id=42c1b89b& ***!
  \************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("tr", [
    _vm._m(0),
    _vm._v(" "),
    _c("td", [
      _c("div", { staticClass: "list-media" }, [
        _c("div", { staticClass: "list-item" }, [
          _c("div", { staticClass: "media-img" }, [
            _c("img", { attrs: { src: _vm.profileImage, alt: "" } })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "info" }, [
            _c("span", { staticClass: "title" }, [
              _vm._v(_vm._s(_vm.objEntity.fullname))
            ]),
            _vm._v(" "),
            _c("span", { staticClass: "sub-title" }, [
              _vm._v("ID " + _vm._s(_vm.objEntity.user_id))
            ])
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c("td", [_vm._v(_vm._s(_vm.objEntity.email))]),
    _vm._v(" "),
    _c(
      "td",
      [
        _vm.objEntity.is_actived == 1
          ? [
              _c(
                "span",
                { staticClass: "badge badge-pill badge-gradient-success" },
                [_vm._v("Đã kích hoạt")]
              )
            ]
          : [
              _c(
                "span",
                { staticClass: "badge badge-pill badge-gradient-danger" },
                [_vm._v("Chưa kích hoạt")]
              )
            ]
      ],
      2
    ),
    _vm._v(" "),
    _c("td", { staticClass: "text-center font-size-18" }, [
      _c(
        "a",
        {
          staticClass: "text-gray m-r-15",
          on: {
            click: function($event) {
              return _vm.goUpdate()
            }
          }
        },
        [_c("i", { staticClass: "ti-pencil" })]
      ),
      _vm._v(" "),
      _c(
        "a",
        {
          staticClass: "text-gray",
          on: {
            click: function($event) {
              return _vm.remove(_vm.objEntity.user_id)
            }
          }
        },
        [
          _c("i", {
            class: {
              "fa fa-spinner fa-pulse fa-fw": _vm.flag.isDeleting,
              "ti-trash": !_vm.flag.isDeleting
            }
          })
        ]
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("td", [
      _c("div", { staticClass: "checkbox" }, [
        _c("input", { attrs: { id: "selectable2", type: "checkbox" } }),
        _vm._v(" "),
        _c("label", { attrs: { for: "selectable2" } })
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/webpack/hot sync ^\\.\\/log$":
/*!*************************************************!*\
  !*** (webpack)/hot sync nonrecursive ^\.\/log$ ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./log": "./node_modules/webpack/hot/log.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/webpack/hot sync ^\\.\\/log$";

/***/ }),

/***/ "./src/js/partial/index/index.js":
/*!***************************************!*\
  !*** ./src/js/partial/index/index.js ***!
  \***************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js-exposed");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.runtime.esm.js");
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var $vuePartialPath_user_UserTable__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! $vuePartialPath/user/UserTable */ "./src/vue/partial/user/UserTable.vue");
/* harmony import */ var $jsStorePath_user__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! $jsStorePath/user */ "./src/js/store/user.js");





vue__WEBPACK_IMPORTED_MODULE_1__["default"].use(vuex__WEBPACK_IMPORTED_MODULE_2__["default"]);
var store = new vuex__WEBPACK_IMPORTED_MODULE_2__["default"].Store($jsStorePath_user__WEBPACK_IMPORTED_MODULE_4__["default"]);

var Object = function () {
  var _renderUserTable = function _renderUserTable() {
    var $el = jquery__WEBPACK_IMPORTED_MODULE_0___default()('#user-table');
    new vue__WEBPACK_IMPORTED_MODULE_1__["default"]({
      store: store,
      render: function render(h) {
        return h($vuePartialPath_user_UserTable__WEBPACK_IMPORTED_MODULE_3__["default"]);
      }
    }).$mount($el[0]);
  };

  return {
    init: function init() {
      _renderUserTable();
    }
  };
}();

document.addEventListener("DOMContentLoaded", function () {
  Object.init();
});

/***/ }),

/***/ "./src/js/store/user.js":
/*!******************************!*\
  !*** ./src/js/store/user.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var $jsCommonPath_api__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! $jsCommonPath/api */ "./src/js/common/api/index.js");
/* harmony import */ var $jsCommonPath_util_GeneralUtil__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! $jsCommonPath/util/GeneralUtil */ "./src/js/common/util/GeneralUtil.js");
/* harmony import */ var $jsCommonPath_util_SafeUtil__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! $jsCommonPath/util/SafeUtil */ "./src/js/common/util/SafeUtil.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }





var _objEntity = {
  user_id: null,
  profile_image: null,
  fullname: null,
  email: null,
  phone: null,
  is_actived: 0
};

var Store = function Store() {
  _classCallCheck(this, Store);

  return {
    state: {
      flag: {
        isRead: false
      },
      arrEntityList: [],
      objEntitySelect: _objectSpread({}, _objEntity)
    },
    mutations: {
      resetEntity: function resetEntity(state) {
        state.entity.objEntitySelect = _objectSpread({}, _objEntity);
      },
      updateFlag: function updateFlag(state, payload) {
        state.flag[payload.flag] = payload.value;
      },
      setEntities: function setEntities(state, payload) {
        state.entity.arrEntityList = payload.arrEntityList;
      },
      setEntity: function setEntity(state, payload) {
        if (Object(lodash__WEBPACK_IMPORTED_MODULE_0__["isEmpty"])(payload.objEntitySelect)) return;

        var _payload = _objectSpread({}, payload),
            objEntitySelect = _payload.objEntitySelect;

        objEntitySelect.password = null;
        state.entity.objEntitySelect = objEntitySelect;
      },
      updateEntity: function updateEntity(state, payload) {
        var _payload2 = _objectSpread({}, payload),
            type = _payload2.type,
            index = _payload2.index,
            arrEntity = _payload2.arrEntity;

        switch (type) {
          case 'create':
            state.entity.arrEntityList.unshift(arrEntity);
            break;

          case 'update':
            state.entity.arrEntityList[index] = arrEntity;
            state.entity.objEntitySelect = arrEntity;
            break;

          case 'delete':
            state.entity.arrEntityList.splice(index, 1);
            break;
        }
      }
    },
    actions: {
      getEntityAct: function getEntityAct(_ref, payload) {
        var commit = _ref.commit;
        return new Promise(function (resolve, reject) {
          var reqParams = _objectSpread({}, payload);

          $jsCommonPath_api__WEBPACK_IMPORTED_MODULE_1__["getUserReq"](reqParams).done(function (res) {
            if (Object($jsCommonPath_util_GeneralUtil__WEBPACK_IMPORTED_MODULE_2__["isErrorAPI"])(res)) {
              reject(res.payload);
              return;
            }

            var _res$payload = _objectSpread({}, res.payload),
                arrUserDetail = _res$payload.arrUserDetail;

            commit('setEntity', {
              objEntitySelect: arrUserDetail
            });
            resolve(res.payload);
          });
        });
      },
      readEntityAct: function readEntityAct(_ref2) {
        var commit = _ref2.commit;
        return new Promise(function (resolve, reject) {
          var reqParams = {};
          $jsCommonPath_api__WEBPACK_IMPORTED_MODULE_1__["readUserReq"](reqParams).done(function (res) {
            if (Object($jsCommonPath_util_GeneralUtil__WEBPACK_IMPORTED_MODULE_2__["isErrorAPI"])(res)) {
              reject(res.payload);
              return;
            }

            var _res$payload2 = _objectSpread({}, res.payload),
                arrUserList = _res$payload2.arrUserList;

            commit('setEntities', {
              arrEntityList: arrUserList
            });
            resolve(res.payload);
          });
        });
      },
      uploadFileAct: function uploadFileAct(_ref3, payload) {
        var commit = _ref3.commit;
        return new Promise(function (resolve, reject) {
          var formData = new FormData();
          formData.append('controller', payload.controller);
          formData.append('file', payload.file);
          $jsCommonPath_api__WEBPACK_IMPORTED_MODULE_1__["uploadReq"](formData, {
            processData: false,
            contentType: false
          }).done(function (res) {
            if (Object($jsCommonPath_util_GeneralUtil__WEBPACK_IMPORTED_MODULE_2__["isErrorAPI"])(res)) {
              Object($jsCommonPath_util_GeneralUtil__WEBPACK_IMPORTED_MODULE_2__["triggerModal"])(res.message);
              reject(res.payload);
              return;
            }

            resolve(res.payload);
          });
        });
      },
      setEntityAct: function setEntityAct(_ref4, payload) {
        var commit = _ref4.commit,
            getters = _ref4.getters;
        if (!payload.hasOwnProperty('index')) return;

        var _payload3 = _objectSpread({}, payload),
            index = _payload3.index;

        commit('setEntity', {
          objEntitySelect: getters.getEntityByIndex(index)[0]
        });
      },
      createEntityAct: function createEntityAct(_ref5, payload) {
        var commit = _ref5.commit,
            getters = _ref5.getters;
        return new Promise(function (resolve, reject) {
          var reqParams = _objectSpread({}, payload);

          $jsCommonPath_api__WEBPACK_IMPORTED_MODULE_1__["createUserReq"](reqParams).done(function (res) {
            Object($jsCommonPath_util_GeneralUtil__WEBPACK_IMPORTED_MODULE_2__["triggerModal"])(res.message);

            if (Object($jsCommonPath_util_GeneralUtil__WEBPACK_IMPORTED_MODULE_2__["isErrorAPI"])(res)) {
              reject(res.payload);
              return;
            }

            var _res$payload3 = _objectSpread({}, res.payload),
                arrUserDetail = _res$payload3.arrUserDetail;

            commit('updateEntity', {
              type: 'create',
              arrEntity: arrUserDetail
            });
            resolve(res.payload);
          });
        });
      },
      updateEntityAct: function updateEntityAct(_ref6, payload) {
        var commit = _ref6.commit,
            getters = _ref6.getters;
        return new Promise(function (resolve, reject) {
          var reqParams = _objectSpread({}, payload);

          $jsCommonPath_api__WEBPACK_IMPORTED_MODULE_1__["updateUserReq"](reqParams).done(function (res) {
            Object($jsCommonPath_util_GeneralUtil__WEBPACK_IMPORTED_MODULE_2__["triggerModal"])(res.message);

            if (Object($jsCommonPath_util_GeneralUtil__WEBPACK_IMPORTED_MODULE_2__["isErrorAPI"])(res)) {
              reject(res.payload);
              return;
            }

            var _res$payload4 = _objectSpread({}, res.payload),
                arrUserDetail = _res$payload4.arrUserDetail;

            commit('updateEntity', {
              type: 'update',
              index: getters.getIndexByID(arrUserDetail.user_id),
              arrEntity: arrUserDetail
            });
            resolve(res.payload);
          });
        });
      },
      deleteEntityAct: function deleteEntityAct(_ref7, payload) {
        var commit = _ref7.commit,
            getters = _ref7.getters;
        return new Promise(function (resolve, reject) {
          var reqParams = _objectSpread({}, payload);

          $jsCommonPath_api__WEBPACK_IMPORTED_MODULE_1__["deleteUserReq"](reqParams).done(function (res) {
            Object($jsCommonPath_util_GeneralUtil__WEBPACK_IMPORTED_MODULE_2__["triggerModal"])(res.message);

            if (Object($jsCommonPath_util_GeneralUtil__WEBPACK_IMPORTED_MODULE_2__["isErrorAPI"])(res)) {
              reject(res.payload);
              return;
            }

            var _res$payload5 = _objectSpread({}, res.payload),
                id = _res$payload5.id;

            console.log(id);
            commit('updateEntity', {
              type: 'delete',
              index: getters.getIndexByID(id)
            });
            resolve(res.payload);
          });
        });
      }
    },
    getters: {
      getEntityByIndex: function getEntityByIndex(state) {
        return function (index) {
          return state.entity.arrEntityList.filter(function (objEntity, currentIndex) {
            return currentIndex == index;
          });
        };
      },
      getIndexByID: function getIndexByID(state) {
        return function (id) {
          var index = -1;

          Object(lodash__WEBPACK_IMPORTED_MODULE_0__["forIn"])(state.entity.arrEntityList, function (objEntity, currentIndex) {
            if (objEntity.user_id != id) return;
            index = currentIndex;
            return false;
          });

          return index;
        };
      }
    }
  };
};

/* harmony default export */ __webpack_exports__["default"] = (new Store());

/***/ }),

/***/ "./src/vue/partial/global/TheModalFullScreen.vue":
/*!*******************************************************!*\
  !*** ./src/vue/partial/global/TheModalFullScreen.vue ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _TheModalFullScreen_vue_vue_type_template_id_1e69cc63___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./TheModalFullScreen.vue?vue&type=template&id=1e69cc63& */ "./src/vue/partial/global/TheModalFullScreen.vue?vue&type=template&id=1e69cc63&");
/* harmony import */ var _TheModalFullScreen_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./TheModalFullScreen.vue?vue&type=script&lang=js& */ "./src/vue/partial/global/TheModalFullScreen.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _TheModalFullScreen_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _TheModalFullScreen_vue_vue_type_template_id_1e69cc63___WEBPACK_IMPORTED_MODULE_0__["render"],
  _TheModalFullScreen_vue_vue_type_template_id_1e69cc63___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null

)

/* hot reload */
if (false) { var api; }
component.options.__file = "src/vue/partial/global/TheModalFullScreen.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./src/vue/partial/global/TheModalFullScreen.vue?vue&type=script&lang=js&":
/*!********************************************************************************!*\
  !*** ./src/vue/partial/global/TheModalFullScreen.vue?vue&type=script&lang=js& ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_1_node_modules_vue_loader_lib_index_js_vue_loader_options_TheModalFullScreen_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--1!../../../../node_modules/vue-loader/lib??vue-loader-options!./TheModalFullScreen.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/global/TheModalFullScreen.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_1_node_modules_vue_loader_lib_index_js_vue_loader_options_TheModalFullScreen_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]);

/***/ }),

/***/ "./src/vue/partial/global/TheModalFullScreen.vue?vue&type=template&id=1e69cc63&":
/*!**************************************************************************************!*\
  !*** ./src/vue/partial/global/TheModalFullScreen.vue?vue&type=template&id=1e69cc63& ***!
  \**************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TheModalFullScreen_vue_vue_type_template_id_1e69cc63___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./TheModalFullScreen.vue?vue&type=template&id=1e69cc63& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/global/TheModalFullScreen.vue?vue&type=template&id=1e69cc63&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TheModalFullScreen_vue_vue_type_template_id_1e69cc63___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TheModalFullScreen_vue_vue_type_template_id_1e69cc63___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./src/vue/partial/user/UserTable.vue":
/*!********************************************!*\
  !*** ./src/vue/partial/user/UserTable.vue ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _UserTable_vue_vue_type_template_id_75b32def___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./UserTable.vue?vue&type=template&id=75b32def& */ "./src/vue/partial/user/UserTable.vue?vue&type=template&id=75b32def&");
/* harmony import */ var _UserTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./UserTable.vue?vue&type=script&lang=js& */ "./src/vue/partial/user/UserTable.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _UserTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _UserTable_vue_vue_type_template_id_75b32def___WEBPACK_IMPORTED_MODULE_0__["render"],
  _UserTable_vue_vue_type_template_id_75b32def___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null

)

/* hot reload */
if (false) { var api; }
component.options.__file = "src/vue/partial/user/UserTable.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./src/vue/partial/user/UserTable.vue?vue&type=script&lang=js&":
/*!*********************************************************************!*\
  !*** ./src/vue/partial/user/UserTable.vue?vue&type=script&lang=js& ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_1_node_modules_vue_loader_lib_index_js_vue_loader_options_UserTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--1!../../../../node_modules/vue-loader/lib??vue-loader-options!./UserTable.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/user/UserTable.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_1_node_modules_vue_loader_lib_index_js_vue_loader_options_UserTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]);

/***/ }),

/***/ "./src/vue/partial/user/UserTable.vue?vue&type=template&id=75b32def&":
/*!***************************************************************************!*\
  !*** ./src/vue/partial/user/UserTable.vue?vue&type=template&id=75b32def& ***!
  \***************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UserTable_vue_vue_type_template_id_75b32def___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./UserTable.vue?vue&type=template&id=75b32def& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/user/UserTable.vue?vue&type=template&id=75b32def&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UserTable_vue_vue_type_template_id_75b32def___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UserTable_vue_vue_type_template_id_75b32def___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./src/vue/partial/user/UserTableRow.vue":
/*!***********************************************!*\
  !*** ./src/vue/partial/user/UserTableRow.vue ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _UserTableRow_vue_vue_type_template_id_42c1b89b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./UserTableRow.vue?vue&type=template&id=42c1b89b& */ "./src/vue/partial/user/UserTableRow.vue?vue&type=template&id=42c1b89b&");
/* harmony import */ var _UserTableRow_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./UserTableRow.vue?vue&type=script&lang=js& */ "./src/vue/partial/user/UserTableRow.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _UserTableRow_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _UserTableRow_vue_vue_type_template_id_42c1b89b___WEBPACK_IMPORTED_MODULE_0__["render"],
  _UserTableRow_vue_vue_type_template_id_42c1b89b___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null

)

/* hot reload */
if (false) { var api; }
component.options.__file = "src/vue/partial/user/UserTableRow.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./src/vue/partial/user/UserTableRow.vue?vue&type=script&lang=js&":
/*!************************************************************************!*\
  !*** ./src/vue/partial/user/UserTableRow.vue?vue&type=script&lang=js& ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_1_node_modules_vue_loader_lib_index_js_vue_loader_options_UserTableRow_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--1!../../../../node_modules/vue-loader/lib??vue-loader-options!./UserTableRow.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/user/UserTableRow.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_1_node_modules_vue_loader_lib_index_js_vue_loader_options_UserTableRow_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]);

/***/ }),

/***/ "./src/vue/partial/user/UserTableRow.vue?vue&type=template&id=42c1b89b&":
/*!******************************************************************************!*\
  !*** ./src/vue/partial/user/UserTableRow.vue?vue&type=template&id=42c1b89b& ***!
  \******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UserTableRow_vue_vue_type_template_id_42c1b89b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./UserTableRow.vue?vue&type=template&id=42c1b89b& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/user/UserTableRow.vue?vue&type=template&id=42c1b89b&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UserTableRow_vue_vue_type_template_id_42c1b89b___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UserTableRow_vue_vue_type_template_id_42c1b89b___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ 0:
/*!******************************************************************************************************************!*\
  !*** multi (webpack)-dev-server/client?http://localhost:1236 ./src/js/vendor.js ./src/js/partial/index/index.js ***!
  \******************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! D:\Project\share\outsouce_thuenha\html\static\backend\v1\node_modules\webpack-dev-server\client\index.js?http://localhost:1236 */"./node_modules/webpack-dev-server/client/index.js?http://localhost:1236");
__webpack_require__(/*! D:\Project\share\outsouce_thuenha\html\static\backend\v1\src/js/vendor.js */"./src/js/vendor.js");
module.exports = __webpack_require__(/*! D:\Project\share\outsouce_thuenha\html\static\backend\v1\src/js/partial/index/index.js */"./src/js/partial/index/index.js");


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vL3NyYy92dWUvcGFydGlhbC9nbG9iYWwvVGhlTW9kYWxGdWxsU2NyZWVuLnZ1ZSIsIndlYnBhY2s6Ly8vc3JjL3Z1ZS9wYXJ0aWFsL3VzZXIvVXNlclRhYmxlLnZ1ZSIsIndlYnBhY2s6Ly8vc3JjL3Z1ZS9wYXJ0aWFsL3VzZXIvVXNlclRhYmxlUm93LnZ1ZSIsIndlYnBhY2s6Ly8vLi9zcmMvdnVlL3BhcnRpYWwvZ2xvYmFsL1RoZU1vZGFsRnVsbFNjcmVlbi52dWU/M2MyNCIsIndlYnBhY2s6Ly8vLi9zcmMvdnVlL3BhcnRpYWwvdXNlci9Vc2VyVGFibGUudnVlPzY1ZmUiLCJ3ZWJwYWNrOi8vLy4vc3JjL3Z1ZS9wYXJ0aWFsL3VzZXIvVXNlclRhYmxlUm93LnZ1ZT83OGM5Iiwid2VicGFjazovLy8od2VicGFjaykvaG90IHN5bmMgbm9ucmVjdXJzaXZlIF5cXC5cXC9sb2ckIiwid2VicGFjazovLy8uL3NyYy9qcy9wYXJ0aWFsL2luZGV4L2luZGV4LmpzIiwid2VicGFjazovLy8uL3NyYy9qcy9zdG9yZS91c2VyLmpzIiwid2VicGFjazovLy8uL3NyYy92dWUvcGFydGlhbC9nbG9iYWwvVGhlTW9kYWxGdWxsU2NyZWVuLnZ1ZSIsIndlYnBhY2s6Ly8vLi9zcmMvdnVlL3BhcnRpYWwvZ2xvYmFsL1RoZU1vZGFsRnVsbFNjcmVlbi52dWU/YTNjYSIsIndlYnBhY2s6Ly8vLi9zcmMvdnVlL3BhcnRpYWwvZ2xvYmFsL1RoZU1vZGFsRnVsbFNjcmVlbi52dWU/MGRlMCIsIndlYnBhY2s6Ly8vLi9zcmMvdnVlL3BhcnRpYWwvdXNlci9Vc2VyVGFibGUudnVlIiwid2VicGFjazovLy8uL3NyYy92dWUvcGFydGlhbC91c2VyL1VzZXJUYWJsZS52dWU/NzA3MSIsIndlYnBhY2s6Ly8vLi9zcmMvdnVlL3BhcnRpYWwvdXNlci9Vc2VyVGFibGUudnVlPzcxNzUiLCJ3ZWJwYWNrOi8vLy4vc3JjL3Z1ZS9wYXJ0aWFsL3VzZXIvVXNlclRhYmxlUm93LnZ1ZSIsIndlYnBhY2s6Ly8vLi9zcmMvdnVlL3BhcnRpYWwvdXNlci9Vc2VyVGFibGVSb3cudnVlPzk2ZmIiLCJ3ZWJwYWNrOi8vLy4vc3JjL3Z1ZS9wYXJ0aWFsL3VzZXIvVXNlclRhYmxlUm93LnZ1ZT84NGQwIl0sIm5hbWVzIjpbIlZ1ZSIsInVzZSIsIlZ1ZXgiLCJzdG9yZSIsIlN0b3JlIiwiT2JqZWN0IiwiX3JlbmRlclVzZXJUYWJsZSIsIiRlbCIsIiQiLCJyZW5kZXIiLCJoIiwiVXNlclRhYmxlIiwiJG1vdW50IiwiaW5pdCIsImRvY3VtZW50IiwiYWRkRXZlbnRMaXN0ZW5lciIsIl9vYmpFbnRpdHkiLCJ1c2VyX2lkIiwicHJvZmlsZV9pbWFnZSIsImZ1bGxuYW1lIiwiZW1haWwiLCJwaG9uZSIsImlzX2FjdGl2ZWQiLCJzdGF0ZSIsImZsYWciLCJpc0dldCIsImFyckVudGl0eUxpc3QiLCJvYmpFbnRpdHlTZWxlY3QiLCJtdXRhdGlvbnMiLCJyZXNldEVudGl0eSIsInVwZGF0ZUZsYWciLCJwYXlsb2FkIiwidmFsdWUiLCJzZXRFbnRpdGllcyIsInNldEVudGl0eSIsIl9pc0VtcHR5IiwicGFzc3dvcmQiLCJ1cGRhdGVFbnRpdHkiLCJ0eXBlIiwiaW5kZXgiLCJhcnJFbnRpdHkiLCJwdXNoIiwic3BsaWNlIiwiYWN0aW9ucyIsImdldEVudGl0eUFjdCIsImNvbW1pdCIsIlByb21pc2UiLCJyZXNvbHZlIiwicmVqZWN0IiwicmVxUGFyYW1zIiwiQVBJIiwiZG9uZSIsInJlcyIsImlzRXJyb3JBUEkiLCJhcnJVc2VyRGV0YWlsIiwicmVhZEVudGl0eUFjdCIsImFyclVzZXJMaXN0IiwidXBsb2FkRmlsZUFjdCIsImZvcm1EYXRhIiwiRm9ybURhdGEiLCJhcHBlbmQiLCJjb250cm9sbGVyIiwiZmlsZSIsInByb2Nlc3NEYXRhIiwiY29udGVudFR5cGUiLCJ0cmlnZ2VyTW9kYWwiLCJtZXNzYWdlIiwic2V0RW50aXR5QWN0IiwiZ2V0dGVycyIsImhhc093blByb3BlcnR5IiwiZ2V0RW50aXR5QnlJbmRleCIsImNyZWF0ZUVudGl0eUFjdCIsInVwZGF0ZUVudGl0eUFjdCIsImdldEluZGV4QnlJRCIsImRlbGV0ZUVudGl0eUFjdCIsImlkIiwiY29uc29sZSIsImxvZyIsImZpbHRlciIsIm9iakVudGl0eSIsImN1cnJlbnRJbmRleCIsIl9mb3JJbiJdLCJtYXBwaW5ncyI6IjtRQUFBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsUUFBUSxvQkFBb0I7UUFDNUI7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQSxpQkFBaUIsNEJBQTRCO1FBQzdDO1FBQ0E7UUFDQSxrQkFBa0IsMkJBQTJCO1FBQzdDO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7OztRQUdBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwwQ0FBMEMsZ0NBQWdDO1FBQzFFO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0Esd0RBQXdELGtCQUFrQjtRQUMxRTtRQUNBLGlEQUFpRCxjQUFjO1FBQy9EOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQSx5Q0FBeUMsaUNBQWlDO1FBQzFFLGdIQUFnSCxtQkFBbUIsRUFBRTtRQUNySTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDJCQUEyQiwwQkFBMEIsRUFBRTtRQUN2RCxpQ0FBaUMsZUFBZTtRQUNoRDtRQUNBO1FBQ0E7O1FBRUE7UUFDQSxzREFBc0QsK0RBQStEOztRQUVySDtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EsZ0JBQWdCLHVCQUF1QjtRQUN2Qzs7O1FBR0E7UUFDQTtRQUNBO1FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMvR0E7QUFDQTtBQUVBO0FBQ0EsZUFEQTtBQUVBLGdCQUZBO0FBR0EsTUFIQSxrQkFHQTtBQUNBO0FBQ0EsMEJBREE7QUFFQTtBQUZBO0FBSUEsR0FSQTtBQVNBLGNBVEE7QUFVQSxTQVZBLHFCQVVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FGQTtBQUlBO0FBQ0E7QUFFQSwwQ0FDQTtBQUVBO0FBQ0EsaUJBQ0E7QUFFQTtBQUNBO0FBQ0EsS0FaQTtBQWFBLEdBN0JBO0FBOEJBO0FBQ0EsV0FEQSxxQkFDQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBOUJBLEc7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ1pBO0FBRUE7QUFDQSxvQkFEQTtBQUVBO0FBQ0E7QUFEQSxHQUZBO0FBS0EsTUFMQSxrQkFLQTtBQUNBO0FBRUEsR0FSQTtBQVNBO0FBQ0EsaUJBREEsMkJBQ0E7QUFDQTtBQUNBO0FBSEEsR0FUQTtBQWNBLFNBZEEscUJBY0EsQ0FFQSxDQWhCQTtBQWlCQTtBQWpCQSxHOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNvQkE7QUFFQTtBQUNBLHdCQURBO0FBRUE7QUFDQTtBQUNBLGlCQURBO0FBRUE7QUFGQSxLQURBO0FBS0E7QUFDQSxpQkFEQTtBQUVBO0FBRkE7QUFMQSxHQUZBO0FBWUEsZ0JBWkE7QUFhQSxNQWJBLGtCQWFBO0FBQ0E7QUFDQTtBQUNBO0FBREEsT0FEQTtBQUlBO0FBQ0E7QUFEQTtBQUpBO0FBUUEsR0F0QkE7QUF1QkE7QUFDQSxnQkFEQSwwQkFDQTtBQUNBLDRIQUNBO0FBRUE7QUFDQTtBQU5BLEdBdkJBO0FBK0JBLFNBL0JBLHFCQStCQSxDQUNBLENBaENBO0FBaUNBO0FBQ0EsWUFEQSxzQkFDQTtBQUNBO0FBQUE7QUFBQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0EsS0FMQTtBQU1BLFVBTkEsa0JBTUEsRUFOQSxFQU1BO0FBQUE7O0FBQ0EsZUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQSxPQUZBO0FBR0E7QUFkQTtBQWpDQSxHOzs7Ozs7Ozs7Ozs7QUN2REE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSyw2Q0FBNkMsaUJBQWlCLEVBQUU7QUFDckU7QUFDQSxpQkFBaUIsc0NBQXNDLG1CQUFtQixFQUFFO0FBQzVFLG1CQUFtQiwrQkFBK0I7QUFDbEQscUJBQXFCLDRCQUE0QjtBQUNqRCx1QkFBdUIsdUNBQXVDO0FBQzlELHlCQUF5QixvREFBb0Q7QUFDN0UsMkJBQTJCLG1DQUFtQztBQUM5RCw2QkFBNkIsK0JBQStCO0FBQzVEO0FBQ0E7QUFDQSw4QkFBOEIsa0NBQWtDO0FBQ2hFO0FBQ0E7QUFDQTtBQUNBLDZCQUE2Qiw2QkFBNkI7QUFDMUQ7QUFDQTtBQUNBO0FBQ0EsK0JBQStCLDZCQUE2QjtBQUM1RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlCQUF5QjtBQUN6QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx3QkFBd0IscUNBQXFDO0FBQzdEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWU7QUFDZix3QkFBd0IsMEJBQTBCO0FBQ2xEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNCQUFzQiwwQ0FBMEM7QUFDaEU7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7OztBQzlFQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLLHdDQUF3QyxtQkFBbUIsRUFBRTtBQUNsRTtBQUNBO0FBQ0E7QUFDQSxTQUFTLG9EQUFvRCxlQUFlLEVBQUU7QUFDOUU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNEJBQTRCO0FBQzVCLG1CQUFtQjtBQUNuQjtBQUNBLGVBQWU7QUFDZjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQiw4QkFBOEI7QUFDbkQ7QUFDQTtBQUNBLHNCQUFzQjtBQUN0QixhQUFhO0FBQ2I7QUFDQSx5QkFBeUIsU0FBUyxxQkFBcUIsRUFBRTtBQUN6RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7OztBQy9EQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLDRCQUE0QjtBQUM3QyxtQkFBbUIsMkJBQTJCO0FBQzlDLHFCQUFxQiwyQkFBMkI7QUFDaEQsdUJBQXVCLFNBQVMsaUNBQWlDLEVBQUU7QUFDbkU7QUFDQTtBQUNBLHFCQUFxQixzQkFBc0I7QUFDM0Msd0JBQXdCLHVCQUF1QjtBQUMvQztBQUNBO0FBQ0E7QUFDQSx3QkFBd0IsMkJBQTJCO0FBQ25EO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLHlEQUF5RDtBQUMxRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsd0RBQXdEO0FBQ3pFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsY0FBYywwQ0FBMEM7QUFDeEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNULGtCQUFrQiwyQkFBMkI7QUFDN0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQiwwQkFBMEI7QUFDM0MscUJBQXFCLFNBQVMsc0NBQXNDLEVBQUU7QUFDdEU7QUFDQSxxQkFBcUIsU0FBUyxxQkFBcUIsRUFBRTtBQUNyRDtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7O0FDckdBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGtFOzs7Ozs7Ozs7Ozs7QUN0QkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBRUFBLDJDQUFHLENBQUNDLEdBQUosQ0FBUUMsNENBQVI7QUFFQSxJQUFNQyxLQUFLLEdBQUcsSUFBSUQsNENBQUksQ0FBQ0UsS0FBVCxDQUFlQSx5REFBZixDQUFkOztBQUVBLElBQU1DLE1BQU0sR0FBSSxZQUFNO0FBQ3BCLE1BQU1DLGdCQUFnQixHQUFHLFNBQW5CQSxnQkFBbUIsR0FBTTtBQUM3QixRQUFJQyxHQUFHLEdBQUdDLDZDQUFDLENBQUMsYUFBRCxDQUFYO0FBRUEsUUFBSVIsMkNBQUosQ0FBUTtBQUNORyxXQUFLLEVBQUxBLEtBRE07QUFFTk0sWUFBTSxFQUFFLGdCQUFBQyxDQUFDO0FBQUEsZUFBSUEsQ0FBQyxDQUFDQyxzRUFBRCxDQUFMO0FBQUE7QUFGSCxLQUFSLEVBR0dDLE1BSEgsQ0FHVUwsR0FBRyxDQUFFLENBQUYsQ0FIYjtBQUlELEdBUEQ7O0FBU0EsU0FBTztBQUNMTSxRQURLLGtCQUNFO0FBQ0xQLHNCQUFnQjtBQUNqQjtBQUhJLEdBQVA7QUFLRCxDQWZjLEVBQWY7O0FBaUJBUSxRQUFRLENBQUNDLGdCQUFULENBQTBCLGtCQUExQixFQUE4QyxZQUFZO0FBQ3hEVixRQUFNLENBQUNRLElBQVA7QUFDRCxDQUZELEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDN0JBO0FBRUE7QUFDQTtBQUNBO0FBRUEsSUFBSUcsVUFBVSxHQUFHO0FBQ2ZDLFNBQU8sRUFBRSxJQURNO0FBRWZDLGVBQWEsRUFBRSxJQUZBO0FBR2ZDLFVBQVEsRUFBRSxJQUhLO0FBSWZDLE9BQUssRUFBRSxJQUpRO0FBS2ZDLE9BQUssRUFBRSxJQUxRO0FBTWZDLFlBQVUsRUFBRTtBQU5HLENBQWpCOztJQVNNbEIsSyxHQUNKLGlCQUFjO0FBQUE7O0FBQ1osU0FBTztBQUNMbUIsU0FBSyxFQUFFO0FBQ0xDLFVBQUksRUFBRTtBQUNKQyxhQUFLLEVBQUU7QUFESCxPQUREO0FBSUxDLG1CQUFhLEVBQUUsRUFKVjtBQUtMQyxxQkFBZSxvQkFBT1gsVUFBUDtBQUxWLEtBREY7QUFRTFksYUFBUyxFQUFFO0FBQ1RDLGlCQURTLHVCQUNHTixLQURILEVBQ1U7QUFDakJBLGFBQUssQ0FBQ0ksZUFBTixxQkFBNkJYLFVBQTdCO0FBQ0QsT0FIUTtBQUlUYyxnQkFKUyxzQkFJRVAsS0FKRixFQUlTUSxPQUpULEVBSWtCO0FBQ3pCUixhQUFLLENBQUNDLElBQU4sQ0FBWU8sT0FBTyxDQUFDUCxJQUFwQixJQUE2Qk8sT0FBTyxDQUFDQyxLQUFyQztBQUNELE9BTlE7QUFPVEMsaUJBUFMsdUJBT0dWLEtBUEgsRUFPVVEsT0FQVixFQU9tQjtBQUMxQlIsYUFBSyxDQUFDRyxhQUFOLEdBQXNCSyxPQUFPLENBQUNMLGFBQTlCO0FBQ0QsT0FUUTtBQVVUUSxlQVZTLHFCQVVDWCxLQVZELEVBVVFRLE9BVlIsRUFVaUI7QUFDeEIsWUFBSUksc0RBQVEsQ0FBQ0osT0FBTyxDQUFDSixlQUFULENBQVosRUFDRTs7QUFGc0IseUNBSU9JLE9BSlA7QUFBQSxZQUlsQkosZUFKa0IsWUFJbEJBLGVBSmtCOztBQU14QkEsdUJBQWUsQ0FBQ1MsUUFBaEIsR0FBMkIsSUFBM0I7QUFFQWIsYUFBSyxDQUFDSSxlQUFOLEdBQXdCQSxlQUF4QjtBQUNELE9BbkJRO0FBb0JUVSxrQkFwQlMsd0JBb0JJZCxLQXBCSixFQW9CV1EsT0FwQlgsRUFvQm9CO0FBQUEsMENBQ1dBLE9BRFg7QUFBQSxZQUNyQk8sSUFEcUIsYUFDckJBLElBRHFCO0FBQUEsWUFDZkMsS0FEZSxhQUNmQSxLQURlO0FBQUEsWUFDUkMsU0FEUSxhQUNSQSxTQURROztBQUczQixnQkFBUUYsSUFBUjtBQUNFLGVBQUssUUFBTDtBQUNFZixpQkFBSyxDQUFDRyxhQUFOLENBQW9CZSxJQUFwQixDQUF5QkQsU0FBekI7QUFDQTs7QUFDRixlQUFLLFFBQUw7QUFDRWpCLGlCQUFLLENBQUNHLGFBQU4sQ0FBcUJhLEtBQXJCLElBQStCQyxTQUEvQjtBQUNBakIsaUJBQUssQ0FBQ0ksZUFBTixHQUF3QmEsU0FBeEI7QUFFQTs7QUFDRixlQUFLLFFBQUw7QUFDRWpCLGlCQUFLLENBQUNHLGFBQU4sQ0FBb0JnQixNQUFwQixDQUEyQkgsS0FBM0IsRUFBa0MsQ0FBbEM7QUFDQTtBQVhKO0FBYUQ7QUFwQ1EsS0FSTjtBQThDTEksV0FBTyxFQUFFO0FBQ1BDLGtCQURPLDhCQUNrQmIsT0FEbEIsRUFDMkI7QUFBQSxZQUFuQmMsTUFBbUIsUUFBbkJBLE1BQW1CO0FBQ2hDLGVBQU8sSUFBSUMsT0FBSixDQUFZLFVBQVVDLE9BQVYsRUFBbUJDLE1BQW5CLEVBQTJCO0FBQzVDLGNBQUlDLFNBQVMscUJBQVFsQixPQUFSLENBQWI7O0FBQ0FtQixzRUFBQSxDQUFlRCxTQUFmLEVBQTBCRSxJQUExQixDQUErQixVQUFDQyxHQUFELEVBQVM7QUFDdEMsZ0JBQUlDLGlGQUFVLENBQUNELEdBQUQsQ0FBZCxFQUFxQjtBQUNuQkosb0JBQU0sQ0FBQ0ksR0FBRyxDQUFDckIsT0FBTCxDQUFOO0FBQ0E7QUFDRDs7QUFKcUMsaURBTVRxQixHQUFHLENBQUNyQixPQU5LO0FBQUEsZ0JBTWhDdUIsYUFOZ0MsZ0JBTWhDQSxhQU5nQzs7QUFRdENULGtCQUFNLENBQUMsV0FBRCxFQUFjO0FBQUVsQiw2QkFBZSxFQUFFMkI7QUFBbkIsYUFBZCxDQUFOO0FBRUFQLG1CQUFPLENBQUNLLEdBQUcsQ0FBQ3JCLE9BQUwsQ0FBUDtBQUNELFdBWEQ7QUFZRCxTQWRNLENBQVA7QUFlRCxPQWpCTTtBQWtCUHdCLG1CQWxCTyxnQ0FrQm1CO0FBQUEsWUFBVlYsTUFBVSxTQUFWQSxNQUFVO0FBQ3hCLGVBQU8sSUFBSUMsT0FBSixDQUFZLFVBQVVDLE9BQVYsRUFBbUJDLE1BQW5CLEVBQTJCO0FBQzVDLGNBQUlDLFNBQVMsR0FBRyxFQUFoQjtBQUNBQyx1RUFBQSxDQUFnQkQsU0FBaEIsRUFBMkJFLElBQTNCLENBQWdDLFVBQUNDLEdBQUQsRUFBUztBQUN2QyxnQkFBSUMsaUZBQVUsQ0FBQ0QsR0FBRCxDQUFkLEVBQXFCO0FBQ25CSixvQkFBTSxDQUFDSSxHQUFHLENBQUNyQixPQUFMLENBQU47QUFDQTtBQUNEOztBQUpzQyxrREFNWnFCLEdBQUcsQ0FBQ3JCLE9BTlE7QUFBQSxnQkFNakN5QixXQU5pQyxpQkFNakNBLFdBTmlDOztBQVF2Q1gsa0JBQU0sQ0FBQyxhQUFELEVBQWdCO0FBQUVuQiwyQkFBYSxFQUFFOEI7QUFBakIsYUFBaEIsQ0FBTjtBQUVBVCxtQkFBTyxDQUFDSyxHQUFHLENBQUNyQixPQUFMLENBQVA7QUFDRCxXQVhEO0FBWUQsU0FkTSxDQUFQO0FBZUQsT0FsQ007QUFtQ1AwQixtQkFuQ08sZ0NBbUNtQjFCLE9BbkNuQixFQW1DNEI7QUFBQSxZQUFuQmMsTUFBbUIsU0FBbkJBLE1BQW1CO0FBQ2pDLGVBQU8sSUFBSUMsT0FBSixDQUFZLFVBQVVDLE9BQVYsRUFBbUJDLE1BQW5CLEVBQTJCO0FBQzVDLGNBQUlVLFFBQVEsR0FBRyxJQUFJQyxRQUFKLEVBQWY7QUFFQUQsa0JBQVEsQ0FBQ0UsTUFBVCxDQUFnQixZQUFoQixFQUE4QjdCLE9BQU8sQ0FBQzhCLFVBQXRDO0FBQ0FILGtCQUFRLENBQUNFLE1BQVQsQ0FBZ0IsTUFBaEIsRUFBd0I3QixPQUFPLENBQUMrQixJQUFoQztBQUVBWixxRUFBQSxDQUFjUSxRQUFkLEVBQXdCO0FBQ3RCSyx1QkFBVyxFQUFFLEtBRFM7QUFFdEJDLHVCQUFXLEVBQUU7QUFGUyxXQUF4QixFQUdHYixJQUhILENBR1EsVUFBQUMsR0FBRyxFQUFJO0FBQ2IsZ0JBQUlDLGlGQUFVLENBQUNELEdBQUQsQ0FBZCxFQUFxQjtBQUNuQmEsaUdBQVksQ0FBQ2IsR0FBRyxDQUFDYyxPQUFMLENBQVo7QUFFQWxCLG9CQUFNLENBQUNJLEdBQUcsQ0FBQ3JCLE9BQUwsQ0FBTjtBQUNBO0FBQ0Q7O0FBRURnQixtQkFBTyxDQUFDSyxHQUFHLENBQUNyQixPQUFMLENBQVA7QUFDRCxXQVpEO0FBYUQsU0FuQk0sQ0FBUDtBQW9CRCxPQXhETTtBQXlEUG9DLGtCQXpETywrQkF5RDJCcEMsT0F6RDNCLEVBeURvQztBQUFBLFlBQTVCYyxNQUE0QixTQUE1QkEsTUFBNEI7QUFBQSxZQUFwQnVCLE9BQW9CLFNBQXBCQSxPQUFvQjtBQUN6QyxZQUFJLENBQUNyQyxPQUFPLENBQUNzQyxjQUFSLENBQXVCLE9BQXZCLENBQUwsRUFDRTs7QUFGdUMsMENBSXBCdEMsT0FKb0I7QUFBQSxZQUluQ1EsS0FKbUMsYUFJbkNBLEtBSm1DOztBQU16Q00sY0FBTSxDQUFDLFdBQUQsRUFBYztBQUFFbEIseUJBQWUsRUFBRXlDLE9BQU8sQ0FBQ0UsZ0JBQVIsQ0FBeUIvQixLQUF6QixFQUFpQyxDQUFqQztBQUFuQixTQUFkLENBQU47QUFDRCxPQWhFTTtBQWlFUGdDLHFCQWpFTyxrQ0FpRThCeEMsT0FqRTlCLEVBaUV1QztBQUFBLFlBQTVCYyxNQUE0QixTQUE1QkEsTUFBNEI7QUFBQSxZQUFwQnVCLE9BQW9CLFNBQXBCQSxPQUFvQjtBQUM1QyxlQUFPLElBQUl0QixPQUFKLENBQVksVUFBVUMsT0FBVixFQUFtQkMsTUFBbkIsRUFBMkI7QUFDNUMsY0FBSUMsU0FBUyxxQkFBUWxCLE9BQVIsQ0FBYjs7QUFDQW1CLHlFQUFBLENBQWtCRCxTQUFsQixFQUE2QkUsSUFBN0IsQ0FBa0MsVUFBQ0MsR0FBRCxFQUFTO0FBQ3pDYSwrRkFBWSxDQUFDYixHQUFHLENBQUNjLE9BQUwsQ0FBWjs7QUFDQSxnQkFBSWIsaUZBQVUsQ0FBQ0QsR0FBRCxDQUFkLEVBQXFCO0FBQ25CSixvQkFBTSxDQUFDSSxHQUFHLENBQUNyQixPQUFMLENBQU47QUFDQTtBQUNEOztBQUx3QyxrREFPWnFCLEdBQUcsQ0FBQ3JCLE9BUFE7QUFBQSxnQkFPbkN1QixhQVBtQyxpQkFPbkNBLGFBUG1DOztBQVN6Q1Qsa0JBQU0sQ0FBQyxjQUFELEVBQWlCO0FBQ3JCUCxrQkFBSSxFQUFFLFFBRGU7QUFFckJFLHVCQUFTLEVBQUVjO0FBRlUsYUFBakIsQ0FBTjtBQUtBUCxtQkFBTyxDQUFDSyxHQUFHLENBQUNyQixPQUFMLENBQVA7QUFDRCxXQWZEO0FBZ0JELFNBbEJNLENBQVA7QUFtQkQsT0FyRk07QUFzRlB5QyxxQkF0Rk8sa0NBc0Y4QnpDLE9BdEY5QixFQXNGdUM7QUFBQSxZQUE1QmMsTUFBNEIsU0FBNUJBLE1BQTRCO0FBQUEsWUFBcEJ1QixPQUFvQixTQUFwQkEsT0FBb0I7QUFDNUMsZUFBTyxJQUFJdEIsT0FBSixDQUFZLFVBQVVDLE9BQVYsRUFBbUJDLE1BQW5CLEVBQTJCO0FBQzVDLGNBQUlDLFNBQVMscUJBQVFsQixPQUFSLENBQWI7O0FBRUFtQix5RUFBQSxDQUFrQkQsU0FBbEIsRUFBNkJFLElBQTdCLENBQWtDLFVBQUNDLEdBQUQsRUFBUztBQUN6Q2EsK0ZBQVksQ0FBQ2IsR0FBRyxDQUFDYyxPQUFMLENBQVo7O0FBQ0EsZ0JBQUliLGlGQUFVLENBQUNELEdBQUQsQ0FBZCxFQUFxQjtBQUNuQkosb0JBQU0sQ0FBQ0ksR0FBRyxDQUFDckIsT0FBTCxDQUFOO0FBQ0E7QUFDRDs7QUFMd0Msa0RBT1pxQixHQUFHLENBQUNyQixPQVBRO0FBQUEsZ0JBT25DdUIsYUFQbUMsaUJBT25DQSxhQVBtQzs7QUFTekNULGtCQUFNLENBQUMsY0FBRCxFQUFpQjtBQUNyQlAsa0JBQUksRUFBRSxRQURlO0FBRXJCQyxtQkFBSyxFQUFFNkIsT0FBTyxDQUFDSyxZQUFSLENBQXFCbkIsYUFBYSxDQUFDckMsT0FBbkMsQ0FGYztBQUdyQnVCLHVCQUFTLEVBQUVjO0FBSFUsYUFBakIsQ0FBTjtBQU1BUCxtQkFBTyxDQUFDSyxHQUFHLENBQUNyQixPQUFMLENBQVA7QUFDRCxXQWhCRDtBQWlCRCxTQXBCTSxDQUFQO0FBcUJELE9BNUdNO0FBNkdQMkMscUJBN0dPLGtDQTZHOEIzQyxPQTdHOUIsRUE2R3VDO0FBQUEsWUFBNUJjLE1BQTRCLFNBQTVCQSxNQUE0QjtBQUFBLFlBQXBCdUIsT0FBb0IsU0FBcEJBLE9BQW9CO0FBQzVDLGVBQU8sSUFBSXRCLE9BQUosQ0FBWSxVQUFVQyxPQUFWLEVBQW1CQyxNQUFuQixFQUEyQjtBQUM1QyxjQUFJQyxTQUFTLHFCQUFRbEIsT0FBUixDQUFiOztBQUVBbUIseUVBQUEsQ0FBa0JELFNBQWxCLEVBQTZCRSxJQUE3QixDQUFrQyxVQUFBQyxHQUFHLEVBQUk7QUFDdkNhLCtGQUFZLENBQUNiLEdBQUcsQ0FBQ2MsT0FBTCxDQUFaOztBQUNBLGdCQUFJYixpRkFBVSxDQUFDRCxHQUFELENBQWQsRUFBcUI7QUFDbkJKLG9CQUFNLENBQUNJLEdBQUcsQ0FBQ3JCLE9BQUwsQ0FBTjtBQUVBO0FBQ0Q7O0FBTnNDLGtEQVFyQnFCLEdBQUcsQ0FBQ3JCLE9BUmlCO0FBQUEsZ0JBUWpDNEMsRUFSaUMsaUJBUWpDQSxFQVJpQzs7QUFVdkNDLG1CQUFPLENBQUNDLEdBQVIsQ0FBWUYsRUFBWjtBQUVBOUIsa0JBQU0sQ0FBQyxjQUFELEVBQWlCO0FBQ3JCUCxrQkFBSSxFQUFFLFFBRGU7QUFFckJDLG1CQUFLLEVBQUU2QixPQUFPLENBQUNLLFlBQVIsQ0FBcUJFLEVBQXJCO0FBRmMsYUFBakIsQ0FBTjtBQUtBNUIsbUJBQU8sQ0FBQ0ssR0FBRyxDQUFDckIsT0FBTCxDQUFQO0FBQ0QsV0FsQkQ7QUFtQkQsU0F0Qk0sQ0FBUDtBQXVCRDtBQXJJTSxLQTlDSjtBQXFMTHFDLFdBQU8sRUFBRTtBQUNQRSxzQkFBZ0IsRUFBRSwwQkFBQS9DLEtBQUs7QUFBQSxlQUFJLFVBQUFnQixLQUFLLEVBQUk7QUFDbEMsaUJBQU9oQixLQUFLLENBQUNHLGFBQU4sQ0FBb0JvRCxNQUFwQixDQUEyQixVQUFDQyxTQUFELEVBQVlDLFlBQVo7QUFBQSxtQkFBNkJBLFlBQVksSUFBSXpDLEtBQTdDO0FBQUEsV0FBM0IsQ0FBUDtBQUNELFNBRnNCO0FBQUEsT0FEaEI7QUFJUGtDLGtCQUFZLEVBQUUsc0JBQUFsRCxLQUFLO0FBQUEsZUFBSSxVQUFBb0QsRUFBRSxFQUFJO0FBQzNCLGNBQUlwQyxLQUFLLEdBQUcsQ0FBQyxDQUFiOztBQUNBMEMsOERBQU0sQ0FBQzFELEtBQUssQ0FBQ0csYUFBUCxFQUFzQixVQUFDcUQsU0FBRCxFQUFZQyxZQUFaLEVBQTZCO0FBQ3ZELGdCQUFJRCxTQUFTLENBQUM5RCxPQUFWLElBQXFCMEQsRUFBekIsRUFDRTtBQUVGcEMsaUJBQUssR0FBR3lDLFlBQVI7QUFDQSxtQkFBTyxLQUFQO0FBQ0QsV0FOSyxDQUFOOztBQVFBLGlCQUFPekMsS0FBUDtBQUNELFNBWGtCO0FBQUE7QUFKWjtBQXJMSixHQUFQO0FBdU1ELEM7O0FBR2EsbUVBQUluQyxLQUFKLEVBQWhCLEU7Ozs7Ozs7Ozs7OztBQzNOQTtBQUFBO0FBQUE7QUFBQTtBQUFpRztBQUMzQjtBQUNMOzs7QUFHakU7QUFDZ0c7QUFDaEcsZ0JBQWdCLDJHQUFVO0FBQzFCLEVBQUUsd0ZBQU07QUFDUixFQUFFLDZGQUFNO0FBQ1IsRUFBRSxzR0FBZTtBQUNqQjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBLElBQUksS0FBVSxFQUFFLFlBaUJmO0FBQ0Q7QUFDZSxnRjs7Ozs7Ozs7Ozs7O0FDdENmO0FBQUE7QUFBQSx3Q0FBc00sQ0FBZ0IsNFBBQUcsRUFBQyxDOzs7Ozs7Ozs7Ozs7QUNBMU47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOzs7Ozs7Ozs7Ozs7O0FDQUE7QUFBQTtBQUFBO0FBQUE7QUFBd0Y7QUFDM0I7QUFDTDs7O0FBR3hEO0FBQ2dHO0FBQ2hHLGdCQUFnQiwyR0FBVTtBQUMxQixFQUFFLCtFQUFNO0FBQ1IsRUFBRSxvRkFBTTtBQUNSLEVBQUUsNkZBQWU7QUFDakI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQSxJQUFJLEtBQVUsRUFBRSxZQWlCZjtBQUNEO0FBQ2UsZ0Y7Ozs7Ozs7Ozs7OztBQ3RDZjtBQUFBO0FBQUEsd0NBQTZMLENBQWdCLG1QQUFHLEVBQUMsQzs7Ozs7Ozs7Ozs7O0FDQWpOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7Ozs7Ozs7Ozs7OztBQ0FBO0FBQUE7QUFBQTtBQUFBO0FBQTJGO0FBQzNCO0FBQ0w7OztBQUczRDtBQUNnRztBQUNoRyxnQkFBZ0IsMkdBQVU7QUFDMUIsRUFBRSxrRkFBTTtBQUNSLEVBQUUsdUZBQU07QUFDUixFQUFFLGdHQUFlO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0EsSUFBSSxLQUFVLEVBQUUsWUFpQmY7QUFDRDtBQUNlLGdGOzs7Ozs7Ozs7Ozs7QUN0Q2Y7QUFBQTtBQUFBLHdDQUFnTSxDQUFnQixzUEFBRyxFQUFDLEM7Ozs7Ozs7Ozs7OztBQ0FwTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEiLCJmaWxlIjoianMvaW5kZXguanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBpbnN0YWxsIGEgSlNPTlAgY2FsbGJhY2sgZm9yIGNodW5rIGxvYWRpbmdcbiBcdGZ1bmN0aW9uIHdlYnBhY2tKc29ucENhbGxiYWNrKGRhdGEpIHtcbiBcdFx0dmFyIGNodW5rSWRzID0gZGF0YVswXTtcbiBcdFx0dmFyIG1vcmVNb2R1bGVzID0gZGF0YVsxXTtcbiBcdFx0dmFyIGV4ZWN1dGVNb2R1bGVzID0gZGF0YVsyXTtcblxuIFx0XHQvLyBhZGQgXCJtb3JlTW9kdWxlc1wiIHRvIHRoZSBtb2R1bGVzIG9iamVjdCxcbiBcdFx0Ly8gdGhlbiBmbGFnIGFsbCBcImNodW5rSWRzXCIgYXMgbG9hZGVkIGFuZCBmaXJlIGNhbGxiYWNrXG4gXHRcdHZhciBtb2R1bGVJZCwgY2h1bmtJZCwgaSA9IDAsIHJlc29sdmVzID0gW107XG4gXHRcdGZvcig7aSA8IGNodW5rSWRzLmxlbmd0aDsgaSsrKSB7XG4gXHRcdFx0Y2h1bmtJZCA9IGNodW5rSWRzW2ldO1xuIFx0XHRcdGlmKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChpbnN0YWxsZWRDaHVua3MsIGNodW5rSWQpICYmIGluc3RhbGxlZENodW5rc1tjaHVua0lkXSkge1xuIFx0XHRcdFx0cmVzb2x2ZXMucHVzaChpbnN0YWxsZWRDaHVua3NbY2h1bmtJZF1bMF0pO1xuIFx0XHRcdH1cbiBcdFx0XHRpbnN0YWxsZWRDaHVua3NbY2h1bmtJZF0gPSAwO1xuIFx0XHR9XG4gXHRcdGZvcihtb2R1bGVJZCBpbiBtb3JlTW9kdWxlcykge1xuIFx0XHRcdGlmKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChtb3JlTW9kdWxlcywgbW9kdWxlSWQpKSB7XG4gXHRcdFx0XHRtb2R1bGVzW21vZHVsZUlkXSA9IG1vcmVNb2R1bGVzW21vZHVsZUlkXTtcbiBcdFx0XHR9XG4gXHRcdH1cbiBcdFx0aWYocGFyZW50SnNvbnBGdW5jdGlvbikgcGFyZW50SnNvbnBGdW5jdGlvbihkYXRhKTtcblxuIFx0XHR3aGlsZShyZXNvbHZlcy5sZW5ndGgpIHtcbiBcdFx0XHRyZXNvbHZlcy5zaGlmdCgpKCk7XG4gXHRcdH1cblxuIFx0XHQvLyBhZGQgZW50cnkgbW9kdWxlcyBmcm9tIGxvYWRlZCBjaHVuayB0byBkZWZlcnJlZCBsaXN0XG4gXHRcdGRlZmVycmVkTW9kdWxlcy5wdXNoLmFwcGx5KGRlZmVycmVkTW9kdWxlcywgZXhlY3V0ZU1vZHVsZXMgfHwgW10pO1xuXG4gXHRcdC8vIHJ1biBkZWZlcnJlZCBtb2R1bGVzIHdoZW4gYWxsIGNodW5rcyByZWFkeVxuIFx0XHRyZXR1cm4gY2hlY2tEZWZlcnJlZE1vZHVsZXMoKTtcbiBcdH07XG4gXHRmdW5jdGlvbiBjaGVja0RlZmVycmVkTW9kdWxlcygpIHtcbiBcdFx0dmFyIHJlc3VsdDtcbiBcdFx0Zm9yKHZhciBpID0gMDsgaSA8IGRlZmVycmVkTW9kdWxlcy5sZW5ndGg7IGkrKykge1xuIFx0XHRcdHZhciBkZWZlcnJlZE1vZHVsZSA9IGRlZmVycmVkTW9kdWxlc1tpXTtcbiBcdFx0XHR2YXIgZnVsZmlsbGVkID0gdHJ1ZTtcbiBcdFx0XHRmb3IodmFyIGogPSAxOyBqIDwgZGVmZXJyZWRNb2R1bGUubGVuZ3RoOyBqKyspIHtcbiBcdFx0XHRcdHZhciBkZXBJZCA9IGRlZmVycmVkTW9kdWxlW2pdO1xuIFx0XHRcdFx0aWYoaW5zdGFsbGVkQ2h1bmtzW2RlcElkXSAhPT0gMCkgZnVsZmlsbGVkID0gZmFsc2U7XG4gXHRcdFx0fVxuIFx0XHRcdGlmKGZ1bGZpbGxlZCkge1xuIFx0XHRcdFx0ZGVmZXJyZWRNb2R1bGVzLnNwbGljZShpLS0sIDEpO1xuIFx0XHRcdFx0cmVzdWx0ID0gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBkZWZlcnJlZE1vZHVsZVswXSk7XG4gXHRcdFx0fVxuIFx0XHR9XG5cbiBcdFx0cmV0dXJuIHJlc3VsdDtcbiBcdH1cblxuIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gb2JqZWN0IHRvIHN0b3JlIGxvYWRlZCBhbmQgbG9hZGluZyBjaHVua3NcbiBcdC8vIHVuZGVmaW5lZCA9IGNodW5rIG5vdCBsb2FkZWQsIG51bGwgPSBjaHVuayBwcmVsb2FkZWQvcHJlZmV0Y2hlZFxuIFx0Ly8gUHJvbWlzZSA9IGNodW5rIGxvYWRpbmcsIDAgPSBjaHVuayBsb2FkZWRcbiBcdHZhciBpbnN0YWxsZWRDaHVua3MgPSB7XG4gXHRcdFwiaW5kZXhcIjogMFxuIFx0fTtcblxuIFx0dmFyIGRlZmVycmVkTW9kdWxlcyA9IFtdO1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHR2YXIganNvbnBBcnJheSA9IHdpbmRvd1tcIndlYnBhY2tKc29ucFwiXSA9IHdpbmRvd1tcIndlYnBhY2tKc29ucFwiXSB8fCBbXTtcbiBcdHZhciBvbGRKc29ucEZ1bmN0aW9uID0ganNvbnBBcnJheS5wdXNoLmJpbmQoanNvbnBBcnJheSk7XG4gXHRqc29ucEFycmF5LnB1c2ggPSB3ZWJwYWNrSnNvbnBDYWxsYmFjaztcbiBcdGpzb25wQXJyYXkgPSBqc29ucEFycmF5LnNsaWNlKCk7XG4gXHRmb3IodmFyIGkgPSAwOyBpIDwganNvbnBBcnJheS5sZW5ndGg7IGkrKykgd2VicGFja0pzb25wQ2FsbGJhY2soanNvbnBBcnJheVtpXSk7XG4gXHR2YXIgcGFyZW50SnNvbnBGdW5jdGlvbiA9IG9sZEpzb25wRnVuY3Rpb247XG5cblxuIFx0Ly8gYWRkIGVudHJ5IG1vZHVsZSB0byBkZWZlcnJlZCBsaXN0XG4gXHRkZWZlcnJlZE1vZHVsZXMucHVzaChbMCxcInZlbmRvclwiXSk7XG4gXHQvLyBydW4gZGVmZXJyZWQgbW9kdWxlcyB3aGVuIHJlYWR5XG4gXHRyZXR1cm4gY2hlY2tEZWZlcnJlZE1vZHVsZXMoKTtcbiIsIjx0ZW1wbGF0ZT5cclxuICA8ZGl2IGNsYXNzPVwibW9kYWwgZmFkZSBtb2RhbC1mc1wiIGlkPVwibW9kYWwtZnNcIj5cclxuICAgIDxkaXYgY2xhc3M9XCJtb2RhbC1kaWFsb2dcIiByb2xlPVwiZG9jdW1lbnRcIj5cclxuICAgICAgPGRpdiBjbGFzcz1cIm1vZGFsLWNvbnRlbnRcIj5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwibW9kYWwtYm9keVwiPlxyXG4gICAgICAgICAgPGRpdiBjbGFzcz1cImNvbnRhaW5lciB0ZXh0LWNlbnRlclwiPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwicm93IGZ1bGwtaGVpZ2h0IGFsaWduLWl0ZW1zLWNlbnRlclwiPlxyXG4gICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtNSBtLWgtYXV0b1wiPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInAtaC0zMCBwLWItNTBcIj5cclxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInRleHQtY2VudGVyIGZvbnQtc2l6ZS03MFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzPVwibWRpIG1kaS1jaGVja2JveC1tYXJrZWQtY2lyY2xlLW91dGxpbmUgaWNvbi1ncmFkaWVudC1zdWNjZXNzXCI+PC9pPlxyXG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgPGgxIGNsYXNzPVwidGV4dC10aGluIG0tdi0xNVwiPnt7dGl0bGV9fTwvaDE+XHJcbiAgICAgICAgICAgICAgICAgIDxwIGNsYXNzPVwibS1iLTI1IGxlYWRcIj57e2NvbnRlbnR9fTwvcD5cclxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInRleHQtY2VudGVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGJ1dHRvblxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjbGFzcz1cImJ0biBidG4tZGVmYXVsdCBidG4tbGdcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBAY2xpY2sucHJldmVudD1cImNvbmZpcm1cIlxyXG4gICAgICAgICAgICAgICAgICAgID5Db25maXJtXHJcbiAgICAgICAgICAgICAgICAgICAgPC9idXR0b24+XHJcbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICA8YVxyXG4gICAgICAgICAgICAgIGNsYXNzPVwibW9kYWwtY2xvc2VcIlxyXG4gICAgICAgICAgICAgIGhyZWY9XCIjXCJcclxuICAgICAgICAgICAgICBkYXRhLWRpc21pc3M9XCJtb2RhbFwiXHJcbiAgICAgICAgICAgICAgQGNsaWNrLnByZXZlbnQ9XCJjb25maXJtXCJcclxuICAgICAgICAgID5cclxuICAgICAgICAgICAgPGkgY2xhc3M9XCJ0aS1jbG9zZVwiPjwvaT5cclxuICAgICAgICAgIDwvYT5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgPC9kaXY+XHJcbiAgICA8L2Rpdj5cclxuICA8L2Rpdj5cclxuPC90ZW1wbGF0ZT5cclxuXHJcbjxzY3JpcHQ+XHJcbiAgaW1wb3J0IHsgRVZFTlRfQlVTX0tFWSB9IGZyb20gJyRqc0Jhc2VQYXRoL2RlZmluZSc7XHJcbiAgaW1wb3J0IEV2ZW50QnVzIGZyb20gJyRqc0Jhc2VQYXRoL0V2ZW50QnVzJztcclxuXHJcbiAgZXhwb3J0IGRlZmF1bHQge1xyXG4gICAgbmFtZTogJ21vZGFsJyxcclxuICAgIGNvbXBvbmVudHM6IHt9LFxyXG4gICAgZGF0YSgpIHtcclxuICAgICAgcmV0dXJuIHtcclxuICAgICAgICB0aXRsZTogJ1RIw5RORyBCw4FPICEnLFxyXG4gICAgICAgIGNvbnRlbnQ6ICdXZSBhcmUgbHVja3kgdG8gbGl2ZSBpbiBhIGdsb3Jpb3VzIGFnZSB0aGF0IGdpdmVzIHVzIGV2ZXJ5dGhpbmcgYXMgYSBodW1hbiByYWNlLidcclxuICAgICAgfVxyXG4gICAgfSxcclxuICAgIGNvbXB1dGVkOiB7fSxcclxuICAgIGNyZWF0ZWQoKSB7XHJcbiAgICAgIGxldCBzZWxmID0gdGhpc1xyXG4gICAgICBFdmVudEJ1cy5vbihFVkVOVF9CVVNfS0VZLlVQREFURV9DT05URU5UX01PREFMLCAoY29udGVudCkgPT4ge1xyXG4gICAgICAgIHNlbGYuY29udGVudCA9IGNvbnRlbnRcclxuICAgICAgfSlcclxuXHJcbiAgICAgICQoZG9jdW1lbnQpLm9uKCdrZXl1cCcsIChlKSA9PiB7XHJcbiAgICAgICAgbGV0IGtleUNvZGUgPSBlLndoaWNoXHJcblxyXG4gICAgICAgIGlmIChrZXlDb2RlICE9IDEzICYmIGtleUNvZGUgIT0gMjcpXHJcbiAgICAgICAgICByZXR1cm5cclxuXHJcbiAgICAgICAgbGV0IGZsYWcgPSAkKCcjbW9kYWwtZnMnKS5oYXNDbGFzcygnc2hvdycpXHJcbiAgICAgICAgaWYgKCFmbGFnKVxyXG4gICAgICAgICAgcmV0dXJuXHJcblxyXG4gICAgICAgICQoJyNtb2RhbC1mcy1idG4nKS50cmlnZ2VyKCdjbGljaycpXHJcbiAgICAgICAgRXZlbnRCdXMuZW1pdChFVkVOVF9CVVNfS0VZLkNPTkZJUk1fTU9EQUwsIHRydWUpXHJcbiAgICAgIH0pXHJcbiAgICB9LFxyXG4gICAgbWV0aG9kczoge1xyXG4gICAgICBjb25maXJtKCkge1xyXG4gICAgICAgICQoJyNtb2RhbC1mcy1idG4nKS50cmlnZ2VyKCdjbGljaycpXHJcbiAgICAgICAgRXZlbnRCdXMuZW1pdChFVkVOVF9CVVNfS0VZLkNPTkZJUk1fTU9EQUwsIHRydWUpXHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbjwvc2NyaXB0PlxyXG5cclxuIiwiPHRlbXBsYXRlPlxyXG4gIDxkaXYgaWQ9XCJ1c2VyLXRhYmxlXCIgY2xhc3M9XCJ0YWJsZS1vdmVyZmxvd1wiPlxyXG4gICAgPHRhYmxlIGlkPVwiZHQtb3B0XCIgY2xhc3M9XCJ0YWJsZSB0YWJsZS1ob3ZlciB0YWJsZS14bFwiPlxyXG4gICAgICA8dGhlYWQ+XHJcbiAgICAgICAgPHRyPlxyXG4gICAgICAgICAgPHRoPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY2hlY2tib3ggcC0wXCI+XHJcbiAgICAgICAgICAgICAgPGlucHV0IGlkPVwic2VsZWN0YWJsZTFcIiB0eXBlPVwiY2hlY2tib3hcIiBjbGFzcz1cImNoZWNrQWxsXCIgbmFtZT1cImNoZWNrQWxsXCI+XHJcbiAgICAgICAgICAgICAgPGxhYmVsIGZvcj1cInNlbGVjdGFibGUxXCI+PC9sYWJlbD5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICA8L3RoPlxyXG4gICAgICAgICAgPHRoPkZVTExOQU1FPC90aD5cclxuICAgICAgICAgIDx0aD5FTUFJTDwvdGg+XHJcbiAgICAgICAgICA8dGg+U1RBVFVTPC90aD5cclxuICAgICAgICAgIDx0aD4jPC90aD5cclxuICAgICAgICAgIDx0aD48L3RoPlxyXG4gICAgICAgIDwvdHI+XHJcbiAgICAgIDwvdGhlYWQ+XHJcbiAgICAgIDx0Ym9keT5cclxuICAgICAgICA8dGVtcGxhdGUgdi1mb3I9XCIob2JqRW50aXR5LCBpbmRleCkgaW4gYXJyRW50aXR5TGlzdFwiPlxyXG4gICAgICAgICAgPHVzZXItdGFibGUtcm93XHJcbiAgICAgICAgICAgICAgOmluZGV4PVwiaW5kZXhcIlxyXG4gICAgICAgICAgICAgIDpvYmotZW50aXR5PVwib2JqRW50aXR5XCJcclxuICAgICAgICAgID48L3VzZXItdGFibGUtcm93PlxyXG4gICAgICAgIDwvdGVtcGxhdGU+XHJcbiAgICAgIDwvdGJvZHk+XHJcbiAgICA8L3RhYmxlPlxyXG4gIDwvZGl2PlxyXG48L3RlbXBsYXRlPlxyXG5cclxuPHNjcmlwdD5cclxuICBpbXBvcnQgVXNlclRhYmxlUm93IGZyb20gJyR2dWVQYXJ0aWFsUGF0aC91c2VyL1VzZXJUYWJsZVJvdydcclxuXHJcbiAgZXhwb3J0IGRlZmF1bHQge1xyXG4gICAgbmFtZTogJ3VzZXItdGFibGUnLFxyXG4gICAgY29tcG9uZW50czoge1xyXG4gICAgICAndXNlci10YWJsZS1yb3cnOiBVc2VyVGFibGVSb3dcclxuICAgIH0sXHJcbiAgICBkYXRhKCkge1xyXG4gICAgICByZXR1cm4ge1xyXG4gICAgICB9XHJcbiAgICB9LFxyXG4gICAgY29tcHV0ZWQ6IHtcclxuICAgICAgYXJyRW50aXR5TGlzdCgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy4kc3RvcmUuc3RhdGUuYXJyRW50aXR5TGlzdFxyXG4gICAgICB9XHJcbiAgICB9LFxyXG4gICAgY3JlYXRlZCgpIHtcclxuXHJcbiAgICB9LFxyXG4gICAgbWV0aG9kczoge31cclxuICB9XHJcbjwvc2NyaXB0PlxyXG5cclxuIiwiPHRlbXBsYXRlPlxyXG4gIDx0cj5cclxuICAgIDx0ZD5cclxuICAgICAgPGRpdiBjbGFzcz1cImNoZWNrYm94XCI+XHJcbiAgICAgICAgPGlucHV0IGlkPVwic2VsZWN0YWJsZTJcIiB0eXBlPVwiY2hlY2tib3hcIj5cclxuICAgICAgICA8bGFiZWwgZm9yPVwic2VsZWN0YWJsZTJcIj48L2xhYmVsPlxyXG4gICAgICA8L2Rpdj5cclxuICAgIDwvdGQ+XHJcbiAgICA8dGQ+XHJcbiAgICAgIDxkaXYgY2xhc3M9XCJsaXN0LW1lZGlhXCI+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cImxpc3QtaXRlbVwiPlxyXG4gICAgICAgICAgPGRpdiBjbGFzcz1cIm1lZGlhLWltZ1wiPlxyXG4gICAgICAgICAgICA8aW1nXHJcbiAgICAgICAgICAgICAgICA6c3JjPVwicHJvZmlsZUltYWdlXCJcclxuICAgICAgICAgICAgICAgIGFsdD1cIlwiXHJcbiAgICAgICAgICAgID5cclxuICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgPGRpdiBjbGFzcz1cImluZm9cIj5cclxuICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJ0aXRsZVwiPnt7b2JqRW50aXR5LmZ1bGxuYW1lfX08L3NwYW4+XHJcbiAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwic3ViLXRpdGxlXCI+SUQge3tvYmpFbnRpdHkudXNlcl9pZH19PC9zcGFuPlxyXG4gICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgPC90ZD5cclxuICAgIDx0ZD57e29iakVudGl0eS5lbWFpbH19PC90ZD5cclxuICAgIDx0ZD5cclxuICAgICAgPHRlbXBsYXRlIHYtaWY9XCJvYmpFbnRpdHkuaXNfYWN0aXZlZCA9PSAxXCI+XHJcbiAgICAgICAgPHNwYW4gY2xhc3M9XCJiYWRnZSBiYWRnZS1waWxsIGJhZGdlLWdyYWRpZW50LXN1Y2Nlc3NcIj7EkMOjIGvDrWNoIGhv4bqhdDwvc3Bhbj5cclxuICAgICAgPC90ZW1wbGF0ZT5cclxuICAgICAgPHRlbXBsYXRlIHYtZWxzZT5cclxuICAgICAgICA8c3BhbiBjbGFzcz1cImJhZGdlIGJhZGdlLXBpbGwgYmFkZ2UtZ3JhZGllbnQtZGFuZ2VyXCI+Q2jGsGEga8OtY2ggaG/huqF0PC9zcGFuPlxyXG4gICAgICA8L3RlbXBsYXRlPlxyXG4gICAgPC90ZD5cclxuICAgIDx0ZCBjbGFzcz1cInRleHQtY2VudGVyIGZvbnQtc2l6ZS0xOFwiPlxyXG4gICAgICA8YVxyXG4gICAgICAgICAgY2xhc3M9XCJ0ZXh0LWdyYXkgbS1yLTE1XCJcclxuICAgICAgICAgIEBjbGljaz1cImdvVXBkYXRlKClcIlxyXG4gICAgICA+PGkgY2xhc3M9XCJ0aS1wZW5jaWxcIj48L2k+XHJcbiAgICAgIDwvYT5cclxuICAgICAgPGFcclxuICAgICAgICAgIGNsYXNzPVwidGV4dC1ncmF5XCJcclxuICAgICAgICAgIEBjbGljaz1cInJlbW92ZShvYmpFbnRpdHkudXNlcl9pZClcIlxyXG4gICAgICA+PGlcclxuICAgICAgICAgIDpjbGFzcz1cIntcclxuICAgICAgICAgICdmYSBmYS1zcGlubmVyIGZhLXB1bHNlIGZhLWZ3JzogZmxhZy5pc0RlbGV0aW5nLFxyXG4gICAgICAgICAgJ3RpLXRyYXNoJzogIWZsYWcuaXNEZWxldGluZ1xyXG4gICAgICAgICAgfVwiPlxyXG4gICAgICA8L2k+PC9hPlxyXG4gICAgPC90ZD5cclxuICA8L3RyPlxyXG48L3RlbXBsYXRlPlxyXG5cclxuPHNjcmlwdD5cclxuICBpbXBvcnQgeyBfaXNFbXB0eVN0cmluZyB9IGZyb20gXCIkanNDb21tb25QYXRoL3V0aWwvU2FmZVV0aWxcIlxyXG5cclxuICBleHBvcnQgZGVmYXVsdCB7XHJcbiAgICBuYW1lOiAndXNlci10YWJsZS1yb3cnLFxyXG4gICAgcHJvcHM6IHtcclxuICAgICAgaW5kZXg6IHtcclxuICAgICAgICBkZWZhdWx0OiB7fSxcclxuICAgICAgICB0eXBlOiBOdW1iZXJcclxuICAgICAgfSxcclxuICAgICAgb2JqRW50aXR5OiB7XHJcbiAgICAgICAgZGVmYXVsdDoge30sXHJcbiAgICAgICAgdHlwZTogT2JqZWN0XHJcbiAgICAgIH0sXHJcbiAgICB9LFxyXG4gICAgY29tcG9uZW50czoge30sXHJcbiAgICBkYXRhKCkge1xyXG4gICAgICByZXR1cm4ge1xyXG4gICAgICAgIHVybDoge1xyXG4gICAgICAgICAgcmVzb3VyY2VVUkw6IENEQVRBLnJlc291cmNlVVJMXHJcbiAgICAgICAgfSxcclxuICAgICAgICBmbGFnOiB7XHJcbiAgICAgICAgICBpc0RlbGV0aW5nOiBmYWxzZVxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfSxcclxuICAgIGNvbXB1dGVkOiB7XHJcbiAgICAgIHByb2ZpbGVJbWFnZSgpIHtcclxuICAgICAgICBpZiAoX2lzRW1wdHlTdHJpbmcodGhpcy5vYmpFbnRpdHkucHJvZmlsZV9pbWFnZSkpXHJcbiAgICAgICAgICByZXR1cm4gdGhpcy51cmwucmVzb3VyY2VVUkwgKyAnL2dsb2JhbC9pbWFnZS9ub3RGb3VuZC5wbmcnXHJcblxyXG4gICAgICAgIHJldHVybiB0aGlzLm9iakVudGl0eS5wcm9maWxlX2ltYWdlXHJcbiAgICAgIH0sXHJcbiAgICB9LFxyXG4gICAgY3JlYXRlZCgpIHtcclxuICAgIH0sXHJcbiAgICBtZXRob2RzOiB7XHJcbiAgICAgIGdvVXBkYXRlKCkge1xyXG4gICAgICAgIHRoaXMuJHN0b3JlLmRpc3BhdGNoKCdzZXRFbnRpdHlBY3QnLCB7IGluZGV4OiB0aGlzLmluZGV4IH0pXHJcblxyXG4gICAgICAgIHRoaXMuJHJvdXRlci5wdXNoKHsgbmFtZTogJ3VzZXI6ZWRpdCcsIHBhcmFtczogeyBpZDogdGhpcy5vYmpFbnRpdHkudXNlcl9pZCwgcGFnZTogMSB9IH0pXHJcbiAgICAgIH0sXHJcbiAgICAgIHJlbW92ZShpZCkge1xyXG4gICAgICAgIGlmICghaWQpXHJcbiAgICAgICAgICByZXR1cm5cclxuXHJcbiAgICAgICAgdGhpcy5mbGFnLmlzRGVsZXRpbmcgPSB0cnVlXHJcbiAgICAgICAgdGhpcy4kc3RvcmUuZGlzcGF0Y2goJ2RlbGV0ZUVudGl0eUFjdCcsIHsgaWQ6IGlkIH0pLnRoZW4ocmVzID0+IHtcclxuICAgICAgICAgIHRoaXMuZmxhZy5pc0RlbGV0aW5nID0gZmFsc2VcclxuICAgICAgICB9KVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG48L3NjcmlwdD5cclxuXHJcbiIsInZhciByZW5kZXIgPSBmdW5jdGlvbigpIHtcbiAgdmFyIF92bSA9IHRoaXNcbiAgdmFyIF9oID0gX3ZtLiRjcmVhdGVFbGVtZW50XG4gIHZhciBfYyA9IF92bS5fc2VsZi5fYyB8fCBfaFxuICByZXR1cm4gX2MoXG4gICAgXCJkaXZcIixcbiAgICB7IHN0YXRpY0NsYXNzOiBcIm1vZGFsIGZhZGUgbW9kYWwtZnNcIiwgYXR0cnM6IHsgaWQ6IFwibW9kYWwtZnNcIiB9IH0sXG4gICAgW1xuICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJtb2RhbC1kaWFsb2dcIiwgYXR0cnM6IHsgcm9sZTogXCJkb2N1bWVudFwiIH0gfSwgW1xuICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcIm1vZGFsLWNvbnRlbnRcIiB9LCBbXG4gICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJtb2RhbC1ib2R5XCIgfSwgW1xuICAgICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjb250YWluZXIgdGV4dC1jZW50ZXJcIiB9LCBbXG4gICAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwicm93IGZ1bGwtaGVpZ2h0IGFsaWduLWl0ZW1zLWNlbnRlclwiIH0sIFtcbiAgICAgICAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImNvbC1tZC01IG0taC1hdXRvXCIgfSwgW1xuICAgICAgICAgICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJwLWgtMzAgcC1iLTUwXCIgfSwgW1xuICAgICAgICAgICAgICAgICAgICBfdm0uX20oMCksXG4gICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgIF9jKFwiaDFcIiwgeyBzdGF0aWNDbGFzczogXCJ0ZXh0LXRoaW4gbS12LTE1XCIgfSwgW1xuICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihfdm0uX3MoX3ZtLnRpdGxlKSlcbiAgICAgICAgICAgICAgICAgICAgXSksXG4gICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgIF9jKFwicFwiLCB7IHN0YXRpY0NsYXNzOiBcIm0tYi0yNSBsZWFkXCIgfSwgW1xuICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihfdm0uX3MoX3ZtLmNvbnRlbnQpKVxuICAgICAgICAgICAgICAgICAgICBdKSxcbiAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJ0ZXh0LWNlbnRlclwiIH0sIFtcbiAgICAgICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiYnV0dG9uXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tZGVmYXVsdCBidG4tbGdcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgb246IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGljazogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkZXZlbnQucHJldmVudERlZmF1bHQoKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIF92bS5jb25maXJtKCRldmVudClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICBbX3ZtLl92KFwiQ29uZmlybVxcbiAgICAgICAgICAgICAgICAgIFwiKV1cbiAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICBdKSxcbiAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgXCJhXCIsXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJtb2RhbC1jbG9zZVwiLFxuICAgICAgICAgICAgICAgIGF0dHJzOiB7IGhyZWY6IFwiI1wiLCBcImRhdGEtZGlzbWlzc1wiOiBcIm1vZGFsXCIgfSxcbiAgICAgICAgICAgICAgICBvbjoge1xuICAgICAgICAgICAgICAgICAgY2xpY2s6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgICAgICAgICAgICAkZXZlbnQucHJldmVudERlZmF1bHQoKVxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gX3ZtLmNvbmZpcm0oJGV2ZW50KVxuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgW19jKFwiaVwiLCB7IHN0YXRpY0NsYXNzOiBcInRpLWNsb3NlXCIgfSldXG4gICAgICAgICAgICApXG4gICAgICAgICAgXSlcbiAgICAgICAgXSlcbiAgICAgIF0pXG4gICAgXVxuICApXG59XG52YXIgc3RhdGljUmVuZGVyRm5zID0gW1xuICBmdW5jdGlvbigpIHtcbiAgICB2YXIgX3ZtID0gdGhpc1xuICAgIHZhciBfaCA9IF92bS4kY3JlYXRlRWxlbWVudFxuICAgIHZhciBfYyA9IF92bS5fc2VsZi5fYyB8fCBfaFxuICAgIHJldHVybiBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcInRleHQtY2VudGVyIGZvbnQtc2l6ZS03MFwiIH0sIFtcbiAgICAgIF9jKFwiaVwiLCB7XG4gICAgICAgIHN0YXRpY0NsYXNzOlxuICAgICAgICAgIFwibWRpIG1kaS1jaGVja2JveC1tYXJrZWQtY2lyY2xlLW91dGxpbmUgaWNvbi1ncmFkaWVudC1zdWNjZXNzXCJcbiAgICAgIH0pXG4gICAgXSlcbiAgfVxuXVxucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5cbmV4cG9ydCB7IHJlbmRlciwgc3RhdGljUmVuZGVyRm5zIH0iLCJ2YXIgcmVuZGVyID0gZnVuY3Rpb24oKSB7XG4gIHZhciBfdm0gPSB0aGlzXG4gIHZhciBfaCA9IF92bS4kY3JlYXRlRWxlbWVudFxuICB2YXIgX2MgPSBfdm0uX3NlbGYuX2MgfHwgX2hcbiAgcmV0dXJuIF9jKFxuICAgIFwiZGl2XCIsXG4gICAgeyBzdGF0aWNDbGFzczogXCJ0YWJsZS1vdmVyZmxvd1wiLCBhdHRyczogeyBpZDogXCJ1c2VyLXRhYmxlXCIgfSB9LFxuICAgIFtcbiAgICAgIF9jKFxuICAgICAgICBcInRhYmxlXCIsXG4gICAgICAgIHsgc3RhdGljQ2xhc3M6IFwidGFibGUgdGFibGUtaG92ZXIgdGFibGUteGxcIiwgYXR0cnM6IHsgaWQ6IFwiZHQtb3B0XCIgfSB9LFxuICAgICAgICBbXG4gICAgICAgICAgX3ZtLl9tKDApLFxuICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgX2MoXG4gICAgICAgICAgICBcInRib2R5XCIsXG4gICAgICAgICAgICBbXG4gICAgICAgICAgICAgIF92bS5fbChfdm0uYXJyRW50aXR5TGlzdCwgZnVuY3Rpb24ob2JqRW50aXR5LCBpbmRleCkge1xuICAgICAgICAgICAgICAgIHJldHVybiBbXG4gICAgICAgICAgICAgICAgICBfYyhcInVzZXItdGFibGUtcm93XCIsIHtcbiAgICAgICAgICAgICAgICAgICAgYXR0cnM6IHsgaW5kZXg6IGluZGV4LCBcIm9iai1lbnRpdHlcIjogb2JqRW50aXR5IH1cbiAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIDJcbiAgICAgICAgICApXG4gICAgICAgIF1cbiAgICAgIClcbiAgICBdXG4gIClcbn1cbnZhciBzdGF0aWNSZW5kZXJGbnMgPSBbXG4gIGZ1bmN0aW9uKCkge1xuICAgIHZhciBfdm0gPSB0aGlzXG4gICAgdmFyIF9oID0gX3ZtLiRjcmVhdGVFbGVtZW50XG4gICAgdmFyIF9jID0gX3ZtLl9zZWxmLl9jIHx8IF9oXG4gICAgcmV0dXJuIF9jKFwidGhlYWRcIiwgW1xuICAgICAgX2MoXCJ0clwiLCBbXG4gICAgICAgIF9jKFwidGhcIiwgW1xuICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY2hlY2tib3ggcC0wXCIgfSwgW1xuICAgICAgICAgICAgX2MoXCJpbnB1dFwiLCB7XG4gICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImNoZWNrQWxsXCIsXG4gICAgICAgICAgICAgIGF0dHJzOiB7IGlkOiBcInNlbGVjdGFibGUxXCIsIHR5cGU6IFwiY2hlY2tib3hcIiwgbmFtZTogXCJjaGVja0FsbFwiIH1cbiAgICAgICAgICAgIH0pLFxuICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgIF9jKFwibGFiZWxcIiwgeyBhdHRyczogeyBmb3I6IFwic2VsZWN0YWJsZTFcIiB9IH0pXG4gICAgICAgICAgXSlcbiAgICAgICAgXSksXG4gICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgIF9jKFwidGhcIiwgW192bS5fdihcIkZVTExOQU1FXCIpXSksXG4gICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgIF9jKFwidGhcIiwgW192bS5fdihcIkVNQUlMXCIpXSksXG4gICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgIF9jKFwidGhcIiwgW192bS5fdihcIlNUQVRVU1wiKV0pLFxuICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICBfYyhcInRoXCIsIFtfdm0uX3YoXCIjXCIpXSksXG4gICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgIF9jKFwidGhcIilcbiAgICAgIF0pXG4gICAgXSlcbiAgfVxuXVxucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5cbmV4cG9ydCB7IHJlbmRlciwgc3RhdGljUmVuZGVyRm5zIH0iLCJ2YXIgcmVuZGVyID0gZnVuY3Rpb24oKSB7XG4gIHZhciBfdm0gPSB0aGlzXG4gIHZhciBfaCA9IF92bS4kY3JlYXRlRWxlbWVudFxuICB2YXIgX2MgPSBfdm0uX3NlbGYuX2MgfHwgX2hcbiAgcmV0dXJuIF9jKFwidHJcIiwgW1xuICAgIF92bS5fbSgwKSxcbiAgICBfdm0uX3YoXCIgXCIpLFxuICAgIF9jKFwidGRcIiwgW1xuICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJsaXN0LW1lZGlhXCIgfSwgW1xuICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImxpc3QtaXRlbVwiIH0sIFtcbiAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcIm1lZGlhLWltZ1wiIH0sIFtcbiAgICAgICAgICAgIF9jKFwiaW1nXCIsIHsgYXR0cnM6IHsgc3JjOiBfdm0ucHJvZmlsZUltYWdlLCBhbHQ6IFwiXCIgfSB9KVxuICAgICAgICAgIF0pLFxuICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJpbmZvXCIgfSwgW1xuICAgICAgICAgICAgX2MoXCJzcGFuXCIsIHsgc3RhdGljQ2xhc3M6IFwidGl0bGVcIiB9LCBbXG4gICAgICAgICAgICAgIF92bS5fdihfdm0uX3MoX3ZtLm9iakVudGl0eS5mdWxsbmFtZSkpXG4gICAgICAgICAgICBdKSxcbiAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICBfYyhcInNwYW5cIiwgeyBzdGF0aWNDbGFzczogXCJzdWItdGl0bGVcIiB9LCBbXG4gICAgICAgICAgICAgIF92bS5fdihcIklEIFwiICsgX3ZtLl9zKF92bS5vYmpFbnRpdHkudXNlcl9pZCkpXG4gICAgICAgICAgICBdKVxuICAgICAgICAgIF0pXG4gICAgICAgIF0pXG4gICAgICBdKVxuICAgIF0pLFxuICAgIF92bS5fdihcIiBcIiksXG4gICAgX2MoXCJ0ZFwiLCBbX3ZtLl92KF92bS5fcyhfdm0ub2JqRW50aXR5LmVtYWlsKSldKSxcbiAgICBfdm0uX3YoXCIgXCIpLFxuICAgIF9jKFxuICAgICAgXCJ0ZFwiLFxuICAgICAgW1xuICAgICAgICBfdm0ub2JqRW50aXR5LmlzX2FjdGl2ZWQgPT0gMVxuICAgICAgICAgID8gW1xuICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICBcInNwYW5cIixcbiAgICAgICAgICAgICAgICB7IHN0YXRpY0NsYXNzOiBcImJhZGdlIGJhZGdlLXBpbGwgYmFkZ2UtZ3JhZGllbnQtc3VjY2Vzc1wiIH0sXG4gICAgICAgICAgICAgICAgW192bS5fdihcIsSQw6Mga8OtY2ggaG/huqF0XCIpXVxuICAgICAgICAgICAgICApXG4gICAgICAgICAgICBdXG4gICAgICAgICAgOiBbXG4gICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgIFwic3BhblwiLFxuICAgICAgICAgICAgICAgIHsgc3RhdGljQ2xhc3M6IFwiYmFkZ2UgYmFkZ2UtcGlsbCBiYWRnZS1ncmFkaWVudC1kYW5nZXJcIiB9LFxuICAgICAgICAgICAgICAgIFtfdm0uX3YoXCJDaMawYSBrw61jaCBob+G6oXRcIildXG4gICAgICAgICAgICAgIClcbiAgICAgICAgICAgIF1cbiAgICAgIF0sXG4gICAgICAyXG4gICAgKSxcbiAgICBfdm0uX3YoXCIgXCIpLFxuICAgIF9jKFwidGRcIiwgeyBzdGF0aWNDbGFzczogXCJ0ZXh0LWNlbnRlciBmb250LXNpemUtMThcIiB9LCBbXG4gICAgICBfYyhcbiAgICAgICAgXCJhXCIsXG4gICAgICAgIHtcbiAgICAgICAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LWdyYXkgbS1yLTE1XCIsXG4gICAgICAgICAgb246IHtcbiAgICAgICAgICAgIGNsaWNrOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIF92bS5nb1VwZGF0ZSgpXG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBbX2MoXCJpXCIsIHsgc3RhdGljQ2xhc3M6IFwidGktcGVuY2lsXCIgfSldXG4gICAgICApLFxuICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgIF9jKFxuICAgICAgICBcImFcIixcbiAgICAgICAge1xuICAgICAgICAgIHN0YXRpY0NsYXNzOiBcInRleHQtZ3JheVwiLFxuICAgICAgICAgIG9uOiB7XG4gICAgICAgICAgICBjbGljazogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgICAgIHJldHVybiBfdm0ucmVtb3ZlKF92bS5vYmpFbnRpdHkudXNlcl9pZClcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIFtcbiAgICAgICAgICBfYyhcImlcIiwge1xuICAgICAgICAgICAgY2xhc3M6IHtcbiAgICAgICAgICAgICAgXCJmYSBmYS1zcGlubmVyIGZhLXB1bHNlIGZhLWZ3XCI6IF92bS5mbGFnLmlzRGVsZXRpbmcsXG4gICAgICAgICAgICAgIFwidGktdHJhc2hcIjogIV92bS5mbGFnLmlzRGVsZXRpbmdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9KVxuICAgICAgICBdXG4gICAgICApXG4gICAgXSlcbiAgXSlcbn1cbnZhciBzdGF0aWNSZW5kZXJGbnMgPSBbXG4gIGZ1bmN0aW9uKCkge1xuICAgIHZhciBfdm0gPSB0aGlzXG4gICAgdmFyIF9oID0gX3ZtLiRjcmVhdGVFbGVtZW50XG4gICAgdmFyIF9jID0gX3ZtLl9zZWxmLl9jIHx8IF9oXG4gICAgcmV0dXJuIF9jKFwidGRcIiwgW1xuICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjaGVja2JveFwiIH0sIFtcbiAgICAgICAgX2MoXCJpbnB1dFwiLCB7IGF0dHJzOiB7IGlkOiBcInNlbGVjdGFibGUyXCIsIHR5cGU6IFwiY2hlY2tib3hcIiB9IH0pLFxuICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICBfYyhcImxhYmVsXCIsIHsgYXR0cnM6IHsgZm9yOiBcInNlbGVjdGFibGUyXCIgfSB9KVxuICAgICAgXSlcbiAgICBdKVxuICB9XG5dXG5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcblxuZXhwb3J0IHsgcmVuZGVyLCBzdGF0aWNSZW5kZXJGbnMgfSIsInZhciBtYXAgPSB7XG5cdFwiLi9sb2dcIjogXCIuL25vZGVfbW9kdWxlcy93ZWJwYWNrL2hvdC9sb2cuanNcIlxufTtcblxuXG5mdW5jdGlvbiB3ZWJwYWNrQ29udGV4dChyZXEpIHtcblx0dmFyIGlkID0gd2VicGFja0NvbnRleHRSZXNvbHZlKHJlcSk7XG5cdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKGlkKTtcbn1cbmZ1bmN0aW9uIHdlYnBhY2tDb250ZXh0UmVzb2x2ZShyZXEpIHtcblx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhtYXAsIHJlcSkpIHtcblx0XHR2YXIgZSA9IG5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIgKyByZXEgKyBcIidcIik7XG5cdFx0ZS5jb2RlID0gJ01PRFVMRV9OT1RfRk9VTkQnO1xuXHRcdHRocm93IGU7XG5cdH1cblx0cmV0dXJuIG1hcFtyZXFdO1xufVxud2VicGFja0NvbnRleHQua2V5cyA9IGZ1bmN0aW9uIHdlYnBhY2tDb250ZXh0S2V5cygpIHtcblx0cmV0dXJuIE9iamVjdC5rZXlzKG1hcCk7XG59O1xud2VicGFja0NvbnRleHQucmVzb2x2ZSA9IHdlYnBhY2tDb250ZXh0UmVzb2x2ZTtcbm1vZHVsZS5leHBvcnRzID0gd2VicGFja0NvbnRleHQ7XG53ZWJwYWNrQ29udGV4dC5pZCA9IFwiLi9ub2RlX21vZHVsZXMvd2VicGFjay9ob3Qgc3luYyBeXFxcXC5cXFxcL2xvZyRcIjsiLCJpbXBvcnQgJCBmcm9tIFwianF1ZXJ5XCJcclxuaW1wb3J0IFZ1ZSBmcm9tICd2dWUnXHJcbmltcG9ydCBWdWV4IGZyb20gJ3Z1ZXgnXHJcblxyXG5pbXBvcnQgVXNlclRhYmxlIGZyb20gJyR2dWVQYXJ0aWFsUGF0aC91c2VyL1VzZXJUYWJsZSdcclxuXHJcbmltcG9ydCBTdG9yZSBmcm9tICckanNTdG9yZVBhdGgvdXNlcidcclxuXHJcblZ1ZS51c2UoVnVleClcclxuXHJcbmNvbnN0IHN0b3JlID0gbmV3IFZ1ZXguU3RvcmUoU3RvcmUpXHJcblxyXG5jb25zdCBPYmplY3QgPSAoKCkgPT4ge1xyXG4gIGNvbnN0IF9yZW5kZXJVc2VyVGFibGUgPSAoKSA9PiB7XHJcbiAgICBsZXQgJGVsID0gJCgnI3VzZXItdGFibGUnKVxyXG5cclxuICAgIG5ldyBWdWUoe1xyXG4gICAgICBzdG9yZSxcclxuICAgICAgcmVuZGVyOiBoID0+IGgoVXNlclRhYmxlKVxyXG4gICAgfSkuJG1vdW50KCRlbFsgMCBdKVxyXG4gIH1cclxuXHJcbiAgcmV0dXJuIHtcclxuICAgIGluaXQoKSB7XHJcbiAgICAgIF9yZW5kZXJVc2VyVGFibGUoKVxyXG4gICAgfVxyXG4gIH1cclxufSkoKVxyXG5cclxuZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcIkRPTUNvbnRlbnRMb2FkZWRcIiwgZnVuY3Rpb24gKCkge1xyXG4gIE9iamVjdC5pbml0KClcclxufSk7IiwiaW1wb3J0IHsgZm9ySW4gYXMgX2ZvckluLCBpc0VtcHR5IGFzIF9pc0VtcHR5IH0gZnJvbSAnbG9kYXNoJztcclxuXHJcbmltcG9ydCAqIGFzIEFQSSBmcm9tICckanNDb21tb25QYXRoL2FwaSdcclxuaW1wb3J0IHsgaXNFcnJvckFQSSwgdHJpZ2dlck1vZGFsIH0gZnJvbSBcIiRqc0NvbW1vblBhdGgvdXRpbC9HZW5lcmFsVXRpbFwiO1xyXG5pbXBvcnQgeyBfaW50IH0gZnJvbSBcIiRqc0NvbW1vblBhdGgvdXRpbC9TYWZlVXRpbFwiO1xyXG5cclxubGV0IF9vYmpFbnRpdHkgPSB7XHJcbiAgdXNlcl9pZDogbnVsbCxcclxuICBwcm9maWxlX2ltYWdlOiBudWxsLFxyXG4gIGZ1bGxuYW1lOiBudWxsLFxyXG4gIGVtYWlsOiBudWxsLFxyXG4gIHBob25lOiBudWxsLFxyXG4gIGlzX2FjdGl2ZWQ6IDAsXHJcbn1cclxuXHJcbmNsYXNzIFN0b3JlIHtcclxuICBjb25zdHJ1Y3RvcigpIHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgIHN0YXRlOiB7XHJcbiAgICAgICAgZmxhZzoge1xyXG4gICAgICAgICAgaXNHZXQ6IGZhbHNlXHJcbiAgICAgICAgfSxcclxuICAgICAgICBhcnJFbnRpdHlMaXN0OiBbXSxcclxuICAgICAgICBvYmpFbnRpdHlTZWxlY3Q6IHsgLi4uX29iakVudGl0eSB9LFxyXG4gICAgICB9LFxyXG4gICAgICBtdXRhdGlvbnM6IHtcclxuICAgICAgICByZXNldEVudGl0eShzdGF0ZSkge1xyXG4gICAgICAgICAgc3RhdGUub2JqRW50aXR5U2VsZWN0ID0geyAuLi5fb2JqRW50aXR5IH1cclxuICAgICAgICB9LFxyXG4gICAgICAgIHVwZGF0ZUZsYWcoc3RhdGUsIHBheWxvYWQpIHtcclxuICAgICAgICAgIHN0YXRlLmZsYWdbIHBheWxvYWQuZmxhZyBdID0gcGF5bG9hZC52YWx1ZVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgc2V0RW50aXRpZXMoc3RhdGUsIHBheWxvYWQpIHtcclxuICAgICAgICAgIHN0YXRlLmFyckVudGl0eUxpc3QgPSBwYXlsb2FkLmFyckVudGl0eUxpc3RcclxuICAgICAgICB9LFxyXG4gICAgICAgIHNldEVudGl0eShzdGF0ZSwgcGF5bG9hZCkge1xyXG4gICAgICAgICAgaWYgKF9pc0VtcHR5KHBheWxvYWQub2JqRW50aXR5U2VsZWN0KSlcclxuICAgICAgICAgICAgcmV0dXJuXHJcblxyXG4gICAgICAgICAgbGV0IHsgb2JqRW50aXR5U2VsZWN0IH0gPSB7IC4uLnBheWxvYWQgfVxyXG5cclxuICAgICAgICAgIG9iakVudGl0eVNlbGVjdC5wYXNzd29yZCA9IG51bGxcclxuXHJcbiAgICAgICAgICBzdGF0ZS5vYmpFbnRpdHlTZWxlY3QgPSBvYmpFbnRpdHlTZWxlY3RcclxuICAgICAgICB9LFxyXG4gICAgICAgIHVwZGF0ZUVudGl0eShzdGF0ZSwgcGF5bG9hZCkge1xyXG4gICAgICAgICAgbGV0IHsgdHlwZSwgaW5kZXgsIGFyckVudGl0eSB9ID0geyAuLi5wYXlsb2FkIH1cclxuXHJcbiAgICAgICAgICBzd2l0Y2ggKHR5cGUpIHtcclxuICAgICAgICAgICAgY2FzZSAnY3JlYXRlJzpcclxuICAgICAgICAgICAgICBzdGF0ZS5hcnJFbnRpdHlMaXN0LnB1c2goYXJyRW50aXR5KVxyXG4gICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICd1cGRhdGUnOlxyXG4gICAgICAgICAgICAgIHN0YXRlLmFyckVudGl0eUxpc3RbIGluZGV4IF0gPSBhcnJFbnRpdHlcclxuICAgICAgICAgICAgICBzdGF0ZS5vYmpFbnRpdHlTZWxlY3QgPSBhcnJFbnRpdHlcclxuXHJcbiAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgJ2RlbGV0ZSc6XHJcbiAgICAgICAgICAgICAgc3RhdGUuYXJyRW50aXR5TGlzdC5zcGxpY2UoaW5kZXgsIDEpXHJcbiAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9LFxyXG4gICAgICBhY3Rpb25zOiB7XHJcbiAgICAgICAgZ2V0RW50aXR5QWN0KHsgY29tbWl0IH0sIHBheWxvYWQpIHtcclxuICAgICAgICAgIHJldHVybiBuZXcgUHJvbWlzZShmdW5jdGlvbiAocmVzb2x2ZSwgcmVqZWN0KSB7XHJcbiAgICAgICAgICAgIGxldCByZXFQYXJhbXMgPSB7IC4uLnBheWxvYWQgfVxyXG4gICAgICAgICAgICBBUEkuZ2V0VXNlclJlcShyZXFQYXJhbXMpLmRvbmUoKHJlcykgPT4ge1xyXG4gICAgICAgICAgICAgIGlmIChpc0Vycm9yQVBJKHJlcykpIHtcclxuICAgICAgICAgICAgICAgIHJlamVjdChyZXMucGF5bG9hZClcclxuICAgICAgICAgICAgICAgIHJldHVyblxyXG4gICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgbGV0IHsgYXJyVXNlckRldGFpbCB9ID0geyAuLi5yZXMucGF5bG9hZCB9XHJcblxyXG4gICAgICAgICAgICAgIGNvbW1pdCgnc2V0RW50aXR5JywgeyBvYmpFbnRpdHlTZWxlY3Q6IGFyclVzZXJEZXRhaWwgfSlcclxuXHJcbiAgICAgICAgICAgICAgcmVzb2x2ZShyZXMucGF5bG9hZClcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgIH0pXHJcbiAgICAgICAgfSxcclxuICAgICAgICByZWFkRW50aXR5QWN0KHsgY29tbWl0IH0pIHtcclxuICAgICAgICAgIHJldHVybiBuZXcgUHJvbWlzZShmdW5jdGlvbiAocmVzb2x2ZSwgcmVqZWN0KSB7XHJcbiAgICAgICAgICAgIGxldCByZXFQYXJhbXMgPSB7fVxyXG4gICAgICAgICAgICBBUEkucmVhZFVzZXJSZXEocmVxUGFyYW1zKS5kb25lKChyZXMpID0+IHtcclxuICAgICAgICAgICAgICBpZiAoaXNFcnJvckFQSShyZXMpKSB7XHJcbiAgICAgICAgICAgICAgICByZWplY3QocmVzLnBheWxvYWQpXHJcbiAgICAgICAgICAgICAgICByZXR1cm5cclxuICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgIGxldCB7IGFyclVzZXJMaXN0IH0gPSB7IC4uLnJlcy5wYXlsb2FkIH1cclxuXHJcbiAgICAgICAgICAgICAgY29tbWl0KCdzZXRFbnRpdGllcycsIHsgYXJyRW50aXR5TGlzdDogYXJyVXNlckxpc3QgfSlcclxuXHJcbiAgICAgICAgICAgICAgcmVzb2x2ZShyZXMucGF5bG9hZClcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgIH0pXHJcbiAgICAgICAgfSxcclxuICAgICAgICB1cGxvYWRGaWxlQWN0KHsgY29tbWl0IH0sIHBheWxvYWQpIHtcclxuICAgICAgICAgIHJldHVybiBuZXcgUHJvbWlzZShmdW5jdGlvbiAocmVzb2x2ZSwgcmVqZWN0KSB7XHJcbiAgICAgICAgICAgIGxldCBmb3JtRGF0YSA9IG5ldyBGb3JtRGF0YVxyXG5cclxuICAgICAgICAgICAgZm9ybURhdGEuYXBwZW5kKCdjb250cm9sbGVyJywgcGF5bG9hZC5jb250cm9sbGVyKVxyXG4gICAgICAgICAgICBmb3JtRGF0YS5hcHBlbmQoJ2ZpbGUnLCBwYXlsb2FkLmZpbGUpXHJcblxyXG4gICAgICAgICAgICBBUEkudXBsb2FkUmVxKGZvcm1EYXRhLCB7XHJcbiAgICAgICAgICAgICAgcHJvY2Vzc0RhdGE6IGZhbHNlLFxyXG4gICAgICAgICAgICAgIGNvbnRlbnRUeXBlOiBmYWxzZSxcclxuICAgICAgICAgICAgfSkuZG9uZShyZXMgPT4ge1xyXG4gICAgICAgICAgICAgIGlmIChpc0Vycm9yQVBJKHJlcykpIHtcclxuICAgICAgICAgICAgICAgIHRyaWdnZXJNb2RhbChyZXMubWVzc2FnZSlcclxuXHJcbiAgICAgICAgICAgICAgICByZWplY3QocmVzLnBheWxvYWQpXHJcbiAgICAgICAgICAgICAgICByZXR1cm5cclxuICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgIHJlc29sdmUocmVzLnBheWxvYWQpXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICB9KVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgc2V0RW50aXR5QWN0KHsgY29tbWl0LCBnZXR0ZXJzIH0sIHBheWxvYWQpIHtcclxuICAgICAgICAgIGlmICghcGF5bG9hZC5oYXNPd25Qcm9wZXJ0eSgnaW5kZXgnKSlcclxuICAgICAgICAgICAgcmV0dXJuXHJcblxyXG4gICAgICAgICAgbGV0IHsgaW5kZXggfSA9IHsgLi4ucGF5bG9hZCB9XHJcblxyXG4gICAgICAgICAgY29tbWl0KCdzZXRFbnRpdHknLCB7IG9iakVudGl0eVNlbGVjdDogZ2V0dGVycy5nZXRFbnRpdHlCeUluZGV4KGluZGV4KVsgMCBdIH0pXHJcbiAgICAgICAgfSxcclxuICAgICAgICBjcmVhdGVFbnRpdHlBY3QoeyBjb21taXQsIGdldHRlcnMgfSwgcGF5bG9hZCkge1xyXG4gICAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKGZ1bmN0aW9uIChyZXNvbHZlLCByZWplY3QpIHtcclxuICAgICAgICAgICAgbGV0IHJlcVBhcmFtcyA9IHsgLi4ucGF5bG9hZCB9XHJcbiAgICAgICAgICAgIEFQSS5jcmVhdGVVc2VyUmVxKHJlcVBhcmFtcykuZG9uZSgocmVzKSA9PiB7XHJcbiAgICAgICAgICAgICAgdHJpZ2dlck1vZGFsKHJlcy5tZXNzYWdlKVxyXG4gICAgICAgICAgICAgIGlmIChpc0Vycm9yQVBJKHJlcykpIHtcclxuICAgICAgICAgICAgICAgIHJlamVjdChyZXMucGF5bG9hZClcclxuICAgICAgICAgICAgICAgIHJldHVyblxyXG4gICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgbGV0IHsgYXJyVXNlckRldGFpbCB9ID0geyAuLi5yZXMucGF5bG9hZCB9XHJcblxyXG4gICAgICAgICAgICAgIGNvbW1pdCgndXBkYXRlRW50aXR5Jywge1xyXG4gICAgICAgICAgICAgICAgdHlwZTogJ2NyZWF0ZScsXHJcbiAgICAgICAgICAgICAgICBhcnJFbnRpdHk6IGFyclVzZXJEZXRhaWxcclxuICAgICAgICAgICAgICB9KVxyXG5cclxuICAgICAgICAgICAgICByZXNvbHZlKHJlcy5wYXlsb2FkKVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgfSlcclxuICAgICAgICB9LFxyXG4gICAgICAgIHVwZGF0ZUVudGl0eUFjdCh7IGNvbW1pdCwgZ2V0dGVycyB9LCBwYXlsb2FkKSB7XHJcbiAgICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoZnVuY3Rpb24gKHJlc29sdmUsIHJlamVjdCkge1xyXG4gICAgICAgICAgICBsZXQgcmVxUGFyYW1zID0geyAuLi5wYXlsb2FkIH1cclxuXHJcbiAgICAgICAgICAgIEFQSS51cGRhdGVVc2VyUmVxKHJlcVBhcmFtcykuZG9uZSgocmVzKSA9PiB7XHJcbiAgICAgICAgICAgICAgdHJpZ2dlck1vZGFsKHJlcy5tZXNzYWdlKVxyXG4gICAgICAgICAgICAgIGlmIChpc0Vycm9yQVBJKHJlcykpIHtcclxuICAgICAgICAgICAgICAgIHJlamVjdChyZXMucGF5bG9hZClcclxuICAgICAgICAgICAgICAgIHJldHVyblxyXG4gICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgbGV0IHsgYXJyVXNlckRldGFpbCB9ID0geyAuLi5yZXMucGF5bG9hZCB9XHJcblxyXG4gICAgICAgICAgICAgIGNvbW1pdCgndXBkYXRlRW50aXR5Jywge1xyXG4gICAgICAgICAgICAgICAgdHlwZTogJ3VwZGF0ZScsXHJcbiAgICAgICAgICAgICAgICBpbmRleDogZ2V0dGVycy5nZXRJbmRleEJ5SUQoYXJyVXNlckRldGFpbC51c2VyX2lkKSxcclxuICAgICAgICAgICAgICAgIGFyckVudGl0eTogYXJyVXNlckRldGFpbFxyXG4gICAgICAgICAgICAgIH0pXHJcblxyXG4gICAgICAgICAgICAgIHJlc29sdmUocmVzLnBheWxvYWQpXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICB9KVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgZGVsZXRlRW50aXR5QWN0KHsgY29tbWl0LCBnZXR0ZXJzIH0sIHBheWxvYWQpIHtcclxuICAgICAgICAgIHJldHVybiBuZXcgUHJvbWlzZShmdW5jdGlvbiAocmVzb2x2ZSwgcmVqZWN0KSB7XHJcbiAgICAgICAgICAgIGxldCByZXFQYXJhbXMgPSB7IC4uLnBheWxvYWQgfVxyXG5cclxuICAgICAgICAgICAgQVBJLmRlbGV0ZVVzZXJSZXEocmVxUGFyYW1zKS5kb25lKHJlcyA9PiB7XHJcbiAgICAgICAgICAgICAgdHJpZ2dlck1vZGFsKHJlcy5tZXNzYWdlKVxyXG4gICAgICAgICAgICAgIGlmIChpc0Vycm9yQVBJKHJlcykpIHtcclxuICAgICAgICAgICAgICAgIHJlamVjdChyZXMucGF5bG9hZClcclxuXHJcbiAgICAgICAgICAgICAgICByZXR1cm5cclxuICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgIGxldCB7IGlkIH0gPSB7IC4uLnJlcy5wYXlsb2FkIH1cclxuXHJcbiAgICAgICAgICAgICAgY29uc29sZS5sb2coaWQpXHJcblxyXG4gICAgICAgICAgICAgIGNvbW1pdCgndXBkYXRlRW50aXR5Jywge1xyXG4gICAgICAgICAgICAgICAgdHlwZTogJ2RlbGV0ZScsXHJcbiAgICAgICAgICAgICAgICBpbmRleDogZ2V0dGVycy5nZXRJbmRleEJ5SUQoaWQpXHJcbiAgICAgICAgICAgICAgfSlcclxuXHJcbiAgICAgICAgICAgICAgcmVzb2x2ZShyZXMucGF5bG9hZClcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgIH0pXHJcbiAgICAgICAgfVxyXG4gICAgICB9LFxyXG4gICAgICBnZXR0ZXJzOiB7XHJcbiAgICAgICAgZ2V0RW50aXR5QnlJbmRleDogc3RhdGUgPT4gaW5kZXggPT4ge1xyXG4gICAgICAgICAgcmV0dXJuIHN0YXRlLmFyckVudGl0eUxpc3QuZmlsdGVyKChvYmpFbnRpdHksIGN1cnJlbnRJbmRleCkgPT4gY3VycmVudEluZGV4ID09IGluZGV4KVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgZ2V0SW5kZXhCeUlEOiBzdGF0ZSA9PiBpZCA9PiB7XHJcbiAgICAgICAgICBsZXQgaW5kZXggPSAtMVxyXG4gICAgICAgICAgX2ZvckluKHN0YXRlLmFyckVudGl0eUxpc3QsIChvYmpFbnRpdHksIGN1cnJlbnRJbmRleCkgPT4ge1xyXG4gICAgICAgICAgICBpZiAob2JqRW50aXR5LnVzZXJfaWQgIT0gaWQpXHJcbiAgICAgICAgICAgICAgcmV0dXJuXHJcblxyXG4gICAgICAgICAgICBpbmRleCA9IGN1cnJlbnRJbmRleFxyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2VcclxuICAgICAgICAgIH0pXHJcblxyXG4gICAgICAgICAgcmV0dXJuIGluZGV4XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCAobmV3IFN0b3JlKCkpO1xyXG5cclxuXHJcbiIsImltcG9ydCB7IHJlbmRlciwgc3RhdGljUmVuZGVyRm5zIH0gZnJvbSBcIi4vVGhlTW9kYWxGdWxsU2NyZWVuLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD0xZTY5Y2M2MyZcIlxuaW1wb3J0IHNjcmlwdCBmcm9tIFwiLi9UaGVNb2RhbEZ1bGxTY3JlZW4udnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiXG5leHBvcnQgKiBmcm9tIFwiLi9UaGVNb2RhbEZ1bGxTY3JlZW4udnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiXG5cblxuLyogbm9ybWFsaXplIGNvbXBvbmVudCAqL1xuaW1wb3J0IG5vcm1hbGl6ZXIgZnJvbSBcIiEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvcnVudGltZS9jb21wb25lbnROb3JtYWxpemVyLmpzXCJcbnZhciBjb21wb25lbnQgPSBub3JtYWxpemVyKFxuICBzY3JpcHQsXG4gIHJlbmRlcixcbiAgc3RhdGljUmVuZGVyRm5zLFxuICBmYWxzZSxcbiAgbnVsbCxcbiAgbnVsbCxcbiAgbnVsbFxuICBcbilcblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHtcbiAgdmFyIGFwaSA9IHJlcXVpcmUoXCJEOlxcXFxQcm9qZWN0XFxcXHNoYXJlXFxcXG91dHNvdWNlX3RodWVuaGFcXFxcaHRtbFxcXFxzdGF0aWNcXFxcYmFja2VuZFxcXFx2MVxcXFxub2RlX21vZHVsZXNcXFxcdnVlLWhvdC1yZWxvYWQtYXBpXFxcXGRpc3RcXFxcaW5kZXguanNcIilcbiAgYXBpLmluc3RhbGwocmVxdWlyZSgndnVlJykpXG4gIGlmIChhcGkuY29tcGF0aWJsZSkge1xuICAgIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgICBpZiAoIWFwaS5pc1JlY29yZGVkKCcxZTY5Y2M2MycpKSB7XG4gICAgICBhcGkuY3JlYXRlUmVjb3JkKCcxZTY5Y2M2MycsIGNvbXBvbmVudC5vcHRpb25zKVxuICAgIH0gZWxzZSB7XG4gICAgICBhcGkucmVsb2FkKCcxZTY5Y2M2MycsIGNvbXBvbmVudC5vcHRpb25zKVxuICAgIH1cbiAgICBtb2R1bGUuaG90LmFjY2VwdChcIi4vVGhlTW9kYWxGdWxsU2NyZWVuLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD0xZTY5Y2M2MyZcIiwgZnVuY3Rpb24gKCkge1xuICAgICAgYXBpLnJlcmVuZGVyKCcxZTY5Y2M2MycsIHtcbiAgICAgICAgcmVuZGVyOiByZW5kZXIsXG4gICAgICAgIHN0YXRpY1JlbmRlckZuczogc3RhdGljUmVuZGVyRm5zXG4gICAgICB9KVxuICAgIH0pXG4gIH1cbn1cbmNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwic3JjL3Z1ZS9wYXJ0aWFsL2dsb2JhbC9UaGVNb2RhbEZ1bGxTY3JlZW4udnVlXCJcbmV4cG9ydCBkZWZhdWx0IGNvbXBvbmVudC5leHBvcnRzIiwiaW1wb3J0IG1vZCBmcm9tIFwiLSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvYmFiZWwtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9pbmRleC5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4vVGhlTW9kYWxGdWxsU2NyZWVuLnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIjsgZXhwb3J0IGRlZmF1bHQgbW9kOyBleHBvcnQgKiBmcm9tIFwiLSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvYmFiZWwtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9pbmRleC5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4vVGhlTW9kYWxGdWxsU2NyZWVuLnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIiIsImV4cG9ydCAqIGZyb20gXCItIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9sb2FkZXJzL3RlbXBsYXRlTG9hZGVyLmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2luZGV4LmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi9UaGVNb2RhbEZ1bGxTY3JlZW4udnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPTFlNjljYzYzJlwiIiwiaW1wb3J0IHsgcmVuZGVyLCBzdGF0aWNSZW5kZXJGbnMgfSBmcm9tIFwiLi9Vc2VyVGFibGUudnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPTc1YjMyZGVmJlwiXG5pbXBvcnQgc2NyaXB0IGZyb20gXCIuL1VzZXJUYWJsZS52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCJcbmV4cG9ydCAqIGZyb20gXCIuL1VzZXJUYWJsZS52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCJcblxuXG4vKiBub3JtYWxpemUgY29tcG9uZW50ICovXG5pbXBvcnQgbm9ybWFsaXplciBmcm9tIFwiIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9ydW50aW1lL2NvbXBvbmVudE5vcm1hbGl6ZXIuanNcIlxudmFyIGNvbXBvbmVudCA9IG5vcm1hbGl6ZXIoXG4gIHNjcmlwdCxcbiAgcmVuZGVyLFxuICBzdGF0aWNSZW5kZXJGbnMsXG4gIGZhbHNlLFxuICBudWxsLFxuICBudWxsLFxuICBudWxsXG4gIFxuKVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkge1xuICB2YXIgYXBpID0gcmVxdWlyZShcIkQ6XFxcXFByb2plY3RcXFxcc2hhcmVcXFxcb3V0c291Y2VfdGh1ZW5oYVxcXFxodG1sXFxcXHN0YXRpY1xcXFxiYWNrZW5kXFxcXHYxXFxcXG5vZGVfbW9kdWxlc1xcXFx2dWUtaG90LXJlbG9hZC1hcGlcXFxcZGlzdFxcXFxpbmRleC5qc1wiKVxuICBhcGkuaW5zdGFsbChyZXF1aXJlKCd2dWUnKSlcbiAgaWYgKGFwaS5jb21wYXRpYmxlKSB7XG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICAgIGlmICghYXBpLmlzUmVjb3JkZWQoJzc1YjMyZGVmJykpIHtcbiAgICAgIGFwaS5jcmVhdGVSZWNvcmQoJzc1YjMyZGVmJywgY29tcG9uZW50Lm9wdGlvbnMpXG4gICAgfSBlbHNlIHtcbiAgICAgIGFwaS5yZWxvYWQoJzc1YjMyZGVmJywgY29tcG9uZW50Lm9wdGlvbnMpXG4gICAgfVxuICAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiLi9Vc2VyVGFibGUudnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPTc1YjMyZGVmJlwiLCBmdW5jdGlvbiAoKSB7XG4gICAgICBhcGkucmVyZW5kZXIoJzc1YjMyZGVmJywge1xuICAgICAgICByZW5kZXI6IHJlbmRlcixcbiAgICAgICAgc3RhdGljUmVuZGVyRm5zOiBzdGF0aWNSZW5kZXJGbnNcbiAgICAgIH0pXG4gICAgfSlcbiAgfVxufVxuY29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJzcmMvdnVlL3BhcnRpYWwvdXNlci9Vc2VyVGFibGUudnVlXCJcbmV4cG9ydCBkZWZhdWx0IGNvbXBvbmVudC5leHBvcnRzIiwiaW1wb3J0IG1vZCBmcm9tIFwiLSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvYmFiZWwtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9pbmRleC5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4vVXNlclRhYmxlLnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIjsgZXhwb3J0IGRlZmF1bHQgbW9kOyBleHBvcnQgKiBmcm9tIFwiLSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvYmFiZWwtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9pbmRleC5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4vVXNlclRhYmxlLnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIiIsImV4cG9ydCAqIGZyb20gXCItIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9sb2FkZXJzL3RlbXBsYXRlTG9hZGVyLmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2luZGV4LmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi9Vc2VyVGFibGUudnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPTc1YjMyZGVmJlwiIiwiaW1wb3J0IHsgcmVuZGVyLCBzdGF0aWNSZW5kZXJGbnMgfSBmcm9tIFwiLi9Vc2VyVGFibGVSb3cudnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPTQyYzFiODliJlwiXG5pbXBvcnQgc2NyaXB0IGZyb20gXCIuL1VzZXJUYWJsZVJvdy52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCJcbmV4cG9ydCAqIGZyb20gXCIuL1VzZXJUYWJsZVJvdy52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCJcblxuXG4vKiBub3JtYWxpemUgY29tcG9uZW50ICovXG5pbXBvcnQgbm9ybWFsaXplciBmcm9tIFwiIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9ydW50aW1lL2NvbXBvbmVudE5vcm1hbGl6ZXIuanNcIlxudmFyIGNvbXBvbmVudCA9IG5vcm1hbGl6ZXIoXG4gIHNjcmlwdCxcbiAgcmVuZGVyLFxuICBzdGF0aWNSZW5kZXJGbnMsXG4gIGZhbHNlLFxuICBudWxsLFxuICBudWxsLFxuICBudWxsXG4gIFxuKVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkge1xuICB2YXIgYXBpID0gcmVxdWlyZShcIkQ6XFxcXFByb2plY3RcXFxcc2hhcmVcXFxcb3V0c291Y2VfdGh1ZW5oYVxcXFxodG1sXFxcXHN0YXRpY1xcXFxiYWNrZW5kXFxcXHYxXFxcXG5vZGVfbW9kdWxlc1xcXFx2dWUtaG90LXJlbG9hZC1hcGlcXFxcZGlzdFxcXFxpbmRleC5qc1wiKVxuICBhcGkuaW5zdGFsbChyZXF1aXJlKCd2dWUnKSlcbiAgaWYgKGFwaS5jb21wYXRpYmxlKSB7XG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICAgIGlmICghYXBpLmlzUmVjb3JkZWQoJzQyYzFiODliJykpIHtcbiAgICAgIGFwaS5jcmVhdGVSZWNvcmQoJzQyYzFiODliJywgY29tcG9uZW50Lm9wdGlvbnMpXG4gICAgfSBlbHNlIHtcbiAgICAgIGFwaS5yZWxvYWQoJzQyYzFiODliJywgY29tcG9uZW50Lm9wdGlvbnMpXG4gICAgfVxuICAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiLi9Vc2VyVGFibGVSb3cudnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPTQyYzFiODliJlwiLCBmdW5jdGlvbiAoKSB7XG4gICAgICBhcGkucmVyZW5kZXIoJzQyYzFiODliJywge1xuICAgICAgICByZW5kZXI6IHJlbmRlcixcbiAgICAgICAgc3RhdGljUmVuZGVyRm5zOiBzdGF0aWNSZW5kZXJGbnNcbiAgICAgIH0pXG4gICAgfSlcbiAgfVxufVxuY29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJzcmMvdnVlL3BhcnRpYWwvdXNlci9Vc2VyVGFibGVSb3cudnVlXCJcbmV4cG9ydCBkZWZhdWx0IGNvbXBvbmVudC5leHBvcnRzIiwiaW1wb3J0IG1vZCBmcm9tIFwiLSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvYmFiZWwtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9pbmRleC5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4vVXNlclRhYmxlUm93LnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIjsgZXhwb3J0IGRlZmF1bHQgbW9kOyBleHBvcnQgKiBmcm9tIFwiLSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvYmFiZWwtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9pbmRleC5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4vVXNlclRhYmxlUm93LnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIiIsImV4cG9ydCAqIGZyb20gXCItIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9sb2FkZXJzL3RlbXBsYXRlTG9hZGVyLmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2luZGV4LmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi9Vc2VyVGFibGVSb3cudnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPTQyYzFiODliJlwiIl0sInNvdXJjZVJvb3QiOiIifQ==
