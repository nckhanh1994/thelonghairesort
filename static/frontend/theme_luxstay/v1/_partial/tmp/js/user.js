/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(Object.prototype.hasOwnProperty.call(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"user": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push([5,"vendor"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/global/TheModalFullScreen.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--1!./node_modules/vue-loader/lib??vue-loader-options!./src/vue/partial/global/TheModalFullScreen.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var $jsBasePath_define__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! $jsBasePath/define */ "./src/js/base/define.js");
/* harmony import */ var $jsBasePath_EventBus__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! $jsBasePath/EventBus */ "./src/js/base/EventBus.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'modal',
  components: {},
  data: function data() {
    return {
      title: 'THÔNG BÁO !',
      content: 'We are lucky to live in a glorious age that gives us everything as a human race.'
    };
  },
  computed: {},
  created: function created() {
    var self = this;
    $jsBasePath_EventBus__WEBPACK_IMPORTED_MODULE_1__["default"].on($jsBasePath_define__WEBPACK_IMPORTED_MODULE_0__["EVENT_BUS_KEY"].UPDATE_CONTENT_MODAL, function (content) {
      self.content = content;
    });
    $(document).on('keyup', function (e) {
      var keyCode = e.which;
      if (keyCode != 13 && keyCode != 27) return;
      var flag = $('#modal-fs').hasClass('show');
      if (!flag) return;
      $('#modal-fs-btn').trigger('click');
      $jsBasePath_EventBus__WEBPACK_IMPORTED_MODULE_1__["default"].emit($jsBasePath_define__WEBPACK_IMPORTED_MODULE_0__["EVENT_BUS_KEY"].CONFIRM_MODAL, true);
    });
  },
  methods: {
    confirm: function confirm() {
      $('#modal-fs-btn').trigger('click');
      $jsBasePath_EventBus__WEBPACK_IMPORTED_MODULE_1__["default"].emit($jsBasePath_define__WEBPACK_IMPORTED_MODULE_0__["EVENT_BUS_KEY"].CONFIRM_MODAL, true);
    }
  }
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js-exposed")))

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/user/App.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--1!./node_modules/vue-loader/lib??vue-loader-options!./src/vue/partial/user/App.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'user-app',
  components: {},
  data: function data() {
    return {};
  },
  computed: {},
  created: function created() {},
  methods: {}
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/user/UserAddSection.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--1!./node_modules/vue-loader/lib??vue-loader-options!./src/vue/partial/user/UserAddSection.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vee_validate__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vee-validate */ "./node_modules/vee-validate/dist/vee-validate.esm.js");
/* harmony import */ var vee_validate_dist_rules__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vee-validate/dist/rules */ "./node_modules/vee-validate/dist/rules.js");
/* harmony import */ var $jsBasePath_define__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! $jsBasePath/define */ "./src/js/base/define.js");
/* harmony import */ var $jsBasePath_EventBus__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! $jsBasePath/EventBus */ "./src/js/base/EventBus.js");
/* harmony import */ var $jsCommonPath_util_VeeValidateMessage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! $jsCommonPath/util/VeeValidateMessage */ "./src/js/common/util/VeeValidateMessage.js");
/* harmony import */ var $jsCommonPath_util_SafeUtil__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! $jsCommonPath/util/SafeUtil */ "./src/js/common/util/SafeUtil.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//







var objMessage = $jsCommonPath_util_VeeValidateMessage__WEBPACK_IMPORTED_MODULE_5__["default"].messages;
Object(vee_validate__WEBPACK_IMPORTED_MODULE_1__["extend"])('required', _objectSpread({}, vee_validate_dist_rules__WEBPACK_IMPORTED_MODULE_2__["required"], {
  message: objMessage.required
}));
Object(vee_validate__WEBPACK_IMPORTED_MODULE_1__["extend"])('email', _objectSpread({}, vee_validate_dist_rules__WEBPACK_IMPORTED_MODULE_2__["email"], {
  message: objMessage.email
}));
Object(vee_validate__WEBPACK_IMPORTED_MODULE_1__["extend"])('min', _objectSpread({}, vee_validate_dist_rules__WEBPACK_IMPORTED_MODULE_2__["min"], {
  message: objMessage.min
}));
Object(vee_validate__WEBPACK_IMPORTED_MODULE_1__["extend"])('max', _objectSpread({}, vee_validate_dist_rules__WEBPACK_IMPORTED_MODULE_2__["max"], {
  message: objMessage.max
}));
Object(vee_validate__WEBPACK_IMPORTED_MODULE_1__["extend"])('integer', _objectSpread({}, vee_validate_dist_rules__WEBPACK_IMPORTED_MODULE_2__["integer"], {
  message: objMessage.integer
}));
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'user-add-section',
  components: {
    'validation-observer': vee_validate__WEBPACK_IMPORTED_MODULE_1__["ValidationObserver"],
    'validation-provider': vee_validate__WEBPACK_IMPORTED_MODULE_1__["ValidationProvider"]
  },
  data: function data() {
    return {
      url: {
        resourceURL: CDATA.url.resourceURL
      },
      flag: {
        isValidating: false,
        isUpdating: false
      },
      frmData: {}
    };
  },
  computed: {
    profileImage: function profileImage() {
      if (Object(lodash__WEBPACK_IMPORTED_MODULE_0__["isEmpty"])(this.frmData.profile_image)) return this.url.resourceURL + '/global/image/notFound.png';
      return this.frmData.profile_image;
    },
    objEntitySelect: function objEntitySelect() {
      return this.$store.state.entity.objEntitySelect;
    }
  },
  created: function created() {
    var _this = this;

    var self = this;
    $jsBasePath_EventBus__WEBPACK_IMPORTED_MODULE_4__["default"].off($jsBasePath_define__WEBPACK_IMPORTED_MODULE_3__["EVENT_BUS_KEY"].CONFIRM_MODAL);
    $jsBasePath_EventBus__WEBPACK_IMPORTED_MODULE_4__["default"].on($jsBasePath_define__WEBPACK_IMPORTED_MODULE_3__["EVENT_BUS_KEY"].CONFIRM_MODAL, function (payload) {
      _this.$store.commit('resetEntity');

      self.flag.isValidating = false;
      self.flag.isUpdating = false;
      _this.frmData = _this.objEntitySelect;
    });
    this.$store.commit('resetEntity');
    this.frmData = {...this.objEntitySelect};
  },
  methods: {
    goRead: function goRead() {
      this.$router.push({
        name: 'user:index'
      });
    },
    goCreate: function goCreate() {
      this.$router.push({
        name: 'user:add',
        params: {
          page: 1
        }
      });
    },
    back: function back() {
      return this.$router.back();
    },
    update: function update(invalid) {
      this.flag.isValidating = true;

      if (invalid) {
        this.$refs.observer.validate();
        return;
      }

      this.flag.isUpdating = true;
      this.$store.dispatch('createEntityAct', this.frmData);
    },
    upload: function upload(e) {
      var _this2 = this;

      var file = e.target.files[0];
      this.$store.dispatch('uploadFileAct', {
        controller: CDATA.common.controller,
        file: file
      }).then(function (payload) {
        var objImage = payload.arrImage[0];

        if (Object(lodash__WEBPACK_IMPORTED_MODULE_0__["isEmpty"])(objImage)) {
          return;
        }

        _this2.frmData.profile_image = objImage.image;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/user/UserEditSection.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--1!./node_modules/vue-loader/lib??vue-loader-options!./src/vue/partial/user/UserEditSection.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vee_validate__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vee-validate */ "./node_modules/vee-validate/dist/vee-validate.esm.js");
/* harmony import */ var vee_validate_dist_rules__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vee-validate/dist/rules */ "./node_modules/vee-validate/dist/rules.js");
/* harmony import */ var $jsBasePath_define__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! $jsBasePath/define */ "./src/js/base/define.js");
/* harmony import */ var $jsBasePath_EventBus__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! $jsBasePath/EventBus */ "./src/js/base/EventBus.js");
/* harmony import */ var $jsCommonPath_util_VeeValidateMessage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! $jsCommonPath/util/VeeValidateMessage */ "./src/js/common/util/VeeValidateMessage.js");
/* harmony import */ var $jsCommonPath_util_SafeUtil__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! $jsCommonPath/util/SafeUtil */ "./src/js/common/util/SafeUtil.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//







var objMessage = $jsCommonPath_util_VeeValidateMessage__WEBPACK_IMPORTED_MODULE_5__["default"].messages;
Object(vee_validate__WEBPACK_IMPORTED_MODULE_1__["extend"])('required', _objectSpread({}, vee_validate_dist_rules__WEBPACK_IMPORTED_MODULE_2__["required"], {
  message: objMessage.required
}));
Object(vee_validate__WEBPACK_IMPORTED_MODULE_1__["extend"])('email', _objectSpread({}, vee_validate_dist_rules__WEBPACK_IMPORTED_MODULE_2__["email"], {
  message: objMessage.email
}));
Object(vee_validate__WEBPACK_IMPORTED_MODULE_1__["extend"])('min', _objectSpread({}, vee_validate_dist_rules__WEBPACK_IMPORTED_MODULE_2__["min"], {
  message: objMessage.min
}));
Object(vee_validate__WEBPACK_IMPORTED_MODULE_1__["extend"])('max', _objectSpread({}, vee_validate_dist_rules__WEBPACK_IMPORTED_MODULE_2__["max"], {
  message: objMessage.max
}));
Object(vee_validate__WEBPACK_IMPORTED_MODULE_1__["extend"])('integer', _objectSpread({}, vee_validate_dist_rules__WEBPACK_IMPORTED_MODULE_2__["integer"], {
  message: objMessage.integer
}));
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'user-add-section',
  components: {
    'validation-observer': vee_validate__WEBPACK_IMPORTED_MODULE_1__["ValidationObserver"],
    'validation-provider': vee_validate__WEBPACK_IMPORTED_MODULE_1__["ValidationProvider"]
  },
  data: function data() {
    return {
      url: {
        resourceURL: CDATA.url.resourceURL
      },
      flag: {
        isValidating: false,
        isUpdating: false
      },
      frmData: {}
    };
  },
  computed: {
    profileImage: function profileImage() {
      if (Object(lodash__WEBPACK_IMPORTED_MODULE_0__["isEmpty"])(this.frmData.profile_image)) return this.url.resourceURL + '/global/image/notFound.png';
      return this.frmData.profile_image;
    },
    objEntitySelect: function objEntitySelect() {
      return this.$store.state.entity.objEntitySelect;
    }
  },
  created: function created() {
    var _this = this;

    var self = this;
    $jsBasePath_EventBus__WEBPACK_IMPORTED_MODULE_4__["default"].off($jsBasePath_define__WEBPACK_IMPORTED_MODULE_3__["EVENT_BUS_KEY"].CONFIRM_MODAL);
    $jsBasePath_EventBus__WEBPACK_IMPORTED_MODULE_4__["default"].on($jsBasePath_define__WEBPACK_IMPORTED_MODULE_3__["EVENT_BUS_KEY"].CONFIRM_MODAL, function (payload) {
      self.flag.isValidating = false;
      self.flag.isUpdating = false;
      _this.frmData = _this.objEntitySelect;
      _this.frmData.password = null;
    });

    if (!this.$store.state.flag.isRead) {
      var _this$$route$params = this.$route.params,
          id = _this$$route$params.id,
          page = _this$$route$params.page;
      if (Object($jsCommonPath_util_SafeUtil__WEBPACK_IMPORTED_MODULE_6__["_int"])(id) <= 0) return;
      this.get(id);
    } else {
      this.frmData = {...this.objEntitySelect};
    }
  },
  methods: {
    goRead: function goRead() {
      this.$router.push({
        name: 'user:index'
      });
    },
    goCreate: function goCreate() {
      this.$router.push({
        name: 'user:add',
        params: {
          page: 1
        }
      });
    },
    back: function back() {
      return this.$router.back();
    },
    get: function get(id) {
      var _this2 = this;

      this.$store.dispatch('getEntityAct', {
        id: id
      }).then(function (payload) {
        _this2.frmData = _this2.objEntitySelect;
      });
    },
    update: function update(invalid) {
      this.flag.isValidating = true;

      if (invalid) {
        this.$refs.observer.validate();
        return;
      }

      this.flag.isUpdating = true;
      var id = this.objEntitySelect.user_id;
      this.$store.dispatch('updateEntityAct', _objectSpread({
        id: id
      }, this.frmData));
    },
    upload: function upload(e) {
      var _this3 = this;

      var file = e.target.files[0];
      this.$store.dispatch('uploadFileAct', {
        controller: CDATA.common.controller,
        file: file
      }).then(function (payload) {
        var objImage = payload.arrImage[0];

        if (Object(lodash__WEBPACK_IMPORTED_MODULE_0__["isEmpty"])(objImage)) {
          return;
        }

        _this3.frmData.profile_image = objImage.image;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/user/UserIndexSection.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--1!./node_modules/vue-loader/lib??vue-loader-options!./src/vue/partial/user/UserIndexSection.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var $vuePartialPath_user_UserTable__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! $vuePartialPath/user/UserTable */ "./src/vue/partial/user/UserTable.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'user-index-section',
  components: {
    'user-table': $vuePartialPath_user_UserTable__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {};
  },
  computed: {},
  created: function created() {
    if (!this.$store.state.flag.isRead) {
      this.$store.commit('updateFlag', {
        flag: 'isRead',
        value: true
      });
      this.$store.dispatch('readEntityAct');
    }
  },
  methods: {
    goRead: function goRead() {
      this.$router.push({
        name: 'user:index'
      });
    },
    goCreate: function goCreate() {
      this.$router.push({
        name: 'user:add',
        params: {
          page: 1
        }
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/user/UserTable.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--1!./node_modules/vue-loader/lib??vue-loader-options!./src/vue/partial/user/UserTable.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var $vuePartialPath_user_UserTableRow__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! $vuePartialPath/user/UserTableRow */ "./src/vue/partial/user/UserTableRow.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'user-table',
  components: {
    'user-table-row': $vuePartialPath_user_UserTableRow__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {};
  },
  computed: {
    arrEntityList: function arrEntityList() {
      return this.$store.state.entity.arrEntityList;
    }
  },
  created: function created() {},
  methods: {}
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/user/UserTableRow.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--1!./node_modules/vue-loader/lib??vue-loader-options!./src/vue/partial/user/UserTableRow.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var $jsCommonPath_util_SafeUtil__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! $jsCommonPath/util/SafeUtil */ "./src/js/common/util/SafeUtil.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'user-table-row',
  props: {
    index: {
      default: {},
      type: Number
    },
    objEntity: {
      default: {},
      type: Object
    }
  },
  components: {},
  data: function data() {
    return {
      url: {
        resourceURL: CDATA.url.resourceURL
      },
      flag: {
        isDeleting: false
      }
    };
  },
  computed: {
    profileImage: function profileImage() {
      if (Object($jsCommonPath_util_SafeUtil__WEBPACK_IMPORTED_MODULE_0__["_isEmptyString"])(this.objEntity.profile_image)) return this.url.resourceURL + '/global/image/notFound.png';
      return this.objEntity.profile_image;
    }
  },
  created: function created() {},
  methods: {
    goUpdate: function goUpdate() {
      this.$store.dispatch('setEntityAct', {
        index: this.index
      });
      this.$router.push({
        name: 'user:edit',
        params: {
          id: this.objEntity.user_id,
          page: 1
        }
      });
    },
    remove: function remove(id) {
      var _this = this;

      if (!id) return;
      this.flag.isDeleting = true;
      this.$store.dispatch('deleteEntityAct', {
        id: id
      }).then(function (res) {
        _this.flag.isDeleting = false;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vee-validate/dist/rules.js":
/*!*************************************************!*\
  !*** ./node_modules/vee-validate/dist/rules.js ***!
  \*************************************************/
/*! exports provided: alpha, alpha_dash, alpha_num, alpha_spaces, between, confirmed, digits, dimensions, email, excluded, ext, image, integer, is, is_not, length, max, max_value, mimes, min, min_value, numeric, oneOf, regex, required, required_if, size */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "alpha", function() { return alpha$1; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "alpha_dash", function() { return alpha_dash; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "alpha_num", function() { return alpha_num; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "alpha_spaces", function() { return alpha_spaces; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "between", function() { return between; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "confirmed", function() { return confirmed; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "digits", function() { return digits; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "dimensions", function() { return dimensions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "email", function() { return email; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "excluded", function() { return excluded; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ext", function() { return ext; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "image", function() { return image; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "integer", function() { return integer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "is", function() { return is; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "is_not", function() { return is_not; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "length", function() { return length; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "max", function() { return max; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "max_value", function() { return max_value; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mimes", function() { return mimes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "min", function() { return min; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "min_value", function() { return min_value; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "numeric", function() { return numeric; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "oneOf", function() { return oneOf; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "regex", function() { return regex; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "required", function() { return required; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "required_if", function() { return required_if; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "size", function() { return size; });
/**
  * vee-validate v3.0.11
  * (c) 2019 Abdelrahman Awad
  * @license MIT
  */
/**
 * Some Alpha Regex helpers.
 * https://github.com/chriso/validator.js/blob/master/src/lib/alpha.js
 */
var alpha = {
    en: /^[A-Z]*$/i,
    cs: /^[A-ZÁČĎÉĚÍŇÓŘŠŤÚŮÝŽ]*$/i,
    da: /^[A-ZÆØÅ]*$/i,
    de: /^[A-ZÄÖÜß]*$/i,
    es: /^[A-ZÁÉÍÑÓÚÜ]*$/i,
    fr: /^[A-ZÀÂÆÇÉÈÊËÏÎÔŒÙÛÜŸ]*$/i,
    it: /^[A-Z\xC0-\xFF]*$/i,
    lt: /^[A-ZĄČĘĖĮŠŲŪŽ]*$/i,
    nl: /^[A-ZÉËÏÓÖÜ]*$/i,
    hu: /^[A-ZÁÉÍÓÖŐÚÜŰ]*$/i,
    pl: /^[A-ZĄĆĘŚŁŃÓŻŹ]*$/i,
    pt: /^[A-ZÃÁÀÂÇÉÊÍÕÓÔÚÜ]*$/i,
    ru: /^[А-ЯЁ]*$/i,
    sk: /^[A-ZÁÄČĎÉÍĹĽŇÓŔŠŤÚÝŽ]*$/i,
    sr: /^[A-ZČĆŽŠĐ]*$/i,
    sv: /^[A-ZÅÄÖ]*$/i,
    tr: /^[A-ZÇĞİıÖŞÜ]*$/i,
    uk: /^[А-ЩЬЮЯЄІЇҐ]*$/i,
    ar: /^[ءآأؤإئابةتثجحخدذرزسشصضطظعغفقكلمنهوىيًٌٍَُِّْٰ]*$/,
    az: /^[A-ZÇƏĞİıÖŞÜ]*$/i
};
var alphaSpaces = {
    en: /^[A-Z\s]*$/i,
    cs: /^[A-ZÁČĎÉĚÍŇÓŘŠŤÚŮÝŽ\s]*$/i,
    da: /^[A-ZÆØÅ\s]*$/i,
    de: /^[A-ZÄÖÜß\s]*$/i,
    es: /^[A-ZÁÉÍÑÓÚÜ\s]*$/i,
    fr: /^[A-ZÀÂÆÇÉÈÊËÏÎÔŒÙÛÜŸ\s]*$/i,
    it: /^[A-Z\xC0-\xFF\s]*$/i,
    lt: /^[A-ZĄČĘĖĮŠŲŪŽ\s]*$/i,
    nl: /^[A-ZÉËÏÓÖÜ\s]*$/i,
    hu: /^[A-ZÁÉÍÓÖŐÚÜŰ\s]*$/i,
    pl: /^[A-ZĄĆĘŚŁŃÓŻŹ\s]*$/i,
    pt: /^[A-ZÃÁÀÂÇÉÊÍÕÓÔÚÜ\s]*$/i,
    ru: /^[А-ЯЁ\s]*$/i,
    sk: /^[A-ZÁÄČĎÉÍĹĽŇÓŔŠŤÚÝŽ\s]*$/i,
    sr: /^[A-ZČĆŽŠĐ\s]*$/i,
    sv: /^[A-ZÅÄÖ\s]*$/i,
    tr: /^[A-ZÇĞİıÖŞÜ\s]*$/i,
    uk: /^[А-ЩЬЮЯЄІЇҐ\s]*$/i,
    ar: /^[ءآأؤإئابةتثجحخدذرزسشصضطظعغفقكلمنهوىيًٌٍَُِّْٰ\s]*$/,
    az: /^[A-ZÇƏĞİıÖŞÜ\s]*$/i
};
var alphanumeric = {
    en: /^[0-9A-Z]*$/i,
    cs: /^[0-9A-ZÁČĎÉĚÍŇÓŘŠŤÚŮÝŽ]*$/i,
    da: /^[0-9A-ZÆØÅ]$/i,
    de: /^[0-9A-ZÄÖÜß]*$/i,
    es: /^[0-9A-ZÁÉÍÑÓÚÜ]*$/i,
    fr: /^[0-9A-ZÀÂÆÇÉÈÊËÏÎÔŒÙÛÜŸ]*$/i,
    it: /^[0-9A-Z\xC0-\xFF]*$/i,
    lt: /^[0-9A-ZĄČĘĖĮŠŲŪŽ]*$/i,
    hu: /^[0-9A-ZÁÉÍÓÖŐÚÜŰ]*$/i,
    nl: /^[0-9A-ZÉËÏÓÖÜ]*$/i,
    pl: /^[0-9A-ZĄĆĘŚŁŃÓŻŹ]*$/i,
    pt: /^[0-9A-ZÃÁÀÂÇÉÊÍÕÓÔÚÜ]*$/i,
    ru: /^[0-9А-ЯЁ]*$/i,
    sk: /^[0-9A-ZÁÄČĎÉÍĹĽŇÓŔŠŤÚÝŽ]*$/i,
    sr: /^[0-9A-ZČĆŽŠĐ]*$/i,
    sv: /^[0-9A-ZÅÄÖ]*$/i,
    tr: /^[0-9A-ZÇĞİıÖŞÜ]*$/i,
    uk: /^[0-9А-ЩЬЮЯЄІЇҐ]*$/i,
    ar: /^[٠١٢٣٤٥٦٧٨٩0-9ءآأؤإئابةتثجحخدذرزسشصضطظعغفقكلمنهوىيًٌٍَُِّْٰ]*$/,
    az: /^[0-9A-ZÇƏĞİıÖŞÜ]*$/i
};
var alphaDash = {
    en: /^[0-9A-Z_-]*$/i,
    cs: /^[0-9A-ZÁČĎÉĚÍŇÓŘŠŤÚŮÝŽ_-]*$/i,
    da: /^[0-9A-ZÆØÅ_-]*$/i,
    de: /^[0-9A-ZÄÖÜß_-]*$/i,
    es: /^[0-9A-ZÁÉÍÑÓÚÜ_-]*$/i,
    fr: /^[0-9A-ZÀÂÆÇÉÈÊËÏÎÔŒÙÛÜŸ_-]*$/i,
    it: /^[0-9A-Z\xC0-\xFF_-]*$/i,
    lt: /^[0-9A-ZĄČĘĖĮŠŲŪŽ_-]*$/i,
    nl: /^[0-9A-ZÉËÏÓÖÜ_-]*$/i,
    hu: /^[0-9A-ZÁÉÍÓÖŐÚÜŰ_-]*$/i,
    pl: /^[0-9A-ZĄĆĘŚŁŃÓŻŹ_-]*$/i,
    pt: /^[0-9A-ZÃÁÀÂÇÉÊÍÕÓÔÚÜ_-]*$/i,
    ru: /^[0-9А-ЯЁ_-]*$/i,
    sk: /^[0-9A-ZÁÄČĎÉÍĹĽŇÓŔŠŤÚÝŽ_-]*$/i,
    sr: /^[0-9A-ZČĆŽŠĐ_-]*$/i,
    sv: /^[0-9A-ZÅÄÖ_-]*$/i,
    tr: /^[0-9A-ZÇĞİıÖŞÜ_-]*$/i,
    uk: /^[0-9А-ЩЬЮЯЄІЇҐ_-]*$/i,
    ar: /^[٠١٢٣٤٥٦٧٨٩0-9ءآأؤإئابةتثجحخدذرزسشصضطظعغفقكلمنهوىيًٌٍَُِّْٰ_-]*$/,
    az: /^[0-9A-ZÇƏĞİıÖŞÜ_-]*$/i
};

var validate = function (value, _a) {
    var _b = (_a === void 0 ? {} : _a).locale, locale = _b === void 0 ? '' : _b;
    if (Array.isArray(value)) {
        return value.every(function (val) { return validate(val, { locale: locale }); });
    }
    // Match at least one locale.
    if (!locale) {
        return Object.keys(alpha).some(function (loc) { return alpha[loc].test(value); });
    }
    return (alpha[locale] || alpha.en).test(value);
};
var params = [
    {
        name: 'locale'
    }
];
var alpha$1 = {
    validate: validate,
    params: params
};

var validate$1 = function (value, _a) {
    var _b = (_a === void 0 ? {} : _a).locale, locale = _b === void 0 ? '' : _b;
    if (Array.isArray(value)) {
        return value.every(function (val) { return validate$1(val, { locale: locale }); });
    }
    // Match at least one locale.
    if (!locale) {
        return Object.keys(alphaDash).some(function (loc) { return alphaDash[loc].test(value); });
    }
    return (alphaDash[locale] || alphaDash.en).test(value);
};
var params$1 = [
    {
        name: 'locale'
    }
];
var alpha_dash = {
    validate: validate$1,
    params: params$1
};

var validate$2 = function (value, _a) {
    var _b = (_a === void 0 ? {} : _a).locale, locale = _b === void 0 ? '' : _b;
    if (Array.isArray(value)) {
        return value.every(function (val) { return validate$2(val, { locale: locale }); });
    }
    // Match at least one locale.
    if (!locale) {
        return Object.keys(alphanumeric).some(function (loc) { return alphanumeric[loc].test(value); });
    }
    return (alphanumeric[locale] || alphanumeric.en).test(value);
};
var params$2 = [
    {
        name: 'locale'
    }
];
var alpha_num = {
    validate: validate$2,
    params: params$2
};

var validate$3 = function (value, _a) {
    var _b = (_a === void 0 ? {} : _a).locale, locale = _b === void 0 ? '' : _b;
    if (Array.isArray(value)) {
        return value.every(function (val) { return validate$3(val, { locale: locale }); });
    }
    // Match at least one locale.
    if (!locale) {
        return Object.keys(alphaSpaces).some(function (loc) { return alphaSpaces[loc].test(value); });
    }
    return (alphaSpaces[locale] || alphaSpaces.en).test(value);
};
var params$3 = [
    {
        name: 'locale'
    }
];
var alpha_spaces = {
    validate: validate$3,
    params: params$3
};

var validate$4 = function (value, _a) {
    var _b = _a === void 0 ? {} : _a, min = _b.min, max = _b.max;
    if (Array.isArray(value)) {
        return value.every(function (val) { return !!validate$4(val, { min: min, max: max }); });
    }
    return Number(min) <= value && Number(max) >= value;
};
var params$4 = [
    {
        name: 'min'
    },
    {
        name: 'max'
    }
];
var between = {
    validate: validate$4,
    params: params$4
};

var validate$5 = function (value, _a) {
    var target = _a.target;
    return String(value) === String(target);
};
var params$5 = [
    {
        name: 'target',
        isTarget: true
    }
];
var confirmed = {
    validate: validate$5,
    params: params$5
};

var validate$6 = function (value, _a) {
    var length = _a.length;
    if (Array.isArray(value)) {
        return value.every(function (val) { return validate$6(val, { length: length }); });
    }
    var strVal = String(value);
    return /^[0-9]*$/.test(strVal) && strVal.length === length;
};
var params$6 = [
    {
        name: 'length',
        cast: function (value) {
            return Number(value);
        }
    }
];
var digits = {
    validate: validate$6,
    params: params$6
};

var validateImage = function (file, width, height) {
    var URL = window.URL || window.webkitURL;
    return new Promise(function (resolve) {
        var image = new Image();
        image.onerror = function () { return resolve(false); };
        image.onload = function () { return resolve(image.width === width && image.height === height); };
        image.src = URL.createObjectURL(file);
    });
};
var validate$7 = function (files, _a) {
    var width = _a.width, height = _a.height;
    var list = [];
    files = Array.isArray(files) ? files : [files];
    for (var i = 0; i < files.length; i++) {
        // if file is not an image, reject.
        if (!/\.(jpg|svg|jpeg|png|bmp|gif)$/i.test(files[i].name)) {
            return Promise.resolve(false);
        }
        list.push(files[i]);
    }
    return Promise.all(list.map(function (file) { return validateImage(file, width, height); })).then(function (values) {
        return values.every(function (v) { return v; });
    });
};
var params$7 = [
    {
        name: 'width',
        cast: function (value) {
            return Number(value);
        }
    },
    {
        name: 'height',
        cast: function (value) {
            return Number(value);
        }
    }
];
var dimensions = {
    validate: validate$7,
    params: params$7
};

var validate$8 = function (value, _a) {
    var multiple = (_a === void 0 ? {} : _a).multiple;
    // eslint-disable-next-line
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (multiple && !Array.isArray(value)) {
        value = String(value)
            .split(',')
            .map(function (emailStr) { return emailStr.trim(); });
    }
    if (Array.isArray(value)) {
        return value.every(function (val) { return re.test(String(val)); });
    }
    return re.test(String(value));
};
var params$8 = [
    {
        name: 'multiple',
        default: false
    }
];
var email = {
    validate: validate$8,
    params: params$8
};

/**
 * Checks if the values are either null or undefined.
 */
var isNullOrUndefined = function (value) {
    return value === null || value === undefined;
};
var includes = function (collection, item) {
    return collection.indexOf(item) !== -1;
};
/**
 * Checks if a function is callable.
 */
var isCallable = function (func) { return typeof func === 'function'; };
/* istanbul ignore next */
function _copyArray(arrayLike) {
    var array = [];
    var length = arrayLike.length;
    for (var i = 0; i < length; i++) {
        array.push(arrayLike[i]);
    }
    return array;
}
/**
 * Converts an array-like object to array, provides a simple polyfill for Array.from
 */
function toArray(arrayLike) {
    if (isCallable(Array.from)) {
        return Array.from(arrayLike);
    }
    /* istanbul ignore next */
    return _copyArray(arrayLike);
}
var isEmptyArray = function (arr) {
    return Array.isArray(arr) && arr.length === 0;
};

var validate$9 = function (value, options) {
    if (Array.isArray(value)) {
        return value.every(function (val) { return validate$9(val, options); });
    }
    return toArray(options).some(function (item) {
        // eslint-disable-next-line
        return item == value;
    });
};
var oneOf = {
    validate: validate$9
};

var validate$a = function (value, args) {
    return !validate$9(value, args);
};
var excluded = {
    validate: validate$a
};

var validate$b = function (files, extensions) {
    var regex = new RegExp(".(" + extensions.join('|') + ")$", 'i');
    if (Array.isArray(files)) {
        return files.every(function (file) { return regex.test(file.name); });
    }
    return regex.test(files.name);
};
var ext = {
    validate: validate$b
};

var validate$c = function (files) {
    var regex = /\.(jpg|svg|jpeg|png|bmp|gif)$/i;
    if (Array.isArray(files)) {
        return files.every(function (file) { return regex.test(file.name); });
    }
    return regex.test(files.name);
};
var image = {
    validate: validate$c
};

var validate$d = function (value) {
    if (Array.isArray(value)) {
        return value.every(function (val) { return /^-?[0-9]+$/.test(String(val)); });
    }
    return /^-?[0-9]+$/.test(String(value));
};
var integer = {
    validate: validate$d
};

var validate$e = function (value, _a) {
    var other = _a.other;
    return value === other;
};
var params$9 = [
    {
        name: 'other'
    }
];
var is = {
    validate: validate$e,
    params: params$9
};

var validate$f = function (value, _a) {
    var other = _a.other;
    return value !== other;
};
var params$a = [
    {
        name: 'other'
    }
];
var is_not = {
    validate: validate$f,
    params: params$a
};

var validate$g = function (value, _a) {
    var length = _a.length;
    if (isNullOrUndefined(value)) {
        return false;
    }
    if (typeof value === 'number') {
        value = String(value);
    }
    if (!value.length) {
        value = toArray(value);
    }
    return value.length === length;
};
var params$b = [
    {
        name: 'length',
        cast: function (value) { return Number(value); }
    }
];
var length = {
    validate: validate$g,
    params: params$b
};

var validate$h = function (value, _a) {
    var length = _a.length;
    if (isNullOrUndefined(value)) {
        return length >= 0;
    }
    if (Array.isArray(value)) {
        return value.every(function (val) { return validate$h(val, { length: length }); });
    }
    return String(value).length <= length;
};
var params$c = [
    {
        name: 'length',
        cast: function (value) {
            return Number(value);
        }
    }
];
var max = {
    validate: validate$h,
    params: params$c
};

var validate$i = function (value, _a) {
    var max = _a.max;
    if (isNullOrUndefined(value) || value === '') {
        return false;
    }
    if (Array.isArray(value)) {
        return value.length > 0 && value.every(function (val) { return validate$i(val, { max: max }); });
    }
    return Number(value) <= max;
};
var params$d = [
    {
        name: 'max',
        cast: function (value) {
            return Number(value);
        }
    }
];
var max_value = {
    validate: validate$i,
    params: params$d
};

var validate$j = function (files, mimes) {
    var regex = new RegExp(mimes.join('|').replace('*', '.+') + "$", 'i');
    if (Array.isArray(files)) {
        return files.every(function (file) { return regex.test(file.type); });
    }
    return regex.test(files.type);
};
var mimes = {
    validate: validate$j
};

var validate$k = function (value, _a) {
    var length = _a.length;
    if (isNullOrUndefined(value)) {
        return false;
    }
    if (Array.isArray(value)) {
        return value.every(function (val) { return validate$k(val, { length: length }); });
    }
    return String(value).length >= length;
};
var params$e = [
    {
        name: 'length',
        cast: function (value) {
            return Number(value);
        }
    }
];
var min = {
    validate: validate$k,
    params: params$e
};

var validate$l = function (value, _a) {
    var min = _a.min;
    if (isNullOrUndefined(value) || value === '') {
        return false;
    }
    if (Array.isArray(value)) {
        return value.length > 0 && value.every(function (val) { return validate$l(val, { min: min }); });
    }
    return Number(value) >= min;
};
var params$f = [
    {
        name: 'min',
        cast: function (value) {
            return Number(value);
        }
    }
];
var min_value = {
    validate: validate$l,
    params: params$f
};

var ar = /^[٠١٢٣٤٥٦٧٨٩]+$/;
var en = /^[0-9]+$/;
var validate$m = function (value) {
    var testValue = function (val) {
        var strValue = String(val);
        return en.test(strValue) || ar.test(strValue);
    };
    if (Array.isArray(value)) {
        return value.every(testValue);
    }
    return testValue(value);
};
var numeric = {
    validate: validate$m
};

var validate$n = function (value, _a) {
    var regex = _a.regex;
    if (Array.isArray(value)) {
        return value.every(function (val) { return validate$n(val, { regex: regex }); });
    }
    return regex.test(String(value));
};
var params$g = [
    {
        name: 'regex',
        cast: function (value) {
            if (typeof value === 'string') {
                return new RegExp(value);
            }
            return value;
        }
    }
];
var regex = {
    validate: validate$n,
    params: params$g
};

var validate$o = function (value, _a) {
    var allowFalse = (_a === void 0 ? { allowFalse: true } : _a).allowFalse;
    var result = {
        valid: false,
        required: true
    };
    if (isNullOrUndefined(value) || isEmptyArray(value)) {
        return result;
    }
    // incase a field considers `false` as an empty value like checkboxes.
    if (value === false && !allowFalse) {
        return result;
    }
    result.valid = !!String(value).trim().length;
    return result;
};
var computesRequired = true;
var params$h = [
    {
        name: 'allowFalse',
        default: true
    }
];
var required = {
    validate: validate$o,
    params: params$h,
    computesRequired: computesRequired
};

var testEmpty = function (value) {
    return isEmptyArray(value) || includes([false, null, undefined], value) || !String(value).trim().length;
};
var validate$p = function (value, _a) {
    var target = _a.target, values = _a.values;
    var required;
    if (values && values.length) {
        if (!Array.isArray(values) && typeof values === 'string') {
            values = [values];
        }
        // eslint-disable-next-line
        required = values.some(function (val) { return val == String(target).trim(); });
    }
    else {
        required = !testEmpty(target);
    }
    if (!required) {
        return {
            valid: true,
            required: required
        };
    }
    return {
        valid: !testEmpty(value),
        required: required
    };
};
var params$i = [
    {
        name: 'target',
        isTarget: true
    },
    {
        name: 'values'
    }
];
var computesRequired$1 = true;
var required_if = {
    validate: validate$p,
    params: params$i,
    computesRequired: computesRequired$1
};

var validate$q = function (files, _a) {
    var size = _a.size;
    if (isNaN(size)) {
        return false;
    }
    var nSize = size * 1024;
    if (!Array.isArray(files)) {
        return files.size <= nSize;
    }
    for (var i = 0; i < files.length; i++) {
        if (files[i].size > nSize) {
            return false;
        }
    }
    return true;
};
var params$j = [
    {
        name: 'size',
        cast: function (value) {
            return Number(value);
        }
    }
];
var size = {
    validate: validate$q,
    params: params$j
};




/***/ }),

/***/ "./node_modules/vee-validate/dist/vee-validate.esm.js":
/*!************************************************************!*\
  !*** ./node_modules/vee-validate/dist/vee-validate.esm.js ***!
  \************************************************************/
/*! exports provided: ValidationObserver, ValidationProvider, configure, extend, install, localize, setInteractionMode, validate, version, withValidation */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ValidationObserver", function() { return ValidationObserver; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ValidationProvider", function() { return ValidationProvider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "configure", function() { return configure; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "extend", function() { return extend; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "install", function() { return install; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "localize", function() { return localize; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setInteractionMode", function() { return setInteractionMode; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "validate", function() { return validate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "version", function() { return version; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "withValidation", function() { return withValidation; });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.runtime.esm.js");
/**
  * vee-validate v3.0.11
  * (c) 2019 Abdelrahman Awad
  * @license MIT
  */


/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};

function __awaiter(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function __generator(thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
}

function __spreadArrays() {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
}

var isNaN = function (value) {
    // NaN is the one value that does not equal itself.
    // eslint-disable-next-line
    return value !== value;
};
/**
 * Checks if the values are either null or undefined.
 */
var isNullOrUndefined = function (value) {
    return value === null || value === undefined;
};
/**
 * Creates the default flags object.
 */
var createFlags = function () { return ({
    untouched: true,
    touched: false,
    dirty: false,
    pristine: true,
    valid: false,
    invalid: false,
    validated: false,
    pending: false,
    required: false,
    changed: false
}); };
/**
 * Checks if the value is an object.
 */
var isObject = function (obj) {
    return obj !== null && obj && typeof obj === 'object' && !Array.isArray(obj);
};
function identity(x) {
    return x;
}
/**
 * Shallow object comparison.
 */
var isEqual = function (lhs, rhs) {
    if (lhs instanceof RegExp && rhs instanceof RegExp) {
        return isEqual(lhs.source, rhs.source) && isEqual(lhs.flags, rhs.flags);
    }
    if (Array.isArray(lhs) && Array.isArray(rhs)) {
        if (lhs.length !== rhs.length)
            return false;
        for (var i = 0; i < lhs.length; i++) {
            if (!isEqual(lhs[i], rhs[i])) {
                return false;
            }
        }
        return true;
    }
    // if both are objects, compare each key recursively.
    if (isObject(lhs) && isObject(rhs)) {
        return (Object.keys(lhs).every(function (key) {
            return isEqual(lhs[key], rhs[key]);
        }) &&
            Object.keys(rhs).every(function (key) {
                return isEqual(lhs[key], rhs[key]);
            }));
    }
    if (isNaN(lhs) && isNaN(rhs)) {
        return true;
    }
    return lhs === rhs;
};
var includes = function (collection, item) {
    return collection.indexOf(item) !== -1;
};
/**
 * Parses a rule string expression.
 */
var parseRule = function (rule) {
    var params = [];
    var name = rule.split(':')[0];
    if (includes(rule, ':')) {
        params = rule
            .split(':')
            .slice(1)
            .join(':')
            .split(',');
    }
    return { name: name, params: params };
};
/**
 * Debounces a function.
 */
var debounce = function (fn, wait, token) {
    if (wait === void 0) { wait = 0; }
    if (token === void 0) { token = { cancelled: false }; }
    if (wait === 0) {
        return fn;
    }
    var timeout;
    return function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        var later = function () {
            timeout = undefined;
            // check if the fn call was cancelled.
            if (!token.cancelled)
                fn.apply(void 0, args);
        };
        // because we might want to use Node.js setTimout for SSR.
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
    };
};
/**
 * Emits a warning to the console.
 */
var warn = function (message) {
    console.warn("[vee-validate] " + message);
};
/**
 * Normalizes the given rules expression.
 */
var normalizeRules = function (rules) {
    // if falsy value return an empty object.
    var acc = {};
    Object.defineProperty(acc, '_$$isNormalized', {
        value: true,
        writable: false,
        enumerable: false,
        configurable: false
    });
    if (!rules) {
        return acc;
    }
    // Object is already normalized, skip.
    if (isObject(rules) && rules._$$isNormalized) {
        return rules;
    }
    if (isObject(rules)) {
        return Object.keys(rules).reduce(function (prev, curr) {
            var params = [];
            if (rules[curr] === true) {
                params = [];
            }
            else if (Array.isArray(rules[curr])) {
                params = rules[curr];
            }
            else if (isObject(rules[curr])) {
                params = rules[curr];
            }
            else {
                params = [rules[curr]];
            }
            if (rules[curr] !== false) {
                prev[curr] = params;
            }
            return prev;
        }, acc);
    }
    /* istanbul ignore if */
    if (typeof rules !== 'string') {
        warn('rules must be either a string or an object.');
        return acc;
    }
    return rules.split('|').reduce(function (prev, rule) {
        var parsedRule = parseRule(rule);
        if (!parsedRule.name) {
            return prev;
        }
        prev[parsedRule.name] = parsedRule.params;
        return prev;
    }, acc);
};
/**
 * Checks if a function is callable.
 */
var isCallable = function (func) { return typeof func === 'function'; };
function computeClassObj(names, flags) {
    var acc = {};
    var keys = Object.keys(flags);
    var length = keys.length;
    var _loop_1 = function (i) {
        var flag = keys[i];
        var className = (names && names[flag]) || flag;
        var value = flags[flag];
        if (isNullOrUndefined(value)) {
            return "continue";
        }
        if ((flag === 'valid' || flag === 'invalid') && !flags.validated) {
            return "continue";
        }
        if (typeof className === 'string') {
            acc[className] = value;
        }
        else if (Array.isArray(className)) {
            className.forEach(function (cls) {
                acc[cls] = value;
            });
        }
    };
    for (var i = 0; i < length; i++) {
        _loop_1(i);
    }
    return acc;
}
/* istanbul ignore next */
function _copyArray(arrayLike) {
    var array = [];
    var length = arrayLike.length;
    for (var i = 0; i < length; i++) {
        array.push(arrayLike[i]);
    }
    return array;
}
/**
 * Converts an array-like object to array, provides a simple polyfill for Array.from
 */
function toArray(arrayLike) {
    if (isCallable(Array.from)) {
        return Array.from(arrayLike);
    }
    /* istanbul ignore next */
    return _copyArray(arrayLike);
}
function findIndex(arrayLike, predicate) {
    var array = Array.isArray(arrayLike) ? arrayLike : toArray(arrayLike);
    if (isCallable(array.findIndex)) {
        return array.findIndex(predicate);
    }
    /* istanbul ignore next */
    for (var i = 0; i < array.length; i++) {
        if (predicate(array[i], i)) {
            return i;
        }
    }
    /* istanbul ignore next */
    return -1;
}
/**
 * finds the first element that satisfies the predicate callback, polyfills array.find
 */
function find(arrayLike, predicate) {
    var array = Array.isArray(arrayLike) ? arrayLike : toArray(arrayLike);
    var idx = findIndex(array, predicate);
    return idx === -1 ? undefined : array[idx];
}
function merge(target, source) {
    Object.keys(source).forEach(function (key) {
        if (isObject(source[key])) {
            if (!target[key]) {
                target[key] = {};
            }
            merge(target[key], source[key]);
            return;
        }
        target[key] = source[key];
    });
    return target;
}
function values(obj) {
    if (isCallable(Object.values)) {
        return Object.values(obj);
    }
    // fallback to keys()
    /* istanbul ignore next */
    return Object.keys(obj).map(function (k) { return obj[k]; });
}
var isEmptyArray = function (arr) {
    return Array.isArray(arr) && arr.length === 0;
};
var interpolate = function (template, values) {
    return template.replace(/\{([^}]+)\}/g, function (_, p) {
        return p in values ? values[p] : "{" + p + "}";
    });
};
// Checks if a given value is not an empty string or null or undefined.
var isSpecified = function (val) {
    if (val === '') {
        return false;
    }
    return !isNullOrUndefined(val);
};

var RULES = {};
function normalizeSchema(schema) {
    if (schema.params && schema.params.length) {
        schema.params = schema.params.map(function (param) {
            if (typeof param === 'string') {
                return { name: param };
            }
            return param;
        });
    }
    return schema;
}
var RuleContainer = /** @class */ (function () {
    function RuleContainer() {
    }
    RuleContainer.extend = function (name, schema) {
        // if rule already exists, overwrite it.
        var rule = normalizeSchema(schema);
        if (RULES[name]) {
            RULES[name] = merge(RULES[name], schema);
            return;
        }
        RULES[name] = __assign({ lazy: false, computesRequired: false }, rule);
    };
    RuleContainer.iterate = function (fn) {
        var keys = Object.keys(RULES);
        var length = keys.length;
        for (var i = 0; i < length; i++) {
            fn(keys[i], RULES[keys[i]]);
        }
    };
    RuleContainer.isLazy = function (name) {
        return !!(RULES[name] && RULES[name].lazy);
    };
    RuleContainer.isRequireRule = function (name) {
        return !!(RULES[name] && RULES[name].computesRequired);
    };
    RuleContainer.isTargetRule = function (name) {
        var definition = RuleContainer.getRuleDefinition(name);
        if (!definition || !definition.params) {
            return false;
        }
        return definition.params.some(function (param) { return !!param.isTarget; });
    };
    RuleContainer.getTargetParamNames = function (rule, params) {
        var definition = RuleContainer.getRuleDefinition(rule);
        if (Array.isArray(params)) {
            return params.filter(function (_, idx) {
                return definition.params && find(definition.params, function (p, i) { return !!p.isTarget && i === idx; });
            });
        }
        return Object.keys(params)
            .filter(function (key) {
            return definition.params && find(definition.params, function (p) { return !!p.isTarget && p.name === key; });
        })
            .map(function (key) { return params[key]; });
    };
    RuleContainer.getRuleDefinition = function (ruleName) {
        return RULES[ruleName];
    };
    return RuleContainer;
}());
/**
 * Adds a custom validator to the list of validation rules.
 */
function extend(name, schema) {
    // makes sure new rules are properly formatted.
    guardExtend(name, schema);
    // Full schema object.
    if (typeof schema === 'object') {
        RuleContainer.extend(name, schema);
        return;
    }
    RuleContainer.extend(name, {
        validate: schema
    });
}
/**
 * Guards from extension violations.
 */
function guardExtend(name, validator) {
    if (isCallable(validator)) {
        return;
    }
    if (isCallable(validator.validate)) {
        return;
    }
    if (RuleContainer.getRuleDefinition(name)) {
        return;
    }
    throw new Error("Extension Error: The validator '" + name + "' must be a function or have a 'validate' method.");
}

var DEFAULT_CONFIG = {
    defaultMessage: "{_field_} is not valid.",
    skipOptional: true,
    classes: {
        touched: 'touched',
        untouched: 'untouched',
        valid: 'valid',
        invalid: 'invalid',
        pristine: 'pristine',
        dirty: 'dirty' // control has been interacted with
    },
    bails: true,
    mode: 'aggressive',
    useConstraintAttrs: true
};
var currentConfig = __assign({}, DEFAULT_CONFIG);
var getConfig = function () { return currentConfig; };
var setConfig = function (newConf) {
    currentConfig = __assign(__assign({}, currentConfig), newConf);
};
var configure = function (cfg) {
    setConfig(cfg);
};

/**
 * Validates a value against the rules.
 */
function validate(value, rules, options) {
    if (options === void 0) { options = {}; }
    return __awaiter(this, void 0, void 0, function () {
        var shouldBail, skipIfEmpty, field, result, errors, ruleMap;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    shouldBail = options && options.bails;
                    skipIfEmpty = options && options.skipIfEmpty;
                    field = {
                        name: (options && options.name) || '{field}',
                        rules: normalizeRules(rules),
                        bails: isNullOrUndefined(shouldBail) ? true : shouldBail,
                        skipIfEmpty: isNullOrUndefined(skipIfEmpty) ? true : skipIfEmpty,
                        forceRequired: false,
                        crossTable: (options && options.values) || {},
                        names: (options && options.names) || {},
                        customMessages: (options && options.customMessages) || {}
                    };
                    return [4 /*yield*/, _validate(field, value, options)];
                case 1:
                    result = _a.sent();
                    errors = [];
                    ruleMap = {};
                    result.errors.forEach(function (e) {
                        errors.push(e.msg);
                        ruleMap[e.rule] = e.msg;
                    });
                    return [2 /*return*/, {
                            valid: result.valid,
                            errors: errors,
                            failedRules: ruleMap
                        }];
            }
        });
    });
}
/**
 * Starts the validation process.
 */
function _validate(field, value, _a) {
    var _b = (_a === void 0 ? {} : _a).isInitial, isInitial = _b === void 0 ? false : _b;
    return __awaiter(this, void 0, void 0, function () {
        var _c, shouldSkip, errors, rules, length, i, rule, result;
        return __generator(this, function (_d) {
            switch (_d.label) {
                case 0: return [4 /*yield*/, _shouldSkip(field, value)];
                case 1:
                    _c = _d.sent(), shouldSkip = _c.shouldSkip, errors = _c.errors;
                    if (shouldSkip) {
                        return [2 /*return*/, {
                                valid: !errors.length,
                                errors: errors
                            }];
                    }
                    rules = Object.keys(field.rules).filter(function (rule) { return !RuleContainer.isRequireRule(rule); });
                    length = rules.length;
                    i = 0;
                    _d.label = 2;
                case 2:
                    if (!(i < length)) return [3 /*break*/, 5];
                    if (isInitial && RuleContainer.isLazy(rules[i])) {
                        return [3 /*break*/, 4];
                    }
                    rule = rules[i];
                    return [4 /*yield*/, _test(field, value, {
                            name: rule,
                            params: field.rules[rule]
                        })];
                case 3:
                    result = _d.sent();
                    if (!result.valid && result.error) {
                        errors.push(result.error);
                        if (field.bails) {
                            return [2 /*return*/, {
                                    valid: false,
                                    errors: errors
                                }];
                        }
                    }
                    _d.label = 4;
                case 4:
                    i++;
                    return [3 /*break*/, 2];
                case 5: return [2 /*return*/, {
                        valid: !errors.length,
                        errors: errors
                    }];
            }
        });
    });
}
function _shouldSkip(field, value) {
    return __awaiter(this, void 0, void 0, function () {
        var requireRules, length, errors, isEmpty, isEmptyAndOptional, isRequired, i, rule, result;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    requireRules = Object.keys(field.rules).filter(RuleContainer.isRequireRule);
                    length = requireRules.length;
                    errors = [];
                    isEmpty = isNullOrUndefined(value) || value === '' || isEmptyArray(value);
                    isEmptyAndOptional = isEmpty && field.skipIfEmpty;
                    isRequired = false;
                    i = 0;
                    _a.label = 1;
                case 1:
                    if (!(i < length)) return [3 /*break*/, 4];
                    rule = requireRules[i];
                    return [4 /*yield*/, _test(field, value, {
                            name: rule,
                            params: field.rules[rule]
                        })];
                case 2:
                    result = _a.sent();
                    if (!isObject(result)) {
                        throw new Error('Require rules has to return an object (see docs)');
                    }
                    if (result.required) {
                        isRequired = true;
                    }
                    if (!result.valid && result.error) {
                        errors.push(result.error);
                        // Exit early as the field is required and failed validation.
                        if (field.bails) {
                            return [2 /*return*/, {
                                    shouldSkip: true,
                                    errors: errors
                                }];
                        }
                    }
                    _a.label = 3;
                case 3:
                    i++;
                    return [3 /*break*/, 1];
                case 4:
                    if (isEmpty && !isRequired && !field.skipIfEmpty) {
                        return [2 /*return*/, {
                                shouldSkip: false,
                                errors: errors
                            }];
                    }
                    // field is configured to run through the pipeline regardless
                    if (!field.bails && !isEmptyAndOptional) {
                        return [2 /*return*/, {
                                shouldSkip: false,
                                errors: errors
                            }];
                    }
                    // skip if the field is not required and has an empty value.
                    return [2 /*return*/, {
                            shouldSkip: !isRequired && isEmpty,
                            errors: errors
                        }];
            }
        });
    });
}
/**
 * Tests a single input value against a rule.
 */
function _test(field, value, rule) {
    return __awaiter(this, void 0, void 0, function () {
        var ruleSchema, params, normalizedValue, result, values;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    ruleSchema = RuleContainer.getRuleDefinition(rule.name);
                    if (!ruleSchema || !ruleSchema.validate) {
                        throw new Error("No such validator '" + rule.name + "' exists.");
                    }
                    params = _buildParams(rule.params, ruleSchema.params, field.crossTable);
                    normalizedValue = ruleSchema.castValue ? ruleSchema.castValue(value) : value;
                    return [4 /*yield*/, ruleSchema.validate(normalizedValue, params)];
                case 1:
                    result = _a.sent();
                    if (typeof result === 'string') {
                        values = __assign(__assign({}, (params || {})), { _field_: field.name, _value_: value, _rule_: rule.name });
                        return [2 /*return*/, {
                                valid: false,
                                error: { rule: rule.name, msg: interpolate(result, values) }
                            }];
                    }
                    if (!isObject(result)) {
                        result = { valid: result, data: {} };
                    }
                    return [2 /*return*/, {
                            valid: result.valid,
                            required: result.required,
                            data: result.data || {},
                            error: result.valid ? undefined : _generateFieldError(field, value, ruleSchema, rule.name, params, result.data)
                        }];
            }
        });
    });
}
/**
 * Generates error messages.
 */
function _generateFieldError(field, value, ruleSchema, ruleName, params, data) {
    var values = __assign(__assign(__assign(__assign({}, (params || {})), (data || {})), { _field_: field.name, _value_: value, _rule_: ruleName }), _getTargetNames(field, ruleSchema, ruleName));
    if (Object.prototype.hasOwnProperty.call(field.customMessages, ruleName) &&
        typeof field.customMessages[ruleName] === 'string') {
        return {
            msg: _normalizeMessage(field.customMessages[ruleName], field.name, values),
            rule: ruleName
        };
    }
    if (ruleSchema.message) {
        return {
            msg: _normalizeMessage(ruleSchema.message, field.name, values),
            rule: ruleName
        };
    }
    return {
        msg: _normalizeMessage(getConfig().defaultMessage, field.name, values),
        rule: ruleName
    };
}
function _getTargetNames(field, ruleSchema, ruleName) {
    if (ruleSchema.params) {
        var numTargets = ruleSchema.params.filter(function (param) { return param.isTarget; }).length;
        if (numTargets > 0) {
            var names = {};
            for (var index = 0; index < ruleSchema.params.length; index++) {
                var param = ruleSchema.params[index];
                if (param.isTarget) {
                    var key = field.rules[ruleName][index];
                    var name_1 = field.names[key] || key;
                    if (numTargets === 1) {
                        names._target_ = name_1;
                        break;
                    }
                    else {
                        names["_" + param.name + "Target_"] = name_1;
                    }
                }
            }
            return names;
        }
    }
    return {};
}
function _normalizeMessage(template, field, values) {
    if (typeof template === 'function') {
        return template(field, values);
    }
    return interpolate(template, __assign(__assign({}, values), { _field_: field }));
}
function _buildParams(provided, defined, crossTable) {
    var params = {};
    if (!defined && !Array.isArray(provided)) {
        throw new Error('You provided an object params to a rule that has no defined schema.');
    }
    // Rule probably uses an array for their args, keep it as is.
    if (Array.isArray(provided) && !defined) {
        return provided;
    }
    var definedRules;
    // collect the params schema.
    if (!defined || (defined.length < provided.length && Array.isArray(provided))) {
        var lastDefinedParam_1;
        // collect any additional parameters in the last item.
        definedRules = provided.map(function (_, idx) {
            var param = defined && defined[idx];
            lastDefinedParam_1 = param || lastDefinedParam_1;
            if (!param) {
                param = lastDefinedParam_1;
            }
            return param;
        });
    }
    else {
        definedRules = defined;
    }
    // Match the provided array length with a temporary schema.
    for (var i = 0; i < definedRules.length; i++) {
        var options = definedRules[i];
        var value = options.default;
        // if the provided is an array, map element value.
        if (Array.isArray(provided)) {
            if (i in provided) {
                value = provided[i];
            }
        }
        else {
            // If the param exists in the provided object.
            if (options.name in provided) {
                value = provided[options.name];
                // if the provided is the first param value.
            }
            else if (definedRules.length === 1) {
                value = provided;
            }
        }
        // if the param is a target, resolve the target value.
        if (options.isTarget) {
            value = crossTable[value];
        }
        // If there is a transformer defined.
        if (options.cast) {
            value = options.cast(value);
        }
        // already been set, probably multiple values.
        if (params[options.name]) {
            params[options.name] = Array.isArray(params[options.name]) ? params[options.name] : [params[options.name]];
            params[options.name].push(value);
        }
        else {
            // set the value.
            params[options.name] = value;
        }
    }
    return params;
}

function install(_, config) {
    setConfig(config);
}

var aggressive = function () { return ({
    on: ['input', 'blur']
}); };
var lazy = function () { return ({
    on: ['change']
}); };
var eager = function (_a) {
    var errors = _a.errors;
    if (errors.length) {
        return {
            on: ['input', 'change']
        };
    }
    return {
        on: ['change', 'blur']
    };
};
var passive = function () { return ({
    on: []
}); };
var modes = {
    aggressive: aggressive,
    eager: eager,
    passive: passive,
    lazy: lazy
};
var setInteractionMode = function (mode, implementation) {
    setConfig({ mode: mode });
    if (!implementation) {
        return;
    }
    if (!isCallable(implementation)) {
        throw new Error('A mode implementation must be a function');
    }
    modes[mode] = implementation;
};

var Dictionary = /** @class */ (function () {
    function Dictionary(locale, dictionary) {
        this.container = {};
        this.locale = locale;
        this.merge(dictionary);
    }
    Dictionary.prototype.resolve = function (field, rule, values) {
        return this.format(this.locale, field, rule, values);
    };
    Dictionary.prototype._hasLocale = function (locale) {
        return !!this.container[locale];
    };
    Dictionary.prototype.format = function (locale, field, rule, values) {
        var message;
        // find if specific message for that field was specified.
        var dict = this.container[locale] && this.container[locale].fields && this.container[locale].fields[field];
        if (dict && dict[rule]) {
            message = dict[rule];
        }
        if (!message && this._hasLocale(locale) && this._hasMessage(locale, rule)) {
            message = this.container[locale].messages[rule];
        }
        if (!message) {
            message = getConfig().defaultMessage;
        }
        if (this._hasName(locale, field)) {
            field = this.getName(locale, field);
        }
        return isCallable(message) ? message(field, values) : interpolate(message, __assign(__assign({}, values), { _field_: field }));
    };
    Dictionary.prototype.merge = function (dictionary) {
        merge(this.container, dictionary);
    };
    Dictionary.prototype.hasRule = function (name) {
        var locale = this.container[this.locale];
        if (!locale)
            return false;
        return !!(locale.messages && locale.messages[name]);
    };
    Dictionary.prototype.getName = function (locale, key) {
        return this.container[locale].names[key];
    };
    Dictionary.prototype._hasMessage = function (locale, key) {
        return !!(this._hasLocale(locale) && this.container[locale].messages && this.container[locale].messages[key]);
    };
    Dictionary.prototype._hasName = function (locale, key) {
        return !!(this._hasLocale(locale) && this.container[locale].names && this.container[locale].names[key]);
    };
    return Dictionary;
}());
var DICTIONARY;
var INSTALLED = false;
function updateRules() {
    if (INSTALLED) {
        return;
    }
    RuleContainer.iterate(function (name, schema) {
        var _a, _b;
        if (schema.message && !DICTIONARY.hasRule(name)) {
            DICTIONARY.merge((_a = {},
                _a[DICTIONARY.locale] = {
                    messages: (_b = {},
                        _b[name] = schema.message,
                        _b)
                },
                _a));
        }
        extend(name, {
            message: function (field, values) {
                return DICTIONARY.resolve(field, name, values || {});
            }
        });
    });
    INSTALLED = true;
}
function localize(locale, dictionary) {
    var _a;
    if (!DICTIONARY) {
        DICTIONARY = new Dictionary('en', {});
    }
    if (typeof locale === 'string') {
        DICTIONARY.locale = locale;
        if (dictionary) {
            DICTIONARY.merge((_a = {}, _a[locale] = dictionary, _a));
        }
        updateRules();
        return;
    }
    DICTIONARY.merge(locale);
    updateRules();
}

var isEvent = function (evt) {
    if (!evt) {
        return false;
    }
    if (typeof Event !== 'undefined' && isCallable(Event) && evt instanceof Event) {
        return true;
    }
    // this is for IE
    /* istanbul ignore next */
    if (evt && evt.srcElement) {
        return true;
    }
    return false;
};
function normalizeEventValue(value) {
    if (!isEvent(value)) {
        return value;
    }
    var input = value.target;
    if (input.type === 'file' && input.files) {
        return toArray(input.files);
    }
    // If the input has a `v-model.number` modifier applied.
    if (input._vModifiers && input._vModifiers.number) {
        // as per the spec the v-model.number uses parseFloat
        var valueAsNumber = parseFloat(input.value);
        if (isNaN(valueAsNumber)) {
            return input.value;
        }
        return valueAsNumber;
    }
    if (input._vModifiers && input._vModifiers.trim) {
        var trimmedValue = typeof input.value === 'string' ? input.value.trim() : input.value;
        return trimmedValue;
    }
    return input.value;
}

var isTextInput = function (vnode) {
    var attrs = (vnode.data && vnode.data.attrs) || vnode.elm;
    // it will fallback to being a text input per browsers spec.
    if (vnode.tag === 'input' && (!attrs || !attrs.type)) {
        return true;
    }
    if (vnode.tag === 'textarea') {
        return true;
    }
    return includes(['text', 'password', 'search', 'email', 'tel', 'url', 'number'], attrs && attrs.type);
};
// export const isCheckboxOrRadioInput = (vnode: VNode): boolean => {
//   const attrs = (vnode.data && vnode.data.attrs) || vnode.elm;
//   return includes(['radio', 'checkbox'], attrs && attrs.type);
// };
// Gets the model object on the vnode.
function findModel(vnode) {
    if (!vnode.data) {
        return undefined;
    }
    // Component Model
    // THIS IS NOT TYPED IN OFFICIAL VUE TYPINGS
    // eslint-disable-next-line
    var nonStandardVNodeData = vnode.data;
    if ('model' in nonStandardVNodeData) {
        return nonStandardVNodeData.model;
    }
    if (!vnode.data.directives) {
        return undefined;
    }
    return find(vnode.data.directives, function (d) { return d.name === 'model'; });
}
function findValue(vnode) {
    var model = findModel(vnode);
    if (model) {
        return { value: model.value };
    }
    var config = findModelConfig(vnode);
    var prop = (config && config.prop) || 'value';
    if (vnode.componentOptions && vnode.componentOptions.propsData && prop in vnode.componentOptions.propsData) {
        var propsDataWithValue = vnode.componentOptions.propsData;
        return { value: propsDataWithValue[prop] };
    }
    if (vnode.data && vnode.data.domProps && 'value' in vnode.data.domProps) {
        return { value: vnode.data.domProps.value };
    }
    return undefined;
}
function extractChildren(vnode) {
    if (Array.isArray(vnode)) {
        return vnode;
    }
    if (Array.isArray(vnode.children)) {
        return vnode.children;
    }
    /* istanbul ignore next */
    if (vnode.componentOptions && Array.isArray(vnode.componentOptions.children)) {
        return vnode.componentOptions.children;
    }
    return [];
}
function extractVNodes(vnode) {
    if (!Array.isArray(vnode) && findValue(vnode) !== undefined) {
        return [vnode];
    }
    var children = extractChildren(vnode);
    return children.reduce(function (nodes, node) {
        var candidates = extractVNodes(node);
        if (candidates.length) {
            nodes.push.apply(nodes, candidates);
        }
        return nodes;
    }, []);
}
// Resolves v-model config if exists.
function findModelConfig(vnode) {
    /* istanbul ignore next */
    if (!vnode.componentOptions)
        return null;
    // This is also not typed in the standard Vue TS.
    return vnode.componentOptions.Ctor.options.model;
}
// Adds a listener to vnode listener object.
function mergeVNodeListeners(obj, eventName, handler) {
    // no listener at all.
    if (isNullOrUndefined(obj[eventName])) {
        obj[eventName] = [handler];
        return;
    }
    // Is an invoker.
    if (isCallable(obj[eventName]) && obj[eventName].fns) {
        var invoker = obj[eventName];
        invoker.fns = Array.isArray(invoker.fns) ? invoker.fns : [invoker.fns];
        if (!includes(invoker.fns, handler)) {
            invoker.fns.push(handler);
        }
        return;
    }
    if (isCallable(obj[eventName])) {
        var prev = obj[eventName];
        obj[eventName] = [prev];
    }
    if (Array.isArray(obj[eventName]) && !includes(obj[eventName], handler)) {
        obj[eventName].push(handler);
    }
}
// Adds a listener to a native HTML vnode.
function addNativeNodeListener(node, eventName, handler) {
    /* istanbul ignore next */
    if (!node.data) {
        node.data = {};
    }
    if (isNullOrUndefined(node.data.on)) {
        node.data.on = {};
    }
    mergeVNodeListeners(node.data.on, eventName, handler);
}
// Adds a listener to a Vue component vnode.
function addComponentNodeListener(node, eventName, handler) {
    /* istanbul ignore next */
    if (!node.componentOptions) {
        return;
    }
    /* istanbul ignore next */
    if (!node.componentOptions.listeners) {
        node.componentOptions.listeners = {};
    }
    mergeVNodeListeners(node.componentOptions.listeners, eventName, handler);
}
function addVNodeListener(vnode, eventName, handler) {
    if (vnode.componentOptions) {
        addComponentNodeListener(vnode, eventName, handler);
        return;
    }
    addNativeNodeListener(vnode, eventName, handler);
}
// Determines if `change` should be used over `input` for listeners.
function getInputEventName(vnode, model) {
    // Is a component.
    if (vnode.componentOptions) {
        var event_1 = (findModelConfig(vnode) || { event: 'input' }).event;
        return event_1;
    }
    // Lazy Models typically use change event
    if (model && model.modifiers && model.modifiers.lazy) {
        return 'change';
    }
    // is a textual-type input.
    if (isTextInput(vnode)) {
        return 'input';
    }
    return 'change';
}
// TODO: Type this one properly.
function normalizeSlots(slots, ctx) {
    var acc = [];
    return Object.keys(slots).reduce(function (arr, key) {
        slots[key].forEach(function (vnode) {
            if (!vnode.context) {
                slots[key].context = ctx;
                if (!vnode.data) {
                    vnode.data = {};
                }
                vnode.data.slot = key;
            }
        });
        return arr.concat(slots[key]);
    }, acc);
}
function resolveTextualRules(vnode) {
    var attrs = vnode.data && vnode.data.attrs;
    var rules = {};
    if (!attrs)
        return rules;
    if (attrs.type === 'email') {
        rules.email = ['multiple' in attrs];
    }
    if (attrs.pattern) {
        rules.regex = attrs.pattern;
    }
    if (attrs.maxlength >= 0) {
        rules.max = attrs.maxlength;
    }
    if (attrs.minlength >= 0) {
        rules.min = attrs.minlength;
    }
    if (attrs.type === 'number') {
        if (isSpecified(attrs.min)) {
            rules.min_value = Number(attrs.min);
        }
        if (isSpecified(attrs.max)) {
            rules.max_value = Number(attrs.max);
        }
    }
    return rules;
}
function resolveRules(vnode) {
    var htmlTags = ['input', 'select'];
    var attrs = vnode.data && vnode.data.attrs;
    if (!includes(htmlTags, vnode.tag) || !attrs) {
        return {};
    }
    var rules = {};
    if ('required' in attrs && attrs.required !== false) {
        rules.required = attrs.type === 'checkbox' ? [true] : true;
    }
    if (isTextInput(vnode)) {
        return normalizeRules(__assign(__assign({}, rules), resolveTextualRules(vnode)));
    }
    return normalizeRules(rules);
}
function normalizeChildren(context, slotProps) {
    if (context.$scopedSlots.default) {
        return context.$scopedSlots.default(slotProps) || [];
    }
    return context.$slots.default || [];
}

/**
 * Determines if a provider needs to run validation.
 */
function shouldValidate(ctx, value) {
    // when an immediate/initial validation is needed and wasn't done before.
    if (!ctx._ignoreImmediate && ctx.immediate) {
        return true;
    }
    // when the value changes for whatever reason.
    if (ctx.value !== value && ctx.normalizedEvents.length) {
        return true;
    }
    // when it needs validation due to props/cross-fields changes.
    if (ctx._needsValidation) {
        return true;
    }
    // when the initial value is undefined and the field wasn't rendered yet.
    if (!ctx.initialized && value === undefined) {
        return true;
    }
    return false;
}
function createValidationCtx(ctx) {
    return __assign(__assign({}, ctx.flags), { errors: ctx.messages, classes: ctx.classes, failedRules: ctx.failedRules, reset: function () { return ctx.reset(); }, validate: function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            return ctx.validate.apply(ctx, args);
        }, ariaInput: {
            'aria-invalid': ctx.flags.invalid ? 'true' : 'false',
            'aria-required': ctx.isRequired ? 'true' : 'false',
            'aria-errormessage': "vee_" + ctx.id
        }, ariaMsg: {
            id: "vee_" + ctx.id,
            'aria-live': ctx.messages.length ? 'assertive' : 'off'
        } });
}
function onRenderUpdate(vm, value) {
    if (!vm.initialized) {
        vm.initialValue = value;
    }
    var validateNow = shouldValidate(vm, value);
    vm._needsValidation = false;
    vm.value = value;
    vm._ignoreImmediate = true;
    if (!validateNow) {
        return;
    }
    vm.validateSilent().then(vm.immediate || vm.flags.validated ? vm.applyResult : identity);
}
function computeModeSetting(ctx) {
    var compute = (isCallable(ctx.mode) ? ctx.mode : modes[ctx.mode]);
    return compute({
        errors: ctx.messages,
        value: ctx.value,
        flags: ctx.flags
    });
}
// Creates the common handlers for a validatable context.
function createCommonHandlers(vm) {
    if (!vm.$veeOnInput) {
        vm.$veeOnInput = function (e) {
            vm.syncValue(e); // track and keep the value updated.
            vm.setFlags({ dirty: true, pristine: false });
        };
    }
    var onInput = vm.$veeOnInput;
    if (!vm.$veeOnBlur) {
        vm.$veeOnBlur = function () {
            vm.setFlags({ touched: true, untouched: false });
        };
    }
    // Blur event listener.
    var onBlur = vm.$veeOnBlur;
    var onValidate = vm.$veeHandler;
    var mode = computeModeSetting(vm);
    // Handle debounce changes.
    if (!onValidate || vm.$veeDebounce !== vm.debounce) {
        onValidate = debounce(function () {
            vm.$nextTick(function () {
                var pendingPromise = vm.validateSilent();
                // avoids race conditions between successive validations.
                vm._pendingValidation = pendingPromise;
                pendingPromise.then(function (result) {
                    if (pendingPromise === vm._pendingValidation) {
                        vm.applyResult(result);
                        vm._pendingValidation = undefined;
                    }
                });
            });
        }, mode.debounce || vm.debounce);
        // Cache the handler so we don't create it each time.
        vm.$veeHandler = onValidate;
        // cache the debounce value so we detect if it was changed.
        vm.$veeDebounce = vm.debounce;
    }
    return { onInput: onInput, onBlur: onBlur, onValidate: onValidate };
}
// Adds all plugin listeners to the vnode.
function addListeners(vm, node) {
    var value = findValue(node);
    // cache the input eventName.
    vm._inputEventName = vm._inputEventName || getInputEventName(node, findModel(node));
    onRenderUpdate(vm, value && value.value);
    var _a = createCommonHandlers(vm), onInput = _a.onInput, onBlur = _a.onBlur, onValidate = _a.onValidate;
    addVNodeListener(node, vm._inputEventName, onInput);
    addVNodeListener(node, 'blur', onBlur);
    // add the validation listeners.
    vm.normalizedEvents.forEach(function (evt) {
        addVNodeListener(node, evt, onValidate);
    });
    vm.initialized = true;
}

var PROVIDER_COUNTER = 0;
function data() {
    var messages = [];
    var defaultValues = {
        messages: messages,
        value: undefined,
        initialized: false,
        initialValue: undefined,
        flags: createFlags(),
        failedRules: {},
        isDeactivated: false,
        id: ''
    };
    return defaultValues;
}
var ValidationProvider = vue__WEBPACK_IMPORTED_MODULE_0__["default"].extend({
    inject: {
        $_veeObserver: {
            from: '$_veeObserver',
            default: function () {
                if (!this.$vnode.context.$_veeObserver) {
                    this.$vnode.context.$_veeObserver = createObserver();
                }
                return this.$vnode.context.$_veeObserver;
            }
        }
    },
    props: {
        vid: {
            type: String,
            default: ''
        },
        name: {
            type: String,
            default: null
        },
        mode: {
            type: [String, Function],
            default: function () {
                return getConfig().mode;
            }
        },
        rules: {
            type: [Object, String],
            default: null
        },
        immediate: {
            type: Boolean,
            default: false
        },
        persist: {
            type: Boolean,
            default: false
        },
        bails: {
            type: Boolean,
            default: function () { return getConfig().bails; }
        },
        skipIfEmpty: {
            type: Boolean,
            default: function () { return getConfig().skipOptional; }
        },
        debounce: {
            type: Number,
            default: 0
        },
        tag: {
            type: String,
            default: 'span'
        },
        slim: {
            type: Boolean,
            default: false
        },
        disabled: {
            type: Boolean,
            default: false
        },
        customMessages: {
            type: Object,
            default: function () {
                return {};
            }
        }
    },
    watch: {
        rules: {
            deep: true,
            handler: function (val, oldVal) {
                this._needsValidation = !isEqual(val, oldVal);
            }
        }
    },
    data: data,
    computed: {
        fieldDeps: function () {
            var _this = this;
            return Object.keys(this.normalizedRules)
                .filter(RuleContainer.isTargetRule)
                .reduce(function (acc, rule) {
                var deps = RuleContainer.getTargetParamNames(rule, _this.normalizedRules[rule]);
                acc.push.apply(acc, deps);
                deps.forEach(function (depName) {
                    watchCrossFieldDep(_this, depName);
                });
                return acc;
            }, []);
        },
        normalizedEvents: function () {
            var _this = this;
            var on = computeModeSetting(this).on;
            return (on || []).map(function (e) {
                if (e === 'input') {
                    return _this._inputEventName;
                }
                return e;
            });
        },
        isRequired: function () {
            var rules = __assign(__assign({}, this._resolvedRules), this.normalizedRules);
            var isRequired = Object.keys(rules).some(RuleContainer.isRequireRule);
            this.flags.required = !!isRequired;
            return isRequired;
        },
        classes: function () {
            var names = getConfig().classes;
            return computeClassObj(names, this.flags);
        },
        normalizedRules: function () {
            return normalizeRules(this.rules);
        }
    },
    render: function (h) {
        var _this = this;
        this.registerField();
        var ctx = createValidationCtx(this);
        var children = normalizeChildren(this, ctx);
        // Handle single-root slot.
        extractVNodes(children).forEach(function (input) {
            _this._resolvedRules = getConfig().useConstraintAttrs ? resolveRules(input) : {};
            addListeners(_this, input);
        });
        return this.slim && children.length <= 1 ? children[0] : h(this.tag, children);
    },
    beforeDestroy: function () {
        // cleanup reference.
        this.$_veeObserver.unsubscribe(this.id);
    },
    activated: function () {
        this.$_veeObserver.subscribe(this);
        this.isDeactivated = false;
    },
    deactivated: function () {
        this.$_veeObserver.unsubscribe(this.id);
        this.isDeactivated = true;
    },
    methods: {
        setFlags: function (flags) {
            var _this = this;
            Object.keys(flags).forEach(function (flag) {
                _this.flags[flag] = flags[flag];
            });
        },
        syncValue: function (v) {
            var value = normalizeEventValue(v);
            this.value = value;
            this.flags.changed = this.initialValue !== value;
        },
        reset: function () {
            this.messages = [];
            this.initialValue = this.value;
            var flags = createFlags();
            flags.required = this.isRequired;
            this.setFlags(flags);
            this.validateSilent();
        },
        validate: function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            return __awaiter(this, void 0, void 0, function () {
                var result;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (args.length > 0) {
                                this.syncValue(args[0]);
                            }
                            return [4 /*yield*/, this.validateSilent()];
                        case 1:
                            result = _a.sent();
                            this.applyResult(result);
                            return [2 /*return*/, result];
                    }
                });
            });
        },
        validateSilent: function () {
            return __awaiter(this, void 0, void 0, function () {
                var rules, result;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            this.setFlags({ pending: true });
                            rules = __assign(__assign({}, this._resolvedRules), this.normalizedRules);
                            Object.defineProperty(rules, '_$$isNormalized', {
                                value: true,
                                writable: false,
                                enumerable: false,
                                configurable: false
                            });
                            return [4 /*yield*/, validate(this.value, rules, {
                                    name: this.name,
                                    values: createValuesLookup(this),
                                    bails: this.bails,
                                    skipIfEmpty: this.skipIfEmpty,
                                    isInitial: !this.initialized,
                                    customMessages: this.customMessages
                                })];
                        case 1:
                            result = _a.sent();
                            this.setFlags({ pending: false });
                            this.setFlags({ valid: result.valid, invalid: !result.valid });
                            return [2 /*return*/, result];
                    }
                });
            });
        },
        setErrors: function (errors) {
            this.applyResult({ errors: errors, failedRules: {} });
        },
        applyResult: function (_a) {
            var errors = _a.errors, failedRules = _a.failedRules;
            this.messages = errors;
            this.failedRules = __assign({}, (failedRules || {}));
            this.setFlags({
                valid: !errors.length,
                changed: this.value !== this.initialValue,
                invalid: !!errors.length,
                validated: true
            });
        },
        registerField: function () {
            updateRenderingContextRefs(this);
        }
    }
});
function createValuesLookup(vm) {
    var providers = vm.$_veeObserver.refs;
    var reduced = {};
    return vm.fieldDeps.reduce(function (acc, depName) {
        if (!providers[depName]) {
            return acc;
        }
        acc[depName] = providers[depName].value;
        return acc;
    }, reduced);
}
function extractId(vm) {
    if (vm.vid) {
        return vm.vid;
    }
    if (vm.name) {
        return vm.name;
    }
    if (vm.id) {
        return vm.id;
    }
    PROVIDER_COUNTER++;
    return "_vee_" + PROVIDER_COUNTER;
}
function updateRenderingContextRefs(vm) {
    var providedId = extractId(vm);
    var id = vm.id;
    // Nothing has changed.
    if (vm.isDeactivated || (id === providedId && vm.$_veeObserver.refs[id])) {
        return;
    }
    // vid was changed.
    if (id !== providedId && vm.$_veeObserver.refs[id] === vm) {
        vm.$_veeObserver.unsubscribe(id);
    }
    vm.id = providedId;
    vm.$_veeObserver.subscribe(vm);
}
function createObserver() {
    return {
        refs: {},
        subscribe: function (vm) {
            this.refs[vm.id] = vm;
        },
        unsubscribe: function (id) {
            delete this.refs[id];
        }
    };
}
function watchCrossFieldDep(ctx, depName, withHooks) {
    if (withHooks === void 0) { withHooks = true; }
    var providers = ctx.$_veeObserver.refs;
    if (!ctx._veeWatchers) {
        ctx._veeWatchers = {};
    }
    if (!providers[depName] && withHooks) {
        return ctx.$once('hook:mounted', function () {
            watchCrossFieldDep(ctx, depName, false);
        });
    }
    if (!isCallable(ctx._veeWatchers[depName]) && providers[depName]) {
        ctx._veeWatchers[depName] = providers[depName].$watch('value', function () {
            if (ctx.flags.validated) {
                ctx._needsValidation = true;
                ctx.validate();
            }
        });
    }
}

var flagMergingStrategy = {
    pristine: 'every',
    dirty: 'some',
    touched: 'some',
    untouched: 'every',
    valid: 'every',
    invalid: 'some',
    pending: 'some',
    validated: 'every',
    changed: 'some'
};
function mergeFlags(lhs, rhs, strategy) {
    var stratName = flagMergingStrategy[strategy];
    return [lhs, rhs][stratName](function (f) { return f; });
}
var OBSERVER_COUNTER = 0;
function data$1() {
    var refs = {};
    var refsByName = {};
    var inactiveRefs = {};
    // FIXME: Not sure of this one can be typed, circular type reference.
    var observers = [];
    return {
        id: '',
        refs: refs,
        refsByName: refsByName,
        observers: observers,
        inactiveRefs: inactiveRefs
    };
}
var ValidationObserver = vue__WEBPACK_IMPORTED_MODULE_0__["default"].extend({
    name: 'ValidationObserver',
    provide: function () {
        return {
            $_veeObserver: this
        };
    },
    inject: {
        $_veeObserver: {
            from: '$_veeObserver',
            default: function () {
                if (!this.$vnode.context.$_veeObserver) {
                    return null;
                }
                return this.$vnode.context.$_veeObserver;
            }
        }
    },
    props: {
        tag: {
            type: String,
            default: 'span'
        },
        vid: {
            type: String,
            default: function () {
                return "obs_" + OBSERVER_COUNTER++;
            }
        },
        slim: {
            type: Boolean,
            default: false
        },
        disabled: {
            type: Boolean,
            default: false
        }
    },
    data: data$1,
    computed: {
        ctx: function () {
            var _this = this;
            var ctx = {
                errors: {},
                passes: function (cb) {
                    return _this.validate().then(function (result) {
                        if (result) {
                            return cb();
                        }
                    });
                },
                validate: function () {
                    var args = [];
                    for (var _i = 0; _i < arguments.length; _i++) {
                        args[_i] = arguments[_i];
                    }
                    return _this.validate.apply(_this, args);
                },
                reset: function () { return _this.reset(); }
            };
            return __spreadArrays(values(this.refs), Object.keys(this.inactiveRefs).map(function (key) {
                return {
                    vid: key,
                    flags: _this.inactiveRefs[key].flags,
                    messages: _this.inactiveRefs[key].errors
                };
            }), this.observers).reduce(function (acc, provider) {
                Object.keys(flagMergingStrategy).forEach(function (flag) {
                    var flags = provider.flags || provider.ctx;
                    if (!(flag in acc)) {
                        acc[flag] = flags[flag];
                        return;
                    }
                    acc[flag] = mergeFlags(acc[flag], flags[flag], flag);
                });
                acc.errors[provider.id] =
                    provider.messages ||
                        values(provider.ctx.errors).reduce(function (errs, obsErrors) {
                            return errs.concat(obsErrors);
                        }, []);
                return acc;
            }, ctx);
        }
    },
    created: function () {
        this.id = this.vid;
        if (this.$_veeObserver) {
            this.$_veeObserver.subscribe(this, 'observer');
        }
    },
    activated: function () {
        if (this.$_veeObserver) {
            this.$_veeObserver.subscribe(this, 'observer');
        }
    },
    deactivated: function () {
        if (this.$_veeObserver) {
            this.$_veeObserver.unsubscribe(this.id, 'observer');
        }
    },
    beforeDestroy: function () {
        if (this.$_veeObserver) {
            this.$_veeObserver.unsubscribe(this.id, 'observer');
        }
    },
    render: function (h) {
        var children = normalizeChildren(this, this.ctx);
        return this.slim && children.length <= 1 ? children[0] : h(this.tag, { on: this.$listeners }, children);
    },
    methods: {
        subscribe: function (subscriber, kind) {
            var _a, _b;
            if (kind === void 0) { kind = 'provider'; }
            if (kind === 'observer') {
                this.observers.push(subscriber);
                return;
            }
            this.refs = __assign(__assign({}, this.refs), (_a = {}, _a[subscriber.id] = subscriber, _a));
            this.refsByName = __assign(__assign({}, this.refsByName), (_b = {}, _b[subscriber.name] = subscriber, _b));
            if (subscriber.persist) {
                this.restoreProviderState(subscriber);
            }
        },
        unsubscribe: function (id, kind) {
            if (kind === void 0) { kind = 'provider'; }
            if (kind === 'provider') {
                this.removeProvider(id);
                return;
            }
            var idx = findIndex(this.observers, function (o) { return o.id === id; });
            if (idx !== -1) {
                this.observers.splice(idx, 1);
            }
        },
        validate: function (_a) {
            var _b = (_a === void 0 ? {} : _a).silent, silent = _b === void 0 ? false : _b;
            return __awaiter(this, void 0, void 0, function () {
                var results;
                return __generator(this, function (_c) {
                    switch (_c.label) {
                        case 0: return [4 /*yield*/, Promise.all(__spreadArrays(values(this.refs)
                                .filter(function (r) { return !r.disabled; })
                                .map(function (ref) { return ref[silent ? 'validateSilent' : 'validate']().then(function (r) { return r.valid; }); }), this.observers.filter(function (o) { return !o.disabled; }).map(function (obs) { return obs.validate({ silent: silent }); })))];
                        case 1:
                            results = _c.sent();
                            return [2 /*return*/, results.every(function (r) { return r; })];
                    }
                });
            });
        },
        reset: function () {
            var _this = this;
            Object.keys(this.inactiveRefs).forEach(function (key) {
                _this.$delete(_this.inactiveRefs, key);
            });
            return __spreadArrays(values(this.refs), this.observers).forEach(function (ref) { return ref.reset(); });
        },
        restoreProviderState: function (provider) {
            var id = provider.id;
            var state = this.inactiveRefs[id];
            if (!state) {
                return;
            }
            provider.setFlags(state.flags);
            provider.applyResult(state);
            this.$delete(this.inactiveRefs, provider.id);
        },
        removeProvider: function (id) {
            var provider = this.refs[id];
            if (!provider) {
                // FIXME: inactive refs are not being cleaned up.
                return;
            }
            if (provider.persist) {
                /* istanbul ignore next */
                if (true) {
                    if (id.indexOf('_vee_') === 0) {
                        warn('Please provide a `vid` or a `name` prop when using `persist`, there might be unexpected issues otherwise.');
                    }
                }
                // save it for the next time.
                this.inactiveRefs[id] = {
                    flags: provider.flags,
                    errors: provider.messages,
                    failedRules: provider.failedRules
                };
            }
            this.$delete(this.refs, id);
            this.$delete(this.refsByName, provider.name);
        },
        setErrors: function (errors) {
            var _this = this;
            Object.keys(errors).forEach(function (key) {
                var provider = _this.refs[key] || _this.refsByName[key];
                if (!provider)
                    return;
                provider.setErrors(errors[key] || []);
            });
            this.observers.forEach(function (observer) {
                observer.setErrors(errors);
            });
        }
    }
});

function withValidation(component, mapProps) {
    if (mapProps === void 0) { mapProps = identity; }
    var options = 'options' in component ? component.options : component;
    var providerOpts = ValidationProvider.options;
    var hoc = {
        name: (options.name || 'AnonymousHoc') + "WithValidation",
        props: __assign({}, providerOpts.props),
        data: providerOpts.data,
        computed: __assign({}, providerOpts.computed),
        methods: __assign({}, providerOpts.methods),
        beforeDestroy: providerOpts.beforeDestroy,
        inject: providerOpts.inject
    };
    var eventName = (options.model && options.model.event) || 'input';
    hoc.render = function (h) {
        var _a;
        this.registerField();
        var vctx = createValidationCtx(this);
        var listeners = __assign({}, this.$listeners);
        var model = findModel(this.$vnode);
        this._inputEventName = this._inputEventName || getInputEventName(this.$vnode, model);
        var value = findValue(this.$vnode);
        onRenderUpdate(this, value && value.value);
        var _b = createCommonHandlers(this), onInput = _b.onInput, onBlur = _b.onBlur, onValidate = _b.onValidate;
        mergeVNodeListeners(listeners, eventName, onInput);
        mergeVNodeListeners(listeners, 'blur', onBlur);
        this.normalizedEvents.forEach(function (evt) {
            mergeVNodeListeners(listeners, evt, onValidate);
        });
        // Props are any attrs not associated with ValidationProvider Plus the model prop.
        // WARNING: Accidental prop overwrite will probably happen.
        var prop = (findModelConfig(this.$vnode) || { prop: 'value' }).prop;
        var props = __assign(__assign(__assign({}, this.$attrs), (_a = {}, _a[prop] = model && model.value, _a)), mapProps(vctx));
        return h(options, {
            attrs: this.$attrs,
            props: props,
            on: listeners
        }, normalizeSlots(this.$slots, this.$vnode.context));
    };
    return hoc;
}

var version = '3.0.11';




/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/global/TheModalFullScreen.vue?vue&type=template&id=1e69cc63&":
/*!********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./src/vue/partial/global/TheModalFullScreen.vue?vue&type=template&id=1e69cc63& ***!
  \********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "modal fade modal-fs", attrs: { id: "modal-fs" } },
    [
      _c("div", { staticClass: "modal-dialog", attrs: { role: "document" } }, [
        _c("div", { staticClass: "modal-content" }, [
          _c("div", { staticClass: "modal-body" }, [
            _c("div", { staticClass: "container text-center" }, [
              _c("div", { staticClass: "row full-height align-items-center" }, [
                _c("div", { staticClass: "col-md-5 m-h-auto" }, [
                  _c("div", { staticClass: "p-h-30 p-b-50" }, [
                    _vm._m(0),
                    _vm._v(" "),
                    _c("h1", { staticClass: "text-thin m-v-15" }, [
                      _vm._v(_vm._s(_vm.title))
                    ]),
                    _vm._v(" "),
                    _c("p", { staticClass: "m-b-25 lead" }, [
                      _vm._v(_vm._s(_vm.content))
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "text-center" }, [
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-default btn-lg",
                          on: {
                            click: function($event) {
                              $event.preventDefault()
                              return _vm.confirm($event)
                            }
                          }
                        },
                        [_vm._v("Confirm\n                  ")]
                      )
                    ])
                  ])
                ])
              ])
            ]),
            _vm._v(" "),
            _c(
              "a",
              {
                staticClass: "modal-close",
                attrs: { href: "#", "data-dismiss": "modal" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                    return _vm.confirm($event)
                  }
                }
              },
              [_c("i", { staticClass: "ti-close" })]
            )
          ])
        ])
      ])
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "text-center font-size-70" }, [
      _c("i", {
        staticClass:
          "mdi mdi-checkbox-marked-circle-outline icon-gradient-success"
      })
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/user/App.vue?vue&type=template&id=9be03d66&":
/*!***************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./src/vue/partial/user/App.vue?vue&type=template&id=9be03d66& ***!
  \***************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("router-view")
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/user/UserAddSection.vue?vue&type=template&id=dfe9829a&":
/*!**************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./src/vue/partial/user/UserAddSection.vue?vue&type=template&id=dfe9829a& ***!
  \**************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container-fluid" }, [
    _c("div", { staticClass: "page-header" }, [
      _c("h2", { staticClass: "header-title" }, [_vm._v("Người dùng")]),
      _vm._v(" "),
      _c("div", { staticClass: "header-sub-title" }, [
        _c("nav", { staticClass: "breadcrumb breadcrumb-dash" }, [
          _vm._m(0),
          _vm._v(" "),
          _c(
            "a",
            {
              staticClass: "breadcrumb-item",
              attrs: { href: "#" },
              on: {
                click: function($event) {
                  $event.preventDefault()
                  return _vm.goRead()
                }
              }
            },
            [_c("i", { staticClass: "ti-user p-r-5" }), _vm._v("Người dùng")]
          ),
          _vm._v(" "),
          _vm._m(1)
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "_header-button" }, [
        _c(
          "button",
          {
            staticClass: "btn btn-gradient-success",
            on: {
              click: function($event) {
                $event.preventDefault()
                return _vm.goCreate()
              }
            }
          },
          [_vm._v("Tạo người dùng\n      ")]
        )
      ])
    ]),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "card" },
      [
        _c("validation-observer", {
          ref: "observer",
          staticClass: "card-body",
          attrs: { tag: "div" },
          scopedSlots: _vm._u([
            {
              key: "default",
              fn: function(ref) {
                var invalid = ref.invalid
                return [
                  _c(
                    "div",
                    {
                      staticClass: "row m-v-30",
                      on: {
                        keyup: function($event) {
                          if (
                            !$event.type.indexOf("key") &&
                            _vm._k(
                              $event.keyCode,
                              "enter",
                              13,
                              $event.key,
                              "Enter"
                            )
                          ) {
                            return null
                          }
                          return _vm.update(invalid)
                        }
                      }
                    },
                    [
                      _c("div", { staticClass: "col-sm-3" }, [
                        _c("img", {
                          staticClass: "img-fluid d-block mx-auto m-b-30",
                          staticStyle: { width: "200px", height: "200px" },
                          attrs: { src: _vm.profileImage, alt: "" }
                        }),
                        _vm._v(" "),
                        _c("input", {
                          staticClass: "form-control",
                          attrs: { type: "file" },
                          on: { change: _vm.upload }
                        })
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-4 text-center text-sm-left" },
                        [
                          _c("validation-provider", {
                            staticClass: "form-group",
                            attrs: {
                              name: "Tên người dùng",
                              rules: "required",
                              tag: "div"
                            },
                            scopedSlots: _vm._u(
                              [
                                {
                                  key: "default",
                                  fn: function(ref) {
                                    var errors = ref.errors
                                    var valid = ref.valid
                                    return [
                                      _c(
                                        "label",
                                        {
                                          staticClass: "control-label",
                                          attrs: { for: "icon-input" }
                                        },
                                        [_vm._v("Name:")]
                                      ),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "icon-input" }, [
                                        _c("i", {
                                          staticClass: "mdi mdi-account"
                                        }),
                                        _vm._v(" "),
                                        _c("input", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: _vm.frmData.fullname,
                                              expression: "frmData.fullname"
                                            }
                                          ],
                                          staticClass: "form-control",
                                          class: {
                                            error:
                                              _vm.flag.isValidating && !valid
                                          },
                                          attrs: {
                                            type: "text",
                                            placeholder: "Full name..."
                                          },
                                          domProps: {
                                            value: _vm.frmData.fullname
                                          },
                                          on: {
                                            input: function($event) {
                                              if ($event.target.composing) {
                                                return
                                              }
                                              _vm.$set(
                                                _vm.frmData,
                                                "fullname",
                                                $event.target.value
                                              )
                                            }
                                          }
                                        })
                                      ]),
                                      _vm._v(" "),
                                      _c(
                                        "label",
                                        {
                                          directives: [
                                            {
                                              name: "show",
                                              rawName: "v-show",
                                              value:
                                                _vm.flag.isValidating && !valid,
                                              expression:
                                                "flag.isValidating && !valid"
                                            }
                                          ],
                                          staticClass: "error"
                                        },
                                        [_vm._v(_vm._s(errors[0]))]
                                      )
                                    ]
                                  }
                                }
                              ],
                              null,
                              true
                            )
                          }),
                          _vm._v(" "),
                          _c("validation-provider", {
                            staticClass: "form-group",
                            attrs: {
                              name: "Email",
                              rules: "required|email",
                              tag: "div"
                            },
                            scopedSlots: _vm._u(
                              [
                                {
                                  key: "default",
                                  fn: function(ref) {
                                    var errors = ref.errors
                                    var valid = ref.valid
                                    return [
                                      _c(
                                        "label",
                                        {
                                          staticClass: "control-label",
                                          attrs: { for: "icon-input" }
                                        },
                                        [_vm._v("Email:")]
                                      ),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "icon-input" }, [
                                        _c("i", {
                                          staticClass: "mdi mdi-email-outline"
                                        }),
                                        _vm._v(" "),
                                        _c("input", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: _vm.frmData.email,
                                              expression: "frmData.email"
                                            }
                                          ],
                                          staticClass: "form-control",
                                          class: {
                                            error:
                                              _vm.flag.isValidating && !valid
                                          },
                                          attrs: {
                                            type: "text",
                                            placeholder: "Email..."
                                          },
                                          domProps: {
                                            value: _vm.frmData.email
                                          },
                                          on: {
                                            input: function($event) {
                                              if ($event.target.composing) {
                                                return
                                              }
                                              _vm.$set(
                                                _vm.frmData,
                                                "email",
                                                $event.target.value
                                              )
                                            }
                                          }
                                        })
                                      ]),
                                      _vm._v(" "),
                                      _c(
                                        "label",
                                        {
                                          directives: [
                                            {
                                              name: "show",
                                              rawName: "v-show",
                                              value:
                                                _vm.flag.isValidating && !valid,
                                              expression:
                                                "flag.isValidating && !valid"
                                            }
                                          ],
                                          staticClass: "error"
                                        },
                                        [_vm._v(_vm._s(errors[0]))]
                                      )
                                    ]
                                  }
                                }
                              ],
                              null,
                              true
                            )
                          }),
                          _vm._v(" "),
                          _c("div", { staticClass: "checkbox" }, [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.frmData.is_actived,
                                  expression: "frmData.is_actived"
                                }
                              ],
                              attrs: {
                                id: "checkbox1",
                                type: "checkbox",
                                "true-value": "1",
                                "false-value": "0"
                              },
                              domProps: {
                                checked: Array.isArray(_vm.frmData.is_actived)
                                  ? _vm._i(_vm.frmData.is_actived, null) > -1
                                  : _vm._q(_vm.frmData.is_actived, "1")
                              },
                              on: {
                                change: function($event) {
                                  var $$a = _vm.frmData.is_actived,
                                    $$el = $event.target,
                                    $$c = $$el.checked ? "1" : "0"
                                  if (Array.isArray($$a)) {
                                    var $$v = null,
                                      $$i = _vm._i($$a, $$v)
                                    if ($$el.checked) {
                                      $$i < 0 &&
                                        _vm.$set(
                                          _vm.frmData,
                                          "is_actived",
                                          $$a.concat([$$v])
                                        )
                                    } else {
                                      $$i > -1 &&
                                        _vm.$set(
                                          _vm.frmData,
                                          "is_actived",
                                          $$a
                                            .slice(0, $$i)
                                            .concat($$a.slice($$i + 1))
                                        )
                                    }
                                  } else {
                                    _vm.$set(_vm.frmData, "is_actived", $$c)
                                  }
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c("label", { attrs: { for: "checkbox1" } }, [
                              _vm._v("Kích hoạt")
                            ])
                          ])
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col" },
                        [
                          _c("validation-provider", {
                            staticClass: "form-group",
                            attrs: {
                              name: "SĐT",
                              rules: "required",
                              tag: "div"
                            },
                            scopedSlots: _vm._u(
                              [
                                {
                                  key: "default",
                                  fn: function(ref) {
                                    var errors = ref.errors
                                    var valid = ref.valid
                                    return [
                                      _c(
                                        "label",
                                        {
                                          staticClass: "control-label",
                                          attrs: { for: "icon-input" }
                                        },
                                        [_vm._v("Phone:")]
                                      ),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "icon-input" }, [
                                        _c("i", {
                                          staticClass: "mdi mdi-cellphone"
                                        }),
                                        _vm._v(" "),
                                        _c("input", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: _vm.frmData.phone,
                                              expression: "frmData.phone"
                                            }
                                          ],
                                          staticClass: "form-control",
                                          class: {
                                            error:
                                              _vm.flag.isValidating && !valid
                                          },
                                          attrs: {
                                            type: "text",
                                            placeholder: "Phone..."
                                          },
                                          domProps: {
                                            value: _vm.frmData.phone
                                          },
                                          on: {
                                            input: function($event) {
                                              if ($event.target.composing) {
                                                return
                                              }
                                              _vm.$set(
                                                _vm.frmData,
                                                "phone",
                                                $event.target.value
                                              )
                                            }
                                          }
                                        })
                                      ]),
                                      _vm._v(" "),
                                      _c(
                                        "label",
                                        {
                                          directives: [
                                            {
                                              name: "show",
                                              rawName: "v-show",
                                              value:
                                                _vm.flag.isValidating && !valid,
                                              expression:
                                                "flag.isValidating && !valid"
                                            }
                                          ],
                                          staticClass: "error"
                                        },
                                        [_vm._v(_vm._s(errors[0]))]
                                      )
                                    ]
                                  }
                                }
                              ],
                              null,
                              true
                            ),
                            model: {
                              value: _vm.frmData.phone,
                              callback: function($$v) {
                                _vm.$set(_vm.frmData, "phone", $$v)
                              },
                              expression: "frmData.phone"
                            }
                          }),
                          _vm._v(" "),
                          _c("validation-provider", {
                            staticClass: "form-group",
                            attrs: {
                              name: "Địa chỉ",
                              rules: "required",
                              tag: "div"
                            },
                            scopedSlots: _vm._u(
                              [
                                {
                                  key: "default",
                                  fn: function(ref) {
                                    var errors = ref.errors
                                    var valid = ref.valid
                                    return [
                                      _c(
                                        "label",
                                        {
                                          staticClass: "control-label",
                                          attrs: { for: "icon-input" }
                                        },
                                        [_vm._v("Địa chỉ:")]
                                      ),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "icon-input" }, [
                                        _c("i", {
                                          staticClass: "mdi mdi-account-box"
                                        }),
                                        _vm._v(" "),
                                        _c("input", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: _vm.frmData.address,
                                              expression: "frmData.address"
                                            }
                                          ],
                                          staticClass: "form-control",
                                          class: {
                                            error:
                                              _vm.flag.isValidating && !valid
                                          },
                                          attrs: {
                                            type: "text",
                                            placeholder: "Address..."
                                          },
                                          domProps: {
                                            value: _vm.frmData.address
                                          },
                                          on: {
                                            input: function($event) {
                                              if ($event.target.composing) {
                                                return
                                              }
                                              _vm.$set(
                                                _vm.frmData,
                                                "address",
                                                $event.target.value
                                              )
                                            }
                                          }
                                        })
                                      ]),
                                      _vm._v(" "),
                                      _c(
                                        "label",
                                        {
                                          directives: [
                                            {
                                              name: "show",
                                              rawName: "v-show",
                                              value:
                                                _vm.flag.isValidating && !valid,
                                              expression:
                                                "flag.isValidating && !valid"
                                            }
                                          ],
                                          staticClass: "error"
                                        },
                                        [_vm._v(_vm._s(errors[0]))]
                                      )
                                    ]
                                  }
                                }
                              ],
                              null,
                              true
                            )
                          }),
                          _vm._v(" "),
                          _c("div", { staticClass: "form-group" }, [
                            _c(
                              "label",
                              {
                                staticClass: "control-label",
                                attrs: { for: "icon-input" }
                              },
                              [_vm._v("Password:")]
                            ),
                            _vm._v(" "),
                            _c("div", { staticClass: "icon-input" }, [
                              _c("i", { staticClass: "mdi mdi-account-key" }),
                              _vm._v(" "),
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.frmData.password,
                                    expression: "frmData.password"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: {
                                  type: "password",
                                  placeholder: "Password..."
                                },
                                domProps: { value: _vm.frmData.password },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.frmData,
                                      "password",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "m-t-25" }, [
                            _c(
                              "button",
                              {
                                staticClass: "btn btn-success",
                                attrs: {
                                  id: "save",
                                  type: "button",
                                  disabled: _vm.flag.isUpdating
                                },
                                on: {
                                  click: function($event) {
                                    $event.preventDefault()
                                    return _vm.update(invalid)
                                  }
                                }
                              },
                              [
                                _vm.flag.isUpdating
                                  ? [
                                      _c("i", {
                                        staticClass:
                                          "fa fa-spinner fa-pulse fa-fw m-r-5"
                                      }),
                                      _vm._v("Updating\n              ")
                                    ]
                                  : [_vm._v("Update")]
                              ],
                              2
                            ),
                            _vm._v(" "),
                            _c(
                              "button",
                              {
                                staticClass: "btn btn-default",
                                attrs: { id: "edit", type: "button" },
                                on: {
                                  click: function($event) {
                                    $event.preventDefault()
                                    return _vm.back($event)
                                  }
                                }
                              },
                              [
                                _c("i", { staticClass: "ti-back-left p-r-10" }),
                                _vm._v("Back\n            ")
                              ]
                            )
                          ])
                        ],
                        1
                      )
                    ]
                  )
                ]
              }
            }
          ])
        })
      ],
      1
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "a",
      { staticClass: "breadcrumb-item", attrs: { href: "/backend" } },
      [_c("i", { staticClass: "ti-home p-r-5" }), _vm._v("Hệ thống")]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "breadcrumb-item active" }, [
      _c("i", { staticClass: "ti-pencil-alt p-r-5" }),
      _vm._v("Tạo người dùng")
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/user/UserEditSection.vue?vue&type=template&id=5594325c&":
/*!***************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./src/vue/partial/user/UserEditSection.vue?vue&type=template&id=5594325c& ***!
  \***************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container-fluid" }, [
    _c("div", { staticClass: "page-header" }, [
      _c("h2", { staticClass: "header-title" }, [_vm._v("Người dùng")]),
      _vm._v(" "),
      _c("div", { staticClass: "header-sub-title" }, [
        _c("nav", { staticClass: "breadcrumb breadcrumb-dash" }, [
          _vm._m(0),
          _vm._v(" "),
          _c(
            "a",
            {
              staticClass: "breadcrumb-item",
              attrs: { href: "#" },
              on: {
                click: function($event) {
                  $event.preventDefault()
                  return _vm.goRead()
                }
              }
            },
            [_c("i", { staticClass: "ti-user p-r-5" }), _vm._v("Người dùng")]
          ),
          _vm._v(" "),
          _vm._m(1)
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "_header-button" }, [
        _c(
          "button",
          {
            staticClass: "btn btn-gradient-success",
            on: {
              click: function($event) {
                $event.preventDefault()
                return _vm.goCreate()
              }
            }
          },
          [_vm._v("Tạo người dùng\n      ")]
        )
      ])
    ]),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "card" },
      [
        _c("validation-observer", {
          ref: "observer",
          staticClass: "card-body",
          attrs: { tag: "div" },
          scopedSlots: _vm._u([
            {
              key: "default",
              fn: function(ref) {
                var invalid = ref.invalid
                return [
                  _c(
                    "div",
                    {
                      staticClass: "row m-v-30",
                      on: {
                        keyup: function($event) {
                          if (
                            !$event.type.indexOf("key") &&
                            _vm._k(
                              $event.keyCode,
                              "enter",
                              13,
                              $event.key,
                              "Enter"
                            )
                          ) {
                            return null
                          }
                          return _vm.update(invalid)
                        }
                      }
                    },
                    [
                      _c("div", { staticClass: "col-sm-3" }, [
                        _c("img", {
                          staticClass: "img-fluid d-block mx-auto m-b-30",
                          staticStyle: { width: "200px", height: "200px" },
                          attrs: { src: _vm.profileImage, alt: "" }
                        }),
                        _vm._v(" "),
                        _c("input", {
                          staticClass: "form-control",
                          attrs: { type: "file" },
                          on: { change: _vm.upload }
                        })
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-4 text-center text-sm-left" },
                        [
                          _c("validation-provider", {
                            staticClass: "form-group",
                            attrs: {
                              name: "Tên người dùng",
                              rules: "required",
                              tag: "div"
                            },
                            scopedSlots: _vm._u(
                              [
                                {
                                  key: "default",
                                  fn: function(ref) {
                                    var errors = ref.errors
                                    var valid = ref.valid
                                    return [
                                      _c(
                                        "label",
                                        {
                                          staticClass: "control-label",
                                          attrs: { for: "icon-input" }
                                        },
                                        [_vm._v("Name:")]
                                      ),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "icon-input" }, [
                                        _c("i", {
                                          staticClass: "mdi mdi-account"
                                        }),
                                        _vm._v(" "),
                                        _c("input", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: _vm.frmData.fullname,
                                              expression: "frmData.fullname"
                                            }
                                          ],
                                          staticClass: "form-control",
                                          class: {
                                            error:
                                              _vm.flag.isValidating && !valid
                                          },
                                          attrs: {
                                            type: "text",
                                            placeholder: "Full name..."
                                          },
                                          domProps: {
                                            value: _vm.frmData.fullname
                                          },
                                          on: {
                                            input: function($event) {
                                              if ($event.target.composing) {
                                                return
                                              }
                                              _vm.$set(
                                                _vm.frmData,
                                                "fullname",
                                                $event.target.value
                                              )
                                            }
                                          }
                                        })
                                      ]),
                                      _vm._v(" "),
                                      _c(
                                        "label",
                                        {
                                          directives: [
                                            {
                                              name: "show",
                                              rawName: "v-show",
                                              value:
                                                _vm.flag.isValidating && !valid,
                                              expression:
                                                "flag.isValidating && !valid"
                                            }
                                          ],
                                          staticClass: "error"
                                        },
                                        [_vm._v(_vm._s(errors[0]))]
                                      )
                                    ]
                                  }
                                }
                              ],
                              null,
                              true
                            )
                          }),
                          _vm._v(" "),
                          _c("validation-provider", {
                            staticClass: "form-group",
                            attrs: {
                              name: "Email",
                              rules: "required|email",
                              tag: "div"
                            },
                            scopedSlots: _vm._u(
                              [
                                {
                                  key: "default",
                                  fn: function(ref) {
                                    var errors = ref.errors
                                    var valid = ref.valid
                                    return [
                                      _c(
                                        "label",
                                        {
                                          staticClass: "control-label",
                                          attrs: { for: "icon-input" }
                                        },
                                        [_vm._v("Email:")]
                                      ),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "icon-input" }, [
                                        _c("i", {
                                          staticClass: "mdi mdi-email-outline"
                                        }),
                                        _vm._v(" "),
                                        _c("input", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: _vm.frmData.email,
                                              expression: "frmData.email"
                                            }
                                          ],
                                          staticClass: "form-control",
                                          class: {
                                            error:
                                              _vm.flag.isValidating && !valid
                                          },
                                          attrs: {
                                            type: "text",
                                            placeholder: "Email..."
                                          },
                                          domProps: {
                                            value: _vm.frmData.email
                                          },
                                          on: {
                                            input: function($event) {
                                              if ($event.target.composing) {
                                                return
                                              }
                                              _vm.$set(
                                                _vm.frmData,
                                                "email",
                                                $event.target.value
                                              )
                                            }
                                          }
                                        })
                                      ]),
                                      _vm._v(" "),
                                      _c(
                                        "label",
                                        {
                                          directives: [
                                            {
                                              name: "show",
                                              rawName: "v-show",
                                              value:
                                                _vm.flag.isValidating && !valid,
                                              expression:
                                                "flag.isValidating && !valid"
                                            }
                                          ],
                                          staticClass: "error"
                                        },
                                        [_vm._v(_vm._s(errors[0]))]
                                      )
                                    ]
                                  }
                                }
                              ],
                              null,
                              true
                            )
                          }),
                          _vm._v(" "),
                          _c("div", { staticClass: "checkbox" }, [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.frmData.is_actived,
                                  expression: "frmData.is_actived"
                                }
                              ],
                              attrs: {
                                id: "checkbox1",
                                type: "checkbox",
                                "true-value": "1",
                                "false-value": "0"
                              },
                              domProps: {
                                checked: Array.isArray(_vm.frmData.is_actived)
                                  ? _vm._i(_vm.frmData.is_actived, null) > -1
                                  : _vm._q(_vm.frmData.is_actived, "1")
                              },
                              on: {
                                change: function($event) {
                                  var $$a = _vm.frmData.is_actived,
                                    $$el = $event.target,
                                    $$c = $$el.checked ? "1" : "0"
                                  if (Array.isArray($$a)) {
                                    var $$v = null,
                                      $$i = _vm._i($$a, $$v)
                                    if ($$el.checked) {
                                      $$i < 0 &&
                                        _vm.$set(
                                          _vm.frmData,
                                          "is_actived",
                                          $$a.concat([$$v])
                                        )
                                    } else {
                                      $$i > -1 &&
                                        _vm.$set(
                                          _vm.frmData,
                                          "is_actived",
                                          $$a
                                            .slice(0, $$i)
                                            .concat($$a.slice($$i + 1))
                                        )
                                    }
                                  } else {
                                    _vm.$set(_vm.frmData, "is_actived", $$c)
                                  }
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c("label", { attrs: { for: "checkbox1" } }, [
                              _vm._v("Kích hoạt")
                            ])
                          ])
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col" },
                        [
                          _c("validation-provider", {
                            staticClass: "form-group",
                            attrs: {
                              name: "SĐT",
                              rules: "required",
                              tag: "div"
                            },
                            scopedSlots: _vm._u(
                              [
                                {
                                  key: "default",
                                  fn: function(ref) {
                                    var errors = ref.errors
                                    var valid = ref.valid
                                    return [
                                      _c(
                                        "label",
                                        {
                                          staticClass: "control-label",
                                          attrs: { for: "icon-input" }
                                        },
                                        [_vm._v("Phone:")]
                                      ),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "icon-input" }, [
                                        _c("i", {
                                          staticClass: "mdi mdi-cellphone"
                                        }),
                                        _vm._v(" "),
                                        _c("input", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: _vm.frmData.phone,
                                              expression: "frmData.phone"
                                            }
                                          ],
                                          staticClass: "form-control",
                                          class: {
                                            error:
                                              _vm.flag.isValidating && !valid
                                          },
                                          attrs: {
                                            type: "text",
                                            placeholder: "Phone..."
                                          },
                                          domProps: {
                                            value: _vm.frmData.phone
                                          },
                                          on: {
                                            input: function($event) {
                                              if ($event.target.composing) {
                                                return
                                              }
                                              _vm.$set(
                                                _vm.frmData,
                                                "phone",
                                                $event.target.value
                                              )
                                            }
                                          }
                                        })
                                      ]),
                                      _vm._v(" "),
                                      _c(
                                        "label",
                                        {
                                          directives: [
                                            {
                                              name: "show",
                                              rawName: "v-show",
                                              value:
                                                _vm.flag.isValidating && !valid,
                                              expression:
                                                "flag.isValidating && !valid"
                                            }
                                          ],
                                          staticClass: "error"
                                        },
                                        [_vm._v(_vm._s(errors[0]))]
                                      )
                                    ]
                                  }
                                }
                              ],
                              null,
                              true
                            ),
                            model: {
                              value: _vm.frmData.phone,
                              callback: function($$v) {
                                _vm.$set(_vm.frmData, "phone", $$v)
                              },
                              expression: "frmData.phone"
                            }
                          }),
                          _vm._v(" "),
                          _c("validation-provider", {
                            staticClass: "form-group",
                            attrs: {
                              name: "Địa chỉ",
                              rules: "required",
                              tag: "div"
                            },
                            scopedSlots: _vm._u(
                              [
                                {
                                  key: "default",
                                  fn: function(ref) {
                                    var errors = ref.errors
                                    var valid = ref.valid
                                    return [
                                      _c(
                                        "label",
                                        {
                                          staticClass: "control-label",
                                          attrs: { for: "icon-input" }
                                        },
                                        [_vm._v("Địa chỉ:")]
                                      ),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "icon-input" }, [
                                        _c("i", {
                                          staticClass: "mdi mdi-account-box"
                                        }),
                                        _vm._v(" "),
                                        _c("input", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: _vm.frmData.address,
                                              expression: "frmData.address"
                                            }
                                          ],
                                          staticClass: "form-control",
                                          class: {
                                            error:
                                              _vm.flag.isValidating && !valid
                                          },
                                          attrs: {
                                            type: "text",
                                            placeholder: "Address..."
                                          },
                                          domProps: {
                                            value: _vm.frmData.address
                                          },
                                          on: {
                                            input: function($event) {
                                              if ($event.target.composing) {
                                                return
                                              }
                                              _vm.$set(
                                                _vm.frmData,
                                                "address",
                                                $event.target.value
                                              )
                                            }
                                          }
                                        })
                                      ]),
                                      _vm._v(" "),
                                      _c(
                                        "label",
                                        {
                                          directives: [
                                            {
                                              name: "show",
                                              rawName: "v-show",
                                              value:
                                                _vm.flag.isValidating && !valid,
                                              expression:
                                                "flag.isValidating && !valid"
                                            }
                                          ],
                                          staticClass: "error"
                                        },
                                        [_vm._v(_vm._s(errors[0]))]
                                      )
                                    ]
                                  }
                                }
                              ],
                              null,
                              true
                            )
                          }),
                          _vm._v(" "),
                          _c("div", { staticClass: "form-group" }, [
                            _c(
                              "label",
                              {
                                staticClass: "control-label",
                                attrs: { for: "icon-input" }
                              },
                              [_vm._v("Password:")]
                            ),
                            _vm._v(" "),
                            _c("div", { staticClass: "icon-input" }, [
                              _c("i", { staticClass: "mdi mdi-account-key" }),
                              _vm._v(" "),
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.frmData.password,
                                    expression: "frmData.password"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: {
                                  type: "password",
                                  placeholder: "Password..."
                                },
                                domProps: { value: _vm.frmData.password },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.frmData,
                                      "password",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "m-t-25" }, [
                            _c(
                              "button",
                              {
                                staticClass: "btn btn-default",
                                attrs: { id: "edit", type: "button" },
                                on: {
                                  click: function($event) {
                                    $event.preventDefault()
                                    return _vm.back($event)
                                  }
                                }
                              },
                              [_vm._v("Back\n            ")]
                            ),
                            _vm._v(" "),
                            _c(
                              "button",
                              {
                                staticClass: "btn btn-success",
                                attrs: {
                                  id: "save",
                                  type: "button",
                                  disabled: _vm.flag.isUpdating
                                },
                                on: {
                                  click: function($event) {
                                    $event.preventDefault()
                                    return _vm.update(invalid)
                                  }
                                }
                              },
                              [
                                _vm.flag.isUpdating
                                  ? [
                                      _c("i", {
                                        staticClass:
                                          "fa fa-spinner fa-pulse fa-fw m-r-5"
                                      }),
                                      _vm._v("Updating\n              ")
                                    ]
                                  : [_vm._v("Update")]
                              ],
                              2
                            )
                          ])
                        ],
                        1
                      )
                    ]
                  )
                ]
              }
            }
          ])
        })
      ],
      1
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "a",
      { staticClass: "breadcrumb-item", attrs: { href: "/backend" } },
      [_c("i", { staticClass: "ti-home p-r-5" }), _vm._v("Hệ thống")]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "breadcrumb-item active" }, [
      _c("i", { staticClass: "ti-pencil-alt p-r-5" }),
      _vm._v("Cập nhật người dùng")
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/user/UserIndexSection.vue?vue&type=template&id=146a67bc&":
/*!****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./src/vue/partial/user/UserIndexSection.vue?vue&type=template&id=146a67bc& ***!
  \****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container-fluid" }, [
    _c("div", { staticClass: "page-header" }, [
      _c("h2", { staticClass: "header-title" }, [_vm._v("Người dùng")]),
      _vm._v(" "),
      _c("div", { staticClass: "header-sub-title" }, [
        _c("nav", { staticClass: "breadcrumb breadcrumb-dash" }, [
          _vm._m(0),
          _vm._v(" "),
          _c(
            "a",
            {
              staticClass: "breadcrumb-item",
              attrs: { href: "#" },
              on: {
                click: function($event) {
                  $event.preventDefault()
                }
              }
            },
            [_c("i", { staticClass: "ti-user p-r-5" }), _vm._v("Người dùng")]
          ),
          _vm._v(" "),
          _vm._m(1)
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "_header-button" }, [
        _c(
          "button",
          {
            staticClass: "btn btn-gradient-success",
            on: {
              click: function($event) {
                $event.preventDefault()
                return _vm.goCreate()
              }
            }
          },
          [_vm._v("Tạo người dùng\n      ")]
        )
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "card" }, [
      _c("div", { staticClass: "card-body" }, [_c("user-table")], 1)
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "a",
      { staticClass: "breadcrumb-item", attrs: { href: "/backend" } },
      [_c("i", { staticClass: "ti-home p-r-5" }), _vm._v("Hệ thống")]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "breadcrumb-item active" }, [
      _c("i", { staticClass: "ti-list p-r-5" }),
      _vm._v("Danh sách")
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/user/UserTable.vue?vue&type=template&id=75b32def&":
/*!*********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./src/vue/partial/user/UserTable.vue?vue&type=template&id=75b32def& ***!
  \*********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "table-overflow", attrs: { id: "user-table" } },
    [
      _c(
        "table",
        { staticClass: "table table-hover table-xl", attrs: { id: "dt-opt" } },
        [
          _vm._m(0),
          _vm._v(" "),
          _c(
            "tbody",
            [
              _vm._l(_vm.arrEntityList, function(objEntity, index) {
                return [
                  _c("user-table-row", {
                    attrs: { index: index, "obj-entity": objEntity }
                  })
                ]
              })
            ],
            2
          )
        ]
      )
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [
          _c("div", { staticClass: "checkbox p-0" }, [
            _c("input", {
              staticClass: "checkAll",
              attrs: { id: "selectable1", type: "checkbox", name: "checkAll" }
            }),
            _vm._v(" "),
            _c("label", { attrs: { for: "selectable1" } })
          ])
        ]),
        _vm._v(" "),
        _c("th", [_vm._v("FULLNAME")]),
        _vm._v(" "),
        _c("th", [_vm._v("EMAIL")]),
        _vm._v(" "),
        _c("th", [_vm._v("STATUS")]),
        _vm._v(" "),
        _c("th", [_vm._v("#")]),
        _vm._v(" "),
        _c("th")
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/user/UserTableRow.vue?vue&type=template&id=42c1b89b&":
/*!************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./src/vue/partial/user/UserTableRow.vue?vue&type=template&id=42c1b89b& ***!
  \************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("tr", [
    _vm._m(0),
    _vm._v(" "),
    _c("td", [
      _c("div", { staticClass: "list-media" }, [
        _c("div", { staticClass: "list-item" }, [
          _c("div", { staticClass: "media-img" }, [
            _c("img", { attrs: { src: _vm.profileImage, alt: "" } })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "info" }, [
            _c("span", { staticClass: "title" }, [
              _vm._v(_vm._s(_vm.objEntity.fullname))
            ]),
            _vm._v(" "),
            _c("span", { staticClass: "sub-title" }, [
              _vm._v("ID " + _vm._s(_vm.objEntity.user_id))
            ])
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _c("td", [_vm._v(_vm._s(_vm.objEntity.email))]),
    _vm._v(" "),
    _c(
      "td",
      [
        _vm.objEntity.is_actived == 1
          ? [
              _c(
                "span",
                { staticClass: "badge badge-pill badge-gradient-success" },
                [_vm._v("Đã kích hoạt")]
              )
            ]
          : [
              _c(
                "span",
                { staticClass: "badge badge-pill badge-gradient-danger" },
                [_vm._v("Chưa kích hoạt")]
              )
            ]
      ],
      2
    ),
    _vm._v(" "),
    _c("td", { staticClass: "text-center font-size-18" }, [
      _c(
        "a",
        {
          staticClass: "text-gray m-r-15",
          on: {
            click: function($event) {
              return _vm.goUpdate()
            }
          }
        },
        [_c("i", { staticClass: "ti-pencil" })]
      ),
      _vm._v(" "),
      _c(
        "a",
        {
          staticClass: "text-gray",
          on: {
            click: function($event) {
              return _vm.remove(_vm.objEntity.user_id)
            }
          }
        },
        [
          _c("i", {
            class: {
              "fa fa-spinner fa-pulse fa-fw": _vm.flag.isDeleting,
              "ti-trash": !_vm.flag.isDeleting
            }
          })
        ]
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("td", [
      _c("div", { staticClass: "checkbox" }, [
        _c("input", { attrs: { id: "selectable2", type: "checkbox" } }),
        _vm._v(" "),
        _c("label", { attrs: { for: "selectable2" } })
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-router/dist/vue-router.esm.js":
/*!********************************************************!*\
  !*** ./node_modules/vue-router/dist/vue-router.esm.js ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/*!
  * vue-router v3.1.3
  * (c) 2019 Evan You
  * @license MIT
  */
/*  */

function assert (condition, message) {
  if (!condition) {
    throw new Error(("[vue-router] " + message))
  }
}

function warn (condition, message) {
  if ( true && !condition) {
    typeof console !== 'undefined' && console.warn(("[vue-router] " + message));
  }
}

function isError (err) {
  return Object.prototype.toString.call(err).indexOf('Error') > -1
}

function isExtendedError (constructor, err) {
  return (
    err instanceof constructor ||
    // _name is to support IE9 too
    (err && (err.name === constructor.name || err._name === constructor._name))
  )
}

function extend (a, b) {
  for (var key in b) {
    a[key] = b[key];
  }
  return a
}

var View = {
  name: 'RouterView',
  functional: true,
  props: {
    name: {
      type: String,
      default: 'default'
    }
  },
  render: function render (_, ref) {
    var props = ref.props;
    var children = ref.children;
    var parent = ref.parent;
    var data = ref.data;

    // used by devtools to display a router-view badge
    data.routerView = true;

    // directly use parent context's createElement() function
    // so that components rendered by router-view can resolve named slots
    var h = parent.$createElement;
    var name = props.name;
    var route = parent.$route;
    var cache = parent._routerViewCache || (parent._routerViewCache = {});

    // determine current view depth, also check to see if the tree
    // has been toggled inactive but kept-alive.
    var depth = 0;
    var inactive = false;
    while (parent && parent._routerRoot !== parent) {
      var vnodeData = parent.$vnode && parent.$vnode.data;
      if (vnodeData) {
        if (vnodeData.routerView) {
          depth++;
        }
        if (vnodeData.keepAlive && parent._inactive) {
          inactive = true;
        }
      }
      parent = parent.$parent;
    }
    data.routerViewDepth = depth;

    // render previous view if the tree is inactive and kept-alive
    if (inactive) {
      return h(cache[name], data, children)
    }

    var matched = route.matched[depth];
    // render empty node if no matched route
    if (!matched) {
      cache[name] = null;
      return h()
    }

    var component = cache[name] = matched.components[name];

    // attach instance registration hook
    // this will be called in the instance's injected lifecycle hooks
    data.registerRouteInstance = function (vm, val) {
      // val could be undefined for unregistration
      var current = matched.instances[name];
      if (
        (val && current !== vm) ||
        (!val && current === vm)
      ) {
        matched.instances[name] = val;
      }
    }

    // also register instance in prepatch hook
    // in case the same component instance is reused across different routes
    ;(data.hook || (data.hook = {})).prepatch = function (_, vnode) {
      matched.instances[name] = vnode.componentInstance;
    };

    // register instance in init hook
    // in case kept-alive component be actived when routes changed
    data.hook.init = function (vnode) {
      if (vnode.data.keepAlive &&
        vnode.componentInstance &&
        vnode.componentInstance !== matched.instances[name]
      ) {
        matched.instances[name] = vnode.componentInstance;
      }
    };

    // resolve props
    var propsToPass = data.props = resolveProps(route, matched.props && matched.props[name]);
    if (propsToPass) {
      // clone to prevent mutation
      propsToPass = data.props = extend({}, propsToPass);
      // pass non-declared props as attrs
      var attrs = data.attrs = data.attrs || {};
      for (var key in propsToPass) {
        if (!component.props || !(key in component.props)) {
          attrs[key] = propsToPass[key];
          delete propsToPass[key];
        }
      }
    }

    return h(component, data, children)
  }
};

function resolveProps (route, config) {
  switch (typeof config) {
    case 'undefined':
      return
    case 'object':
      return config
    case 'function':
      return config(route)
    case 'boolean':
      return config ? route.params : undefined
    default:
      if (true) {
        warn(
          false,
          "props in \"" + (route.path) + "\" is a " + (typeof config) + ", " +
          "expecting an object, function or boolean."
        );
      }
  }
}

/*  */

var encodeReserveRE = /[!'()*]/g;
var encodeReserveReplacer = function (c) { return '%' + c.charCodeAt(0).toString(16); };
var commaRE = /%2C/g;

// fixed encodeURIComponent which is more conformant to RFC3986:
// - escapes [!'()*]
// - preserve commas
var encode = function (str) { return encodeURIComponent(str)
  .replace(encodeReserveRE, encodeReserveReplacer)
  .replace(commaRE, ','); };

var decode = decodeURIComponent;

function resolveQuery (
  query,
  extraQuery,
  _parseQuery
) {
  if ( extraQuery === void 0 ) extraQuery = {};

  var parse = _parseQuery || parseQuery;
  var parsedQuery;
  try {
    parsedQuery = parse(query || '');
  } catch (e) {
     true && warn(false, e.message);
    parsedQuery = {};
  }
  for (var key in extraQuery) {
    parsedQuery[key] = extraQuery[key];
  }
  return parsedQuery
}

function parseQuery (query) {
  var res = {};

  query = query.trim().replace(/^(\?|#|&)/, '');

  if (!query) {
    return res
  }

  query.split('&').forEach(function (param) {
    var parts = param.replace(/\+/g, ' ').split('=');
    var key = decode(parts.shift());
    var val = parts.length > 0
      ? decode(parts.join('='))
      : null;

    if (res[key] === undefined) {
      res[key] = val;
    } else if (Array.isArray(res[key])) {
      res[key].push(val);
    } else {
      res[key] = [res[key], val];
    }
  });

  return res
}

function stringifyQuery (obj) {
  var res = obj ? Object.keys(obj).map(function (key) {
    var val = obj[key];

    if (val === undefined) {
      return ''
    }

    if (val === null) {
      return encode(key)
    }

    if (Array.isArray(val)) {
      var result = [];
      val.forEach(function (val2) {
        if (val2 === undefined) {
          return
        }
        if (val2 === null) {
          result.push(encode(key));
        } else {
          result.push(encode(key) + '=' + encode(val2));
        }
      });
      return result.join('&')
    }

    return encode(key) + '=' + encode(val)
  }).filter(function (x) { return x.length > 0; }).join('&') : null;
  return res ? ("?" + res) : ''
}

/*  */

var trailingSlashRE = /\/?$/;

function createRoute (
  record,
  location,
  redirectedFrom,
  router
) {
  var stringifyQuery = router && router.options.stringifyQuery;

  var query = location.query || {};
  try {
    query = clone(query);
  } catch (e) {}

  var route = {
    name: location.name || (record && record.name),
    meta: (record && record.meta) || {},
    path: location.path || '/',
    hash: location.hash || '',
    query: query,
    params: location.params || {},
    fullPath: getFullPath(location, stringifyQuery),
    matched: record ? formatMatch(record) : []
  };
  if (redirectedFrom) {
    route.redirectedFrom = getFullPath(redirectedFrom, stringifyQuery);
  }
  return Object.freeze(route)
}

function clone (value) {
  if (Array.isArray(value)) {
    return value.map(clone)
  } else if (value && typeof value === 'object') {
    var res = {};
    for (var key in value) {
      res[key] = clone(value[key]);
    }
    return res
  } else {
    return value
  }
}

// the starting route that represents the initial state
var START = createRoute(null, {
  path: '/'
});

function formatMatch (record) {
  var res = [];
  while (record) {
    res.unshift(record);
    record = record.parent;
  }
  return res
}

function getFullPath (
  ref,
  _stringifyQuery
) {
  var path = ref.path;
  var query = ref.query; if ( query === void 0 ) query = {};
  var hash = ref.hash; if ( hash === void 0 ) hash = '';

  var stringify = _stringifyQuery || stringifyQuery;
  return (path || '/') + stringify(query) + hash
}

function isSameRoute (a, b) {
  if (b === START) {
    return a === b
  } else if (!b) {
    return false
  } else if (a.path && b.path) {
    return (
      a.path.replace(trailingSlashRE, '') === b.path.replace(trailingSlashRE, '') &&
      a.hash === b.hash &&
      isObjectEqual(a.query, b.query)
    )
  } else if (a.name && b.name) {
    return (
      a.name === b.name &&
      a.hash === b.hash &&
      isObjectEqual(a.query, b.query) &&
      isObjectEqual(a.params, b.params)
    )
  } else {
    return false
  }
}

function isObjectEqual (a, b) {
  if ( a === void 0 ) a = {};
  if ( b === void 0 ) b = {};

  // handle null value #1566
  if (!a || !b) { return a === b }
  var aKeys = Object.keys(a);
  var bKeys = Object.keys(b);
  if (aKeys.length !== bKeys.length) {
    return false
  }
  return aKeys.every(function (key) {
    var aVal = a[key];
    var bVal = b[key];
    // check nested equality
    if (typeof aVal === 'object' && typeof bVal === 'object') {
      return isObjectEqual(aVal, bVal)
    }
    return String(aVal) === String(bVal)
  })
}

function isIncludedRoute (current, target) {
  return (
    current.path.replace(trailingSlashRE, '/').indexOf(
      target.path.replace(trailingSlashRE, '/')
    ) === 0 &&
    (!target.hash || current.hash === target.hash) &&
    queryIncludes(current.query, target.query)
  )
}

function queryIncludes (current, target) {
  for (var key in target) {
    if (!(key in current)) {
      return false
    }
  }
  return true
}

/*  */

function resolvePath (
  relative,
  base,
  append
) {
  var firstChar = relative.charAt(0);
  if (firstChar === '/') {
    return relative
  }

  if (firstChar === '?' || firstChar === '#') {
    return base + relative
  }

  var stack = base.split('/');

  // remove trailing segment if:
  // - not appending
  // - appending to trailing slash (last segment is empty)
  if (!append || !stack[stack.length - 1]) {
    stack.pop();
  }

  // resolve relative path
  var segments = relative.replace(/^\//, '').split('/');
  for (var i = 0; i < segments.length; i++) {
    var segment = segments[i];
    if (segment === '..') {
      stack.pop();
    } else if (segment !== '.') {
      stack.push(segment);
    }
  }

  // ensure leading slash
  if (stack[0] !== '') {
    stack.unshift('');
  }

  return stack.join('/')
}

function parsePath (path) {
  var hash = '';
  var query = '';

  var hashIndex = path.indexOf('#');
  if (hashIndex >= 0) {
    hash = path.slice(hashIndex);
    path = path.slice(0, hashIndex);
  }

  var queryIndex = path.indexOf('?');
  if (queryIndex >= 0) {
    query = path.slice(queryIndex + 1);
    path = path.slice(0, queryIndex);
  }

  return {
    path: path,
    query: query,
    hash: hash
  }
}

function cleanPath (path) {
  return path.replace(/\/\//g, '/')
}

var isarray = Array.isArray || function (arr) {
  return Object.prototype.toString.call(arr) == '[object Array]';
};

/**
 * Expose `pathToRegexp`.
 */
var pathToRegexp_1 = pathToRegexp;
var parse_1 = parse;
var compile_1 = compile;
var tokensToFunction_1 = tokensToFunction;
var tokensToRegExp_1 = tokensToRegExp;

/**
 * The main path matching regexp utility.
 *
 * @type {RegExp}
 */
var PATH_REGEXP = new RegExp([
  // Match escaped characters that would otherwise appear in future matches.
  // This allows the user to escape special characters that won't transform.
  '(\\\\.)',
  // Match Express-style parameters and un-named parameters with a prefix
  // and optional suffixes. Matches appear as:
  //
  // "/:test(\\d+)?" => ["/", "test", "\d+", undefined, "?", undefined]
  // "/route(\\d+)"  => [undefined, undefined, undefined, "\d+", undefined, undefined]
  // "/*"            => ["/", undefined, undefined, undefined, undefined, "*"]
  '([\\/.])?(?:(?:\\:(\\w+)(?:\\(((?:\\\\.|[^\\\\()])+)\\))?|\\(((?:\\\\.|[^\\\\()])+)\\))([+*?])?|(\\*))'
].join('|'), 'g');

/**
 * Parse a string for the raw tokens.
 *
 * @param  {string}  str
 * @param  {Object=} options
 * @return {!Array}
 */
function parse (str, options) {
  var tokens = [];
  var key = 0;
  var index = 0;
  var path = '';
  var defaultDelimiter = options && options.delimiter || '/';
  var res;

  while ((res = PATH_REGEXP.exec(str)) != null) {
    var m = res[0];
    var escaped = res[1];
    var offset = res.index;
    path += str.slice(index, offset);
    index = offset + m.length;

    // Ignore already escaped sequences.
    if (escaped) {
      path += escaped[1];
      continue
    }

    var next = str[index];
    var prefix = res[2];
    var name = res[3];
    var capture = res[4];
    var group = res[5];
    var modifier = res[6];
    var asterisk = res[7];

    // Push the current path onto the tokens.
    if (path) {
      tokens.push(path);
      path = '';
    }

    var partial = prefix != null && next != null && next !== prefix;
    var repeat = modifier === '+' || modifier === '*';
    var optional = modifier === '?' || modifier === '*';
    var delimiter = res[2] || defaultDelimiter;
    var pattern = capture || group;

    tokens.push({
      name: name || key++,
      prefix: prefix || '',
      delimiter: delimiter,
      optional: optional,
      repeat: repeat,
      partial: partial,
      asterisk: !!asterisk,
      pattern: pattern ? escapeGroup(pattern) : (asterisk ? '.*' : '[^' + escapeString(delimiter) + ']+?')
    });
  }

  // Match any characters still remaining.
  if (index < str.length) {
    path += str.substr(index);
  }

  // If the path exists, push it onto the end.
  if (path) {
    tokens.push(path);
  }

  return tokens
}

/**
 * Compile a string to a template function for the path.
 *
 * @param  {string}             str
 * @param  {Object=}            options
 * @return {!function(Object=, Object=)}
 */
function compile (str, options) {
  return tokensToFunction(parse(str, options))
}

/**
 * Prettier encoding of URI path segments.
 *
 * @param  {string}
 * @return {string}
 */
function encodeURIComponentPretty (str) {
  return encodeURI(str).replace(/[\/?#]/g, function (c) {
    return '%' + c.charCodeAt(0).toString(16).toUpperCase()
  })
}

/**
 * Encode the asterisk parameter. Similar to `pretty`, but allows slashes.
 *
 * @param  {string}
 * @return {string}
 */
function encodeAsterisk (str) {
  return encodeURI(str).replace(/[?#]/g, function (c) {
    return '%' + c.charCodeAt(0).toString(16).toUpperCase()
  })
}

/**
 * Expose a method for transforming tokens into the path function.
 */
function tokensToFunction (tokens) {
  // Compile all the tokens into regexps.
  var matches = new Array(tokens.length);

  // Compile all the patterns before compilation.
  for (var i = 0; i < tokens.length; i++) {
    if (typeof tokens[i] === 'object') {
      matches[i] = new RegExp('^(?:' + tokens[i].pattern + ')$');
    }
  }

  return function (obj, opts) {
    var path = '';
    var data = obj || {};
    var options = opts || {};
    var encode = options.pretty ? encodeURIComponentPretty : encodeURIComponent;

    for (var i = 0; i < tokens.length; i++) {
      var token = tokens[i];

      if (typeof token === 'string') {
        path += token;

        continue
      }

      var value = data[token.name];
      var segment;

      if (value == null) {
        if (token.optional) {
          // Prepend partial segment prefixes.
          if (token.partial) {
            path += token.prefix;
          }

          continue
        } else {
          throw new TypeError('Expected "' + token.name + '" to be defined')
        }
      }

      if (isarray(value)) {
        if (!token.repeat) {
          throw new TypeError('Expected "' + token.name + '" to not repeat, but received `' + JSON.stringify(value) + '`')
        }

        if (value.length === 0) {
          if (token.optional) {
            continue
          } else {
            throw new TypeError('Expected "' + token.name + '" to not be empty')
          }
        }

        for (var j = 0; j < value.length; j++) {
          segment = encode(value[j]);

          if (!matches[i].test(segment)) {
            throw new TypeError('Expected all "' + token.name + '" to match "' + token.pattern + '", but received `' + JSON.stringify(segment) + '`')
          }

          path += (j === 0 ? token.prefix : token.delimiter) + segment;
        }

        continue
      }

      segment = token.asterisk ? encodeAsterisk(value) : encode(value);

      if (!matches[i].test(segment)) {
        throw new TypeError('Expected "' + token.name + '" to match "' + token.pattern + '", but received "' + segment + '"')
      }

      path += token.prefix + segment;
    }

    return path
  }
}

/**
 * Escape a regular expression string.
 *
 * @param  {string} str
 * @return {string}
 */
function escapeString (str) {
  return str.replace(/([.+*?=^!:${}()[\]|\/\\])/g, '\\$1')
}

/**
 * Escape the capturing group by escaping special characters and meaning.
 *
 * @param  {string} group
 * @return {string}
 */
function escapeGroup (group) {
  return group.replace(/([=!:$\/()])/g, '\\$1')
}

/**
 * Attach the keys as a property of the regexp.
 *
 * @param  {!RegExp} re
 * @param  {Array}   keys
 * @return {!RegExp}
 */
function attachKeys (re, keys) {
  re.keys = keys;
  return re
}

/**
 * Get the flags for a regexp from the options.
 *
 * @param  {Object} options
 * @return {string}
 */
function flags (options) {
  return options.sensitive ? '' : 'i'
}

/**
 * Pull out keys from a regexp.
 *
 * @param  {!RegExp} path
 * @param  {!Array}  keys
 * @return {!RegExp}
 */
function regexpToRegexp (path, keys) {
  // Use a negative lookahead to match only capturing groups.
  var groups = path.source.match(/\((?!\?)/g);

  if (groups) {
    for (var i = 0; i < groups.length; i++) {
      keys.push({
        name: i,
        prefix: null,
        delimiter: null,
        optional: false,
        repeat: false,
        partial: false,
        asterisk: false,
        pattern: null
      });
    }
  }

  return attachKeys(path, keys)
}

/**
 * Transform an array into a regexp.
 *
 * @param  {!Array}  path
 * @param  {Array}   keys
 * @param  {!Object} options
 * @return {!RegExp}
 */
function arrayToRegexp (path, keys, options) {
  var parts = [];

  for (var i = 0; i < path.length; i++) {
    parts.push(pathToRegexp(path[i], keys, options).source);
  }

  var regexp = new RegExp('(?:' + parts.join('|') + ')', flags(options));

  return attachKeys(regexp, keys)
}

/**
 * Create a path regexp from string input.
 *
 * @param  {string}  path
 * @param  {!Array}  keys
 * @param  {!Object} options
 * @return {!RegExp}
 */
function stringToRegexp (path, keys, options) {
  return tokensToRegExp(parse(path, options), keys, options)
}

/**
 * Expose a function for taking tokens and returning a RegExp.
 *
 * @param  {!Array}          tokens
 * @param  {(Array|Object)=} keys
 * @param  {Object=}         options
 * @return {!RegExp}
 */
function tokensToRegExp (tokens, keys, options) {
  if (!isarray(keys)) {
    options = /** @type {!Object} */ (keys || options);
    keys = [];
  }

  options = options || {};

  var strict = options.strict;
  var end = options.end !== false;
  var route = '';

  // Iterate over the tokens and create our regexp string.
  for (var i = 0; i < tokens.length; i++) {
    var token = tokens[i];

    if (typeof token === 'string') {
      route += escapeString(token);
    } else {
      var prefix = escapeString(token.prefix);
      var capture = '(?:' + token.pattern + ')';

      keys.push(token);

      if (token.repeat) {
        capture += '(?:' + prefix + capture + ')*';
      }

      if (token.optional) {
        if (!token.partial) {
          capture = '(?:' + prefix + '(' + capture + '))?';
        } else {
          capture = prefix + '(' + capture + ')?';
        }
      } else {
        capture = prefix + '(' + capture + ')';
      }

      route += capture;
    }
  }

  var delimiter = escapeString(options.delimiter || '/');
  var endsWithDelimiter = route.slice(-delimiter.length) === delimiter;

  // In non-strict mode we allow a slash at the end of match. If the path to
  // match already ends with a slash, we remove it for consistency. The slash
  // is valid at the end of a path match, not in the middle. This is important
  // in non-ending mode, where "/test/" shouldn't match "/test//route".
  if (!strict) {
    route = (endsWithDelimiter ? route.slice(0, -delimiter.length) : route) + '(?:' + delimiter + '(?=$))?';
  }

  if (end) {
    route += '$';
  } else {
    // In non-ending mode, we need the capturing groups to match as much as
    // possible by using a positive lookahead to the end or next path segment.
    route += strict && endsWithDelimiter ? '' : '(?=' + delimiter + '|$)';
  }

  return attachKeys(new RegExp('^' + route, flags(options)), keys)
}

/**
 * Normalize the given path string, returning a regular expression.
 *
 * An empty array can be passed in for the keys, which will hold the
 * placeholder key descriptions. For example, using `/user/:id`, `keys` will
 * contain `[{ name: 'id', delimiter: '/', optional: false, repeat: false }]`.
 *
 * @param  {(string|RegExp|Array)} path
 * @param  {(Array|Object)=}       keys
 * @param  {Object=}               options
 * @return {!RegExp}
 */
function pathToRegexp (path, keys, options) {
  if (!isarray(keys)) {
    options = /** @type {!Object} */ (keys || options);
    keys = [];
  }

  options = options || {};

  if (path instanceof RegExp) {
    return regexpToRegexp(path, /** @type {!Array} */ (keys))
  }

  if (isarray(path)) {
    return arrayToRegexp(/** @type {!Array} */ (path), /** @type {!Array} */ (keys), options)
  }

  return stringToRegexp(/** @type {string} */ (path), /** @type {!Array} */ (keys), options)
}
pathToRegexp_1.parse = parse_1;
pathToRegexp_1.compile = compile_1;
pathToRegexp_1.tokensToFunction = tokensToFunction_1;
pathToRegexp_1.tokensToRegExp = tokensToRegExp_1;

/*  */

// $flow-disable-line
var regexpCompileCache = Object.create(null);

function fillParams (
  path,
  params,
  routeMsg
) {
  params = params || {};
  try {
    var filler =
      regexpCompileCache[path] ||
      (regexpCompileCache[path] = pathToRegexp_1.compile(path));

    // Fix #2505 resolving asterisk routes { name: 'not-found', params: { pathMatch: '/not-found' }}
    if (params.pathMatch) { params[0] = params.pathMatch; }

    return filler(params, { pretty: true })
  } catch (e) {
    if (true) {
      warn(false, ("missing param for " + routeMsg + ": " + (e.message)));
    }
    return ''
  } finally {
    // delete the 0 if it was added
    delete params[0];
  }
}

/*  */

function normalizeLocation (
  raw,
  current,
  append,
  router
) {
  var next = typeof raw === 'string' ? { path: raw } : raw;
  // named target
  if (next._normalized) {
    return next
  } else if (next.name) {
    return extend({}, raw)
  }

  // relative params
  if (!next.path && next.params && current) {
    next = extend({}, next);
    next._normalized = true;
    var params = extend(extend({}, current.params), next.params);
    if (current.name) {
      next.name = current.name;
      next.params = params;
    } else if (current.matched.length) {
      var rawPath = current.matched[current.matched.length - 1].path;
      next.path = fillParams(rawPath, params, ("path " + (current.path)));
    } else if (true) {
      warn(false, "relative params navigation requires a current route.");
    }
    return next
  }

  var parsedPath = parsePath(next.path || '');
  var basePath = (current && current.path) || '/';
  var path = parsedPath.path
    ? resolvePath(parsedPath.path, basePath, append || next.append)
    : basePath;

  var query = resolveQuery(
    parsedPath.query,
    next.query,
    router && router.options.parseQuery
  );

  var hash = next.hash || parsedPath.hash;
  if (hash && hash.charAt(0) !== '#') {
    hash = "#" + hash;
  }

  return {
    _normalized: true,
    path: path,
    query: query,
    hash: hash
  }
}

/*  */

// work around weird flow bug
var toTypes = [String, Object];
var eventTypes = [String, Array];

var noop = function () {};

var Link = {
  name: 'RouterLink',
  props: {
    to: {
      type: toTypes,
      required: true
    },
    tag: {
      type: String,
      default: 'a'
    },
    exact: Boolean,
    append: Boolean,
    replace: Boolean,
    activeClass: String,
    exactActiveClass: String,
    event: {
      type: eventTypes,
      default: 'click'
    }
  },
  render: function render (h) {
    var this$1 = this;

    var router = this.$router;
    var current = this.$route;
    var ref = router.resolve(
      this.to,
      current,
      this.append
    );
    var location = ref.location;
    var route = ref.route;
    var href = ref.href;

    var classes = {};
    var globalActiveClass = router.options.linkActiveClass;
    var globalExactActiveClass = router.options.linkExactActiveClass;
    // Support global empty active class
    var activeClassFallback =
      globalActiveClass == null ? 'router-link-active' : globalActiveClass;
    var exactActiveClassFallback =
      globalExactActiveClass == null
        ? 'router-link-exact-active'
        : globalExactActiveClass;
    var activeClass =
      this.activeClass == null ? activeClassFallback : this.activeClass;
    var exactActiveClass =
      this.exactActiveClass == null
        ? exactActiveClassFallback
        : this.exactActiveClass;

    var compareTarget = route.redirectedFrom
      ? createRoute(null, normalizeLocation(route.redirectedFrom), null, router)
      : route;

    classes[exactActiveClass] = isSameRoute(current, compareTarget);
    classes[activeClass] = this.exact
      ? classes[exactActiveClass]
      : isIncludedRoute(current, compareTarget);

    var handler = function (e) {
      if (guardEvent(e)) {
        if (this$1.replace) {
          router.replace(location, noop);
        } else {
          router.push(location, noop);
        }
      }
    };

    var on = { click: guardEvent };
    if (Array.isArray(this.event)) {
      this.event.forEach(function (e) {
        on[e] = handler;
      });
    } else {
      on[this.event] = handler;
    }

    var data = { class: classes };

    var scopedSlot =
      !this.$scopedSlots.$hasNormal &&
      this.$scopedSlots.default &&
      this.$scopedSlots.default({
        href: href,
        route: route,
        navigate: handler,
        isActive: classes[activeClass],
        isExactActive: classes[exactActiveClass]
      });

    if (scopedSlot) {
      if (scopedSlot.length === 1) {
        return scopedSlot[0]
      } else if (scopedSlot.length > 1 || !scopedSlot.length) {
        if (true) {
          warn(
            false,
            ("RouterLink with to=\"" + (this.props.to) + "\" is trying to use a scoped slot but it didn't provide exactly one child.")
          );
        }
        return scopedSlot.length === 0 ? h() : h('span', {}, scopedSlot)
      }
    }

    if (this.tag === 'a') {
      data.on = on;
      data.attrs = { href: href };
    } else {
      // find the first <a> child and apply listener and href
      var a = findAnchor(this.$slots.default);
      if (a) {
        // in case the <a> is a static node
        a.isStatic = false;
        var aData = (a.data = extend({}, a.data));
        aData.on = aData.on || {};
        // transform existing events in both objects into arrays so we can push later
        for (var event in aData.on) {
          var handler$1 = aData.on[event];
          if (event in on) {
            aData.on[event] = Array.isArray(handler$1) ? handler$1 : [handler$1];
          }
        }
        // append new listeners for router-link
        for (var event$1 in on) {
          if (event$1 in aData.on) {
            // on[event] is always a function
            aData.on[event$1].push(on[event$1]);
          } else {
            aData.on[event$1] = handler;
          }
        }

        var aAttrs = (a.data.attrs = extend({}, a.data.attrs));
        aAttrs.href = href;
      } else {
        // doesn't have <a> child, apply listener to self
        data.on = on;
      }
    }

    return h(this.tag, data, this.$slots.default)
  }
};

function guardEvent (e) {
  // don't redirect with control keys
  if (e.metaKey || e.altKey || e.ctrlKey || e.shiftKey) { return }
  // don't redirect when preventDefault called
  if (e.defaultPrevented) { return }
  // don't redirect on right click
  if (e.button !== undefined && e.button !== 0) { return }
  // don't redirect if `target="_blank"`
  if (e.currentTarget && e.currentTarget.getAttribute) {
    var target = e.currentTarget.getAttribute('target');
    if (/\b_blank\b/i.test(target)) { return }
  }
  // this may be a Weex event which doesn't have this method
  if (e.preventDefault) {
    e.preventDefault();
  }
  return true
}

function findAnchor (children) {
  if (children) {
    var child;
    for (var i = 0; i < children.length; i++) {
      child = children[i];
      if (child.tag === 'a') {
        return child
      }
      if (child.children && (child = findAnchor(child.children))) {
        return child
      }
    }
  }
}

var _Vue;

function install (Vue) {
  if (install.installed && _Vue === Vue) { return }
  install.installed = true;

  _Vue = Vue;

  var isDef = function (v) { return v !== undefined; };

  var registerInstance = function (vm, callVal) {
    var i = vm.$options._parentVnode;
    if (isDef(i) && isDef(i = i.data) && isDef(i = i.registerRouteInstance)) {
      i(vm, callVal);
    }
  };

  Vue.mixin({
    beforeCreate: function beforeCreate () {
      if (isDef(this.$options.router)) {
        this._routerRoot = this;
        this._router = this.$options.router;
        this._router.init(this);
        Vue.util.defineReactive(this, '_route', this._router.history.current);
      } else {
        this._routerRoot = (this.$parent && this.$parent._routerRoot) || this;
      }
      registerInstance(this, this);
    },
    destroyed: function destroyed () {
      registerInstance(this);
    }
  });

  Object.defineProperty(Vue.prototype, '$router', {
    get: function get () { return this._routerRoot._router }
  });

  Object.defineProperty(Vue.prototype, '$route', {
    get: function get () { return this._routerRoot._route }
  });

  Vue.component('RouterView', View);
  Vue.component('RouterLink', Link);

  var strats = Vue.config.optionMergeStrategies;
  // use the same hook merging strategy for route hooks
  strats.beforeRouteEnter = strats.beforeRouteLeave = strats.beforeRouteUpdate = strats.created;
}

/*  */

var inBrowser = typeof window !== 'undefined';

/*  */

function createRouteMap (
  routes,
  oldPathList,
  oldPathMap,
  oldNameMap
) {
  // the path list is used to control path matching priority
  var pathList = oldPathList || [];
  // $flow-disable-line
  var pathMap = oldPathMap || Object.create(null);
  // $flow-disable-line
  var nameMap = oldNameMap || Object.create(null);

  routes.forEach(function (route) {
    addRouteRecord(pathList, pathMap, nameMap, route);
  });

  // ensure wildcard routes are always at the end
  for (var i = 0, l = pathList.length; i < l; i++) {
    if (pathList[i] === '*') {
      pathList.push(pathList.splice(i, 1)[0]);
      l--;
      i--;
    }
  }

  if (true) {
    // warn if routes do not include leading slashes
    var found = pathList
    // check for missing leading slash
      .filter(function (path) { return path && path.charAt(0) !== '*' && path.charAt(0) !== '/'; });

    if (found.length > 0) {
      var pathNames = found.map(function (path) { return ("- " + path); }).join('\n');
      warn(false, ("Non-nested routes must include a leading slash character. Fix the following routes: \n" + pathNames));
    }
  }

  return {
    pathList: pathList,
    pathMap: pathMap,
    nameMap: nameMap
  }
}

function addRouteRecord (
  pathList,
  pathMap,
  nameMap,
  route,
  parent,
  matchAs
) {
  var path = route.path;
  var name = route.name;
  if (true) {
    assert(path != null, "\"path\" is required in a route configuration.");
    assert(
      typeof route.component !== 'string',
      "route config \"component\" for path: " + (String(
        path || name
      )) + " cannot be a " + "string id. Use an actual component instead."
    );
  }

  var pathToRegexpOptions =
    route.pathToRegexpOptions || {};
  var normalizedPath = normalizePath(path, parent, pathToRegexpOptions.strict);

  if (typeof route.caseSensitive === 'boolean') {
    pathToRegexpOptions.sensitive = route.caseSensitive;
  }

  var record = {
    path: normalizedPath,
    regex: compileRouteRegex(normalizedPath, pathToRegexpOptions),
    components: route.components || { default: route.component },
    instances: {},
    name: name,
    parent: parent,
    matchAs: matchAs,
    redirect: route.redirect,
    beforeEnter: route.beforeEnter,
    meta: route.meta || {},
    props:
      route.props == null
        ? {}
        : route.components
          ? route.props
          : { default: route.props }
  };

  if (route.children) {
    // Warn if route is named, does not redirect and has a default child route.
    // If users navigate to this route by name, the default child will
    // not be rendered (GH Issue #629)
    if (true) {
      if (
        route.name &&
        !route.redirect &&
        route.children.some(function (child) { return /^\/?$/.test(child.path); })
      ) {
        warn(
          false,
          "Named Route '" + (route.name) + "' has a default child route. " +
            "When navigating to this named route (:to=\"{name: '" + (route.name) + "'\"), " +
            "the default child route will not be rendered. Remove the name from " +
            "this route and use the name of the default child route for named " +
            "links instead."
        );
      }
    }
    route.children.forEach(function (child) {
      var childMatchAs = matchAs
        ? cleanPath((matchAs + "/" + (child.path)))
        : undefined;
      addRouteRecord(pathList, pathMap, nameMap, child, record, childMatchAs);
    });
  }

  if (!pathMap[record.path]) {
    pathList.push(record.path);
    pathMap[record.path] = record;
  }

  if (route.alias !== undefined) {
    var aliases = Array.isArray(route.alias) ? route.alias : [route.alias];
    for (var i = 0; i < aliases.length; ++i) {
      var alias = aliases[i];
      if ( true && alias === path) {
        warn(
          false,
          ("Found an alias with the same value as the path: \"" + path + "\". You have to remove that alias. It will be ignored in development.")
        );
        // skip in dev to make it work
        continue
      }

      var aliasRoute = {
        path: alias,
        children: route.children
      };
      addRouteRecord(
        pathList,
        pathMap,
        nameMap,
        aliasRoute,
        parent,
        record.path || '/' // matchAs
      );
    }
  }

  if (name) {
    if (!nameMap[name]) {
      nameMap[name] = record;
    } else if ( true && !matchAs) {
      warn(
        false,
        "Duplicate named routes definition: " +
          "{ name: \"" + name + "\", path: \"" + (record.path) + "\" }"
      );
    }
  }
}

function compileRouteRegex (
  path,
  pathToRegexpOptions
) {
  var regex = pathToRegexp_1(path, [], pathToRegexpOptions);
  if (true) {
    var keys = Object.create(null);
    regex.keys.forEach(function (key) {
      warn(
        !keys[key.name],
        ("Duplicate param keys in route with path: \"" + path + "\"")
      );
      keys[key.name] = true;
    });
  }
  return regex
}

function normalizePath (
  path,
  parent,
  strict
) {
  if (!strict) { path = path.replace(/\/$/, ''); }
  if (path[0] === '/') { return path }
  if (parent == null) { return path }
  return cleanPath(((parent.path) + "/" + path))
}

/*  */



function createMatcher (
  routes,
  router
) {
  var ref = createRouteMap(routes);
  var pathList = ref.pathList;
  var pathMap = ref.pathMap;
  var nameMap = ref.nameMap;

  function addRoutes (routes) {
    createRouteMap(routes, pathList, pathMap, nameMap);
  }

  function match (
    raw,
    currentRoute,
    redirectedFrom
  ) {
    var location = normalizeLocation(raw, currentRoute, false, router);
    var name = location.name;

    if (name) {
      var record = nameMap[name];
      if (true) {
        warn(record, ("Route with name '" + name + "' does not exist"));
      }
      if (!record) { return _createRoute(null, location) }
      var paramNames = record.regex.keys
        .filter(function (key) { return !key.optional; })
        .map(function (key) { return key.name; });

      if (typeof location.params !== 'object') {
        location.params = {};
      }

      if (currentRoute && typeof currentRoute.params === 'object') {
        for (var key in currentRoute.params) {
          if (!(key in location.params) && paramNames.indexOf(key) > -1) {
            location.params[key] = currentRoute.params[key];
          }
        }
      }

      location.path = fillParams(record.path, location.params, ("named route \"" + name + "\""));
      return _createRoute(record, location, redirectedFrom)
    } else if (location.path) {
      location.params = {};
      for (var i = 0; i < pathList.length; i++) {
        var path = pathList[i];
        var record$1 = pathMap[path];
        if (matchRoute(record$1.regex, location.path, location.params)) {
          return _createRoute(record$1, location, redirectedFrom)
        }
      }
    }
    // no match
    return _createRoute(null, location)
  }

  function redirect (
    record,
    location
  ) {
    var originalRedirect = record.redirect;
    var redirect = typeof originalRedirect === 'function'
      ? originalRedirect(createRoute(record, location, null, router))
      : originalRedirect;

    if (typeof redirect === 'string') {
      redirect = { path: redirect };
    }

    if (!redirect || typeof redirect !== 'object') {
      if (true) {
        warn(
          false, ("invalid redirect option: " + (JSON.stringify(redirect)))
        );
      }
      return _createRoute(null, location)
    }

    var re = redirect;
    var name = re.name;
    var path = re.path;
    var query = location.query;
    var hash = location.hash;
    var params = location.params;
    query = re.hasOwnProperty('query') ? re.query : query;
    hash = re.hasOwnProperty('hash') ? re.hash : hash;
    params = re.hasOwnProperty('params') ? re.params : params;

    if (name) {
      // resolved named direct
      var targetRecord = nameMap[name];
      if (true) {
        assert(targetRecord, ("redirect failed: named route \"" + name + "\" not found."));
      }
      return match({
        _normalized: true,
        name: name,
        query: query,
        hash: hash,
        params: params
      }, undefined, location)
    } else if (path) {
      // 1. resolve relative redirect
      var rawPath = resolveRecordPath(path, record);
      // 2. resolve params
      var resolvedPath = fillParams(rawPath, params, ("redirect route with path \"" + rawPath + "\""));
      // 3. rematch with existing query and hash
      return match({
        _normalized: true,
        path: resolvedPath,
        query: query,
        hash: hash
      }, undefined, location)
    } else {
      if (true) {
        warn(false, ("invalid redirect option: " + (JSON.stringify(redirect))));
      }
      return _createRoute(null, location)
    }
  }

  function alias (
    record,
    location,
    matchAs
  ) {
    var aliasedPath = fillParams(matchAs, location.params, ("aliased route with path \"" + matchAs + "\""));
    var aliasedMatch = match({
      _normalized: true,
      path: aliasedPath
    });
    if (aliasedMatch) {
      var matched = aliasedMatch.matched;
      var aliasedRecord = matched[matched.length - 1];
      location.params = aliasedMatch.params;
      return _createRoute(aliasedRecord, location)
    }
    return _createRoute(null, location)
  }

  function _createRoute (
    record,
    location,
    redirectedFrom
  ) {
    if (record && record.redirect) {
      return redirect(record, redirectedFrom || location)
    }
    if (record && record.matchAs) {
      return alias(record, location, record.matchAs)
    }
    return createRoute(record, location, redirectedFrom, router)
  }

  return {
    match: match,
    addRoutes: addRoutes
  }
}

function matchRoute (
  regex,
  path,
  params
) {
  var m = path.match(regex);

  if (!m) {
    return false
  } else if (!params) {
    return true
  }

  for (var i = 1, len = m.length; i < len; ++i) {
    var key = regex.keys[i - 1];
    var val = typeof m[i] === 'string' ? decodeURIComponent(m[i]) : m[i];
    if (key) {
      // Fix #1994: using * with props: true generates a param named 0
      params[key.name || 'pathMatch'] = val;
    }
  }

  return true
}

function resolveRecordPath (path, record) {
  return resolvePath(path, record.parent ? record.parent.path : '/', true)
}

/*  */

// use User Timing api (if present) for more accurate key precision
var Time =
  inBrowser && window.performance && window.performance.now
    ? window.performance
    : Date;

function genStateKey () {
  return Time.now().toFixed(3)
}

var _key = genStateKey();

function getStateKey () {
  return _key
}

function setStateKey (key) {
  return (_key = key)
}

/*  */

var positionStore = Object.create(null);

function setupScroll () {
  // Fix for #1585 for Firefox
  // Fix for #2195 Add optional third attribute to workaround a bug in safari https://bugs.webkit.org/show_bug.cgi?id=182678
  // Fix for #2774 Support for apps loaded from Windows file shares not mapped to network drives: replaced location.origin with
  // window.location.protocol + '//' + window.location.host
  // location.host contains the port and location.hostname doesn't
  var protocolAndPath = window.location.protocol + '//' + window.location.host;
  var absolutePath = window.location.href.replace(protocolAndPath, '');
  window.history.replaceState({ key: getStateKey() }, '', absolutePath);
  window.addEventListener('popstate', function (e) {
    saveScrollPosition();
    if (e.state && e.state.key) {
      setStateKey(e.state.key);
    }
  });
}

function handleScroll (
  router,
  to,
  from,
  isPop
) {
  if (!router.app) {
    return
  }

  var behavior = router.options.scrollBehavior;
  if (!behavior) {
    return
  }

  if (true) {
    assert(typeof behavior === 'function', "scrollBehavior must be a function");
  }

  // wait until re-render finishes before scrolling
  router.app.$nextTick(function () {
    var position = getScrollPosition();
    var shouldScroll = behavior.call(
      router,
      to,
      from,
      isPop ? position : null
    );

    if (!shouldScroll) {
      return
    }

    if (typeof shouldScroll.then === 'function') {
      shouldScroll
        .then(function (shouldScroll) {
          scrollToPosition((shouldScroll), position);
        })
        .catch(function (err) {
          if (true) {
            assert(false, err.toString());
          }
        });
    } else {
      scrollToPosition(shouldScroll, position);
    }
  });
}

function saveScrollPosition () {
  var key = getStateKey();
  if (key) {
    positionStore[key] = {
      x: window.pageXOffset,
      y: window.pageYOffset
    };
  }
}

function getScrollPosition () {
  var key = getStateKey();
  if (key) {
    return positionStore[key]
  }
}

function getElementPosition (el, offset) {
  var docEl = document.documentElement;
  var docRect = docEl.getBoundingClientRect();
  var elRect = el.getBoundingClientRect();
  return {
    x: elRect.left - docRect.left - offset.x,
    y: elRect.top - docRect.top - offset.y
  }
}

function isValidPosition (obj) {
  return isNumber(obj.x) || isNumber(obj.y)
}

function normalizePosition (obj) {
  return {
    x: isNumber(obj.x) ? obj.x : window.pageXOffset,
    y: isNumber(obj.y) ? obj.y : window.pageYOffset
  }
}

function normalizeOffset (obj) {
  return {
    x: isNumber(obj.x) ? obj.x : 0,
    y: isNumber(obj.y) ? obj.y : 0
  }
}

function isNumber (v) {
  return typeof v === 'number'
}

var hashStartsWithNumberRE = /^#\d/;

function scrollToPosition (shouldScroll, position) {
  var isObject = typeof shouldScroll === 'object';
  if (isObject && typeof shouldScroll.selector === 'string') {
    // getElementById would still fail if the selector contains a more complicated query like #main[data-attr]
    // but at the same time, it doesn't make much sense to select an element with an id and an extra selector
    var el = hashStartsWithNumberRE.test(shouldScroll.selector) // $flow-disable-line
      ? document.getElementById(shouldScroll.selector.slice(1)) // $flow-disable-line
      : document.querySelector(shouldScroll.selector);

    if (el) {
      var offset =
        shouldScroll.offset && typeof shouldScroll.offset === 'object'
          ? shouldScroll.offset
          : {};
      offset = normalizeOffset(offset);
      position = getElementPosition(el, offset);
    } else if (isValidPosition(shouldScroll)) {
      position = normalizePosition(shouldScroll);
    }
  } else if (isObject && isValidPosition(shouldScroll)) {
    position = normalizePosition(shouldScroll);
  }

  if (position) {
    window.scrollTo(position.x, position.y);
  }
}

/*  */

var supportsPushState =
  inBrowser &&
  (function () {
    var ua = window.navigator.userAgent;

    if (
      (ua.indexOf('Android 2.') !== -1 || ua.indexOf('Android 4.0') !== -1) &&
      ua.indexOf('Mobile Safari') !== -1 &&
      ua.indexOf('Chrome') === -1 &&
      ua.indexOf('Windows Phone') === -1
    ) {
      return false
    }

    return window.history && 'pushState' in window.history
  })();

function pushState (url, replace) {
  saveScrollPosition();
  // try...catch the pushState call to get around Safari
  // DOM Exception 18 where it limits to 100 pushState calls
  var history = window.history;
  try {
    if (replace) {
      history.replaceState({ key: getStateKey() }, '', url);
    } else {
      history.pushState({ key: setStateKey(genStateKey()) }, '', url);
    }
  } catch (e) {
    window.location[replace ? 'replace' : 'assign'](url);
  }
}

function replaceState (url) {
  pushState(url, true);
}

/*  */

function runQueue (queue, fn, cb) {
  var step = function (index) {
    if (index >= queue.length) {
      cb();
    } else {
      if (queue[index]) {
        fn(queue[index], function () {
          step(index + 1);
        });
      } else {
        step(index + 1);
      }
    }
  };
  step(0);
}

/*  */

function resolveAsyncComponents (matched) {
  return function (to, from, next) {
    var hasAsync = false;
    var pending = 0;
    var error = null;

    flatMapComponents(matched, function (def, _, match, key) {
      // if it's a function and doesn't have cid attached,
      // assume it's an async component resolve function.
      // we are not using Vue's default async resolving mechanism because
      // we want to halt the navigation until the incoming component has been
      // resolved.
      if (typeof def === 'function' && def.cid === undefined) {
        hasAsync = true;
        pending++;

        var resolve = once(function (resolvedDef) {
          if (isESModule(resolvedDef)) {
            resolvedDef = resolvedDef.default;
          }
          // save resolved on async factory in case it's used elsewhere
          def.resolved = typeof resolvedDef === 'function'
            ? resolvedDef
            : _Vue.extend(resolvedDef);
          match.components[key] = resolvedDef;
          pending--;
          if (pending <= 0) {
            next();
          }
        });

        var reject = once(function (reason) {
          var msg = "Failed to resolve async component " + key + ": " + reason;
           true && warn(false, msg);
          if (!error) {
            error = isError(reason)
              ? reason
              : new Error(msg);
            next(error);
          }
        });

        var res;
        try {
          res = def(resolve, reject);
        } catch (e) {
          reject(e);
        }
        if (res) {
          if (typeof res.then === 'function') {
            res.then(resolve, reject);
          } else {
            // new syntax in Vue 2.3
            var comp = res.component;
            if (comp && typeof comp.then === 'function') {
              comp.then(resolve, reject);
            }
          }
        }
      }
    });

    if (!hasAsync) { next(); }
  }
}

function flatMapComponents (
  matched,
  fn
) {
  return flatten(matched.map(function (m) {
    return Object.keys(m.components).map(function (key) { return fn(
      m.components[key],
      m.instances[key],
      m, key
    ); })
  }))
}

function flatten (arr) {
  return Array.prototype.concat.apply([], arr)
}

var hasSymbol =
  typeof Symbol === 'function' &&
  typeof Symbol.toStringTag === 'symbol';

function isESModule (obj) {
  return obj.__esModule || (hasSymbol && obj[Symbol.toStringTag] === 'Module')
}

// in Webpack 2, require.ensure now also returns a Promise
// so the resolve/reject functions may get called an extra time
// if the user uses an arrow function shorthand that happens to
// return that Promise.
function once (fn) {
  var called = false;
  return function () {
    var args = [], len = arguments.length;
    while ( len-- ) args[ len ] = arguments[ len ];

    if (called) { return }
    called = true;
    return fn.apply(this, args)
  }
}

var NavigationDuplicated = /*@__PURE__*/(function (Error) {
  function NavigationDuplicated (normalizedLocation) {
    Error.call(this);
    this.name = this._name = 'NavigationDuplicated';
    // passing the message to super() doesn't seem to work in the transpiled version
    this.message = "Navigating to current location (\"" + (normalizedLocation.fullPath) + "\") is not allowed";
    // add a stack property so services like Sentry can correctly display it
    Object.defineProperty(this, 'stack', {
      value: new Error().stack,
      writable: true,
      configurable: true
    });
    // we could also have used
    // Error.captureStackTrace(this, this.constructor)
    // but it only exists on node and chrome
  }

  if ( Error ) NavigationDuplicated.__proto__ = Error;
  NavigationDuplicated.prototype = Object.create( Error && Error.prototype );
  NavigationDuplicated.prototype.constructor = NavigationDuplicated;

  return NavigationDuplicated;
}(Error));

// support IE9
NavigationDuplicated._name = 'NavigationDuplicated';

/*  */

var History = function History (router, base) {
  this.router = router;
  this.base = normalizeBase(base);
  // start with a route object that stands for "nowhere"
  this.current = START;
  this.pending = null;
  this.ready = false;
  this.readyCbs = [];
  this.readyErrorCbs = [];
  this.errorCbs = [];
};

History.prototype.listen = function listen (cb) {
  this.cb = cb;
};

History.prototype.onReady = function onReady (cb, errorCb) {
  if (this.ready) {
    cb();
  } else {
    this.readyCbs.push(cb);
    if (errorCb) {
      this.readyErrorCbs.push(errorCb);
    }
  }
};

History.prototype.onError = function onError (errorCb) {
  this.errorCbs.push(errorCb);
};

History.prototype.transitionTo = function transitionTo (
  location,
  onComplete,
  onAbort
) {
    var this$1 = this;

  var route = this.router.match(location, this.current);
  this.confirmTransition(
    route,
    function () {
      this$1.updateRoute(route);
      onComplete && onComplete(route);
      this$1.ensureURL();

      // fire ready cbs once
      if (!this$1.ready) {
        this$1.ready = true;
        this$1.readyCbs.forEach(function (cb) {
          cb(route);
        });
      }
    },
    function (err) {
      if (onAbort) {
        onAbort(err);
      }
      if (err && !this$1.ready) {
        this$1.ready = true;
        this$1.readyErrorCbs.forEach(function (cb) {
          cb(err);
        });
      }
    }
  );
};

History.prototype.confirmTransition = function confirmTransition (route, onComplete, onAbort) {
    var this$1 = this;

  var current = this.current;
  var abort = function (err) {
    // after merging https://github.com/vuejs/vue-router/pull/2771 we
    // When the user navigates through history through back/forward buttons
    // we do not want to throw the error. We only throw it if directly calling
    // push/replace. That's why it's not included in isError
    if (!isExtendedError(NavigationDuplicated, err) && isError(err)) {
      if (this$1.errorCbs.length) {
        this$1.errorCbs.forEach(function (cb) {
          cb(err);
        });
      } else {
        warn(false, 'uncaught error during route navigation:');
        console.error(err);
      }
    }
    onAbort && onAbort(err);
  };
  if (
    isSameRoute(route, current) &&
    // in the case the route map has been dynamically appended to
    route.matched.length === current.matched.length
  ) {
    this.ensureURL();
    return abort(new NavigationDuplicated(route))
  }

  var ref = resolveQueue(
    this.current.matched,
    route.matched
  );
    var updated = ref.updated;
    var deactivated = ref.deactivated;
    var activated = ref.activated;

  var queue = [].concat(
    // in-component leave guards
    extractLeaveGuards(deactivated),
    // global before hooks
    this.router.beforeHooks,
    // in-component update hooks
    extractUpdateHooks(updated),
    // in-config enter guards
    activated.map(function (m) { return m.beforeEnter; }),
    // async components
    resolveAsyncComponents(activated)
  );

  this.pending = route;
  var iterator = function (hook, next) {
    if (this$1.pending !== route) {
      return abort()
    }
    try {
      hook(route, current, function (to) {
        if (to === false || isError(to)) {
          // next(false) -> abort navigation, ensure current URL
          this$1.ensureURL(true);
          abort(to);
        } else if (
          typeof to === 'string' ||
          (typeof to === 'object' &&
            (typeof to.path === 'string' || typeof to.name === 'string'))
        ) {
          // next('/') or next({ path: '/' }) -> redirect
          abort();
          if (typeof to === 'object' && to.replace) {
            this$1.replace(to);
          } else {
            this$1.push(to);
          }
        } else {
          // confirm transition and pass on the value
          next(to);
        }
      });
    } catch (e) {
      abort(e);
    }
  };

  runQueue(queue, iterator, function () {
    var postEnterCbs = [];
    var isValid = function () { return this$1.current === route; };
    // wait until async components are resolved before
    // extracting in-component enter guards
    var enterGuards = extractEnterGuards(activated, postEnterCbs, isValid);
    var queue = enterGuards.concat(this$1.router.resolveHooks);
    runQueue(queue, iterator, function () {
      if (this$1.pending !== route) {
        return abort()
      }
      this$1.pending = null;
      onComplete(route);
      if (this$1.router.app) {
        this$1.router.app.$nextTick(function () {
          postEnterCbs.forEach(function (cb) {
            cb();
          });
        });
      }
    });
  });
};

History.prototype.updateRoute = function updateRoute (route) {
  var prev = this.current;
  this.current = route;
  this.cb && this.cb(route);
  this.router.afterHooks.forEach(function (hook) {
    hook && hook(route, prev);
  });
};

function normalizeBase (base) {
  if (!base) {
    if (inBrowser) {
      // respect <base> tag
      var baseEl = document.querySelector('base');
      base = (baseEl && baseEl.getAttribute('href')) || '/';
      // strip full URL origin
      base = base.replace(/^https?:\/\/[^\/]+/, '');
    } else {
      base = '/';
    }
  }
  // make sure there's the starting slash
  if (base.charAt(0) !== '/') {
    base = '/' + base;
  }
  // remove trailing slash
  return base.replace(/\/$/, '')
}

function resolveQueue (
  current,
  next
) {
  var i;
  var max = Math.max(current.length, next.length);
  for (i = 0; i < max; i++) {
    if (current[i] !== next[i]) {
      break
    }
  }
  return {
    updated: next.slice(0, i),
    activated: next.slice(i),
    deactivated: current.slice(i)
  }
}

function extractGuards (
  records,
  name,
  bind,
  reverse
) {
  var guards = flatMapComponents(records, function (def, instance, match, key) {
    var guard = extractGuard(def, name);
    if (guard) {
      return Array.isArray(guard)
        ? guard.map(function (guard) { return bind(guard, instance, match, key); })
        : bind(guard, instance, match, key)
    }
  });
  return flatten(reverse ? guards.reverse() : guards)
}

function extractGuard (
  def,
  key
) {
  if (typeof def !== 'function') {
    // extend now so that global mixins are applied.
    def = _Vue.extend(def);
  }
  return def.options[key]
}

function extractLeaveGuards (deactivated) {
  return extractGuards(deactivated, 'beforeRouteLeave', bindGuard, true)
}

function extractUpdateHooks (updated) {
  return extractGuards(updated, 'beforeRouteUpdate', bindGuard)
}

function bindGuard (guard, instance) {
  if (instance) {
    return function boundRouteGuard () {
      return guard.apply(instance, arguments)
    }
  }
}

function extractEnterGuards (
  activated,
  cbs,
  isValid
) {
  return extractGuards(
    activated,
    'beforeRouteEnter',
    function (guard, _, match, key) {
      return bindEnterGuard(guard, match, key, cbs, isValid)
    }
  )
}

function bindEnterGuard (
  guard,
  match,
  key,
  cbs,
  isValid
) {
  return function routeEnterGuard (to, from, next) {
    return guard(to, from, function (cb) {
      if (typeof cb === 'function') {
        cbs.push(function () {
          // #750
          // if a router-view is wrapped with an out-in transition,
          // the instance may not have been registered at this time.
          // we will need to poll for registration until current route
          // is no longer valid.
          poll(cb, match.instances, key, isValid);
        });
      }
      next(cb);
    })
  }
}

function poll (
  cb, // somehow flow cannot infer this is a function
  instances,
  key,
  isValid
) {
  if (
    instances[key] &&
    !instances[key]._isBeingDestroyed // do not reuse being destroyed instance
  ) {
    cb(instances[key]);
  } else if (isValid()) {
    setTimeout(function () {
      poll(cb, instances, key, isValid);
    }, 16);
  }
}

/*  */

var HTML5History = /*@__PURE__*/(function (History) {
  function HTML5History (router, base) {
    var this$1 = this;

    History.call(this, router, base);

    var expectScroll = router.options.scrollBehavior;
    var supportsScroll = supportsPushState && expectScroll;

    if (supportsScroll) {
      setupScroll();
    }

    var initLocation = getLocation(this.base);
    window.addEventListener('popstate', function (e) {
      var current = this$1.current;

      // Avoiding first `popstate` event dispatched in some browsers but first
      // history route not updated since async guard at the same time.
      var location = getLocation(this$1.base);
      if (this$1.current === START && location === initLocation) {
        return
      }

      this$1.transitionTo(location, function (route) {
        if (supportsScroll) {
          handleScroll(router, route, current, true);
        }
      });
    });
  }

  if ( History ) HTML5History.__proto__ = History;
  HTML5History.prototype = Object.create( History && History.prototype );
  HTML5History.prototype.constructor = HTML5History;

  HTML5History.prototype.go = function go (n) {
    window.history.go(n);
  };

  HTML5History.prototype.push = function push (location, onComplete, onAbort) {
    var this$1 = this;

    var ref = this;
    var fromRoute = ref.current;
    this.transitionTo(location, function (route) {
      pushState(cleanPath(this$1.base + route.fullPath));
      handleScroll(this$1.router, route, fromRoute, false);
      onComplete && onComplete(route);
    }, onAbort);
  };

  HTML5History.prototype.replace = function replace (location, onComplete, onAbort) {
    var this$1 = this;

    var ref = this;
    var fromRoute = ref.current;
    this.transitionTo(location, function (route) {
      replaceState(cleanPath(this$1.base + route.fullPath));
      handleScroll(this$1.router, route, fromRoute, false);
      onComplete && onComplete(route);
    }, onAbort);
  };

  HTML5History.prototype.ensureURL = function ensureURL (push) {
    if (getLocation(this.base) !== this.current.fullPath) {
      var current = cleanPath(this.base + this.current.fullPath);
      push ? pushState(current) : replaceState(current);
    }
  };

  HTML5History.prototype.getCurrentLocation = function getCurrentLocation () {
    return getLocation(this.base)
  };

  return HTML5History;
}(History));

function getLocation (base) {
  var path = decodeURI(window.location.pathname);
  if (base && path.indexOf(base) === 0) {
    path = path.slice(base.length);
  }
  return (path || '/') + window.location.search + window.location.hash
}

/*  */

var HashHistory = /*@__PURE__*/(function (History) {
  function HashHistory (router, base, fallback) {
    History.call(this, router, base);
    // check history fallback deeplinking
    if (fallback && checkFallback(this.base)) {
      return
    }
    ensureSlash();
  }

  if ( History ) HashHistory.__proto__ = History;
  HashHistory.prototype = Object.create( History && History.prototype );
  HashHistory.prototype.constructor = HashHistory;

  // this is delayed until the app mounts
  // to avoid the hashchange listener being fired too early
  HashHistory.prototype.setupListeners = function setupListeners () {
    var this$1 = this;

    var router = this.router;
    var expectScroll = router.options.scrollBehavior;
    var supportsScroll = supportsPushState && expectScroll;

    if (supportsScroll) {
      setupScroll();
    }

    window.addEventListener(
      supportsPushState ? 'popstate' : 'hashchange',
      function () {
        var current = this$1.current;
        if (!ensureSlash()) {
          return
        }
        this$1.transitionTo(getHash(), function (route) {
          if (supportsScroll) {
            handleScroll(this$1.router, route, current, true);
          }
          if (!supportsPushState) {
            replaceHash(route.fullPath);
          }
        });
      }
    );
  };

  HashHistory.prototype.push = function push (location, onComplete, onAbort) {
    var this$1 = this;

    var ref = this;
    var fromRoute = ref.current;
    this.transitionTo(
      location,
      function (route) {
        pushHash(route.fullPath);
        handleScroll(this$1.router, route, fromRoute, false);
        onComplete && onComplete(route);
      },
      onAbort
    );
  };

  HashHistory.prototype.replace = function replace (location, onComplete, onAbort) {
    var this$1 = this;

    var ref = this;
    var fromRoute = ref.current;
    this.transitionTo(
      location,
      function (route) {
        replaceHash(route.fullPath);
        handleScroll(this$1.router, route, fromRoute, false);
        onComplete && onComplete(route);
      },
      onAbort
    );
  };

  HashHistory.prototype.go = function go (n) {
    window.history.go(n);
  };

  HashHistory.prototype.ensureURL = function ensureURL (push) {
    var current = this.current.fullPath;
    if (getHash() !== current) {
      push ? pushHash(current) : replaceHash(current);
    }
  };

  HashHistory.prototype.getCurrentLocation = function getCurrentLocation () {
    return getHash()
  };

  return HashHistory;
}(History));

function checkFallback (base) {
  var location = getLocation(base);
  if (!/^\/#/.test(location)) {
    window.location.replace(cleanPath(base + '/#' + location));
    return true
  }
}

function ensureSlash () {
  var path = getHash();
  if (path.charAt(0) === '/') {
    return true
  }
  replaceHash('/' + path);
  return false
}

function getHash () {
  // We can't use window.location.hash here because it's not
  // consistent across browsers - Firefox will pre-decode it!
  var href = window.location.href;
  var index = href.indexOf('#');
  // empty path
  if (index < 0) { return '' }

  href = href.slice(index + 1);
  // decode the hash but not the search or hash
  // as search(query) is already decoded
  // https://github.com/vuejs/vue-router/issues/2708
  var searchIndex = href.indexOf('?');
  if (searchIndex < 0) {
    var hashIndex = href.indexOf('#');
    if (hashIndex > -1) {
      href = decodeURI(href.slice(0, hashIndex)) + href.slice(hashIndex);
    } else { href = decodeURI(href); }
  } else {
    if (searchIndex > -1) {
      href = decodeURI(href.slice(0, searchIndex)) + href.slice(searchIndex);
    }
  }

  return href
}

function getUrl (path) {
  var href = window.location.href;
  var i = href.indexOf('#');
  var base = i >= 0 ? href.slice(0, i) : href;
  return (base + "#" + path)
}

function pushHash (path) {
  if (supportsPushState) {
    pushState(getUrl(path));
  } else {
    window.location.hash = path;
  }
}

function replaceHash (path) {
  if (supportsPushState) {
    replaceState(getUrl(path));
  } else {
    window.location.replace(getUrl(path));
  }
}

/*  */

var AbstractHistory = /*@__PURE__*/(function (History) {
  function AbstractHistory (router, base) {
    History.call(this, router, base);
    this.stack = [];
    this.index = -1;
  }

  if ( History ) AbstractHistory.__proto__ = History;
  AbstractHistory.prototype = Object.create( History && History.prototype );
  AbstractHistory.prototype.constructor = AbstractHistory;

  AbstractHistory.prototype.push = function push (location, onComplete, onAbort) {
    var this$1 = this;

    this.transitionTo(
      location,
      function (route) {
        this$1.stack = this$1.stack.slice(0, this$1.index + 1).concat(route);
        this$1.index++;
        onComplete && onComplete(route);
      },
      onAbort
    );
  };

  AbstractHistory.prototype.replace = function replace (location, onComplete, onAbort) {
    var this$1 = this;

    this.transitionTo(
      location,
      function (route) {
        this$1.stack = this$1.stack.slice(0, this$1.index).concat(route);
        onComplete && onComplete(route);
      },
      onAbort
    );
  };

  AbstractHistory.prototype.go = function go (n) {
    var this$1 = this;

    var targetIndex = this.index + n;
    if (targetIndex < 0 || targetIndex >= this.stack.length) {
      return
    }
    var route = this.stack[targetIndex];
    this.confirmTransition(
      route,
      function () {
        this$1.index = targetIndex;
        this$1.updateRoute(route);
      },
      function (err) {
        if (isExtendedError(NavigationDuplicated, err)) {
          this$1.index = targetIndex;
        }
      }
    );
  };

  AbstractHistory.prototype.getCurrentLocation = function getCurrentLocation () {
    var current = this.stack[this.stack.length - 1];
    return current ? current.fullPath : '/'
  };

  AbstractHistory.prototype.ensureURL = function ensureURL () {
    // noop
  };

  return AbstractHistory;
}(History));

/*  */



var VueRouter = function VueRouter (options) {
  if ( options === void 0 ) options = {};

  this.app = null;
  this.apps = [];
  this.options = options;
  this.beforeHooks = [];
  this.resolveHooks = [];
  this.afterHooks = [];
  this.matcher = createMatcher(options.routes || [], this);

  var mode = options.mode || 'hash';
  this.fallback = mode === 'history' && !supportsPushState && options.fallback !== false;
  if (this.fallback) {
    mode = 'hash';
  }
  if (!inBrowser) {
    mode = 'abstract';
  }
  this.mode = mode;

  switch (mode) {
    case 'history':
      this.history = new HTML5History(this, options.base);
      break
    case 'hash':
      this.history = new HashHistory(this, options.base, this.fallback);
      break
    case 'abstract':
      this.history = new AbstractHistory(this, options.base);
      break
    default:
      if (true) {
        assert(false, ("invalid mode: " + mode));
      }
  }
};

var prototypeAccessors = { currentRoute: { configurable: true } };

VueRouter.prototype.match = function match (
  raw,
  current,
  redirectedFrom
) {
  return this.matcher.match(raw, current, redirectedFrom)
};

prototypeAccessors.currentRoute.get = function () {
  return this.history && this.history.current
};

VueRouter.prototype.init = function init (app /* Vue component instance */) {
    var this$1 = this;

   true && assert(
    install.installed,
    "not installed. Make sure to call `Vue.use(VueRouter)` " +
    "before creating root instance."
  );

  this.apps.push(app);

  // set up app destroyed handler
  // https://github.com/vuejs/vue-router/issues/2639
  app.$once('hook:destroyed', function () {
    // clean out app from this.apps array once destroyed
    var index = this$1.apps.indexOf(app);
    if (index > -1) { this$1.apps.splice(index, 1); }
    // ensure we still have a main app or null if no apps
    // we do not release the router so it can be reused
    if (this$1.app === app) { this$1.app = this$1.apps[0] || null; }
  });

  // main app previously initialized
  // return as we don't need to set up new history listener
  if (this.app) {
    return
  }

  this.app = app;

  var history = this.history;

  if (history instanceof HTML5History) {
    history.transitionTo(history.getCurrentLocation());
  } else if (history instanceof HashHistory) {
    var setupHashListener = function () {
      history.setupListeners();
    };
    history.transitionTo(
      history.getCurrentLocation(),
      setupHashListener,
      setupHashListener
    );
  }

  history.listen(function (route) {
    this$1.apps.forEach(function (app) {
      app._route = route;
    });
  });
};

VueRouter.prototype.beforeEach = function beforeEach (fn) {
  return registerHook(this.beforeHooks, fn)
};

VueRouter.prototype.beforeResolve = function beforeResolve (fn) {
  return registerHook(this.resolveHooks, fn)
};

VueRouter.prototype.afterEach = function afterEach (fn) {
  return registerHook(this.afterHooks, fn)
};

VueRouter.prototype.onReady = function onReady (cb, errorCb) {
  this.history.onReady(cb, errorCb);
};

VueRouter.prototype.onError = function onError (errorCb) {
  this.history.onError(errorCb);
};

VueRouter.prototype.push = function push (location, onComplete, onAbort) {
    var this$1 = this;

  // $flow-disable-line
  if (!onComplete && !onAbort && typeof Promise !== 'undefined') {
    return new Promise(function (resolve, reject) {
      this$1.history.push(location, resolve, reject);
    })
  } else {
    this.history.push(location, onComplete, onAbort);
  }
};

VueRouter.prototype.replace = function replace (location, onComplete, onAbort) {
    var this$1 = this;

  // $flow-disable-line
  if (!onComplete && !onAbort && typeof Promise !== 'undefined') {
    return new Promise(function (resolve, reject) {
      this$1.history.replace(location, resolve, reject);
    })
  } else {
    this.history.replace(location, onComplete, onAbort);
  }
};

VueRouter.prototype.go = function go (n) {
  this.history.go(n);
};

VueRouter.prototype.back = function back () {
  this.go(-1);
};

VueRouter.prototype.forward = function forward () {
  this.go(1);
};

VueRouter.prototype.getMatchedComponents = function getMatchedComponents (to) {
  var route = to
    ? to.matched
      ? to
      : this.resolve(to).route
    : this.currentRoute;
  if (!route) {
    return []
  }
  return [].concat.apply([], route.matched.map(function (m) {
    return Object.keys(m.components).map(function (key) {
      return m.components[key]
    })
  }))
};

VueRouter.prototype.resolve = function resolve (
  to,
  current,
  append
) {
  current = current || this.history.current;
  var location = normalizeLocation(
    to,
    current,
    append,
    this
  );
  var route = this.match(location, current);
  var fullPath = route.redirectedFrom || route.fullPath;
  var base = this.history.base;
  var href = createHref(base, fullPath, this.mode);
  return {
    location: location,
    route: route,
    href: href,
    // for backwards compat
    normalizedTo: location,
    resolved: route
  }
};

VueRouter.prototype.addRoutes = function addRoutes (routes) {
  this.matcher.addRoutes(routes);
  if (this.history.current !== START) {
    this.history.transitionTo(this.history.getCurrentLocation());
  }
};

Object.defineProperties( VueRouter.prototype, prototypeAccessors );

function registerHook (list, fn) {
  list.push(fn);
  return function () {
    var i = list.indexOf(fn);
    if (i > -1) { list.splice(i, 1); }
  }
}

function createHref (base, fullPath, mode) {
  var path = mode === 'hash' ? '#' + fullPath : fullPath;
  return base ? cleanPath(base + '/' + path) : path
}

VueRouter.install = install;
VueRouter.version = '3.1.3';

if (inBrowser && window.Vue) {
  window.Vue.use(VueRouter);
}

/* harmony default export */ __webpack_exports__["default"] = (VueRouter);


/***/ }),

/***/ "./node_modules/webpack/hot sync ^\\.\\/log$":
/*!*************************************************!*\
  !*** (webpack)/hot sync nonrecursive ^\.\/log$ ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./log": "./node_modules/webpack/hot/log.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/webpack/hot sync ^\\.\\/log$";

/***/ }),

/***/ "./src/js/common/util/VeeValidateMessage.js":
/*!**************************************************!*\
  !*** ./src/js/common/util/VeeValidateMessage.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'vi',
  messages: {
    _default: function _default(n) {
      return 'Giá trị của ' + n + ' không đúng.';
    },
    after: function after(n, t) {
      return n + ' phải xuất hiện sau ' + t.length + '.';
    },
    alpha_dash: function alpha_dash(n) {
      return n + ' có thể chứa các kí tự chữ (A-Z a-z), số (0-9), gạch ngang (-) và gạch dưới (_).';
    },
    alpha_num: function alpha_num(n) {
      return n + ' chỉ có thể chứa các kí tự chữ và số.';
    },
    alpha_spaces: function alpha_spaces(n) {
      return n + ' chỉ có thế chứa các kí tự và khoảng trắng';
    },
    alpha: function alpha(n) {
      return n + ' chỉ có thể chứa các kí tự chữ.';
    },
    before: function before(n, t) {
      return n + ' phải xuất hiện trước ' + t.length + '.';
    },
    between: function between(n, t) {
      return n + ' phải có giá trị nằm trong khoảng giữa ' + t.length + ' và ' + t[1] + '.';
    },
    confirmed: function confirmed(n, t) {
      return n + ' khác với ' + t.length + '.';
    },
    credit_card: function credit_card(n) {
      return 'Đã điền ' + n + ' không chính xác.';
    },
    date_between: function date_between(n, t) {
      return n + ' phải có giá trị nằm trong khoảng giữa  ' + t.length + ' và ' + t[1] + '.';
    },
    date_format: function date_format(n, t) {
      return n + ' phải có giá trị dưới định dạng ' + t.length + '.';
    },
    decimal: function decimal(n, t) {
      void 0 === t && (t = []);
      var c = t.length;
      return void 0 === c && (c = '*'), n + ' chỉ có thể chứa các kí tự số và dấu thập phân ' + ('*' === c ? '' : c) + '.';
    },
    digits: function digits(n, t) {
      return 'Trường ' + n + ' chỉ có thể chứa các kí tự số và bắt buộc phải có độ dài là ' + t.length + '.';
    },
    dimensions: function dimensions(n, t) {
      return n + ' phải có chiều rộng ' + t.length + ' pixels và chiều cao ' + t[1] + ' pixels.';
    },
    email: function email(n) {
      return n + ' phải là một địa chỉ email hợp lệ.';
    },
    ext: function ext(n) {
      return n + ' phải là một tệp.';
    },
    image: function image(n) {
      return 'Trường ' + n + ' phải là một ảnh.';
    },
    included: function included(n) {
      return n + ' phải là một giá trị.';
    },
    ip: function ip(n) {
      return n + ' phải là một địa chỉ ip hợp lệ.';
    },
    max: function max(n, t) {
      return n + ' không thể có nhiều hơn ' + t.length + ' kí tự.';
    },
    max_value: function max_value(n, t) {
      return n + ' phải nhỏ hơn hoặc bằng ' + t.length + '.';
    },
    mimes: function mimes(n) {
      return n + ' phải chứa kiểu tệp phù hợp.';
    },
    min: function min(n, t) {
      return n + ' phải chứa ít nhất ' + t.length + ' kí tự.';
    },
    min_value: function min_value(n, t) {
      return n + ' phải lớn hơn hoặc bằng ' + t.length + '.';
    },
    excluded: function excluded(n) {
      return n + ' phải chứa một giá trị hợp lệ.';
    },
    numeric: function numeric(n) {
      return n + ' chỉ có thể có các kí tự số.';
    },
    regex: function regex(n) {
      return n + ' có định dạng không đúng.';
    },
    required: function required(n) {
      return n + ' không được để trống.';
    },
    size: function size(n, t) {
      var c,
          e,
          i,
          h = t.length;
      return n + ' chỉ có thể chứa tệp nhỏ hơn ' + (c = h, e = 1024, i = 0 == (c = Number(c) * e) ? 0 : Math.floor(Math.log(c) / Math.log(e)), 1 * (c / Math.pow(e, i)).toFixed(2) + ' ' + ['Byte', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'][i]) + '.';
    },
    url: function url(n) {
      return n + ' không phải là một địa chỉ URL hợp lệ.';
    }
  }
});

/***/ }),

/***/ "./src/js/partial/user/user.js":
/*!*************************************!*\
  !*** ./src/js/partial/user/user.js ***!
  \*************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js-exposed");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.runtime.esm.js");
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var vue_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! vue-router */ "./node_modules/vue-router/dist/vue-router.esm.js");
/* harmony import */ var $vuePartialPath_user_App__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! $vuePartialPath/user/App */ "./src/vue/partial/user/App.vue");
/* harmony import */ var $vuePartialPath_user_UserIndexSection__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! $vuePartialPath/user/UserIndexSection */ "./src/vue/partial/user/UserIndexSection.vue");
/* harmony import */ var $vuePartialPath_user_UserAddSection__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! $vuePartialPath/user/UserAddSection */ "./src/vue/partial/user/UserAddSection.vue");
/* harmony import */ var $vuePartialPath_user_UserEditSection__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! $vuePartialPath/user/UserEditSection */ "./src/vue/partial/user/UserEditSection.vue");
/* harmony import */ var $vuePartialPath_user_UserTable__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! $vuePartialPath/user/UserTable */ "./src/vue/partial/user/UserTable.vue");
/* harmony import */ var $jsStorePath_user__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! $jsStorePath/user */ "./src/js/store/user.js");










vue__WEBPACK_IMPORTED_MODULE_1__["default"].use(vuex__WEBPACK_IMPORTED_MODULE_2__["default"]);
vue__WEBPACK_IMPORTED_MODULE_1__["default"].use(vue_router__WEBPACK_IMPORTED_MODULE_3__["default"]);
var store = new vuex__WEBPACK_IMPORTED_MODULE_2__["default"].Store($jsStorePath_user__WEBPACK_IMPORTED_MODULE_9__["default"]);
var router = new vue_router__WEBPACK_IMPORTED_MODULE_3__["default"]({
  mode: 'history',
  routes: [{
    name: 'user:index',
    path: '/backend/user/index/:page',
    component: $vuePartialPath_user_UserIndexSection__WEBPACK_IMPORTED_MODULE_5__["default"]
  }, {
    name: 'user:add',
    path: '/backend/user/add/:page',
    component: $vuePartialPath_user_UserAddSection__WEBPACK_IMPORTED_MODULE_6__["default"]
  }, {
    name: 'user:edit',
    path: '/backend/user/edit/:id/:page',
    component: $vuePartialPath_user_UserEditSection__WEBPACK_IMPORTED_MODULE_7__["default"]
  }]
});

var Object = function () {
  var _renderUserTable = function _renderUserTable() {
    var $el = jquery__WEBPACK_IMPORTED_MODULE_0___default()('#user-section');
    new vue__WEBPACK_IMPORTED_MODULE_1__["default"]({
      router: router,
      store: store,
      render: function render(h) {
        return h($vuePartialPath_user_App__WEBPACK_IMPORTED_MODULE_4__["default"]);
      }
    }).$mount($el[0]);
  };

  return {
    init: function init() {
      _renderUserTable();
    }
  };
}();

document.addEventListener("DOMContentLoaded", function () {
  Object.init();
});

/***/ }),

/***/ "./src/js/store/user.js":
/*!******************************!*\
  !*** ./src/js/store/user.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var $jsCommonPath_api__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! $jsCommonPath/api */ "./src/js/common/api/index.js");
/* harmony import */ var $jsCommonPath_util_GeneralUtil__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! $jsCommonPath/util/GeneralUtil */ "./src/js/common/util/GeneralUtil.js");
/* harmony import */ var $jsCommonPath_util_SafeUtil__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! $jsCommonPath/util/SafeUtil */ "./src/js/common/util/SafeUtil.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }





var _objEntity = {
  user_id: null,
  profile_image: null,
  fullname: null,
  email: null,
  phone: null,
  is_actived: 0
};

var Store = function Store() {
  _classCallCheck(this, Store);

  return {
    state: {
      flag: {
        isRead: false
      },
      arrEntityList: [],
      objEntitySelect: _objectSpread({}, _objEntity)
    },
    mutations: {
      resetEntity: function resetEntity(state) {
        state.entity.objEntitySelect = _objectSpread({}, _objEntity);
      },
      updateFlag: function updateFlag(state, payload) {
        state.flag[payload.flag] = payload.value;
      },
      setEntities: function setEntities(state, payload) {
        state.entity.arrEntityList = payload.arrEntityList;
      },
      setEntity: function setEntity(state, payload) {
        if (Object(lodash__WEBPACK_IMPORTED_MODULE_0__["isEmpty"])(payload.objEntitySelect)) return;

        var _payload = _objectSpread({}, payload),
            objEntitySelect = _payload.objEntitySelect;

        objEntitySelect.password = null;
        state.entity.objEntitySelect = objEntitySelect;
      },
      updateEntity: function updateEntity(state, payload) {
        var _payload2 = _objectSpread({}, payload),
            type = _payload2.type,
            index = _payload2.index,
            arrEntity = _payload2.arrEntity;

        switch (type) {
          case 'create':
            state.entity.arrEntityList.unshift(arrEntity);
            break;

          case 'update':
            state.entity.arrEntityList[index] = arrEntity;
            state.entity.objEntitySelect = arrEntity;
            break;

          case 'delete':
            state.entity.arrEntityList.splice(index, 1);
            break;
        }
      }
    },
    actions: {
      getEntityAct: function getEntityAct(_ref, payload) {
        var commit = _ref.commit;
        return new Promise(function (resolve, reject) {
          var reqParams = _objectSpread({}, payload);

          $jsCommonPath_api__WEBPACK_IMPORTED_MODULE_1__["getUserReq"](reqParams).done(function (res) {
            if (Object($jsCommonPath_util_GeneralUtil__WEBPACK_IMPORTED_MODULE_2__["isErrorAPI"])(res)) {
              reject(res.payload);
              return;
            }

            var _res$payload = _objectSpread({}, res.payload),
                arrUserDetail = _res$payload.arrUserDetail;

            commit('setEntity', {
              objEntitySelect: arrUserDetail
            });
            resolve(res.payload);
          });
        });
      },
      readEntityAct: function readEntityAct(_ref2) {
        var commit = _ref2.commit;
        return new Promise(function (resolve, reject) {
          var reqParams = {};
          $jsCommonPath_api__WEBPACK_IMPORTED_MODULE_1__["readUserReq"](reqParams).done(function (res) {
            if (Object($jsCommonPath_util_GeneralUtil__WEBPACK_IMPORTED_MODULE_2__["isErrorAPI"])(res)) {
              reject(res.payload);
              return;
            }

            var _res$payload2 = _objectSpread({}, res.payload),
                arrUserList = _res$payload2.arrUserList;

            commit('setEntities', {
              arrEntityList: arrUserList
            });
            resolve(res.payload);
          });
        });
      },
      uploadFileAct: function uploadFileAct(_ref3, payload) {
        var commit = _ref3.commit;
        return new Promise(function (resolve, reject) {
          var formData = new FormData();
          formData.append('controller', payload.controller);
          formData.append('file', payload.file);
          $jsCommonPath_api__WEBPACK_IMPORTED_MODULE_1__["uploadReq"](formData, {
            processData: false,
            contentType: false
          }).done(function (res) {
            if (Object($jsCommonPath_util_GeneralUtil__WEBPACK_IMPORTED_MODULE_2__["isErrorAPI"])(res)) {
              Object($jsCommonPath_util_GeneralUtil__WEBPACK_IMPORTED_MODULE_2__["triggerModal"])(res.message);
              reject(res.payload);
              return;
            }

            resolve(res.payload);
          });
        });
      },
      setEntityAct: function setEntityAct(_ref4, payload) {
        var commit = _ref4.commit,
            getters = _ref4.getters;
        if (!payload.hasOwnProperty('index')) return;

        var _payload3 = _objectSpread({}, payload),
            index = _payload3.index;

        commit('setEntity', {
          objEntitySelect: getters.getEntityByIndex(index)[0]
        });
      },
      createEntityAct: function createEntityAct(_ref5, payload) {
        var commit = _ref5.commit,
            getters = _ref5.getters;
        return new Promise(function (resolve, reject) {
          var reqParams = _objectSpread({}, payload);

          $jsCommonPath_api__WEBPACK_IMPORTED_MODULE_1__["createUserReq"](reqParams).done(function (res) {
            Object($jsCommonPath_util_GeneralUtil__WEBPACK_IMPORTED_MODULE_2__["triggerModal"])(res.message);

            if (Object($jsCommonPath_util_GeneralUtil__WEBPACK_IMPORTED_MODULE_2__["isErrorAPI"])(res)) {
              reject(res.payload);
              return;
            }

            var _res$payload3 = _objectSpread({}, res.payload),
                arrUserDetail = _res$payload3.arrUserDetail;

            commit('updateEntity', {
              type: 'create',
              arrEntity: arrUserDetail
            });
            resolve(res.payload);
          });
        });
      },
      updateEntityAct: function updateEntityAct(_ref6, payload) {
        var commit = _ref6.commit,
            getters = _ref6.getters;
        return new Promise(function (resolve, reject) {
          var reqParams = _objectSpread({}, payload);

          $jsCommonPath_api__WEBPACK_IMPORTED_MODULE_1__["updateUserReq"](reqParams).done(function (res) {
            Object($jsCommonPath_util_GeneralUtil__WEBPACK_IMPORTED_MODULE_2__["triggerModal"])(res.message);

            if (Object($jsCommonPath_util_GeneralUtil__WEBPACK_IMPORTED_MODULE_2__["isErrorAPI"])(res)) {
              reject(res.payload);
              return;
            }

            var _res$payload4 = _objectSpread({}, res.payload),
                arrUserDetail = _res$payload4.arrUserDetail;

            commit('updateEntity', {
              type: 'update',
              index: getters.getIndexByID(arrUserDetail.user_id),
              arrEntity: arrUserDetail
            });
            resolve(res.payload);
          });
        });
      },
      deleteEntityAct: function deleteEntityAct(_ref7, payload) {
        var commit = _ref7.commit,
            getters = _ref7.getters;
        return new Promise(function (resolve, reject) {
          var reqParams = _objectSpread({}, payload);

          $jsCommonPath_api__WEBPACK_IMPORTED_MODULE_1__["deleteUserReq"](reqParams).done(function (res) {
            Object($jsCommonPath_util_GeneralUtil__WEBPACK_IMPORTED_MODULE_2__["triggerModal"])(res.message);

            if (Object($jsCommonPath_util_GeneralUtil__WEBPACK_IMPORTED_MODULE_2__["isErrorAPI"])(res)) {
              reject(res.payload);
              return;
            }

            var _res$payload5 = _objectSpread({}, res.payload),
                id = _res$payload5.id;

            console.log(id);
            commit('updateEntity', {
              type: 'delete',
              index: getters.getIndexByID(id)
            });
            resolve(res.payload);
          });
        });
      }
    },
    getters: {
      getEntityByIndex: function getEntityByIndex(state) {
        return function (index) {
          return state.entity.arrEntityList.filter(function (objEntity, currentIndex) {
            return currentIndex == index;
          });
        };
      },
      getIndexByID: function getIndexByID(state) {
        return function (id) {
          var index = -1;

          Object(lodash__WEBPACK_IMPORTED_MODULE_0__["forIn"])(state.entity.arrEntityList, function (objEntity, currentIndex) {
            if (objEntity.user_id != id) return;
            index = currentIndex;
            return false;
          });

          return index;
        };
      }
    }
  };
};

/* harmony default export */ __webpack_exports__["default"] = (new Store());

/***/ }),

/***/ "./src/vue/partial/global/TheModalFullScreen.vue":
/*!*******************************************************!*\
  !*** ./src/vue/partial/global/TheModalFullScreen.vue ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _TheModalFullScreen_vue_vue_type_template_id_1e69cc63___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./TheModalFullScreen.vue?vue&type=template&id=1e69cc63& */ "./src/vue/partial/global/TheModalFullScreen.vue?vue&type=template&id=1e69cc63&");
/* harmony import */ var _TheModalFullScreen_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./TheModalFullScreen.vue?vue&type=script&lang=js& */ "./src/vue/partial/global/TheModalFullScreen.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _TheModalFullScreen_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _TheModalFullScreen_vue_vue_type_template_id_1e69cc63___WEBPACK_IMPORTED_MODULE_0__["render"],
  _TheModalFullScreen_vue_vue_type_template_id_1e69cc63___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "src/vue/partial/global/TheModalFullScreen.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./src/vue/partial/global/TheModalFullScreen.vue?vue&type=script&lang=js&":
/*!********************************************************************************!*\
  !*** ./src/vue/partial/global/TheModalFullScreen.vue?vue&type=script&lang=js& ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_1_node_modules_vue_loader_lib_index_js_vue_loader_options_TheModalFullScreen_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--1!../../../../node_modules/vue-loader/lib??vue-loader-options!./TheModalFullScreen.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/global/TheModalFullScreen.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_1_node_modules_vue_loader_lib_index_js_vue_loader_options_TheModalFullScreen_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./src/vue/partial/global/TheModalFullScreen.vue?vue&type=template&id=1e69cc63&":
/*!**************************************************************************************!*\
  !*** ./src/vue/partial/global/TheModalFullScreen.vue?vue&type=template&id=1e69cc63& ***!
  \**************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TheModalFullScreen_vue_vue_type_template_id_1e69cc63___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./TheModalFullScreen.vue?vue&type=template&id=1e69cc63& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/global/TheModalFullScreen.vue?vue&type=template&id=1e69cc63&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TheModalFullScreen_vue_vue_type_template_id_1e69cc63___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TheModalFullScreen_vue_vue_type_template_id_1e69cc63___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./src/vue/partial/user/App.vue":
/*!**************************************!*\
  !*** ./src/vue/partial/user/App.vue ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _App_vue_vue_type_template_id_9be03d66___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./App.vue?vue&type=template&id=9be03d66& */ "./src/vue/partial/user/App.vue?vue&type=template&id=9be03d66&");
/* harmony import */ var _App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./App.vue?vue&type=script&lang=js& */ "./src/vue/partial/user/App.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _App_vue_vue_type_template_id_9be03d66___WEBPACK_IMPORTED_MODULE_0__["render"],
  _App_vue_vue_type_template_id_9be03d66___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "src/vue/partial/user/App.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./src/vue/partial/user/App.vue?vue&type=script&lang=js&":
/*!***************************************************************!*\
  !*** ./src/vue/partial/user/App.vue?vue&type=script&lang=js& ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_1_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--1!../../../../node_modules/vue-loader/lib??vue-loader-options!./App.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/user/App.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_1_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./src/vue/partial/user/App.vue?vue&type=template&id=9be03d66&":
/*!*********************************************************************!*\
  !*** ./src/vue/partial/user/App.vue?vue&type=template&id=9be03d66& ***!
  \*********************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_template_id_9be03d66___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./App.vue?vue&type=template&id=9be03d66& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/user/App.vue?vue&type=template&id=9be03d66&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_template_id_9be03d66___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_template_id_9be03d66___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./src/vue/partial/user/UserAddSection.vue":
/*!*************************************************!*\
  !*** ./src/vue/partial/user/UserAddSection.vue ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _UserAddSection_vue_vue_type_template_id_dfe9829a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./UserAddSection.vue?vue&type=template&id=dfe9829a& */ "./src/vue/partial/user/UserAddSection.vue?vue&type=template&id=dfe9829a&");
/* harmony import */ var _UserAddSection_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./UserAddSection.vue?vue&type=script&lang=js& */ "./src/vue/partial/user/UserAddSection.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _UserAddSection_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _UserAddSection_vue_vue_type_template_id_dfe9829a___WEBPACK_IMPORTED_MODULE_0__["render"],
  _UserAddSection_vue_vue_type_template_id_dfe9829a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "src/vue/partial/user/UserAddSection.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./src/vue/partial/user/UserAddSection.vue?vue&type=script&lang=js&":
/*!**************************************************************************!*\
  !*** ./src/vue/partial/user/UserAddSection.vue?vue&type=script&lang=js& ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_1_node_modules_vue_loader_lib_index_js_vue_loader_options_UserAddSection_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--1!../../../../node_modules/vue-loader/lib??vue-loader-options!./UserAddSection.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/user/UserAddSection.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_1_node_modules_vue_loader_lib_index_js_vue_loader_options_UserAddSection_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./src/vue/partial/user/UserAddSection.vue?vue&type=template&id=dfe9829a&":
/*!********************************************************************************!*\
  !*** ./src/vue/partial/user/UserAddSection.vue?vue&type=template&id=dfe9829a& ***!
  \********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UserAddSection_vue_vue_type_template_id_dfe9829a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./UserAddSection.vue?vue&type=template&id=dfe9829a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/user/UserAddSection.vue?vue&type=template&id=dfe9829a&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UserAddSection_vue_vue_type_template_id_dfe9829a___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UserAddSection_vue_vue_type_template_id_dfe9829a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./src/vue/partial/user/UserEditSection.vue":
/*!**************************************************!*\
  !*** ./src/vue/partial/user/UserEditSection.vue ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _UserEditSection_vue_vue_type_template_id_5594325c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./UserEditSection.vue?vue&type=template&id=5594325c& */ "./src/vue/partial/user/UserEditSection.vue?vue&type=template&id=5594325c&");
/* harmony import */ var _UserEditSection_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./UserEditSection.vue?vue&type=script&lang=js& */ "./src/vue/partial/user/UserEditSection.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _UserEditSection_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _UserEditSection_vue_vue_type_template_id_5594325c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _UserEditSection_vue_vue_type_template_id_5594325c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "src/vue/partial/user/UserEditSection.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./src/vue/partial/user/UserEditSection.vue?vue&type=script&lang=js&":
/*!***************************************************************************!*\
  !*** ./src/vue/partial/user/UserEditSection.vue?vue&type=script&lang=js& ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_1_node_modules_vue_loader_lib_index_js_vue_loader_options_UserEditSection_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--1!../../../../node_modules/vue-loader/lib??vue-loader-options!./UserEditSection.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/user/UserEditSection.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_1_node_modules_vue_loader_lib_index_js_vue_loader_options_UserEditSection_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./src/vue/partial/user/UserEditSection.vue?vue&type=template&id=5594325c&":
/*!*********************************************************************************!*\
  !*** ./src/vue/partial/user/UserEditSection.vue?vue&type=template&id=5594325c& ***!
  \*********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UserEditSection_vue_vue_type_template_id_5594325c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./UserEditSection.vue?vue&type=template&id=5594325c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/user/UserEditSection.vue?vue&type=template&id=5594325c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UserEditSection_vue_vue_type_template_id_5594325c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UserEditSection_vue_vue_type_template_id_5594325c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./src/vue/partial/user/UserIndexSection.vue":
/*!***************************************************!*\
  !*** ./src/vue/partial/user/UserIndexSection.vue ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _UserIndexSection_vue_vue_type_template_id_146a67bc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./UserIndexSection.vue?vue&type=template&id=146a67bc& */ "./src/vue/partial/user/UserIndexSection.vue?vue&type=template&id=146a67bc&");
/* harmony import */ var _UserIndexSection_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./UserIndexSection.vue?vue&type=script&lang=js& */ "./src/vue/partial/user/UserIndexSection.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _UserIndexSection_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _UserIndexSection_vue_vue_type_template_id_146a67bc___WEBPACK_IMPORTED_MODULE_0__["render"],
  _UserIndexSection_vue_vue_type_template_id_146a67bc___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "src/vue/partial/user/UserIndexSection.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./src/vue/partial/user/UserIndexSection.vue?vue&type=script&lang=js&":
/*!****************************************************************************!*\
  !*** ./src/vue/partial/user/UserIndexSection.vue?vue&type=script&lang=js& ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_1_node_modules_vue_loader_lib_index_js_vue_loader_options_UserIndexSection_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--1!../../../../node_modules/vue-loader/lib??vue-loader-options!./UserIndexSection.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/user/UserIndexSection.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_1_node_modules_vue_loader_lib_index_js_vue_loader_options_UserIndexSection_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./src/vue/partial/user/UserIndexSection.vue?vue&type=template&id=146a67bc&":
/*!**********************************************************************************!*\
  !*** ./src/vue/partial/user/UserIndexSection.vue?vue&type=template&id=146a67bc& ***!
  \**********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UserIndexSection_vue_vue_type_template_id_146a67bc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./UserIndexSection.vue?vue&type=template&id=146a67bc& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/user/UserIndexSection.vue?vue&type=template&id=146a67bc&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UserIndexSection_vue_vue_type_template_id_146a67bc___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UserIndexSection_vue_vue_type_template_id_146a67bc___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./src/vue/partial/user/UserTable.vue":
/*!********************************************!*\
  !*** ./src/vue/partial/user/UserTable.vue ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _UserTable_vue_vue_type_template_id_75b32def___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./UserTable.vue?vue&type=template&id=75b32def& */ "./src/vue/partial/user/UserTable.vue?vue&type=template&id=75b32def&");
/* harmony import */ var _UserTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./UserTable.vue?vue&type=script&lang=js& */ "./src/vue/partial/user/UserTable.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _UserTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _UserTable_vue_vue_type_template_id_75b32def___WEBPACK_IMPORTED_MODULE_0__["render"],
  _UserTable_vue_vue_type_template_id_75b32def___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "src/vue/partial/user/UserTable.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./src/vue/partial/user/UserTable.vue?vue&type=script&lang=js&":
/*!*********************************************************************!*\
  !*** ./src/vue/partial/user/UserTable.vue?vue&type=script&lang=js& ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_1_node_modules_vue_loader_lib_index_js_vue_loader_options_UserTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--1!../../../../node_modules/vue-loader/lib??vue-loader-options!./UserTable.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/user/UserTable.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_1_node_modules_vue_loader_lib_index_js_vue_loader_options_UserTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./src/vue/partial/user/UserTable.vue?vue&type=template&id=75b32def&":
/*!***************************************************************************!*\
  !*** ./src/vue/partial/user/UserTable.vue?vue&type=template&id=75b32def& ***!
  \***************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UserTable_vue_vue_type_template_id_75b32def___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./UserTable.vue?vue&type=template&id=75b32def& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/user/UserTable.vue?vue&type=template&id=75b32def&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UserTable_vue_vue_type_template_id_75b32def___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UserTable_vue_vue_type_template_id_75b32def___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./src/vue/partial/user/UserTableRow.vue":
/*!***********************************************!*\
  !*** ./src/vue/partial/user/UserTableRow.vue ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _UserTableRow_vue_vue_type_template_id_42c1b89b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./UserTableRow.vue?vue&type=template&id=42c1b89b& */ "./src/vue/partial/user/UserTableRow.vue?vue&type=template&id=42c1b89b&");
/* harmony import */ var _UserTableRow_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./UserTableRow.vue?vue&type=script&lang=js& */ "./src/vue/partial/user/UserTableRow.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _UserTableRow_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _UserTableRow_vue_vue_type_template_id_42c1b89b___WEBPACK_IMPORTED_MODULE_0__["render"],
  _UserTableRow_vue_vue_type_template_id_42c1b89b___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "src/vue/partial/user/UserTableRow.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./src/vue/partial/user/UserTableRow.vue?vue&type=script&lang=js&":
/*!************************************************************************!*\
  !*** ./src/vue/partial/user/UserTableRow.vue?vue&type=script&lang=js& ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_1_node_modules_vue_loader_lib_index_js_vue_loader_options_UserTableRow_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--1!../../../../node_modules/vue-loader/lib??vue-loader-options!./UserTableRow.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/user/UserTableRow.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_1_node_modules_vue_loader_lib_index_js_vue_loader_options_UserTableRow_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./src/vue/partial/user/UserTableRow.vue?vue&type=template&id=42c1b89b&":
/*!******************************************************************************!*\
  !*** ./src/vue/partial/user/UserTableRow.vue?vue&type=template&id=42c1b89b& ***!
  \******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UserTableRow_vue_vue_type_template_id_42c1b89b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./UserTableRow.vue?vue&type=template&id=42c1b89b& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/user/UserTableRow.vue?vue&type=template&id=42c1b89b&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UserTableRow_vue_vue_type_template_id_42c1b89b___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_UserTableRow_vue_vue_type_template_id_42c1b89b___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ 5:
/*!****************************************************************************************************************!*\
  !*** multi (webpack)-dev-server/client?http://localhost:1236 ./src/js/vendor.js ./src/js/partial/user/user.js ***!
  \****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! D:\Project\share\outsouce_thuenha\html\static\backend\v1\node_modules\webpack-dev-server\client\index.js?http://localhost:1236 */"./node_modules/webpack-dev-server/client/index.js?http://localhost:1236");
__webpack_require__(/*! D:\Project\share\outsouce_thuenha\html\static\backend\v1\src/js/vendor.js */"./src/js/vendor.js");
module.exports = __webpack_require__(/*! D:\Project\share\outsouce_thuenha\html\static\backend\v1\src/js/partial/user/user.js */"./src/js/partial/user/user.js");


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vL3NyYy92dWUvcGFydGlhbC9nbG9iYWwvVGhlTW9kYWxGdWxsU2NyZWVuLnZ1ZSIsIndlYnBhY2s6Ly8vc3JjL3Z1ZS9wYXJ0aWFsL2l0ZW0vQXBwLnZ1ZSIsIndlYnBhY2s6Ly8vc3JjL3Z1ZS9wYXJ0aWFsL3VzZXIvVXNlckFkZFNlY3Rpb24udnVlIiwid2VicGFjazovLy9zcmMvdnVlL3BhcnRpYWwvdXNlci9Vc2VyRWRpdFNlY3Rpb24udnVlIiwid2VicGFjazovLy9zcmMvdnVlL3BhcnRpYWwvdXNlci9Vc2VySW5kZXhTZWN0aW9uLnZ1ZSIsIndlYnBhY2s6Ly8vc3JjL3Z1ZS9wYXJ0aWFsL3VzZXIvVXNlclRhYmxlLnZ1ZSIsIndlYnBhY2s6Ly8vc3JjL3Z1ZS9wYXJ0aWFsL3VzZXIvVXNlclRhYmxlUm93LnZ1ZSIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvdmVlLXZhbGlkYXRlL2Rpc3QvcnVsZXMuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3ZlZS12YWxpZGF0ZS9kaXN0L3ZlZS12YWxpZGF0ZS5lc20uanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3Z1ZS9wYXJ0aWFsL2dsb2JhbC9UaGVNb2RhbEZ1bGxTY3JlZW4udnVlPzNjMjQiLCJ3ZWJwYWNrOi8vLy4vc3JjL3Z1ZS9wYXJ0aWFsL3VzZXIvQXBwLnZ1ZT9iZTkzIiwid2VicGFjazovLy8uL3NyYy92dWUvcGFydGlhbC91c2VyL1VzZXJBZGRTZWN0aW9uLnZ1ZT9iNjMyIiwid2VicGFjazovLy8uL3NyYy92dWUvcGFydGlhbC91c2VyL1VzZXJFZGl0U2VjdGlvbi52dWU/ZGI1YiIsIndlYnBhY2s6Ly8vLi9zcmMvdnVlL3BhcnRpYWwvdXNlci9Vc2VySW5kZXhTZWN0aW9uLnZ1ZT85OGVkIiwid2VicGFjazovLy8uL3NyYy92dWUvcGFydGlhbC91c2VyL1VzZXJUYWJsZS52dWU/NjVmZSIsIndlYnBhY2s6Ly8vLi9zcmMvdnVlL3BhcnRpYWwvdXNlci9Vc2VyVGFibGVSb3cudnVlPzc4YzkiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3Z1ZS1yb3V0ZXIvZGlzdC92dWUtcm91dGVyLmVzbS5qcyIsIndlYnBhY2s6Ly8vKHdlYnBhY2spL2hvdCBzeW5jIG5vbnJlY3Vyc2l2ZSBeXFwuXFwvbG9nJCIsIndlYnBhY2s6Ly8vLi9zcmMvanMvY29tbW9uL3V0aWwvVmVlVmFsaWRhdGVNZXNzYWdlLmpzIiwid2VicGFjazovLy8uL3NyYy9qcy9wYXJ0aWFsL3VzZXIvdXNlci5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvanMvc3RvcmUvdXNlci5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvdnVlL3BhcnRpYWwvZ2xvYmFsL1RoZU1vZGFsRnVsbFNjcmVlbi52dWUiLCJ3ZWJwYWNrOi8vLy4vc3JjL3Z1ZS9wYXJ0aWFsL2dsb2JhbC9UaGVNb2RhbEZ1bGxTY3JlZW4udnVlP2EzY2EiLCJ3ZWJwYWNrOi8vLy4vc3JjL3Z1ZS9wYXJ0aWFsL2dsb2JhbC9UaGVNb2RhbEZ1bGxTY3JlZW4udnVlPzBkZTAiLCJ3ZWJwYWNrOi8vLy4vc3JjL3Z1ZS9wYXJ0aWFsL3VzZXIvQXBwLnZ1ZSIsIndlYnBhY2s6Ly8vLi9zcmMvdnVlL3BhcnRpYWwvdXNlci9BcHAudnVlP2U0ZTUiLCJ3ZWJwYWNrOi8vLy4vc3JjL3Z1ZS9wYXJ0aWFsL3VzZXIvQXBwLnZ1ZT85OGVlIiwid2VicGFjazovLy8uL3NyYy92dWUvcGFydGlhbC91c2VyL1VzZXJBZGRTZWN0aW9uLnZ1ZSIsIndlYnBhY2s6Ly8vLi9zcmMvdnVlL3BhcnRpYWwvdXNlci9Vc2VyQWRkU2VjdGlvbi52dWU/NWE1NSIsIndlYnBhY2s6Ly8vLi9zcmMvdnVlL3BhcnRpYWwvdXNlci9Vc2VyQWRkU2VjdGlvbi52dWU/YzYyMSIsIndlYnBhY2s6Ly8vLi9zcmMvdnVlL3BhcnRpYWwvdXNlci9Vc2VyRWRpdFNlY3Rpb24udnVlIiwid2VicGFjazovLy8uL3NyYy92dWUvcGFydGlhbC91c2VyL1VzZXJFZGl0U2VjdGlvbi52dWU/OTZlOSIsIndlYnBhY2s6Ly8vLi9zcmMvdnVlL3BhcnRpYWwvdXNlci9Vc2VyRWRpdFNlY3Rpb24udnVlP2IxZDYiLCJ3ZWJwYWNrOi8vLy4vc3JjL3Z1ZS9wYXJ0aWFsL3VzZXIvVXNlckluZGV4U2VjdGlvbi52dWUiLCJ3ZWJwYWNrOi8vLy4vc3JjL3Z1ZS9wYXJ0aWFsL3VzZXIvVXNlckluZGV4U2VjdGlvbi52dWU/MDBiZSIsIndlYnBhY2s6Ly8vLi9zcmMvdnVlL3BhcnRpYWwvdXNlci9Vc2VySW5kZXhTZWN0aW9uLnZ1ZT9lY2I1Iiwid2VicGFjazovLy8uL3NyYy92dWUvcGFydGlhbC91c2VyL1VzZXJUYWJsZS52dWUiLCJ3ZWJwYWNrOi8vLy4vc3JjL3Z1ZS9wYXJ0aWFsL3VzZXIvVXNlclRhYmxlLnZ1ZT83MDcxIiwid2VicGFjazovLy8uL3NyYy92dWUvcGFydGlhbC91c2VyL1VzZXJUYWJsZS52dWU/NzE3NSIsIndlYnBhY2s6Ly8vLi9zcmMvdnVlL3BhcnRpYWwvdXNlci9Vc2VyVGFibGVSb3cudnVlIiwid2VicGFjazovLy8uL3NyYy92dWUvcGFydGlhbC91c2VyL1VzZXJUYWJsZVJvdy52dWU/OTZmYiIsIndlYnBhY2s6Ly8vLi9zcmMvdnVlL3BhcnRpYWwvdXNlci9Vc2VyVGFibGVSb3cudnVlPzg0ZDAiXSwibmFtZXMiOlsibmFtZSIsIm1lc3NhZ2VzIiwiX2RlZmF1bHQiLCJuIiwiYWZ0ZXIiLCJ0IiwibGVuZ3RoIiwiYWxwaGFfZGFzaCIsImFscGhhX251bSIsImFscGhhX3NwYWNlcyIsImFscGhhIiwiYmVmb3JlIiwiYmV0d2VlbiIsImNvbmZpcm1lZCIsImNyZWRpdF9jYXJkIiwiZGF0ZV9iZXR3ZWVuIiwiZGF0ZV9mb3JtYXQiLCJkZWNpbWFsIiwiYyIsImRpZ2l0cyIsImRpbWVuc2lvbnMiLCJlbWFpbCIsImV4dCIsImltYWdlIiwiaW5jbHVkZWQiLCJpcCIsIm1heCIsIm1heF92YWx1ZSIsIm1pbWVzIiwibWluIiwibWluX3ZhbHVlIiwiZXhjbHVkZWQiLCJudW1lcmljIiwicmVnZXgiLCJyZXF1aXJlZCIsInNpemUiLCJlIiwiaSIsImgiLCJOdW1iZXIiLCJNYXRoIiwiZmxvb3IiLCJsb2ciLCJwb3ciLCJ0b0ZpeGVkIiwidXJsIiwiVnVlIiwidXNlIiwiVnVleCIsIlZ1ZVJvdXRlciIsInN0b3JlIiwiU3RvcmUiLCJyb3V0ZXIiLCJtb2RlIiwicm91dGVzIiwicGF0aCIsImNvbXBvbmVudCIsIlVzZXJJbmRleFNlY3Rpb24iLCJVc2VyQWRkU2VjdGlvbiIsIlVzZXJFZGl0U2VjdGlvbiIsIk9iamVjdCIsIl9yZW5kZXJVc2VyVGFibGUiLCIkZWwiLCIkIiwicmVuZGVyIiwiQXBwIiwiJG1vdW50IiwiaW5pdCIsImRvY3VtZW50IiwiYWRkRXZlbnRMaXN0ZW5lciIsIl9vYmpFbnRpdHkiLCJ1c2VyX2lkIiwicHJvZmlsZV9pbWFnZSIsImZ1bGxuYW1lIiwicGhvbmUiLCJpc19hY3RpdmVkIiwic3RhdGUiLCJmbGFnIiwiaXNHZXQiLCJhcnJFbnRpdHlMaXN0Iiwib2JqRW50aXR5U2VsZWN0IiwibXV0YXRpb25zIiwicmVzZXRFbnRpdHkiLCJ1cGRhdGVGbGFnIiwicGF5bG9hZCIsInZhbHVlIiwic2V0RW50aXRpZXMiLCJzZXRFbnRpdHkiLCJfaXNFbXB0eSIsInBhc3N3b3JkIiwidXBkYXRlRW50aXR5IiwidHlwZSIsImluZGV4IiwiYXJyRW50aXR5IiwicHVzaCIsInNwbGljZSIsImFjdGlvbnMiLCJnZXRFbnRpdHlBY3QiLCJjb21taXQiLCJQcm9taXNlIiwicmVzb2x2ZSIsInJlamVjdCIsInJlcVBhcmFtcyIsIkFQSSIsImRvbmUiLCJyZXMiLCJpc0Vycm9yQVBJIiwiYXJyVXNlckRldGFpbCIsInJlYWRFbnRpdHlBY3QiLCJhcnJVc2VyTGlzdCIsInVwbG9hZEZpbGVBY3QiLCJmb3JtRGF0YSIsIkZvcm1EYXRhIiwiYXBwZW5kIiwiY29udHJvbGxlciIsImZpbGUiLCJwcm9jZXNzRGF0YSIsImNvbnRlbnRUeXBlIiwidHJpZ2dlck1vZGFsIiwibWVzc2FnZSIsInNldEVudGl0eUFjdCIsImdldHRlcnMiLCJoYXNPd25Qcm9wZXJ0eSIsImdldEVudGl0eUJ5SW5kZXgiLCJjcmVhdGVFbnRpdHlBY3QiLCJ1cGRhdGVFbnRpdHlBY3QiLCJnZXRJbmRleEJ5SUQiLCJkZWxldGVFbnRpdHlBY3QiLCJpZCIsImNvbnNvbGUiLCJmaWx0ZXIiLCJvYmpFbnRpdHkiLCJjdXJyZW50SW5kZXgiLCJfZm9ySW4iXSwibWFwcGluZ3MiOiI7UUFBQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLFFBQVEsb0JBQW9CO1FBQzVCO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EsaUJBQWlCLDRCQUE0QjtRQUM3QztRQUNBO1FBQ0Esa0JBQWtCLDJCQUEyQjtRQUM3QztRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBOzs7UUFHQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMENBQTBDLGdDQUFnQztRQUMxRTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLHdEQUF3RCxrQkFBa0I7UUFDMUU7UUFDQSxpREFBaUQsY0FBYztRQUMvRDs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EseUNBQXlDLGlDQUFpQztRQUMxRSxnSEFBZ0gsbUJBQW1CLEVBQUU7UUFDckk7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwyQkFBMkIsMEJBQTBCLEVBQUU7UUFDdkQsaUNBQWlDLGVBQWU7UUFDaEQ7UUFDQTtRQUNBOztRQUVBO1FBQ0Esc0RBQXNELCtEQUErRDs7UUFFckg7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLGdCQUFnQix1QkFBdUI7UUFDdkM7OztRQUdBO1FBQ0E7UUFDQTtRQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDL0dBO0FBQ0E7QUFFQTtBQUNBLGVBREE7QUFFQSxnQkFGQTtBQUdBLE1BSEEsa0JBR0E7QUFDQTtBQUNBLDBCQURBO0FBRUE7QUFGQTtBQUlBLEdBUkE7QUFTQSxjQVRBO0FBVUEsU0FWQSxxQkFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBRkE7QUFJQTtBQUNBO0FBRUEsMENBQ0E7QUFFQTtBQUNBLGlCQUNBO0FBRUE7QUFDQTtBQUNBLEtBWkE7QUFhQSxHQTdCQTtBQThCQTtBQUNBLFdBREEscUJBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQTlCQSxHOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN0Q0E7QUFDQSxrQkFEQTtBQUVBLGdCQUZBO0FBR0EsTUFIQSxrQkFHQTtBQUNBO0FBQ0EsR0FMQTtBQU1BLGNBTkE7QUFPQSxTQVBBLHFCQU9BLENBRUEsQ0FUQTtBQVVBO0FBVkEsRzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDMk1BO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBR0E7QUFFQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFFQTtBQUNBLDBCQURBO0FBRUE7QUFDQSwwRkFEQTtBQUVBO0FBRkEsR0FGQTtBQU1BLE1BTkEsa0JBTUE7QUFDQTtBQUNBO0FBQ0E7QUFEQSxPQURBO0FBSUE7QUFDQSwyQkFEQTtBQUVBO0FBRkEsT0FKQTtBQVFBO0FBUkE7QUFVQSxHQWpCQTtBQWtCQTtBQUNBLGdCQURBLDBCQUNBO0FBQ0EsOEZBQ0E7QUFFQTtBQUNBLEtBTkE7QUFPQSxtQkFQQSw2QkFPQTtBQUNBO0FBQ0E7QUFUQSxHQWxCQTtBQTZCQSxTQTdCQSxxQkE2QkE7QUFBQTs7QUFDQTtBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBRUE7QUFDQSxLQVBBO0FBU0E7QUFDQTtBQUNBLEdBNUNBO0FBNkNBO0FBQ0EsVUFEQSxvQkFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBLEtBSEE7QUFJQSxZQUpBLHNCQUlBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0EsS0FOQTtBQU9BLFFBUEEsa0JBT0E7QUFDQTtBQUNBLEtBVEE7QUFVQSxVQVZBLGtCQVVBLE9BVkEsRUFVQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxLQXBCQTtBQXFCQSxVQXJCQSxrQkFxQkEsQ0FyQkEsRUFxQkE7QUFBQTs7QUFDQTtBQUVBO0FBQ0Esb0NBREE7QUFFQTtBQUZBLFNBR0EsSUFIQSxDQUdBO0FBQ0E7O0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsT0FWQTtBQVdBO0FBbkNBO0FBN0NBLEc7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ25CQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBRUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRUE7QUFDQSwwQkFEQTtBQUVBO0FBQ0EsMEZBREE7QUFFQTtBQUZBLEdBRkE7QUFNQSxNQU5BLGtCQU1BO0FBQ0E7QUFDQTtBQUNBO0FBREEsT0FEQTtBQUlBO0FBQ0EsMkJBREE7QUFFQTtBQUZBLE9BSkE7QUFRQTtBQVJBO0FBVUEsR0FqQkE7QUFrQkE7QUFDQSxnQkFEQSwwQkFDQTtBQUNBLDhGQUNBO0FBRUE7QUFDQSxLQU5BO0FBT0EsbUJBUEEsNkJBT0E7QUFDQTtBQUNBO0FBVEEsR0FsQkE7QUE2QkEsU0E3QkEscUJBNkJBO0FBQUE7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBRUE7QUFDQSxLQVBBOztBQVNBO0FBQUEsZ0NBQ0Esa0JBREE7QUFBQSxVQUNBLEVBREEsdUJBQ0EsRUFEQTtBQUFBLFVBQ0EsSUFEQSx1QkFDQSxJQURBO0FBRUEsNkZBQ0E7QUFFQTtBQUNBLEtBTkEsTUFNQTtBQUNBO0FBQ0E7QUFDQSxHQWxEQTtBQW1EQTtBQUNBLFVBREEsb0JBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQSxLQUhBO0FBSUEsWUFKQSxzQkFJQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBLEtBTkE7QUFPQSxRQVBBLGtCQU9BO0FBQ0E7QUFDQSxLQVRBO0FBVUEsT0FWQSxlQVVBLEVBVkEsRUFVQTtBQUFBOztBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0EsT0FGQTtBQUdBLEtBZEE7QUFlQSxVQWZBLGtCQWVBLE9BZkEsRUFlQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFDQSxLQTNCQTtBQTRCQSxVQTVCQSxrQkE0QkEsQ0E1QkEsRUE0QkE7QUFBQTs7QUFDQTtBQUVBO0FBQ0Esb0NBREE7QUFFQTtBQUZBLFNBR0EsSUFIQSxDQUdBO0FBQ0E7O0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsT0FWQTtBQVdBO0FBMUNBO0FBbkRBLEc7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2xNQTtBQUVBO0FBQ0EsNEJBREE7QUFFQTtBQUNBO0FBREEsR0FGQTtBQUtBLE1BTEEsa0JBS0E7QUFDQTtBQUNBLEdBUEE7QUFRQSxjQVJBO0FBU0EsU0FUQSxxQkFTQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0EsR0FkQTtBQWVBO0FBQ0EsVUFEQSxvQkFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBLEtBSEE7QUFJQSxZQUpBLHNCQUlBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFOQTtBQWZBLEc7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDSEE7QUFFQTtBQUNBLG9CQURBO0FBRUE7QUFDQTtBQURBLEdBRkE7QUFLQSxNQUxBLGtCQUtBO0FBQ0E7QUFFQSxHQVJBO0FBU0E7QUFDQSxpQkFEQSwyQkFDQTtBQUNBO0FBQ0E7QUFIQSxHQVRBO0FBY0EsU0FkQSxxQkFjQSxDQUVBLENBaEJBO0FBaUJBO0FBakJBLEc7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ29CQTtBQUVBO0FBQ0Esd0JBREE7QUFFQTtBQUNBO0FBQ0EsaUJBREE7QUFFQTtBQUZBLEtBREE7QUFLQTtBQUNBLGlCQURBO0FBRUE7QUFGQTtBQUxBLEdBRkE7QUFZQSxnQkFaQTtBQWFBLE1BYkEsa0JBYUE7QUFDQTtBQUNBO0FBQ0E7QUFEQSxPQURBO0FBSUE7QUFDQTtBQURBO0FBSkE7QUFRQSxHQXRCQTtBQXVCQTtBQUNBLGdCQURBLDBCQUNBO0FBQ0EsNEhBQ0E7QUFFQTtBQUNBO0FBTkEsR0F2QkE7QUErQkEsU0EvQkEscUJBK0JBLENBQ0EsQ0FoQ0E7QUFpQ0E7QUFDQSxZQURBLHNCQUNBO0FBQ0E7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQSxLQUxBO0FBTUEsVUFOQSxrQkFNQSxFQU5BLEVBTUE7QUFBQTs7QUFDQSxlQUNBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBLE9BRkE7QUFHQTtBQWRBO0FBakNBLEc7Ozs7Ozs7Ozs7OztBQ3ZEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsZ0NBQWdDO0FBQ2hDO0FBQ0EsMkNBQTJDLHVCQUF1QixpQkFBaUIsRUFBRSxFQUFFO0FBQ3ZGO0FBQ0E7QUFDQTtBQUNBLHVEQUF1RCwrQkFBK0IsRUFBRTtBQUN4RjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxnQ0FBZ0M7QUFDaEM7QUFDQSwyQ0FBMkMseUJBQXlCLGlCQUFpQixFQUFFLEVBQUU7QUFDekY7QUFDQTtBQUNBO0FBQ0EsMkRBQTJELG1DQUFtQyxFQUFFO0FBQ2hHO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLGdDQUFnQztBQUNoQztBQUNBLDJDQUEyQyx5QkFBeUIsaUJBQWlCLEVBQUUsRUFBRTtBQUN6RjtBQUNBO0FBQ0E7QUFDQSw4REFBOEQsc0NBQXNDLEVBQUU7QUFDdEc7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsZ0NBQWdDO0FBQ2hDO0FBQ0EsMkNBQTJDLHlCQUF5QixpQkFBaUIsRUFBRSxFQUFFO0FBQ3pGO0FBQ0E7QUFDQTtBQUNBLDZEQUE2RCxxQ0FBcUMsRUFBRTtBQUNwRztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSwrQkFBK0I7QUFDL0I7QUFDQSwyQ0FBMkMsMkJBQTJCLHFCQUFxQixFQUFFLEVBQUU7QUFDL0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSwyQ0FBMkMseUJBQXlCLGlCQUFpQixFQUFFLEVBQUU7QUFDekY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQ0FBcUMsdUJBQXVCO0FBQzVELG9DQUFvQyxrRUFBa0U7QUFDdEc7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQixrQkFBa0I7QUFDckM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaURBQWlELDJDQUEyQyxFQUFFO0FBQzlGLDBDQUEwQyxVQUFVLEVBQUU7QUFDdEQsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxzQ0FBc0M7QUFDdEM7QUFDQSxnQ0FBZ0MseUJBQXlCLDZCQUE2QixJQUFJLFFBQVEsSUFBSSxRQUFRLElBQUksUUFBUSxJQUFJLGlDQUFpQyxHQUFHO0FBQ2xLO0FBQ0E7QUFDQTtBQUNBLHNDQUFzQyx3QkFBd0IsRUFBRTtBQUNoRTtBQUNBO0FBQ0EsMkNBQTJDLDZCQUE2QixFQUFFO0FBQzFFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGtDQUFrQyxtQ0FBbUM7QUFDckU7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUIsWUFBWTtBQUMvQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSwyQ0FBMkMsaUNBQWlDLEVBQUU7QUFDOUU7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLDRDQUE0Qyw4QkFBOEIsRUFBRTtBQUM1RTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsNENBQTRDLDhCQUE4QixFQUFFO0FBQzVFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsMkNBQTJDLHVDQUF1QyxFQUFFO0FBQ3BGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdDQUFnQyxzQkFBc0I7QUFDdEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDJDQUEyQyx5QkFBeUIsaUJBQWlCLEVBQUUsRUFBRTtBQUN6RjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0RBQStELHlCQUF5QixXQUFXLEVBQUUsRUFBRTtBQUN2RztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsNENBQTRDLDhCQUE4QixFQUFFO0FBQzVFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwyQ0FBMkMseUJBQXlCLGlCQUFpQixFQUFFLEVBQUU7QUFDekY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtEQUErRCx5QkFBeUIsV0FBVyxFQUFFLEVBQUU7QUFDdkc7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSwyQ0FBMkMseUJBQXlCLGVBQWUsRUFBRSxFQUFFO0FBQ3ZGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLHVDQUF1QyxtQkFBbUI7QUFDMUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0NBQStDLHFDQUFxQyxFQUFFO0FBQ3RGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQixrQkFBa0I7QUFDckM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUU4UDs7Ozs7Ozs7Ozs7OztBQ2pyQjlQO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDc0I7O0FBRXRCO0FBQ0E7QUFDQSwrREFBK0Q7QUFDL0Q7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGdEQUFnRCxPQUFPO0FBQ3ZEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxtQ0FBbUMsTUFBTSw2QkFBNkIsRUFBRSxZQUFZLFdBQVcsRUFBRTtBQUNqRyxrQ0FBa0MsTUFBTSxpQ0FBaUMsRUFBRSxZQUFZLFdBQVcsRUFBRTtBQUNwRywrQkFBK0IsaUVBQWlFLHVCQUF1QixFQUFFLDRCQUE0QjtBQUNySjtBQUNBLEtBQUs7QUFDTDs7QUFFQTtBQUNBLGFBQWEsNkJBQTZCLDBCQUEwQixhQUFhLEVBQUUscUJBQXFCO0FBQ3hHLGdCQUFnQixxREFBcUQsb0VBQW9FLGFBQWEsRUFBRTtBQUN4SixzQkFBc0Isc0JBQXNCLHFCQUFxQixHQUFHO0FBQ3BFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVDQUF1QztBQUN2QyxrQ0FBa0MsU0FBUztBQUMzQyxrQ0FBa0MsV0FBVyxVQUFVO0FBQ3ZELHlDQUF5QyxjQUFjO0FBQ3ZEO0FBQ0EsNkdBQTZHLE9BQU8sVUFBVTtBQUM5SCxnRkFBZ0YsaUJBQWlCLE9BQU87QUFDeEcsd0RBQXdELGdCQUFnQixRQUFRLE9BQU87QUFDdkYsOENBQThDLGdCQUFnQixnQkFBZ0IsT0FBTztBQUNyRjtBQUNBLGlDQUFpQztBQUNqQztBQUNBO0FBQ0EsU0FBUyxZQUFZLGFBQWEsT0FBTyxFQUFFLFVBQVUsV0FBVztBQUNoRSxtQ0FBbUMsU0FBUztBQUM1QztBQUNBOztBQUVBO0FBQ0EsaURBQWlELFFBQVE7QUFDekQsd0NBQXdDLFFBQVE7QUFDaEQsd0RBQXdELFFBQVE7QUFDaEU7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrQkFBK0I7QUFDL0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDLEVBQUU7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVCQUF1QixnQkFBZ0I7QUFDdkM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFlBQVk7QUFDWjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMEJBQTBCLFVBQVU7QUFDcEMsMkJBQTJCLFVBQVUsb0JBQW9CO0FBQ3pEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHdCQUF3Qix1QkFBdUI7QUFDL0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrQ0FBa0MsbUNBQW1DO0FBQ3JFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBLG1CQUFtQixZQUFZO0FBQy9CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUIsWUFBWTtBQUMvQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CLGtCQUFrQjtBQUNyQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsOENBQThDLGVBQWUsRUFBRTtBQUMvRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0JBQStCLElBQUksS0FBSztBQUN4QywyQ0FBMkMsVUFBVTtBQUNyRCxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esd0JBQXdCO0FBQ3hCO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxnQ0FBZ0MsdUNBQXVDO0FBQ3ZFO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdUJBQXVCLFlBQVk7QUFDbkM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHdEQUF3RCx5QkFBeUIsRUFBRTtBQUNuRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUZBQXFGLGtDQUFrQyxFQUFFO0FBQ3pILGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQSw4RUFBOEUsdUNBQXVDLEVBQUU7QUFDdkgsU0FBUztBQUNULGlDQUFpQyxvQkFBb0IsRUFBRTtBQUN2RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0Esc0JBQXNCLFFBQVE7QUFDOUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtCQUErQjtBQUMvQiw2QkFBNkIsc0JBQXNCO0FBQ25EO0FBQ0Esd0NBQXdDO0FBQ3hDO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNkJBQTZCLGNBQWM7QUFDM0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDZEQUE2RCxNQUFNO0FBQ25FO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUVBQXFFO0FBQ3JFLCtEQUErRDtBQUMvRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlCQUF5QjtBQUN6QjtBQUNBLFNBQVM7QUFDVCxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdDQUFnQztBQUNoQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNkJBQTZCO0FBQzdCO0FBQ0EsNkVBQTZFLDJDQUEyQyxFQUFFO0FBQzFIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlCQUF5QjtBQUN6QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUNBQWlDO0FBQ2pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQjtBQUNBLFNBQVM7QUFDVCxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUJBQXlCO0FBQ3pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlDQUFpQztBQUNqQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNkJBQTZCO0FBQzdCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDZCQUE2QjtBQUM3QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUJBQXlCO0FBQ3pCO0FBQ0EsU0FBUztBQUNULEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscURBQXFELGVBQWUsS0FBSyx5REFBeUQ7QUFDbEk7QUFDQTtBQUNBLHdDQUF3QztBQUN4Qyw2QkFBNkI7QUFDN0I7QUFDQTtBQUNBLGtDQUFrQyx3QkFBd0I7QUFDMUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtREFBbUQ7QUFDbkQ7QUFDQSx5QkFBeUI7QUFDekI7QUFDQSxTQUFTO0FBQ1QsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx1REFBdUQsZUFBZSxlQUFlLEtBQUssd0RBQXdEO0FBQ2xKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxvRUFBb0UsdUJBQXVCLEVBQUU7QUFDN0Y7QUFDQTtBQUNBLCtCQUErQixrQ0FBa0M7QUFDakU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxREFBcUQsWUFBWSxpQkFBaUI7QUFDbEY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQix5QkFBeUI7QUFDNUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUEsOEJBQThCO0FBQzlCO0FBQ0EsQ0FBQyxFQUFFO0FBQ0gsd0JBQXdCO0FBQ3hCO0FBQ0EsQ0FBQyxFQUFFO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDJCQUEyQjtBQUMzQjtBQUNBLENBQUMsRUFBRTtBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZSxhQUFhO0FBQzVCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx1R0FBdUcsWUFBWSxpQkFBaUI7QUFDcEk7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQ0FBcUM7QUFDckM7QUFDQSxzQ0FBc0M7QUFDdEM7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1FQUFtRTtBQUNuRTtBQUNBLFNBQVM7QUFDVCxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDRDQUE0QztBQUM1QztBQUNBO0FBQ0E7QUFDQTtBQUNBLHFDQUFxQztBQUNyQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxREFBcUQsMkJBQTJCLEVBQUU7QUFDbEY7QUFDQTtBQUNBO0FBQ0E7QUFDQSxnQkFBZ0I7QUFDaEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdCQUFnQjtBQUNoQjtBQUNBO0FBQ0EsZ0JBQWdCO0FBQ2hCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGtEQUFrRCxpQkFBaUI7QUFDbkU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGtEQUFrRDtBQUNsRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtCQUErQixlQUFlLCtGQUErRixvQkFBb0IsRUFBRTtBQUNuSztBQUNBLDRCQUE0Qix1QkFBdUI7QUFDbkQ7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsU0FBUyxFQUFFO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNEJBQTRCO0FBQzVCLHlCQUF5QiwrQkFBK0I7QUFDeEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlCQUF5QixrQ0FBa0M7QUFDM0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCLGFBQWE7QUFDYixTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFlBQVk7QUFDWjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVCQUF1QjtBQUN2QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUJBQXlCLDJDQUFHO0FBQzVCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxrQ0FBa0MsMEJBQTBCO0FBQzVELFNBQVM7QUFDVDtBQUNBO0FBQ0Esa0NBQWtDLGlDQUFpQztBQUNuRSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0EsYUFBYTtBQUNiLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiLFNBQVM7QUFDVDtBQUNBLDRDQUE0QztBQUM1QztBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYixTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLDRCQUE0Qix1QkFBdUI7QUFDbkQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakIsYUFBYTtBQUNiLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwyQ0FBMkMsZ0JBQWdCO0FBQzNELHdEQUF3RDtBQUN4RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNkJBQTZCO0FBQzdCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUNBQWlDO0FBQ2pDO0FBQ0E7QUFDQSwyQ0FBMkMsaUJBQWlCO0FBQzVELDJDQUEyQyw4Q0FBOEM7QUFDekY7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQixhQUFhO0FBQ2IsU0FBUztBQUNUO0FBQ0EsOEJBQThCLGdDQUFnQyxFQUFFO0FBQ2hFLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQSwwQ0FBMEMsb0JBQW9CO0FBQzlEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2IsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxnQkFBZ0I7QUFDaEI7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrQkFBK0Isa0JBQWtCO0FBQ2pEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrQ0FBK0MsVUFBVSxFQUFFO0FBQzNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUJBQXlCLDJDQUFHO0FBQzVCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDBCQUEwQjtBQUMxQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0Esb0NBQW9DLHVCQUF1QjtBQUMzRDtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakIsb0NBQW9DLHNCQUFzQjtBQUMxRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlCQUF5QjtBQUN6QjtBQUNBLGFBQWE7QUFDYjtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSw4RUFBOEUsc0JBQXNCO0FBQ3BHLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxrQ0FBa0MsbUJBQW1CO0FBQ3JEO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNENBQTRDLHNCQUFzQjtBQUNsRSxrREFBa0QsNEJBQTRCO0FBQzlFO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBLGtDQUFrQyxtQkFBbUI7QUFDckQ7QUFDQTtBQUNBO0FBQ0E7QUFDQSw4REFBOEQsb0JBQW9CLEVBQUU7QUFDcEY7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0Esd0NBQXdDO0FBQ3hDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxzREFBc0Qsb0JBQW9CLEVBQUU7QUFDNUUscURBQXFELHlFQUF5RSxnQkFBZ0IsRUFBRSxFQUFFLEVBQUUsdUNBQXVDLG9CQUFvQixFQUFFLHNCQUFzQixzQkFBc0IsaUJBQWlCLEVBQUUsRUFBRTtBQUNsUjtBQUNBO0FBQ0EsOEVBQThFLFVBQVUsRUFBRTtBQUMxRjtBQUNBLGlCQUFpQjtBQUNqQixhQUFhO0FBQ2IsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiLDZGQUE2RixvQkFBb0IsRUFBRTtBQUNuSCxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxvQkFBb0IsSUFBcUM7QUFDekQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBLDhCQUE4QixxQkFBcUI7QUFDbkQ7QUFDQTtBQUNBO0FBQ0E7QUFDQSwwQkFBMEI7QUFDMUI7QUFDQSw2QkFBNkI7QUFDN0IsNEJBQTRCO0FBQzVCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBbUM7QUFDbkM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxxREFBcUQsZ0JBQWdCO0FBQ3JFLGlEQUFpRCx3QkFBd0I7QUFDekU7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBOztBQUVBOztBQUUrSTs7Ozs7Ozs7Ozs7OztBQ3YyRC9JO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUssNkNBQTZDLGlCQUFpQixFQUFFO0FBQ3JFO0FBQ0EsaUJBQWlCLHNDQUFzQyxtQkFBbUIsRUFBRTtBQUM1RSxtQkFBbUIsK0JBQStCO0FBQ2xELHFCQUFxQiw0QkFBNEI7QUFDakQsdUJBQXVCLHVDQUF1QztBQUM5RCx5QkFBeUIsb0RBQW9EO0FBQzdFLDJCQUEyQixtQ0FBbUM7QUFDOUQsNkJBQTZCLCtCQUErQjtBQUM1RDtBQUNBO0FBQ0EsOEJBQThCLGtDQUFrQztBQUNoRTtBQUNBO0FBQ0E7QUFDQSw2QkFBNkIsNkJBQTZCO0FBQzFEO0FBQ0E7QUFDQTtBQUNBLCtCQUErQiw2QkFBNkI7QUFDNUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx5QkFBeUI7QUFDekI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esd0JBQXdCLHFDQUFxQztBQUM3RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2Ysd0JBQXdCLDBCQUEwQjtBQUNsRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxzQkFBc0IsMENBQTBDO0FBQ2hFO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7QUM5RUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7QUNQQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG9CQUFvQixpQ0FBaUM7QUFDckQsZUFBZSw2QkFBNkI7QUFDNUMsZ0JBQWdCLDhCQUE4QjtBQUM5QztBQUNBLGlCQUFpQixrQ0FBa0M7QUFDbkQsbUJBQW1CLDRDQUE0QztBQUMvRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxzQkFBc0IsWUFBWTtBQUNsQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2Isc0JBQXNCLCtCQUErQjtBQUNyRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsZ0NBQWdDO0FBQ2pEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTyxzQkFBc0I7QUFDN0I7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrQkFBa0IsYUFBYTtBQUMvQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckI7QUFDQSxpQ0FBaUMsMEJBQTBCO0FBQzNEO0FBQ0E7QUFDQSx3Q0FBd0Msa0NBQWtDO0FBQzFFLGtDQUFrQztBQUNsQyx5QkFBeUI7QUFDekI7QUFDQTtBQUNBO0FBQ0Esa0NBQWtDLGVBQWU7QUFDakQsK0JBQStCO0FBQy9CLHlCQUF5QjtBQUN6QjtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlCQUF5QixtREFBbUQ7QUFDNUU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw2QkFBNkI7QUFDN0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esa0RBQWtEO0FBQ2xELHlDQUF5QztBQUN6QztBQUNBO0FBQ0E7QUFDQSxpREFBaUQsNEJBQTRCO0FBQzdFO0FBQ0E7QUFDQSx5Q0FBeUM7QUFDekM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDJDQUEyQztBQUMzQztBQUNBO0FBQ0E7QUFDQSwyQ0FBMkM7QUFDM0M7QUFDQTtBQUNBLDJDQUEyQztBQUMzQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx5Q0FBeUM7QUFDekM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx5Q0FBeUM7QUFDekM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMkJBQTJCO0FBQzNCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNkJBQTZCO0FBQzdCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGtEQUFrRDtBQUNsRCx5Q0FBeUM7QUFDekM7QUFDQTtBQUNBO0FBQ0EsaURBQWlELDRCQUE0QjtBQUM3RTtBQUNBO0FBQ0EseUNBQXlDO0FBQ3pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwyQ0FBMkM7QUFDM0M7QUFDQTtBQUNBO0FBQ0EsMkNBQTJDO0FBQzNDO0FBQ0E7QUFDQSwyQ0FBMkM7QUFDM0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUNBQXlDO0FBQ3pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUNBQXlDO0FBQ3pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDJCQUEyQjtBQUMzQjtBQUNBLHFDQUFxQywwQkFBMEI7QUFDL0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtCQUErQjtBQUMvQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtCQUErQjtBQUMvQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQ0FBcUM7QUFDckM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUNBQW1DO0FBQ25DO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNkJBQTZCO0FBQzdCO0FBQ0EseUNBQXlDLFNBQVMsbUJBQW1CLEVBQUU7QUFDdkU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUJBQXlCLHFCQUFxQjtBQUM5QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDZCQUE2QjtBQUM3QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrREFBa0Q7QUFDbEQseUNBQXlDO0FBQ3pDO0FBQ0E7QUFDQTtBQUNBLGlEQUFpRCw0QkFBNEI7QUFDN0U7QUFDQTtBQUNBLHlDQUF5QztBQUN6QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMkNBQTJDO0FBQzNDO0FBQ0E7QUFDQTtBQUNBLDJDQUEyQztBQUMzQztBQUNBO0FBQ0EsMkNBQTJDO0FBQzNDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlDQUF5QztBQUN6QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlDQUF5QztBQUN6QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtCQUErQjtBQUMvQjtBQUNBO0FBQ0EsMkJBQTJCO0FBQzNCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNkJBQTZCO0FBQzdCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGtEQUFrRDtBQUNsRCx5Q0FBeUM7QUFDekM7QUFDQTtBQUNBO0FBQ0EsaURBQWlELDRCQUE0QjtBQUM3RTtBQUNBO0FBQ0EseUNBQXlDO0FBQ3pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwyQ0FBMkM7QUFDM0M7QUFDQTtBQUNBO0FBQ0EsMkNBQTJDO0FBQzNDO0FBQ0E7QUFDQSwyQ0FBMkM7QUFDM0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUNBQXlDO0FBQ3pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUNBQXlDO0FBQ3pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDJCQUEyQjtBQUMzQjtBQUNBLHFDQUFxQyw0QkFBNEI7QUFDakU7QUFDQTtBQUNBO0FBQ0E7QUFDQSx3Q0FBd0M7QUFDeEMsK0JBQStCO0FBQy9CO0FBQ0E7QUFDQTtBQUNBLHVDQUF1Qyw0QkFBNEI7QUFDbkUsdUNBQXVDLHFDQUFxQztBQUM1RTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUNBQWlDO0FBQ2pDLDJDQUEyQyw4QkFBOEI7QUFDekU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0JBQStCO0FBQy9CO0FBQ0E7QUFDQTtBQUNBLHFDQUFxQyx3QkFBd0I7QUFDN0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlDQUFpQztBQUNqQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrQkFBK0I7QUFDL0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdUNBQXVDO0FBQ3ZDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx3Q0FBd0MsNkJBQTZCO0FBQ3JFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtCQUErQjtBQUMvQjtBQUNBLHlDQUF5QyxxQ0FBcUM7QUFDOUU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTyx5Q0FBeUMsbUJBQW1CLEVBQUU7QUFDckUsZ0JBQWdCLCtCQUErQjtBQUMvQztBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVCQUF1Qix3Q0FBd0M7QUFDL0QsZUFBZSxxQ0FBcUM7QUFDcEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7QUM1cEJBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esb0JBQW9CLGlDQUFpQztBQUNyRCxlQUFlLDZCQUE2QjtBQUM1QyxnQkFBZ0IsOEJBQThCO0FBQzlDO0FBQ0EsaUJBQWlCLGtDQUFrQztBQUNuRCxtQkFBbUIsNENBQTRDO0FBQy9EO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNCQUFzQixZQUFZO0FBQ2xDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYixzQkFBc0IsK0JBQStCO0FBQ3JEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixnQ0FBZ0M7QUFDakQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPLHNCQUFzQjtBQUM3QjtBQUNBO0FBQ0E7QUFDQTtBQUNBLGtCQUFrQixhQUFhO0FBQy9CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQjtBQUNBLGlDQUFpQywwQkFBMEI7QUFDM0Q7QUFDQTtBQUNBLHdDQUF3QyxrQ0FBa0M7QUFDMUUsa0NBQWtDO0FBQ2xDLHlCQUF5QjtBQUN6QjtBQUNBO0FBQ0E7QUFDQSxrQ0FBa0MsZUFBZTtBQUNqRCwrQkFBK0I7QUFDL0IseUJBQXlCO0FBQ3pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUJBQXlCLG1EQUFtRDtBQUM1RTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDZCQUE2QjtBQUM3QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrREFBa0Q7QUFDbEQseUNBQXlDO0FBQ3pDO0FBQ0E7QUFDQTtBQUNBLGlEQUFpRCw0QkFBNEI7QUFDN0U7QUFDQTtBQUNBLHlDQUF5QztBQUN6QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMkNBQTJDO0FBQzNDO0FBQ0E7QUFDQTtBQUNBLDJDQUEyQztBQUMzQztBQUNBO0FBQ0EsMkNBQTJDO0FBQzNDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlDQUF5QztBQUN6QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlDQUF5QztBQUN6QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwyQkFBMkI7QUFDM0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw2QkFBNkI7QUFDN0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esa0RBQWtEO0FBQ2xELHlDQUF5QztBQUN6QztBQUNBO0FBQ0E7QUFDQSxpREFBaUQsNEJBQTRCO0FBQzdFO0FBQ0E7QUFDQSx5Q0FBeUM7QUFDekM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDJDQUEyQztBQUMzQztBQUNBO0FBQ0E7QUFDQSwyQ0FBMkM7QUFDM0M7QUFDQTtBQUNBLDJDQUEyQztBQUMzQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx5Q0FBeUM7QUFDekM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx5Q0FBeUM7QUFDekM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMkJBQTJCO0FBQzNCO0FBQ0EscUNBQXFDLDBCQUEwQjtBQUMvRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0JBQStCO0FBQy9CO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0JBQStCO0FBQy9CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFDQUFxQztBQUNyQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBbUM7QUFDbkM7QUFDQTtBQUNBO0FBQ0E7QUFDQSw2QkFBNkI7QUFDN0I7QUFDQSx5Q0FBeUMsU0FBUyxtQkFBbUIsRUFBRTtBQUN2RTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx5QkFBeUIscUJBQXFCO0FBQzlDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNkJBQTZCO0FBQzdCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGtEQUFrRDtBQUNsRCx5Q0FBeUM7QUFDekM7QUFDQTtBQUNBO0FBQ0EsaURBQWlELDRCQUE0QjtBQUM3RTtBQUNBO0FBQ0EseUNBQXlDO0FBQ3pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwyQ0FBMkM7QUFDM0M7QUFDQTtBQUNBO0FBQ0EsMkNBQTJDO0FBQzNDO0FBQ0E7QUFDQSwyQ0FBMkM7QUFDM0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUNBQXlDO0FBQ3pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUNBQXlDO0FBQ3pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0JBQStCO0FBQy9CO0FBQ0E7QUFDQSwyQkFBMkI7QUFDM0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw2QkFBNkI7QUFDN0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esa0RBQWtEO0FBQ2xELHlDQUF5QztBQUN6QztBQUNBO0FBQ0E7QUFDQSxpREFBaUQsNEJBQTRCO0FBQzdFO0FBQ0E7QUFDQSx5Q0FBeUM7QUFDekM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDJDQUEyQztBQUMzQztBQUNBO0FBQ0E7QUFDQSwyQ0FBMkM7QUFDM0M7QUFDQTtBQUNBLDJDQUEyQztBQUMzQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx5Q0FBeUM7QUFDekM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx5Q0FBeUM7QUFDekM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMkJBQTJCO0FBQzNCO0FBQ0EscUNBQXFDLDRCQUE0QjtBQUNqRTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHdDQUF3QztBQUN4QywrQkFBK0I7QUFDL0I7QUFDQTtBQUNBO0FBQ0EsdUNBQXVDLDRCQUE0QjtBQUNuRSx1Q0FBdUMscUNBQXFDO0FBQzVFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQ0FBaUM7QUFDakMsMkNBQTJDLDhCQUE4QjtBQUN6RTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrQkFBK0I7QUFDL0I7QUFDQTtBQUNBO0FBQ0EscUNBQXFDLHdCQUF3QjtBQUM3RDtBQUNBO0FBQ0E7QUFDQTtBQUNBLHdDQUF3Qyw2QkFBNkI7QUFDckU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0JBQStCO0FBQy9CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQ0FBaUM7QUFDakM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0JBQStCO0FBQy9CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVDQUF1QztBQUN2QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPLHlDQUF5QyxtQkFBbUIsRUFBRTtBQUNyRSxnQkFBZ0IsK0JBQStCO0FBQy9DO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdUJBQXVCLHdDQUF3QztBQUMvRCxlQUFlLHFDQUFxQztBQUNwRDtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7OztBQ3pwQkE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxvQkFBb0IsaUNBQWlDO0FBQ3JELGVBQWUsNkJBQTZCO0FBQzVDLGdCQUFnQiw4QkFBOEI7QUFDOUM7QUFDQSxpQkFBaUIsa0NBQWtDO0FBQ25ELG1CQUFtQiw0Q0FBNEM7QUFDL0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esc0JBQXNCLFlBQVk7QUFDbEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYixzQkFBc0IsK0JBQStCO0FBQ3JEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixnQ0FBZ0M7QUFDakQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsc0JBQXNCO0FBQ3JDLGlCQUFpQiwyQkFBMkI7QUFDNUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPLHlDQUF5QyxtQkFBbUIsRUFBRTtBQUNyRSxnQkFBZ0IsK0JBQStCO0FBQy9DO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdUJBQXVCLHdDQUF3QztBQUMvRCxlQUFlLCtCQUErQjtBQUM5QztBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7OztBQ3pFQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLLHdDQUF3QyxtQkFBbUIsRUFBRTtBQUNsRTtBQUNBO0FBQ0E7QUFDQSxTQUFTLG9EQUFvRCxlQUFlLEVBQUU7QUFDOUU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNEJBQTRCO0FBQzVCLG1CQUFtQjtBQUNuQjtBQUNBLGVBQWU7QUFDZjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQiw4QkFBOEI7QUFDbkQ7QUFDQTtBQUNBLHNCQUFzQjtBQUN0QixhQUFhO0FBQ2I7QUFDQSx5QkFBeUIsU0FBUyxxQkFBcUIsRUFBRTtBQUN6RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7OztBQy9EQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLDRCQUE0QjtBQUM3QyxtQkFBbUIsMkJBQTJCO0FBQzlDLHFCQUFxQiwyQkFBMkI7QUFDaEQsdUJBQXVCLFNBQVMsaUNBQWlDLEVBQUU7QUFDbkU7QUFDQTtBQUNBLHFCQUFxQixzQkFBc0I7QUFDM0Msd0JBQXdCLHVCQUF1QjtBQUMvQztBQUNBO0FBQ0E7QUFDQSx3QkFBd0IsMkJBQTJCO0FBQ25EO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLHlEQUF5RDtBQUMxRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsd0RBQXdEO0FBQ3pFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsY0FBYywwQ0FBMEM7QUFDeEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNULGtCQUFrQiwyQkFBMkI7QUFDN0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQiwwQkFBMEI7QUFDM0MscUJBQXFCLFNBQVMsc0NBQXNDLEVBQUU7QUFDdEU7QUFDQSxxQkFBcUIsU0FBUyxxQkFBcUIsRUFBRTtBQUNyRDtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7OztBQ3JHQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsTUFBTSxLQUFxQztBQUMzQztBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esd0VBQXdFOztBQUV4RTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEtBQUssNkJBQTZCO0FBQ2xDO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSwwQ0FBMEM7QUFDMUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsVUFBVSxJQUFxQztBQUMvQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0EsMENBQTBDLDJDQUEyQztBQUNyRjs7QUFFQTtBQUNBO0FBQ0E7QUFDQSw2QkFBNkI7QUFDN0I7QUFDQSx5QkFBeUI7O0FBRXpCOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSCxJQUFJLEtBQXFDO0FBQ3pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7O0FBRUE7QUFDQSxHQUFHLHVCQUF1QixxQkFBcUIsRUFBRTtBQUNqRDtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBLHVDQUF1QztBQUN2QztBQUNBO0FBQ0E7QUFDQSxpQ0FBaUM7QUFDakM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx3QkFBd0I7QUFDeEIsc0JBQXNCOztBQUV0QjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGlCQUFpQixxQkFBcUI7QUFDdEM7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxVQUFVO0FBQ1Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsWUFBWSxPQUFPO0FBQ25CLFlBQVksUUFBUTtBQUNwQixZQUFZO0FBQ1o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsWUFBWSxPQUFPO0FBQ25CLFlBQVksUUFBUTtBQUNwQixZQUFZO0FBQ1o7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsWUFBWTtBQUNaLFlBQVk7QUFDWjtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxZQUFZO0FBQ1osWUFBWTtBQUNaO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLGlCQUFpQixtQkFBbUI7QUFDcEM7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxtQkFBbUIsbUJBQW1CO0FBQ3RDOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTs7QUFFQSx1QkFBdUIsa0JBQWtCO0FBQ3pDOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFlBQVksT0FBTztBQUNuQixZQUFZO0FBQ1o7QUFDQTtBQUNBLG1DQUFtQztBQUNuQzs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxZQUFZLE9BQU87QUFDbkIsWUFBWTtBQUNaO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFlBQVksUUFBUTtBQUNwQixZQUFZLE1BQU07QUFDbEIsWUFBWTtBQUNaO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsWUFBWSxPQUFPO0FBQ25CLFlBQVk7QUFDWjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxZQUFZLFFBQVE7QUFDcEIsWUFBWSxPQUFPO0FBQ25CLFlBQVk7QUFDWjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLG1CQUFtQixtQkFBbUI7QUFDdEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxZQUFZLE9BQU87QUFDbkIsWUFBWSxNQUFNO0FBQ2xCLFlBQVksUUFBUTtBQUNwQixZQUFZO0FBQ1o7QUFDQTtBQUNBOztBQUVBLGlCQUFpQixpQkFBaUI7QUFDbEM7QUFDQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFlBQVksT0FBTztBQUNuQixZQUFZLE9BQU87QUFDbkIsWUFBWSxRQUFRO0FBQ3BCLFlBQVk7QUFDWjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxZQUFZLE9BQU87QUFDbkIsWUFBWSxnQkFBZ0I7QUFDNUIsWUFBWSxRQUFRO0FBQ3BCLFlBQVk7QUFDWjtBQUNBO0FBQ0E7QUFDQSx5QkFBeUIsUUFBUTtBQUNqQztBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLGlCQUFpQixtQkFBbUI7QUFDcEM7O0FBRUE7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsY0FBYyw2REFBNkQ7QUFDM0U7QUFDQSxZQUFZLHNCQUFzQjtBQUNsQyxZQUFZLGdCQUFnQjtBQUM1QixZQUFZLFFBQVE7QUFDcEIsWUFBWTtBQUNaO0FBQ0E7QUFDQTtBQUNBLHlCQUF5QixRQUFRO0FBQ2pDO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQSwyQ0FBMkMsT0FBTztBQUNsRDs7QUFFQTtBQUNBLG9DQUFvQyxPQUFPLHVCQUF1QixPQUFPO0FBQ3pFOztBQUVBLG1DQUFtQyxPQUFPLHVCQUF1QixPQUFPO0FBQ3hFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSw0Q0FBNEMsNkJBQTZCO0FBQ3pFLDJCQUEyQiw4QkFBOEI7O0FBRXpELDJCQUEyQixlQUFlO0FBQzFDLEdBQUc7QUFDSCxRQUFRLElBQXFDO0FBQzdDO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx3Q0FBd0MsWUFBWTtBQUNwRDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0gsb0JBQW9CO0FBQ3BCOztBQUVBO0FBQ0E7QUFDQSxvQkFBb0I7QUFDcEI7QUFDQSxpQ0FBaUM7QUFDakM7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLLFVBQVUsSUFBcUM7QUFDcEQ7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxjQUFjO0FBQ2Q7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQLEtBQUs7QUFDTDtBQUNBOztBQUVBLGdCQUFnQjs7QUFFaEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTzs7QUFFUDtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1AsWUFBWSxJQUFxQztBQUNqRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMkRBQTJEO0FBQzNEO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLG9CQUFvQjtBQUNwQixLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVDQUF1QztBQUN2QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7O0FBRUEsOENBQThDO0FBQzlDO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EseURBQXlEO0FBQ3pEO0FBQ0EsMkJBQTJCO0FBQzNCO0FBQ0EsaURBQWlEO0FBQ2pEO0FBQ0E7QUFDQTtBQUNBLHFDQUFxQztBQUNyQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUIscUJBQXFCO0FBQ3hDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0EsMENBQTBDO0FBQzFDOztBQUVBOztBQUVBLDRCQUE0Qix3QkFBd0I7O0FBRXBEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBLDBCQUEwQjtBQUMxQixHQUFHOztBQUVIO0FBQ0EsMEJBQTBCO0FBQzFCLEdBQUc7O0FBRUg7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQSxzQ0FBc0MsT0FBTztBQUM3QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsTUFBTSxJQUFzQztBQUM1QztBQUNBO0FBQ0E7QUFDQSwrQkFBK0IsaUVBQWlFLEVBQUU7O0FBRWxHO0FBQ0EsaURBQWlELHNCQUFzQixFQUFFO0FBQ3pFO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxNQUFNLElBQXFDO0FBQzNDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxxQ0FBcUMsMkJBQTJCO0FBQ2hFLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMEJBQTBCO0FBQzFCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxRQUFRLElBQXFDO0FBQzdDO0FBQ0E7QUFDQTtBQUNBLDhDQUE4QyxpQ0FBaUMsRUFBRTtBQUNqRjtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlEQUF5RDtBQUN6RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxtQkFBbUIsb0JBQW9CO0FBQ3ZDO0FBQ0EsVUFBVSxLQUFxQztBQUMvQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBQUssVUFBVSxLQUFxQztBQUNwRDtBQUNBO0FBQ0E7QUFDQSxZQUFZLDBEQUEwRDtBQUN0RTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsTUFBTSxJQUFxQztBQUMzQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdCQUFnQixnQ0FBZ0M7QUFDaEQsd0JBQXdCO0FBQ3hCLHVCQUF1QjtBQUN2QjtBQUNBOztBQUVBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLFVBQVUsSUFBcUM7QUFDL0M7QUFDQTtBQUNBLG9CQUFvQjtBQUNwQjtBQUNBLGdDQUFnQyxzQkFBc0IsRUFBRTtBQUN4RCw2QkFBNkIsaUJBQWlCLEVBQUU7O0FBRWhEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EscUJBQXFCLHFCQUFxQjtBQUMxQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0Esa0JBQWtCO0FBQ2xCOztBQUVBO0FBQ0EsVUFBVSxJQUFxQztBQUMvQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFVBQVUsSUFBcUM7QUFDL0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUCxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1AsS0FBSztBQUNMLFVBQVUsSUFBcUM7QUFDL0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7O0FBRUEsaUNBQWlDLFNBQVM7QUFDMUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0JBQStCLHFCQUFxQjtBQUNwRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxNQUFNLElBQXFDO0FBQzNDO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBLGNBQWMsSUFBcUM7QUFDbkQ7QUFDQTtBQUNBLFNBQVM7QUFDVCxLQUFLO0FBQ0w7QUFDQTtBQUNBLEdBQUc7QUFDSDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw0QkFBNEIscUJBQXFCO0FBQ2pELEtBQUs7QUFDTCx5QkFBeUIsa0NBQWtDO0FBQzNEO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNULE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTOztBQUVUO0FBQ0E7QUFDQSxVQUFVLEtBQXFDO0FBQy9DO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7O0FBRVQ7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7O0FBRUwsb0JBQW9CLFFBQVE7QUFDNUI7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseURBQXlEO0FBQ3pEO0FBQ0E7QUFDQTtBQUNBLE1BQU0sRUFBRTtBQUNSLEdBQUc7QUFDSDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVCxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0NBQWdDLHNCQUFzQixFQUFFO0FBQ3hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0NBQWdDLFlBQVk7QUFDNUM7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQLEtBQUs7QUFDTDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLCtCQUErQixpQ0FBaUM7QUFDaEU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWCxTQUFTO0FBQ1Q7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsU0FBUztBQUN0QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNDQUFzQywwQ0FBMEMsRUFBRTtBQUNsRjtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUCxLQUFLO0FBQ0w7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGtCQUFrQjs7QUFFbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSyxPQUFPLHdCQUF3QjtBQUNwQyxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxDQUFDOztBQUVEOzs7O0FBSUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFVBQVUsSUFBcUM7QUFDL0M7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsMEJBQTBCLGdCQUFnQixxQkFBcUI7O0FBRS9EO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLEVBQUUsS0FBcUM7QUFDdkM7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQiw4QkFBOEI7QUFDbkQ7QUFDQTtBQUNBLDZCQUE2QixxQ0FBcUM7QUFDbEUsR0FBRzs7QUFFSDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLG1CQUFtQjtBQUNwQztBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVlLHdFQUFTLEVBQUM7Ozs7Ozs7Ozs7OztBQ2owRnpCO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGtFOzs7Ozs7Ozs7Ozs7QUN0QkE7QUFBZTtBQUNiQSxNQUFJLEVBQUUsSUFETztBQUViQyxVQUFRLEVBQUU7QUFDUkMsWUFBUSxFQUFFLGtCQUFVQyxDQUFWLEVBQWE7QUFDckIsYUFBTyxpQkFBaUJBLENBQWpCLEdBQXFCLGNBQTVCO0FBQ0QsS0FITztBQUlSQyxTQUFLLEVBQUUsZUFBVUQsQ0FBVixFQUFhRSxDQUFiLEVBQWdCO0FBQ3JCLGFBQU9GLENBQUMsR0FBRyxzQkFBSixHQUE2QkUsQ0FBQyxDQUFDQyxNQUEvQixHQUF3QyxHQUEvQztBQUNELEtBTk87QUFPUkMsY0FBVSxFQUFFLG9CQUFVSixDQUFWLEVBQWE7QUFDdkIsYUFBT0EsQ0FBQyxHQUFHLGtGQUFYO0FBQ0QsS0FUTztBQVVSSyxhQUFTLEVBQUUsbUJBQVVMLENBQVYsRUFBYTtBQUN0QixhQUFPQSxDQUFDLEdBQUcsdUNBQVg7QUFDRCxLQVpPO0FBYVJNLGdCQUFZLEVBQUUsc0JBQVVOLENBQVYsRUFBYTtBQUN6QixhQUFPQSxDQUFDLEdBQUcsNENBQVg7QUFDRCxLQWZPO0FBZ0JSTyxTQUFLLEVBQUUsZUFBVVAsQ0FBVixFQUFhO0FBQ2xCLGFBQU9BLENBQUMsR0FBRyxpQ0FBWDtBQUNELEtBbEJPO0FBbUJSUSxVQUFNLEVBQUUsZ0JBQVVSLENBQVYsRUFBYUUsQ0FBYixFQUFnQjtBQUN0QixhQUFPRixDQUFDLEdBQUcsd0JBQUosR0FBK0JFLENBQUMsQ0FBQ0MsTUFBakMsR0FBMEMsR0FBakQ7QUFDRCxLQXJCTztBQXNCUk0sV0FBTyxFQUFFLGlCQUFVVCxDQUFWLEVBQWFFLENBQWIsRUFBZ0I7QUFDdkIsYUFBT0YsQ0FBQyxHQUFHLHlDQUFKLEdBQWdERSxDQUFDLENBQUNDLE1BQWxELEdBQTJELE1BQTNELEdBQW9FRCxDQUFDLENBQUUsQ0FBRixDQUFyRSxHQUE2RSxHQUFwRjtBQUNELEtBeEJPO0FBeUJSUSxhQUFTLEVBQUUsbUJBQVVWLENBQVYsRUFBYUUsQ0FBYixFQUFnQjtBQUN6QixhQUFPRixDQUFDLEdBQUcsWUFBSixHQUFtQkUsQ0FBQyxDQUFDQyxNQUFyQixHQUE4QixHQUFyQztBQUNELEtBM0JPO0FBNEJSUSxlQUFXLEVBQUUscUJBQVVYLENBQVYsRUFBYTtBQUN4QixhQUFPLGFBQWFBLENBQWIsR0FBaUIsbUJBQXhCO0FBQ0QsS0E5Qk87QUErQlJZLGdCQUFZLEVBQUUsc0JBQVVaLENBQVYsRUFBYUUsQ0FBYixFQUFnQjtBQUM1QixhQUFPRixDQUFDLEdBQUcsMENBQUosR0FBaURFLENBQUMsQ0FBQ0MsTUFBbkQsR0FBNEQsTUFBNUQsR0FBcUVELENBQUMsQ0FBRSxDQUFGLENBQXRFLEdBQThFLEdBQXJGO0FBQ0QsS0FqQ087QUFrQ1JXLGVBQVcsRUFBRSxxQkFBVWIsQ0FBVixFQUFhRSxDQUFiLEVBQWdCO0FBQzNCLGFBQU9GLENBQUMsR0FBRyxrQ0FBSixHQUF5Q0UsQ0FBQyxDQUFDQyxNQUEzQyxHQUFvRCxHQUEzRDtBQUNELEtBcENPO0FBcUNSVyxXQUFPLEVBQUUsaUJBQVVkLENBQVYsRUFBYUUsQ0FBYixFQUFnQjtBQUN2QixXQUFLLENBQUwsS0FBV0EsQ0FBWCxLQUFpQkEsQ0FBQyxHQUFHLEVBQXJCO0FBQ0EsVUFBSWEsQ0FBQyxHQUFHYixDQUFDLENBQUNDLE1BQVY7QUFDQSxhQUFPLEtBQUssQ0FBTCxLQUFXWSxDQUFYLEtBQWlCQSxDQUFDLEdBQUcsR0FBckIsR0FBMkJmLENBQUMsR0FBRyxpREFBSixJQUF5RCxRQUFRZSxDQUFSLEdBQVksRUFBWixHQUFpQkEsQ0FBMUUsSUFBK0UsR0FBakg7QUFDRCxLQXpDTztBQTBDUkMsVUFBTSxFQUFFLGdCQUFVaEIsQ0FBVixFQUFhRSxDQUFiLEVBQWdCO0FBQ3RCLGFBQU8sWUFBWUYsQ0FBWixHQUFnQiw4REFBaEIsR0FBaUZFLENBQUMsQ0FBQ0MsTUFBbkYsR0FBNEYsR0FBbkc7QUFDRCxLQTVDTztBQTZDUmMsY0FBVSxFQUFFLG9CQUFVakIsQ0FBVixFQUFhRSxDQUFiLEVBQWdCO0FBQzFCLGFBQU9GLENBQUMsR0FBRyxzQkFBSixHQUE2QkUsQ0FBQyxDQUFDQyxNQUEvQixHQUF3Qyx1QkFBeEMsR0FBa0VELENBQUMsQ0FBRSxDQUFGLENBQW5FLEdBQTJFLFVBQWxGO0FBQ0QsS0EvQ087QUFnRFJnQixTQUFLLEVBQUUsZUFBVWxCLENBQVYsRUFBYTtBQUNsQixhQUFPQSxDQUFDLEdBQUcsb0NBQVg7QUFDRCxLQWxETztBQW1EUm1CLE9BQUcsRUFBRSxhQUFVbkIsQ0FBVixFQUFhO0FBQ2hCLGFBQU9BLENBQUMsR0FBRyxtQkFBWDtBQUNELEtBckRPO0FBc0RSb0IsU0FBSyxFQUFFLGVBQVVwQixDQUFWLEVBQWE7QUFDbEIsYUFBTyxZQUFZQSxDQUFaLEdBQWdCLG1CQUF2QjtBQUNELEtBeERPO0FBeURScUIsWUFBUSxFQUFFLGtCQUFVckIsQ0FBVixFQUFhO0FBQ3JCLGFBQU9BLENBQUMsR0FBRyx1QkFBWDtBQUNELEtBM0RPO0FBNERSc0IsTUFBRSxFQUFFLFlBQVV0QixDQUFWLEVBQWE7QUFDZixhQUFPQSxDQUFDLEdBQUcsaUNBQVg7QUFDRCxLQTlETztBQStEUnVCLE9BQUcsRUFBRSxhQUFVdkIsQ0FBVixFQUFhRSxDQUFiLEVBQWdCO0FBQ25CLGFBQU9GLENBQUMsR0FBRywwQkFBSixHQUFpQ0UsQ0FBQyxDQUFDQyxNQUFuQyxHQUE0QyxTQUFuRDtBQUNELEtBakVPO0FBa0VScUIsYUFBUyxFQUFFLG1CQUFVeEIsQ0FBVixFQUFhRSxDQUFiLEVBQWdCO0FBQ3pCLGFBQU9GLENBQUMsR0FBRywwQkFBSixHQUFpQ0UsQ0FBQyxDQUFDQyxNQUFuQyxHQUE0QyxHQUFuRDtBQUNELEtBcEVPO0FBcUVSc0IsU0FBSyxFQUFFLGVBQVV6QixDQUFWLEVBQWE7QUFDbEIsYUFBT0EsQ0FBQyxHQUFHLDhCQUFYO0FBQ0QsS0F2RU87QUF3RVIwQixPQUFHLEVBQUUsYUFBVTFCLENBQVYsRUFBYUUsQ0FBYixFQUFnQjtBQUNuQixhQUFPRixDQUFDLEdBQUcscUJBQUosR0FBNEJFLENBQUMsQ0FBQ0MsTUFBOUIsR0FBdUMsU0FBOUM7QUFDRCxLQTFFTztBQTJFUndCLGFBQVMsRUFBRSxtQkFBVTNCLENBQVYsRUFBYUUsQ0FBYixFQUFnQjtBQUN6QixhQUFPRixDQUFDLEdBQUcsMEJBQUosR0FBaUNFLENBQUMsQ0FBQ0MsTUFBbkMsR0FBNEMsR0FBbkQ7QUFDRCxLQTdFTztBQThFUnlCLFlBQVEsRUFBRSxrQkFBVTVCLENBQVYsRUFBYTtBQUNyQixhQUFPQSxDQUFDLEdBQUcsZ0NBQVg7QUFDRCxLQWhGTztBQWlGUjZCLFdBQU8sRUFBRSxpQkFBVTdCLENBQVYsRUFBYTtBQUNwQixhQUFPQSxDQUFDLEdBQUcsOEJBQVg7QUFDRCxLQW5GTztBQW9GUjhCLFNBQUssRUFBRSxlQUFVOUIsQ0FBVixFQUFhO0FBQ2xCLGFBQU9BLENBQUMsR0FBRywyQkFBWDtBQUNELEtBdEZPO0FBdUZSK0IsWUFBUSxFQUFFLGtCQUFVL0IsQ0FBVixFQUFhO0FBQ3JCLGFBQU9BLENBQUMsR0FBRyx1QkFBWDtBQUNELEtBekZPO0FBMEZSZ0MsUUFBSSxFQUFFLGNBQVVoQyxDQUFWLEVBQWFFLENBQWIsRUFBZ0I7QUFDcEIsVUFBSWEsQ0FBSjtBQUFBLFVBQ0lrQixDQURKO0FBQUEsVUFFSUMsQ0FGSjtBQUFBLFVBR0lDLENBQUMsR0FBR2pDLENBQUMsQ0FBQ0MsTUFIVjtBQUlBLGFBQ0lILENBQUMsR0FDRCwrQkFEQSxJQUVFZSxDQUFDLEdBQUdvQixDQUFMLEVBQ0lGLENBQUMsR0FBRyxJQURSLEVBRUlDLENBQUMsR0FBRyxNQUFNbkIsQ0FBQyxHQUFHcUIsTUFBTSxDQUFDckIsQ0FBRCxDQUFOLEdBQVlrQixDQUF0QixJQUEyQixDQUEzQixHQUErQkksSUFBSSxDQUFDQyxLQUFMLENBQVdELElBQUksQ0FBQ0UsR0FBTCxDQUFTeEIsQ0FBVCxJQUFjc0IsSUFBSSxDQUFDRSxHQUFMLENBQVNOLENBQVQsQ0FBekIsQ0FGdkMsRUFHRCxJQUFJLENBQUNsQixDQUFDLEdBQUdzQixJQUFJLENBQUNHLEdBQUwsQ0FBU1AsQ0FBVCxFQUFZQyxDQUFaLENBQUwsRUFBcUJPLE9BQXJCLENBQTZCLENBQTdCLENBQUosR0FBc0MsR0FBdEMsR0FBNEMsQ0FBRSxNQUFGLEVBQVUsSUFBVixFQUFnQixJQUFoQixFQUFzQixJQUF0QixFQUE0QixJQUE1QixFQUFrQyxJQUFsQyxFQUF3QyxJQUF4QyxFQUE4QyxJQUE5QyxFQUFvRCxJQUFwRCxFQUE0RFAsQ0FBNUQsQ0FMNUMsSUFNQSxHQVBKO0FBU0QsS0F4R087QUF5R1JRLE9BQUcsRUFBRSxhQUFVMUMsQ0FBVixFQUFhO0FBQ2hCLGFBQU9BLENBQUMsR0FBRyx3Q0FBWDtBQUNEO0FBM0dPO0FBRkcsQ0FBZixFOzs7Ozs7Ozs7Ozs7QUNBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUVBMkMsMkNBQUcsQ0FBQ0MsR0FBSixDQUFRQyw0Q0FBUjtBQUNBRiwyQ0FBRyxDQUFDQyxHQUFKLENBQVFFLGtEQUFSO0FBRUEsSUFBTUMsS0FBSyxHQUFHLElBQUlGLDRDQUFJLENBQUNHLEtBQVQsQ0FBZUEseURBQWYsQ0FBZDtBQUNBLElBQU1DLE1BQU0sR0FBRyxJQUFJSCxrREFBSixDQUFjO0FBQzNCSSxNQUFJLEVBQUUsU0FEcUI7QUFFM0JDLFFBQU0sRUFBRSxDQUNOO0FBQUV0RCxRQUFJLEVBQUUsWUFBUjtBQUFzQnVELFFBQUksRUFBRSwyQkFBNUI7QUFBeURDLGFBQVMsRUFBRUMsNkVBQWdCQTtBQUFwRixHQURNLEVBRU47QUFBRXpELFFBQUksRUFBRSxVQUFSO0FBQW9CdUQsUUFBSSxFQUFFLHlCQUExQjtBQUFxREMsYUFBUyxFQUFFRSwyRUFBY0E7QUFBOUUsR0FGTSxFQUdOO0FBQUUxRCxRQUFJLEVBQUUsV0FBUjtBQUFxQnVELFFBQUksRUFBRSw4QkFBM0I7QUFBMkRDLGFBQVMsRUFBRUcsNEVBQWVBO0FBQXJGLEdBSE07QUFGbUIsQ0FBZCxDQUFmOztBQVNBLElBQU1DLE1BQU0sR0FBSSxZQUFNO0FBQ3BCLE1BQU1DLGdCQUFnQixHQUFHLFNBQW5CQSxnQkFBbUIsR0FBTTtBQUM3QixRQUFJQyxHQUFHLEdBQUdDLDZDQUFDLENBQUMsZUFBRCxDQUFYO0FBRUEsUUFBSWpCLDJDQUFKLENBQVE7QUFDTk0sWUFBTSxFQUFOQSxNQURNO0FBRU5GLFdBQUssRUFBTEEsS0FGTTtBQUdOYyxZQUFNLEVBQUUsZ0JBQUExQixDQUFDO0FBQUEsZUFBSUEsQ0FBQyxDQUFDMkIsZ0VBQUQsQ0FBTDtBQUFBO0FBSEgsS0FBUixFQUlHQyxNQUpILENBSVVKLEdBQUcsQ0FBRSxDQUFGLENBSmI7QUFLRCxHQVJEOztBQVVBLFNBQU87QUFDTEssUUFESyxrQkFDRTtBQUNMTixzQkFBZ0I7QUFDakI7QUFISSxHQUFQO0FBS0QsQ0FoQmMsRUFBZjs7QUFrQkFPLFFBQVEsQ0FBQ0MsZ0JBQVQsQ0FBMEIsa0JBQTFCLEVBQThDLFlBQVk7QUFDeERULFFBQU0sQ0FBQ08sSUFBUDtBQUNELENBRkQsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM3Q0E7QUFFQTtBQUNBO0FBQ0E7QUFFQSxJQUFJRyxVQUFVLEdBQUc7QUFDZkMsU0FBTyxFQUFFLElBRE07QUFFZkMsZUFBYSxFQUFFLElBRkE7QUFHZkMsVUFBUSxFQUFFLElBSEs7QUFJZnBELE9BQUssRUFBRSxJQUpRO0FBS2ZxRCxPQUFLLEVBQUUsSUFMUTtBQU1mQyxZQUFVLEVBQUU7QUFORyxDQUFqQjs7SUFTTXhCLEssR0FDSixpQkFBYztBQUFBOztBQUNaLFNBQU87QUFDTHlCLFNBQUssRUFBRTtBQUNMQyxVQUFJLEVBQUU7QUFDSkMsYUFBSyxFQUFFO0FBREgsT0FERDtBQUlMQyxtQkFBYSxFQUFFLEVBSlY7QUFLTEMscUJBQWUsb0JBQU9WLFVBQVA7QUFMVixLQURGO0FBUUxXLGFBQVMsRUFBRTtBQUNUQyxpQkFEUyx1QkFDR04sS0FESCxFQUNVO0FBQ2pCQSxhQUFLLENBQUNJLGVBQU4scUJBQTZCVixVQUE3QjtBQUNELE9BSFE7QUFJVGEsZ0JBSlMsc0JBSUVQLEtBSkYsRUFJU1EsT0FKVCxFQUlrQjtBQUN6QlIsYUFBSyxDQUFDQyxJQUFOLENBQVlPLE9BQU8sQ0FBQ1AsSUFBcEIsSUFBNkJPLE9BQU8sQ0FBQ0MsS0FBckM7QUFDRCxPQU5RO0FBT1RDLGlCQVBTLHVCQU9HVixLQVBILEVBT1VRLE9BUFYsRUFPbUI7QUFDMUJSLGFBQUssQ0FBQ0csYUFBTixHQUFzQkssT0FBTyxDQUFDTCxhQUE5QjtBQUNELE9BVFE7QUFVVFEsZUFWUyxxQkFVQ1gsS0FWRCxFQVVRUSxPQVZSLEVBVWlCO0FBQ3hCLFlBQUlJLHNEQUFRLENBQUNKLE9BQU8sQ0FBQ0osZUFBVCxDQUFaLEVBQ0U7O0FBRnNCLHlDQUlPSSxPQUpQO0FBQUEsWUFJbEJKLGVBSmtCLFlBSWxCQSxlQUprQjs7QUFNeEJBLHVCQUFlLENBQUNTLFFBQWhCLEdBQTJCLElBQTNCO0FBRUFiLGFBQUssQ0FBQ0ksZUFBTixHQUF3QkEsZUFBeEI7QUFDRCxPQW5CUTtBQW9CVFUsa0JBcEJTLHdCQW9CSWQsS0FwQkosRUFvQldRLE9BcEJYLEVBb0JvQjtBQUFBLDBDQUNXQSxPQURYO0FBQUEsWUFDckJPLElBRHFCLGFBQ3JCQSxJQURxQjtBQUFBLFlBQ2ZDLEtBRGUsYUFDZkEsS0FEZTtBQUFBLFlBQ1JDLFNBRFEsYUFDUkEsU0FEUTs7QUFHM0IsZ0JBQVFGLElBQVI7QUFDRSxlQUFLLFFBQUw7QUFDRWYsaUJBQUssQ0FBQ0csYUFBTixDQUFvQmUsSUFBcEIsQ0FBeUJELFNBQXpCO0FBQ0E7O0FBQ0YsZUFBSyxRQUFMO0FBQ0VqQixpQkFBSyxDQUFDRyxhQUFOLENBQXFCYSxLQUFyQixJQUErQkMsU0FBL0I7QUFDQWpCLGlCQUFLLENBQUNJLGVBQU4sR0FBd0JhLFNBQXhCO0FBRUE7O0FBQ0YsZUFBSyxRQUFMO0FBQ0VqQixpQkFBSyxDQUFDRyxhQUFOLENBQW9CZ0IsTUFBcEIsQ0FBMkJILEtBQTNCLEVBQWtDLENBQWxDO0FBQ0E7QUFYSjtBQWFEO0FBcENRLEtBUk47QUE4Q0xJLFdBQU8sRUFBRTtBQUNQQyxrQkFETyw4QkFDa0JiLE9BRGxCLEVBQzJCO0FBQUEsWUFBbkJjLE1BQW1CLFFBQW5CQSxNQUFtQjtBQUNoQyxlQUFPLElBQUlDLE9BQUosQ0FBWSxVQUFVQyxPQUFWLEVBQW1CQyxNQUFuQixFQUEyQjtBQUM1QyxjQUFJQyxTQUFTLHFCQUFRbEIsT0FBUixDQUFiOztBQUNBbUIsc0VBQUEsQ0FBZUQsU0FBZixFQUEwQkUsSUFBMUIsQ0FBK0IsVUFBQ0MsR0FBRCxFQUFTO0FBQ3RDLGdCQUFJQyxpRkFBVSxDQUFDRCxHQUFELENBQWQsRUFBcUI7QUFDbkJKLG9CQUFNLENBQUNJLEdBQUcsQ0FBQ3JCLE9BQUwsQ0FBTjtBQUNBO0FBQ0Q7O0FBSnFDLGlEQU1UcUIsR0FBRyxDQUFDckIsT0FOSztBQUFBLGdCQU1oQ3VCLGFBTmdDLGdCQU1oQ0EsYUFOZ0M7O0FBUXRDVCxrQkFBTSxDQUFDLFdBQUQsRUFBYztBQUFFbEIsNkJBQWUsRUFBRTJCO0FBQW5CLGFBQWQsQ0FBTjtBQUVBUCxtQkFBTyxDQUFDSyxHQUFHLENBQUNyQixPQUFMLENBQVA7QUFDRCxXQVhEO0FBWUQsU0FkTSxDQUFQO0FBZUQsT0FqQk07QUFrQlB3QixtQkFsQk8sZ0NBa0JtQjtBQUFBLFlBQVZWLE1BQVUsU0FBVkEsTUFBVTtBQUN4QixlQUFPLElBQUlDLE9BQUosQ0FBWSxVQUFVQyxPQUFWLEVBQW1CQyxNQUFuQixFQUEyQjtBQUM1QyxjQUFJQyxTQUFTLEdBQUcsRUFBaEI7QUFDQUMsdUVBQUEsQ0FBZ0JELFNBQWhCLEVBQTJCRSxJQUEzQixDQUFnQyxVQUFDQyxHQUFELEVBQVM7QUFDdkMsZ0JBQUlDLGlGQUFVLENBQUNELEdBQUQsQ0FBZCxFQUFxQjtBQUNuQkosb0JBQU0sQ0FBQ0ksR0FBRyxDQUFDckIsT0FBTCxDQUFOO0FBQ0E7QUFDRDs7QUFKc0Msa0RBTVpxQixHQUFHLENBQUNyQixPQU5RO0FBQUEsZ0JBTWpDeUIsV0FOaUMsaUJBTWpDQSxXQU5pQzs7QUFRdkNYLGtCQUFNLENBQUMsYUFBRCxFQUFnQjtBQUFFbkIsMkJBQWEsRUFBRThCO0FBQWpCLGFBQWhCLENBQU47QUFFQVQsbUJBQU8sQ0FBQ0ssR0FBRyxDQUFDckIsT0FBTCxDQUFQO0FBQ0QsV0FYRDtBQVlELFNBZE0sQ0FBUDtBQWVELE9BbENNO0FBbUNQMEIsbUJBbkNPLGdDQW1DbUIxQixPQW5DbkIsRUFtQzRCO0FBQUEsWUFBbkJjLE1BQW1CLFNBQW5CQSxNQUFtQjtBQUNqQyxlQUFPLElBQUlDLE9BQUosQ0FBWSxVQUFVQyxPQUFWLEVBQW1CQyxNQUFuQixFQUEyQjtBQUM1QyxjQUFJVSxRQUFRLEdBQUcsSUFBSUMsUUFBSixFQUFmO0FBRUFELGtCQUFRLENBQUNFLE1BQVQsQ0FBZ0IsWUFBaEIsRUFBOEI3QixPQUFPLENBQUM4QixVQUF0QztBQUNBSCxrQkFBUSxDQUFDRSxNQUFULENBQWdCLE1BQWhCLEVBQXdCN0IsT0FBTyxDQUFDK0IsSUFBaEM7QUFFQVoscUVBQUEsQ0FBY1EsUUFBZCxFQUF3QjtBQUN0QkssdUJBQVcsRUFBRSxLQURTO0FBRXRCQyx1QkFBVyxFQUFFO0FBRlMsV0FBeEIsRUFHR2IsSUFISCxDQUdRLFVBQUFDLEdBQUcsRUFBSTtBQUNiLGdCQUFJQyxpRkFBVSxDQUFDRCxHQUFELENBQWQsRUFBcUI7QUFDbkJhLGlHQUFZLENBQUNiLEdBQUcsQ0FBQ2MsT0FBTCxDQUFaO0FBRUFsQixvQkFBTSxDQUFDSSxHQUFHLENBQUNyQixPQUFMLENBQU47QUFDQTtBQUNEOztBQUVEZ0IsbUJBQU8sQ0FBQ0ssR0FBRyxDQUFDckIsT0FBTCxDQUFQO0FBQ0QsV0FaRDtBQWFELFNBbkJNLENBQVA7QUFvQkQsT0F4RE07QUF5RFBvQyxrQkF6RE8sK0JBeUQyQnBDLE9BekQzQixFQXlEb0M7QUFBQSxZQUE1QmMsTUFBNEIsU0FBNUJBLE1BQTRCO0FBQUEsWUFBcEJ1QixPQUFvQixTQUFwQkEsT0FBb0I7QUFDekMsWUFBSSxDQUFDckMsT0FBTyxDQUFDc0MsY0FBUixDQUF1QixPQUF2QixDQUFMLEVBQ0U7O0FBRnVDLDBDQUlwQnRDLE9BSm9CO0FBQUEsWUFJbkNRLEtBSm1DLGFBSW5DQSxLQUptQzs7QUFNekNNLGNBQU0sQ0FBQyxXQUFELEVBQWM7QUFBRWxCLHlCQUFlLEVBQUV5QyxPQUFPLENBQUNFLGdCQUFSLENBQXlCL0IsS0FBekIsRUFBaUMsQ0FBakM7QUFBbkIsU0FBZCxDQUFOO0FBQ0QsT0FoRU07QUFpRVBnQyxxQkFqRU8sa0NBaUU4QnhDLE9BakU5QixFQWlFdUM7QUFBQSxZQUE1QmMsTUFBNEIsU0FBNUJBLE1BQTRCO0FBQUEsWUFBcEJ1QixPQUFvQixTQUFwQkEsT0FBb0I7QUFDNUMsZUFBTyxJQUFJdEIsT0FBSixDQUFZLFVBQVVDLE9BQVYsRUFBbUJDLE1BQW5CLEVBQTJCO0FBQzVDLGNBQUlDLFNBQVMscUJBQVFsQixPQUFSLENBQWI7O0FBQ0FtQix5RUFBQSxDQUFrQkQsU0FBbEIsRUFBNkJFLElBQTdCLENBQWtDLFVBQUNDLEdBQUQsRUFBUztBQUN6Q2EsK0ZBQVksQ0FBQ2IsR0FBRyxDQUFDYyxPQUFMLENBQVo7O0FBQ0EsZ0JBQUliLGlGQUFVLENBQUNELEdBQUQsQ0FBZCxFQUFxQjtBQUNuQkosb0JBQU0sQ0FBQ0ksR0FBRyxDQUFDckIsT0FBTCxDQUFOO0FBQ0E7QUFDRDs7QUFMd0Msa0RBT1pxQixHQUFHLENBQUNyQixPQVBRO0FBQUEsZ0JBT25DdUIsYUFQbUMsaUJBT25DQSxhQVBtQzs7QUFTekNULGtCQUFNLENBQUMsY0FBRCxFQUFpQjtBQUNyQlAsa0JBQUksRUFBRSxRQURlO0FBRXJCRSx1QkFBUyxFQUFFYztBQUZVLGFBQWpCLENBQU47QUFLQVAsbUJBQU8sQ0FBQ0ssR0FBRyxDQUFDckIsT0FBTCxDQUFQO0FBQ0QsV0FmRDtBQWdCRCxTQWxCTSxDQUFQO0FBbUJELE9BckZNO0FBc0ZQeUMscUJBdEZPLGtDQXNGOEJ6QyxPQXRGOUIsRUFzRnVDO0FBQUEsWUFBNUJjLE1BQTRCLFNBQTVCQSxNQUE0QjtBQUFBLFlBQXBCdUIsT0FBb0IsU0FBcEJBLE9BQW9CO0FBQzVDLGVBQU8sSUFBSXRCLE9BQUosQ0FBWSxVQUFVQyxPQUFWLEVBQW1CQyxNQUFuQixFQUEyQjtBQUM1QyxjQUFJQyxTQUFTLHFCQUFRbEIsT0FBUixDQUFiOztBQUVBbUIseUVBQUEsQ0FBa0JELFNBQWxCLEVBQTZCRSxJQUE3QixDQUFrQyxVQUFDQyxHQUFELEVBQVM7QUFDekNhLCtGQUFZLENBQUNiLEdBQUcsQ0FBQ2MsT0FBTCxDQUFaOztBQUNBLGdCQUFJYixpRkFBVSxDQUFDRCxHQUFELENBQWQsRUFBcUI7QUFDbkJKLG9CQUFNLENBQUNJLEdBQUcsQ0FBQ3JCLE9BQUwsQ0FBTjtBQUNBO0FBQ0Q7O0FBTHdDLGtEQU9acUIsR0FBRyxDQUFDckIsT0FQUTtBQUFBLGdCQU9uQ3VCLGFBUG1DLGlCQU9uQ0EsYUFQbUM7O0FBU3pDVCxrQkFBTSxDQUFDLGNBQUQsRUFBaUI7QUFDckJQLGtCQUFJLEVBQUUsUUFEZTtBQUVyQkMsbUJBQUssRUFBRTZCLE9BQU8sQ0FBQ0ssWUFBUixDQUFxQm5CLGFBQWEsQ0FBQ3BDLE9BQW5DLENBRmM7QUFHckJzQix1QkFBUyxFQUFFYztBQUhVLGFBQWpCLENBQU47QUFNQVAsbUJBQU8sQ0FBQ0ssR0FBRyxDQUFDckIsT0FBTCxDQUFQO0FBQ0QsV0FoQkQ7QUFpQkQsU0FwQk0sQ0FBUDtBQXFCRCxPQTVHTTtBQTZHUDJDLHFCQTdHTyxrQ0E2RzhCM0MsT0E3RzlCLEVBNkd1QztBQUFBLFlBQTVCYyxNQUE0QixTQUE1QkEsTUFBNEI7QUFBQSxZQUFwQnVCLE9BQW9CLFNBQXBCQSxPQUFvQjtBQUM1QyxlQUFPLElBQUl0QixPQUFKLENBQVksVUFBVUMsT0FBVixFQUFtQkMsTUFBbkIsRUFBMkI7QUFDNUMsY0FBSUMsU0FBUyxxQkFBUWxCLE9BQVIsQ0FBYjs7QUFFQW1CLHlFQUFBLENBQWtCRCxTQUFsQixFQUE2QkUsSUFBN0IsQ0FBa0MsVUFBQUMsR0FBRyxFQUFJO0FBQ3ZDYSwrRkFBWSxDQUFDYixHQUFHLENBQUNjLE9BQUwsQ0FBWjs7QUFDQSxnQkFBSWIsaUZBQVUsQ0FBQ0QsR0FBRCxDQUFkLEVBQXFCO0FBQ25CSixvQkFBTSxDQUFDSSxHQUFHLENBQUNyQixPQUFMLENBQU47QUFFQTtBQUNEOztBQU5zQyxrREFRckJxQixHQUFHLENBQUNyQixPQVJpQjtBQUFBLGdCQVFqQzRDLEVBUmlDLGlCQVFqQ0EsRUFSaUM7O0FBVXZDQyxtQkFBTyxDQUFDdkYsR0FBUixDQUFZc0YsRUFBWjtBQUVBOUIsa0JBQU0sQ0FBQyxjQUFELEVBQWlCO0FBQ3JCUCxrQkFBSSxFQUFFLFFBRGU7QUFFckJDLG1CQUFLLEVBQUU2QixPQUFPLENBQUNLLFlBQVIsQ0FBcUJFLEVBQXJCO0FBRmMsYUFBakIsQ0FBTjtBQUtBNUIsbUJBQU8sQ0FBQ0ssR0FBRyxDQUFDckIsT0FBTCxDQUFQO0FBQ0QsV0FsQkQ7QUFtQkQsU0F0Qk0sQ0FBUDtBQXVCRDtBQXJJTSxLQTlDSjtBQXFMTHFDLFdBQU8sRUFBRTtBQUNQRSxzQkFBZ0IsRUFBRSwwQkFBQS9DLEtBQUs7QUFBQSxlQUFJLFVBQUFnQixLQUFLLEVBQUk7QUFDbEMsaUJBQU9oQixLQUFLLENBQUNHLGFBQU4sQ0FBb0JtRCxNQUFwQixDQUEyQixVQUFDQyxTQUFELEVBQVlDLFlBQVo7QUFBQSxtQkFBNkJBLFlBQVksSUFBSXhDLEtBQTdDO0FBQUEsV0FBM0IsQ0FBUDtBQUNELFNBRnNCO0FBQUEsT0FEaEI7QUFJUGtDLGtCQUFZLEVBQUUsc0JBQUFsRCxLQUFLO0FBQUEsZUFBSSxVQUFBb0QsRUFBRSxFQUFJO0FBQzNCLGNBQUlwQyxLQUFLLEdBQUcsQ0FBQyxDQUFiOztBQUNBeUMsOERBQU0sQ0FBQ3pELEtBQUssQ0FBQ0csYUFBUCxFQUFzQixVQUFDb0QsU0FBRCxFQUFZQyxZQUFaLEVBQTZCO0FBQ3ZELGdCQUFJRCxTQUFTLENBQUM1RCxPQUFWLElBQXFCeUQsRUFBekIsRUFDRTtBQUVGcEMsaUJBQUssR0FBR3dDLFlBQVI7QUFDQSxtQkFBTyxLQUFQO0FBQ0QsV0FOSyxDQUFOOztBQVFBLGlCQUFPeEMsS0FBUDtBQUNELFNBWGtCO0FBQUE7QUFKWjtBQXJMSixHQUFQO0FBdU1ELEM7O0FBR2EsbUVBQUl6QyxLQUFKLEVBQWhCLEU7Ozs7Ozs7Ozs7OztBQzNOQTtBQUFBO0FBQUE7QUFBQTtBQUFpRztBQUMzQjtBQUNMOzs7QUFHakU7QUFDZ0c7QUFDaEcsZ0JBQWdCLDJHQUFVO0FBQzFCLEVBQUUsd0ZBQU07QUFDUixFQUFFLDZGQUFNO0FBQ1IsRUFBRSxzR0FBZTtBQUNqQjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBLElBQUksS0FBVSxFQUFFLFlBaUJmO0FBQ0Q7QUFDZSxnRjs7Ozs7Ozs7Ozs7O0FDdENmO0FBQUE7QUFBQSx3Q0FBc00sQ0FBZ0IsNFBBQUcsRUFBQyxDOzs7Ozs7Ozs7Ozs7QUNBMU47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOzs7Ozs7Ozs7Ozs7O0FDQUE7QUFBQTtBQUFBO0FBQUE7QUFBa0Y7QUFDM0I7QUFDTDs7O0FBR2xEO0FBQ2dHO0FBQ2hHLGdCQUFnQiwyR0FBVTtBQUMxQixFQUFFLHlFQUFNO0FBQ1IsRUFBRSw4RUFBTTtBQUNSLEVBQUUsdUZBQWU7QUFDakI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQSxJQUFJLEtBQVUsRUFBRSxZQWlCZjtBQUNEO0FBQ2UsZ0Y7Ozs7Ozs7Ozs7OztBQ3RDZjtBQUFBO0FBQUEsd0NBQXVMLENBQWdCLDZPQUFHLEVBQUMsQzs7Ozs7Ozs7Ozs7O0FDQTNNO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7Ozs7Ozs7Ozs7OztBQ0FBO0FBQUE7QUFBQTtBQUFBO0FBQTZGO0FBQzNCO0FBQ0w7OztBQUc3RDtBQUNnRztBQUNoRyxnQkFBZ0IsMkdBQVU7QUFDMUIsRUFBRSxvRkFBTTtBQUNSLEVBQUUseUZBQU07QUFDUixFQUFFLGtHQUFlO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0EsSUFBSSxLQUFVLEVBQUUsWUFpQmY7QUFDRDtBQUNlLGdGOzs7Ozs7Ozs7Ozs7QUN0Q2Y7QUFBQTtBQUFBLHdDQUFrTSxDQUFnQix3UEFBRyxFQUFDLEM7Ozs7Ozs7Ozs7OztBQ0F0TjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7Ozs7Ozs7Ozs7QUNBQTtBQUFBO0FBQUE7QUFBQTtBQUE4RjtBQUMzQjtBQUNMOzs7QUFHOUQ7QUFDZ0c7QUFDaEcsZ0JBQWdCLDJHQUFVO0FBQzFCLEVBQUUscUZBQU07QUFDUixFQUFFLDBGQUFNO0FBQ1IsRUFBRSxtR0FBZTtBQUNqQjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBLElBQUksS0FBVSxFQUFFLFlBaUJmO0FBQ0Q7QUFDZSxnRjs7Ozs7Ozs7Ozs7O0FDdENmO0FBQUE7QUFBQSx3Q0FBbU0sQ0FBZ0IseVBBQUcsRUFBQyxDOzs7Ozs7Ozs7Ozs7QUNBdk47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOzs7Ozs7Ozs7Ozs7O0FDQUE7QUFBQTtBQUFBO0FBQUE7QUFBK0Y7QUFDM0I7QUFDTDs7O0FBRy9EO0FBQ2dHO0FBQ2hHLGdCQUFnQiwyR0FBVTtBQUMxQixFQUFFLHNGQUFNO0FBQ1IsRUFBRSwyRkFBTTtBQUNSLEVBQUUsb0dBQWU7QUFDakI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQSxJQUFJLEtBQVUsRUFBRSxZQWlCZjtBQUNEO0FBQ2UsZ0Y7Ozs7Ozs7Ozs7OztBQ3RDZjtBQUFBO0FBQUEsd0NBQW9NLENBQWdCLDBQQUFHLEVBQUMsQzs7Ozs7Ozs7Ozs7O0FDQXhOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7Ozs7Ozs7Ozs7OztBQ0FBO0FBQUE7QUFBQTtBQUFBO0FBQXdGO0FBQzNCO0FBQ0w7OztBQUd4RDtBQUNnRztBQUNoRyxnQkFBZ0IsMkdBQVU7QUFDMUIsRUFBRSwrRUFBTTtBQUNSLEVBQUUsb0ZBQU07QUFDUixFQUFFLDZGQUFlO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0EsSUFBSSxLQUFVLEVBQUUsWUFpQmY7QUFDRDtBQUNlLGdGOzs7Ozs7Ozs7Ozs7QUN0Q2Y7QUFBQTtBQUFBLHdDQUE2TCxDQUFnQixtUEFBRyxFQUFDLEM7Ozs7Ozs7Ozs7OztBQ0FqTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7Ozs7Ozs7Ozs7QUNBQTtBQUFBO0FBQUE7QUFBQTtBQUEyRjtBQUMzQjtBQUNMOzs7QUFHM0Q7QUFDZ0c7QUFDaEcsZ0JBQWdCLDJHQUFVO0FBQzFCLEVBQUUsa0ZBQU07QUFDUixFQUFFLHVGQUFNO0FBQ1IsRUFBRSxnR0FBZTtBQUNqQjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBLElBQUksS0FBVSxFQUFFLFlBaUJmO0FBQ0Q7QUFDZSxnRjs7Ozs7Ozs7Ozs7O0FDdENmO0FBQUE7QUFBQSx3Q0FBZ00sQ0FBZ0Isc1BBQUcsRUFBQyxDOzs7Ozs7Ozs7Ozs7QUNBcE47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBIiwiZmlsZSI6ImpzL3VzZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBpbnN0YWxsIGEgSlNPTlAgY2FsbGJhY2sgZm9yIGNodW5rIGxvYWRpbmdcbiBcdGZ1bmN0aW9uIHdlYnBhY2tKc29ucENhbGxiYWNrKGRhdGEpIHtcbiBcdFx0dmFyIGNodW5rSWRzID0gZGF0YVswXTtcbiBcdFx0dmFyIG1vcmVNb2R1bGVzID0gZGF0YVsxXTtcbiBcdFx0dmFyIGV4ZWN1dGVNb2R1bGVzID0gZGF0YVsyXTtcblxuIFx0XHQvLyBhZGQgXCJtb3JlTW9kdWxlc1wiIHRvIHRoZSBtb2R1bGVzIG9iamVjdCxcbiBcdFx0Ly8gdGhlbiBmbGFnIGFsbCBcImNodW5rSWRzXCIgYXMgbG9hZGVkIGFuZCBmaXJlIGNhbGxiYWNrXG4gXHRcdHZhciBtb2R1bGVJZCwgY2h1bmtJZCwgaSA9IDAsIHJlc29sdmVzID0gW107XG4gXHRcdGZvcig7aSA8IGNodW5rSWRzLmxlbmd0aDsgaSsrKSB7XG4gXHRcdFx0Y2h1bmtJZCA9IGNodW5rSWRzW2ldO1xuIFx0XHRcdGlmKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChpbnN0YWxsZWRDaHVua3MsIGNodW5rSWQpICYmIGluc3RhbGxlZENodW5rc1tjaHVua0lkXSkge1xuIFx0XHRcdFx0cmVzb2x2ZXMucHVzaChpbnN0YWxsZWRDaHVua3NbY2h1bmtJZF1bMF0pO1xuIFx0XHRcdH1cbiBcdFx0XHRpbnN0YWxsZWRDaHVua3NbY2h1bmtJZF0gPSAwO1xuIFx0XHR9XG4gXHRcdGZvcihtb2R1bGVJZCBpbiBtb3JlTW9kdWxlcykge1xuIFx0XHRcdGlmKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChtb3JlTW9kdWxlcywgbW9kdWxlSWQpKSB7XG4gXHRcdFx0XHRtb2R1bGVzW21vZHVsZUlkXSA9IG1vcmVNb2R1bGVzW21vZHVsZUlkXTtcbiBcdFx0XHR9XG4gXHRcdH1cbiBcdFx0aWYocGFyZW50SnNvbnBGdW5jdGlvbikgcGFyZW50SnNvbnBGdW5jdGlvbihkYXRhKTtcblxuIFx0XHR3aGlsZShyZXNvbHZlcy5sZW5ndGgpIHtcbiBcdFx0XHRyZXNvbHZlcy5zaGlmdCgpKCk7XG4gXHRcdH1cblxuIFx0XHQvLyBhZGQgZW50cnkgbW9kdWxlcyBmcm9tIGxvYWRlZCBjaHVuayB0byBkZWZlcnJlZCBsaXN0XG4gXHRcdGRlZmVycmVkTW9kdWxlcy5wdXNoLmFwcGx5KGRlZmVycmVkTW9kdWxlcywgZXhlY3V0ZU1vZHVsZXMgfHwgW10pO1xuXG4gXHRcdC8vIHJ1biBkZWZlcnJlZCBtb2R1bGVzIHdoZW4gYWxsIGNodW5rcyByZWFkeVxuIFx0XHRyZXR1cm4gY2hlY2tEZWZlcnJlZE1vZHVsZXMoKTtcbiBcdH07XG4gXHRmdW5jdGlvbiBjaGVja0RlZmVycmVkTW9kdWxlcygpIHtcbiBcdFx0dmFyIHJlc3VsdDtcbiBcdFx0Zm9yKHZhciBpID0gMDsgaSA8IGRlZmVycmVkTW9kdWxlcy5sZW5ndGg7IGkrKykge1xuIFx0XHRcdHZhciBkZWZlcnJlZE1vZHVsZSA9IGRlZmVycmVkTW9kdWxlc1tpXTtcbiBcdFx0XHR2YXIgZnVsZmlsbGVkID0gdHJ1ZTtcbiBcdFx0XHRmb3IodmFyIGogPSAxOyBqIDwgZGVmZXJyZWRNb2R1bGUubGVuZ3RoOyBqKyspIHtcbiBcdFx0XHRcdHZhciBkZXBJZCA9IGRlZmVycmVkTW9kdWxlW2pdO1xuIFx0XHRcdFx0aWYoaW5zdGFsbGVkQ2h1bmtzW2RlcElkXSAhPT0gMCkgZnVsZmlsbGVkID0gZmFsc2U7XG4gXHRcdFx0fVxuIFx0XHRcdGlmKGZ1bGZpbGxlZCkge1xuIFx0XHRcdFx0ZGVmZXJyZWRNb2R1bGVzLnNwbGljZShpLS0sIDEpO1xuIFx0XHRcdFx0cmVzdWx0ID0gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBkZWZlcnJlZE1vZHVsZVswXSk7XG4gXHRcdFx0fVxuIFx0XHR9XG5cbiBcdFx0cmV0dXJuIHJlc3VsdDtcbiBcdH1cblxuIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gb2JqZWN0IHRvIHN0b3JlIGxvYWRlZCBhbmQgbG9hZGluZyBjaHVua3NcbiBcdC8vIHVuZGVmaW5lZCA9IGNodW5rIG5vdCBsb2FkZWQsIG51bGwgPSBjaHVuayBwcmVsb2FkZWQvcHJlZmV0Y2hlZFxuIFx0Ly8gUHJvbWlzZSA9IGNodW5rIGxvYWRpbmcsIDAgPSBjaHVuayBsb2FkZWRcbiBcdHZhciBpbnN0YWxsZWRDaHVua3MgPSB7XG4gXHRcdFwidXNlclwiOiAwXG4gXHR9O1xuXG4gXHR2YXIgZGVmZXJyZWRNb2R1bGVzID0gW107XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cbiBcdHZhciBqc29ucEFycmF5ID0gd2luZG93W1wid2VicGFja0pzb25wXCJdID0gd2luZG93W1wid2VicGFja0pzb25wXCJdIHx8IFtdO1xuIFx0dmFyIG9sZEpzb25wRnVuY3Rpb24gPSBqc29ucEFycmF5LnB1c2guYmluZChqc29ucEFycmF5KTtcbiBcdGpzb25wQXJyYXkucHVzaCA9IHdlYnBhY2tKc29ucENhbGxiYWNrO1xuIFx0anNvbnBBcnJheSA9IGpzb25wQXJyYXkuc2xpY2UoKTtcbiBcdGZvcih2YXIgaSA9IDA7IGkgPCBqc29ucEFycmF5Lmxlbmd0aDsgaSsrKSB3ZWJwYWNrSnNvbnBDYWxsYmFjayhqc29ucEFycmF5W2ldKTtcbiBcdHZhciBwYXJlbnRKc29ucEZ1bmN0aW9uID0gb2xkSnNvbnBGdW5jdGlvbjtcblxuXG4gXHQvLyBhZGQgZW50cnkgbW9kdWxlIHRvIGRlZmVycmVkIGxpc3RcbiBcdGRlZmVycmVkTW9kdWxlcy5wdXNoKFs1LFwidmVuZG9yXCJdKTtcbiBcdC8vIHJ1biBkZWZlcnJlZCBtb2R1bGVzIHdoZW4gcmVhZHlcbiBcdHJldHVybiBjaGVja0RlZmVycmVkTW9kdWxlcygpO1xuIiwiPHRlbXBsYXRlPlxyXG4gIDxkaXYgY2xhc3M9XCJtb2RhbCBmYWRlIG1vZGFsLWZzXCIgaWQ9XCJtb2RhbC1mc1wiPlxyXG4gICAgPGRpdiBjbGFzcz1cIm1vZGFsLWRpYWxvZ1wiIHJvbGU9XCJkb2N1bWVudFwiPlxyXG4gICAgICA8ZGl2IGNsYXNzPVwibW9kYWwtY29udGVudFwiPlxyXG4gICAgICAgIDxkaXYgY2xhc3M9XCJtb2RhbC1ib2R5XCI+XHJcbiAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29udGFpbmVyIHRleHQtY2VudGVyXCI+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJyb3cgZnVsbC1oZWlnaHQgYWxpZ24taXRlbXMtY2VudGVyXCI+XHJcbiAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1tZC01IG0taC1hdXRvXCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwicC1oLTMwIHAtYi01MFwiPlxyXG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwidGV4dC1jZW50ZXIgZm9udC1zaXplLTcwXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3M9XCJtZGkgbWRpLWNoZWNrYm94LW1hcmtlZC1jaXJjbGUtb3V0bGluZSBpY29uLWdyYWRpZW50LXN1Y2Nlc3NcIj48L2k+XHJcbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICA8aDEgY2xhc3M9XCJ0ZXh0LXRoaW4gbS12LTE1XCI+e3t0aXRsZX19PC9oMT5cclxuICAgICAgICAgICAgICAgICAgPHAgY2xhc3M9XCJtLWItMjUgbGVhZFwiPnt7Y29udGVudH19PC9wPlxyXG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwidGV4dC1jZW50ZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICA8YnV0dG9uXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzPVwiYnRuIGJ0bi1kZWZhdWx0IGJ0bi1sZ1wiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIEBjbGljay5wcmV2ZW50PVwiY29uZmlybVwiXHJcbiAgICAgICAgICAgICAgICAgICAgPkNvbmZpcm1cclxuICAgICAgICAgICAgICAgICAgICA8L2J1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgIDxhXHJcbiAgICAgICAgICAgICAgY2xhc3M9XCJtb2RhbC1jbG9zZVwiXHJcbiAgICAgICAgICAgICAgaHJlZj1cIiNcIlxyXG4gICAgICAgICAgICAgIGRhdGEtZGlzbWlzcz1cIm1vZGFsXCJcclxuICAgICAgICAgICAgICBAY2xpY2sucHJldmVudD1cImNvbmZpcm1cIlxyXG4gICAgICAgICAgPlxyXG4gICAgICAgICAgICA8aSBjbGFzcz1cInRpLWNsb3NlXCI+PC9pPlxyXG4gICAgICAgICAgPC9hPlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICA8L2Rpdj5cclxuICAgIDwvZGl2PlxyXG4gIDwvZGl2PlxyXG48L3RlbXBsYXRlPlxyXG5cclxuPHNjcmlwdD5cclxuICBpbXBvcnQgeyBFVkVOVF9CVVNfS0VZIH0gZnJvbSAnJGpzQmFzZVBhdGgvZGVmaW5lJztcclxuICBpbXBvcnQgRXZlbnRCdXMgZnJvbSAnJGpzQmFzZVBhdGgvRXZlbnRCdXMnO1xyXG5cclxuICBleHBvcnQgZGVmYXVsdCB7XHJcbiAgICBuYW1lOiAnbW9kYWwnLFxyXG4gICAgY29tcG9uZW50czoge30sXHJcbiAgICBkYXRhKCkge1xyXG4gICAgICByZXR1cm4ge1xyXG4gICAgICAgIHRpdGxlOiAnVEjDlE5HIELDgU8gIScsXHJcbiAgICAgICAgY29udGVudDogJ1dlIGFyZSBsdWNreSB0byBsaXZlIGluIGEgZ2xvcmlvdXMgYWdlIHRoYXQgZ2l2ZXMgdXMgZXZlcnl0aGluZyBhcyBhIGh1bWFuIHJhY2UuJ1xyXG4gICAgICB9XHJcbiAgICB9LFxyXG4gICAgY29tcHV0ZWQ6IHt9LFxyXG4gICAgY3JlYXRlZCgpIHtcclxuICAgICAgbGV0IHNlbGYgPSB0aGlzXHJcbiAgICAgIEV2ZW50QnVzLm9uKEVWRU5UX0JVU19LRVkuVVBEQVRFX0NPTlRFTlRfTU9EQUwsIChjb250ZW50KSA9PiB7XHJcbiAgICAgICAgc2VsZi5jb250ZW50ID0gY29udGVudFxyXG4gICAgICB9KVxyXG5cclxuICAgICAgJChkb2N1bWVudCkub24oJ2tleXVwJywgKGUpID0+IHtcclxuICAgICAgICBsZXQga2V5Q29kZSA9IGUud2hpY2hcclxuXHJcbiAgICAgICAgaWYgKGtleUNvZGUgIT0gMTMgJiYga2V5Q29kZSAhPSAyNylcclxuICAgICAgICAgIHJldHVyblxyXG5cclxuICAgICAgICBsZXQgZmxhZyA9ICQoJyNtb2RhbC1mcycpLmhhc0NsYXNzKCdzaG93JylcclxuICAgICAgICBpZiAoIWZsYWcpXHJcbiAgICAgICAgICByZXR1cm5cclxuXHJcbiAgICAgICAgJCgnI21vZGFsLWZzLWJ0bicpLnRyaWdnZXIoJ2NsaWNrJylcclxuICAgICAgICBFdmVudEJ1cy5lbWl0KEVWRU5UX0JVU19LRVkuQ09ORklSTV9NT0RBTCwgdHJ1ZSlcclxuICAgICAgfSlcclxuICAgIH0sXHJcbiAgICBtZXRob2RzOiB7XHJcbiAgICAgIGNvbmZpcm0oKSB7XHJcbiAgICAgICAgJCgnI21vZGFsLWZzLWJ0bicpLnRyaWdnZXIoJ2NsaWNrJylcclxuICAgICAgICBFdmVudEJ1cy5lbWl0KEVWRU5UX0JVU19LRVkuQ09ORklSTV9NT0RBTCwgdHJ1ZSlcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuPC9zY3JpcHQ+XHJcblxyXG4iLCI8dGVtcGxhdGU+XHJcbiAgPHJvdXRlci12aWV3Pjwvcm91dGVyLXZpZXc+XHJcbjwvdGVtcGxhdGU+XHJcblxyXG48c2NyaXB0PlxyXG4gIGV4cG9ydCBkZWZhdWx0IHtcclxuICAgIG5hbWU6ICd1c2VyLWFwcCcsXHJcbiAgICBjb21wb25lbnRzOiB7fSxcclxuICAgIGRhdGEoKSB7XHJcbiAgICAgIHJldHVybiB7fVxyXG4gICAgfSxcclxuICAgIGNvbXB1dGVkOiB7fSxcclxuICAgIGNyZWF0ZWQoKSB7XHJcblxyXG4gICAgfSxcclxuICAgIG1ldGhvZHM6IHt9XHJcbiAgfVxyXG48L3NjcmlwdD5cclxuXHJcbiIsIjx0ZW1wbGF0ZT5cclxuICA8ZGl2IGNsYXNzPVwiY29udGFpbmVyLWZsdWlkXCI+XHJcbiAgICA8ZGl2IGNsYXNzPVwicGFnZS1oZWFkZXJcIj5cclxuICAgICAgPGgyIGNsYXNzPVwiaGVhZGVyLXRpdGxlXCI+TmfGsOG7nWkgZMO5bmc8L2gyPlxyXG4gICAgICA8ZGl2IGNsYXNzPVwiaGVhZGVyLXN1Yi10aXRsZVwiPlxyXG4gICAgICAgIDxuYXYgY2xhc3M9XCJicmVhZGNydW1iIGJyZWFkY3J1bWItZGFzaFwiPlxyXG4gICAgICAgICAgPGEgaHJlZj1cIi9iYWNrZW5kXCIgY2xhc3M9XCJicmVhZGNydW1iLWl0ZW1cIj48aSBjbGFzcz1cInRpLWhvbWUgcC1yLTVcIj48L2k+SOG7hyB0aOG7kW5nPC9hPlxyXG4gICAgICAgICAgPGFcclxuICAgICAgICAgICAgICBjbGFzcz1cImJyZWFkY3J1bWItaXRlbVwiXHJcbiAgICAgICAgICAgICAgaHJlZj1cIiNcIlxyXG4gICAgICAgICAgICAgIEBjbGljay5wcmV2ZW50PVwiZ29SZWFkKClcIlxyXG4gICAgICAgICAgPjxpIGNsYXNzPVwidGktdXNlciBwLXItNVwiPjwvaT5OZ8aw4budaSBkw7luZzwvYT5cclxuICAgICAgICAgIDxzcGFuIGNsYXNzPVwiYnJlYWRjcnVtYi1pdGVtIGFjdGl2ZVwiPjxpIGNsYXNzPVwidGktcGVuY2lsLWFsdCBwLXItNVwiPjwvaT5U4bqhbyBuZ8aw4budaSBkw7luZzwvc3Bhbj5cclxuICAgICAgICA8L25hdj5cclxuICAgICAgPC9kaXY+XHJcbiAgICAgIDxkaXYgY2xhc3M9XCJfaGVhZGVyLWJ1dHRvblwiPlxyXG4gICAgICAgIDxidXR0b25cclxuICAgICAgICAgICAgY2xhc3M9XCJidG4gYnRuLWdyYWRpZW50LXN1Y2Nlc3NcIlxyXG4gICAgICAgICAgICBAY2xpY2sucHJldmVudD1cImdvQ3JlYXRlKClcIlxyXG4gICAgICAgID5U4bqhbyBuZ8aw4budaSBkw7luZ1xyXG4gICAgICAgIDwvYnV0dG9uPlxyXG4gICAgICA8L2Rpdj5cclxuICAgIDwvZGl2PlxyXG4gICAgPGRpdiBjbGFzcz1cImNhcmRcIj5cclxuICAgICAgPHZhbGlkYXRpb24tb2JzZXJ2ZXJcclxuICAgICAgICAgIHJlZj1cIm9ic2VydmVyXCJcclxuICAgICAgICAgIHYtc2xvdD1cInsgaW52YWxpZCB9XCJcclxuICAgICAgICAgIHRhZz1cImRpdlwiXHJcbiAgICAgICAgICBjbGFzcz1cImNhcmQtYm9keVwiXHJcbiAgICAgID5cclxuICAgICAgICA8ZGl2XHJcbiAgICAgICAgICAgIGNsYXNzPVwicm93IG0tdi0zMFwiXHJcbiAgICAgICAgICAgIEBrZXl1cC5lbnRlcj1cInVwZGF0ZShpbnZhbGlkKVwiXHJcbiAgICAgICAgPlxyXG4gICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1zbS0zXCI+XHJcbiAgICAgICAgICAgIDxpbWdcclxuICAgICAgICAgICAgICAgIGNsYXNzPVwiaW1nLWZsdWlkIGQtYmxvY2sgbXgtYXV0byBtLWItMzBcIlxyXG4gICAgICAgICAgICAgICAgOnNyYz1cInByb2ZpbGVJbWFnZVwiXHJcbiAgICAgICAgICAgICAgICBhbHQ9XCJcIlxyXG4gICAgICAgICAgICAgICAgc3R5bGU9XCJ3aWR0aDogMjAwcHg7IGhlaWdodDogMjAwcHg7XCJcclxuICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICA8aW5wdXRcclxuICAgICAgICAgICAgICAgIHR5cGU9XCJmaWxlXCJcclxuICAgICAgICAgICAgICAgIGNsYXNzPVwiZm9ybS1jb250cm9sXCJcclxuICAgICAgICAgICAgICAgIEBjaGFuZ2U9XCJ1cGxvYWRcIlxyXG4gICAgICAgICAgICA+XHJcbiAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtc20tNCB0ZXh0LWNlbnRlciB0ZXh0LXNtLWxlZnRcIj5cclxuICAgICAgICAgICAgPCEtLSBmdWxsbmFtZSAtLT5cclxuICAgICAgICAgICAgPHZhbGlkYXRpb24tcHJvdmlkZXJcclxuICAgICAgICAgICAgICAgIG5hbWU9XCJUw6puIG5nxrDhu51pIGTDuW5nXCJcclxuICAgICAgICAgICAgICAgIHJ1bGVzPVwicmVxdWlyZWRcIlxyXG4gICAgICAgICAgICAgICAgdi1zbG90PVwieyBlcnJvcnMsIHZhbGlkIH1cIlxyXG4gICAgICAgICAgICAgICAgdGFnPVwiZGl2XCJcclxuICAgICAgICAgICAgICAgIGNsYXNzPVwiZm9ybS1ncm91cFwiXHJcbiAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICA8bGFiZWwgY2xhc3M9XCJjb250cm9sLWxhYmVsXCIgZm9yPVwiaWNvbi1pbnB1dFwiPk5hbWU6PC9sYWJlbD5cclxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiaWNvbi1pbnB1dFwiPlxyXG4gICAgICAgICAgICAgICAgPGkgY2xhc3M9XCJtZGkgbWRpLWFjY291bnRcIj48L2k+XHJcbiAgICAgICAgICAgICAgICA8aW5wdXRcclxuICAgICAgICAgICAgICAgICAgICB2LW1vZGVsPVwiZnJtRGF0YS5mdWxsbmFtZVwiXHJcbiAgICAgICAgICAgICAgICAgICAgdHlwZT1cInRleHRcIlxyXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzPVwiZm9ybS1jb250cm9sXCJcclxuICAgICAgICAgICAgICAgICAgICA6Y2xhc3M9XCJ7J2Vycm9yJzogZmxhZy5pc1ZhbGlkYXRpbmcgJiYgIXZhbGlkfVwiXHJcbiAgICAgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI9XCJGdWxsIG5hbWUuLi5cIlxyXG4gICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgIDxsYWJlbFxyXG4gICAgICAgICAgICAgICAgICB2LXNob3c9XCJmbGFnLmlzVmFsaWRhdGluZyAmJiAhdmFsaWRcIlxyXG4gICAgICAgICAgICAgICAgICBjbGFzcz1cImVycm9yXCJcclxuICAgICAgICAgICAgICA+e3tlcnJvcnNbMF19fTwvbGFiZWw+XHJcbiAgICAgICAgICAgIDwvdmFsaWRhdGlvbi1wcm92aWRlcj5cclxuICAgICAgICAgICAgPCEtLSBmdWxsbmFtZSBlbmQgLS0+XHJcblxyXG4gICAgICAgICAgICA8IS0tIGVtYWlsIC0tPlxyXG4gICAgICAgICAgICA8dmFsaWRhdGlvbi1wcm92aWRlclxyXG4gICAgICAgICAgICAgICAgbmFtZT1cIkVtYWlsXCJcclxuICAgICAgICAgICAgICAgIHJ1bGVzPVwicmVxdWlyZWR8ZW1haWxcIlxyXG4gICAgICAgICAgICAgICAgdi1zbG90PVwieyBlcnJvcnMsIHZhbGlkIH1cIlxyXG4gICAgICAgICAgICAgICAgdGFnPVwiZGl2XCJcclxuICAgICAgICAgICAgICAgIGNsYXNzPVwiZm9ybS1ncm91cFwiXHJcbiAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICA8bGFiZWwgY2xhc3M9XCJjb250cm9sLWxhYmVsXCIgZm9yPVwiaWNvbi1pbnB1dFwiPkVtYWlsOjwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImljb24taW5wdXRcIj5cclxuICAgICAgICAgICAgICAgIDxpIGNsYXNzPVwibWRpIG1kaS1lbWFpbC1vdXRsaW5lXCI+PC9pPlxyXG4gICAgICAgICAgICAgICAgPGlucHV0XHJcbiAgICAgICAgICAgICAgICAgICAgdi1tb2RlbD1cImZybURhdGEuZW1haWxcIlxyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU9XCJ0ZXh0XCJcclxuICAgICAgICAgICAgICAgICAgICBjbGFzcz1cImZvcm0tY29udHJvbFwiXHJcbiAgICAgICAgICAgICAgICAgICAgOmNsYXNzPVwieydlcnJvcic6IGZsYWcuaXNWYWxpZGF0aW5nICYmICF2YWxpZH1cIlxyXG4gICAgICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyPVwiRW1haWwuLi5cIlxyXG4gICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgIDxsYWJlbFxyXG4gICAgICAgICAgICAgICAgICB2LXNob3c9XCJmbGFnLmlzVmFsaWRhdGluZyAmJiAhdmFsaWRcIlxyXG4gICAgICAgICAgICAgICAgICBjbGFzcz1cImVycm9yXCJcclxuICAgICAgICAgICAgICA+e3tlcnJvcnNbMF19fTwvbGFiZWw+XHJcbiAgICAgICAgICAgIDwvdmFsaWRhdGlvbi1wcm92aWRlcj5cclxuICAgICAgICAgICAgPCEtLSBlbWFpbCBlbmQgLS0+XHJcblxyXG4gICAgICAgICAgICA8IS0tIGlzX2FjdGl2ZWQgLS0+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjaGVja2JveFwiPlxyXG4gICAgICAgICAgICAgIDxpbnB1dFxyXG4gICAgICAgICAgICAgICAgICB2LW1vZGVsPVwiZnJtRGF0YS5pc19hY3RpdmVkXCJcclxuICAgICAgICAgICAgICAgICAgaWQ9XCJjaGVja2JveDFcIlxyXG4gICAgICAgICAgICAgICAgICB0eXBlPVwiY2hlY2tib3hcIlxyXG4gICAgICAgICAgICAgICAgICB0cnVlLXZhbHVlPVwiMVwiXHJcbiAgICAgICAgICAgICAgICAgIGZhbHNlLXZhbHVlPVwiMFwiXHJcbiAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgIDxsYWJlbCBmb3I9XCJjaGVja2JveDFcIj5Lw61jaCBob+G6oXQ8L2xhYmVsPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgPGRpdiBjbGFzcz1cImNvbFwiPlxyXG4gICAgICAgICAgICA8IS0tIHBob25lIC0tPlxyXG4gICAgICAgICAgICA8dmFsaWRhdGlvbi1wcm92aWRlclxyXG4gICAgICAgICAgICAgICAgdi1tb2RlbD1cImZybURhdGEucGhvbmVcIlxyXG4gICAgICAgICAgICAgICAgbmFtZT1cIlPEkFRcIlxyXG4gICAgICAgICAgICAgICAgcnVsZXM9XCJyZXF1aXJlZFwiXHJcbiAgICAgICAgICAgICAgICB2LXNsb3Q9XCJ7IGVycm9ycywgdmFsaWQgfVwiXHJcbiAgICAgICAgICAgICAgICB0YWc9XCJkaXZcIlxyXG4gICAgICAgICAgICAgICAgY2xhc3M9XCJmb3JtLWdyb3VwXCJcclxuICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgIDxsYWJlbCBjbGFzcz1cImNvbnRyb2wtbGFiZWxcIiBmb3I9XCJpY29uLWlucHV0XCI+UGhvbmU6PC9sYWJlbD5cclxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiaWNvbi1pbnB1dFwiPlxyXG4gICAgICAgICAgICAgICAgPGkgY2xhc3M9XCJtZGkgbWRpLWNlbGxwaG9uZVwiPjwvaT5cclxuICAgICAgICAgICAgICAgIDxpbnB1dFxyXG4gICAgICAgICAgICAgICAgICAgIHYtbW9kZWw9XCJmcm1EYXRhLnBob25lXCJcclxuICAgICAgICAgICAgICAgICAgICB0eXBlPVwidGV4dFwiXHJcbiAgICAgICAgICAgICAgICAgICAgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIlxyXG4gICAgICAgICAgICAgICAgICAgIDpjbGFzcz1cInsnZXJyb3InOiBmbGFnLmlzVmFsaWRhdGluZyAmJiAhdmFsaWR9XCJcclxuICAgICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcj1cIlBob25lLi4uXCJcclxuICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICA8bGFiZWxcclxuICAgICAgICAgICAgICAgICAgdi1zaG93PVwiZmxhZy5pc1ZhbGlkYXRpbmcgJiYgIXZhbGlkXCJcclxuICAgICAgICAgICAgICAgICAgY2xhc3M9XCJlcnJvclwiXHJcbiAgICAgICAgICAgICAgPnt7ZXJyb3JzWzBdfX08L2xhYmVsPlxyXG4gICAgICAgICAgICA8L3ZhbGlkYXRpb24tcHJvdmlkZXI+XHJcbiAgICAgICAgICAgIDwhLS0gcGhvbmUgZW5kIC0tPlxyXG5cclxuICAgICAgICAgICAgPCEtLSBhZGRyZXNzIC0tPlxyXG4gICAgICAgICAgICA8dmFsaWRhdGlvbi1wcm92aWRlclxyXG4gICAgICAgICAgICAgICAgbmFtZT1cIsSQ4buLYSBjaOG7iVwiXHJcbiAgICAgICAgICAgICAgICBydWxlcz1cInJlcXVpcmVkXCJcclxuICAgICAgICAgICAgICAgIHYtc2xvdD1cInsgZXJyb3JzLCB2YWxpZCB9XCJcclxuICAgICAgICAgICAgICAgIHRhZz1cImRpdlwiXHJcbiAgICAgICAgICAgICAgICBjbGFzcz1cImZvcm0tZ3JvdXBcIlxyXG4gICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgPGxhYmVsIGNsYXNzPVwiY29udHJvbC1sYWJlbFwiIGZvcj1cImljb24taW5wdXRcIj7EkOG7i2EgY2jhu4k6PC9sYWJlbD5cclxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiaWNvbi1pbnB1dFwiPlxyXG4gICAgICAgICAgICAgICAgPGkgY2xhc3M9XCJtZGkgbWRpLWFjY291bnQtYm94XCI+PC9pPlxyXG4gICAgICAgICAgICAgICAgPGlucHV0XHJcbiAgICAgICAgICAgICAgICAgICAgdi1tb2RlbD1cImZybURhdGEuYWRkcmVzc1wiXHJcbiAgICAgICAgICAgICAgICAgICAgdHlwZT1cInRleHRcIlxyXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzPVwiZm9ybS1jb250cm9sXCJcclxuICAgICAgICAgICAgICAgICAgICA6Y2xhc3M9XCJ7J2Vycm9yJzogZmxhZy5pc1ZhbGlkYXRpbmcgJiYgIXZhbGlkfVwiXHJcbiAgICAgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI9XCJBZGRyZXNzLi4uXCJcclxuICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICA8bGFiZWxcclxuICAgICAgICAgICAgICAgICAgdi1zaG93PVwiZmxhZy5pc1ZhbGlkYXRpbmcgJiYgIXZhbGlkXCJcclxuICAgICAgICAgICAgICAgICAgY2xhc3M9XCJlcnJvclwiXHJcbiAgICAgICAgICAgICAgPnt7ZXJyb3JzWzBdfX08L2xhYmVsPlxyXG4gICAgICAgICAgICA8L3ZhbGlkYXRpb24tcHJvdmlkZXI+XHJcbiAgICAgICAgICAgIDwhLS0gYWRkcmVzcyBlbmQgLS0+XHJcblxyXG4gICAgICAgICAgICA8IS0tIHBhc3N3b3JkIC0tPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxyXG4gICAgICAgICAgICAgIDxsYWJlbCBjbGFzcz1cImNvbnRyb2wtbGFiZWxcIiBmb3I9XCJpY29uLWlucHV0XCI+UGFzc3dvcmQ6PC9sYWJlbD5cclxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiaWNvbi1pbnB1dFwiPlxyXG4gICAgICAgICAgICAgICAgPGkgY2xhc3M9XCJtZGkgbWRpLWFjY291bnQta2V5XCI+PC9pPlxyXG4gICAgICAgICAgICAgICAgPGlucHV0XHJcbiAgICAgICAgICAgICAgICAgICAgdi1tb2RlbD1cImZybURhdGEucGFzc3dvcmRcIlxyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU9XCJwYXNzd29yZFwiXHJcbiAgICAgICAgICAgICAgICAgICAgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIlxyXG4gICAgICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyPVwiUGFzc3dvcmQuLi5cIlxyXG4gICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPCEtLSBwYXNzd29yZCBlbmQgLS0+XHJcblxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwibS10LTI1XCI+XHJcbiAgICAgICAgICAgICAgPGJ1dHRvblxyXG4gICAgICAgICAgICAgICAgICBpZD1cInNhdmVcIlxyXG4gICAgICAgICAgICAgICAgICBjbGFzcz1cImJ0biBidG4tc3VjY2Vzc1wiXHJcbiAgICAgICAgICAgICAgICAgIHR5cGU9XCJidXR0b25cIlxyXG4gICAgICAgICAgICAgICAgICA6ZGlzYWJsZWQ9XCJmbGFnLmlzVXBkYXRpbmdcIlxyXG4gICAgICAgICAgICAgICAgICBAY2xpY2sucHJldmVudD1cInVwZGF0ZShpbnZhbGlkKVwiXHJcbiAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgPHRlbXBsYXRlIHYtaWY9XCJmbGFnLmlzVXBkYXRpbmdcIj5cclxuICAgICAgICAgICAgICAgICAgPGkgY2xhc3M9XCJmYSBmYS1zcGlubmVyIGZhLXB1bHNlIGZhLWZ3IG0tci01XCI+PC9pPlVwZGF0aW5nXHJcbiAgICAgICAgICAgICAgICA8L3RlbXBsYXRlPlxyXG4gICAgICAgICAgICAgICAgPHRlbXBsYXRlIHYtZWxzZT5VcGRhdGU8L3RlbXBsYXRlPlxyXG4gICAgICAgICAgICAgIDwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgIDxidXR0b25cclxuICAgICAgICAgICAgICAgICAgaWQ9XCJlZGl0XCIgY2xhc3M9XCJidG4gYnRuLWRlZmF1bHRcIiB0eXBlPVwiYnV0dG9uXCJcclxuICAgICAgICAgICAgICAgICAgQGNsaWNrLnByZXZlbnQ9XCJiYWNrXCJcclxuICAgICAgICAgICAgICA+PGkgY2xhc3M9XCJ0aS1iYWNrLWxlZnQgcC1yLTEwXCI+PC9pPkJhY2tcclxuICAgICAgICAgICAgICA8L2J1dHRvbj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgPC92YWxpZGF0aW9uLW9ic2VydmVyPlxyXG4gICAgPC9kaXY+XHJcbiAgPC9kaXY+XHJcbjwvdGVtcGxhdGU+XHJcblxyXG48c2NyaXB0PlxyXG4gIGltcG9ydCB7IGlzRW1wdHkgYXMgX2lzRW1wdHkgfSBmcm9tICdsb2Rhc2gnXHJcbiAgaW1wb3J0IHsgVmFsaWRhdGlvbk9ic2VydmVyLCBWYWxpZGF0aW9uUHJvdmlkZXIsIGV4dGVuZCB9IGZyb20gXCJ2ZWUtdmFsaWRhdGVcIlxyXG4gIGltcG9ydCB7IHJlcXVpcmVkLCBlbWFpbCwgbWluLCBtYXgsIGludGVnZXIgfSBmcm9tICd2ZWUtdmFsaWRhdGUvZGlzdC9ydWxlcydcclxuXHJcbiAgaW1wb3J0IHsgRVZFTlRfQlVTX0tFWSB9IGZyb20gJyRqc0Jhc2VQYXRoL2RlZmluZSc7XHJcbiAgaW1wb3J0IEV2ZW50QnVzIGZyb20gJyRqc0Jhc2VQYXRoL0V2ZW50QnVzJztcclxuXHJcbiAgaW1wb3J0IFZlZVZhbGlkYXRlTWVzc2FnZSBmcm9tICckanNDb21tb25QYXRoL3V0aWwvVmVlVmFsaWRhdGVNZXNzYWdlJ1xyXG4gIGltcG9ydCB7IF9pc0VtcHR5U3RyaW5nIH0gZnJvbSBcIiRqc0NvbW1vblBhdGgvdXRpbC9TYWZlVXRpbFwiXHJcblxyXG5cclxuICBsZXQgb2JqTWVzc2FnZSA9IFZlZVZhbGlkYXRlTWVzc2FnZS5tZXNzYWdlc1xyXG5cclxuICBleHRlbmQoJ3JlcXVpcmVkJywgeyAuLi5yZXF1aXJlZCwgbWVzc2FnZTogb2JqTWVzc2FnZS5yZXF1aXJlZCB9KVxyXG4gIGV4dGVuZCgnZW1haWwnLCB7IC4uLmVtYWlsLCBtZXNzYWdlOiBvYmpNZXNzYWdlLmVtYWlsIH0pXHJcbiAgZXh0ZW5kKCdtaW4nLCB7IC4uLm1pbiwgbWVzc2FnZTogb2JqTWVzc2FnZS5taW4gfSlcclxuICBleHRlbmQoJ21heCcsIHsgLi4ubWF4LCBtZXNzYWdlOiBvYmpNZXNzYWdlLm1heCB9KVxyXG4gIGV4dGVuZCgnaW50ZWdlcicsIHsgLi4uaW50ZWdlciwgbWVzc2FnZTogb2JqTWVzc2FnZS5pbnRlZ2VyIH0pXHJcblxyXG4gIGV4cG9ydCBkZWZhdWx0IHtcclxuICAgIG5hbWU6ICd1c2VyLWFkZC1zZWN0aW9uJyxcclxuICAgIGNvbXBvbmVudHM6IHtcclxuICAgICAgJ3ZhbGlkYXRpb24tb2JzZXJ2ZXInOiBWYWxpZGF0aW9uT2JzZXJ2ZXIsXHJcbiAgICAgICd2YWxpZGF0aW9uLXByb3ZpZGVyJzogVmFsaWRhdGlvblByb3ZpZGVyXHJcbiAgICB9LFxyXG4gICAgZGF0YSgpIHtcclxuICAgICAgcmV0dXJuIHtcclxuICAgICAgICB1cmw6IHtcclxuICAgICAgICAgIHJlc291cmNlVVJMOiBDREFUQS5yZXNvdXJjZVVSTFxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgZmxhZzoge1xyXG4gICAgICAgICAgaXNWYWxpZGF0aW5nOiBmYWxzZSxcclxuICAgICAgICAgIGlzVXBkYXRpbmc6IGZhbHNlXHJcbiAgICAgICAgfSxcclxuICAgICAgICBmcm1EYXRhOiB7fVxyXG4gICAgICB9XHJcbiAgICB9LFxyXG4gICAgY29tcHV0ZWQ6IHtcclxuICAgICAgcHJvZmlsZUltYWdlKCkge1xyXG4gICAgICAgIGlmIChfaXNFbXB0eSh0aGlzLmZybURhdGEucHJvZmlsZV9pbWFnZSkpXHJcbiAgICAgICAgICByZXR1cm4gdGhpcy51cmwucmVzb3VyY2VVUkwgKyAnL2dsb2JhbC9pbWFnZS9ub3RGb3VuZC5wbmcnXHJcblxyXG4gICAgICAgIHJldHVybiB0aGlzLmZybURhdGEucHJvZmlsZV9pbWFnZVxyXG4gICAgICB9LFxyXG4gICAgICBvYmpFbnRpdHlTZWxlY3QoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuJHN0b3JlLnN0YXRlLm9iakVudGl0eVNlbGVjdFxyXG4gICAgICB9XHJcbiAgICB9LFxyXG4gICAgY3JlYXRlZCgpIHtcclxuICAgICAgbGV0IHNlbGYgPSB0aGlzXHJcblxyXG4gICAgICBFdmVudEJ1cy5vZmYoRVZFTlRfQlVTX0tFWS5DT05GSVJNX01PREFMKVxyXG4gICAgICBFdmVudEJ1cy5vbihFVkVOVF9CVVNfS0VZLkNPTkZJUk1fTU9EQUwsIChwYXlsb2FkKSA9PiB7XHJcbiAgICAgICAgdGhpcy4kc3RvcmUuY29tbWl0KCdyZXNldEVudGl0eScpXHJcblxyXG4gICAgICAgIHNlbGYuZmxhZy5pc1ZhbGlkYXRpbmcgPSBmYWxzZVxyXG4gICAgICAgIHNlbGYuZmxhZy5pc1VwZGF0aW5nID0gZmFsc2VcclxuXHJcbiAgICAgICAgdGhpcy5mcm1EYXRhID0gdGhpcy5vYmpFbnRpdHlTZWxlY3RcclxuICAgICAgfSlcclxuXHJcbiAgICAgIHRoaXMuJHN0b3JlLmNvbW1pdCgncmVzZXRFbnRpdHknKVxyXG4gICAgICB0aGlzLmZybURhdGEgPSB0aGlzLm9iakVudGl0eVNlbGVjdFxyXG4gICAgfSxcclxuICAgIG1ldGhvZHM6IHtcclxuICAgICAgZ29SZWFkKCkge1xyXG4gICAgICAgIHRoaXMuJHJvdXRlci5wdXNoKHsgbmFtZTogJ3VzZXI6aW5kZXgnIH0pXHJcbiAgICAgIH0sXHJcbiAgICAgIGdvQ3JlYXRlKCkge1xyXG4gICAgICAgIHRoaXMuJHJvdXRlci5wdXNoKHsgbmFtZTogJ3VzZXI6YWRkJywgcGFyYW1zOiB7IHBhZ2U6IDEgfSB9KVxyXG4gICAgICB9LFxyXG4gICAgICBiYWNrKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLiRyb3V0ZXIuYmFjaygpXHJcbiAgICAgIH0sXHJcbiAgICAgIHVwZGF0ZShpbnZhbGlkKSB7XHJcbiAgICAgICAgdGhpcy5mbGFnLmlzVmFsaWRhdGluZyA9IHRydWVcclxuXHJcbiAgICAgICAgaWYgKGludmFsaWQpIHtcclxuICAgICAgICAgIHRoaXMuJHJlZnMub2JzZXJ2ZXIudmFsaWRhdGUoKVxyXG4gICAgICAgICAgcmV0dXJuXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLmZsYWcuaXNVcGRhdGluZyA9IHRydWVcclxuICAgICAgICB0aGlzLiRzdG9yZS5kaXNwYXRjaCgnY3JlYXRlRW50aXR5QWN0JywgdGhpcy5mcm1EYXRhKVxyXG4gICAgICB9LFxyXG4gICAgICB1cGxvYWQoZSkge1xyXG4gICAgICAgIGNvbnN0IGZpbGUgPSBlLnRhcmdldC5maWxlc1sgMCBdXHJcblxyXG4gICAgICAgIHRoaXMuJHN0b3JlLmRpc3BhdGNoKCd1cGxvYWRGaWxlQWN0Jywge1xyXG4gICAgICAgICAgY29udHJvbGxlcjogQ0RBVEEuY29udHJvbGxlcixcclxuICAgICAgICAgIGZpbGU6IGZpbGVcclxuICAgICAgICB9KS50aGVuKHBheWxvYWQgPT4ge1xyXG4gICAgICAgICAgbGV0IG9iakltYWdlID0gcGF5bG9hZC5hcnJJbWFnZVsgMCBdXHJcbiAgICAgICAgICBpZiAoX2lzRW1wdHkob2JqSW1hZ2UpKSB7XHJcbiAgICAgICAgICAgIHJldHVyblxyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIHRoaXMuZnJtRGF0YS5wcm9maWxlX2ltYWdlID0gb2JqSW1hZ2UuaW1hZ2VcclxuICAgICAgICB9KVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG48L3NjcmlwdD5cclxuXHJcbiIsIjx0ZW1wbGF0ZT5cclxuICA8ZGl2IGNsYXNzPVwiY29udGFpbmVyLWZsdWlkXCI+XHJcbiAgICA8ZGl2IGNsYXNzPVwicGFnZS1oZWFkZXJcIj5cclxuICAgICAgPGgyIGNsYXNzPVwiaGVhZGVyLXRpdGxlXCI+TmfGsOG7nWkgZMO5bmc8L2gyPlxyXG4gICAgICA8ZGl2IGNsYXNzPVwiaGVhZGVyLXN1Yi10aXRsZVwiPlxyXG4gICAgICAgIDxuYXYgY2xhc3M9XCJicmVhZGNydW1iIGJyZWFkY3J1bWItZGFzaFwiPlxyXG4gICAgICAgICAgPGEgaHJlZj1cIi9iYWNrZW5kXCIgY2xhc3M9XCJicmVhZGNydW1iLWl0ZW1cIj48aSBjbGFzcz1cInRpLWhvbWUgcC1yLTVcIj48L2k+SOG7hyB0aOG7kW5nPC9hPlxyXG4gICAgICAgICAgPGFcclxuICAgICAgICAgICAgICBjbGFzcz1cImJyZWFkY3J1bWItaXRlbVwiXHJcbiAgICAgICAgICAgICAgaHJlZj1cIiNcIlxyXG4gICAgICAgICAgICAgIEBjbGljay5wcmV2ZW50PVwiZ29SZWFkKClcIlxyXG4gICAgICAgICAgPjxpIGNsYXNzPVwidGktdXNlciBwLXItNVwiPjwvaT5OZ8aw4budaSBkw7luZzwvYT5cclxuICAgICAgICAgIDxzcGFuIGNsYXNzPVwiYnJlYWRjcnVtYi1pdGVtIGFjdGl2ZVwiPjxpIGNsYXNzPVwidGktcGVuY2lsLWFsdCBwLXItNVwiPjwvaT5D4bqtcCBuaOG6rXQgbmfGsOG7nWkgZMO5bmc8L3NwYW4+XHJcbiAgICAgICAgPC9uYXY+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgICA8ZGl2IGNsYXNzPVwiX2hlYWRlci1idXR0b25cIj5cclxuICAgICAgICA8YnV0dG9uXHJcbiAgICAgICAgICAgIGNsYXNzPVwiYnRuIGJ0bi1ncmFkaWVudC1zdWNjZXNzXCJcclxuICAgICAgICAgICAgQGNsaWNrLnByZXZlbnQ9XCJnb0NyZWF0ZSgpXCJcclxuICAgICAgICA+VOG6oW8gbmfGsOG7nWkgZMO5bmdcclxuICAgICAgICA8L2J1dHRvbj5cclxuICAgICAgPC9kaXY+XHJcbiAgICA8L2Rpdj5cclxuICAgIDxkaXYgY2xhc3M9XCJjYXJkXCI+XHJcbiAgICAgIDx2YWxpZGF0aW9uLW9ic2VydmVyXHJcbiAgICAgICAgICByZWY9XCJvYnNlcnZlclwiXHJcbiAgICAgICAgICB2LXNsb3Q9XCJ7IGludmFsaWQgfVwiXHJcbiAgICAgICAgICB0YWc9XCJkaXZcIlxyXG4gICAgICAgICAgY2xhc3M9XCJjYXJkLWJvZHlcIlxyXG4gICAgICA+XHJcbiAgICAgICAgPGRpdlxyXG4gICAgICAgICAgICBjbGFzcz1cInJvdyBtLXYtMzBcIlxyXG4gICAgICAgICAgICBAa2V5dXAuZW50ZXI9XCJ1cGRhdGUoaW52YWxpZClcIlxyXG4gICAgICAgID5cclxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtc20tM1wiPlxyXG4gICAgICAgICAgICA8aW1nXHJcbiAgICAgICAgICAgICAgICBjbGFzcz1cImltZy1mbHVpZCBkLWJsb2NrIG14LWF1dG8gbS1iLTMwXCJcclxuICAgICAgICAgICAgICAgIDpzcmM9XCJwcm9maWxlSW1hZ2VcIlxyXG4gICAgICAgICAgICAgICAgYWx0PVwiXCJcclxuICAgICAgICAgICAgICAgIHN0eWxlPVwid2lkdGg6IDIwMHB4OyBoZWlnaHQ6IDIwMHB4O1wiXHJcbiAgICAgICAgICAgID5cclxuICAgICAgICAgICAgPGlucHV0XHJcbiAgICAgICAgICAgICAgICB0eXBlPVwiZmlsZVwiXHJcbiAgICAgICAgICAgICAgICBjbGFzcz1cImZvcm0tY29udHJvbFwiXHJcbiAgICAgICAgICAgICAgICBAY2hhbmdlPVwidXBsb2FkXCJcclxuICAgICAgICAgICAgPlxyXG4gICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXNtLTQgdGV4dC1jZW50ZXIgdGV4dC1zbS1sZWZ0XCI+XHJcbiAgICAgICAgICAgIDwhLS0gZnVsbG5hbWUgLS0+XHJcbiAgICAgICAgICAgIDx2YWxpZGF0aW9uLXByb3ZpZGVyXHJcbiAgICAgICAgICAgICAgICBuYW1lPVwiVMOqbiBuZ8aw4budaSBkw7luZ1wiXHJcbiAgICAgICAgICAgICAgICBydWxlcz1cInJlcXVpcmVkXCJcclxuICAgICAgICAgICAgICAgIHYtc2xvdD1cInsgZXJyb3JzLCB2YWxpZCB9XCJcclxuICAgICAgICAgICAgICAgIHRhZz1cImRpdlwiXHJcbiAgICAgICAgICAgICAgICBjbGFzcz1cImZvcm0tZ3JvdXBcIlxyXG4gICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgPGxhYmVsIGNsYXNzPVwiY29udHJvbC1sYWJlbFwiIGZvcj1cImljb24taW5wdXRcIj5OYW1lOjwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImljb24taW5wdXRcIj5cclxuICAgICAgICAgICAgICAgIDxpIGNsYXNzPVwibWRpIG1kaS1hY2NvdW50XCI+PC9pPlxyXG4gICAgICAgICAgICAgICAgPGlucHV0XHJcbiAgICAgICAgICAgICAgICAgICAgdi1tb2RlbD1cImZybURhdGEuZnVsbG5hbWVcIlxyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU9XCJ0ZXh0XCJcclxuICAgICAgICAgICAgICAgICAgICBjbGFzcz1cImZvcm0tY29udHJvbFwiXHJcbiAgICAgICAgICAgICAgICAgICAgOmNsYXNzPVwieydlcnJvcic6IGZsYWcuaXNWYWxpZGF0aW5nICYmICF2YWxpZH1cIlxyXG4gICAgICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyPVwiRnVsbCBuYW1lLi4uXCJcclxuICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICA8bGFiZWxcclxuICAgICAgICAgICAgICAgICAgdi1zaG93PVwiZmxhZy5pc1ZhbGlkYXRpbmcgJiYgIXZhbGlkXCJcclxuICAgICAgICAgICAgICAgICAgY2xhc3M9XCJlcnJvclwiXHJcbiAgICAgICAgICAgICAgPnt7ZXJyb3JzWzBdfX08L2xhYmVsPlxyXG4gICAgICAgICAgICA8L3ZhbGlkYXRpb24tcHJvdmlkZXI+XHJcbiAgICAgICAgICAgIDwhLS0gZnVsbG5hbWUgZW5kIC0tPlxyXG5cclxuICAgICAgICAgICAgPCEtLSBlbWFpbCAtLT5cclxuICAgICAgICAgICAgPHZhbGlkYXRpb24tcHJvdmlkZXJcclxuICAgICAgICAgICAgICAgIG5hbWU9XCJFbWFpbFwiXHJcbiAgICAgICAgICAgICAgICBydWxlcz1cInJlcXVpcmVkfGVtYWlsXCJcclxuICAgICAgICAgICAgICAgIHYtc2xvdD1cInsgZXJyb3JzLCB2YWxpZCB9XCJcclxuICAgICAgICAgICAgICAgIHRhZz1cImRpdlwiXHJcbiAgICAgICAgICAgICAgICBjbGFzcz1cImZvcm0tZ3JvdXBcIlxyXG4gICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgPGxhYmVsIGNsYXNzPVwiY29udHJvbC1sYWJlbFwiIGZvcj1cImljb24taW5wdXRcIj5FbWFpbDo8L2xhYmVsPlxyXG4gICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpY29uLWlucHV0XCI+XHJcbiAgICAgICAgICAgICAgICA8aSBjbGFzcz1cIm1kaSBtZGktZW1haWwtb3V0bGluZVwiPjwvaT5cclxuICAgICAgICAgICAgICAgIDxpbnB1dFxyXG4gICAgICAgICAgICAgICAgICAgIHYtbW9kZWw9XCJmcm1EYXRhLmVtYWlsXCJcclxuICAgICAgICAgICAgICAgICAgICB0eXBlPVwidGV4dFwiXHJcbiAgICAgICAgICAgICAgICAgICAgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIlxyXG4gICAgICAgICAgICAgICAgICAgIDpjbGFzcz1cInsnZXJyb3InOiBmbGFnLmlzVmFsaWRhdGluZyAmJiAhdmFsaWR9XCJcclxuICAgICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcj1cIkVtYWlsLi4uXCJcclxuICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICA8bGFiZWxcclxuICAgICAgICAgICAgICAgICAgdi1zaG93PVwiZmxhZy5pc1ZhbGlkYXRpbmcgJiYgIXZhbGlkXCJcclxuICAgICAgICAgICAgICAgICAgY2xhc3M9XCJlcnJvclwiXHJcbiAgICAgICAgICAgICAgPnt7ZXJyb3JzWzBdfX08L2xhYmVsPlxyXG4gICAgICAgICAgICA8L3ZhbGlkYXRpb24tcHJvdmlkZXI+XHJcbiAgICAgICAgICAgIDwhLS0gZW1haWwgZW5kIC0tPlxyXG5cclxuICAgICAgICAgICAgPCEtLSBpc19hY3RpdmVkIC0tPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY2hlY2tib3hcIj5cclxuICAgICAgICAgICAgICA8aW5wdXRcclxuICAgICAgICAgICAgICAgICAgdi1tb2RlbD1cImZybURhdGEuaXNfYWN0aXZlZFwiXHJcbiAgICAgICAgICAgICAgICAgIGlkPVwiY2hlY2tib3gxXCJcclxuICAgICAgICAgICAgICAgICAgdHlwZT1cImNoZWNrYm94XCJcclxuICAgICAgICAgICAgICAgICAgdHJ1ZS12YWx1ZT1cIjFcIlxyXG4gICAgICAgICAgICAgICAgICBmYWxzZS12YWx1ZT1cIjBcIlxyXG4gICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICA8bGFiZWwgZm9yPVwiY2hlY2tib3gxXCI+S8OtY2ggaG/huqF0PC9sYWJlbD5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2xcIj5cclxuICAgICAgICAgICAgPCEtLSBwaG9uZSAtLT5cclxuICAgICAgICAgICAgPHZhbGlkYXRpb24tcHJvdmlkZXJcclxuICAgICAgICAgICAgICAgIHYtbW9kZWw9XCJmcm1EYXRhLnBob25lXCJcclxuICAgICAgICAgICAgICAgIG5hbWU9XCJTxJBUXCJcclxuICAgICAgICAgICAgICAgIHJ1bGVzPVwicmVxdWlyZWRcIlxyXG4gICAgICAgICAgICAgICAgdi1zbG90PVwieyBlcnJvcnMsIHZhbGlkIH1cIlxyXG4gICAgICAgICAgICAgICAgdGFnPVwiZGl2XCJcclxuICAgICAgICAgICAgICAgIGNsYXNzPVwiZm9ybS1ncm91cFwiXHJcbiAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICA8bGFiZWwgY2xhc3M9XCJjb250cm9sLWxhYmVsXCIgZm9yPVwiaWNvbi1pbnB1dFwiPlBob25lOjwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImljb24taW5wdXRcIj5cclxuICAgICAgICAgICAgICAgIDxpIGNsYXNzPVwibWRpIG1kaS1jZWxscGhvbmVcIj48L2k+XHJcbiAgICAgICAgICAgICAgICA8aW5wdXRcclxuICAgICAgICAgICAgICAgICAgICB2LW1vZGVsPVwiZnJtRGF0YS5waG9uZVwiXHJcbiAgICAgICAgICAgICAgICAgICAgdHlwZT1cInRleHRcIlxyXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzPVwiZm9ybS1jb250cm9sXCJcclxuICAgICAgICAgICAgICAgICAgICA6Y2xhc3M9XCJ7J2Vycm9yJzogZmxhZy5pc1ZhbGlkYXRpbmcgJiYgIXZhbGlkfVwiXHJcbiAgICAgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI9XCJQaG9uZS4uLlwiXHJcbiAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgPGxhYmVsXHJcbiAgICAgICAgICAgICAgICAgIHYtc2hvdz1cImZsYWcuaXNWYWxpZGF0aW5nICYmICF2YWxpZFwiXHJcbiAgICAgICAgICAgICAgICAgIGNsYXNzPVwiZXJyb3JcIlxyXG4gICAgICAgICAgICAgID57e2Vycm9yc1swXX19PC9sYWJlbD5cclxuICAgICAgICAgICAgPC92YWxpZGF0aW9uLXByb3ZpZGVyPlxyXG4gICAgICAgICAgICA8IS0tIHBob25lIGVuZCAtLT5cclxuXHJcbiAgICAgICAgICAgIDwhLS0gYWRkcmVzcyAtLT5cclxuICAgICAgICAgICAgPHZhbGlkYXRpb24tcHJvdmlkZXJcclxuICAgICAgICAgICAgICAgIG5hbWU9XCLEkOG7i2EgY2jhu4lcIlxyXG4gICAgICAgICAgICAgICAgcnVsZXM9XCJyZXF1aXJlZFwiXHJcbiAgICAgICAgICAgICAgICB2LXNsb3Q9XCJ7IGVycm9ycywgdmFsaWQgfVwiXHJcbiAgICAgICAgICAgICAgICB0YWc9XCJkaXZcIlxyXG4gICAgICAgICAgICAgICAgY2xhc3M9XCJmb3JtLWdyb3VwXCJcclxuICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgIDxsYWJlbCBjbGFzcz1cImNvbnRyb2wtbGFiZWxcIiBmb3I9XCJpY29uLWlucHV0XCI+xJDhu4thIGNo4buJOjwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImljb24taW5wdXRcIj5cclxuICAgICAgICAgICAgICAgIDxpIGNsYXNzPVwibWRpIG1kaS1hY2NvdW50LWJveFwiPjwvaT5cclxuICAgICAgICAgICAgICAgIDxpbnB1dFxyXG4gICAgICAgICAgICAgICAgICAgIHYtbW9kZWw9XCJmcm1EYXRhLmFkZHJlc3NcIlxyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU9XCJ0ZXh0XCJcclxuICAgICAgICAgICAgICAgICAgICBjbGFzcz1cImZvcm0tY29udHJvbFwiXHJcbiAgICAgICAgICAgICAgICAgICAgOmNsYXNzPVwieydlcnJvcic6IGZsYWcuaXNWYWxpZGF0aW5nICYmICF2YWxpZH1cIlxyXG4gICAgICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyPVwiQWRkcmVzcy4uLlwiXHJcbiAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgPGxhYmVsXHJcbiAgICAgICAgICAgICAgICAgIHYtc2hvdz1cImZsYWcuaXNWYWxpZGF0aW5nICYmICF2YWxpZFwiXHJcbiAgICAgICAgICAgICAgICAgIGNsYXNzPVwiZXJyb3JcIlxyXG4gICAgICAgICAgICAgID57e2Vycm9yc1swXX19PC9sYWJlbD5cclxuICAgICAgICAgICAgPC92YWxpZGF0aW9uLXByb3ZpZGVyPlxyXG4gICAgICAgICAgICA8IS0tIGFkZHJlc3MgZW5kIC0tPlxyXG5cclxuICAgICAgICAgICAgPCEtLSBwYXNzd29yZCAtLT5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cclxuICAgICAgICAgICAgICA8bGFiZWwgY2xhc3M9XCJjb250cm9sLWxhYmVsXCIgZm9yPVwiaWNvbi1pbnB1dFwiPlBhc3N3b3JkOjwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImljb24taW5wdXRcIj5cclxuICAgICAgICAgICAgICAgIDxpIGNsYXNzPVwibWRpIG1kaS1hY2NvdW50LWtleVwiPjwvaT5cclxuICAgICAgICAgICAgICAgIDxpbnB1dFxyXG4gICAgICAgICAgICAgICAgICAgIHYtbW9kZWw9XCJmcm1EYXRhLnBhc3N3b3JkXCJcclxuICAgICAgICAgICAgICAgICAgICB0eXBlPVwicGFzc3dvcmRcIlxyXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzPVwiZm9ybS1jb250cm9sXCJcclxuICAgICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcj1cIlBhc3N3b3JkLi4uXCJcclxuICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwhLS0gcGFzc3dvcmQgZW5kIC0tPlxyXG5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm0tdC0yNVwiPlxyXG4gICAgICAgICAgICAgIDxidXR0b25cclxuICAgICAgICAgICAgICAgICAgaWQ9XCJlZGl0XCIgY2xhc3M9XCJidG4gYnRuLWRlZmF1bHRcIiB0eXBlPVwiYnV0dG9uXCJcclxuICAgICAgICAgICAgICAgICAgQGNsaWNrLnByZXZlbnQ9XCJiYWNrXCJcclxuICAgICAgICAgICAgICA+QmFja1xyXG4gICAgICAgICAgICAgIDwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgIDxidXR0b25cclxuICAgICAgICAgICAgICAgICAgaWQ9XCJzYXZlXCJcclxuICAgICAgICAgICAgICAgICAgY2xhc3M9XCJidG4gYnRuLXN1Y2Nlc3NcIlxyXG4gICAgICAgICAgICAgICAgICB0eXBlPVwiYnV0dG9uXCJcclxuICAgICAgICAgICAgICAgICAgOmRpc2FibGVkPVwiZmxhZy5pc1VwZGF0aW5nXCJcclxuICAgICAgICAgICAgICAgICAgQGNsaWNrLnByZXZlbnQ9XCJ1cGRhdGUoaW52YWxpZClcIlxyXG4gICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgIDx0ZW1wbGF0ZSB2LWlmPVwiZmxhZy5pc1VwZGF0aW5nXCI+XHJcbiAgICAgICAgICAgICAgICAgIDxpIGNsYXNzPVwiZmEgZmEtc3Bpbm5lciBmYS1wdWxzZSBmYS1mdyBtLXItNVwiPjwvaT5VcGRhdGluZ1xyXG4gICAgICAgICAgICAgICAgPC90ZW1wbGF0ZT5cclxuICAgICAgICAgICAgICAgIDx0ZW1wbGF0ZSB2LWVsc2U+VXBkYXRlPC90ZW1wbGF0ZT5cclxuICAgICAgICAgICAgICA8L2J1dHRvbj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgPC92YWxpZGF0aW9uLW9ic2VydmVyPlxyXG4gICAgPC9kaXY+XHJcbiAgPC9kaXY+XHJcbjwvdGVtcGxhdGU+XHJcblxyXG48c2NyaXB0PlxyXG4gIGltcG9ydCB7IGlzRW1wdHkgYXMgX2lzRW1wdHkgfSBmcm9tICdsb2Rhc2gnXHJcbiAgaW1wb3J0IHsgVmFsaWRhdGlvbk9ic2VydmVyLCBWYWxpZGF0aW9uUHJvdmlkZXIsIGV4dGVuZCB9IGZyb20gXCJ2ZWUtdmFsaWRhdGVcIlxyXG4gIGltcG9ydCB7IHJlcXVpcmVkLCBlbWFpbCwgbWluLCBtYXgsIGludGVnZXIgfSBmcm9tICd2ZWUtdmFsaWRhdGUvZGlzdC9ydWxlcydcclxuXHJcbiAgaW1wb3J0IHsgRVZFTlRfQlVTX0tFWSB9IGZyb20gJyRqc0Jhc2VQYXRoL2RlZmluZSc7XHJcbiAgaW1wb3J0IEV2ZW50QnVzIGZyb20gJyRqc0Jhc2VQYXRoL0V2ZW50QnVzJztcclxuXHJcbiAgaW1wb3J0IFZlZVZhbGlkYXRlTWVzc2FnZSBmcm9tICckanNDb21tb25QYXRoL3V0aWwvVmVlVmFsaWRhdGVNZXNzYWdlJ1xyXG4gIGltcG9ydCB7IF9pc0VtcHR5U3RyaW5nLCBfaW50IH0gZnJvbSBcIiRqc0NvbW1vblBhdGgvdXRpbC9TYWZlVXRpbFwiXHJcblxyXG4gIGxldCBvYmpNZXNzYWdlID0gVmVlVmFsaWRhdGVNZXNzYWdlLm1lc3NhZ2VzXHJcblxyXG4gIGV4dGVuZCgncmVxdWlyZWQnLCB7IC4uLnJlcXVpcmVkLCBtZXNzYWdlOiBvYmpNZXNzYWdlLnJlcXVpcmVkIH0pXHJcbiAgZXh0ZW5kKCdlbWFpbCcsIHsgLi4uZW1haWwsIG1lc3NhZ2U6IG9iak1lc3NhZ2UuZW1haWwgfSlcclxuICBleHRlbmQoJ21pbicsIHsgLi4ubWluLCBtZXNzYWdlOiBvYmpNZXNzYWdlLm1pbiB9KVxyXG4gIGV4dGVuZCgnbWF4JywgeyAuLi5tYXgsIG1lc3NhZ2U6IG9iak1lc3NhZ2UubWF4IH0pXHJcbiAgZXh0ZW5kKCdpbnRlZ2VyJywgeyAuLi5pbnRlZ2VyLCBtZXNzYWdlOiBvYmpNZXNzYWdlLmludGVnZXIgfSlcclxuXHJcbiAgZXhwb3J0IGRlZmF1bHQge1xyXG4gICAgbmFtZTogJ3VzZXItYWRkLXNlY3Rpb24nLFxyXG4gICAgY29tcG9uZW50czoge1xyXG4gICAgICAndmFsaWRhdGlvbi1vYnNlcnZlcic6IFZhbGlkYXRpb25PYnNlcnZlcixcclxuICAgICAgJ3ZhbGlkYXRpb24tcHJvdmlkZXInOiBWYWxpZGF0aW9uUHJvdmlkZXJcclxuICAgIH0sXHJcbiAgICBkYXRhKCkge1xyXG4gICAgICByZXR1cm4ge1xyXG4gICAgICAgIHVybDoge1xyXG4gICAgICAgICAgcmVzb3VyY2VVUkw6IENEQVRBLnJlc291cmNlVVJMXHJcbiAgICAgICAgfSxcclxuICAgICAgICBmbGFnOiB7XHJcbiAgICAgICAgICBpc1ZhbGlkYXRpbmc6IGZhbHNlLFxyXG4gICAgICAgICAgaXNVcGRhdGluZzogZmFsc2VcclxuICAgICAgICB9LFxyXG4gICAgICAgIGZybURhdGE6IHt9XHJcbiAgICAgIH1cclxuICAgIH0sXHJcbiAgICBjb21wdXRlZDoge1xyXG4gICAgICBwcm9maWxlSW1hZ2UoKSB7XHJcbiAgICAgICAgaWYgKF9pc0VtcHR5KHRoaXMuZnJtRGF0YS5wcm9maWxlX2ltYWdlKSlcclxuICAgICAgICAgIHJldHVybiB0aGlzLnVybC5yZXNvdXJjZVVSTCArICcvZ2xvYmFsL2ltYWdlL25vdEZvdW5kLnBuZydcclxuXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZnJtRGF0YS5wcm9maWxlX2ltYWdlXHJcbiAgICAgIH0sXHJcbiAgICAgIG9iakVudGl0eVNlbGVjdCgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy4kc3RvcmUuc3RhdGUub2JqRW50aXR5U2VsZWN0XHJcbiAgICAgIH1cclxuICAgIH0sXHJcbiAgICBjcmVhdGVkKCkge1xyXG4gICAgICBsZXQgc2VsZiA9IHRoaXNcclxuICAgICAgRXZlbnRCdXMub2ZmKEVWRU5UX0JVU19LRVkuQ09ORklSTV9NT0RBTClcclxuICAgICAgRXZlbnRCdXMub24oRVZFTlRfQlVTX0tFWS5DT05GSVJNX01PREFMLCAocGF5bG9hZCkgPT4ge1xyXG4gICAgICAgIHNlbGYuZmxhZy5pc1ZhbGlkYXRpbmcgPSBmYWxzZVxyXG4gICAgICAgIHNlbGYuZmxhZy5pc1VwZGF0aW5nID0gZmFsc2VcclxuXHJcbiAgICAgICAgdGhpcy5mcm1EYXRhID0gdGhpcy5vYmpFbnRpdHlTZWxlY3RcclxuXHJcbiAgICAgICAgdGhpcy5mcm1EYXRhLnBhc3N3b3JkID0gbnVsbFxyXG4gICAgICB9KVxyXG5cclxuICAgICAgaWYgKCF0aGlzLiRzdG9yZS5zdGF0ZS5mbGFnLmlzR2V0KSB7XHJcbiAgICAgICAgbGV0IHsgaWQsIHBhZ2UgfSA9IHRoaXMuJHJvdXRlLnBhcmFtc1xyXG4gICAgICAgIGlmIChfaW50KGlkKSA8PSAwKVxyXG4gICAgICAgICAgcmV0dXJuXHJcblxyXG4gICAgICAgIHRoaXMuZ2V0KGlkKVxyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMuZnJtRGF0YSA9IHRoaXMub2JqRW50aXR5U2VsZWN0XHJcbiAgICAgIH1cclxuICAgIH0sXHJcbiAgICBtZXRob2RzOiB7XHJcbiAgICAgIGdvUmVhZCgpIHtcclxuICAgICAgICB0aGlzLiRyb3V0ZXIucHVzaCh7IG5hbWU6ICd1c2VyOmluZGV4JyB9KVxyXG4gICAgICB9LFxyXG4gICAgICBnb0NyZWF0ZSgpIHtcclxuICAgICAgICB0aGlzLiRyb3V0ZXIucHVzaCh7IG5hbWU6ICd1c2VyOmFkZCcsIHBhcmFtczogeyBwYWdlOiAxIH0gfSlcclxuICAgICAgfSxcclxuICAgICAgYmFjaygpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy4kcm91dGVyLmJhY2soKVxyXG4gICAgICB9LFxyXG4gICAgICBnZXQoaWQpIHtcclxuICAgICAgICB0aGlzLiRzdG9yZS5kaXNwYXRjaCgnZ2V0RW50aXR5QWN0JywgeyBpZDogaWQgfSkudGhlbihwYXlsb2FkID0+IHtcclxuICAgICAgICAgIHRoaXMuZnJtRGF0YSA9IHRoaXMub2JqRW50aXR5U2VsZWN0XHJcbiAgICAgICAgfSlcclxuICAgICAgfSxcclxuICAgICAgdXBkYXRlKGludmFsaWQpIHtcclxuICAgICAgICB0aGlzLmZsYWcuaXNWYWxpZGF0aW5nID0gdHJ1ZVxyXG5cclxuICAgICAgICBpZiAoaW52YWxpZCkge1xyXG4gICAgICAgICAgdGhpcy4kcmVmcy5vYnNlcnZlci52YWxpZGF0ZSgpXHJcbiAgICAgICAgICByZXR1cm5cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuZmxhZy5pc1VwZGF0aW5nID0gdHJ1ZVxyXG5cclxuICAgICAgICBsZXQgaWQgPSB0aGlzLm9iakVudGl0eVNlbGVjdC51c2VyX2lkXHJcbiAgICAgICAgdGhpcy4kc3RvcmUuZGlzcGF0Y2goJ3VwZGF0ZUVudGl0eUFjdCcsIHsgaWQ6IGlkLCAuLi50aGlzLmZybURhdGEgfSlcclxuICAgICAgfSxcclxuICAgICAgdXBsb2FkKGUpIHtcclxuICAgICAgICBjb25zdCBmaWxlID0gZS50YXJnZXQuZmlsZXNbIDAgXVxyXG5cclxuICAgICAgICB0aGlzLiRzdG9yZS5kaXNwYXRjaCgndXBsb2FkRmlsZUFjdCcsIHtcclxuICAgICAgICAgIGNvbnRyb2xsZXI6IENEQVRBLmNvbnRyb2xsZXIsXHJcbiAgICAgICAgICBmaWxlOiBmaWxlXHJcbiAgICAgICAgfSkudGhlbihwYXlsb2FkID0+IHtcclxuICAgICAgICAgIGxldCBvYmpJbWFnZSA9IHBheWxvYWQuYXJySW1hZ2VbIDAgXVxyXG4gICAgICAgICAgaWYgKF9pc0VtcHR5KG9iakltYWdlKSkge1xyXG4gICAgICAgICAgICByZXR1cm5cclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICB0aGlzLmZybURhdGEucHJvZmlsZV9pbWFnZSA9IG9iakltYWdlLmltYWdlXHJcbiAgICAgICAgfSlcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuPC9zY3JpcHQ+XHJcblxyXG4iLCI8dGVtcGxhdGU+XHJcbiAgPGRpdiBjbGFzcz1cImNvbnRhaW5lci1mbHVpZFwiPlxyXG4gICAgPGRpdiBjbGFzcz1cInBhZ2UtaGVhZGVyXCI+XHJcbiAgICAgIDxoMiBjbGFzcz1cImhlYWRlci10aXRsZVwiPk5nxrDhu51pIGTDuW5nPC9oMj5cclxuICAgICAgPGRpdiBjbGFzcz1cImhlYWRlci1zdWItdGl0bGVcIj5cclxuICAgICAgICA8bmF2IGNsYXNzPVwiYnJlYWRjcnVtYiBicmVhZGNydW1iLWRhc2hcIj5cclxuICAgICAgICAgIDxhIGhyZWY9XCIvYmFja2VuZFwiIGNsYXNzPVwiYnJlYWRjcnVtYi1pdGVtXCI+PGkgY2xhc3M9XCJ0aS1ob21lIHAtci01XCI+PC9pPkjhu4cgdGjhu5FuZzwvYT5cclxuICAgICAgICAgIDxhXHJcbiAgICAgICAgICAgICAgY2xhc3M9XCJicmVhZGNydW1iLWl0ZW1cIlxyXG4gICAgICAgICAgICAgIGhyZWY9XCIjXCJcclxuICAgICAgICAgICAgICBAY2xpY2sucHJldmVudD1cIlwiXHJcbiAgICAgICAgICA+PGkgY2xhc3M9XCJ0aS11c2VyIHAtci01XCI+PC9pPk5nxrDhu51pIGTDuW5nPC9hPlxyXG4gICAgICAgICAgPHNwYW4gY2xhc3M9XCJicmVhZGNydW1iLWl0ZW0gYWN0aXZlXCI+PGkgY2xhc3M9XCJ0aS1saXN0IHAtci01XCI+PC9pPkRhbmggc8OhY2g8L3NwYW4+XHJcbiAgICAgICAgPC9uYXY+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgICA8ZGl2IGNsYXNzPVwiX2hlYWRlci1idXR0b25cIj5cclxuICAgICAgICA8YnV0dG9uXHJcbiAgICAgICAgICAgIGNsYXNzPVwiYnRuIGJ0bi1ncmFkaWVudC1zdWNjZXNzXCJcclxuICAgICAgICAgICAgQGNsaWNrLnByZXZlbnQ9XCJnb0NyZWF0ZSgpXCJcclxuICAgICAgICA+VOG6oW8gbmfGsOG7nWkgZMO5bmdcclxuICAgICAgICA8L2J1dHRvbj5cclxuICAgICAgPC9kaXY+XHJcbiAgICA8L2Rpdj5cclxuICAgIDxkaXYgY2xhc3M9XCJjYXJkXCI+XHJcbiAgICAgIDxkaXYgY2xhc3M9XCJjYXJkLWJvZHlcIj5cclxuICAgICAgICA8dXNlci10YWJsZT48L3VzZXItdGFibGU+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgPC9kaXY+XHJcbiAgPC9kaXY+XHJcbjwvdGVtcGxhdGU+XHJcblxyXG48c2NyaXB0PlxyXG4gIGltcG9ydCBVc2VyVGFibGUgZnJvbSAnJHZ1ZVBhcnRpYWxQYXRoL3VzZXIvVXNlclRhYmxlJ1xyXG5cclxuICBleHBvcnQgZGVmYXVsdCB7XHJcbiAgICBuYW1lOiAndXNlci1pbmRleC1zZWN0aW9uJyxcclxuICAgIGNvbXBvbmVudHM6IHtcclxuICAgICAgJ3VzZXItdGFibGUnOiBVc2VyVGFibGVcclxuICAgIH0sXHJcbiAgICBkYXRhKCkge1xyXG4gICAgICByZXR1cm4ge31cclxuICAgIH0sXHJcbiAgICBjb21wdXRlZDoge30sXHJcbiAgICBjcmVhdGVkKCkge1xyXG4gICAgICBpZiAoIXRoaXMuJHN0b3JlLnN0YXRlLmZsYWcuaXNHZXQpIHtcclxuICAgICAgICB0aGlzLiRzdG9yZS5jb21taXQoJ3VwZGF0ZUZsYWcnLCB7IGZsYWc6ICdpc0dldCcsIHZhbHVlOiB0cnVlIH0pXHJcbiAgICAgICAgdGhpcy4kc3RvcmUuZGlzcGF0Y2goJ3JlYWRFbnRpdHlBY3QnKVxyXG4gICAgICB9XHJcbiAgICB9LFxyXG4gICAgbWV0aG9kczoge1xyXG4gICAgICBnb1JlYWQoKSB7XHJcbiAgICAgICAgdGhpcy4kcm91dGVyLnB1c2goeyBuYW1lOiAndXNlcjppbmRleCcgfSlcclxuICAgICAgfSxcclxuICAgICAgZ29DcmVhdGUoKSB7XHJcbiAgICAgICAgdGhpcy4kcm91dGVyLnB1c2goeyBuYW1lOiAndXNlcjphZGQnLCBwYXJhbXM6IHsgcGFnZTogMSB9IH0pXHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbjwvc2NyaXB0PlxyXG5cclxuIiwiPHRlbXBsYXRlPlxyXG4gIDxkaXYgaWQ9XCJ1c2VyLXRhYmxlXCIgY2xhc3M9XCJ0YWJsZS1vdmVyZmxvd1wiPlxyXG4gICAgPHRhYmxlIGlkPVwiZHQtb3B0XCIgY2xhc3M9XCJ0YWJsZSB0YWJsZS1ob3ZlciB0YWJsZS14bFwiPlxyXG4gICAgICA8dGhlYWQ+XHJcbiAgICAgICAgPHRyPlxyXG4gICAgICAgICAgPHRoPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY2hlY2tib3ggcC0wXCI+XHJcbiAgICAgICAgICAgICAgPGlucHV0IGlkPVwic2VsZWN0YWJsZTFcIiB0eXBlPVwiY2hlY2tib3hcIiBjbGFzcz1cImNoZWNrQWxsXCIgbmFtZT1cImNoZWNrQWxsXCI+XHJcbiAgICAgICAgICAgICAgPGxhYmVsIGZvcj1cInNlbGVjdGFibGUxXCI+PC9sYWJlbD5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICA8L3RoPlxyXG4gICAgICAgICAgPHRoPkZVTExOQU1FPC90aD5cclxuICAgICAgICAgIDx0aD5FTUFJTDwvdGg+XHJcbiAgICAgICAgICA8dGg+U1RBVFVTPC90aD5cclxuICAgICAgICAgIDx0aD4jPC90aD5cclxuICAgICAgICAgIDx0aD48L3RoPlxyXG4gICAgICAgIDwvdHI+XHJcbiAgICAgIDwvdGhlYWQ+XHJcbiAgICAgIDx0Ym9keT5cclxuICAgICAgICA8dGVtcGxhdGUgdi1mb3I9XCIob2JqRW50aXR5LCBpbmRleCkgaW4gYXJyRW50aXR5TGlzdFwiPlxyXG4gICAgICAgICAgPHVzZXItdGFibGUtcm93XHJcbiAgICAgICAgICAgICAgOmluZGV4PVwiaW5kZXhcIlxyXG4gICAgICAgICAgICAgIDpvYmotZW50aXR5PVwib2JqRW50aXR5XCJcclxuICAgICAgICAgID48L3VzZXItdGFibGUtcm93PlxyXG4gICAgICAgIDwvdGVtcGxhdGU+XHJcbiAgICAgIDwvdGJvZHk+XHJcbiAgICA8L3RhYmxlPlxyXG4gIDwvZGl2PlxyXG48L3RlbXBsYXRlPlxyXG5cclxuPHNjcmlwdD5cclxuICBpbXBvcnQgVXNlclRhYmxlUm93IGZyb20gJyR2dWVQYXJ0aWFsUGF0aC91c2VyL1VzZXJUYWJsZVJvdydcclxuXHJcbiAgZXhwb3J0IGRlZmF1bHQge1xyXG4gICAgbmFtZTogJ3VzZXItdGFibGUnLFxyXG4gICAgY29tcG9uZW50czoge1xyXG4gICAgICAndXNlci10YWJsZS1yb3cnOiBVc2VyVGFibGVSb3dcclxuICAgIH0sXHJcbiAgICBkYXRhKCkge1xyXG4gICAgICByZXR1cm4ge1xyXG4gICAgICB9XHJcbiAgICB9LFxyXG4gICAgY29tcHV0ZWQ6IHtcclxuICAgICAgYXJyRW50aXR5TGlzdCgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy4kc3RvcmUuc3RhdGUuYXJyRW50aXR5TGlzdFxyXG4gICAgICB9XHJcbiAgICB9LFxyXG4gICAgY3JlYXRlZCgpIHtcclxuXHJcbiAgICB9LFxyXG4gICAgbWV0aG9kczoge31cclxuICB9XHJcbjwvc2NyaXB0PlxyXG5cclxuIiwiPHRlbXBsYXRlPlxyXG4gIDx0cj5cclxuICAgIDx0ZD5cclxuICAgICAgPGRpdiBjbGFzcz1cImNoZWNrYm94XCI+XHJcbiAgICAgICAgPGlucHV0IGlkPVwic2VsZWN0YWJsZTJcIiB0eXBlPVwiY2hlY2tib3hcIj5cclxuICAgICAgICA8bGFiZWwgZm9yPVwic2VsZWN0YWJsZTJcIj48L2xhYmVsPlxyXG4gICAgICA8L2Rpdj5cclxuICAgIDwvdGQ+XHJcbiAgICA8dGQ+XHJcbiAgICAgIDxkaXYgY2xhc3M9XCJsaXN0LW1lZGlhXCI+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cImxpc3QtaXRlbVwiPlxyXG4gICAgICAgICAgPGRpdiBjbGFzcz1cIm1lZGlhLWltZ1wiPlxyXG4gICAgICAgICAgICA8aW1nXHJcbiAgICAgICAgICAgICAgICA6c3JjPVwicHJvZmlsZUltYWdlXCJcclxuICAgICAgICAgICAgICAgIGFsdD1cIlwiXHJcbiAgICAgICAgICAgID5cclxuICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgPGRpdiBjbGFzcz1cImluZm9cIj5cclxuICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJ0aXRsZVwiPnt7b2JqRW50aXR5LmZ1bGxuYW1lfX08L3NwYW4+XHJcbiAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwic3ViLXRpdGxlXCI+SUQge3tvYmpFbnRpdHkudXNlcl9pZH19PC9zcGFuPlxyXG4gICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgPC90ZD5cclxuICAgIDx0ZD57e29iakVudGl0eS5lbWFpbH19PC90ZD5cclxuICAgIDx0ZD5cclxuICAgICAgPHRlbXBsYXRlIHYtaWY9XCJvYmpFbnRpdHkuaXNfYWN0aXZlZCA9PSAxXCI+XHJcbiAgICAgICAgPHNwYW4gY2xhc3M9XCJiYWRnZSBiYWRnZS1waWxsIGJhZGdlLWdyYWRpZW50LXN1Y2Nlc3NcIj7EkMOjIGvDrWNoIGhv4bqhdDwvc3Bhbj5cclxuICAgICAgPC90ZW1wbGF0ZT5cclxuICAgICAgPHRlbXBsYXRlIHYtZWxzZT5cclxuICAgICAgICA8c3BhbiBjbGFzcz1cImJhZGdlIGJhZGdlLXBpbGwgYmFkZ2UtZ3JhZGllbnQtZGFuZ2VyXCI+Q2jGsGEga8OtY2ggaG/huqF0PC9zcGFuPlxyXG4gICAgICA8L3RlbXBsYXRlPlxyXG4gICAgPC90ZD5cclxuICAgIDx0ZCBjbGFzcz1cInRleHQtY2VudGVyIGZvbnQtc2l6ZS0xOFwiPlxyXG4gICAgICA8YVxyXG4gICAgICAgICAgY2xhc3M9XCJ0ZXh0LWdyYXkgbS1yLTE1XCJcclxuICAgICAgICAgIEBjbGljaz1cImdvVXBkYXRlKClcIlxyXG4gICAgICA+PGkgY2xhc3M9XCJ0aS1wZW5jaWxcIj48L2k+XHJcbiAgICAgIDwvYT5cclxuICAgICAgPGFcclxuICAgICAgICAgIGNsYXNzPVwidGV4dC1ncmF5XCJcclxuICAgICAgICAgIEBjbGljaz1cInJlbW92ZShvYmpFbnRpdHkudXNlcl9pZClcIlxyXG4gICAgICA+PGlcclxuICAgICAgICAgIDpjbGFzcz1cIntcclxuICAgICAgICAgICdmYSBmYS1zcGlubmVyIGZhLXB1bHNlIGZhLWZ3JzogZmxhZy5pc0RlbGV0aW5nLFxyXG4gICAgICAgICAgJ3RpLXRyYXNoJzogIWZsYWcuaXNEZWxldGluZ1xyXG4gICAgICAgICAgfVwiPlxyXG4gICAgICA8L2k+PC9hPlxyXG4gICAgPC90ZD5cclxuICA8L3RyPlxyXG48L3RlbXBsYXRlPlxyXG5cclxuPHNjcmlwdD5cclxuICBpbXBvcnQgeyBfaXNFbXB0eVN0cmluZyB9IGZyb20gXCIkanNDb21tb25QYXRoL3V0aWwvU2FmZVV0aWxcIlxyXG5cclxuICBleHBvcnQgZGVmYXVsdCB7XHJcbiAgICBuYW1lOiAndXNlci10YWJsZS1yb3cnLFxyXG4gICAgcHJvcHM6IHtcclxuICAgICAgaW5kZXg6IHtcclxuICAgICAgICBkZWZhdWx0OiB7fSxcclxuICAgICAgICB0eXBlOiBOdW1iZXJcclxuICAgICAgfSxcclxuICAgICAgb2JqRW50aXR5OiB7XHJcbiAgICAgICAgZGVmYXVsdDoge30sXHJcbiAgICAgICAgdHlwZTogT2JqZWN0XHJcbiAgICAgIH0sXHJcbiAgICB9LFxyXG4gICAgY29tcG9uZW50czoge30sXHJcbiAgICBkYXRhKCkge1xyXG4gICAgICByZXR1cm4ge1xyXG4gICAgICAgIHVybDoge1xyXG4gICAgICAgICAgcmVzb3VyY2VVUkw6IENEQVRBLnJlc291cmNlVVJMXHJcbiAgICAgICAgfSxcclxuICAgICAgICBmbGFnOiB7XHJcbiAgICAgICAgICBpc0RlbGV0aW5nOiBmYWxzZVxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfSxcclxuICAgIGNvbXB1dGVkOiB7XHJcbiAgICAgIHByb2ZpbGVJbWFnZSgpIHtcclxuICAgICAgICBpZiAoX2lzRW1wdHlTdHJpbmcodGhpcy5vYmpFbnRpdHkucHJvZmlsZV9pbWFnZSkpXHJcbiAgICAgICAgICByZXR1cm4gdGhpcy51cmwucmVzb3VyY2VVUkwgKyAnL2dsb2JhbC9pbWFnZS9ub3RGb3VuZC5wbmcnXHJcblxyXG4gICAgICAgIHJldHVybiB0aGlzLm9iakVudGl0eS5wcm9maWxlX2ltYWdlXHJcbiAgICAgIH0sXHJcbiAgICB9LFxyXG4gICAgY3JlYXRlZCgpIHtcclxuICAgIH0sXHJcbiAgICBtZXRob2RzOiB7XHJcbiAgICAgIGdvVXBkYXRlKCkge1xyXG4gICAgICAgIHRoaXMuJHN0b3JlLmRpc3BhdGNoKCdzZXRFbnRpdHlBY3QnLCB7IGluZGV4OiB0aGlzLmluZGV4IH0pXHJcblxyXG4gICAgICAgIHRoaXMuJHJvdXRlci5wdXNoKHsgbmFtZTogJ3VzZXI6ZWRpdCcsIHBhcmFtczogeyBpZDogdGhpcy5vYmpFbnRpdHkudXNlcl9pZCwgcGFnZTogMSB9IH0pXHJcbiAgICAgIH0sXHJcbiAgICAgIHJlbW92ZShpZCkge1xyXG4gICAgICAgIGlmICghaWQpXHJcbiAgICAgICAgICByZXR1cm5cclxuXHJcbiAgICAgICAgdGhpcy5mbGFnLmlzRGVsZXRpbmcgPSB0cnVlXHJcbiAgICAgICAgdGhpcy4kc3RvcmUuZGlzcGF0Y2goJ2RlbGV0ZUVudGl0eUFjdCcsIHsgaWQ6IGlkIH0pLnRoZW4ocmVzID0+IHtcclxuICAgICAgICAgIHRoaXMuZmxhZy5pc0RlbGV0aW5nID0gZmFsc2VcclxuICAgICAgICB9KVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG48L3NjcmlwdD5cclxuXHJcbiIsIi8qKlxuICAqIHZlZS12YWxpZGF0ZSB2My4wLjExXG4gICogKGMpIDIwMTkgQWJkZWxyYWhtYW4gQXdhZFxuICAqIEBsaWNlbnNlIE1JVFxuICAqL1xuLyoqXHJcbiAqIFNvbWUgQWxwaGEgUmVnZXggaGVscGVycy5cclxuICogaHR0cHM6Ly9naXRodWIuY29tL2Nocmlzby92YWxpZGF0b3IuanMvYmxvYi9tYXN0ZXIvc3JjL2xpYi9hbHBoYS5qc1xyXG4gKi9cclxudmFyIGFscGhhID0ge1xyXG4gICAgZW46IC9eW0EtWl0qJC9pLFxyXG4gICAgY3M6IC9eW0EtWsOBxIzEjsOJxJrDjcWHw5PFmMWgxaTDmsWuw53FvV0qJC9pLFxyXG4gICAgZGE6IC9eW0EtWsOGw5jDhV0qJC9pLFxyXG4gICAgZGU6IC9eW0EtWsOEw5bDnMOfXSokL2ksXHJcbiAgICBlczogL15bQS1aw4HDicONw5HDk8Oaw5xdKiQvaSxcclxuICAgIGZyOiAvXltBLVrDgMOCw4bDh8OJw4jDisOLw4/DjsOUxZLDmcObw5zFuF0qJC9pLFxyXG4gICAgaXQ6IC9eW0EtWlxceEMwLVxceEZGXSokL2ksXHJcbiAgICBsdDogL15bQS1axITEjMSYxJbErsWgxbLFqsW9XSokL2ksXHJcbiAgICBubDogL15bQS1aw4nDi8OPw5PDlsOcXSokL2ksXHJcbiAgICBodTogL15bQS1aw4HDicONw5PDlsWQw5rDnMWwXSokL2ksXHJcbiAgICBwbDogL15bQS1axITEhsSYxZrFgcWDw5PFu8W5XSokL2ksXHJcbiAgICBwdDogL15bQS1aw4PDgcOAw4LDh8OJw4rDjcOVw5PDlMOaw5xdKiQvaSxcclxuICAgIHJ1OiAvXlvQkC3Qr9CBXSokL2ksXHJcbiAgICBzazogL15bQS1aw4HDhMSMxI7DicONxLnEvcWHw5PFlMWgxaTDmsOdxb1dKiQvaSxcclxuICAgIHNyOiAvXltBLVrEjMSGxb3FoMSQXSokL2ksXHJcbiAgICBzdjogL15bQS1aw4XDhMOWXSokL2ksXHJcbiAgICB0cjogL15bQS1aw4fEnsSwxLHDlsWew5xdKiQvaSxcclxuICAgIHVrOiAvXlvQkC3QqdCs0K7Qr9CE0IbQh9KQXSokL2ksXHJcbiAgICBhcjogL15b2KHYotij2KTYpdim2KfYqNip2KrYq9is2K3Yrtiv2LDYsdiy2LPYtNi12LbYt9i42LnYutmB2YLZg9mE2YXZhtmH2YjZidmK2YvZjNmN2Y7Zj9mQ2ZHZktmwXSokLyxcclxuICAgIGF6OiAvXltBLVrDh8aPxJ7EsMSxw5bFnsOcXSokL2lcclxufTtcclxudmFyIGFscGhhU3BhY2VzID0ge1xyXG4gICAgZW46IC9eW0EtWlxcc10qJC9pLFxyXG4gICAgY3M6IC9eW0EtWsOBxIzEjsOJxJrDjcWHw5PFmMWgxaTDmsWuw53FvVxcc10qJC9pLFxyXG4gICAgZGE6IC9eW0EtWsOGw5jDhVxcc10qJC9pLFxyXG4gICAgZGU6IC9eW0EtWsOEw5bDnMOfXFxzXSokL2ksXHJcbiAgICBlczogL15bQS1aw4HDicONw5HDk8Oaw5xcXHNdKiQvaSxcclxuICAgIGZyOiAvXltBLVrDgMOCw4bDh8OJw4jDisOLw4/DjsOUxZLDmcObw5zFuFxcc10qJC9pLFxyXG4gICAgaXQ6IC9eW0EtWlxceEMwLVxceEZGXFxzXSokL2ksXHJcbiAgICBsdDogL15bQS1axITEjMSYxJbErsWgxbLFqsW9XFxzXSokL2ksXHJcbiAgICBubDogL15bQS1aw4nDi8OPw5PDlsOcXFxzXSokL2ksXHJcbiAgICBodTogL15bQS1aw4HDicONw5PDlsWQw5rDnMWwXFxzXSokL2ksXHJcbiAgICBwbDogL15bQS1axITEhsSYxZrFgcWDw5PFu8W5XFxzXSokL2ksXHJcbiAgICBwdDogL15bQS1aw4PDgcOAw4LDh8OJw4rDjcOVw5PDlMOaw5xcXHNdKiQvaSxcclxuICAgIHJ1OiAvXlvQkC3Qr9CBXFxzXSokL2ksXHJcbiAgICBzazogL15bQS1aw4HDhMSMxI7DicONxLnEvcWHw5PFlMWgxaTDmsOdxb1cXHNdKiQvaSxcclxuICAgIHNyOiAvXltBLVrEjMSGxb3FoMSQXFxzXSokL2ksXHJcbiAgICBzdjogL15bQS1aw4XDhMOWXFxzXSokL2ksXHJcbiAgICB0cjogL15bQS1aw4fEnsSwxLHDlsWew5xcXHNdKiQvaSxcclxuICAgIHVrOiAvXlvQkC3QqdCs0K7Qr9CE0IbQh9KQXFxzXSokL2ksXHJcbiAgICBhcjogL15b2KHYotij2KTYpdim2KfYqNip2KrYq9is2K3Yrtiv2LDYsdiy2LPYtNi12LbYt9i42LnYutmB2YLZg9mE2YXZhtmH2YjZidmK2YvZjNmN2Y7Zj9mQ2ZHZktmwXFxzXSokLyxcclxuICAgIGF6OiAvXltBLVrDh8aPxJ7EsMSxw5bFnsOcXFxzXSokL2lcclxufTtcclxudmFyIGFscGhhbnVtZXJpYyA9IHtcclxuICAgIGVuOiAvXlswLTlBLVpdKiQvaSxcclxuICAgIGNzOiAvXlswLTlBLVrDgcSMxI7DicSaw43Fh8OTxZjFoMWkw5rFrsOdxb1dKiQvaSxcclxuICAgIGRhOiAvXlswLTlBLVrDhsOYw4VdJC9pLFxyXG4gICAgZGU6IC9eWzAtOUEtWsOEw5bDnMOfXSokL2ksXHJcbiAgICBlczogL15bMC05QS1aw4HDicONw5HDk8Oaw5xdKiQvaSxcclxuICAgIGZyOiAvXlswLTlBLVrDgMOCw4bDh8OJw4jDisOLw4/DjsOUxZLDmcObw5zFuF0qJC9pLFxyXG4gICAgaXQ6IC9eWzAtOUEtWlxceEMwLVxceEZGXSokL2ksXHJcbiAgICBsdDogL15bMC05QS1axITEjMSYxJbErsWgxbLFqsW9XSokL2ksXHJcbiAgICBodTogL15bMC05QS1aw4HDicONw5PDlsWQw5rDnMWwXSokL2ksXHJcbiAgICBubDogL15bMC05QS1aw4nDi8OPw5PDlsOcXSokL2ksXHJcbiAgICBwbDogL15bMC05QS1axITEhsSYxZrFgcWDw5PFu8W5XSokL2ksXHJcbiAgICBwdDogL15bMC05QS1aw4PDgcOAw4LDh8OJw4rDjcOVw5PDlMOaw5xdKiQvaSxcclxuICAgIHJ1OiAvXlswLTnQkC3Qr9CBXSokL2ksXHJcbiAgICBzazogL15bMC05QS1aw4HDhMSMxI7DicONxLnEvcWHw5PFlMWgxaTDmsOdxb1dKiQvaSxcclxuICAgIHNyOiAvXlswLTlBLVrEjMSGxb3FoMSQXSokL2ksXHJcbiAgICBzdjogL15bMC05QS1aw4XDhMOWXSokL2ksXHJcbiAgICB0cjogL15bMC05QS1aw4fEnsSwxLHDlsWew5xdKiQvaSxcclxuICAgIHVrOiAvXlswLTnQkC3QqdCs0K7Qr9CE0IbQh9KQXSokL2ksXHJcbiAgICBhcjogL15b2aDZodmi2aPZpNml2abZp9mo2akwLTnYodii2KPYpNil2KbYp9io2KnYqtir2KzYrdiu2K/YsNix2LLYs9i02LXYtti32LjYudi62YHZgtmD2YTZhdmG2YfZiNmJ2YrZi9mM2Y3ZjtmP2ZDZkdmS2bBdKiQvLFxyXG4gICAgYXo6IC9eWzAtOUEtWsOHxo/EnsSwxLHDlsWew5xdKiQvaVxyXG59O1xyXG52YXIgYWxwaGFEYXNoID0ge1xyXG4gICAgZW46IC9eWzAtOUEtWl8tXSokL2ksXHJcbiAgICBjczogL15bMC05QS1aw4HEjMSOw4nEmsONxYfDk8WYxaDFpMOaxa7DncW9Xy1dKiQvaSxcclxuICAgIGRhOiAvXlswLTlBLVrDhsOYw4VfLV0qJC9pLFxyXG4gICAgZGU6IC9eWzAtOUEtWsOEw5bDnMOfXy1dKiQvaSxcclxuICAgIGVzOiAvXlswLTlBLVrDgcOJw43DkcOTw5rDnF8tXSokL2ksXHJcbiAgICBmcjogL15bMC05QS1aw4DDgsOGw4fDicOIw4rDi8OPw47DlMWSw5nDm8OcxbhfLV0qJC9pLFxyXG4gICAgaXQ6IC9eWzAtOUEtWlxceEMwLVxceEZGXy1dKiQvaSxcclxuICAgIGx0OiAvXlswLTlBLVrEhMSMxJjElsSuxaDFssWqxb1fLV0qJC9pLFxyXG4gICAgbmw6IC9eWzAtOUEtWsOJw4vDj8OTw5bDnF8tXSokL2ksXHJcbiAgICBodTogL15bMC05QS1aw4HDicONw5PDlsWQw5rDnMWwXy1dKiQvaSxcclxuICAgIHBsOiAvXlswLTlBLVrEhMSGxJjFmsWBxYPDk8W7xblfLV0qJC9pLFxyXG4gICAgcHQ6IC9eWzAtOUEtWsODw4HDgMOCw4fDicOKw43DlcOTw5TDmsOcXy1dKiQvaSxcclxuICAgIHJ1OiAvXlswLTnQkC3Qr9CBXy1dKiQvaSxcclxuICAgIHNrOiAvXlswLTlBLVrDgcOExIzEjsOJw43EucS9xYfDk8WUxaDFpMOaw53FvV8tXSokL2ksXHJcbiAgICBzcjogL15bMC05QS1axIzEhsW9xaDEkF8tXSokL2ksXHJcbiAgICBzdjogL15bMC05QS1aw4XDhMOWXy1dKiQvaSxcclxuICAgIHRyOiAvXlswLTlBLVrDh8SexLDEscOWxZ7DnF8tXSokL2ksXHJcbiAgICB1azogL15bMC050JAt0KnQrNCu0K/QhNCG0IfSkF8tXSokL2ksXHJcbiAgICBhcjogL15b2aDZodmi2aPZpNml2abZp9mo2akwLTnYodii2KPYpNil2KbYp9io2KnYqtir2KzYrdiu2K/YsNix2LLYs9i02LXYtti32LjYudi62YHZgtmD2YTZhdmG2YfZiNmJ2YrZi9mM2Y3ZjtmP2ZDZkdmS2bBfLV0qJC8sXHJcbiAgICBhejogL15bMC05QS1aw4fGj8SexLDEscOWxZ7DnF8tXSokL2lcclxufTtcblxudmFyIHZhbGlkYXRlID0gZnVuY3Rpb24gKHZhbHVlLCBfYSkge1xyXG4gICAgdmFyIF9iID0gKF9hID09PSB2b2lkIDAgPyB7fSA6IF9hKS5sb2NhbGUsIGxvY2FsZSA9IF9iID09PSB2b2lkIDAgPyAnJyA6IF9iO1xyXG4gICAgaWYgKEFycmF5LmlzQXJyYXkodmFsdWUpKSB7XHJcbiAgICAgICAgcmV0dXJuIHZhbHVlLmV2ZXJ5KGZ1bmN0aW9uICh2YWwpIHsgcmV0dXJuIHZhbGlkYXRlKHZhbCwgeyBsb2NhbGU6IGxvY2FsZSB9KTsgfSk7XHJcbiAgICB9XHJcbiAgICAvLyBNYXRjaCBhdCBsZWFzdCBvbmUgbG9jYWxlLlxyXG4gICAgaWYgKCFsb2NhbGUpIHtcclxuICAgICAgICByZXR1cm4gT2JqZWN0LmtleXMoYWxwaGEpLnNvbWUoZnVuY3Rpb24gKGxvYykgeyByZXR1cm4gYWxwaGFbbG9jXS50ZXN0KHZhbHVlKTsgfSk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gKGFscGhhW2xvY2FsZV0gfHwgYWxwaGEuZW4pLnRlc3QodmFsdWUpO1xyXG59O1xyXG52YXIgcGFyYW1zID0gW1xyXG4gICAge1xyXG4gICAgICAgIG5hbWU6ICdsb2NhbGUnXHJcbiAgICB9XHJcbl07XHJcbnZhciBhbHBoYSQxID0ge1xyXG4gICAgdmFsaWRhdGU6IHZhbGlkYXRlLFxyXG4gICAgcGFyYW1zOiBwYXJhbXNcclxufTtcblxudmFyIHZhbGlkYXRlJDEgPSBmdW5jdGlvbiAodmFsdWUsIF9hKSB7XHJcbiAgICB2YXIgX2IgPSAoX2EgPT09IHZvaWQgMCA/IHt9IDogX2EpLmxvY2FsZSwgbG9jYWxlID0gX2IgPT09IHZvaWQgMCA/ICcnIDogX2I7XHJcbiAgICBpZiAoQXJyYXkuaXNBcnJheSh2YWx1ZSkpIHtcclxuICAgICAgICByZXR1cm4gdmFsdWUuZXZlcnkoZnVuY3Rpb24gKHZhbCkgeyByZXR1cm4gdmFsaWRhdGUkMSh2YWwsIHsgbG9jYWxlOiBsb2NhbGUgfSk7IH0pO1xyXG4gICAgfVxyXG4gICAgLy8gTWF0Y2ggYXQgbGVhc3Qgb25lIGxvY2FsZS5cclxuICAgIGlmICghbG9jYWxlKSB7XHJcbiAgICAgICAgcmV0dXJuIE9iamVjdC5rZXlzKGFscGhhRGFzaCkuc29tZShmdW5jdGlvbiAobG9jKSB7IHJldHVybiBhbHBoYURhc2hbbG9jXS50ZXN0KHZhbHVlKTsgfSk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gKGFscGhhRGFzaFtsb2NhbGVdIHx8IGFscGhhRGFzaC5lbikudGVzdCh2YWx1ZSk7XHJcbn07XHJcbnZhciBwYXJhbXMkMSA9IFtcclxuICAgIHtcclxuICAgICAgICBuYW1lOiAnbG9jYWxlJ1xyXG4gICAgfVxyXG5dO1xyXG52YXIgYWxwaGFfZGFzaCA9IHtcclxuICAgIHZhbGlkYXRlOiB2YWxpZGF0ZSQxLFxyXG4gICAgcGFyYW1zOiBwYXJhbXMkMVxyXG59O1xuXG52YXIgdmFsaWRhdGUkMiA9IGZ1bmN0aW9uICh2YWx1ZSwgX2EpIHtcclxuICAgIHZhciBfYiA9IChfYSA9PT0gdm9pZCAwID8ge30gOiBfYSkubG9jYWxlLCBsb2NhbGUgPSBfYiA9PT0gdm9pZCAwID8gJycgOiBfYjtcclxuICAgIGlmIChBcnJheS5pc0FycmF5KHZhbHVlKSkge1xyXG4gICAgICAgIHJldHVybiB2YWx1ZS5ldmVyeShmdW5jdGlvbiAodmFsKSB7IHJldHVybiB2YWxpZGF0ZSQyKHZhbCwgeyBsb2NhbGU6IGxvY2FsZSB9KTsgfSk7XHJcbiAgICB9XHJcbiAgICAvLyBNYXRjaCBhdCBsZWFzdCBvbmUgbG9jYWxlLlxyXG4gICAgaWYgKCFsb2NhbGUpIHtcclxuICAgICAgICByZXR1cm4gT2JqZWN0LmtleXMoYWxwaGFudW1lcmljKS5zb21lKGZ1bmN0aW9uIChsb2MpIHsgcmV0dXJuIGFscGhhbnVtZXJpY1tsb2NdLnRlc3QodmFsdWUpOyB9KTtcclxuICAgIH1cclxuICAgIHJldHVybiAoYWxwaGFudW1lcmljW2xvY2FsZV0gfHwgYWxwaGFudW1lcmljLmVuKS50ZXN0KHZhbHVlKTtcclxufTtcclxudmFyIHBhcmFtcyQyID0gW1xyXG4gICAge1xyXG4gICAgICAgIG5hbWU6ICdsb2NhbGUnXHJcbiAgICB9XHJcbl07XHJcbnZhciBhbHBoYV9udW0gPSB7XHJcbiAgICB2YWxpZGF0ZTogdmFsaWRhdGUkMixcclxuICAgIHBhcmFtczogcGFyYW1zJDJcclxufTtcblxudmFyIHZhbGlkYXRlJDMgPSBmdW5jdGlvbiAodmFsdWUsIF9hKSB7XHJcbiAgICB2YXIgX2IgPSAoX2EgPT09IHZvaWQgMCA/IHt9IDogX2EpLmxvY2FsZSwgbG9jYWxlID0gX2IgPT09IHZvaWQgMCA/ICcnIDogX2I7XHJcbiAgICBpZiAoQXJyYXkuaXNBcnJheSh2YWx1ZSkpIHtcclxuICAgICAgICByZXR1cm4gdmFsdWUuZXZlcnkoZnVuY3Rpb24gKHZhbCkgeyByZXR1cm4gdmFsaWRhdGUkMyh2YWwsIHsgbG9jYWxlOiBsb2NhbGUgfSk7IH0pO1xyXG4gICAgfVxyXG4gICAgLy8gTWF0Y2ggYXQgbGVhc3Qgb25lIGxvY2FsZS5cclxuICAgIGlmICghbG9jYWxlKSB7XHJcbiAgICAgICAgcmV0dXJuIE9iamVjdC5rZXlzKGFscGhhU3BhY2VzKS5zb21lKGZ1bmN0aW9uIChsb2MpIHsgcmV0dXJuIGFscGhhU3BhY2VzW2xvY10udGVzdCh2YWx1ZSk7IH0pO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIChhbHBoYVNwYWNlc1tsb2NhbGVdIHx8IGFscGhhU3BhY2VzLmVuKS50ZXN0KHZhbHVlKTtcclxufTtcclxudmFyIHBhcmFtcyQzID0gW1xyXG4gICAge1xyXG4gICAgICAgIG5hbWU6ICdsb2NhbGUnXHJcbiAgICB9XHJcbl07XHJcbnZhciBhbHBoYV9zcGFjZXMgPSB7XHJcbiAgICB2YWxpZGF0ZTogdmFsaWRhdGUkMyxcclxuICAgIHBhcmFtczogcGFyYW1zJDNcclxufTtcblxudmFyIHZhbGlkYXRlJDQgPSBmdW5jdGlvbiAodmFsdWUsIF9hKSB7XHJcbiAgICB2YXIgX2IgPSBfYSA9PT0gdm9pZCAwID8ge30gOiBfYSwgbWluID0gX2IubWluLCBtYXggPSBfYi5tYXg7XHJcbiAgICBpZiAoQXJyYXkuaXNBcnJheSh2YWx1ZSkpIHtcclxuICAgICAgICByZXR1cm4gdmFsdWUuZXZlcnkoZnVuY3Rpb24gKHZhbCkgeyByZXR1cm4gISF2YWxpZGF0ZSQ0KHZhbCwgeyBtaW46IG1pbiwgbWF4OiBtYXggfSk7IH0pO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIE51bWJlcihtaW4pIDw9IHZhbHVlICYmIE51bWJlcihtYXgpID49IHZhbHVlO1xyXG59O1xyXG52YXIgcGFyYW1zJDQgPSBbXHJcbiAgICB7XHJcbiAgICAgICAgbmFtZTogJ21pbidcclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgICAgbmFtZTogJ21heCdcclxuICAgIH1cclxuXTtcclxudmFyIGJldHdlZW4gPSB7XHJcbiAgICB2YWxpZGF0ZTogdmFsaWRhdGUkNCxcclxuICAgIHBhcmFtczogcGFyYW1zJDRcclxufTtcblxudmFyIHZhbGlkYXRlJDUgPSBmdW5jdGlvbiAodmFsdWUsIF9hKSB7XHJcbiAgICB2YXIgdGFyZ2V0ID0gX2EudGFyZ2V0O1xyXG4gICAgcmV0dXJuIFN0cmluZyh2YWx1ZSkgPT09IFN0cmluZyh0YXJnZXQpO1xyXG59O1xyXG52YXIgcGFyYW1zJDUgPSBbXHJcbiAgICB7XHJcbiAgICAgICAgbmFtZTogJ3RhcmdldCcsXHJcbiAgICAgICAgaXNUYXJnZXQ6IHRydWVcclxuICAgIH1cclxuXTtcclxudmFyIGNvbmZpcm1lZCA9IHtcclxuICAgIHZhbGlkYXRlOiB2YWxpZGF0ZSQ1LFxyXG4gICAgcGFyYW1zOiBwYXJhbXMkNVxyXG59O1xuXG52YXIgdmFsaWRhdGUkNiA9IGZ1bmN0aW9uICh2YWx1ZSwgX2EpIHtcclxuICAgIHZhciBsZW5ndGggPSBfYS5sZW5ndGg7XHJcbiAgICBpZiAoQXJyYXkuaXNBcnJheSh2YWx1ZSkpIHtcclxuICAgICAgICByZXR1cm4gdmFsdWUuZXZlcnkoZnVuY3Rpb24gKHZhbCkgeyByZXR1cm4gdmFsaWRhdGUkNih2YWwsIHsgbGVuZ3RoOiBsZW5ndGggfSk7IH0pO1xyXG4gICAgfVxyXG4gICAgdmFyIHN0clZhbCA9IFN0cmluZyh2YWx1ZSk7XHJcbiAgICByZXR1cm4gL15bMC05XSokLy50ZXN0KHN0clZhbCkgJiYgc3RyVmFsLmxlbmd0aCA9PT0gbGVuZ3RoO1xyXG59O1xyXG52YXIgcGFyYW1zJDYgPSBbXHJcbiAgICB7XHJcbiAgICAgICAgbmFtZTogJ2xlbmd0aCcsXHJcbiAgICAgICAgY2FzdDogZnVuY3Rpb24gKHZhbHVlKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBOdW1iZXIodmFsdWUpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXTtcclxudmFyIGRpZ2l0cyA9IHtcclxuICAgIHZhbGlkYXRlOiB2YWxpZGF0ZSQ2LFxyXG4gICAgcGFyYW1zOiBwYXJhbXMkNlxyXG59O1xuXG52YXIgdmFsaWRhdGVJbWFnZSA9IGZ1bmN0aW9uIChmaWxlLCB3aWR0aCwgaGVpZ2h0KSB7XHJcbiAgICB2YXIgVVJMID0gd2luZG93LlVSTCB8fCB3aW5kb3cud2Via2l0VVJMO1xyXG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKGZ1bmN0aW9uIChyZXNvbHZlKSB7XHJcbiAgICAgICAgdmFyIGltYWdlID0gbmV3IEltYWdlKCk7XHJcbiAgICAgICAgaW1hZ2Uub25lcnJvciA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIHJlc29sdmUoZmFsc2UpOyB9O1xyXG4gICAgICAgIGltYWdlLm9ubG9hZCA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIHJlc29sdmUoaW1hZ2Uud2lkdGggPT09IHdpZHRoICYmIGltYWdlLmhlaWdodCA9PT0gaGVpZ2h0KTsgfTtcclxuICAgICAgICBpbWFnZS5zcmMgPSBVUkwuY3JlYXRlT2JqZWN0VVJMKGZpbGUpO1xyXG4gICAgfSk7XHJcbn07XHJcbnZhciB2YWxpZGF0ZSQ3ID0gZnVuY3Rpb24gKGZpbGVzLCBfYSkge1xyXG4gICAgdmFyIHdpZHRoID0gX2Eud2lkdGgsIGhlaWdodCA9IF9hLmhlaWdodDtcclxuICAgIHZhciBsaXN0ID0gW107XHJcbiAgICBmaWxlcyA9IEFycmF5LmlzQXJyYXkoZmlsZXMpID8gZmlsZXMgOiBbZmlsZXNdO1xyXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBmaWxlcy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgIC8vIGlmIGZpbGUgaXMgbm90IGFuIGltYWdlLCByZWplY3QuXHJcbiAgICAgICAgaWYgKCEvXFwuKGpwZ3xzdmd8anBlZ3xwbmd8Ym1wfGdpZikkL2kudGVzdChmaWxlc1tpXS5uYW1lKSkge1xyXG4gICAgICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKGZhbHNlKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGlzdC5wdXNoKGZpbGVzW2ldKTtcclxuICAgIH1cclxuICAgIHJldHVybiBQcm9taXNlLmFsbChsaXN0Lm1hcChmdW5jdGlvbiAoZmlsZSkgeyByZXR1cm4gdmFsaWRhdGVJbWFnZShmaWxlLCB3aWR0aCwgaGVpZ2h0KTsgfSkpLnRoZW4oZnVuY3Rpb24gKHZhbHVlcykge1xyXG4gICAgICAgIHJldHVybiB2YWx1ZXMuZXZlcnkoZnVuY3Rpb24gKHYpIHsgcmV0dXJuIHY7IH0pO1xyXG4gICAgfSk7XHJcbn07XHJcbnZhciBwYXJhbXMkNyA9IFtcclxuICAgIHtcclxuICAgICAgICBuYW1lOiAnd2lkdGgnLFxyXG4gICAgICAgIGNhc3Q6IGZ1bmN0aW9uICh2YWx1ZSkge1xyXG4gICAgICAgICAgICByZXR1cm4gTnVtYmVyKHZhbHVlKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICAgIG5hbWU6ICdoZWlnaHQnLFxyXG4gICAgICAgIGNhc3Q6IGZ1bmN0aW9uICh2YWx1ZSkge1xyXG4gICAgICAgICAgICByZXR1cm4gTnVtYmVyKHZhbHVlKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbl07XHJcbnZhciBkaW1lbnNpb25zID0ge1xyXG4gICAgdmFsaWRhdGU6IHZhbGlkYXRlJDcsXHJcbiAgICBwYXJhbXM6IHBhcmFtcyQ3XHJcbn07XG5cbnZhciB2YWxpZGF0ZSQ4ID0gZnVuY3Rpb24gKHZhbHVlLCBfYSkge1xyXG4gICAgdmFyIG11bHRpcGxlID0gKF9hID09PSB2b2lkIDAgPyB7fSA6IF9hKS5tdWx0aXBsZTtcclxuICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZVxyXG4gICAgdmFyIHJlID0gL14oKFtePD4oKVxcW1xcXVxcXFwuLDs6XFxzQFwiXSsoXFwuW148PigpXFxbXFxdXFxcXC4sOzpcXHNAXCJdKykqKXwoXCIuK1wiKSlAKChcXFtbMC05XXsxLDN9XFwuWzAtOV17MSwzfVxcLlswLTldezEsM31cXC5bMC05XXsxLDN9XFxdKXwoKFthLXpBLVpcXC0wLTldK1xcLikrW2EtekEtWl17Mix9KSkkLztcclxuICAgIGlmIChtdWx0aXBsZSAmJiAhQXJyYXkuaXNBcnJheSh2YWx1ZSkpIHtcclxuICAgICAgICB2YWx1ZSA9IFN0cmluZyh2YWx1ZSlcclxuICAgICAgICAgICAgLnNwbGl0KCcsJylcclxuICAgICAgICAgICAgLm1hcChmdW5jdGlvbiAoZW1haWxTdHIpIHsgcmV0dXJuIGVtYWlsU3RyLnRyaW0oKTsgfSk7XHJcbiAgICB9XHJcbiAgICBpZiAoQXJyYXkuaXNBcnJheSh2YWx1ZSkpIHtcclxuICAgICAgICByZXR1cm4gdmFsdWUuZXZlcnkoZnVuY3Rpb24gKHZhbCkgeyByZXR1cm4gcmUudGVzdChTdHJpbmcodmFsKSk7IH0pO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHJlLnRlc3QoU3RyaW5nKHZhbHVlKSk7XHJcbn07XHJcbnZhciBwYXJhbXMkOCA9IFtcclxuICAgIHtcclxuICAgICAgICBuYW1lOiAnbXVsdGlwbGUnLFxyXG4gICAgICAgIGRlZmF1bHQ6IGZhbHNlXHJcbiAgICB9XHJcbl07XHJcbnZhciBlbWFpbCA9IHtcclxuICAgIHZhbGlkYXRlOiB2YWxpZGF0ZSQ4LFxyXG4gICAgcGFyYW1zOiBwYXJhbXMkOFxyXG59O1xuXG4vKipcclxuICogQ2hlY2tzIGlmIHRoZSB2YWx1ZXMgYXJlIGVpdGhlciBudWxsIG9yIHVuZGVmaW5lZC5cclxuICovXHJcbnZhciBpc051bGxPclVuZGVmaW5lZCA9IGZ1bmN0aW9uICh2YWx1ZSkge1xyXG4gICAgcmV0dXJuIHZhbHVlID09PSBudWxsIHx8IHZhbHVlID09PSB1bmRlZmluZWQ7XHJcbn07XHJcbnZhciBpbmNsdWRlcyA9IGZ1bmN0aW9uIChjb2xsZWN0aW9uLCBpdGVtKSB7XHJcbiAgICByZXR1cm4gY29sbGVjdGlvbi5pbmRleE9mKGl0ZW0pICE9PSAtMTtcclxufTtcclxuLyoqXHJcbiAqIENoZWNrcyBpZiBhIGZ1bmN0aW9uIGlzIGNhbGxhYmxlLlxyXG4gKi9cclxudmFyIGlzQ2FsbGFibGUgPSBmdW5jdGlvbiAoZnVuYykgeyByZXR1cm4gdHlwZW9mIGZ1bmMgPT09ICdmdW5jdGlvbic7IH07XHJcbi8qIGlzdGFuYnVsIGlnbm9yZSBuZXh0ICovXHJcbmZ1bmN0aW9uIF9jb3B5QXJyYXkoYXJyYXlMaWtlKSB7XHJcbiAgICB2YXIgYXJyYXkgPSBbXTtcclxuICAgIHZhciBsZW5ndGggPSBhcnJheUxpa2UubGVuZ3RoO1xyXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBsZW5ndGg7IGkrKykge1xyXG4gICAgICAgIGFycmF5LnB1c2goYXJyYXlMaWtlW2ldKTtcclxuICAgIH1cclxuICAgIHJldHVybiBhcnJheTtcclxufVxyXG4vKipcclxuICogQ29udmVydHMgYW4gYXJyYXktbGlrZSBvYmplY3QgdG8gYXJyYXksIHByb3ZpZGVzIGEgc2ltcGxlIHBvbHlmaWxsIGZvciBBcnJheS5mcm9tXHJcbiAqL1xyXG5mdW5jdGlvbiB0b0FycmF5KGFycmF5TGlrZSkge1xyXG4gICAgaWYgKGlzQ2FsbGFibGUoQXJyYXkuZnJvbSkpIHtcclxuICAgICAgICByZXR1cm4gQXJyYXkuZnJvbShhcnJheUxpa2UpO1xyXG4gICAgfVxyXG4gICAgLyogaXN0YW5idWwgaWdub3JlIG5leHQgKi9cclxuICAgIHJldHVybiBfY29weUFycmF5KGFycmF5TGlrZSk7XHJcbn1cclxudmFyIGlzRW1wdHlBcnJheSA9IGZ1bmN0aW9uIChhcnIpIHtcclxuICAgIHJldHVybiBBcnJheS5pc0FycmF5KGFycikgJiYgYXJyLmxlbmd0aCA9PT0gMDtcclxufTtcblxudmFyIHZhbGlkYXRlJDkgPSBmdW5jdGlvbiAodmFsdWUsIG9wdGlvbnMpIHtcclxuICAgIGlmIChBcnJheS5pc0FycmF5KHZhbHVlKSkge1xyXG4gICAgICAgIHJldHVybiB2YWx1ZS5ldmVyeShmdW5jdGlvbiAodmFsKSB7IHJldHVybiB2YWxpZGF0ZSQ5KHZhbCwgb3B0aW9ucyk7IH0pO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHRvQXJyYXkob3B0aW9ucykuc29tZShmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZVxyXG4gICAgICAgIHJldHVybiBpdGVtID09IHZhbHVlO1xyXG4gICAgfSk7XHJcbn07XHJcbnZhciBvbmVPZiA9IHtcclxuICAgIHZhbGlkYXRlOiB2YWxpZGF0ZSQ5XHJcbn07XG5cbnZhciB2YWxpZGF0ZSRhID0gZnVuY3Rpb24gKHZhbHVlLCBhcmdzKSB7XHJcbiAgICByZXR1cm4gIXZhbGlkYXRlJDkodmFsdWUsIGFyZ3MpO1xyXG59O1xyXG52YXIgZXhjbHVkZWQgPSB7XHJcbiAgICB2YWxpZGF0ZTogdmFsaWRhdGUkYVxyXG59O1xuXG52YXIgdmFsaWRhdGUkYiA9IGZ1bmN0aW9uIChmaWxlcywgZXh0ZW5zaW9ucykge1xyXG4gICAgdmFyIHJlZ2V4ID0gbmV3IFJlZ0V4cChcIi4oXCIgKyBleHRlbnNpb25zLmpvaW4oJ3wnKSArIFwiKSRcIiwgJ2knKTtcclxuICAgIGlmIChBcnJheS5pc0FycmF5KGZpbGVzKSkge1xyXG4gICAgICAgIHJldHVybiBmaWxlcy5ldmVyeShmdW5jdGlvbiAoZmlsZSkgeyByZXR1cm4gcmVnZXgudGVzdChmaWxlLm5hbWUpOyB9KTtcclxuICAgIH1cclxuICAgIHJldHVybiByZWdleC50ZXN0KGZpbGVzLm5hbWUpO1xyXG59O1xyXG52YXIgZXh0ID0ge1xyXG4gICAgdmFsaWRhdGU6IHZhbGlkYXRlJGJcclxufTtcblxudmFyIHZhbGlkYXRlJGMgPSBmdW5jdGlvbiAoZmlsZXMpIHtcclxuICAgIHZhciByZWdleCA9IC9cXC4oanBnfHN2Z3xqcGVnfHBuZ3xibXB8Z2lmKSQvaTtcclxuICAgIGlmIChBcnJheS5pc0FycmF5KGZpbGVzKSkge1xyXG4gICAgICAgIHJldHVybiBmaWxlcy5ldmVyeShmdW5jdGlvbiAoZmlsZSkgeyByZXR1cm4gcmVnZXgudGVzdChmaWxlLm5hbWUpOyB9KTtcclxuICAgIH1cclxuICAgIHJldHVybiByZWdleC50ZXN0KGZpbGVzLm5hbWUpO1xyXG59O1xyXG52YXIgaW1hZ2UgPSB7XHJcbiAgICB2YWxpZGF0ZTogdmFsaWRhdGUkY1xyXG59O1xuXG52YXIgdmFsaWRhdGUkZCA9IGZ1bmN0aW9uICh2YWx1ZSkge1xyXG4gICAgaWYgKEFycmF5LmlzQXJyYXkodmFsdWUpKSB7XHJcbiAgICAgICAgcmV0dXJuIHZhbHVlLmV2ZXJ5KGZ1bmN0aW9uICh2YWwpIHsgcmV0dXJuIC9eLT9bMC05XSskLy50ZXN0KFN0cmluZyh2YWwpKTsgfSk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gL14tP1swLTldKyQvLnRlc3QoU3RyaW5nKHZhbHVlKSk7XHJcbn07XHJcbnZhciBpbnRlZ2VyID0ge1xyXG4gICAgdmFsaWRhdGU6IHZhbGlkYXRlJGRcclxufTtcblxudmFyIHZhbGlkYXRlJGUgPSBmdW5jdGlvbiAodmFsdWUsIF9hKSB7XHJcbiAgICB2YXIgb3RoZXIgPSBfYS5vdGhlcjtcclxuICAgIHJldHVybiB2YWx1ZSA9PT0gb3RoZXI7XHJcbn07XHJcbnZhciBwYXJhbXMkOSA9IFtcclxuICAgIHtcclxuICAgICAgICBuYW1lOiAnb3RoZXInXHJcbiAgICB9XHJcbl07XHJcbnZhciBpcyA9IHtcclxuICAgIHZhbGlkYXRlOiB2YWxpZGF0ZSRlLFxyXG4gICAgcGFyYW1zOiBwYXJhbXMkOVxyXG59O1xuXG52YXIgdmFsaWRhdGUkZiA9IGZ1bmN0aW9uICh2YWx1ZSwgX2EpIHtcclxuICAgIHZhciBvdGhlciA9IF9hLm90aGVyO1xyXG4gICAgcmV0dXJuIHZhbHVlICE9PSBvdGhlcjtcclxufTtcclxudmFyIHBhcmFtcyRhID0gW1xyXG4gICAge1xyXG4gICAgICAgIG5hbWU6ICdvdGhlcidcclxuICAgIH1cclxuXTtcclxudmFyIGlzX25vdCA9IHtcclxuICAgIHZhbGlkYXRlOiB2YWxpZGF0ZSRmLFxyXG4gICAgcGFyYW1zOiBwYXJhbXMkYVxyXG59O1xuXG52YXIgdmFsaWRhdGUkZyA9IGZ1bmN0aW9uICh2YWx1ZSwgX2EpIHtcclxuICAgIHZhciBsZW5ndGggPSBfYS5sZW5ndGg7XHJcbiAgICBpZiAoaXNOdWxsT3JVbmRlZmluZWQodmFsdWUpKSB7XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG4gICAgaWYgKHR5cGVvZiB2YWx1ZSA9PT0gJ251bWJlcicpIHtcclxuICAgICAgICB2YWx1ZSA9IFN0cmluZyh2YWx1ZSk7XHJcbiAgICB9XHJcbiAgICBpZiAoIXZhbHVlLmxlbmd0aCkge1xyXG4gICAgICAgIHZhbHVlID0gdG9BcnJheSh2YWx1ZSk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gdmFsdWUubGVuZ3RoID09PSBsZW5ndGg7XHJcbn07XHJcbnZhciBwYXJhbXMkYiA9IFtcclxuICAgIHtcclxuICAgICAgICBuYW1lOiAnbGVuZ3RoJyxcclxuICAgICAgICBjYXN0OiBmdW5jdGlvbiAodmFsdWUpIHsgcmV0dXJuIE51bWJlcih2YWx1ZSk7IH1cclxuICAgIH1cclxuXTtcclxudmFyIGxlbmd0aCA9IHtcclxuICAgIHZhbGlkYXRlOiB2YWxpZGF0ZSRnLFxyXG4gICAgcGFyYW1zOiBwYXJhbXMkYlxyXG59O1xuXG52YXIgdmFsaWRhdGUkaCA9IGZ1bmN0aW9uICh2YWx1ZSwgX2EpIHtcclxuICAgIHZhciBsZW5ndGggPSBfYS5sZW5ndGg7XHJcbiAgICBpZiAoaXNOdWxsT3JVbmRlZmluZWQodmFsdWUpKSB7XHJcbiAgICAgICAgcmV0dXJuIGxlbmd0aCA+PSAwO1xyXG4gICAgfVxyXG4gICAgaWYgKEFycmF5LmlzQXJyYXkodmFsdWUpKSB7XHJcbiAgICAgICAgcmV0dXJuIHZhbHVlLmV2ZXJ5KGZ1bmN0aW9uICh2YWwpIHsgcmV0dXJuIHZhbGlkYXRlJGgodmFsLCB7IGxlbmd0aDogbGVuZ3RoIH0pOyB9KTtcclxuICAgIH1cclxuICAgIHJldHVybiBTdHJpbmcodmFsdWUpLmxlbmd0aCA8PSBsZW5ndGg7XHJcbn07XHJcbnZhciBwYXJhbXMkYyA9IFtcclxuICAgIHtcclxuICAgICAgICBuYW1lOiAnbGVuZ3RoJyxcclxuICAgICAgICBjYXN0OiBmdW5jdGlvbiAodmFsdWUpIHtcclxuICAgICAgICAgICAgcmV0dXJuIE51bWJlcih2YWx1ZSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5dO1xyXG52YXIgbWF4ID0ge1xyXG4gICAgdmFsaWRhdGU6IHZhbGlkYXRlJGgsXHJcbiAgICBwYXJhbXM6IHBhcmFtcyRjXHJcbn07XG5cbnZhciB2YWxpZGF0ZSRpID0gZnVuY3Rpb24gKHZhbHVlLCBfYSkge1xyXG4gICAgdmFyIG1heCA9IF9hLm1heDtcclxuICAgIGlmIChpc051bGxPclVuZGVmaW5lZCh2YWx1ZSkgfHwgdmFsdWUgPT09ICcnKSB7XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG4gICAgaWYgKEFycmF5LmlzQXJyYXkodmFsdWUpKSB7XHJcbiAgICAgICAgcmV0dXJuIHZhbHVlLmxlbmd0aCA+IDAgJiYgdmFsdWUuZXZlcnkoZnVuY3Rpb24gKHZhbCkgeyByZXR1cm4gdmFsaWRhdGUkaSh2YWwsIHsgbWF4OiBtYXggfSk7IH0pO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIE51bWJlcih2YWx1ZSkgPD0gbWF4O1xyXG59O1xyXG52YXIgcGFyYW1zJGQgPSBbXHJcbiAgICB7XHJcbiAgICAgICAgbmFtZTogJ21heCcsXHJcbiAgICAgICAgY2FzdDogZnVuY3Rpb24gKHZhbHVlKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBOdW1iZXIodmFsdWUpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXTtcclxudmFyIG1heF92YWx1ZSA9IHtcclxuICAgIHZhbGlkYXRlOiB2YWxpZGF0ZSRpLFxyXG4gICAgcGFyYW1zOiBwYXJhbXMkZFxyXG59O1xuXG52YXIgdmFsaWRhdGUkaiA9IGZ1bmN0aW9uIChmaWxlcywgbWltZXMpIHtcclxuICAgIHZhciByZWdleCA9IG5ldyBSZWdFeHAobWltZXMuam9pbignfCcpLnJlcGxhY2UoJyonLCAnLisnKSArIFwiJFwiLCAnaScpO1xyXG4gICAgaWYgKEFycmF5LmlzQXJyYXkoZmlsZXMpKSB7XHJcbiAgICAgICAgcmV0dXJuIGZpbGVzLmV2ZXJ5KGZ1bmN0aW9uIChmaWxlKSB7IHJldHVybiByZWdleC50ZXN0KGZpbGUudHlwZSk7IH0pO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHJlZ2V4LnRlc3QoZmlsZXMudHlwZSk7XHJcbn07XHJcbnZhciBtaW1lcyA9IHtcclxuICAgIHZhbGlkYXRlOiB2YWxpZGF0ZSRqXHJcbn07XG5cbnZhciB2YWxpZGF0ZSRrID0gZnVuY3Rpb24gKHZhbHVlLCBfYSkge1xyXG4gICAgdmFyIGxlbmd0aCA9IF9hLmxlbmd0aDtcclxuICAgIGlmIChpc051bGxPclVuZGVmaW5lZCh2YWx1ZSkpIHtcclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcbiAgICBpZiAoQXJyYXkuaXNBcnJheSh2YWx1ZSkpIHtcclxuICAgICAgICByZXR1cm4gdmFsdWUuZXZlcnkoZnVuY3Rpb24gKHZhbCkgeyByZXR1cm4gdmFsaWRhdGUkayh2YWwsIHsgbGVuZ3RoOiBsZW5ndGggfSk7IH0pO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIFN0cmluZyh2YWx1ZSkubGVuZ3RoID49IGxlbmd0aDtcclxufTtcclxudmFyIHBhcmFtcyRlID0gW1xyXG4gICAge1xyXG4gICAgICAgIG5hbWU6ICdsZW5ndGgnLFxyXG4gICAgICAgIGNhc3Q6IGZ1bmN0aW9uICh2YWx1ZSkge1xyXG4gICAgICAgICAgICByZXR1cm4gTnVtYmVyKHZhbHVlKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbl07XHJcbnZhciBtaW4gPSB7XHJcbiAgICB2YWxpZGF0ZTogdmFsaWRhdGUkayxcclxuICAgIHBhcmFtczogcGFyYW1zJGVcclxufTtcblxudmFyIHZhbGlkYXRlJGwgPSBmdW5jdGlvbiAodmFsdWUsIF9hKSB7XHJcbiAgICB2YXIgbWluID0gX2EubWluO1xyXG4gICAgaWYgKGlzTnVsbE9yVW5kZWZpbmVkKHZhbHVlKSB8fCB2YWx1ZSA9PT0gJycpIHtcclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcbiAgICBpZiAoQXJyYXkuaXNBcnJheSh2YWx1ZSkpIHtcclxuICAgICAgICByZXR1cm4gdmFsdWUubGVuZ3RoID4gMCAmJiB2YWx1ZS5ldmVyeShmdW5jdGlvbiAodmFsKSB7IHJldHVybiB2YWxpZGF0ZSRsKHZhbCwgeyBtaW46IG1pbiB9KTsgfSk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gTnVtYmVyKHZhbHVlKSA+PSBtaW47XHJcbn07XHJcbnZhciBwYXJhbXMkZiA9IFtcclxuICAgIHtcclxuICAgICAgICBuYW1lOiAnbWluJyxcclxuICAgICAgICBjYXN0OiBmdW5jdGlvbiAodmFsdWUpIHtcclxuICAgICAgICAgICAgcmV0dXJuIE51bWJlcih2YWx1ZSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5dO1xyXG52YXIgbWluX3ZhbHVlID0ge1xyXG4gICAgdmFsaWRhdGU6IHZhbGlkYXRlJGwsXHJcbiAgICBwYXJhbXM6IHBhcmFtcyRmXHJcbn07XG5cbnZhciBhciA9IC9eW9mg2aHZotmj2aTZpdmm2afZqNmpXSskLztcclxudmFyIGVuID0gL15bMC05XSskLztcclxudmFyIHZhbGlkYXRlJG0gPSBmdW5jdGlvbiAodmFsdWUpIHtcclxuICAgIHZhciB0ZXN0VmFsdWUgPSBmdW5jdGlvbiAodmFsKSB7XHJcbiAgICAgICAgdmFyIHN0clZhbHVlID0gU3RyaW5nKHZhbCk7XHJcbiAgICAgICAgcmV0dXJuIGVuLnRlc3Qoc3RyVmFsdWUpIHx8IGFyLnRlc3Qoc3RyVmFsdWUpO1xyXG4gICAgfTtcclxuICAgIGlmIChBcnJheS5pc0FycmF5KHZhbHVlKSkge1xyXG4gICAgICAgIHJldHVybiB2YWx1ZS5ldmVyeSh0ZXN0VmFsdWUpO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHRlc3RWYWx1ZSh2YWx1ZSk7XHJcbn07XHJcbnZhciBudW1lcmljID0ge1xyXG4gICAgdmFsaWRhdGU6IHZhbGlkYXRlJG1cclxufTtcblxudmFyIHZhbGlkYXRlJG4gPSBmdW5jdGlvbiAodmFsdWUsIF9hKSB7XHJcbiAgICB2YXIgcmVnZXggPSBfYS5yZWdleDtcclxuICAgIGlmIChBcnJheS5pc0FycmF5KHZhbHVlKSkge1xyXG4gICAgICAgIHJldHVybiB2YWx1ZS5ldmVyeShmdW5jdGlvbiAodmFsKSB7IHJldHVybiB2YWxpZGF0ZSRuKHZhbCwgeyByZWdleDogcmVnZXggfSk7IH0pO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHJlZ2V4LnRlc3QoU3RyaW5nKHZhbHVlKSk7XHJcbn07XHJcbnZhciBwYXJhbXMkZyA9IFtcclxuICAgIHtcclxuICAgICAgICBuYW1lOiAncmVnZXgnLFxyXG4gICAgICAgIGNhc3Q6IGZ1bmN0aW9uICh2YWx1ZSkge1xyXG4gICAgICAgICAgICBpZiAodHlwZW9mIHZhbHVlID09PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBSZWdFeHAodmFsdWUpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybiB2YWx1ZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbl07XHJcbnZhciByZWdleCA9IHtcclxuICAgIHZhbGlkYXRlOiB2YWxpZGF0ZSRuLFxyXG4gICAgcGFyYW1zOiBwYXJhbXMkZ1xyXG59O1xuXG52YXIgdmFsaWRhdGUkbyA9IGZ1bmN0aW9uICh2YWx1ZSwgX2EpIHtcclxuICAgIHZhciBhbGxvd0ZhbHNlID0gKF9hID09PSB2b2lkIDAgPyB7IGFsbG93RmFsc2U6IHRydWUgfSA6IF9hKS5hbGxvd0ZhbHNlO1xyXG4gICAgdmFyIHJlc3VsdCA9IHtcclxuICAgICAgICB2YWxpZDogZmFsc2UsXHJcbiAgICAgICAgcmVxdWlyZWQ6IHRydWVcclxuICAgIH07XHJcbiAgICBpZiAoaXNOdWxsT3JVbmRlZmluZWQodmFsdWUpIHx8IGlzRW1wdHlBcnJheSh2YWx1ZSkpIHtcclxuICAgICAgICByZXR1cm4gcmVzdWx0O1xyXG4gICAgfVxyXG4gICAgLy8gaW5jYXNlIGEgZmllbGQgY29uc2lkZXJzIGBmYWxzZWAgYXMgYW4gZW1wdHkgdmFsdWUgbGlrZSBjaGVja2JveGVzLlxyXG4gICAgaWYgKHZhbHVlID09PSBmYWxzZSAmJiAhYWxsb3dGYWxzZSkge1xyXG4gICAgICAgIHJldHVybiByZXN1bHQ7XHJcbiAgICB9XHJcbiAgICByZXN1bHQudmFsaWQgPSAhIVN0cmluZyh2YWx1ZSkudHJpbSgpLmxlbmd0aDtcclxuICAgIHJldHVybiByZXN1bHQ7XHJcbn07XHJcbnZhciBjb21wdXRlc1JlcXVpcmVkID0gdHJ1ZTtcclxudmFyIHBhcmFtcyRoID0gW1xyXG4gICAge1xyXG4gICAgICAgIG5hbWU6ICdhbGxvd0ZhbHNlJyxcclxuICAgICAgICBkZWZhdWx0OiB0cnVlXHJcbiAgICB9XHJcbl07XHJcbnZhciByZXF1aXJlZCA9IHtcclxuICAgIHZhbGlkYXRlOiB2YWxpZGF0ZSRvLFxyXG4gICAgcGFyYW1zOiBwYXJhbXMkaCxcclxuICAgIGNvbXB1dGVzUmVxdWlyZWQ6IGNvbXB1dGVzUmVxdWlyZWRcclxufTtcblxudmFyIHRlc3RFbXB0eSA9IGZ1bmN0aW9uICh2YWx1ZSkge1xyXG4gICAgcmV0dXJuIGlzRW1wdHlBcnJheSh2YWx1ZSkgfHwgaW5jbHVkZXMoW2ZhbHNlLCBudWxsLCB1bmRlZmluZWRdLCB2YWx1ZSkgfHwgIVN0cmluZyh2YWx1ZSkudHJpbSgpLmxlbmd0aDtcclxufTtcclxudmFyIHZhbGlkYXRlJHAgPSBmdW5jdGlvbiAodmFsdWUsIF9hKSB7XHJcbiAgICB2YXIgdGFyZ2V0ID0gX2EudGFyZ2V0LCB2YWx1ZXMgPSBfYS52YWx1ZXM7XHJcbiAgICB2YXIgcmVxdWlyZWQ7XHJcbiAgICBpZiAodmFsdWVzICYmIHZhbHVlcy5sZW5ndGgpIHtcclxuICAgICAgICBpZiAoIUFycmF5LmlzQXJyYXkodmFsdWVzKSAmJiB0eXBlb2YgdmFsdWVzID09PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgICB2YWx1ZXMgPSBbdmFsdWVzXTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lXHJcbiAgICAgICAgcmVxdWlyZWQgPSB2YWx1ZXMuc29tZShmdW5jdGlvbiAodmFsKSB7IHJldHVybiB2YWwgPT0gU3RyaW5nKHRhcmdldCkudHJpbSgpOyB9KTtcclxuICAgIH1cclxuICAgIGVsc2Uge1xyXG4gICAgICAgIHJlcXVpcmVkID0gIXRlc3RFbXB0eSh0YXJnZXQpO1xyXG4gICAgfVxyXG4gICAgaWYgKCFyZXF1aXJlZCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIHZhbGlkOiB0cnVlLFxyXG4gICAgICAgICAgICByZXF1aXJlZDogcmVxdWlyZWRcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHtcclxuICAgICAgICB2YWxpZDogIXRlc3RFbXB0eSh2YWx1ZSksXHJcbiAgICAgICAgcmVxdWlyZWQ6IHJlcXVpcmVkXHJcbiAgICB9O1xyXG59O1xyXG52YXIgcGFyYW1zJGkgPSBbXHJcbiAgICB7XHJcbiAgICAgICAgbmFtZTogJ3RhcmdldCcsXHJcbiAgICAgICAgaXNUYXJnZXQ6IHRydWVcclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgICAgbmFtZTogJ3ZhbHVlcydcclxuICAgIH1cclxuXTtcclxudmFyIGNvbXB1dGVzUmVxdWlyZWQkMSA9IHRydWU7XHJcbnZhciByZXF1aXJlZF9pZiA9IHtcclxuICAgIHZhbGlkYXRlOiB2YWxpZGF0ZSRwLFxyXG4gICAgcGFyYW1zOiBwYXJhbXMkaSxcclxuICAgIGNvbXB1dGVzUmVxdWlyZWQ6IGNvbXB1dGVzUmVxdWlyZWQkMVxyXG59O1xuXG52YXIgdmFsaWRhdGUkcSA9IGZ1bmN0aW9uIChmaWxlcywgX2EpIHtcclxuICAgIHZhciBzaXplID0gX2Euc2l6ZTtcclxuICAgIGlmIChpc05hTihzaXplKSkge1xyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuICAgIHZhciBuU2l6ZSA9IHNpemUgKiAxMDI0O1xyXG4gICAgaWYgKCFBcnJheS5pc0FycmF5KGZpbGVzKSkge1xyXG4gICAgICAgIHJldHVybiBmaWxlcy5zaXplIDw9IG5TaXplO1xyXG4gICAgfVxyXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBmaWxlcy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgIGlmIChmaWxlc1tpXS5zaXplID4gblNpemUpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIHJldHVybiB0cnVlO1xyXG59O1xyXG52YXIgcGFyYW1zJGogPSBbXHJcbiAgICB7XHJcbiAgICAgICAgbmFtZTogJ3NpemUnLFxyXG4gICAgICAgIGNhc3Q6IGZ1bmN0aW9uICh2YWx1ZSkge1xyXG4gICAgICAgICAgICByZXR1cm4gTnVtYmVyKHZhbHVlKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbl07XHJcbnZhciBzaXplID0ge1xyXG4gICAgdmFsaWRhdGU6IHZhbGlkYXRlJHEsXHJcbiAgICBwYXJhbXM6IHBhcmFtcyRqXHJcbn07XG5cbmV4cG9ydCB7IGFscGhhJDEgYXMgYWxwaGEsIGFscGhhX2Rhc2gsIGFscGhhX251bSwgYWxwaGFfc3BhY2VzLCBiZXR3ZWVuLCBjb25maXJtZWQsIGRpZ2l0cywgZGltZW5zaW9ucywgZW1haWwsIGV4Y2x1ZGVkLCBleHQsIGltYWdlLCBpbnRlZ2VyLCBpcywgaXNfbm90LCBsZW5ndGgsIG1heCwgbWF4X3ZhbHVlLCBtaW1lcywgbWluLCBtaW5fdmFsdWUsIG51bWVyaWMsIG9uZU9mLCByZWdleCwgcmVxdWlyZWQsIHJlcXVpcmVkX2lmLCBzaXplIH07XG4iLCIvKipcbiAgKiB2ZWUtdmFsaWRhdGUgdjMuMC4xMVxuICAqIChjKSAyMDE5IEFiZGVscmFobWFuIEF3YWRcbiAgKiBAbGljZW5zZSBNSVRcbiAgKi9cbmltcG9ydCBWdWUgZnJvbSAndnVlJztcblxuLyohICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXHJcbkNvcHlyaWdodCAoYykgTWljcm9zb2Z0IENvcnBvcmF0aW9uLiBBbGwgcmlnaHRzIHJlc2VydmVkLlxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2VcclxudGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGVcclxuTGljZW5zZSBhdCBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblRISVMgQ09ERSBJUyBQUk9WSURFRCBPTiBBTiAqQVMgSVMqIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcclxuS0lORCwgRUlUSEVSIEVYUFJFU1MgT1IgSU1QTElFRCwgSU5DTFVESU5HIFdJVEhPVVQgTElNSVRBVElPTiBBTlkgSU1QTElFRFxyXG5XQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgVElUTEUsIEZJVE5FU1MgRk9SIEEgUEFSVElDVUxBUiBQVVJQT1NFLFxyXG5NRVJDSEFOVEFCTElUWSBPUiBOT04tSU5GUklOR0VNRU5ULlxyXG5cclxuU2VlIHRoZSBBcGFjaGUgVmVyc2lvbiAyLjAgTGljZW5zZSBmb3Igc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zXHJcbmFuZCBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiogKi9cclxuXHJcbnZhciBfX2Fzc2lnbiA9IGZ1bmN0aW9uKCkge1xyXG4gICAgX19hc3NpZ24gPSBPYmplY3QuYXNzaWduIHx8IGZ1bmN0aW9uIF9fYXNzaWduKHQpIHtcclxuICAgICAgICBmb3IgKHZhciBzLCBpID0gMSwgbiA9IGFyZ3VtZW50cy5sZW5ndGg7IGkgPCBuOyBpKyspIHtcclxuICAgICAgICAgICAgcyA9IGFyZ3VtZW50c1tpXTtcclxuICAgICAgICAgICAgZm9yICh2YXIgcCBpbiBzKSBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHMsIHApKSB0W3BdID0gc1twXTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHQ7XHJcbiAgICB9O1xyXG4gICAgcmV0dXJuIF9fYXNzaWduLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7XHJcbn07XHJcblxyXG5mdW5jdGlvbiBfX2F3YWl0ZXIodGhpc0FyZywgX2FyZ3VtZW50cywgUCwgZ2VuZXJhdG9yKSB7XHJcbiAgICByZXR1cm4gbmV3IChQIHx8IChQID0gUHJvbWlzZSkpKGZ1bmN0aW9uIChyZXNvbHZlLCByZWplY3QpIHtcclxuICAgICAgICBmdW5jdGlvbiBmdWxmaWxsZWQodmFsdWUpIHsgdHJ5IHsgc3RlcChnZW5lcmF0b3IubmV4dCh2YWx1ZSkpOyB9IGNhdGNoIChlKSB7IHJlamVjdChlKTsgfSB9XHJcbiAgICAgICAgZnVuY3Rpb24gcmVqZWN0ZWQodmFsdWUpIHsgdHJ5IHsgc3RlcChnZW5lcmF0b3JbXCJ0aHJvd1wiXSh2YWx1ZSkpOyB9IGNhdGNoIChlKSB7IHJlamVjdChlKTsgfSB9XHJcbiAgICAgICAgZnVuY3Rpb24gc3RlcChyZXN1bHQpIHsgcmVzdWx0LmRvbmUgPyByZXNvbHZlKHJlc3VsdC52YWx1ZSkgOiBuZXcgUChmdW5jdGlvbiAocmVzb2x2ZSkgeyByZXNvbHZlKHJlc3VsdC52YWx1ZSk7IH0pLnRoZW4oZnVsZmlsbGVkLCByZWplY3RlZCk7IH1cclxuICAgICAgICBzdGVwKChnZW5lcmF0b3IgPSBnZW5lcmF0b3IuYXBwbHkodGhpc0FyZywgX2FyZ3VtZW50cyB8fCBbXSkpLm5leHQoKSk7XHJcbiAgICB9KTtcclxufVxyXG5cclxuZnVuY3Rpb24gX19nZW5lcmF0b3IodGhpc0FyZywgYm9keSkge1xyXG4gICAgdmFyIF8gPSB7IGxhYmVsOiAwLCBzZW50OiBmdW5jdGlvbigpIHsgaWYgKHRbMF0gJiAxKSB0aHJvdyB0WzFdOyByZXR1cm4gdFsxXTsgfSwgdHJ5czogW10sIG9wczogW10gfSwgZiwgeSwgdCwgZztcclxuICAgIHJldHVybiBnID0geyBuZXh0OiB2ZXJiKDApLCBcInRocm93XCI6IHZlcmIoMSksIFwicmV0dXJuXCI6IHZlcmIoMikgfSwgdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIChnW1N5bWJvbC5pdGVyYXRvcl0gPSBmdW5jdGlvbigpIHsgcmV0dXJuIHRoaXM7IH0pLCBnO1xyXG4gICAgZnVuY3Rpb24gdmVyYihuKSB7IHJldHVybiBmdW5jdGlvbiAodikgeyByZXR1cm4gc3RlcChbbiwgdl0pOyB9OyB9XHJcbiAgICBmdW5jdGlvbiBzdGVwKG9wKSB7XHJcbiAgICAgICAgaWYgKGYpIHRocm93IG5ldyBUeXBlRXJyb3IoXCJHZW5lcmF0b3IgaXMgYWxyZWFkeSBleGVjdXRpbmcuXCIpO1xyXG4gICAgICAgIHdoaWxlIChfKSB0cnkge1xyXG4gICAgICAgICAgICBpZiAoZiA9IDEsIHkgJiYgKHQgPSBvcFswXSAmIDIgPyB5W1wicmV0dXJuXCJdIDogb3BbMF0gPyB5W1widGhyb3dcIl0gfHwgKCh0ID0geVtcInJldHVyblwiXSkgJiYgdC5jYWxsKHkpLCAwKSA6IHkubmV4dCkgJiYgISh0ID0gdC5jYWxsKHksIG9wWzFdKSkuZG9uZSkgcmV0dXJuIHQ7XHJcbiAgICAgICAgICAgIGlmICh5ID0gMCwgdCkgb3AgPSBbb3BbMF0gJiAyLCB0LnZhbHVlXTtcclxuICAgICAgICAgICAgc3dpdGNoIChvcFswXSkge1xyXG4gICAgICAgICAgICAgICAgY2FzZSAwOiBjYXNlIDE6IHQgPSBvcDsgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICBjYXNlIDQ6IF8ubGFiZWwrKzsgcmV0dXJuIHsgdmFsdWU6IG9wWzFdLCBkb25lOiBmYWxzZSB9O1xyXG4gICAgICAgICAgICAgICAgY2FzZSA1OiBfLmxhYmVsKys7IHkgPSBvcFsxXTsgb3AgPSBbMF07IGNvbnRpbnVlO1xyXG4gICAgICAgICAgICAgICAgY2FzZSA3OiBvcCA9IF8ub3BzLnBvcCgpOyBfLnRyeXMucG9wKCk7IGNvbnRpbnVlO1xyXG4gICAgICAgICAgICAgICAgZGVmYXVsdDpcclxuICAgICAgICAgICAgICAgICAgICBpZiAoISh0ID0gXy50cnlzLCB0ID0gdC5sZW5ndGggPiAwICYmIHRbdC5sZW5ndGggLSAxXSkgJiYgKG9wWzBdID09PSA2IHx8IG9wWzBdID09PSAyKSkgeyBfID0gMDsgY29udGludWU7IH1cclxuICAgICAgICAgICAgICAgICAgICBpZiAob3BbMF0gPT09IDMgJiYgKCF0IHx8IChvcFsxXSA+IHRbMF0gJiYgb3BbMV0gPCB0WzNdKSkpIHsgXy5sYWJlbCA9IG9wWzFdOyBicmVhazsgfVxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChvcFswXSA9PT0gNiAmJiBfLmxhYmVsIDwgdFsxXSkgeyBfLmxhYmVsID0gdFsxXTsgdCA9IG9wOyBicmVhazsgfVxyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0ICYmIF8ubGFiZWwgPCB0WzJdKSB7IF8ubGFiZWwgPSB0WzJdOyBfLm9wcy5wdXNoKG9wKTsgYnJlYWs7IH1cclxuICAgICAgICAgICAgICAgICAgICBpZiAodFsyXSkgXy5vcHMucG9wKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgXy50cnlzLnBvcCgpOyBjb250aW51ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBvcCA9IGJvZHkuY2FsbCh0aGlzQXJnLCBfKTtcclxuICAgICAgICB9IGNhdGNoIChlKSB7IG9wID0gWzYsIGVdOyB5ID0gMDsgfSBmaW5hbGx5IHsgZiA9IHQgPSAwOyB9XHJcbiAgICAgICAgaWYgKG9wWzBdICYgNSkgdGhyb3cgb3BbMV07IHJldHVybiB7IHZhbHVlOiBvcFswXSA/IG9wWzFdIDogdm9pZCAwLCBkb25lOiB0cnVlIH07XHJcbiAgICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uIF9fc3ByZWFkQXJyYXlzKCkge1xyXG4gICAgZm9yICh2YXIgcyA9IDAsIGkgPSAwLCBpbCA9IGFyZ3VtZW50cy5sZW5ndGg7IGkgPCBpbDsgaSsrKSBzICs9IGFyZ3VtZW50c1tpXS5sZW5ndGg7XHJcbiAgICBmb3IgKHZhciByID0gQXJyYXkocyksIGsgPSAwLCBpID0gMDsgaSA8IGlsOyBpKyspXHJcbiAgICAgICAgZm9yICh2YXIgYSA9IGFyZ3VtZW50c1tpXSwgaiA9IDAsIGpsID0gYS5sZW5ndGg7IGogPCBqbDsgaisrLCBrKyspXHJcbiAgICAgICAgICAgIHJba10gPSBhW2pdO1xyXG4gICAgcmV0dXJuIHI7XHJcbn1cblxudmFyIGlzTmFOID0gZnVuY3Rpb24gKHZhbHVlKSB7XHJcbiAgICAvLyBOYU4gaXMgdGhlIG9uZSB2YWx1ZSB0aGF0IGRvZXMgbm90IGVxdWFsIGl0c2VsZi5cclxuICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZVxyXG4gICAgcmV0dXJuIHZhbHVlICE9PSB2YWx1ZTtcclxufTtcclxuLyoqXHJcbiAqIENoZWNrcyBpZiB0aGUgdmFsdWVzIGFyZSBlaXRoZXIgbnVsbCBvciB1bmRlZmluZWQuXHJcbiAqL1xyXG52YXIgaXNOdWxsT3JVbmRlZmluZWQgPSBmdW5jdGlvbiAodmFsdWUpIHtcclxuICAgIHJldHVybiB2YWx1ZSA9PT0gbnVsbCB8fCB2YWx1ZSA9PT0gdW5kZWZpbmVkO1xyXG59O1xyXG4vKipcclxuICogQ3JlYXRlcyB0aGUgZGVmYXVsdCBmbGFncyBvYmplY3QuXHJcbiAqL1xyXG52YXIgY3JlYXRlRmxhZ3MgPSBmdW5jdGlvbiAoKSB7IHJldHVybiAoe1xyXG4gICAgdW50b3VjaGVkOiB0cnVlLFxyXG4gICAgdG91Y2hlZDogZmFsc2UsXHJcbiAgICBkaXJ0eTogZmFsc2UsXHJcbiAgICBwcmlzdGluZTogdHJ1ZSxcclxuICAgIHZhbGlkOiBmYWxzZSxcclxuICAgIGludmFsaWQ6IGZhbHNlLFxyXG4gICAgdmFsaWRhdGVkOiBmYWxzZSxcclxuICAgIHBlbmRpbmc6IGZhbHNlLFxyXG4gICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgY2hhbmdlZDogZmFsc2VcclxufSk7IH07XHJcbi8qKlxyXG4gKiBDaGVja3MgaWYgdGhlIHZhbHVlIGlzIGFuIG9iamVjdC5cclxuICovXHJcbnZhciBpc09iamVjdCA9IGZ1bmN0aW9uIChvYmopIHtcclxuICAgIHJldHVybiBvYmogIT09IG51bGwgJiYgb2JqICYmIHR5cGVvZiBvYmogPT09ICdvYmplY3QnICYmICFBcnJheS5pc0FycmF5KG9iaik7XHJcbn07XHJcbmZ1bmN0aW9uIGlkZW50aXR5KHgpIHtcclxuICAgIHJldHVybiB4O1xyXG59XHJcbi8qKlxyXG4gKiBTaGFsbG93IG9iamVjdCBjb21wYXJpc29uLlxyXG4gKi9cclxudmFyIGlzRXF1YWwgPSBmdW5jdGlvbiAobGhzLCByaHMpIHtcclxuICAgIGlmIChsaHMgaW5zdGFuY2VvZiBSZWdFeHAgJiYgcmhzIGluc3RhbmNlb2YgUmVnRXhwKSB7XHJcbiAgICAgICAgcmV0dXJuIGlzRXF1YWwobGhzLnNvdXJjZSwgcmhzLnNvdXJjZSkgJiYgaXNFcXVhbChsaHMuZmxhZ3MsIHJocy5mbGFncyk7XHJcbiAgICB9XHJcbiAgICBpZiAoQXJyYXkuaXNBcnJheShsaHMpICYmIEFycmF5LmlzQXJyYXkocmhzKSkge1xyXG4gICAgICAgIGlmIChsaHMubGVuZ3RoICE9PSByaHMubGVuZ3RoKVxyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBsaHMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgaWYgKCFpc0VxdWFsKGxoc1tpXSwgcmhzW2ldKSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG4gICAgLy8gaWYgYm90aCBhcmUgb2JqZWN0cywgY29tcGFyZSBlYWNoIGtleSByZWN1cnNpdmVseS5cclxuICAgIGlmIChpc09iamVjdChsaHMpICYmIGlzT2JqZWN0KHJocykpIHtcclxuICAgICAgICByZXR1cm4gKE9iamVjdC5rZXlzKGxocykuZXZlcnkoZnVuY3Rpb24gKGtleSkge1xyXG4gICAgICAgICAgICByZXR1cm4gaXNFcXVhbChsaHNba2V5XSwgcmhzW2tleV0pO1xyXG4gICAgICAgIH0pICYmXHJcbiAgICAgICAgICAgIE9iamVjdC5rZXlzKHJocykuZXZlcnkoZnVuY3Rpb24gKGtleSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGlzRXF1YWwobGhzW2tleV0sIHJoc1trZXldKTtcclxuICAgICAgICAgICAgfSkpO1xyXG4gICAgfVxyXG4gICAgaWYgKGlzTmFOKGxocykgJiYgaXNOYU4ocmhzKSkge1xyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIGxocyA9PT0gcmhzO1xyXG59O1xyXG52YXIgaW5jbHVkZXMgPSBmdW5jdGlvbiAoY29sbGVjdGlvbiwgaXRlbSkge1xyXG4gICAgcmV0dXJuIGNvbGxlY3Rpb24uaW5kZXhPZihpdGVtKSAhPT0gLTE7XHJcbn07XHJcbi8qKlxyXG4gKiBQYXJzZXMgYSBydWxlIHN0cmluZyBleHByZXNzaW9uLlxyXG4gKi9cclxudmFyIHBhcnNlUnVsZSA9IGZ1bmN0aW9uIChydWxlKSB7XHJcbiAgICB2YXIgcGFyYW1zID0gW107XHJcbiAgICB2YXIgbmFtZSA9IHJ1bGUuc3BsaXQoJzonKVswXTtcclxuICAgIGlmIChpbmNsdWRlcyhydWxlLCAnOicpKSB7XHJcbiAgICAgICAgcGFyYW1zID0gcnVsZVxyXG4gICAgICAgICAgICAuc3BsaXQoJzonKVxyXG4gICAgICAgICAgICAuc2xpY2UoMSlcclxuICAgICAgICAgICAgLmpvaW4oJzonKVxyXG4gICAgICAgICAgICAuc3BsaXQoJywnKTtcclxuICAgIH1cclxuICAgIHJldHVybiB7IG5hbWU6IG5hbWUsIHBhcmFtczogcGFyYW1zIH07XHJcbn07XHJcbi8qKlxyXG4gKiBEZWJvdW5jZXMgYSBmdW5jdGlvbi5cclxuICovXHJcbnZhciBkZWJvdW5jZSA9IGZ1bmN0aW9uIChmbiwgd2FpdCwgdG9rZW4pIHtcclxuICAgIGlmICh3YWl0ID09PSB2b2lkIDApIHsgd2FpdCA9IDA7IH1cclxuICAgIGlmICh0b2tlbiA9PT0gdm9pZCAwKSB7IHRva2VuID0geyBjYW5jZWxsZWQ6IGZhbHNlIH07IH1cclxuICAgIGlmICh3YWl0ID09PSAwKSB7XHJcbiAgICAgICAgcmV0dXJuIGZuO1xyXG4gICAgfVxyXG4gICAgdmFyIHRpbWVvdXQ7XHJcbiAgICByZXR1cm4gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHZhciBhcmdzID0gW107XHJcbiAgICAgICAgZm9yICh2YXIgX2kgPSAwOyBfaSA8IGFyZ3VtZW50cy5sZW5ndGg7IF9pKyspIHtcclxuICAgICAgICAgICAgYXJnc1tfaV0gPSBhcmd1bWVudHNbX2ldO1xyXG4gICAgICAgIH1cclxuICAgICAgICB2YXIgbGF0ZXIgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIHRpbWVvdXQgPSB1bmRlZmluZWQ7XHJcbiAgICAgICAgICAgIC8vIGNoZWNrIGlmIHRoZSBmbiBjYWxsIHdhcyBjYW5jZWxsZWQuXHJcbiAgICAgICAgICAgIGlmICghdG9rZW4uY2FuY2VsbGVkKVxyXG4gICAgICAgICAgICAgICAgZm4uYXBwbHkodm9pZCAwLCBhcmdzKTtcclxuICAgICAgICB9O1xyXG4gICAgICAgIC8vIGJlY2F1c2Ugd2UgbWlnaHQgd2FudCB0byB1c2UgTm9kZS5qcyBzZXRUaW1vdXQgZm9yIFNTUi5cclxuICAgICAgICBjbGVhclRpbWVvdXQodGltZW91dCk7XHJcbiAgICAgICAgdGltZW91dCA9IHNldFRpbWVvdXQobGF0ZXIsIHdhaXQpO1xyXG4gICAgfTtcclxufTtcclxuLyoqXHJcbiAqIEVtaXRzIGEgd2FybmluZyB0byB0aGUgY29uc29sZS5cclxuICovXHJcbnZhciB3YXJuID0gZnVuY3Rpb24gKG1lc3NhZ2UpIHtcclxuICAgIGNvbnNvbGUud2FybihcIlt2ZWUtdmFsaWRhdGVdIFwiICsgbWVzc2FnZSk7XHJcbn07XHJcbi8qKlxyXG4gKiBOb3JtYWxpemVzIHRoZSBnaXZlbiBydWxlcyBleHByZXNzaW9uLlxyXG4gKi9cclxudmFyIG5vcm1hbGl6ZVJ1bGVzID0gZnVuY3Rpb24gKHJ1bGVzKSB7XHJcbiAgICAvLyBpZiBmYWxzeSB2YWx1ZSByZXR1cm4gYW4gZW1wdHkgb2JqZWN0LlxyXG4gICAgdmFyIGFjYyA9IHt9O1xyXG4gICAgT2JqZWN0LmRlZmluZVByb3BlcnR5KGFjYywgJ18kJGlzTm9ybWFsaXplZCcsIHtcclxuICAgICAgICB2YWx1ZTogdHJ1ZSxcclxuICAgICAgICB3cml0YWJsZTogZmFsc2UsXHJcbiAgICAgICAgZW51bWVyYWJsZTogZmFsc2UsXHJcbiAgICAgICAgY29uZmlndXJhYmxlOiBmYWxzZVxyXG4gICAgfSk7XHJcbiAgICBpZiAoIXJ1bGVzKSB7XHJcbiAgICAgICAgcmV0dXJuIGFjYztcclxuICAgIH1cclxuICAgIC8vIE9iamVjdCBpcyBhbHJlYWR5IG5vcm1hbGl6ZWQsIHNraXAuXHJcbiAgICBpZiAoaXNPYmplY3QocnVsZXMpICYmIHJ1bGVzLl8kJGlzTm9ybWFsaXplZCkge1xyXG4gICAgICAgIHJldHVybiBydWxlcztcclxuICAgIH1cclxuICAgIGlmIChpc09iamVjdChydWxlcykpIHtcclxuICAgICAgICByZXR1cm4gT2JqZWN0LmtleXMocnVsZXMpLnJlZHVjZShmdW5jdGlvbiAocHJldiwgY3Vycikge1xyXG4gICAgICAgICAgICB2YXIgcGFyYW1zID0gW107XHJcbiAgICAgICAgICAgIGlmIChydWxlc1tjdXJyXSA9PT0gdHJ1ZSkge1xyXG4gICAgICAgICAgICAgICAgcGFyYW1zID0gW107XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZWxzZSBpZiAoQXJyYXkuaXNBcnJheShydWxlc1tjdXJyXSkpIHtcclxuICAgICAgICAgICAgICAgIHBhcmFtcyA9IHJ1bGVzW2N1cnJdO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGVsc2UgaWYgKGlzT2JqZWN0KHJ1bGVzW2N1cnJdKSkge1xyXG4gICAgICAgICAgICAgICAgcGFyYW1zID0gcnVsZXNbY3Vycl07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBwYXJhbXMgPSBbcnVsZXNbY3Vycl1dO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmIChydWxlc1tjdXJyXSAhPT0gZmFsc2UpIHtcclxuICAgICAgICAgICAgICAgIHByZXZbY3Vycl0gPSBwYXJhbXM7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIHByZXY7XHJcbiAgICAgICAgfSwgYWNjKTtcclxuICAgIH1cclxuICAgIC8qIGlzdGFuYnVsIGlnbm9yZSBpZiAqL1xyXG4gICAgaWYgKHR5cGVvZiBydWxlcyAhPT0gJ3N0cmluZycpIHtcclxuICAgICAgICB3YXJuKCdydWxlcyBtdXN0IGJlIGVpdGhlciBhIHN0cmluZyBvciBhbiBvYmplY3QuJyk7XHJcbiAgICAgICAgcmV0dXJuIGFjYztcclxuICAgIH1cclxuICAgIHJldHVybiBydWxlcy5zcGxpdCgnfCcpLnJlZHVjZShmdW5jdGlvbiAocHJldiwgcnVsZSkge1xyXG4gICAgICAgIHZhciBwYXJzZWRSdWxlID0gcGFyc2VSdWxlKHJ1bGUpO1xyXG4gICAgICAgIGlmICghcGFyc2VkUnVsZS5uYW1lKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBwcmV2O1xyXG4gICAgICAgIH1cclxuICAgICAgICBwcmV2W3BhcnNlZFJ1bGUubmFtZV0gPSBwYXJzZWRSdWxlLnBhcmFtcztcclxuICAgICAgICByZXR1cm4gcHJldjtcclxuICAgIH0sIGFjYyk7XHJcbn07XHJcbi8qKlxyXG4gKiBDaGVja3MgaWYgYSBmdW5jdGlvbiBpcyBjYWxsYWJsZS5cclxuICovXHJcbnZhciBpc0NhbGxhYmxlID0gZnVuY3Rpb24gKGZ1bmMpIHsgcmV0dXJuIHR5cGVvZiBmdW5jID09PSAnZnVuY3Rpb24nOyB9O1xyXG5mdW5jdGlvbiBjb21wdXRlQ2xhc3NPYmoobmFtZXMsIGZsYWdzKSB7XHJcbiAgICB2YXIgYWNjID0ge307XHJcbiAgICB2YXIga2V5cyA9IE9iamVjdC5rZXlzKGZsYWdzKTtcclxuICAgIHZhciBsZW5ndGggPSBrZXlzLmxlbmd0aDtcclxuICAgIHZhciBfbG9vcF8xID0gZnVuY3Rpb24gKGkpIHtcclxuICAgICAgICB2YXIgZmxhZyA9IGtleXNbaV07XHJcbiAgICAgICAgdmFyIGNsYXNzTmFtZSA9IChuYW1lcyAmJiBuYW1lc1tmbGFnXSkgfHwgZmxhZztcclxuICAgICAgICB2YXIgdmFsdWUgPSBmbGFnc1tmbGFnXTtcclxuICAgICAgICBpZiAoaXNOdWxsT3JVbmRlZmluZWQodmFsdWUpKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBcImNvbnRpbnVlXCI7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICgoZmxhZyA9PT0gJ3ZhbGlkJyB8fCBmbGFnID09PSAnaW52YWxpZCcpICYmICFmbGFncy52YWxpZGF0ZWQpIHtcclxuICAgICAgICAgICAgcmV0dXJuIFwiY29udGludWVcIjtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHR5cGVvZiBjbGFzc05hbWUgPT09ICdzdHJpbmcnKSB7XHJcbiAgICAgICAgICAgIGFjY1tjbGFzc05hbWVdID0gdmFsdWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2UgaWYgKEFycmF5LmlzQXJyYXkoY2xhc3NOYW1lKSkge1xyXG4gICAgICAgICAgICBjbGFzc05hbWUuZm9yRWFjaChmdW5jdGlvbiAoY2xzKSB7XHJcbiAgICAgICAgICAgICAgICBhY2NbY2xzXSA9IHZhbHVlO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBsZW5ndGg7IGkrKykge1xyXG4gICAgICAgIF9sb29wXzEoaSk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gYWNjO1xyXG59XHJcbi8qIGlzdGFuYnVsIGlnbm9yZSBuZXh0ICovXHJcbmZ1bmN0aW9uIF9jb3B5QXJyYXkoYXJyYXlMaWtlKSB7XHJcbiAgICB2YXIgYXJyYXkgPSBbXTtcclxuICAgIHZhciBsZW5ndGggPSBhcnJheUxpa2UubGVuZ3RoO1xyXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBsZW5ndGg7IGkrKykge1xyXG4gICAgICAgIGFycmF5LnB1c2goYXJyYXlMaWtlW2ldKTtcclxuICAgIH1cclxuICAgIHJldHVybiBhcnJheTtcclxufVxyXG4vKipcclxuICogQ29udmVydHMgYW4gYXJyYXktbGlrZSBvYmplY3QgdG8gYXJyYXksIHByb3ZpZGVzIGEgc2ltcGxlIHBvbHlmaWxsIGZvciBBcnJheS5mcm9tXHJcbiAqL1xyXG5mdW5jdGlvbiB0b0FycmF5KGFycmF5TGlrZSkge1xyXG4gICAgaWYgKGlzQ2FsbGFibGUoQXJyYXkuZnJvbSkpIHtcclxuICAgICAgICByZXR1cm4gQXJyYXkuZnJvbShhcnJheUxpa2UpO1xyXG4gICAgfVxyXG4gICAgLyogaXN0YW5idWwgaWdub3JlIG5leHQgKi9cclxuICAgIHJldHVybiBfY29weUFycmF5KGFycmF5TGlrZSk7XHJcbn1cclxuZnVuY3Rpb24gZmluZEluZGV4KGFycmF5TGlrZSwgcHJlZGljYXRlKSB7XHJcbiAgICB2YXIgYXJyYXkgPSBBcnJheS5pc0FycmF5KGFycmF5TGlrZSkgPyBhcnJheUxpa2UgOiB0b0FycmF5KGFycmF5TGlrZSk7XHJcbiAgICBpZiAoaXNDYWxsYWJsZShhcnJheS5maW5kSW5kZXgpKSB7XHJcbiAgICAgICAgcmV0dXJuIGFycmF5LmZpbmRJbmRleChwcmVkaWNhdGUpO1xyXG4gICAgfVxyXG4gICAgLyogaXN0YW5idWwgaWdub3JlIG5leHQgKi9cclxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgYXJyYXkubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICBpZiAocHJlZGljYXRlKGFycmF5W2ldLCBpKSkge1xyXG4gICAgICAgICAgICByZXR1cm4gaTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAvKiBpc3RhbmJ1bCBpZ25vcmUgbmV4dCAqL1xyXG4gICAgcmV0dXJuIC0xO1xyXG59XHJcbi8qKlxyXG4gKiBmaW5kcyB0aGUgZmlyc3QgZWxlbWVudCB0aGF0IHNhdGlzZmllcyB0aGUgcHJlZGljYXRlIGNhbGxiYWNrLCBwb2x5ZmlsbHMgYXJyYXkuZmluZFxyXG4gKi9cclxuZnVuY3Rpb24gZmluZChhcnJheUxpa2UsIHByZWRpY2F0ZSkge1xyXG4gICAgdmFyIGFycmF5ID0gQXJyYXkuaXNBcnJheShhcnJheUxpa2UpID8gYXJyYXlMaWtlIDogdG9BcnJheShhcnJheUxpa2UpO1xyXG4gICAgdmFyIGlkeCA9IGZpbmRJbmRleChhcnJheSwgcHJlZGljYXRlKTtcclxuICAgIHJldHVybiBpZHggPT09IC0xID8gdW5kZWZpbmVkIDogYXJyYXlbaWR4XTtcclxufVxyXG5mdW5jdGlvbiBtZXJnZSh0YXJnZXQsIHNvdXJjZSkge1xyXG4gICAgT2JqZWN0LmtleXMoc291cmNlKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcclxuICAgICAgICBpZiAoaXNPYmplY3Qoc291cmNlW2tleV0pKSB7XHJcbiAgICAgICAgICAgIGlmICghdGFyZ2V0W2tleV0pIHtcclxuICAgICAgICAgICAgICAgIHRhcmdldFtrZXldID0ge307XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgbWVyZ2UodGFyZ2V0W2tleV0sIHNvdXJjZVtrZXldKTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0YXJnZXRba2V5XSA9IHNvdXJjZVtrZXldO1xyXG4gICAgfSk7XHJcbiAgICByZXR1cm4gdGFyZ2V0O1xyXG59XHJcbmZ1bmN0aW9uIHZhbHVlcyhvYmopIHtcclxuICAgIGlmIChpc0NhbGxhYmxlKE9iamVjdC52YWx1ZXMpKSB7XHJcbiAgICAgICAgcmV0dXJuIE9iamVjdC52YWx1ZXMob2JqKTtcclxuICAgIH1cclxuICAgIC8vIGZhbGxiYWNrIHRvIGtleXMoKVxyXG4gICAgLyogaXN0YW5idWwgaWdub3JlIG5leHQgKi9cclxuICAgIHJldHVybiBPYmplY3Qua2V5cyhvYmopLm1hcChmdW5jdGlvbiAoaykgeyByZXR1cm4gb2JqW2tdOyB9KTtcclxufVxyXG52YXIgaXNFbXB0eUFycmF5ID0gZnVuY3Rpb24gKGFycikge1xyXG4gICAgcmV0dXJuIEFycmF5LmlzQXJyYXkoYXJyKSAmJiBhcnIubGVuZ3RoID09PSAwO1xyXG59O1xyXG52YXIgaW50ZXJwb2xhdGUgPSBmdW5jdGlvbiAodGVtcGxhdGUsIHZhbHVlcykge1xyXG4gICAgcmV0dXJuIHRlbXBsYXRlLnJlcGxhY2UoL1xceyhbXn1dKylcXH0vZywgZnVuY3Rpb24gKF8sIHApIHtcclxuICAgICAgICByZXR1cm4gcCBpbiB2YWx1ZXMgPyB2YWx1ZXNbcF0gOiBcIntcIiArIHAgKyBcIn1cIjtcclxuICAgIH0pO1xyXG59O1xyXG4vLyBDaGVja3MgaWYgYSBnaXZlbiB2YWx1ZSBpcyBub3QgYW4gZW1wdHkgc3RyaW5nIG9yIG51bGwgb3IgdW5kZWZpbmVkLlxyXG52YXIgaXNTcGVjaWZpZWQgPSBmdW5jdGlvbiAodmFsKSB7XHJcbiAgICBpZiAodmFsID09PSAnJykge1xyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuICAgIHJldHVybiAhaXNOdWxsT3JVbmRlZmluZWQodmFsKTtcclxufTtcblxudmFyIFJVTEVTID0ge307XHJcbmZ1bmN0aW9uIG5vcm1hbGl6ZVNjaGVtYShzY2hlbWEpIHtcclxuICAgIGlmIChzY2hlbWEucGFyYW1zICYmIHNjaGVtYS5wYXJhbXMubGVuZ3RoKSB7XHJcbiAgICAgICAgc2NoZW1hLnBhcmFtcyA9IHNjaGVtYS5wYXJhbXMubWFwKGZ1bmN0aW9uIChwYXJhbSkge1xyXG4gICAgICAgICAgICBpZiAodHlwZW9mIHBhcmFtID09PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHsgbmFtZTogcGFyYW0gfTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gcGFyYW07XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gc2NoZW1hO1xyXG59XHJcbnZhciBSdWxlQ29udGFpbmVyID0gLyoqIEBjbGFzcyAqLyAoZnVuY3Rpb24gKCkge1xyXG4gICAgZnVuY3Rpb24gUnVsZUNvbnRhaW5lcigpIHtcclxuICAgIH1cclxuICAgIFJ1bGVDb250YWluZXIuZXh0ZW5kID0gZnVuY3Rpb24gKG5hbWUsIHNjaGVtYSkge1xyXG4gICAgICAgIC8vIGlmIHJ1bGUgYWxyZWFkeSBleGlzdHMsIG92ZXJ3cml0ZSBpdC5cclxuICAgICAgICB2YXIgcnVsZSA9IG5vcm1hbGl6ZVNjaGVtYShzY2hlbWEpO1xyXG4gICAgICAgIGlmIChSVUxFU1tuYW1lXSkge1xyXG4gICAgICAgICAgICBSVUxFU1tuYW1lXSA9IG1lcmdlKFJVTEVTW25hbWVdLCBzY2hlbWEpO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFJVTEVTW25hbWVdID0gX19hc3NpZ24oeyBsYXp5OiBmYWxzZSwgY29tcHV0ZXNSZXF1aXJlZDogZmFsc2UgfSwgcnVsZSk7XHJcbiAgICB9O1xyXG4gICAgUnVsZUNvbnRhaW5lci5pdGVyYXRlID0gZnVuY3Rpb24gKGZuKSB7XHJcbiAgICAgICAgdmFyIGtleXMgPSBPYmplY3Qua2V5cyhSVUxFUyk7XHJcbiAgICAgICAgdmFyIGxlbmd0aCA9IGtleXMubGVuZ3RoO1xyXG4gICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgbGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgZm4oa2V5c1tpXSwgUlVMRVNba2V5c1tpXV0pO1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcbiAgICBSdWxlQ29udGFpbmVyLmlzTGF6eSA9IGZ1bmN0aW9uIChuYW1lKSB7XHJcbiAgICAgICAgcmV0dXJuICEhKFJVTEVTW25hbWVdICYmIFJVTEVTW25hbWVdLmxhenkpO1xyXG4gICAgfTtcclxuICAgIFJ1bGVDb250YWluZXIuaXNSZXF1aXJlUnVsZSA9IGZ1bmN0aW9uIChuYW1lKSB7XHJcbiAgICAgICAgcmV0dXJuICEhKFJVTEVTW25hbWVdICYmIFJVTEVTW25hbWVdLmNvbXB1dGVzUmVxdWlyZWQpO1xyXG4gICAgfTtcclxuICAgIFJ1bGVDb250YWluZXIuaXNUYXJnZXRSdWxlID0gZnVuY3Rpb24gKG5hbWUpIHtcclxuICAgICAgICB2YXIgZGVmaW5pdGlvbiA9IFJ1bGVDb250YWluZXIuZ2V0UnVsZURlZmluaXRpb24obmFtZSk7XHJcbiAgICAgICAgaWYgKCFkZWZpbml0aW9uIHx8ICFkZWZpbml0aW9uLnBhcmFtcykge1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBkZWZpbml0aW9uLnBhcmFtcy5zb21lKGZ1bmN0aW9uIChwYXJhbSkgeyByZXR1cm4gISFwYXJhbS5pc1RhcmdldDsgfSk7XHJcbiAgICB9O1xyXG4gICAgUnVsZUNvbnRhaW5lci5nZXRUYXJnZXRQYXJhbU5hbWVzID0gZnVuY3Rpb24gKHJ1bGUsIHBhcmFtcykge1xyXG4gICAgICAgIHZhciBkZWZpbml0aW9uID0gUnVsZUNvbnRhaW5lci5nZXRSdWxlRGVmaW5pdGlvbihydWxlKTtcclxuICAgICAgICBpZiAoQXJyYXkuaXNBcnJheShwYXJhbXMpKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBwYXJhbXMuZmlsdGVyKGZ1bmN0aW9uIChfLCBpZHgpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBkZWZpbml0aW9uLnBhcmFtcyAmJiBmaW5kKGRlZmluaXRpb24ucGFyYW1zLCBmdW5jdGlvbiAocCwgaSkgeyByZXR1cm4gISFwLmlzVGFyZ2V0ICYmIGkgPT09IGlkeDsgfSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gT2JqZWN0LmtleXMocGFyYW1zKVxyXG4gICAgICAgICAgICAuZmlsdGVyKGZ1bmN0aW9uIChrZXkpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGRlZmluaXRpb24ucGFyYW1zICYmIGZpbmQoZGVmaW5pdGlvbi5wYXJhbXMsIGZ1bmN0aW9uIChwKSB7IHJldHVybiAhIXAuaXNUYXJnZXQgJiYgcC5uYW1lID09PSBrZXk7IH0pO1xyXG4gICAgICAgIH0pXHJcbiAgICAgICAgICAgIC5tYXAoZnVuY3Rpb24gKGtleSkgeyByZXR1cm4gcGFyYW1zW2tleV07IH0pO1xyXG4gICAgfTtcclxuICAgIFJ1bGVDb250YWluZXIuZ2V0UnVsZURlZmluaXRpb24gPSBmdW5jdGlvbiAocnVsZU5hbWUpIHtcclxuICAgICAgICByZXR1cm4gUlVMRVNbcnVsZU5hbWVdO1xyXG4gICAgfTtcclxuICAgIHJldHVybiBSdWxlQ29udGFpbmVyO1xyXG59KCkpO1xyXG4vKipcclxuICogQWRkcyBhIGN1c3RvbSB2YWxpZGF0b3IgdG8gdGhlIGxpc3Qgb2YgdmFsaWRhdGlvbiBydWxlcy5cclxuICovXHJcbmZ1bmN0aW9uIGV4dGVuZChuYW1lLCBzY2hlbWEpIHtcclxuICAgIC8vIG1ha2VzIHN1cmUgbmV3IHJ1bGVzIGFyZSBwcm9wZXJseSBmb3JtYXR0ZWQuXHJcbiAgICBndWFyZEV4dGVuZChuYW1lLCBzY2hlbWEpO1xyXG4gICAgLy8gRnVsbCBzY2hlbWEgb2JqZWN0LlxyXG4gICAgaWYgKHR5cGVvZiBzY2hlbWEgPT09ICdvYmplY3QnKSB7XHJcbiAgICAgICAgUnVsZUNvbnRhaW5lci5leHRlbmQobmFtZSwgc2NoZW1hKTtcclxuICAgICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICBSdWxlQ29udGFpbmVyLmV4dGVuZChuYW1lLCB7XHJcbiAgICAgICAgdmFsaWRhdGU6IHNjaGVtYVxyXG4gICAgfSk7XHJcbn1cclxuLyoqXHJcbiAqIEd1YXJkcyBmcm9tIGV4dGVuc2lvbiB2aW9sYXRpb25zLlxyXG4gKi9cclxuZnVuY3Rpb24gZ3VhcmRFeHRlbmQobmFtZSwgdmFsaWRhdG9yKSB7XHJcbiAgICBpZiAoaXNDYWxsYWJsZSh2YWxpZGF0b3IpKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgaWYgKGlzQ2FsbGFibGUodmFsaWRhdG9yLnZhbGlkYXRlKSkge1xyXG4gICAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIGlmIChSdWxlQ29udGFpbmVyLmdldFJ1bGVEZWZpbml0aW9uKG5hbWUpKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgdGhyb3cgbmV3IEVycm9yKFwiRXh0ZW5zaW9uIEVycm9yOiBUaGUgdmFsaWRhdG9yICdcIiArIG5hbWUgKyBcIicgbXVzdCBiZSBhIGZ1bmN0aW9uIG9yIGhhdmUgYSAndmFsaWRhdGUnIG1ldGhvZC5cIik7XHJcbn1cblxudmFyIERFRkFVTFRfQ09ORklHID0ge1xyXG4gICAgZGVmYXVsdE1lc3NhZ2U6IFwie19maWVsZF99IGlzIG5vdCB2YWxpZC5cIixcclxuICAgIHNraXBPcHRpb25hbDogdHJ1ZSxcclxuICAgIGNsYXNzZXM6IHtcclxuICAgICAgICB0b3VjaGVkOiAndG91Y2hlZCcsXHJcbiAgICAgICAgdW50b3VjaGVkOiAndW50b3VjaGVkJyxcclxuICAgICAgICB2YWxpZDogJ3ZhbGlkJyxcclxuICAgICAgICBpbnZhbGlkOiAnaW52YWxpZCcsXHJcbiAgICAgICAgcHJpc3RpbmU6ICdwcmlzdGluZScsXHJcbiAgICAgICAgZGlydHk6ICdkaXJ0eScgLy8gY29udHJvbCBoYXMgYmVlbiBpbnRlcmFjdGVkIHdpdGhcclxuICAgIH0sXHJcbiAgICBiYWlsczogdHJ1ZSxcclxuICAgIG1vZGU6ICdhZ2dyZXNzaXZlJyxcclxuICAgIHVzZUNvbnN0cmFpbnRBdHRyczogdHJ1ZVxyXG59O1xyXG52YXIgY3VycmVudENvbmZpZyA9IF9fYXNzaWduKHt9LCBERUZBVUxUX0NPTkZJRyk7XHJcbnZhciBnZXRDb25maWcgPSBmdW5jdGlvbiAoKSB7IHJldHVybiBjdXJyZW50Q29uZmlnOyB9O1xyXG52YXIgc2V0Q29uZmlnID0gZnVuY3Rpb24gKG5ld0NvbmYpIHtcclxuICAgIGN1cnJlbnRDb25maWcgPSBfX2Fzc2lnbihfX2Fzc2lnbih7fSwgY3VycmVudENvbmZpZyksIG5ld0NvbmYpO1xyXG59O1xyXG52YXIgY29uZmlndXJlID0gZnVuY3Rpb24gKGNmZykge1xyXG4gICAgc2V0Q29uZmlnKGNmZyk7XHJcbn07XG5cbi8qKlxyXG4gKiBWYWxpZGF0ZXMgYSB2YWx1ZSBhZ2FpbnN0IHRoZSBydWxlcy5cclxuICovXHJcbmZ1bmN0aW9uIHZhbGlkYXRlKHZhbHVlLCBydWxlcywgb3B0aW9ucykge1xyXG4gICAgaWYgKG9wdGlvbnMgPT09IHZvaWQgMCkgeyBvcHRpb25zID0ge307IH1cclxuICAgIHJldHVybiBfX2F3YWl0ZXIodGhpcywgdm9pZCAwLCB2b2lkIDAsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICB2YXIgc2hvdWxkQmFpbCwgc2tpcElmRW1wdHksIGZpZWxkLCByZXN1bHQsIGVycm9ycywgcnVsZU1hcDtcclxuICAgICAgICByZXR1cm4gX19nZW5lcmF0b3IodGhpcywgZnVuY3Rpb24gKF9hKSB7XHJcbiAgICAgICAgICAgIHN3aXRjaCAoX2EubGFiZWwpIHtcclxuICAgICAgICAgICAgICAgIGNhc2UgMDpcclxuICAgICAgICAgICAgICAgICAgICBzaG91bGRCYWlsID0gb3B0aW9ucyAmJiBvcHRpb25zLmJhaWxzO1xyXG4gICAgICAgICAgICAgICAgICAgIHNraXBJZkVtcHR5ID0gb3B0aW9ucyAmJiBvcHRpb25zLnNraXBJZkVtcHR5O1xyXG4gICAgICAgICAgICAgICAgICAgIGZpZWxkID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiAob3B0aW9ucyAmJiBvcHRpb25zLm5hbWUpIHx8ICd7ZmllbGR9JyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcnVsZXM6IG5vcm1hbGl6ZVJ1bGVzKHJ1bGVzKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgYmFpbHM6IGlzTnVsbE9yVW5kZWZpbmVkKHNob3VsZEJhaWwpID8gdHJ1ZSA6IHNob3VsZEJhaWwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNraXBJZkVtcHR5OiBpc051bGxPclVuZGVmaW5lZChza2lwSWZFbXB0eSkgPyB0cnVlIDogc2tpcElmRW1wdHksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvcmNlUmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjcm9zc1RhYmxlOiAob3B0aW9ucyAmJiBvcHRpb25zLnZhbHVlcykgfHwge30sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG5hbWVzOiAob3B0aW9ucyAmJiBvcHRpb25zLm5hbWVzKSB8fCB7fSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY3VzdG9tTWVzc2FnZXM6IChvcHRpb25zICYmIG9wdGlvbnMuY3VzdG9tTWVzc2FnZXMpIHx8IHt9XHJcbiAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gWzQgLyp5aWVsZCovLCBfdmFsaWRhdGUoZmllbGQsIHZhbHVlLCBvcHRpb25zKV07XHJcbiAgICAgICAgICAgICAgICBjYXNlIDE6XHJcbiAgICAgICAgICAgICAgICAgICAgcmVzdWx0ID0gX2Euc2VudCgpO1xyXG4gICAgICAgICAgICAgICAgICAgIGVycm9ycyA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgIHJ1bGVNYXAgPSB7fTtcclxuICAgICAgICAgICAgICAgICAgICByZXN1bHQuZXJyb3JzLmZvckVhY2goZnVuY3Rpb24gKGUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZXJyb3JzLnB1c2goZS5tc2cpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBydWxlTWFwW2UucnVsZV0gPSBlLm1zZztcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gWzIgLypyZXR1cm4qLywge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsaWQ6IHJlc3VsdC52YWxpZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yczogZXJyb3JzLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZmFpbGVkUnVsZXM6IHJ1bGVNYXBcclxuICAgICAgICAgICAgICAgICAgICAgICAgfV07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH0pO1xyXG59XHJcbi8qKlxyXG4gKiBTdGFydHMgdGhlIHZhbGlkYXRpb24gcHJvY2Vzcy5cclxuICovXHJcbmZ1bmN0aW9uIF92YWxpZGF0ZShmaWVsZCwgdmFsdWUsIF9hKSB7XHJcbiAgICB2YXIgX2IgPSAoX2EgPT09IHZvaWQgMCA/IHt9IDogX2EpLmlzSW5pdGlhbCwgaXNJbml0aWFsID0gX2IgPT09IHZvaWQgMCA/IGZhbHNlIDogX2I7XHJcbiAgICByZXR1cm4gX19hd2FpdGVyKHRoaXMsIHZvaWQgMCwgdm9pZCAwLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdmFyIF9jLCBzaG91bGRTa2lwLCBlcnJvcnMsIHJ1bGVzLCBsZW5ndGgsIGksIHJ1bGUsIHJlc3VsdDtcclxuICAgICAgICByZXR1cm4gX19nZW5lcmF0b3IodGhpcywgZnVuY3Rpb24gKF9kKSB7XHJcbiAgICAgICAgICAgIHN3aXRjaCAoX2QubGFiZWwpIHtcclxuICAgICAgICAgICAgICAgIGNhc2UgMDogcmV0dXJuIFs0IC8qeWllbGQqLywgX3Nob3VsZFNraXAoZmllbGQsIHZhbHVlKV07XHJcbiAgICAgICAgICAgICAgICBjYXNlIDE6XHJcbiAgICAgICAgICAgICAgICAgICAgX2MgPSBfZC5zZW50KCksIHNob3VsZFNraXAgPSBfYy5zaG91bGRTa2lwLCBlcnJvcnMgPSBfYy5lcnJvcnM7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHNob3VsZFNraXApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFsyIC8qcmV0dXJuKi8sIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWxpZDogIWVycm9ycy5sZW5ndGgsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZXJyb3JzOiBlcnJvcnNcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1dO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBydWxlcyA9IE9iamVjdC5rZXlzKGZpZWxkLnJ1bGVzKS5maWx0ZXIoZnVuY3Rpb24gKHJ1bGUpIHsgcmV0dXJuICFSdWxlQ29udGFpbmVyLmlzUmVxdWlyZVJ1bGUocnVsZSk7IH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIGxlbmd0aCA9IHJ1bGVzLmxlbmd0aDtcclxuICAgICAgICAgICAgICAgICAgICBpID0gMDtcclxuICAgICAgICAgICAgICAgICAgICBfZC5sYWJlbCA9IDI7XHJcbiAgICAgICAgICAgICAgICBjYXNlIDI6XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCEoaSA8IGxlbmd0aCkpIHJldHVybiBbMyAvKmJyZWFrKi8sIDVdO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChpc0luaXRpYWwgJiYgUnVsZUNvbnRhaW5lci5pc0xhenkocnVsZXNbaV0pKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBbMyAvKmJyZWFrKi8sIDRdO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBydWxlID0gcnVsZXNbaV07XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFs0IC8qeWllbGQqLywgX3Rlc3QoZmllbGQsIHZhbHVlLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiBydWxlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFyYW1zOiBmaWVsZC5ydWxlc1tydWxlXVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KV07XHJcbiAgICAgICAgICAgICAgICBjYXNlIDM6XHJcbiAgICAgICAgICAgICAgICAgICAgcmVzdWx0ID0gX2Quc2VudCgpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICghcmVzdWx0LnZhbGlkICYmIHJlc3VsdC5lcnJvcikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBlcnJvcnMucHVzaChyZXN1bHQuZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoZmllbGQuYmFpbHMpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBbMiAvKnJldHVybiovLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbGlkOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZXJyb3JzOiBlcnJvcnNcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBfZC5sYWJlbCA9IDQ7XHJcbiAgICAgICAgICAgICAgICBjYXNlIDQ6XHJcbiAgICAgICAgICAgICAgICAgICAgaSsrO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBbMyAvKmJyZWFrKi8sIDJdO1xyXG4gICAgICAgICAgICAgICAgY2FzZSA1OiByZXR1cm4gWzIgLypyZXR1cm4qLywge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWxpZDogIWVycm9ycy5sZW5ndGgsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yczogZXJyb3JzXHJcbiAgICAgICAgICAgICAgICAgICAgfV07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH0pO1xyXG59XHJcbmZ1bmN0aW9uIF9zaG91bGRTa2lwKGZpZWxkLCB2YWx1ZSkge1xyXG4gICAgcmV0dXJuIF9fYXdhaXRlcih0aGlzLCB2b2lkIDAsIHZvaWQgMCwgZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHZhciByZXF1aXJlUnVsZXMsIGxlbmd0aCwgZXJyb3JzLCBpc0VtcHR5LCBpc0VtcHR5QW5kT3B0aW9uYWwsIGlzUmVxdWlyZWQsIGksIHJ1bGUsIHJlc3VsdDtcclxuICAgICAgICByZXR1cm4gX19nZW5lcmF0b3IodGhpcywgZnVuY3Rpb24gKF9hKSB7XHJcbiAgICAgICAgICAgIHN3aXRjaCAoX2EubGFiZWwpIHtcclxuICAgICAgICAgICAgICAgIGNhc2UgMDpcclxuICAgICAgICAgICAgICAgICAgICByZXF1aXJlUnVsZXMgPSBPYmplY3Qua2V5cyhmaWVsZC5ydWxlcykuZmlsdGVyKFJ1bGVDb250YWluZXIuaXNSZXF1aXJlUnVsZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgbGVuZ3RoID0gcmVxdWlyZVJ1bGVzLmxlbmd0aDtcclxuICAgICAgICAgICAgICAgICAgICBlcnJvcnMgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICBpc0VtcHR5ID0gaXNOdWxsT3JVbmRlZmluZWQodmFsdWUpIHx8IHZhbHVlID09PSAnJyB8fCBpc0VtcHR5QXJyYXkodmFsdWUpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlzRW1wdHlBbmRPcHRpb25hbCA9IGlzRW1wdHkgJiYgZmllbGQuc2tpcElmRW1wdHk7XHJcbiAgICAgICAgICAgICAgICAgICAgaXNSZXF1aXJlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIGkgPSAwO1xyXG4gICAgICAgICAgICAgICAgICAgIF9hLmxhYmVsID0gMTtcclxuICAgICAgICAgICAgICAgIGNhc2UgMTpcclxuICAgICAgICAgICAgICAgICAgICBpZiAoIShpIDwgbGVuZ3RoKSkgcmV0dXJuIFszIC8qYnJlYWsqLywgNF07XHJcbiAgICAgICAgICAgICAgICAgICAgcnVsZSA9IHJlcXVpcmVSdWxlc1tpXTtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gWzQgLyp5aWVsZCovLCBfdGVzdChmaWVsZCwgdmFsdWUsIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IHJ1bGUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwYXJhbXM6IGZpZWxkLnJ1bGVzW3J1bGVdXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXTtcclxuICAgICAgICAgICAgICAgIGNhc2UgMjpcclxuICAgICAgICAgICAgICAgICAgICByZXN1bHQgPSBfYS5zZW50KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFpc09iamVjdChyZXN1bHQpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcignUmVxdWlyZSBydWxlcyBoYXMgdG8gcmV0dXJuIGFuIG9iamVjdCAoc2VlIGRvY3MpJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXN1bHQucmVxdWlyZWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaXNSZXF1aXJlZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIGlmICghcmVzdWx0LnZhbGlkICYmIHJlc3VsdC5lcnJvcikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBlcnJvcnMucHVzaChyZXN1bHQuZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBFeGl0IGVhcmx5IGFzIHRoZSBmaWVsZCBpcyByZXF1aXJlZCBhbmQgZmFpbGVkIHZhbGlkYXRpb24uXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChmaWVsZC5iYWlscykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFsyIC8qcmV0dXJuKi8sIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2hvdWxkU2tpcDogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZXJyb3JzOiBlcnJvcnNcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBfYS5sYWJlbCA9IDM7XHJcbiAgICAgICAgICAgICAgICBjYXNlIDM6XHJcbiAgICAgICAgICAgICAgICAgICAgaSsrO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBbMyAvKmJyZWFrKi8sIDFdO1xyXG4gICAgICAgICAgICAgICAgY2FzZSA0OlxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChpc0VtcHR5ICYmICFpc1JlcXVpcmVkICYmICFmaWVsZC5za2lwSWZFbXB0eSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gWzIgLypyZXR1cm4qLywge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNob3VsZFNraXA6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yczogZXJyb3JzXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gZmllbGQgaXMgY29uZmlndXJlZCB0byBydW4gdGhyb3VnaCB0aGUgcGlwZWxpbmUgcmVnYXJkbGVzc1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICghZmllbGQuYmFpbHMgJiYgIWlzRW1wdHlBbmRPcHRpb25hbCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gWzIgLypyZXR1cm4qLywge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNob3VsZFNraXA6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yczogZXJyb3JzXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gc2tpcCBpZiB0aGUgZmllbGQgaXMgbm90IHJlcXVpcmVkIGFuZCBoYXMgYW4gZW1wdHkgdmFsdWUuXHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFsyIC8qcmV0dXJuKi8sIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNob3VsZFNraXA6ICFpc1JlcXVpcmVkICYmIGlzRW1wdHksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBlcnJvcnM6IGVycm9yc1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfSk7XHJcbn1cclxuLyoqXHJcbiAqIFRlc3RzIGEgc2luZ2xlIGlucHV0IHZhbHVlIGFnYWluc3QgYSBydWxlLlxyXG4gKi9cclxuZnVuY3Rpb24gX3Rlc3QoZmllbGQsIHZhbHVlLCBydWxlKSB7XHJcbiAgICByZXR1cm4gX19hd2FpdGVyKHRoaXMsIHZvaWQgMCwgdm9pZCAwLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdmFyIHJ1bGVTY2hlbWEsIHBhcmFtcywgbm9ybWFsaXplZFZhbHVlLCByZXN1bHQsIHZhbHVlcztcclxuICAgICAgICByZXR1cm4gX19nZW5lcmF0b3IodGhpcywgZnVuY3Rpb24gKF9hKSB7XHJcbiAgICAgICAgICAgIHN3aXRjaCAoX2EubGFiZWwpIHtcclxuICAgICAgICAgICAgICAgIGNhc2UgMDpcclxuICAgICAgICAgICAgICAgICAgICBydWxlU2NoZW1hID0gUnVsZUNvbnRhaW5lci5nZXRSdWxlRGVmaW5pdGlvbihydWxlLm5hbWUpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICghcnVsZVNjaGVtYSB8fCAhcnVsZVNjaGVtYS52YWxpZGF0ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJObyBzdWNoIHZhbGlkYXRvciAnXCIgKyBydWxlLm5hbWUgKyBcIicgZXhpc3RzLlwiKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgcGFyYW1zID0gX2J1aWxkUGFyYW1zKHJ1bGUucGFyYW1zLCBydWxlU2NoZW1hLnBhcmFtcywgZmllbGQuY3Jvc3NUYWJsZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgbm9ybWFsaXplZFZhbHVlID0gcnVsZVNjaGVtYS5jYXN0VmFsdWUgPyBydWxlU2NoZW1hLmNhc3RWYWx1ZSh2YWx1ZSkgOiB2YWx1ZTtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gWzQgLyp5aWVsZCovLCBydWxlU2NoZW1hLnZhbGlkYXRlKG5vcm1hbGl6ZWRWYWx1ZSwgcGFyYW1zKV07XHJcbiAgICAgICAgICAgICAgICBjYXNlIDE6XHJcbiAgICAgICAgICAgICAgICAgICAgcmVzdWx0ID0gX2Euc2VudCgpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0eXBlb2YgcmVzdWx0ID09PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZXMgPSBfX2Fzc2lnbihfX2Fzc2lnbih7fSwgKHBhcmFtcyB8fCB7fSkpLCB7IF9maWVsZF86IGZpZWxkLm5hbWUsIF92YWx1ZV86IHZhbHVlLCBfcnVsZV86IHJ1bGUubmFtZSB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFsyIC8qcmV0dXJuKi8sIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWxpZDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZXJyb3I6IHsgcnVsZTogcnVsZS5uYW1lLCBtc2c6IGludGVycG9sYXRlKHJlc3VsdCwgdmFsdWVzKSB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFpc09iamVjdChyZXN1bHQpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3VsdCA9IHsgdmFsaWQ6IHJlc3VsdCwgZGF0YToge30gfTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFsyIC8qcmV0dXJuKi8sIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbGlkOiByZXN1bHQudmFsaWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXF1aXJlZDogcmVzdWx0LnJlcXVpcmVkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0YTogcmVzdWx0LmRhdGEgfHwge30sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBlcnJvcjogcmVzdWx0LnZhbGlkID8gdW5kZWZpbmVkIDogX2dlbmVyYXRlRmllbGRFcnJvcihmaWVsZCwgdmFsdWUsIHJ1bGVTY2hlbWEsIHJ1bGUubmFtZSwgcGFyYW1zLCByZXN1bHQuZGF0YSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgfV07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH0pO1xyXG59XHJcbi8qKlxyXG4gKiBHZW5lcmF0ZXMgZXJyb3IgbWVzc2FnZXMuXHJcbiAqL1xyXG5mdW5jdGlvbiBfZ2VuZXJhdGVGaWVsZEVycm9yKGZpZWxkLCB2YWx1ZSwgcnVsZVNjaGVtYSwgcnVsZU5hbWUsIHBhcmFtcywgZGF0YSkge1xyXG4gICAgdmFyIHZhbHVlcyA9IF9fYXNzaWduKF9fYXNzaWduKF9fYXNzaWduKF9fYXNzaWduKHt9LCAocGFyYW1zIHx8IHt9KSksIChkYXRhIHx8IHt9KSksIHsgX2ZpZWxkXzogZmllbGQubmFtZSwgX3ZhbHVlXzogdmFsdWUsIF9ydWxlXzogcnVsZU5hbWUgfSksIF9nZXRUYXJnZXROYW1lcyhmaWVsZCwgcnVsZVNjaGVtYSwgcnVsZU5hbWUpKTtcclxuICAgIGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoZmllbGQuY3VzdG9tTWVzc2FnZXMsIHJ1bGVOYW1lKSAmJlxyXG4gICAgICAgIHR5cGVvZiBmaWVsZC5jdXN0b21NZXNzYWdlc1tydWxlTmFtZV0gPT09ICdzdHJpbmcnKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgbXNnOiBfbm9ybWFsaXplTWVzc2FnZShmaWVsZC5jdXN0b21NZXNzYWdlc1tydWxlTmFtZV0sIGZpZWxkLm5hbWUsIHZhbHVlcyksXHJcbiAgICAgICAgICAgIHJ1bGU6IHJ1bGVOYW1lXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuICAgIGlmIChydWxlU2NoZW1hLm1lc3NhZ2UpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBtc2c6IF9ub3JtYWxpemVNZXNzYWdlKHJ1bGVTY2hlbWEubWVzc2FnZSwgZmllbGQubmFtZSwgdmFsdWVzKSxcclxuICAgICAgICAgICAgcnVsZTogcnVsZU5hbWVcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHtcclxuICAgICAgICBtc2c6IF9ub3JtYWxpemVNZXNzYWdlKGdldENvbmZpZygpLmRlZmF1bHRNZXNzYWdlLCBmaWVsZC5uYW1lLCB2YWx1ZXMpLFxyXG4gICAgICAgIHJ1bGU6IHJ1bGVOYW1lXHJcbiAgICB9O1xyXG59XHJcbmZ1bmN0aW9uIF9nZXRUYXJnZXROYW1lcyhmaWVsZCwgcnVsZVNjaGVtYSwgcnVsZU5hbWUpIHtcclxuICAgIGlmIChydWxlU2NoZW1hLnBhcmFtcykge1xyXG4gICAgICAgIHZhciBudW1UYXJnZXRzID0gcnVsZVNjaGVtYS5wYXJhbXMuZmlsdGVyKGZ1bmN0aW9uIChwYXJhbSkgeyByZXR1cm4gcGFyYW0uaXNUYXJnZXQ7IH0pLmxlbmd0aDtcclxuICAgICAgICBpZiAobnVtVGFyZ2V0cyA+IDApIHtcclxuICAgICAgICAgICAgdmFyIG5hbWVzID0ge307XHJcbiAgICAgICAgICAgIGZvciAodmFyIGluZGV4ID0gMDsgaW5kZXggPCBydWxlU2NoZW1hLnBhcmFtcy5sZW5ndGg7IGluZGV4KyspIHtcclxuICAgICAgICAgICAgICAgIHZhciBwYXJhbSA9IHJ1bGVTY2hlbWEucGFyYW1zW2luZGV4XTtcclxuICAgICAgICAgICAgICAgIGlmIChwYXJhbS5pc1RhcmdldCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHZhciBrZXkgPSBmaWVsZC5ydWxlc1tydWxlTmFtZV1baW5kZXhdO1xyXG4gICAgICAgICAgICAgICAgICAgIHZhciBuYW1lXzEgPSBmaWVsZC5uYW1lc1trZXldIHx8IGtleTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAobnVtVGFyZ2V0cyA9PT0gMSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBuYW1lcy5fdGFyZ2V0XyA9IG5hbWVfMTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBuYW1lc1tcIl9cIiArIHBhcmFtLm5hbWUgKyBcIlRhcmdldF9cIl0gPSBuYW1lXzE7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybiBuYW1lcztcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICByZXR1cm4ge307XHJcbn1cclxuZnVuY3Rpb24gX25vcm1hbGl6ZU1lc3NhZ2UodGVtcGxhdGUsIGZpZWxkLCB2YWx1ZXMpIHtcclxuICAgIGlmICh0eXBlb2YgdGVtcGxhdGUgPT09ICdmdW5jdGlvbicpIHtcclxuICAgICAgICByZXR1cm4gdGVtcGxhdGUoZmllbGQsIHZhbHVlcyk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gaW50ZXJwb2xhdGUodGVtcGxhdGUsIF9fYXNzaWduKF9fYXNzaWduKHt9LCB2YWx1ZXMpLCB7IF9maWVsZF86IGZpZWxkIH0pKTtcclxufVxyXG5mdW5jdGlvbiBfYnVpbGRQYXJhbXMocHJvdmlkZWQsIGRlZmluZWQsIGNyb3NzVGFibGUpIHtcclxuICAgIHZhciBwYXJhbXMgPSB7fTtcclxuICAgIGlmICghZGVmaW5lZCAmJiAhQXJyYXkuaXNBcnJheShwcm92aWRlZCkpIHtcclxuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1lvdSBwcm92aWRlZCBhbiBvYmplY3QgcGFyYW1zIHRvIGEgcnVsZSB0aGF0IGhhcyBubyBkZWZpbmVkIHNjaGVtYS4nKTtcclxuICAgIH1cclxuICAgIC8vIFJ1bGUgcHJvYmFibHkgdXNlcyBhbiBhcnJheSBmb3IgdGhlaXIgYXJncywga2VlcCBpdCBhcyBpcy5cclxuICAgIGlmIChBcnJheS5pc0FycmF5KHByb3ZpZGVkKSAmJiAhZGVmaW5lZCkge1xyXG4gICAgICAgIHJldHVybiBwcm92aWRlZDtcclxuICAgIH1cclxuICAgIHZhciBkZWZpbmVkUnVsZXM7XHJcbiAgICAvLyBjb2xsZWN0IHRoZSBwYXJhbXMgc2NoZW1hLlxyXG4gICAgaWYgKCFkZWZpbmVkIHx8IChkZWZpbmVkLmxlbmd0aCA8IHByb3ZpZGVkLmxlbmd0aCAmJiBBcnJheS5pc0FycmF5KHByb3ZpZGVkKSkpIHtcclxuICAgICAgICB2YXIgbGFzdERlZmluZWRQYXJhbV8xO1xyXG4gICAgICAgIC8vIGNvbGxlY3QgYW55IGFkZGl0aW9uYWwgcGFyYW1ldGVycyBpbiB0aGUgbGFzdCBpdGVtLlxyXG4gICAgICAgIGRlZmluZWRSdWxlcyA9IHByb3ZpZGVkLm1hcChmdW5jdGlvbiAoXywgaWR4KSB7XHJcbiAgICAgICAgICAgIHZhciBwYXJhbSA9IGRlZmluZWQgJiYgZGVmaW5lZFtpZHhdO1xyXG4gICAgICAgICAgICBsYXN0RGVmaW5lZFBhcmFtXzEgPSBwYXJhbSB8fCBsYXN0RGVmaW5lZFBhcmFtXzE7XHJcbiAgICAgICAgICAgIGlmICghcGFyYW0pIHtcclxuICAgICAgICAgICAgICAgIHBhcmFtID0gbGFzdERlZmluZWRQYXJhbV8xO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybiBwYXJhbTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuICAgIGVsc2Uge1xyXG4gICAgICAgIGRlZmluZWRSdWxlcyA9IGRlZmluZWQ7XHJcbiAgICB9XHJcbiAgICAvLyBNYXRjaCB0aGUgcHJvdmlkZWQgYXJyYXkgbGVuZ3RoIHdpdGggYSB0ZW1wb3Jhcnkgc2NoZW1hLlxyXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBkZWZpbmVkUnVsZXMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICB2YXIgb3B0aW9ucyA9IGRlZmluZWRSdWxlc1tpXTtcclxuICAgICAgICB2YXIgdmFsdWUgPSBvcHRpb25zLmRlZmF1bHQ7XHJcbiAgICAgICAgLy8gaWYgdGhlIHByb3ZpZGVkIGlzIGFuIGFycmF5LCBtYXAgZWxlbWVudCB2YWx1ZS5cclxuICAgICAgICBpZiAoQXJyYXkuaXNBcnJheShwcm92aWRlZCkpIHtcclxuICAgICAgICAgICAgaWYgKGkgaW4gcHJvdmlkZWQpIHtcclxuICAgICAgICAgICAgICAgIHZhbHVlID0gcHJvdmlkZWRbaV07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgIC8vIElmIHRoZSBwYXJhbSBleGlzdHMgaW4gdGhlIHByb3ZpZGVkIG9iamVjdC5cclxuICAgICAgICAgICAgaWYgKG9wdGlvbnMubmFtZSBpbiBwcm92aWRlZCkge1xyXG4gICAgICAgICAgICAgICAgdmFsdWUgPSBwcm92aWRlZFtvcHRpb25zLm5hbWVdO1xyXG4gICAgICAgICAgICAgICAgLy8gaWYgdGhlIHByb3ZpZGVkIGlzIHRoZSBmaXJzdCBwYXJhbSB2YWx1ZS5cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBlbHNlIGlmIChkZWZpbmVkUnVsZXMubGVuZ3RoID09PSAxKSB7XHJcbiAgICAgICAgICAgICAgICB2YWx1ZSA9IHByb3ZpZGVkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vIGlmIHRoZSBwYXJhbSBpcyBhIHRhcmdldCwgcmVzb2x2ZSB0aGUgdGFyZ2V0IHZhbHVlLlxyXG4gICAgICAgIGlmIChvcHRpb25zLmlzVGFyZ2V0KSB7XHJcbiAgICAgICAgICAgIHZhbHVlID0gY3Jvc3NUYWJsZVt2YWx1ZV07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vIElmIHRoZXJlIGlzIGEgdHJhbnNmb3JtZXIgZGVmaW5lZC5cclxuICAgICAgICBpZiAob3B0aW9ucy5jYXN0KSB7XHJcbiAgICAgICAgICAgIHZhbHVlID0gb3B0aW9ucy5jYXN0KHZhbHVlKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLy8gYWxyZWFkeSBiZWVuIHNldCwgcHJvYmFibHkgbXVsdGlwbGUgdmFsdWVzLlxyXG4gICAgICAgIGlmIChwYXJhbXNbb3B0aW9ucy5uYW1lXSkge1xyXG4gICAgICAgICAgICBwYXJhbXNbb3B0aW9ucy5uYW1lXSA9IEFycmF5LmlzQXJyYXkocGFyYW1zW29wdGlvbnMubmFtZV0pID8gcGFyYW1zW29wdGlvbnMubmFtZV0gOiBbcGFyYW1zW29wdGlvbnMubmFtZV1dO1xyXG4gICAgICAgICAgICBwYXJhbXNbb3B0aW9ucy5uYW1lXS5wdXNoKHZhbHVlKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgIC8vIHNldCB0aGUgdmFsdWUuXHJcbiAgICAgICAgICAgIHBhcmFtc1tvcHRpb25zLm5hbWVdID0gdmFsdWU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgcmV0dXJuIHBhcmFtcztcclxufVxuXG5mdW5jdGlvbiBpbnN0YWxsKF8sIGNvbmZpZykge1xyXG4gICAgc2V0Q29uZmlnKGNvbmZpZyk7XHJcbn1cblxudmFyIGFnZ3Jlc3NpdmUgPSBmdW5jdGlvbiAoKSB7IHJldHVybiAoe1xyXG4gICAgb246IFsnaW5wdXQnLCAnYmx1ciddXHJcbn0pOyB9O1xyXG52YXIgbGF6eSA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuICh7XHJcbiAgICBvbjogWydjaGFuZ2UnXVxyXG59KTsgfTtcclxudmFyIGVhZ2VyID0gZnVuY3Rpb24gKF9hKSB7XHJcbiAgICB2YXIgZXJyb3JzID0gX2EuZXJyb3JzO1xyXG4gICAgaWYgKGVycm9ycy5sZW5ndGgpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBvbjogWydpbnB1dCcsICdjaGFuZ2UnXVxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICAgIG9uOiBbJ2NoYW5nZScsICdibHVyJ11cclxuICAgIH07XHJcbn07XHJcbnZhciBwYXNzaXZlID0gZnVuY3Rpb24gKCkgeyByZXR1cm4gKHtcclxuICAgIG9uOiBbXVxyXG59KTsgfTtcclxudmFyIG1vZGVzID0ge1xyXG4gICAgYWdncmVzc2l2ZTogYWdncmVzc2l2ZSxcclxuICAgIGVhZ2VyOiBlYWdlcixcclxuICAgIHBhc3NpdmU6IHBhc3NpdmUsXHJcbiAgICBsYXp5OiBsYXp5XHJcbn07XHJcbnZhciBzZXRJbnRlcmFjdGlvbk1vZGUgPSBmdW5jdGlvbiAobW9kZSwgaW1wbGVtZW50YXRpb24pIHtcclxuICAgIHNldENvbmZpZyh7IG1vZGU6IG1vZGUgfSk7XHJcbiAgICBpZiAoIWltcGxlbWVudGF0aW9uKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgaWYgKCFpc0NhbGxhYmxlKGltcGxlbWVudGF0aW9uKSkge1xyXG4gICAgICAgIHRocm93IG5ldyBFcnJvcignQSBtb2RlIGltcGxlbWVudGF0aW9uIG11c3QgYmUgYSBmdW5jdGlvbicpO1xyXG4gICAgfVxyXG4gICAgbW9kZXNbbW9kZV0gPSBpbXBsZW1lbnRhdGlvbjtcclxufTtcblxudmFyIERpY3Rpb25hcnkgPSAvKiogQGNsYXNzICovIChmdW5jdGlvbiAoKSB7XHJcbiAgICBmdW5jdGlvbiBEaWN0aW9uYXJ5KGxvY2FsZSwgZGljdGlvbmFyeSkge1xyXG4gICAgICAgIHRoaXMuY29udGFpbmVyID0ge307XHJcbiAgICAgICAgdGhpcy5sb2NhbGUgPSBsb2NhbGU7XHJcbiAgICAgICAgdGhpcy5tZXJnZShkaWN0aW9uYXJ5KTtcclxuICAgIH1cclxuICAgIERpY3Rpb25hcnkucHJvdG90eXBlLnJlc29sdmUgPSBmdW5jdGlvbiAoZmllbGQsIHJ1bGUsIHZhbHVlcykge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmZvcm1hdCh0aGlzLmxvY2FsZSwgZmllbGQsIHJ1bGUsIHZhbHVlcyk7XHJcbiAgICB9O1xyXG4gICAgRGljdGlvbmFyeS5wcm90b3R5cGUuX2hhc0xvY2FsZSA9IGZ1bmN0aW9uIChsb2NhbGUpIHtcclxuICAgICAgICByZXR1cm4gISF0aGlzLmNvbnRhaW5lcltsb2NhbGVdO1xyXG4gICAgfTtcclxuICAgIERpY3Rpb25hcnkucHJvdG90eXBlLmZvcm1hdCA9IGZ1bmN0aW9uIChsb2NhbGUsIGZpZWxkLCBydWxlLCB2YWx1ZXMpIHtcclxuICAgICAgICB2YXIgbWVzc2FnZTtcclxuICAgICAgICAvLyBmaW5kIGlmIHNwZWNpZmljIG1lc3NhZ2UgZm9yIHRoYXQgZmllbGQgd2FzIHNwZWNpZmllZC5cclxuICAgICAgICB2YXIgZGljdCA9IHRoaXMuY29udGFpbmVyW2xvY2FsZV0gJiYgdGhpcy5jb250YWluZXJbbG9jYWxlXS5maWVsZHMgJiYgdGhpcy5jb250YWluZXJbbG9jYWxlXS5maWVsZHNbZmllbGRdO1xyXG4gICAgICAgIGlmIChkaWN0ICYmIGRpY3RbcnVsZV0pIHtcclxuICAgICAgICAgICAgbWVzc2FnZSA9IGRpY3RbcnVsZV07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICghbWVzc2FnZSAmJiB0aGlzLl9oYXNMb2NhbGUobG9jYWxlKSAmJiB0aGlzLl9oYXNNZXNzYWdlKGxvY2FsZSwgcnVsZSkpIHtcclxuICAgICAgICAgICAgbWVzc2FnZSA9IHRoaXMuY29udGFpbmVyW2xvY2FsZV0ubWVzc2FnZXNbcnVsZV07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICghbWVzc2FnZSkge1xyXG4gICAgICAgICAgICBtZXNzYWdlID0gZ2V0Q29uZmlnKCkuZGVmYXVsdE1lc3NhZ2U7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLl9oYXNOYW1lKGxvY2FsZSwgZmllbGQpKSB7XHJcbiAgICAgICAgICAgIGZpZWxkID0gdGhpcy5nZXROYW1lKGxvY2FsZSwgZmllbGQpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gaXNDYWxsYWJsZShtZXNzYWdlKSA/IG1lc3NhZ2UoZmllbGQsIHZhbHVlcykgOiBpbnRlcnBvbGF0ZShtZXNzYWdlLCBfX2Fzc2lnbihfX2Fzc2lnbih7fSwgdmFsdWVzKSwgeyBfZmllbGRfOiBmaWVsZCB9KSk7XHJcbiAgICB9O1xyXG4gICAgRGljdGlvbmFyeS5wcm90b3R5cGUubWVyZ2UgPSBmdW5jdGlvbiAoZGljdGlvbmFyeSkge1xyXG4gICAgICAgIG1lcmdlKHRoaXMuY29udGFpbmVyLCBkaWN0aW9uYXJ5KTtcclxuICAgIH07XHJcbiAgICBEaWN0aW9uYXJ5LnByb3RvdHlwZS5oYXNSdWxlID0gZnVuY3Rpb24gKG5hbWUpIHtcclxuICAgICAgICB2YXIgbG9jYWxlID0gdGhpcy5jb250YWluZXJbdGhpcy5sb2NhbGVdO1xyXG4gICAgICAgIGlmICghbG9jYWxlKVxyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgcmV0dXJuICEhKGxvY2FsZS5tZXNzYWdlcyAmJiBsb2NhbGUubWVzc2FnZXNbbmFtZV0pO1xyXG4gICAgfTtcclxuICAgIERpY3Rpb25hcnkucHJvdG90eXBlLmdldE5hbWUgPSBmdW5jdGlvbiAobG9jYWxlLCBrZXkpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5jb250YWluZXJbbG9jYWxlXS5uYW1lc1trZXldO1xyXG4gICAgfTtcclxuICAgIERpY3Rpb25hcnkucHJvdG90eXBlLl9oYXNNZXNzYWdlID0gZnVuY3Rpb24gKGxvY2FsZSwga2V5KSB7XHJcbiAgICAgICAgcmV0dXJuICEhKHRoaXMuX2hhc0xvY2FsZShsb2NhbGUpICYmIHRoaXMuY29udGFpbmVyW2xvY2FsZV0ubWVzc2FnZXMgJiYgdGhpcy5jb250YWluZXJbbG9jYWxlXS5tZXNzYWdlc1trZXldKTtcclxuICAgIH07XHJcbiAgICBEaWN0aW9uYXJ5LnByb3RvdHlwZS5faGFzTmFtZSA9IGZ1bmN0aW9uIChsb2NhbGUsIGtleSkge1xyXG4gICAgICAgIHJldHVybiAhISh0aGlzLl9oYXNMb2NhbGUobG9jYWxlKSAmJiB0aGlzLmNvbnRhaW5lcltsb2NhbGVdLm5hbWVzICYmIHRoaXMuY29udGFpbmVyW2xvY2FsZV0ubmFtZXNba2V5XSk7XHJcbiAgICB9O1xyXG4gICAgcmV0dXJuIERpY3Rpb25hcnk7XHJcbn0oKSk7XHJcbnZhciBESUNUSU9OQVJZO1xyXG52YXIgSU5TVEFMTEVEID0gZmFsc2U7XHJcbmZ1bmN0aW9uIHVwZGF0ZVJ1bGVzKCkge1xyXG4gICAgaWYgKElOU1RBTExFRCkge1xyXG4gICAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIFJ1bGVDb250YWluZXIuaXRlcmF0ZShmdW5jdGlvbiAobmFtZSwgc2NoZW1hKSB7XHJcbiAgICAgICAgdmFyIF9hLCBfYjtcclxuICAgICAgICBpZiAoc2NoZW1hLm1lc3NhZ2UgJiYgIURJQ1RJT05BUlkuaGFzUnVsZShuYW1lKSkge1xyXG4gICAgICAgICAgICBESUNUSU9OQVJZLm1lcmdlKChfYSA9IHt9LFxyXG4gICAgICAgICAgICAgICAgX2FbRElDVElPTkFSWS5sb2NhbGVdID0ge1xyXG4gICAgICAgICAgICAgICAgICAgIG1lc3NhZ2VzOiAoX2IgPSB7fSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgX2JbbmFtZV0gPSBzY2hlbWEubWVzc2FnZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgX2IpXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgX2EpKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZXh0ZW5kKG5hbWUsIHtcclxuICAgICAgICAgICAgbWVzc2FnZTogZnVuY3Rpb24gKGZpZWxkLCB2YWx1ZXMpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBESUNUSU9OQVJZLnJlc29sdmUoZmllbGQsIG5hbWUsIHZhbHVlcyB8fCB7fSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH0pO1xyXG4gICAgSU5TVEFMTEVEID0gdHJ1ZTtcclxufVxyXG5mdW5jdGlvbiBsb2NhbGl6ZShsb2NhbGUsIGRpY3Rpb25hcnkpIHtcclxuICAgIHZhciBfYTtcclxuICAgIGlmICghRElDVElPTkFSWSkge1xyXG4gICAgICAgIERJQ1RJT05BUlkgPSBuZXcgRGljdGlvbmFyeSgnZW4nLCB7fSk7XHJcbiAgICB9XHJcbiAgICBpZiAodHlwZW9mIGxvY2FsZSA9PT0gJ3N0cmluZycpIHtcclxuICAgICAgICBESUNUSU9OQVJZLmxvY2FsZSA9IGxvY2FsZTtcclxuICAgICAgICBpZiAoZGljdGlvbmFyeSkge1xyXG4gICAgICAgICAgICBESUNUSU9OQVJZLm1lcmdlKChfYSA9IHt9LCBfYVtsb2NhbGVdID0gZGljdGlvbmFyeSwgX2EpKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdXBkYXRlUnVsZXMoKTtcclxuICAgICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICBESUNUSU9OQVJZLm1lcmdlKGxvY2FsZSk7XHJcbiAgICB1cGRhdGVSdWxlcygpO1xyXG59XG5cbnZhciBpc0V2ZW50ID0gZnVuY3Rpb24gKGV2dCkge1xyXG4gICAgaWYgKCFldnQpIHtcclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcbiAgICBpZiAodHlwZW9mIEV2ZW50ICE9PSAndW5kZWZpbmVkJyAmJiBpc0NhbGxhYmxlKEV2ZW50KSAmJiBldnQgaW5zdGFuY2VvZiBFdmVudCkge1xyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG4gICAgLy8gdGhpcyBpcyBmb3IgSUVcclxuICAgIC8qIGlzdGFuYnVsIGlnbm9yZSBuZXh0ICovXHJcbiAgICBpZiAoZXZ0ICYmIGV2dC5zcmNFbGVtZW50KSB7XHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gZmFsc2U7XHJcbn07XHJcbmZ1bmN0aW9uIG5vcm1hbGl6ZUV2ZW50VmFsdWUodmFsdWUpIHtcclxuICAgIGlmICghaXNFdmVudCh2YWx1ZSkpIHtcclxuICAgICAgICByZXR1cm4gdmFsdWU7XHJcbiAgICB9XHJcbiAgICB2YXIgaW5wdXQgPSB2YWx1ZS50YXJnZXQ7XHJcbiAgICBpZiAoaW5wdXQudHlwZSA9PT0gJ2ZpbGUnICYmIGlucHV0LmZpbGVzKSB7XHJcbiAgICAgICAgcmV0dXJuIHRvQXJyYXkoaW5wdXQuZmlsZXMpO1xyXG4gICAgfVxyXG4gICAgLy8gSWYgdGhlIGlucHV0IGhhcyBhIGB2LW1vZGVsLm51bWJlcmAgbW9kaWZpZXIgYXBwbGllZC5cclxuICAgIGlmIChpbnB1dC5fdk1vZGlmaWVycyAmJiBpbnB1dC5fdk1vZGlmaWVycy5udW1iZXIpIHtcclxuICAgICAgICAvLyBhcyBwZXIgdGhlIHNwZWMgdGhlIHYtbW9kZWwubnVtYmVyIHVzZXMgcGFyc2VGbG9hdFxyXG4gICAgICAgIHZhciB2YWx1ZUFzTnVtYmVyID0gcGFyc2VGbG9hdChpbnB1dC52YWx1ZSk7XHJcbiAgICAgICAgaWYgKGlzTmFOKHZhbHVlQXNOdW1iZXIpKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBpbnB1dC52YWx1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHZhbHVlQXNOdW1iZXI7XHJcbiAgICB9XHJcbiAgICBpZiAoaW5wdXQuX3ZNb2RpZmllcnMgJiYgaW5wdXQuX3ZNb2RpZmllcnMudHJpbSkge1xyXG4gICAgICAgIHZhciB0cmltbWVkVmFsdWUgPSB0eXBlb2YgaW5wdXQudmFsdWUgPT09ICdzdHJpbmcnID8gaW5wdXQudmFsdWUudHJpbSgpIDogaW5wdXQudmFsdWU7XHJcbiAgICAgICAgcmV0dXJuIHRyaW1tZWRWYWx1ZTtcclxuICAgIH1cclxuICAgIHJldHVybiBpbnB1dC52YWx1ZTtcclxufVxuXG52YXIgaXNUZXh0SW5wdXQgPSBmdW5jdGlvbiAodm5vZGUpIHtcclxuICAgIHZhciBhdHRycyA9ICh2bm9kZS5kYXRhICYmIHZub2RlLmRhdGEuYXR0cnMpIHx8IHZub2RlLmVsbTtcclxuICAgIC8vIGl0IHdpbGwgZmFsbGJhY2sgdG8gYmVpbmcgYSB0ZXh0IGlucHV0IHBlciBicm93c2VycyBzcGVjLlxyXG4gICAgaWYgKHZub2RlLnRhZyA9PT0gJ2lucHV0JyAmJiAoIWF0dHJzIHx8ICFhdHRycy50eXBlKSkge1xyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG4gICAgaWYgKHZub2RlLnRhZyA9PT0gJ3RleHRhcmVhJykge1xyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIGluY2x1ZGVzKFsndGV4dCcsICdwYXNzd29yZCcsICdzZWFyY2gnLCAnZW1haWwnLCAndGVsJywgJ3VybCcsICdudW1iZXInXSwgYXR0cnMgJiYgYXR0cnMudHlwZSk7XHJcbn07XHJcbi8vIGV4cG9ydCBjb25zdCBpc0NoZWNrYm94T3JSYWRpb0lucHV0ID0gKHZub2RlOiBWTm9kZSk6IGJvb2xlYW4gPT4ge1xyXG4vLyAgIGNvbnN0IGF0dHJzID0gKHZub2RlLmRhdGEgJiYgdm5vZGUuZGF0YS5hdHRycykgfHwgdm5vZGUuZWxtO1xyXG4vLyAgIHJldHVybiBpbmNsdWRlcyhbJ3JhZGlvJywgJ2NoZWNrYm94J10sIGF0dHJzICYmIGF0dHJzLnR5cGUpO1xyXG4vLyB9O1xyXG4vLyBHZXRzIHRoZSBtb2RlbCBvYmplY3Qgb24gdGhlIHZub2RlLlxyXG5mdW5jdGlvbiBmaW5kTW9kZWwodm5vZGUpIHtcclxuICAgIGlmICghdm5vZGUuZGF0YSkge1xyXG4gICAgICAgIHJldHVybiB1bmRlZmluZWQ7XHJcbiAgICB9XHJcbiAgICAvLyBDb21wb25lbnQgTW9kZWxcclxuICAgIC8vIFRISVMgSVMgTk9UIFRZUEVEIElOIE9GRklDSUFMIFZVRSBUWVBJTkdTXHJcbiAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmVcclxuICAgIHZhciBub25TdGFuZGFyZFZOb2RlRGF0YSA9IHZub2RlLmRhdGE7XHJcbiAgICBpZiAoJ21vZGVsJyBpbiBub25TdGFuZGFyZFZOb2RlRGF0YSkge1xyXG4gICAgICAgIHJldHVybiBub25TdGFuZGFyZFZOb2RlRGF0YS5tb2RlbDtcclxuICAgIH1cclxuICAgIGlmICghdm5vZGUuZGF0YS5kaXJlY3RpdmVzKSB7XHJcbiAgICAgICAgcmV0dXJuIHVuZGVmaW5lZDtcclxuICAgIH1cclxuICAgIHJldHVybiBmaW5kKHZub2RlLmRhdGEuZGlyZWN0aXZlcywgZnVuY3Rpb24gKGQpIHsgcmV0dXJuIGQubmFtZSA9PT0gJ21vZGVsJzsgfSk7XHJcbn1cclxuZnVuY3Rpb24gZmluZFZhbHVlKHZub2RlKSB7XHJcbiAgICB2YXIgbW9kZWwgPSBmaW5kTW9kZWwodm5vZGUpO1xyXG4gICAgaWYgKG1vZGVsKSB7XHJcbiAgICAgICAgcmV0dXJuIHsgdmFsdWU6IG1vZGVsLnZhbHVlIH07XHJcbiAgICB9XHJcbiAgICB2YXIgY29uZmlnID0gZmluZE1vZGVsQ29uZmlnKHZub2RlKTtcclxuICAgIHZhciBwcm9wID0gKGNvbmZpZyAmJiBjb25maWcucHJvcCkgfHwgJ3ZhbHVlJztcclxuICAgIGlmICh2bm9kZS5jb21wb25lbnRPcHRpb25zICYmIHZub2RlLmNvbXBvbmVudE9wdGlvbnMucHJvcHNEYXRhICYmIHByb3AgaW4gdm5vZGUuY29tcG9uZW50T3B0aW9ucy5wcm9wc0RhdGEpIHtcclxuICAgICAgICB2YXIgcHJvcHNEYXRhV2l0aFZhbHVlID0gdm5vZGUuY29tcG9uZW50T3B0aW9ucy5wcm9wc0RhdGE7XHJcbiAgICAgICAgcmV0dXJuIHsgdmFsdWU6IHByb3BzRGF0YVdpdGhWYWx1ZVtwcm9wXSB9O1xyXG4gICAgfVxyXG4gICAgaWYgKHZub2RlLmRhdGEgJiYgdm5vZGUuZGF0YS5kb21Qcm9wcyAmJiAndmFsdWUnIGluIHZub2RlLmRhdGEuZG9tUHJvcHMpIHtcclxuICAgICAgICByZXR1cm4geyB2YWx1ZTogdm5vZGUuZGF0YS5kb21Qcm9wcy52YWx1ZSB9O1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHVuZGVmaW5lZDtcclxufVxyXG5mdW5jdGlvbiBleHRyYWN0Q2hpbGRyZW4odm5vZGUpIHtcclxuICAgIGlmIChBcnJheS5pc0FycmF5KHZub2RlKSkge1xyXG4gICAgICAgIHJldHVybiB2bm9kZTtcclxuICAgIH1cclxuICAgIGlmIChBcnJheS5pc0FycmF5KHZub2RlLmNoaWxkcmVuKSkge1xyXG4gICAgICAgIHJldHVybiB2bm9kZS5jaGlsZHJlbjtcclxuICAgIH1cclxuICAgIC8qIGlzdGFuYnVsIGlnbm9yZSBuZXh0ICovXHJcbiAgICBpZiAodm5vZGUuY29tcG9uZW50T3B0aW9ucyAmJiBBcnJheS5pc0FycmF5KHZub2RlLmNvbXBvbmVudE9wdGlvbnMuY2hpbGRyZW4pKSB7XHJcbiAgICAgICAgcmV0dXJuIHZub2RlLmNvbXBvbmVudE9wdGlvbnMuY2hpbGRyZW47XHJcbiAgICB9XHJcbiAgICByZXR1cm4gW107XHJcbn1cclxuZnVuY3Rpb24gZXh0cmFjdFZOb2Rlcyh2bm9kZSkge1xyXG4gICAgaWYgKCFBcnJheS5pc0FycmF5KHZub2RlKSAmJiBmaW5kVmFsdWUodm5vZGUpICE9PSB1bmRlZmluZWQpIHtcclxuICAgICAgICByZXR1cm4gW3Zub2RlXTtcclxuICAgIH1cclxuICAgIHZhciBjaGlsZHJlbiA9IGV4dHJhY3RDaGlsZHJlbih2bm9kZSk7XHJcbiAgICByZXR1cm4gY2hpbGRyZW4ucmVkdWNlKGZ1bmN0aW9uIChub2Rlcywgbm9kZSkge1xyXG4gICAgICAgIHZhciBjYW5kaWRhdGVzID0gZXh0cmFjdFZOb2Rlcyhub2RlKTtcclxuICAgICAgICBpZiAoY2FuZGlkYXRlcy5sZW5ndGgpIHtcclxuICAgICAgICAgICAgbm9kZXMucHVzaC5hcHBseShub2RlcywgY2FuZGlkYXRlcyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBub2RlcztcclxuICAgIH0sIFtdKTtcclxufVxyXG4vLyBSZXNvbHZlcyB2LW1vZGVsIGNvbmZpZyBpZiBleGlzdHMuXHJcbmZ1bmN0aW9uIGZpbmRNb2RlbENvbmZpZyh2bm9kZSkge1xyXG4gICAgLyogaXN0YW5idWwgaWdub3JlIG5leHQgKi9cclxuICAgIGlmICghdm5vZGUuY29tcG9uZW50T3B0aW9ucylcclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgIC8vIFRoaXMgaXMgYWxzbyBub3QgdHlwZWQgaW4gdGhlIHN0YW5kYXJkIFZ1ZSBUUy5cclxuICAgIHJldHVybiB2bm9kZS5jb21wb25lbnRPcHRpb25zLkN0b3Iub3B0aW9ucy5tb2RlbDtcclxufVxyXG4vLyBBZGRzIGEgbGlzdGVuZXIgdG8gdm5vZGUgbGlzdGVuZXIgb2JqZWN0LlxyXG5mdW5jdGlvbiBtZXJnZVZOb2RlTGlzdGVuZXJzKG9iaiwgZXZlbnROYW1lLCBoYW5kbGVyKSB7XHJcbiAgICAvLyBubyBsaXN0ZW5lciBhdCBhbGwuXHJcbiAgICBpZiAoaXNOdWxsT3JVbmRlZmluZWQob2JqW2V2ZW50TmFtZV0pKSB7XHJcbiAgICAgICAgb2JqW2V2ZW50TmFtZV0gPSBbaGFuZGxlcl07XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgLy8gSXMgYW4gaW52b2tlci5cclxuICAgIGlmIChpc0NhbGxhYmxlKG9ialtldmVudE5hbWVdKSAmJiBvYmpbZXZlbnROYW1lXS5mbnMpIHtcclxuICAgICAgICB2YXIgaW52b2tlciA9IG9ialtldmVudE5hbWVdO1xyXG4gICAgICAgIGludm9rZXIuZm5zID0gQXJyYXkuaXNBcnJheShpbnZva2VyLmZucykgPyBpbnZva2VyLmZucyA6IFtpbnZva2VyLmZuc107XHJcbiAgICAgICAgaWYgKCFpbmNsdWRlcyhpbnZva2VyLmZucywgaGFuZGxlcikpIHtcclxuICAgICAgICAgICAgaW52b2tlci5mbnMucHVzaChoYW5kbGVyKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgaWYgKGlzQ2FsbGFibGUob2JqW2V2ZW50TmFtZV0pKSB7XHJcbiAgICAgICAgdmFyIHByZXYgPSBvYmpbZXZlbnROYW1lXTtcclxuICAgICAgICBvYmpbZXZlbnROYW1lXSA9IFtwcmV2XTtcclxuICAgIH1cclxuICAgIGlmIChBcnJheS5pc0FycmF5KG9ialtldmVudE5hbWVdKSAmJiAhaW5jbHVkZXMob2JqW2V2ZW50TmFtZV0sIGhhbmRsZXIpKSB7XHJcbiAgICAgICAgb2JqW2V2ZW50TmFtZV0ucHVzaChoYW5kbGVyKTtcclxuICAgIH1cclxufVxyXG4vLyBBZGRzIGEgbGlzdGVuZXIgdG8gYSBuYXRpdmUgSFRNTCB2bm9kZS5cclxuZnVuY3Rpb24gYWRkTmF0aXZlTm9kZUxpc3RlbmVyKG5vZGUsIGV2ZW50TmFtZSwgaGFuZGxlcikge1xyXG4gICAgLyogaXN0YW5idWwgaWdub3JlIG5leHQgKi9cclxuICAgIGlmICghbm9kZS5kYXRhKSB7XHJcbiAgICAgICAgbm9kZS5kYXRhID0ge307XHJcbiAgICB9XHJcbiAgICBpZiAoaXNOdWxsT3JVbmRlZmluZWQobm9kZS5kYXRhLm9uKSkge1xyXG4gICAgICAgIG5vZGUuZGF0YS5vbiA9IHt9O1xyXG4gICAgfVxyXG4gICAgbWVyZ2VWTm9kZUxpc3RlbmVycyhub2RlLmRhdGEub24sIGV2ZW50TmFtZSwgaGFuZGxlcik7XHJcbn1cclxuLy8gQWRkcyBhIGxpc3RlbmVyIHRvIGEgVnVlIGNvbXBvbmVudCB2bm9kZS5cclxuZnVuY3Rpb24gYWRkQ29tcG9uZW50Tm9kZUxpc3RlbmVyKG5vZGUsIGV2ZW50TmFtZSwgaGFuZGxlcikge1xyXG4gICAgLyogaXN0YW5idWwgaWdub3JlIG5leHQgKi9cclxuICAgIGlmICghbm9kZS5jb21wb25lbnRPcHRpb25zKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgLyogaXN0YW5idWwgaWdub3JlIG5leHQgKi9cclxuICAgIGlmICghbm9kZS5jb21wb25lbnRPcHRpb25zLmxpc3RlbmVycykge1xyXG4gICAgICAgIG5vZGUuY29tcG9uZW50T3B0aW9ucy5saXN0ZW5lcnMgPSB7fTtcclxuICAgIH1cclxuICAgIG1lcmdlVk5vZGVMaXN0ZW5lcnMobm9kZS5jb21wb25lbnRPcHRpb25zLmxpc3RlbmVycywgZXZlbnROYW1lLCBoYW5kbGVyKTtcclxufVxyXG5mdW5jdGlvbiBhZGRWTm9kZUxpc3RlbmVyKHZub2RlLCBldmVudE5hbWUsIGhhbmRsZXIpIHtcclxuICAgIGlmICh2bm9kZS5jb21wb25lbnRPcHRpb25zKSB7XHJcbiAgICAgICAgYWRkQ29tcG9uZW50Tm9kZUxpc3RlbmVyKHZub2RlLCBldmVudE5hbWUsIGhhbmRsZXIpO1xyXG4gICAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIGFkZE5hdGl2ZU5vZGVMaXN0ZW5lcih2bm9kZSwgZXZlbnROYW1lLCBoYW5kbGVyKTtcclxufVxyXG4vLyBEZXRlcm1pbmVzIGlmIGBjaGFuZ2VgIHNob3VsZCBiZSB1c2VkIG92ZXIgYGlucHV0YCBmb3IgbGlzdGVuZXJzLlxyXG5mdW5jdGlvbiBnZXRJbnB1dEV2ZW50TmFtZSh2bm9kZSwgbW9kZWwpIHtcclxuICAgIC8vIElzIGEgY29tcG9uZW50LlxyXG4gICAgaWYgKHZub2RlLmNvbXBvbmVudE9wdGlvbnMpIHtcclxuICAgICAgICB2YXIgZXZlbnRfMSA9IChmaW5kTW9kZWxDb25maWcodm5vZGUpIHx8IHsgZXZlbnQ6ICdpbnB1dCcgfSkuZXZlbnQ7XHJcbiAgICAgICAgcmV0dXJuIGV2ZW50XzE7XHJcbiAgICB9XHJcbiAgICAvLyBMYXp5IE1vZGVscyB0eXBpY2FsbHkgdXNlIGNoYW5nZSBldmVudFxyXG4gICAgaWYgKG1vZGVsICYmIG1vZGVsLm1vZGlmaWVycyAmJiBtb2RlbC5tb2RpZmllcnMubGF6eSkge1xyXG4gICAgICAgIHJldHVybiAnY2hhbmdlJztcclxuICAgIH1cclxuICAgIC8vIGlzIGEgdGV4dHVhbC10eXBlIGlucHV0LlxyXG4gICAgaWYgKGlzVGV4dElucHV0KHZub2RlKSkge1xyXG4gICAgICAgIHJldHVybiAnaW5wdXQnO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuICdjaGFuZ2UnO1xyXG59XHJcbi8vIFRPRE86IFR5cGUgdGhpcyBvbmUgcHJvcGVybHkuXHJcbmZ1bmN0aW9uIG5vcm1hbGl6ZVNsb3RzKHNsb3RzLCBjdHgpIHtcclxuICAgIHZhciBhY2MgPSBbXTtcclxuICAgIHJldHVybiBPYmplY3Qua2V5cyhzbG90cykucmVkdWNlKGZ1bmN0aW9uIChhcnIsIGtleSkge1xyXG4gICAgICAgIHNsb3RzW2tleV0uZm9yRWFjaChmdW5jdGlvbiAodm5vZGUpIHtcclxuICAgICAgICAgICAgaWYgKCF2bm9kZS5jb250ZXh0KSB7XHJcbiAgICAgICAgICAgICAgICBzbG90c1trZXldLmNvbnRleHQgPSBjdHg7XHJcbiAgICAgICAgICAgICAgICBpZiAoIXZub2RlLmRhdGEpIHtcclxuICAgICAgICAgICAgICAgICAgICB2bm9kZS5kYXRhID0ge307XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB2bm9kZS5kYXRhLnNsb3QgPSBrZXk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICByZXR1cm4gYXJyLmNvbmNhdChzbG90c1trZXldKTtcclxuICAgIH0sIGFjYyk7XHJcbn1cclxuZnVuY3Rpb24gcmVzb2x2ZVRleHR1YWxSdWxlcyh2bm9kZSkge1xyXG4gICAgdmFyIGF0dHJzID0gdm5vZGUuZGF0YSAmJiB2bm9kZS5kYXRhLmF0dHJzO1xyXG4gICAgdmFyIHJ1bGVzID0ge307XHJcbiAgICBpZiAoIWF0dHJzKVxyXG4gICAgICAgIHJldHVybiBydWxlcztcclxuICAgIGlmIChhdHRycy50eXBlID09PSAnZW1haWwnKSB7XHJcbiAgICAgICAgcnVsZXMuZW1haWwgPSBbJ211bHRpcGxlJyBpbiBhdHRyc107XHJcbiAgICB9XHJcbiAgICBpZiAoYXR0cnMucGF0dGVybikge1xyXG4gICAgICAgIHJ1bGVzLnJlZ2V4ID0gYXR0cnMucGF0dGVybjtcclxuICAgIH1cclxuICAgIGlmIChhdHRycy5tYXhsZW5ndGggPj0gMCkge1xyXG4gICAgICAgIHJ1bGVzLm1heCA9IGF0dHJzLm1heGxlbmd0aDtcclxuICAgIH1cclxuICAgIGlmIChhdHRycy5taW5sZW5ndGggPj0gMCkge1xyXG4gICAgICAgIHJ1bGVzLm1pbiA9IGF0dHJzLm1pbmxlbmd0aDtcclxuICAgIH1cclxuICAgIGlmIChhdHRycy50eXBlID09PSAnbnVtYmVyJykge1xyXG4gICAgICAgIGlmIChpc1NwZWNpZmllZChhdHRycy5taW4pKSB7XHJcbiAgICAgICAgICAgIHJ1bGVzLm1pbl92YWx1ZSA9IE51bWJlcihhdHRycy5taW4pO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoaXNTcGVjaWZpZWQoYXR0cnMubWF4KSkge1xyXG4gICAgICAgICAgICBydWxlcy5tYXhfdmFsdWUgPSBOdW1iZXIoYXR0cnMubWF4KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICByZXR1cm4gcnVsZXM7XHJcbn1cclxuZnVuY3Rpb24gcmVzb2x2ZVJ1bGVzKHZub2RlKSB7XHJcbiAgICB2YXIgaHRtbFRhZ3MgPSBbJ2lucHV0JywgJ3NlbGVjdCddO1xyXG4gICAgdmFyIGF0dHJzID0gdm5vZGUuZGF0YSAmJiB2bm9kZS5kYXRhLmF0dHJzO1xyXG4gICAgaWYgKCFpbmNsdWRlcyhodG1sVGFncywgdm5vZGUudGFnKSB8fCAhYXR0cnMpIHtcclxuICAgICAgICByZXR1cm4ge307XHJcbiAgICB9XHJcbiAgICB2YXIgcnVsZXMgPSB7fTtcclxuICAgIGlmICgncmVxdWlyZWQnIGluIGF0dHJzICYmIGF0dHJzLnJlcXVpcmVkICE9PSBmYWxzZSkge1xyXG4gICAgICAgIHJ1bGVzLnJlcXVpcmVkID0gYXR0cnMudHlwZSA9PT0gJ2NoZWNrYm94JyA/IFt0cnVlXSA6IHRydWU7XHJcbiAgICB9XHJcbiAgICBpZiAoaXNUZXh0SW5wdXQodm5vZGUpKSB7XHJcbiAgICAgICAgcmV0dXJuIG5vcm1hbGl6ZVJ1bGVzKF9fYXNzaWduKF9fYXNzaWduKHt9LCBydWxlcyksIHJlc29sdmVUZXh0dWFsUnVsZXModm5vZGUpKSk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gbm9ybWFsaXplUnVsZXMocnVsZXMpO1xyXG59XHJcbmZ1bmN0aW9uIG5vcm1hbGl6ZUNoaWxkcmVuKGNvbnRleHQsIHNsb3RQcm9wcykge1xyXG4gICAgaWYgKGNvbnRleHQuJHNjb3BlZFNsb3RzLmRlZmF1bHQpIHtcclxuICAgICAgICByZXR1cm4gY29udGV4dC4kc2NvcGVkU2xvdHMuZGVmYXVsdChzbG90UHJvcHMpIHx8IFtdO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIGNvbnRleHQuJHNsb3RzLmRlZmF1bHQgfHwgW107XHJcbn1cblxuLyoqXHJcbiAqIERldGVybWluZXMgaWYgYSBwcm92aWRlciBuZWVkcyB0byBydW4gdmFsaWRhdGlvbi5cclxuICovXHJcbmZ1bmN0aW9uIHNob3VsZFZhbGlkYXRlKGN0eCwgdmFsdWUpIHtcclxuICAgIC8vIHdoZW4gYW4gaW1tZWRpYXRlL2luaXRpYWwgdmFsaWRhdGlvbiBpcyBuZWVkZWQgYW5kIHdhc24ndCBkb25lIGJlZm9yZS5cclxuICAgIGlmICghY3R4Ll9pZ25vcmVJbW1lZGlhdGUgJiYgY3R4LmltbWVkaWF0ZSkge1xyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG4gICAgLy8gd2hlbiB0aGUgdmFsdWUgY2hhbmdlcyBmb3Igd2hhdGV2ZXIgcmVhc29uLlxyXG4gICAgaWYgKGN0eC52YWx1ZSAhPT0gdmFsdWUgJiYgY3R4Lm5vcm1hbGl6ZWRFdmVudHMubGVuZ3RoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcbiAgICAvLyB3aGVuIGl0IG5lZWRzIHZhbGlkYXRpb24gZHVlIHRvIHByb3BzL2Nyb3NzLWZpZWxkcyBjaGFuZ2VzLlxyXG4gICAgaWYgKGN0eC5fbmVlZHNWYWxpZGF0aW9uKSB7XHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcbiAgICAvLyB3aGVuIHRoZSBpbml0aWFsIHZhbHVlIGlzIHVuZGVmaW5lZCBhbmQgdGhlIGZpZWxkIHdhc24ndCByZW5kZXJlZCB5ZXQuXHJcbiAgICBpZiAoIWN0eC5pbml0aWFsaXplZCAmJiB2YWx1ZSA9PT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gZmFsc2U7XHJcbn1cclxuZnVuY3Rpb24gY3JlYXRlVmFsaWRhdGlvbkN0eChjdHgpIHtcclxuICAgIHJldHVybiBfX2Fzc2lnbihfX2Fzc2lnbih7fSwgY3R4LmZsYWdzKSwgeyBlcnJvcnM6IGN0eC5tZXNzYWdlcywgY2xhc3NlczogY3R4LmNsYXNzZXMsIGZhaWxlZFJ1bGVzOiBjdHguZmFpbGVkUnVsZXMsIHJlc2V0OiBmdW5jdGlvbiAoKSB7IHJldHVybiBjdHgucmVzZXQoKTsgfSwgdmFsaWRhdGU6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgdmFyIGFyZ3MgPSBbXTtcclxuICAgICAgICAgICAgZm9yICh2YXIgX2kgPSAwOyBfaSA8IGFyZ3VtZW50cy5sZW5ndGg7IF9pKyspIHtcclxuICAgICAgICAgICAgICAgIGFyZ3NbX2ldID0gYXJndW1lbnRzW19pXTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gY3R4LnZhbGlkYXRlLmFwcGx5KGN0eCwgYXJncyk7XHJcbiAgICAgICAgfSwgYXJpYUlucHV0OiB7XHJcbiAgICAgICAgICAgICdhcmlhLWludmFsaWQnOiBjdHguZmxhZ3MuaW52YWxpZCA/ICd0cnVlJyA6ICdmYWxzZScsXHJcbiAgICAgICAgICAgICdhcmlhLXJlcXVpcmVkJzogY3R4LmlzUmVxdWlyZWQgPyAndHJ1ZScgOiAnZmFsc2UnLFxyXG4gICAgICAgICAgICAnYXJpYS1lcnJvcm1lc3NhZ2UnOiBcInZlZV9cIiArIGN0eC5pZFxyXG4gICAgICAgIH0sIGFyaWFNc2c6IHtcclxuICAgICAgICAgICAgaWQ6IFwidmVlX1wiICsgY3R4LmlkLFxyXG4gICAgICAgICAgICAnYXJpYS1saXZlJzogY3R4Lm1lc3NhZ2VzLmxlbmd0aCA/ICdhc3NlcnRpdmUnIDogJ29mZidcclxuICAgICAgICB9IH0pO1xyXG59XHJcbmZ1bmN0aW9uIG9uUmVuZGVyVXBkYXRlKHZtLCB2YWx1ZSkge1xyXG4gICAgaWYgKCF2bS5pbml0aWFsaXplZCkge1xyXG4gICAgICAgIHZtLmluaXRpYWxWYWx1ZSA9IHZhbHVlO1xyXG4gICAgfVxyXG4gICAgdmFyIHZhbGlkYXRlTm93ID0gc2hvdWxkVmFsaWRhdGUodm0sIHZhbHVlKTtcclxuICAgIHZtLl9uZWVkc1ZhbGlkYXRpb24gPSBmYWxzZTtcclxuICAgIHZtLnZhbHVlID0gdmFsdWU7XHJcbiAgICB2bS5faWdub3JlSW1tZWRpYXRlID0gdHJ1ZTtcclxuICAgIGlmICghdmFsaWRhdGVOb3cpIHtcclxuICAgICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICB2bS52YWxpZGF0ZVNpbGVudCgpLnRoZW4odm0uaW1tZWRpYXRlIHx8IHZtLmZsYWdzLnZhbGlkYXRlZCA/IHZtLmFwcGx5UmVzdWx0IDogaWRlbnRpdHkpO1xyXG59XHJcbmZ1bmN0aW9uIGNvbXB1dGVNb2RlU2V0dGluZyhjdHgpIHtcclxuICAgIHZhciBjb21wdXRlID0gKGlzQ2FsbGFibGUoY3R4Lm1vZGUpID8gY3R4Lm1vZGUgOiBtb2Rlc1tjdHgubW9kZV0pO1xyXG4gICAgcmV0dXJuIGNvbXB1dGUoe1xyXG4gICAgICAgIGVycm9yczogY3R4Lm1lc3NhZ2VzLFxyXG4gICAgICAgIHZhbHVlOiBjdHgudmFsdWUsXHJcbiAgICAgICAgZmxhZ3M6IGN0eC5mbGFnc1xyXG4gICAgfSk7XHJcbn1cclxuLy8gQ3JlYXRlcyB0aGUgY29tbW9uIGhhbmRsZXJzIGZvciBhIHZhbGlkYXRhYmxlIGNvbnRleHQuXHJcbmZ1bmN0aW9uIGNyZWF0ZUNvbW1vbkhhbmRsZXJzKHZtKSB7XHJcbiAgICBpZiAoIXZtLiR2ZWVPbklucHV0KSB7XHJcbiAgICAgICAgdm0uJHZlZU9uSW5wdXQgPSBmdW5jdGlvbiAoZSkge1xyXG4gICAgICAgICAgICB2bS5zeW5jVmFsdWUoZSk7IC8vIHRyYWNrIGFuZCBrZWVwIHRoZSB2YWx1ZSB1cGRhdGVkLlxyXG4gICAgICAgICAgICB2bS5zZXRGbGFncyh7IGRpcnR5OiB0cnVlLCBwcmlzdGluZTogZmFsc2UgfSk7XHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuICAgIHZhciBvbklucHV0ID0gdm0uJHZlZU9uSW5wdXQ7XHJcbiAgICBpZiAoIXZtLiR2ZWVPbkJsdXIpIHtcclxuICAgICAgICB2bS4kdmVlT25CbHVyID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICB2bS5zZXRGbGFncyh7IHRvdWNoZWQ6IHRydWUsIHVudG91Y2hlZDogZmFsc2UgfSk7XHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuICAgIC8vIEJsdXIgZXZlbnQgbGlzdGVuZXIuXHJcbiAgICB2YXIgb25CbHVyID0gdm0uJHZlZU9uQmx1cjtcclxuICAgIHZhciBvblZhbGlkYXRlID0gdm0uJHZlZUhhbmRsZXI7XHJcbiAgICB2YXIgbW9kZSA9IGNvbXB1dGVNb2RlU2V0dGluZyh2bSk7XHJcbiAgICAvLyBIYW5kbGUgZGVib3VuY2UgY2hhbmdlcy5cclxuICAgIGlmICghb25WYWxpZGF0ZSB8fCB2bS4kdmVlRGVib3VuY2UgIT09IHZtLmRlYm91bmNlKSB7XHJcbiAgICAgICAgb25WYWxpZGF0ZSA9IGRlYm91bmNlKGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgdm0uJG5leHRUaWNrKGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgIHZhciBwZW5kaW5nUHJvbWlzZSA9IHZtLnZhbGlkYXRlU2lsZW50KCk7XHJcbiAgICAgICAgICAgICAgICAvLyBhdm9pZHMgcmFjZSBjb25kaXRpb25zIGJldHdlZW4gc3VjY2Vzc2l2ZSB2YWxpZGF0aW9ucy5cclxuICAgICAgICAgICAgICAgIHZtLl9wZW5kaW5nVmFsaWRhdGlvbiA9IHBlbmRpbmdQcm9taXNlO1xyXG4gICAgICAgICAgICAgICAgcGVuZGluZ1Byb21pc2UudGhlbihmdW5jdGlvbiAocmVzdWx0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHBlbmRpbmdQcm9taXNlID09PSB2bS5fcGVuZGluZ1ZhbGlkYXRpb24pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdm0uYXBwbHlSZXN1bHQocmVzdWx0KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdm0uX3BlbmRpbmdWYWxpZGF0aW9uID0gdW5kZWZpbmVkO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LCBtb2RlLmRlYm91bmNlIHx8IHZtLmRlYm91bmNlKTtcclxuICAgICAgICAvLyBDYWNoZSB0aGUgaGFuZGxlciBzbyB3ZSBkb24ndCBjcmVhdGUgaXQgZWFjaCB0aW1lLlxyXG4gICAgICAgIHZtLiR2ZWVIYW5kbGVyID0gb25WYWxpZGF0ZTtcclxuICAgICAgICAvLyBjYWNoZSB0aGUgZGVib3VuY2UgdmFsdWUgc28gd2UgZGV0ZWN0IGlmIGl0IHdhcyBjaGFuZ2VkLlxyXG4gICAgICAgIHZtLiR2ZWVEZWJvdW5jZSA9IHZtLmRlYm91bmNlO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHsgb25JbnB1dDogb25JbnB1dCwgb25CbHVyOiBvbkJsdXIsIG9uVmFsaWRhdGU6IG9uVmFsaWRhdGUgfTtcclxufVxyXG4vLyBBZGRzIGFsbCBwbHVnaW4gbGlzdGVuZXJzIHRvIHRoZSB2bm9kZS5cclxuZnVuY3Rpb24gYWRkTGlzdGVuZXJzKHZtLCBub2RlKSB7XHJcbiAgICB2YXIgdmFsdWUgPSBmaW5kVmFsdWUobm9kZSk7XHJcbiAgICAvLyBjYWNoZSB0aGUgaW5wdXQgZXZlbnROYW1lLlxyXG4gICAgdm0uX2lucHV0RXZlbnROYW1lID0gdm0uX2lucHV0RXZlbnROYW1lIHx8IGdldElucHV0RXZlbnROYW1lKG5vZGUsIGZpbmRNb2RlbChub2RlKSk7XHJcbiAgICBvblJlbmRlclVwZGF0ZSh2bSwgdmFsdWUgJiYgdmFsdWUudmFsdWUpO1xyXG4gICAgdmFyIF9hID0gY3JlYXRlQ29tbW9uSGFuZGxlcnModm0pLCBvbklucHV0ID0gX2Eub25JbnB1dCwgb25CbHVyID0gX2Eub25CbHVyLCBvblZhbGlkYXRlID0gX2Eub25WYWxpZGF0ZTtcclxuICAgIGFkZFZOb2RlTGlzdGVuZXIobm9kZSwgdm0uX2lucHV0RXZlbnROYW1lLCBvbklucHV0KTtcclxuICAgIGFkZFZOb2RlTGlzdGVuZXIobm9kZSwgJ2JsdXInLCBvbkJsdXIpO1xyXG4gICAgLy8gYWRkIHRoZSB2YWxpZGF0aW9uIGxpc3RlbmVycy5cclxuICAgIHZtLm5vcm1hbGl6ZWRFdmVudHMuZm9yRWFjaChmdW5jdGlvbiAoZXZ0KSB7XHJcbiAgICAgICAgYWRkVk5vZGVMaXN0ZW5lcihub2RlLCBldnQsIG9uVmFsaWRhdGUpO1xyXG4gICAgfSk7XHJcbiAgICB2bS5pbml0aWFsaXplZCA9IHRydWU7XHJcbn1cblxudmFyIFBST1ZJREVSX0NPVU5URVIgPSAwO1xyXG5mdW5jdGlvbiBkYXRhKCkge1xyXG4gICAgdmFyIG1lc3NhZ2VzID0gW107XHJcbiAgICB2YXIgZGVmYXVsdFZhbHVlcyA9IHtcclxuICAgICAgICBtZXNzYWdlczogbWVzc2FnZXMsXHJcbiAgICAgICAgdmFsdWU6IHVuZGVmaW5lZCxcclxuICAgICAgICBpbml0aWFsaXplZDogZmFsc2UsXHJcbiAgICAgICAgaW5pdGlhbFZhbHVlOiB1bmRlZmluZWQsXHJcbiAgICAgICAgZmxhZ3M6IGNyZWF0ZUZsYWdzKCksXHJcbiAgICAgICAgZmFpbGVkUnVsZXM6IHt9LFxyXG4gICAgICAgIGlzRGVhY3RpdmF0ZWQ6IGZhbHNlLFxyXG4gICAgICAgIGlkOiAnJ1xyXG4gICAgfTtcclxuICAgIHJldHVybiBkZWZhdWx0VmFsdWVzO1xyXG59XHJcbnZhciBWYWxpZGF0aW9uUHJvdmlkZXIgPSBWdWUuZXh0ZW5kKHtcclxuICAgIGluamVjdDoge1xyXG4gICAgICAgICRfdmVlT2JzZXJ2ZXI6IHtcclxuICAgICAgICAgICAgZnJvbTogJyRfdmVlT2JzZXJ2ZXInLFxyXG4gICAgICAgICAgICBkZWZhdWx0OiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIXRoaXMuJHZub2RlLmNvbnRleHQuJF92ZWVPYnNlcnZlcikge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuJHZub2RlLmNvbnRleHQuJF92ZWVPYnNlcnZlciA9IGNyZWF0ZU9ic2VydmVyKCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy4kdm5vZGUuY29udGV4dC4kX3ZlZU9ic2VydmVyO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIHByb3BzOiB7XHJcbiAgICAgICAgdmlkOiB7XHJcbiAgICAgICAgICAgIHR5cGU6IFN0cmluZyxcclxuICAgICAgICAgICAgZGVmYXVsdDogJydcclxuICAgICAgICB9LFxyXG4gICAgICAgIG5hbWU6IHtcclxuICAgICAgICAgICAgdHlwZTogU3RyaW5nLFxyXG4gICAgICAgICAgICBkZWZhdWx0OiBudWxsXHJcbiAgICAgICAgfSxcclxuICAgICAgICBtb2RlOiB7XHJcbiAgICAgICAgICAgIHR5cGU6IFtTdHJpbmcsIEZ1bmN0aW9uXSxcclxuICAgICAgICAgICAgZGVmYXVsdDogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGdldENvbmZpZygpLm1vZGU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG4gICAgICAgIHJ1bGVzOiB7XHJcbiAgICAgICAgICAgIHR5cGU6IFtPYmplY3QsIFN0cmluZ10sXHJcbiAgICAgICAgICAgIGRlZmF1bHQ6IG51bGxcclxuICAgICAgICB9LFxyXG4gICAgICAgIGltbWVkaWF0ZToge1xyXG4gICAgICAgICAgICB0eXBlOiBCb29sZWFuLFxyXG4gICAgICAgICAgICBkZWZhdWx0OiBmYWxzZVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgcGVyc2lzdDoge1xyXG4gICAgICAgICAgICB0eXBlOiBCb29sZWFuLFxyXG4gICAgICAgICAgICBkZWZhdWx0OiBmYWxzZVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgYmFpbHM6IHtcclxuICAgICAgICAgICAgdHlwZTogQm9vbGVhbixcclxuICAgICAgICAgICAgZGVmYXVsdDogZnVuY3Rpb24gKCkgeyByZXR1cm4gZ2V0Q29uZmlnKCkuYmFpbHM7IH1cclxuICAgICAgICB9LFxyXG4gICAgICAgIHNraXBJZkVtcHR5OiB7XHJcbiAgICAgICAgICAgIHR5cGU6IEJvb2xlYW4sXHJcbiAgICAgICAgICAgIGRlZmF1bHQ6IGZ1bmN0aW9uICgpIHsgcmV0dXJuIGdldENvbmZpZygpLnNraXBPcHRpb25hbDsgfVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgZGVib3VuY2U6IHtcclxuICAgICAgICAgICAgdHlwZTogTnVtYmVyLFxyXG4gICAgICAgICAgICBkZWZhdWx0OiAwXHJcbiAgICAgICAgfSxcclxuICAgICAgICB0YWc6IHtcclxuICAgICAgICAgICAgdHlwZTogU3RyaW5nLFxyXG4gICAgICAgICAgICBkZWZhdWx0OiAnc3BhbidcclxuICAgICAgICB9LFxyXG4gICAgICAgIHNsaW06IHtcclxuICAgICAgICAgICAgdHlwZTogQm9vbGVhbixcclxuICAgICAgICAgICAgZGVmYXVsdDogZmFsc2VcclxuICAgICAgICB9LFxyXG4gICAgICAgIGRpc2FibGVkOiB7XHJcbiAgICAgICAgICAgIHR5cGU6IEJvb2xlYW4sXHJcbiAgICAgICAgICAgIGRlZmF1bHQ6IGZhbHNlXHJcbiAgICAgICAgfSxcclxuICAgICAgICBjdXN0b21NZXNzYWdlczoge1xyXG4gICAgICAgICAgICB0eXBlOiBPYmplY3QsXHJcbiAgICAgICAgICAgIGRlZmF1bHQ6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB7fTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICB3YXRjaDoge1xyXG4gICAgICAgIHJ1bGVzOiB7XHJcbiAgICAgICAgICAgIGRlZXA6IHRydWUsXHJcbiAgICAgICAgICAgIGhhbmRsZXI6IGZ1bmN0aW9uICh2YWwsIG9sZFZhbCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fbmVlZHNWYWxpZGF0aW9uID0gIWlzRXF1YWwodmFsLCBvbGRWYWwpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIGRhdGE6IGRhdGEsXHJcbiAgICBjb21wdXRlZDoge1xyXG4gICAgICAgIGZpZWxkRGVwczogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xyXG4gICAgICAgICAgICByZXR1cm4gT2JqZWN0LmtleXModGhpcy5ub3JtYWxpemVkUnVsZXMpXHJcbiAgICAgICAgICAgICAgICAuZmlsdGVyKFJ1bGVDb250YWluZXIuaXNUYXJnZXRSdWxlKVxyXG4gICAgICAgICAgICAgICAgLnJlZHVjZShmdW5jdGlvbiAoYWNjLCBydWxlKSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgZGVwcyA9IFJ1bGVDb250YWluZXIuZ2V0VGFyZ2V0UGFyYW1OYW1lcyhydWxlLCBfdGhpcy5ub3JtYWxpemVkUnVsZXNbcnVsZV0pO1xyXG4gICAgICAgICAgICAgICAgYWNjLnB1c2guYXBwbHkoYWNjLCBkZXBzKTtcclxuICAgICAgICAgICAgICAgIGRlcHMuZm9yRWFjaChmdW5jdGlvbiAoZGVwTmFtZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHdhdGNoQ3Jvc3NGaWVsZERlcChfdGhpcywgZGVwTmFtZSk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIHJldHVybiBhY2M7XHJcbiAgICAgICAgICAgIH0sIFtdKTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIG5vcm1hbGl6ZWRFdmVudHM6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgdmFyIF90aGlzID0gdGhpcztcclxuICAgICAgICAgICAgdmFyIG9uID0gY29tcHV0ZU1vZGVTZXR0aW5nKHRoaXMpLm9uO1xyXG4gICAgICAgICAgICByZXR1cm4gKG9uIHx8IFtdKS5tYXAoZnVuY3Rpb24gKGUpIHtcclxuICAgICAgICAgICAgICAgIGlmIChlID09PSAnaW5wdXQnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIF90aGlzLl9pbnB1dEV2ZW50TmFtZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHJldHVybiBlO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIGlzUmVxdWlyZWQ6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgdmFyIHJ1bGVzID0gX19hc3NpZ24oX19hc3NpZ24oe30sIHRoaXMuX3Jlc29sdmVkUnVsZXMpLCB0aGlzLm5vcm1hbGl6ZWRSdWxlcyk7XHJcbiAgICAgICAgICAgIHZhciBpc1JlcXVpcmVkID0gT2JqZWN0LmtleXMocnVsZXMpLnNvbWUoUnVsZUNvbnRhaW5lci5pc1JlcXVpcmVSdWxlKTtcclxuICAgICAgICAgICAgdGhpcy5mbGFncy5yZXF1aXJlZCA9ICEhaXNSZXF1aXJlZDtcclxuICAgICAgICAgICAgcmV0dXJuIGlzUmVxdWlyZWQ7XHJcbiAgICAgICAgfSxcclxuICAgICAgICBjbGFzc2VzOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIHZhciBuYW1lcyA9IGdldENvbmZpZygpLmNsYXNzZXM7XHJcbiAgICAgICAgICAgIHJldHVybiBjb21wdXRlQ2xhc3NPYmoobmFtZXMsIHRoaXMuZmxhZ3MpO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgbm9ybWFsaXplZFJ1bGVzOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBub3JtYWxpemVSdWxlcyh0aGlzLnJ1bGVzKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAgcmVuZGVyOiBmdW5jdGlvbiAoaCkge1xyXG4gICAgICAgIHZhciBfdGhpcyA9IHRoaXM7XHJcbiAgICAgICAgdGhpcy5yZWdpc3RlckZpZWxkKCk7XHJcbiAgICAgICAgdmFyIGN0eCA9IGNyZWF0ZVZhbGlkYXRpb25DdHgodGhpcyk7XHJcbiAgICAgICAgdmFyIGNoaWxkcmVuID0gbm9ybWFsaXplQ2hpbGRyZW4odGhpcywgY3R4KTtcclxuICAgICAgICAvLyBIYW5kbGUgc2luZ2xlLXJvb3Qgc2xvdC5cclxuICAgICAgICBleHRyYWN0Vk5vZGVzKGNoaWxkcmVuKS5mb3JFYWNoKGZ1bmN0aW9uIChpbnB1dCkge1xyXG4gICAgICAgICAgICBfdGhpcy5fcmVzb2x2ZWRSdWxlcyA9IGdldENvbmZpZygpLnVzZUNvbnN0cmFpbnRBdHRycyA/IHJlc29sdmVSdWxlcyhpbnB1dCkgOiB7fTtcclxuICAgICAgICAgICAgYWRkTGlzdGVuZXJzKF90aGlzLCBpbnB1dCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc2xpbSAmJiBjaGlsZHJlbi5sZW5ndGggPD0gMSA/IGNoaWxkcmVuWzBdIDogaCh0aGlzLnRhZywgY2hpbGRyZW4pO1xyXG4gICAgfSxcclxuICAgIGJlZm9yZURlc3Ryb3k6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAvLyBjbGVhbnVwIHJlZmVyZW5jZS5cclxuICAgICAgICB0aGlzLiRfdmVlT2JzZXJ2ZXIudW5zdWJzY3JpYmUodGhpcy5pZCk7XHJcbiAgICB9LFxyXG4gICAgYWN0aXZhdGVkOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdGhpcy4kX3ZlZU9ic2VydmVyLnN1YnNjcmliZSh0aGlzKTtcclxuICAgICAgICB0aGlzLmlzRGVhY3RpdmF0ZWQgPSBmYWxzZTtcclxuICAgIH0sXHJcbiAgICBkZWFjdGl2YXRlZDogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHRoaXMuJF92ZWVPYnNlcnZlci51bnN1YnNjcmliZSh0aGlzLmlkKTtcclxuICAgICAgICB0aGlzLmlzRGVhY3RpdmF0ZWQgPSB0cnVlO1xyXG4gICAgfSxcclxuICAgIG1ldGhvZHM6IHtcclxuICAgICAgICBzZXRGbGFnczogZnVuY3Rpb24gKGZsYWdzKSB7XHJcbiAgICAgICAgICAgIHZhciBfdGhpcyA9IHRoaXM7XHJcbiAgICAgICAgICAgIE9iamVjdC5rZXlzKGZsYWdzKS5mb3JFYWNoKGZ1bmN0aW9uIChmbGFnKSB7XHJcbiAgICAgICAgICAgICAgICBfdGhpcy5mbGFnc1tmbGFnXSA9IGZsYWdzW2ZsYWddO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIHN5bmNWYWx1ZTogZnVuY3Rpb24gKHYpIHtcclxuICAgICAgICAgICAgdmFyIHZhbHVlID0gbm9ybWFsaXplRXZlbnRWYWx1ZSh2KTtcclxuICAgICAgICAgICAgdGhpcy52YWx1ZSA9IHZhbHVlO1xyXG4gICAgICAgICAgICB0aGlzLmZsYWdzLmNoYW5nZWQgPSB0aGlzLmluaXRpYWxWYWx1ZSAhPT0gdmFsdWU7XHJcbiAgICAgICAgfSxcclxuICAgICAgICByZXNldDogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICB0aGlzLm1lc3NhZ2VzID0gW107XHJcbiAgICAgICAgICAgIHRoaXMuaW5pdGlhbFZhbHVlID0gdGhpcy52YWx1ZTtcclxuICAgICAgICAgICAgdmFyIGZsYWdzID0gY3JlYXRlRmxhZ3MoKTtcclxuICAgICAgICAgICAgZmxhZ3MucmVxdWlyZWQgPSB0aGlzLmlzUmVxdWlyZWQ7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0RmxhZ3MoZmxhZ3MpO1xyXG4gICAgICAgICAgICB0aGlzLnZhbGlkYXRlU2lsZW50KCk7XHJcbiAgICAgICAgfSxcclxuICAgICAgICB2YWxpZGF0ZTogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICB2YXIgYXJncyA9IFtdO1xyXG4gICAgICAgICAgICBmb3IgKHZhciBfaSA9IDA7IF9pIDwgYXJndW1lbnRzLmxlbmd0aDsgX2krKykge1xyXG4gICAgICAgICAgICAgICAgYXJnc1tfaV0gPSBhcmd1bWVudHNbX2ldO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybiBfX2F3YWl0ZXIodGhpcywgdm9pZCAwLCB2b2lkIDAsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgIHZhciByZXN1bHQ7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gX19nZW5lcmF0b3IodGhpcywgZnVuY3Rpb24gKF9hKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgc3dpdGNoIChfYS5sYWJlbCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjYXNlIDA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoYXJncy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zeW5jVmFsdWUoYXJnc1swXSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gWzQgLyp5aWVsZCovLCB0aGlzLnZhbGlkYXRlU2lsZW50KCldO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjYXNlIDE6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXN1bHQgPSBfYS5zZW50KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmFwcGx5UmVzdWx0KHJlc3VsdCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gWzIgLypyZXR1cm4qLywgcmVzdWx0XTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSxcclxuICAgICAgICB2YWxpZGF0ZVNpbGVudDogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICByZXR1cm4gX19hd2FpdGVyKHRoaXMsIHZvaWQgMCwgdm9pZCAwLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgcnVsZXMsIHJlc3VsdDtcclxuICAgICAgICAgICAgICAgIHJldHVybiBfX2dlbmVyYXRvcih0aGlzLCBmdW5jdGlvbiAoX2EpIHtcclxuICAgICAgICAgICAgICAgICAgICBzd2l0Y2ggKF9hLmxhYmVsKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhc2UgMDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0RmxhZ3MoeyBwZW5kaW5nOiB0cnVlIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcnVsZXMgPSBfX2Fzc2lnbihfX2Fzc2lnbih7fSwgdGhpcy5fcmVzb2x2ZWRSdWxlcyksIHRoaXMubm9ybWFsaXplZFJ1bGVzKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShydWxlcywgJ18kJGlzTm9ybWFsaXplZCcsIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB3cml0YWJsZTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZW51bWVyYWJsZTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uZmlndXJhYmxlOiBmYWxzZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gWzQgLyp5aWVsZCovLCB2YWxpZGF0ZSh0aGlzLnZhbHVlLCBydWxlcywge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiB0aGlzLm5hbWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlczogY3JlYXRlVmFsdWVzTG9va3VwKHRoaXMpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBiYWlsczogdGhpcy5iYWlscyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2tpcElmRW1wdHk6IHRoaXMuc2tpcElmRW1wdHksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlzSW5pdGlhbDogIXRoaXMuaW5pdGlhbGl6ZWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGN1c3RvbU1lc3NhZ2VzOiB0aGlzLmN1c3RvbU1lc3NhZ2VzXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSldO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjYXNlIDE6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXN1bHQgPSBfYS5zZW50KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNldEZsYWdzKHsgcGVuZGluZzogZmFsc2UgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNldEZsYWdzKHsgdmFsaWQ6IHJlc3VsdC52YWxpZCwgaW52YWxpZDogIXJlc3VsdC52YWxpZCB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBbMiAvKnJldHVybiovLCByZXN1bHRdO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIHNldEVycm9yczogZnVuY3Rpb24gKGVycm9ycykge1xyXG4gICAgICAgICAgICB0aGlzLmFwcGx5UmVzdWx0KHsgZXJyb3JzOiBlcnJvcnMsIGZhaWxlZFJ1bGVzOiB7fSB9KTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIGFwcGx5UmVzdWx0OiBmdW5jdGlvbiAoX2EpIHtcclxuICAgICAgICAgICAgdmFyIGVycm9ycyA9IF9hLmVycm9ycywgZmFpbGVkUnVsZXMgPSBfYS5mYWlsZWRSdWxlcztcclxuICAgICAgICAgICAgdGhpcy5tZXNzYWdlcyA9IGVycm9ycztcclxuICAgICAgICAgICAgdGhpcy5mYWlsZWRSdWxlcyA9IF9fYXNzaWduKHt9LCAoZmFpbGVkUnVsZXMgfHwge30pKTtcclxuICAgICAgICAgICAgdGhpcy5zZXRGbGFncyh7XHJcbiAgICAgICAgICAgICAgICB2YWxpZDogIWVycm9ycy5sZW5ndGgsXHJcbiAgICAgICAgICAgICAgICBjaGFuZ2VkOiB0aGlzLnZhbHVlICE9PSB0aGlzLmluaXRpYWxWYWx1ZSxcclxuICAgICAgICAgICAgICAgIGludmFsaWQ6ICEhZXJyb3JzLmxlbmd0aCxcclxuICAgICAgICAgICAgICAgIHZhbGlkYXRlZDogdHJ1ZVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIHJlZ2lzdGVyRmllbGQ6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgdXBkYXRlUmVuZGVyaW5nQ29udGV4dFJlZnModGhpcyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59KTtcclxuZnVuY3Rpb24gY3JlYXRlVmFsdWVzTG9va3VwKHZtKSB7XHJcbiAgICB2YXIgcHJvdmlkZXJzID0gdm0uJF92ZWVPYnNlcnZlci5yZWZzO1xyXG4gICAgdmFyIHJlZHVjZWQgPSB7fTtcclxuICAgIHJldHVybiB2bS5maWVsZERlcHMucmVkdWNlKGZ1bmN0aW9uIChhY2MsIGRlcE5hbWUpIHtcclxuICAgICAgICBpZiAoIXByb3ZpZGVyc1tkZXBOYW1lXSkge1xyXG4gICAgICAgICAgICByZXR1cm4gYWNjO1xyXG4gICAgICAgIH1cclxuICAgICAgICBhY2NbZGVwTmFtZV0gPSBwcm92aWRlcnNbZGVwTmFtZV0udmFsdWU7XHJcbiAgICAgICAgcmV0dXJuIGFjYztcclxuICAgIH0sIHJlZHVjZWQpO1xyXG59XHJcbmZ1bmN0aW9uIGV4dHJhY3RJZCh2bSkge1xyXG4gICAgaWYgKHZtLnZpZCkge1xyXG4gICAgICAgIHJldHVybiB2bS52aWQ7XHJcbiAgICB9XHJcbiAgICBpZiAodm0ubmFtZSkge1xyXG4gICAgICAgIHJldHVybiB2bS5uYW1lO1xyXG4gICAgfVxyXG4gICAgaWYgKHZtLmlkKSB7XHJcbiAgICAgICAgcmV0dXJuIHZtLmlkO1xyXG4gICAgfVxyXG4gICAgUFJPVklERVJfQ09VTlRFUisrO1xyXG4gICAgcmV0dXJuIFwiX3ZlZV9cIiArIFBST1ZJREVSX0NPVU5URVI7XHJcbn1cclxuZnVuY3Rpb24gdXBkYXRlUmVuZGVyaW5nQ29udGV4dFJlZnModm0pIHtcclxuICAgIHZhciBwcm92aWRlZElkID0gZXh0cmFjdElkKHZtKTtcclxuICAgIHZhciBpZCA9IHZtLmlkO1xyXG4gICAgLy8gTm90aGluZyBoYXMgY2hhbmdlZC5cclxuICAgIGlmICh2bS5pc0RlYWN0aXZhdGVkIHx8IChpZCA9PT0gcHJvdmlkZWRJZCAmJiB2bS4kX3ZlZU9ic2VydmVyLnJlZnNbaWRdKSkge1xyXG4gICAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIC8vIHZpZCB3YXMgY2hhbmdlZC5cclxuICAgIGlmIChpZCAhPT0gcHJvdmlkZWRJZCAmJiB2bS4kX3ZlZU9ic2VydmVyLnJlZnNbaWRdID09PSB2bSkge1xyXG4gICAgICAgIHZtLiRfdmVlT2JzZXJ2ZXIudW5zdWJzY3JpYmUoaWQpO1xyXG4gICAgfVxyXG4gICAgdm0uaWQgPSBwcm92aWRlZElkO1xyXG4gICAgdm0uJF92ZWVPYnNlcnZlci5zdWJzY3JpYmUodm0pO1xyXG59XHJcbmZ1bmN0aW9uIGNyZWF0ZU9ic2VydmVyKCkge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgICByZWZzOiB7fSxcclxuICAgICAgICBzdWJzY3JpYmU6IGZ1bmN0aW9uICh2bSkge1xyXG4gICAgICAgICAgICB0aGlzLnJlZnNbdm0uaWRdID0gdm07XHJcbiAgICAgICAgfSxcclxuICAgICAgICB1bnN1YnNjcmliZTogZnVuY3Rpb24gKGlkKSB7XHJcbiAgICAgICAgICAgIGRlbGV0ZSB0aGlzLnJlZnNbaWRdO1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcbn1cclxuZnVuY3Rpb24gd2F0Y2hDcm9zc0ZpZWxkRGVwKGN0eCwgZGVwTmFtZSwgd2l0aEhvb2tzKSB7XHJcbiAgICBpZiAod2l0aEhvb2tzID09PSB2b2lkIDApIHsgd2l0aEhvb2tzID0gdHJ1ZTsgfVxyXG4gICAgdmFyIHByb3ZpZGVycyA9IGN0eC4kX3ZlZU9ic2VydmVyLnJlZnM7XHJcbiAgICBpZiAoIWN0eC5fdmVlV2F0Y2hlcnMpIHtcclxuICAgICAgICBjdHguX3ZlZVdhdGNoZXJzID0ge307XHJcbiAgICB9XHJcbiAgICBpZiAoIXByb3ZpZGVyc1tkZXBOYW1lXSAmJiB3aXRoSG9va3MpIHtcclxuICAgICAgICByZXR1cm4gY3R4LiRvbmNlKCdob29rOm1vdW50ZWQnLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIHdhdGNoQ3Jvc3NGaWVsZERlcChjdHgsIGRlcE5hbWUsIGZhbHNlKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuICAgIGlmICghaXNDYWxsYWJsZShjdHguX3ZlZVdhdGNoZXJzW2RlcE5hbWVdKSAmJiBwcm92aWRlcnNbZGVwTmFtZV0pIHtcclxuICAgICAgICBjdHguX3ZlZVdhdGNoZXJzW2RlcE5hbWVdID0gcHJvdmlkZXJzW2RlcE5hbWVdLiR3YXRjaCgndmFsdWUnLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIGlmIChjdHguZmxhZ3MudmFsaWRhdGVkKSB7XHJcbiAgICAgICAgICAgICAgICBjdHguX25lZWRzVmFsaWRhdGlvbiA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICBjdHgudmFsaWRhdGUoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG59XG5cbnZhciBmbGFnTWVyZ2luZ1N0cmF0ZWd5ID0ge1xyXG4gICAgcHJpc3RpbmU6ICdldmVyeScsXHJcbiAgICBkaXJ0eTogJ3NvbWUnLFxyXG4gICAgdG91Y2hlZDogJ3NvbWUnLFxyXG4gICAgdW50b3VjaGVkOiAnZXZlcnknLFxyXG4gICAgdmFsaWQ6ICdldmVyeScsXHJcbiAgICBpbnZhbGlkOiAnc29tZScsXHJcbiAgICBwZW5kaW5nOiAnc29tZScsXHJcbiAgICB2YWxpZGF0ZWQ6ICdldmVyeScsXHJcbiAgICBjaGFuZ2VkOiAnc29tZSdcclxufTtcclxuZnVuY3Rpb24gbWVyZ2VGbGFncyhsaHMsIHJocywgc3RyYXRlZ3kpIHtcclxuICAgIHZhciBzdHJhdE5hbWUgPSBmbGFnTWVyZ2luZ1N0cmF0ZWd5W3N0cmF0ZWd5XTtcclxuICAgIHJldHVybiBbbGhzLCByaHNdW3N0cmF0TmFtZV0oZnVuY3Rpb24gKGYpIHsgcmV0dXJuIGY7IH0pO1xyXG59XHJcbnZhciBPQlNFUlZFUl9DT1VOVEVSID0gMDtcclxuZnVuY3Rpb24gZGF0YSQxKCkge1xyXG4gICAgdmFyIHJlZnMgPSB7fTtcclxuICAgIHZhciByZWZzQnlOYW1lID0ge307XHJcbiAgICB2YXIgaW5hY3RpdmVSZWZzID0ge307XHJcbiAgICAvLyBGSVhNRTogTm90IHN1cmUgb2YgdGhpcyBvbmUgY2FuIGJlIHR5cGVkLCBjaXJjdWxhciB0eXBlIHJlZmVyZW5jZS5cclxuICAgIHZhciBvYnNlcnZlcnMgPSBbXTtcclxuICAgIHJldHVybiB7XHJcbiAgICAgICAgaWQ6ICcnLFxyXG4gICAgICAgIHJlZnM6IHJlZnMsXHJcbiAgICAgICAgcmVmc0J5TmFtZTogcmVmc0J5TmFtZSxcclxuICAgICAgICBvYnNlcnZlcnM6IG9ic2VydmVycyxcclxuICAgICAgICBpbmFjdGl2ZVJlZnM6IGluYWN0aXZlUmVmc1xyXG4gICAgfTtcclxufVxyXG52YXIgVmFsaWRhdGlvbk9ic2VydmVyID0gVnVlLmV4dGVuZCh7XHJcbiAgICBuYW1lOiAnVmFsaWRhdGlvbk9ic2VydmVyJyxcclxuICAgIHByb3ZpZGU6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAkX3ZlZU9ic2VydmVyOiB0aGlzXHJcbiAgICAgICAgfTtcclxuICAgIH0sXHJcbiAgICBpbmplY3Q6IHtcclxuICAgICAgICAkX3ZlZU9ic2VydmVyOiB7XHJcbiAgICAgICAgICAgIGZyb206ICckX3ZlZU9ic2VydmVyJyxcclxuICAgICAgICAgICAgZGVmYXVsdDogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgaWYgKCF0aGlzLiR2bm9kZS5jb250ZXh0LiRfdmVlT2JzZXJ2ZXIpIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLiR2bm9kZS5jb250ZXh0LiRfdmVlT2JzZXJ2ZXI7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAgcHJvcHM6IHtcclxuICAgICAgICB0YWc6IHtcclxuICAgICAgICAgICAgdHlwZTogU3RyaW5nLFxyXG4gICAgICAgICAgICBkZWZhdWx0OiAnc3BhbidcclxuICAgICAgICB9LFxyXG4gICAgICAgIHZpZDoge1xyXG4gICAgICAgICAgICB0eXBlOiBTdHJpbmcsXHJcbiAgICAgICAgICAgIGRlZmF1bHQ6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBcIm9ic19cIiArIE9CU0VSVkVSX0NPVU5URVIrKztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgc2xpbToge1xyXG4gICAgICAgICAgICB0eXBlOiBCb29sZWFuLFxyXG4gICAgICAgICAgICBkZWZhdWx0OiBmYWxzZVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgZGlzYWJsZWQ6IHtcclxuICAgICAgICAgICAgdHlwZTogQm9vbGVhbixcclxuICAgICAgICAgICAgZGVmYXVsdDogZmFsc2VcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAgZGF0YTogZGF0YSQxLFxyXG4gICAgY29tcHV0ZWQ6IHtcclxuICAgICAgICBjdHg6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgdmFyIF90aGlzID0gdGhpcztcclxuICAgICAgICAgICAgdmFyIGN0eCA9IHtcclxuICAgICAgICAgICAgICAgIGVycm9yczoge30sXHJcbiAgICAgICAgICAgICAgICBwYXNzZXM6IGZ1bmN0aW9uIChjYikge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBfdGhpcy52YWxpZGF0ZSgpLnRoZW4oZnVuY3Rpb24gKHJlc3VsdCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVzdWx0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gY2IoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIHZhbGlkYXRlOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFyIGFyZ3MgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICBmb3IgKHZhciBfaSA9IDA7IF9pIDwgYXJndW1lbnRzLmxlbmd0aDsgX2krKykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhcmdzW19pXSA9IGFyZ3VtZW50c1tfaV07XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBfdGhpcy52YWxpZGF0ZS5hcHBseShfdGhpcywgYXJncyk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgcmVzZXQ6IGZ1bmN0aW9uICgpIHsgcmV0dXJuIF90aGlzLnJlc2V0KCk7IH1cclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgcmV0dXJuIF9fc3ByZWFkQXJyYXlzKHZhbHVlcyh0aGlzLnJlZnMpLCBPYmplY3Qua2V5cyh0aGlzLmluYWN0aXZlUmVmcykubWFwKGZ1bmN0aW9uIChrZXkpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdmlkOiBrZXksXHJcbiAgICAgICAgICAgICAgICAgICAgZmxhZ3M6IF90aGlzLmluYWN0aXZlUmVmc1trZXldLmZsYWdzLFxyXG4gICAgICAgICAgICAgICAgICAgIG1lc3NhZ2VzOiBfdGhpcy5pbmFjdGl2ZVJlZnNba2V5XS5lcnJvcnNcclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIH0pLCB0aGlzLm9ic2VydmVycykucmVkdWNlKGZ1bmN0aW9uIChhY2MsIHByb3ZpZGVyKSB7XHJcbiAgICAgICAgICAgICAgICBPYmplY3Qua2V5cyhmbGFnTWVyZ2luZ1N0cmF0ZWd5KS5mb3JFYWNoKGZ1bmN0aW9uIChmbGFnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFyIGZsYWdzID0gcHJvdmlkZXIuZmxhZ3MgfHwgcHJvdmlkZXIuY3R4O1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICghKGZsYWcgaW4gYWNjKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhY2NbZmxhZ10gPSBmbGFnc1tmbGFnXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBhY2NbZmxhZ10gPSBtZXJnZUZsYWdzKGFjY1tmbGFnXSwgZmxhZ3NbZmxhZ10sIGZsYWcpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICBhY2MuZXJyb3JzW3Byb3ZpZGVyLmlkXSA9XHJcbiAgICAgICAgICAgICAgICAgICAgcHJvdmlkZXIubWVzc2FnZXMgfHxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWVzKHByb3ZpZGVyLmN0eC5lcnJvcnMpLnJlZHVjZShmdW5jdGlvbiAoZXJycywgb2JzRXJyb3JzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gZXJycy5jb25jYXQob2JzRXJyb3JzKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSwgW10pO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGFjYztcclxuICAgICAgICAgICAgfSwgY3R4KTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAgY3JlYXRlZDogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHRoaXMuaWQgPSB0aGlzLnZpZDtcclxuICAgICAgICBpZiAodGhpcy4kX3ZlZU9ic2VydmVyKSB7XHJcbiAgICAgICAgICAgIHRoaXMuJF92ZWVPYnNlcnZlci5zdWJzY3JpYmUodGhpcywgJ29ic2VydmVyJyk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIGFjdGl2YXRlZDogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIGlmICh0aGlzLiRfdmVlT2JzZXJ2ZXIpIHtcclxuICAgICAgICAgICAgdGhpcy4kX3ZlZU9ic2VydmVyLnN1YnNjcmliZSh0aGlzLCAnb2JzZXJ2ZXInKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAgZGVhY3RpdmF0ZWQ6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICBpZiAodGhpcy4kX3ZlZU9ic2VydmVyKSB7XHJcbiAgICAgICAgICAgIHRoaXMuJF92ZWVPYnNlcnZlci51bnN1YnNjcmliZSh0aGlzLmlkLCAnb2JzZXJ2ZXInKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAgYmVmb3JlRGVzdHJveTogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIGlmICh0aGlzLiRfdmVlT2JzZXJ2ZXIpIHtcclxuICAgICAgICAgICAgdGhpcy4kX3ZlZU9ic2VydmVyLnVuc3Vic2NyaWJlKHRoaXMuaWQsICdvYnNlcnZlcicpO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICByZW5kZXI6IGZ1bmN0aW9uIChoKSB7XHJcbiAgICAgICAgdmFyIGNoaWxkcmVuID0gbm9ybWFsaXplQ2hpbGRyZW4odGhpcywgdGhpcy5jdHgpO1xyXG4gICAgICAgIHJldHVybiB0aGlzLnNsaW0gJiYgY2hpbGRyZW4ubGVuZ3RoIDw9IDEgPyBjaGlsZHJlblswXSA6IGgodGhpcy50YWcsIHsgb246IHRoaXMuJGxpc3RlbmVycyB9LCBjaGlsZHJlbik7XHJcbiAgICB9LFxyXG4gICAgbWV0aG9kczoge1xyXG4gICAgICAgIHN1YnNjcmliZTogZnVuY3Rpb24gKHN1YnNjcmliZXIsIGtpbmQpIHtcclxuICAgICAgICAgICAgdmFyIF9hLCBfYjtcclxuICAgICAgICAgICAgaWYgKGtpbmQgPT09IHZvaWQgMCkgeyBraW5kID0gJ3Byb3ZpZGVyJzsgfVxyXG4gICAgICAgICAgICBpZiAoa2luZCA9PT0gJ29ic2VydmVyJykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5vYnNlcnZlcnMucHVzaChzdWJzY3JpYmVyKTtcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLnJlZnMgPSBfX2Fzc2lnbihfX2Fzc2lnbih7fSwgdGhpcy5yZWZzKSwgKF9hID0ge30sIF9hW3N1YnNjcmliZXIuaWRdID0gc3Vic2NyaWJlciwgX2EpKTtcclxuICAgICAgICAgICAgdGhpcy5yZWZzQnlOYW1lID0gX19hc3NpZ24oX19hc3NpZ24oe30sIHRoaXMucmVmc0J5TmFtZSksIChfYiA9IHt9LCBfYltzdWJzY3JpYmVyLm5hbWVdID0gc3Vic2NyaWJlciwgX2IpKTtcclxuICAgICAgICAgICAgaWYgKHN1YnNjcmliZXIucGVyc2lzdCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5yZXN0b3JlUHJvdmlkZXJTdGF0ZShzdWJzY3JpYmVyKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgdW5zdWJzY3JpYmU6IGZ1bmN0aW9uIChpZCwga2luZCkge1xyXG4gICAgICAgICAgICBpZiAoa2luZCA9PT0gdm9pZCAwKSB7IGtpbmQgPSAncHJvdmlkZXInOyB9XHJcbiAgICAgICAgICAgIGlmIChraW5kID09PSAncHJvdmlkZXInKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnJlbW92ZVByb3ZpZGVyKGlkKTtcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB2YXIgaWR4ID0gZmluZEluZGV4KHRoaXMub2JzZXJ2ZXJzLCBmdW5jdGlvbiAobykgeyByZXR1cm4gby5pZCA9PT0gaWQ7IH0pO1xyXG4gICAgICAgICAgICBpZiAoaWR4ICE9PSAtMSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5vYnNlcnZlcnMuc3BsaWNlKGlkeCwgMSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG4gICAgICAgIHZhbGlkYXRlOiBmdW5jdGlvbiAoX2EpIHtcclxuICAgICAgICAgICAgdmFyIF9iID0gKF9hID09PSB2b2lkIDAgPyB7fSA6IF9hKS5zaWxlbnQsIHNpbGVudCA9IF9iID09PSB2b2lkIDAgPyBmYWxzZSA6IF9iO1xyXG4gICAgICAgICAgICByZXR1cm4gX19hd2FpdGVyKHRoaXMsIHZvaWQgMCwgdm9pZCAwLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgcmVzdWx0cztcclxuICAgICAgICAgICAgICAgIHJldHVybiBfX2dlbmVyYXRvcih0aGlzLCBmdW5jdGlvbiAoX2MpIHtcclxuICAgICAgICAgICAgICAgICAgICBzd2l0Y2ggKF9jLmxhYmVsKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhc2UgMDogcmV0dXJuIFs0IC8qeWllbGQqLywgUHJvbWlzZS5hbGwoX19zcHJlYWRBcnJheXModmFsdWVzKHRoaXMucmVmcylcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuZmlsdGVyKGZ1bmN0aW9uIChyKSB7IHJldHVybiAhci5kaXNhYmxlZDsgfSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAubWFwKGZ1bmN0aW9uIChyZWYpIHsgcmV0dXJuIHJlZltzaWxlbnQgPyAndmFsaWRhdGVTaWxlbnQnIDogJ3ZhbGlkYXRlJ10oKS50aGVuKGZ1bmN0aW9uIChyKSB7IHJldHVybiByLnZhbGlkOyB9KTsgfSksIHRoaXMub2JzZXJ2ZXJzLmZpbHRlcihmdW5jdGlvbiAobykgeyByZXR1cm4gIW8uZGlzYWJsZWQ7IH0pLm1hcChmdW5jdGlvbiAob2JzKSB7IHJldHVybiBvYnMudmFsaWRhdGUoeyBzaWxlbnQ6IHNpbGVudCB9KTsgfSkpKV07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhc2UgMTpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc3VsdHMgPSBfYy5zZW50KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gWzIgLypyZXR1cm4qLywgcmVzdWx0cy5ldmVyeShmdW5jdGlvbiAocikgeyByZXR1cm4gcjsgfSldO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIHJlc2V0OiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIHZhciBfdGhpcyA9IHRoaXM7XHJcbiAgICAgICAgICAgIE9iamVjdC5rZXlzKHRoaXMuaW5hY3RpdmVSZWZzKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcclxuICAgICAgICAgICAgICAgIF90aGlzLiRkZWxldGUoX3RoaXMuaW5hY3RpdmVSZWZzLCBrZXkpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgcmV0dXJuIF9fc3ByZWFkQXJyYXlzKHZhbHVlcyh0aGlzLnJlZnMpLCB0aGlzLm9ic2VydmVycykuZm9yRWFjaChmdW5jdGlvbiAocmVmKSB7IHJldHVybiByZWYucmVzZXQoKTsgfSk7XHJcbiAgICAgICAgfSxcclxuICAgICAgICByZXN0b3JlUHJvdmlkZXJTdGF0ZTogZnVuY3Rpb24gKHByb3ZpZGVyKSB7XHJcbiAgICAgICAgICAgIHZhciBpZCA9IHByb3ZpZGVyLmlkO1xyXG4gICAgICAgICAgICB2YXIgc3RhdGUgPSB0aGlzLmluYWN0aXZlUmVmc1tpZF07XHJcbiAgICAgICAgICAgIGlmICghc3RhdGUpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBwcm92aWRlci5zZXRGbGFncyhzdGF0ZS5mbGFncyk7XHJcbiAgICAgICAgICAgIHByb3ZpZGVyLmFwcGx5UmVzdWx0KHN0YXRlKTtcclxuICAgICAgICAgICAgdGhpcy4kZGVsZXRlKHRoaXMuaW5hY3RpdmVSZWZzLCBwcm92aWRlci5pZCk7XHJcbiAgICAgICAgfSxcclxuICAgICAgICByZW1vdmVQcm92aWRlcjogZnVuY3Rpb24gKGlkKSB7XHJcbiAgICAgICAgICAgIHZhciBwcm92aWRlciA9IHRoaXMucmVmc1tpZF07XHJcbiAgICAgICAgICAgIGlmICghcHJvdmlkZXIpIHtcclxuICAgICAgICAgICAgICAgIC8vIEZJWE1FOiBpbmFjdGl2ZSByZWZzIGFyZSBub3QgYmVpbmcgY2xlYW5lZCB1cC5cclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAocHJvdmlkZXIucGVyc2lzdCkge1xyXG4gICAgICAgICAgICAgICAgLyogaXN0YW5idWwgaWdub3JlIG5leHQgKi9cclxuICAgICAgICAgICAgICAgIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGlkLmluZGV4T2YoJ192ZWVfJykgPT09IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgd2FybignUGxlYXNlIHByb3ZpZGUgYSBgdmlkYCBvciBhIGBuYW1lYCBwcm9wIHdoZW4gdXNpbmcgYHBlcnNpc3RgLCB0aGVyZSBtaWdodCBiZSB1bmV4cGVjdGVkIGlzc3VlcyBvdGhlcndpc2UuJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgLy8gc2F2ZSBpdCBmb3IgdGhlIG5leHQgdGltZS5cclxuICAgICAgICAgICAgICAgIHRoaXMuaW5hY3RpdmVSZWZzW2lkXSA9IHtcclxuICAgICAgICAgICAgICAgICAgICBmbGFnczogcHJvdmlkZXIuZmxhZ3MsXHJcbiAgICAgICAgICAgICAgICAgICAgZXJyb3JzOiBwcm92aWRlci5tZXNzYWdlcyxcclxuICAgICAgICAgICAgICAgICAgICBmYWlsZWRSdWxlczogcHJvdmlkZXIuZmFpbGVkUnVsZXNcclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy4kZGVsZXRlKHRoaXMucmVmcywgaWQpO1xyXG4gICAgICAgICAgICB0aGlzLiRkZWxldGUodGhpcy5yZWZzQnlOYW1lLCBwcm92aWRlci5uYW1lKTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIHNldEVycm9yczogZnVuY3Rpb24gKGVycm9ycykge1xyXG4gICAgICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xyXG4gICAgICAgICAgICBPYmplY3Qua2V5cyhlcnJvcnMpLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xyXG4gICAgICAgICAgICAgICAgdmFyIHByb3ZpZGVyID0gX3RoaXMucmVmc1trZXldIHx8IF90aGlzLnJlZnNCeU5hbWVba2V5XTtcclxuICAgICAgICAgICAgICAgIGlmICghcHJvdmlkZXIpXHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgcHJvdmlkZXIuc2V0RXJyb3JzKGVycm9yc1trZXldIHx8IFtdKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIHRoaXMub2JzZXJ2ZXJzLmZvckVhY2goZnVuY3Rpb24gKG9ic2VydmVyKSB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5zZXRFcnJvcnMoZXJyb3JzKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59KTtcblxuZnVuY3Rpb24gd2l0aFZhbGlkYXRpb24oY29tcG9uZW50LCBtYXBQcm9wcykge1xyXG4gICAgaWYgKG1hcFByb3BzID09PSB2b2lkIDApIHsgbWFwUHJvcHMgPSBpZGVudGl0eTsgfVxyXG4gICAgdmFyIG9wdGlvbnMgPSAnb3B0aW9ucycgaW4gY29tcG9uZW50ID8gY29tcG9uZW50Lm9wdGlvbnMgOiBjb21wb25lbnQ7XHJcbiAgICB2YXIgcHJvdmlkZXJPcHRzID0gVmFsaWRhdGlvblByb3ZpZGVyLm9wdGlvbnM7XHJcbiAgICB2YXIgaG9jID0ge1xyXG4gICAgICAgIG5hbWU6IChvcHRpb25zLm5hbWUgfHwgJ0Fub255bW91c0hvYycpICsgXCJXaXRoVmFsaWRhdGlvblwiLFxyXG4gICAgICAgIHByb3BzOiBfX2Fzc2lnbih7fSwgcHJvdmlkZXJPcHRzLnByb3BzKSxcclxuICAgICAgICBkYXRhOiBwcm92aWRlck9wdHMuZGF0YSxcclxuICAgICAgICBjb21wdXRlZDogX19hc3NpZ24oe30sIHByb3ZpZGVyT3B0cy5jb21wdXRlZCksXHJcbiAgICAgICAgbWV0aG9kczogX19hc3NpZ24oe30sIHByb3ZpZGVyT3B0cy5tZXRob2RzKSxcclxuICAgICAgICBiZWZvcmVEZXN0cm95OiBwcm92aWRlck9wdHMuYmVmb3JlRGVzdHJveSxcclxuICAgICAgICBpbmplY3Q6IHByb3ZpZGVyT3B0cy5pbmplY3RcclxuICAgIH07XHJcbiAgICB2YXIgZXZlbnROYW1lID0gKG9wdGlvbnMubW9kZWwgJiYgb3B0aW9ucy5tb2RlbC5ldmVudCkgfHwgJ2lucHV0JztcclxuICAgIGhvYy5yZW5kZXIgPSBmdW5jdGlvbiAoaCkge1xyXG4gICAgICAgIHZhciBfYTtcclxuICAgICAgICB0aGlzLnJlZ2lzdGVyRmllbGQoKTtcclxuICAgICAgICB2YXIgdmN0eCA9IGNyZWF0ZVZhbGlkYXRpb25DdHgodGhpcyk7XHJcbiAgICAgICAgdmFyIGxpc3RlbmVycyA9IF9fYXNzaWduKHt9LCB0aGlzLiRsaXN0ZW5lcnMpO1xyXG4gICAgICAgIHZhciBtb2RlbCA9IGZpbmRNb2RlbCh0aGlzLiR2bm9kZSk7XHJcbiAgICAgICAgdGhpcy5faW5wdXRFdmVudE5hbWUgPSB0aGlzLl9pbnB1dEV2ZW50TmFtZSB8fCBnZXRJbnB1dEV2ZW50TmFtZSh0aGlzLiR2bm9kZSwgbW9kZWwpO1xyXG4gICAgICAgIHZhciB2YWx1ZSA9IGZpbmRWYWx1ZSh0aGlzLiR2bm9kZSk7XHJcbiAgICAgICAgb25SZW5kZXJVcGRhdGUodGhpcywgdmFsdWUgJiYgdmFsdWUudmFsdWUpO1xyXG4gICAgICAgIHZhciBfYiA9IGNyZWF0ZUNvbW1vbkhhbmRsZXJzKHRoaXMpLCBvbklucHV0ID0gX2Iub25JbnB1dCwgb25CbHVyID0gX2Iub25CbHVyLCBvblZhbGlkYXRlID0gX2Iub25WYWxpZGF0ZTtcclxuICAgICAgICBtZXJnZVZOb2RlTGlzdGVuZXJzKGxpc3RlbmVycywgZXZlbnROYW1lLCBvbklucHV0KTtcclxuICAgICAgICBtZXJnZVZOb2RlTGlzdGVuZXJzKGxpc3RlbmVycywgJ2JsdXInLCBvbkJsdXIpO1xyXG4gICAgICAgIHRoaXMubm9ybWFsaXplZEV2ZW50cy5mb3JFYWNoKGZ1bmN0aW9uIChldnQpIHtcclxuICAgICAgICAgICAgbWVyZ2VWTm9kZUxpc3RlbmVycyhsaXN0ZW5lcnMsIGV2dCwgb25WYWxpZGF0ZSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgLy8gUHJvcHMgYXJlIGFueSBhdHRycyBub3QgYXNzb2NpYXRlZCB3aXRoIFZhbGlkYXRpb25Qcm92aWRlciBQbHVzIHRoZSBtb2RlbCBwcm9wLlxyXG4gICAgICAgIC8vIFdBUk5JTkc6IEFjY2lkZW50YWwgcHJvcCBvdmVyd3JpdGUgd2lsbCBwcm9iYWJseSBoYXBwZW4uXHJcbiAgICAgICAgdmFyIHByb3AgPSAoZmluZE1vZGVsQ29uZmlnKHRoaXMuJHZub2RlKSB8fCB7IHByb3A6ICd2YWx1ZScgfSkucHJvcDtcclxuICAgICAgICB2YXIgcHJvcHMgPSBfX2Fzc2lnbihfX2Fzc2lnbihfX2Fzc2lnbih7fSwgdGhpcy4kYXR0cnMpLCAoX2EgPSB7fSwgX2FbcHJvcF0gPSBtb2RlbCAmJiBtb2RlbC52YWx1ZSwgX2EpKSwgbWFwUHJvcHModmN0eCkpO1xyXG4gICAgICAgIHJldHVybiBoKG9wdGlvbnMsIHtcclxuICAgICAgICAgICAgYXR0cnM6IHRoaXMuJGF0dHJzLFxyXG4gICAgICAgICAgICBwcm9wczogcHJvcHMsXHJcbiAgICAgICAgICAgIG9uOiBsaXN0ZW5lcnNcclxuICAgICAgICB9LCBub3JtYWxpemVTbG90cyh0aGlzLiRzbG90cywgdGhpcy4kdm5vZGUuY29udGV4dCkpO1xyXG4gICAgfTtcclxuICAgIHJldHVybiBob2M7XHJcbn1cblxudmFyIHZlcnNpb24gPSAnMy4wLjExJztcblxuZXhwb3J0IHsgVmFsaWRhdGlvbk9ic2VydmVyLCBWYWxpZGF0aW9uUHJvdmlkZXIsIGNvbmZpZ3VyZSwgZXh0ZW5kLCBpbnN0YWxsLCBsb2NhbGl6ZSwgc2V0SW50ZXJhY3Rpb25Nb2RlLCB2YWxpZGF0ZSwgdmVyc2lvbiwgd2l0aFZhbGlkYXRpb24gfTtcbiIsInZhciByZW5kZXIgPSBmdW5jdGlvbigpIHtcbiAgdmFyIF92bSA9IHRoaXNcbiAgdmFyIF9oID0gX3ZtLiRjcmVhdGVFbGVtZW50XG4gIHZhciBfYyA9IF92bS5fc2VsZi5fYyB8fCBfaFxuICByZXR1cm4gX2MoXG4gICAgXCJkaXZcIixcbiAgICB7IHN0YXRpY0NsYXNzOiBcIm1vZGFsIGZhZGUgbW9kYWwtZnNcIiwgYXR0cnM6IHsgaWQ6IFwibW9kYWwtZnNcIiB9IH0sXG4gICAgW1xuICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJtb2RhbC1kaWFsb2dcIiwgYXR0cnM6IHsgcm9sZTogXCJkb2N1bWVudFwiIH0gfSwgW1xuICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcIm1vZGFsLWNvbnRlbnRcIiB9LCBbXG4gICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJtb2RhbC1ib2R5XCIgfSwgW1xuICAgICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjb250YWluZXIgdGV4dC1jZW50ZXJcIiB9LCBbXG4gICAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwicm93IGZ1bGwtaGVpZ2h0IGFsaWduLWl0ZW1zLWNlbnRlclwiIH0sIFtcbiAgICAgICAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImNvbC1tZC01IG0taC1hdXRvXCIgfSwgW1xuICAgICAgICAgICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJwLWgtMzAgcC1iLTUwXCIgfSwgW1xuICAgICAgICAgICAgICAgICAgICBfdm0uX20oMCksXG4gICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgIF9jKFwiaDFcIiwgeyBzdGF0aWNDbGFzczogXCJ0ZXh0LXRoaW4gbS12LTE1XCIgfSwgW1xuICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihfdm0uX3MoX3ZtLnRpdGxlKSlcbiAgICAgICAgICAgICAgICAgICAgXSksXG4gICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgIF9jKFwicFwiLCB7IHN0YXRpY0NsYXNzOiBcIm0tYi0yNSBsZWFkXCIgfSwgW1xuICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihfdm0uX3MoX3ZtLmNvbnRlbnQpKVxuICAgICAgICAgICAgICAgICAgICBdKSxcbiAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJ0ZXh0LWNlbnRlclwiIH0sIFtcbiAgICAgICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiYnV0dG9uXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tZGVmYXVsdCBidG4tbGdcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgb246IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGljazogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkZXZlbnQucHJldmVudERlZmF1bHQoKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIF92bS5jb25maXJtKCRldmVudClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICBbX3ZtLl92KFwiQ29uZmlybVxcbiAgICAgICAgICAgICAgICAgIFwiKV1cbiAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICBdKSxcbiAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgXCJhXCIsXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJtb2RhbC1jbG9zZVwiLFxuICAgICAgICAgICAgICAgIGF0dHJzOiB7IGhyZWY6IFwiI1wiLCBcImRhdGEtZGlzbWlzc1wiOiBcIm1vZGFsXCIgfSxcbiAgICAgICAgICAgICAgICBvbjoge1xuICAgICAgICAgICAgICAgICAgY2xpY2s6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgICAgICAgICAgICAkZXZlbnQucHJldmVudERlZmF1bHQoKVxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gX3ZtLmNvbmZpcm0oJGV2ZW50KVxuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgW19jKFwiaVwiLCB7IHN0YXRpY0NsYXNzOiBcInRpLWNsb3NlXCIgfSldXG4gICAgICAgICAgICApXG4gICAgICAgICAgXSlcbiAgICAgICAgXSlcbiAgICAgIF0pXG4gICAgXVxuICApXG59XG52YXIgc3RhdGljUmVuZGVyRm5zID0gW1xuICBmdW5jdGlvbigpIHtcbiAgICB2YXIgX3ZtID0gdGhpc1xuICAgIHZhciBfaCA9IF92bS4kY3JlYXRlRWxlbWVudFxuICAgIHZhciBfYyA9IF92bS5fc2VsZi5fYyB8fCBfaFxuICAgIHJldHVybiBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcInRleHQtY2VudGVyIGZvbnQtc2l6ZS03MFwiIH0sIFtcbiAgICAgIF9jKFwiaVwiLCB7XG4gICAgICAgIHN0YXRpY0NsYXNzOlxuICAgICAgICAgIFwibWRpIG1kaS1jaGVja2JveC1tYXJrZWQtY2lyY2xlLW91dGxpbmUgaWNvbi1ncmFkaWVudC1zdWNjZXNzXCJcbiAgICAgIH0pXG4gICAgXSlcbiAgfVxuXVxucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5cbmV4cG9ydCB7IHJlbmRlciwgc3RhdGljUmVuZGVyRm5zIH0iLCJ2YXIgcmVuZGVyID0gZnVuY3Rpb24oKSB7XG4gIHZhciBfdm0gPSB0aGlzXG4gIHZhciBfaCA9IF92bS4kY3JlYXRlRWxlbWVudFxuICB2YXIgX2MgPSBfdm0uX3NlbGYuX2MgfHwgX2hcbiAgcmV0dXJuIF9jKFwicm91dGVyLXZpZXdcIilcbn1cbnZhciBzdGF0aWNSZW5kZXJGbnMgPSBbXVxucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5cbmV4cG9ydCB7IHJlbmRlciwgc3RhdGljUmVuZGVyRm5zIH0iLCJ2YXIgcmVuZGVyID0gZnVuY3Rpb24oKSB7XG4gIHZhciBfdm0gPSB0aGlzXG4gIHZhciBfaCA9IF92bS4kY3JlYXRlRWxlbWVudFxuICB2YXIgX2MgPSBfdm0uX3NlbGYuX2MgfHwgX2hcbiAgcmV0dXJuIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY29udGFpbmVyLWZsdWlkXCIgfSwgW1xuICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwicGFnZS1oZWFkZXJcIiB9LCBbXG4gICAgICBfYyhcImgyXCIsIHsgc3RhdGljQ2xhc3M6IFwiaGVhZGVyLXRpdGxlXCIgfSwgW192bS5fdihcIk5nxrDhu51pIGTDuW5nXCIpXSksXG4gICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJoZWFkZXItc3ViLXRpdGxlXCIgfSwgW1xuICAgICAgICBfYyhcIm5hdlwiLCB7IHN0YXRpY0NsYXNzOiBcImJyZWFkY3J1bWIgYnJlYWRjcnVtYi1kYXNoXCIgfSwgW1xuICAgICAgICAgIF92bS5fbSgwKSxcbiAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgIF9jKFxuICAgICAgICAgICAgXCJhXCIsXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImJyZWFkY3J1bWItaXRlbVwiLFxuICAgICAgICAgICAgICBhdHRyczogeyBocmVmOiBcIiNcIiB9LFxuICAgICAgICAgICAgICBvbjoge1xuICAgICAgICAgICAgICAgIGNsaWNrOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAgICRldmVudC5wcmV2ZW50RGVmYXVsdCgpXG4gICAgICAgICAgICAgICAgICByZXR1cm4gX3ZtLmdvUmVhZCgpXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgW19jKFwiaVwiLCB7IHN0YXRpY0NsYXNzOiBcInRpLXVzZXIgcC1yLTVcIiB9KSwgX3ZtLl92KFwiTmfGsOG7nWkgZMO5bmdcIildXG4gICAgICAgICAgKSxcbiAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgIF92bS5fbSgxKVxuICAgICAgICBdKVxuICAgICAgXSksXG4gICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJfaGVhZGVyLWJ1dHRvblwiIH0sIFtcbiAgICAgICAgX2MoXG4gICAgICAgICAgXCJidXR0b25cIixcbiAgICAgICAgICB7XG4gICAgICAgICAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLWdyYWRpZW50LXN1Y2Nlc3NcIixcbiAgICAgICAgICAgIG9uOiB7XG4gICAgICAgICAgICAgIGNsaWNrOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAkZXZlbnQucHJldmVudERlZmF1bHQoKVxuICAgICAgICAgICAgICAgIHJldHVybiBfdm0uZ29DcmVhdGUoKVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSxcbiAgICAgICAgICBbX3ZtLl92KFwiVOG6oW8gbmfGsOG7nWkgZMO5bmdcXG4gICAgICBcIildXG4gICAgICAgIClcbiAgICAgIF0pXG4gICAgXSksXG4gICAgX3ZtLl92KFwiIFwiKSxcbiAgICBfYyhcbiAgICAgIFwiZGl2XCIsXG4gICAgICB7IHN0YXRpY0NsYXNzOiBcImNhcmRcIiB9LFxuICAgICAgW1xuICAgICAgICBfYyhcInZhbGlkYXRpb24tb2JzZXJ2ZXJcIiwge1xuICAgICAgICAgIHJlZjogXCJvYnNlcnZlclwiLFxuICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImNhcmQtYm9keVwiLFxuICAgICAgICAgIGF0dHJzOiB7IHRhZzogXCJkaXZcIiB9LFxuICAgICAgICAgIHNjb3BlZFNsb3RzOiBfdm0uX3UoW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBrZXk6IFwiZGVmYXVsdFwiLFxuICAgICAgICAgICAgICBmbjogZnVuY3Rpb24ocmVmKSB7XG4gICAgICAgICAgICAgICAgdmFyIGludmFsaWQgPSByZWYuaW52YWxpZFxuICAgICAgICAgICAgICAgIHJldHVybiBbXG4gICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgXCJkaXZcIixcbiAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcInJvdyBtLXYtMzBcIixcbiAgICAgICAgICAgICAgICAgICAgICBvbjoge1xuICAgICAgICAgICAgICAgICAgICAgICAga2V5dXA6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgISRldmVudC50eXBlLmluZGV4T2YoXCJrZXlcIikgJiZcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX2soXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkZXZlbnQua2V5Q29kZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiZW50ZXJcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDEzLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJGV2ZW50LmtleSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiRW50ZXJcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG51bGxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gX3ZtLnVwZGF0ZShpbnZhbGlkKVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgW1xuICAgICAgICAgICAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY29sLXNtLTNcIiB9LCBbXG4gICAgICAgICAgICAgICAgICAgICAgICBfYyhcImltZ1wiLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImltZy1mbHVpZCBkLWJsb2NrIG14LWF1dG8gbS1iLTMwXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY1N0eWxlOiB7IHdpZHRoOiBcIjIwMHB4XCIsIGhlaWdodDogXCIyMDBweFwiIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgIGF0dHJzOiB7IHNyYzogX3ZtLnByb2ZpbGVJbWFnZSwgYWx0OiBcIlwiIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH0pLFxuICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwiaW5wdXRcIiwge1xuICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2xcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgYXR0cnM6IHsgdHlwZTogXCJmaWxlXCIgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgb246IHsgY2hhbmdlOiBfdm0udXBsb2FkIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgICAgXSksXG4gICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiZGl2XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICB7IHN0YXRpY0NsYXNzOiBcImNvbC1zbS00IHRleHQtY2VudGVyIHRleHQtc20tbGVmdFwiIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwidmFsaWRhdGlvbi1wcm92aWRlclwiLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF0dHJzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiBcIlTDqm4gbmfGsOG7nWkgZMO5bmdcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJ1bGVzOiBcInJlcXVpcmVkXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0YWc6IFwiZGl2XCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlZFNsb3RzOiBfdm0uX3UoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBrZXk6IFwiZGVmYXVsdFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZuOiBmdW5jdGlvbihyZWYpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBlcnJvcnMgPSByZWYuZXJyb3JzXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgdmFsaWQgPSByZWYudmFsaWRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwibGFiZWxcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJjb250cm9sLWxhYmVsXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhdHRyczogeyBmb3I6IFwiaWNvbi1pbnB1dFwiIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtfdm0uX3YoXCJOYW1lOlwiKV1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJpY29uLWlucHV0XCIgfSwgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwiaVwiLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJtZGkgbWRpLWFjY291bnRcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJpbnB1dFwiLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXJlY3RpdmVzOiBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IF92bS5mcm1EYXRhLmZ1bGxuYW1lLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV4cHJlc3Npb246IFwiZnJtRGF0YS5mdWxsbmFtZVwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2xcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5mbGFnLmlzVmFsaWRhdGluZyAmJiAhdmFsaWRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhdHRyczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiBcInRleHRcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI6IFwiRnVsbCBuYW1lLi4uXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkb21Qcm9wczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogX3ZtLmZybURhdGEuZnVsbG5hbWVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpbnB1dDogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLiRzZXQoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uZnJtRGF0YSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiZnVsbG5hbWVcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRldmVudC50YXJnZXQudmFsdWVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJsYWJlbFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpcmVjdGl2ZXM6IFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IFwic2hvd1wiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uZmxhZy5pc1ZhbGlkYXRpbmcgJiYgIXZhbGlkLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV4cHJlc3Npb246XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImZsYWcuaXNWYWxpZGF0aW5nICYmICF2YWxpZFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJlcnJvclwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbX3ZtLl92KF92bS5fcyhlcnJvcnNbMF0pKV1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bGwsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0cnVlXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICB9KSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJ2YWxpZGF0aW9uLXByb3ZpZGVyXCIsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYXR0cnM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IFwiRW1haWxcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJ1bGVzOiBcInJlcXVpcmVkfGVtYWlsXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0YWc6IFwiZGl2XCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlZFNsb3RzOiBfdm0uX3UoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBrZXk6IFwiZGVmYXVsdFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZuOiBmdW5jdGlvbihyZWYpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBlcnJvcnMgPSByZWYuZXJyb3JzXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgdmFsaWQgPSByZWYudmFsaWRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwibGFiZWxcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJjb250cm9sLWxhYmVsXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhdHRyczogeyBmb3I6IFwiaWNvbi1pbnB1dFwiIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtfdm0uX3YoXCJFbWFpbDpcIildXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiaWNvbi1pbnB1dFwiIH0sIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcImlcIiwge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwibWRpIG1kaS1lbWFpbC1vdXRsaW5lXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwiaW5wdXRcIiwge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlyZWN0aXZlczogW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiBfdm0uZnJtRGF0YS5lbWFpbCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBleHByZXNzaW9uOiBcImZybURhdGEuZW1haWxcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBlcnJvcjpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uZmxhZy5pc1ZhbGlkYXRpbmcgJiYgIXZhbGlkXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXR0cnM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogXCJ0ZXh0XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyOiBcIkVtYWlsLi4uXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkb21Qcm9wczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogX3ZtLmZybURhdGEuZW1haWxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpbnB1dDogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLiRzZXQoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uZnJtRGF0YSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiZW1haWxcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRldmVudC50YXJnZXQudmFsdWVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJsYWJlbFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpcmVjdGl2ZXM6IFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IFwic2hvd1wiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uZmxhZy5pc1ZhbGlkYXRpbmcgJiYgIXZhbGlkLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV4cHJlc3Npb246XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImZsYWcuaXNWYWxpZGF0aW5nICYmICF2YWxpZFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJlcnJvclwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbX3ZtLl92KF92bS5fcyhlcnJvcnNbMF0pKV1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bGwsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0cnVlXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICB9KSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjaGVja2JveFwiIH0sIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcImlucHV0XCIsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpcmVjdGl2ZXM6IFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogX3ZtLmZybURhdGEuaXNfYWN0aXZlZCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBleHByZXNzaW9uOiBcImZybURhdGEuaXNfYWN0aXZlZFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhdHRyczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZDogXCJjaGVja2JveDFcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogXCJjaGVja2JveFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInRydWUtdmFsdWVcIjogXCIxXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiZmFsc2UtdmFsdWVcIjogXCIwXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkb21Qcm9wczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjaGVja2VkOiBBcnJheS5pc0FycmF5KF92bS5mcm1EYXRhLmlzX2FjdGl2ZWQpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPyBfdm0uX2koX3ZtLmZybURhdGEuaXNfYWN0aXZlZCwgbnVsbCkgPiAtMVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDogX3ZtLl9xKF92bS5mcm1EYXRhLmlzX2FjdGl2ZWQsIFwiMVwiKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNoYW5nZTogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyICQkYSA9IF92bS5mcm1EYXRhLmlzX2FjdGl2ZWQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkJGVsID0gJGV2ZW50LnRhcmdldCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICQkYyA9ICQkZWwuY2hlY2tlZCA/IFwiMVwiIDogXCIwXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoQXJyYXkuaXNBcnJheSgkJGEpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgJCR2ID0gbnVsbCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJCRpID0gX3ZtLl9pKCQkYSwgJCR2KVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCQkZWwuY2hlY2tlZCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkJGkgPCAwICYmXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLiRzZXQoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uZnJtRGF0YSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiaXNfYWN0aXZlZFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJCRhLmNvbmNhdChbJCR2XSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkJGkgPiAtMSAmJlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS4kc2V0KFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLmZybURhdGEsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImlzX2FjdGl2ZWRcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICQkYVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuc2xpY2UoMCwgJCRpKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuY29uY2F0KCQkYS5zbGljZSgkJGkgKyAxKSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS4kc2V0KF92bS5mcm1EYXRhLCBcImlzX2FjdGl2ZWRcIiwgJCRjKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJsYWJlbFwiLCB7IGF0dHJzOiB7IGZvcjogXCJjaGVja2JveDFcIiB9IH0sIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIkvDrWNoIGhv4bqhdFwiKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgICAgICAgICBdLFxuICAgICAgICAgICAgICAgICAgICAgICAgMVxuICAgICAgICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiZGl2XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICB7IHN0YXRpY0NsYXNzOiBcImNvbFwiIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwidmFsaWRhdGlvbi1wcm92aWRlclwiLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF0dHJzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiBcIlPEkFRcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJ1bGVzOiBcInJlcXVpcmVkXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0YWc6IFwiZGl2XCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlZFNsb3RzOiBfdm0uX3UoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBrZXk6IFwiZGVmYXVsdFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZuOiBmdW5jdGlvbihyZWYpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBlcnJvcnMgPSByZWYuZXJyb3JzXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgdmFsaWQgPSByZWYudmFsaWRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwibGFiZWxcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJjb250cm9sLWxhYmVsXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhdHRyczogeyBmb3I6IFwiaWNvbi1pbnB1dFwiIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtfdm0uX3YoXCJQaG9uZTpcIildXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiaWNvbi1pbnB1dFwiIH0sIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcImlcIiwge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwibWRpIG1kaS1jZWxscGhvbmVcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJpbnB1dFwiLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXJlY3RpdmVzOiBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IF92bS5mcm1EYXRhLnBob25lLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV4cHJlc3Npb246IFwiZnJtRGF0YS5waG9uZVwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2xcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5mbGFnLmlzVmFsaWRhdGluZyAmJiAhdmFsaWRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhdHRyczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiBcInRleHRcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI6IFwiUGhvbmUuLi5cIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRvbVByb3BzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiBfdm0uZnJtRGF0YS5waG9uZVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlucHV0OiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoJGV2ZW50LnRhcmdldC5jb21wb3NpbmcpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVyblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uJHNldChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5mcm1EYXRhLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJwaG9uZVwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJGV2ZW50LnRhcmdldC52YWx1ZVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImxhYmVsXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlyZWN0aXZlczogW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmF3TmFtZTogXCJ2LXNob3dcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5mbGFnLmlzVmFsaWRhdGluZyAmJiAhdmFsaWQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZXhwcmVzc2lvbjpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiZmxhZy5pc1ZhbGlkYXRpbmcgJiYgIXZhbGlkXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImVycm9yXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtfdm0uX3YoX3ZtLl9zKGVycm9yc1swXSkpXVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbnVsbCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRydWVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICApLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1vZGVsOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogX3ZtLmZybURhdGEucGhvbmUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYWxsYmFjazogZnVuY3Rpb24oJCR2KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS4kc2V0KF92bS5mcm1EYXRhLCBcInBob25lXCIsICQkdilcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBleHByZXNzaW9uOiBcImZybURhdGEucGhvbmVcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgfSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwidmFsaWRhdGlvbi1wcm92aWRlclwiLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF0dHJzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiBcIsSQ4buLYSBjaOG7iVwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcnVsZXM6IFwicmVxdWlyZWRcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRhZzogXCJkaXZcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGVkU2xvdHM6IF92bS5fdShcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGtleTogXCJkZWZhdWx0XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZm46IGZ1bmN0aW9uKHJlZikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGVycm9ycyA9IHJlZi5lcnJvcnNcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciB2YWxpZCA9IHJlZi52YWxpZFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJsYWJlbFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImNvbnRyb2wtbGFiZWxcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF0dHJzOiB7IGZvcjogXCJpY29uLWlucHV0XCIgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgW192bS5fdihcIsSQ4buLYSBjaOG7iTpcIildXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiaWNvbi1pbnB1dFwiIH0sIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcImlcIiwge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwibWRpIG1kaS1hY2NvdW50LWJveFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcImlucHV0XCIsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpcmVjdGl2ZXM6IFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogX3ZtLmZybURhdGEuYWRkcmVzcyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBleHByZXNzaW9uOiBcImZybURhdGEuYWRkcmVzc1wiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2xcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5mbGFnLmlzVmFsaWRhdGluZyAmJiAhdmFsaWRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhdHRyczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiBcInRleHRcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI6IFwiQWRkcmVzcy4uLlwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZG9tUHJvcHM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IF92bS5mcm1EYXRhLmFkZHJlc3NcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpbnB1dDogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLiRzZXQoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uZnJtRGF0YSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiYWRkcmVzc1wiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJGV2ZW50LnRhcmdldC52YWx1ZVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImxhYmVsXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlyZWN0aXZlczogW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmF3TmFtZTogXCJ2LXNob3dcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5mbGFnLmlzVmFsaWRhdGluZyAmJiAhdmFsaWQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZXhwcmVzc2lvbjpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiZmxhZy5pc1ZhbGlkYXRpbmcgJiYgIXZhbGlkXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImVycm9yXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtfdm0uX3YoX3ZtLl9zKGVycm9yc1swXSkpXVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbnVsbCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRydWVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgIH0pLFxuICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIiB9LCBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImxhYmVsXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImNvbnRyb2wtbGFiZWxcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXR0cnM6IHsgZm9yOiBcImljb24taW5wdXRcIiB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgW192bS5fdihcIlBhc3N3b3JkOlwiKV1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICApLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJpY29uLWlucHV0XCIgfSwgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJpXCIsIHsgc3RhdGljQ2xhc3M6IFwibWRpIG1kaS1hY2NvdW50LWtleVwiIH0pLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwiaW5wdXRcIiwge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXJlY3RpdmVzOiBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogX3ZtLmZybURhdGEucGFzc3dvcmQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBleHByZXNzaW9uOiBcImZybURhdGEucGFzc3dvcmRcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF0dHJzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogXCJwYXNzd29yZFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyOiBcIlBhc3N3b3JkLi4uXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZG9tUHJvcHM6IHsgdmFsdWU6IF92bS5mcm1EYXRhLnBhc3N3b3JkIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaW5wdXQ6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVyblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLiRzZXQoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5mcm1EYXRhLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInBhc3N3b3JkXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRldmVudC50YXJnZXQudmFsdWVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgXSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwibS10LTI1XCIgfSwgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJidXR0b25cIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1zdWNjZXNzXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF0dHJzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ6IFwic2F2ZVwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6IFwiYnV0dG9uXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ6IF92bS5mbGFnLmlzVXBkYXRpbmdcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb246IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGljazogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkZXZlbnQucHJldmVudERlZmF1bHQoKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIF92bS51cGRhdGUoaW52YWxpZClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5mbGFnLmlzVXBkYXRpbmdcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA/IFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJpXCIsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiZmEgZmEtc3Bpbm5lciBmYS1wdWxzZSBmYS1mdyBtLXItNVwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCJVcGRhdGluZ1xcbiAgICAgICAgICAgICAgXCIpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgOiBbX3ZtLl92KFwiVXBkYXRlXCIpXVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICApLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImJ1dHRvblwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLWRlZmF1bHRcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXR0cnM6IHsgaWQ6IFwiZWRpdFwiLCB0eXBlOiBcImJ1dHRvblwiIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xpY2s6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJGV2ZW50LnByZXZlbnREZWZhdWx0KClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBfdm0uYmFjaygkZXZlbnQpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcImlcIiwgeyBzdGF0aWNDbGFzczogXCJ0aS1iYWNrLWxlZnQgcC1yLTEwXCIgfSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIkJhY2tcXG4gICAgICAgICAgICBcIilcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgICAgICAgICBdLFxuICAgICAgICAgICAgICAgICAgICAgICAgMVxuICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgIF0pXG4gICAgICAgIH0pXG4gICAgICBdLFxuICAgICAgMVxuICAgIClcbiAgXSlcbn1cbnZhciBzdGF0aWNSZW5kZXJGbnMgPSBbXG4gIGZ1bmN0aW9uKCkge1xuICAgIHZhciBfdm0gPSB0aGlzXG4gICAgdmFyIF9oID0gX3ZtLiRjcmVhdGVFbGVtZW50XG4gICAgdmFyIF9jID0gX3ZtLl9zZWxmLl9jIHx8IF9oXG4gICAgcmV0dXJuIF9jKFxuICAgICAgXCJhXCIsXG4gICAgICB7IHN0YXRpY0NsYXNzOiBcImJyZWFkY3J1bWItaXRlbVwiLCBhdHRyczogeyBocmVmOiBcIi9iYWNrZW5kXCIgfSB9LFxuICAgICAgW19jKFwiaVwiLCB7IHN0YXRpY0NsYXNzOiBcInRpLWhvbWUgcC1yLTVcIiB9KSwgX3ZtLl92KFwiSOG7hyB0aOG7kW5nXCIpXVxuICAgIClcbiAgfSxcbiAgZnVuY3Rpb24oKSB7XG4gICAgdmFyIF92bSA9IHRoaXNcbiAgICB2YXIgX2ggPSBfdm0uJGNyZWF0ZUVsZW1lbnRcbiAgICB2YXIgX2MgPSBfdm0uX3NlbGYuX2MgfHwgX2hcbiAgICByZXR1cm4gX2MoXCJzcGFuXCIsIHsgc3RhdGljQ2xhc3M6IFwiYnJlYWRjcnVtYi1pdGVtIGFjdGl2ZVwiIH0sIFtcbiAgICAgIF9jKFwiaVwiLCB7IHN0YXRpY0NsYXNzOiBcInRpLXBlbmNpbC1hbHQgcC1yLTVcIiB9KSxcbiAgICAgIF92bS5fdihcIlThuqFvIG5nxrDhu51pIGTDuW5nXCIpXG4gICAgXSlcbiAgfVxuXVxucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5cbmV4cG9ydCB7IHJlbmRlciwgc3RhdGljUmVuZGVyRm5zIH0iLCJ2YXIgcmVuZGVyID0gZnVuY3Rpb24oKSB7XG4gIHZhciBfdm0gPSB0aGlzXG4gIHZhciBfaCA9IF92bS4kY3JlYXRlRWxlbWVudFxuICB2YXIgX2MgPSBfdm0uX3NlbGYuX2MgfHwgX2hcbiAgcmV0dXJuIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY29udGFpbmVyLWZsdWlkXCIgfSwgW1xuICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwicGFnZS1oZWFkZXJcIiB9LCBbXG4gICAgICBfYyhcImgyXCIsIHsgc3RhdGljQ2xhc3M6IFwiaGVhZGVyLXRpdGxlXCIgfSwgW192bS5fdihcIk5nxrDhu51pIGTDuW5nXCIpXSksXG4gICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJoZWFkZXItc3ViLXRpdGxlXCIgfSwgW1xuICAgICAgICBfYyhcIm5hdlwiLCB7IHN0YXRpY0NsYXNzOiBcImJyZWFkY3J1bWIgYnJlYWRjcnVtYi1kYXNoXCIgfSwgW1xuICAgICAgICAgIF92bS5fbSgwKSxcbiAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgIF9jKFxuICAgICAgICAgICAgXCJhXCIsXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImJyZWFkY3J1bWItaXRlbVwiLFxuICAgICAgICAgICAgICBhdHRyczogeyBocmVmOiBcIiNcIiB9LFxuICAgICAgICAgICAgICBvbjoge1xuICAgICAgICAgICAgICAgIGNsaWNrOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAgICRldmVudC5wcmV2ZW50RGVmYXVsdCgpXG4gICAgICAgICAgICAgICAgICByZXR1cm4gX3ZtLmdvUmVhZCgpXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgW19jKFwiaVwiLCB7IHN0YXRpY0NsYXNzOiBcInRpLXVzZXIgcC1yLTVcIiB9KSwgX3ZtLl92KFwiTmfGsOG7nWkgZMO5bmdcIildXG4gICAgICAgICAgKSxcbiAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgIF92bS5fbSgxKVxuICAgICAgICBdKVxuICAgICAgXSksXG4gICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJfaGVhZGVyLWJ1dHRvblwiIH0sIFtcbiAgICAgICAgX2MoXG4gICAgICAgICAgXCJidXR0b25cIixcbiAgICAgICAgICB7XG4gICAgICAgICAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLWdyYWRpZW50LXN1Y2Nlc3NcIixcbiAgICAgICAgICAgIG9uOiB7XG4gICAgICAgICAgICAgIGNsaWNrOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAkZXZlbnQucHJldmVudERlZmF1bHQoKVxuICAgICAgICAgICAgICAgIHJldHVybiBfdm0uZ29DcmVhdGUoKVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSxcbiAgICAgICAgICBbX3ZtLl92KFwiVOG6oW8gbmfGsOG7nWkgZMO5bmdcXG4gICAgICBcIildXG4gICAgICAgIClcbiAgICAgIF0pXG4gICAgXSksXG4gICAgX3ZtLl92KFwiIFwiKSxcbiAgICBfYyhcbiAgICAgIFwiZGl2XCIsXG4gICAgICB7IHN0YXRpY0NsYXNzOiBcImNhcmRcIiB9LFxuICAgICAgW1xuICAgICAgICBfYyhcInZhbGlkYXRpb24tb2JzZXJ2ZXJcIiwge1xuICAgICAgICAgIHJlZjogXCJvYnNlcnZlclwiLFxuICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImNhcmQtYm9keVwiLFxuICAgICAgICAgIGF0dHJzOiB7IHRhZzogXCJkaXZcIiB9LFxuICAgICAgICAgIHNjb3BlZFNsb3RzOiBfdm0uX3UoW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBrZXk6IFwiZGVmYXVsdFwiLFxuICAgICAgICAgICAgICBmbjogZnVuY3Rpb24ocmVmKSB7XG4gICAgICAgICAgICAgICAgdmFyIGludmFsaWQgPSByZWYuaW52YWxpZFxuICAgICAgICAgICAgICAgIHJldHVybiBbXG4gICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgXCJkaXZcIixcbiAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcInJvdyBtLXYtMzBcIixcbiAgICAgICAgICAgICAgICAgICAgICBvbjoge1xuICAgICAgICAgICAgICAgICAgICAgICAga2V5dXA6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgISRldmVudC50eXBlLmluZGV4T2YoXCJrZXlcIikgJiZcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX2soXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkZXZlbnQua2V5Q29kZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiZW50ZXJcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDEzLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJGV2ZW50LmtleSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiRW50ZXJcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG51bGxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gX3ZtLnVwZGF0ZShpbnZhbGlkKVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgW1xuICAgICAgICAgICAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY29sLXNtLTNcIiB9LCBbXG4gICAgICAgICAgICAgICAgICAgICAgICBfYyhcImltZ1wiLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImltZy1mbHVpZCBkLWJsb2NrIG14LWF1dG8gbS1iLTMwXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY1N0eWxlOiB7IHdpZHRoOiBcIjIwMHB4XCIsIGhlaWdodDogXCIyMDBweFwiIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgIGF0dHJzOiB7IHNyYzogX3ZtLnByb2ZpbGVJbWFnZSwgYWx0OiBcIlwiIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH0pLFxuICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwiaW5wdXRcIiwge1xuICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2xcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgYXR0cnM6IHsgdHlwZTogXCJmaWxlXCIgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgb246IHsgY2hhbmdlOiBfdm0udXBsb2FkIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgICAgXSksXG4gICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiZGl2XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICB7IHN0YXRpY0NsYXNzOiBcImNvbC1zbS00IHRleHQtY2VudGVyIHRleHQtc20tbGVmdFwiIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwidmFsaWRhdGlvbi1wcm92aWRlclwiLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF0dHJzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiBcIlTDqm4gbmfGsOG7nWkgZMO5bmdcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJ1bGVzOiBcInJlcXVpcmVkXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0YWc6IFwiZGl2XCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlZFNsb3RzOiBfdm0uX3UoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBrZXk6IFwiZGVmYXVsdFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZuOiBmdW5jdGlvbihyZWYpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBlcnJvcnMgPSByZWYuZXJyb3JzXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgdmFsaWQgPSByZWYudmFsaWRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwibGFiZWxcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJjb250cm9sLWxhYmVsXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhdHRyczogeyBmb3I6IFwiaWNvbi1pbnB1dFwiIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtfdm0uX3YoXCJOYW1lOlwiKV1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJpY29uLWlucHV0XCIgfSwgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwiaVwiLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJtZGkgbWRpLWFjY291bnRcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJpbnB1dFwiLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXJlY3RpdmVzOiBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IF92bS5mcm1EYXRhLmZ1bGxuYW1lLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV4cHJlc3Npb246IFwiZnJtRGF0YS5mdWxsbmFtZVwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2xcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5mbGFnLmlzVmFsaWRhdGluZyAmJiAhdmFsaWRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhdHRyczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiBcInRleHRcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI6IFwiRnVsbCBuYW1lLi4uXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkb21Qcm9wczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogX3ZtLmZybURhdGEuZnVsbG5hbWVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpbnB1dDogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLiRzZXQoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uZnJtRGF0YSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiZnVsbG5hbWVcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRldmVudC50YXJnZXQudmFsdWVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJsYWJlbFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpcmVjdGl2ZXM6IFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IFwic2hvd1wiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uZmxhZy5pc1ZhbGlkYXRpbmcgJiYgIXZhbGlkLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV4cHJlc3Npb246XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImZsYWcuaXNWYWxpZGF0aW5nICYmICF2YWxpZFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJlcnJvclwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbX3ZtLl92KF92bS5fcyhlcnJvcnNbMF0pKV1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bGwsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0cnVlXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICB9KSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJ2YWxpZGF0aW9uLXByb3ZpZGVyXCIsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYXR0cnM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IFwiRW1haWxcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJ1bGVzOiBcInJlcXVpcmVkfGVtYWlsXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0YWc6IFwiZGl2XCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlZFNsb3RzOiBfdm0uX3UoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBrZXk6IFwiZGVmYXVsdFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZuOiBmdW5jdGlvbihyZWYpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBlcnJvcnMgPSByZWYuZXJyb3JzXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgdmFsaWQgPSByZWYudmFsaWRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwibGFiZWxcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJjb250cm9sLWxhYmVsXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhdHRyczogeyBmb3I6IFwiaWNvbi1pbnB1dFwiIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtfdm0uX3YoXCJFbWFpbDpcIildXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiaWNvbi1pbnB1dFwiIH0sIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcImlcIiwge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwibWRpIG1kaS1lbWFpbC1vdXRsaW5lXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwiaW5wdXRcIiwge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlyZWN0aXZlczogW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJhd05hbWU6IFwidi1tb2RlbFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiBfdm0uZnJtRGF0YS5lbWFpbCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBleHByZXNzaW9uOiBcImZybURhdGEuZW1haWxcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBlcnJvcjpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uZmxhZy5pc1ZhbGlkYXRpbmcgJiYgIXZhbGlkXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXR0cnM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogXCJ0ZXh0XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyOiBcIkVtYWlsLi4uXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkb21Qcm9wczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogX3ZtLmZybURhdGEuZW1haWxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpbnB1dDogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLiRzZXQoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uZnJtRGF0YSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiZW1haWxcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRldmVudC50YXJnZXQudmFsdWVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJsYWJlbFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpcmVjdGl2ZXM6IFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IFwic2hvd1wiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uZmxhZy5pc1ZhbGlkYXRpbmcgJiYgIXZhbGlkLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV4cHJlc3Npb246XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImZsYWcuaXNWYWxpZGF0aW5nICYmICF2YWxpZFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJlcnJvclwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbX3ZtLl92KF92bS5fcyhlcnJvcnNbMF0pKV1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bGwsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0cnVlXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICB9KSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjaGVja2JveFwiIH0sIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcImlucHV0XCIsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpcmVjdGl2ZXM6IFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogX3ZtLmZybURhdGEuaXNfYWN0aXZlZCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBleHByZXNzaW9uOiBcImZybURhdGEuaXNfYWN0aXZlZFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhdHRyczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZDogXCJjaGVja2JveDFcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogXCJjaGVja2JveFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInRydWUtdmFsdWVcIjogXCIxXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiZmFsc2UtdmFsdWVcIjogXCIwXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkb21Qcm9wczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjaGVja2VkOiBBcnJheS5pc0FycmF5KF92bS5mcm1EYXRhLmlzX2FjdGl2ZWQpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPyBfdm0uX2koX3ZtLmZybURhdGEuaXNfYWN0aXZlZCwgbnVsbCkgPiAtMVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDogX3ZtLl9xKF92bS5mcm1EYXRhLmlzX2FjdGl2ZWQsIFwiMVwiKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNoYW5nZTogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyICQkYSA9IF92bS5mcm1EYXRhLmlzX2FjdGl2ZWQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkJGVsID0gJGV2ZW50LnRhcmdldCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICQkYyA9ICQkZWwuY2hlY2tlZCA/IFwiMVwiIDogXCIwXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoQXJyYXkuaXNBcnJheSgkJGEpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgJCR2ID0gbnVsbCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJCRpID0gX3ZtLl9pKCQkYSwgJCR2KVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCQkZWwuY2hlY2tlZCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkJGkgPCAwICYmXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLiRzZXQoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uZnJtRGF0YSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiaXNfYWN0aXZlZFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJCRhLmNvbmNhdChbJCR2XSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkJGkgPiAtMSAmJlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS4kc2V0KFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLmZybURhdGEsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImlzX2FjdGl2ZWRcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICQkYVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuc2xpY2UoMCwgJCRpKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuY29uY2F0KCQkYS5zbGljZSgkJGkgKyAxKSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS4kc2V0KF92bS5mcm1EYXRhLCBcImlzX2FjdGl2ZWRcIiwgJCRjKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJsYWJlbFwiLCB7IGF0dHJzOiB7IGZvcjogXCJjaGVja2JveDFcIiB9IH0sIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIkvDrWNoIGhv4bqhdFwiKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgICAgICAgICBdLFxuICAgICAgICAgICAgICAgICAgICAgICAgMVxuICAgICAgICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiZGl2XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICB7IHN0YXRpY0NsYXNzOiBcImNvbFwiIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwidmFsaWRhdGlvbi1wcm92aWRlclwiLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF0dHJzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiBcIlPEkFRcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJ1bGVzOiBcInJlcXVpcmVkXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0YWc6IFwiZGl2XCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlZFNsb3RzOiBfdm0uX3UoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBrZXk6IFwiZGVmYXVsdFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZuOiBmdW5jdGlvbihyZWYpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBlcnJvcnMgPSByZWYuZXJyb3JzXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgdmFsaWQgPSByZWYudmFsaWRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwibGFiZWxcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJjb250cm9sLWxhYmVsXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhdHRyczogeyBmb3I6IFwiaWNvbi1pbnB1dFwiIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtfdm0uX3YoXCJQaG9uZTpcIildXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiaWNvbi1pbnB1dFwiIH0sIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcImlcIiwge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwibWRpIG1kaS1jZWxscGhvbmVcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJpbnB1dFwiLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXJlY3RpdmVzOiBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IF92bS5mcm1EYXRhLnBob25lLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV4cHJlc3Npb246IFwiZnJtRGF0YS5waG9uZVwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2xcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5mbGFnLmlzVmFsaWRhdGluZyAmJiAhdmFsaWRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhdHRyczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiBcInRleHRcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI6IFwiUGhvbmUuLi5cIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRvbVByb3BzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiBfdm0uZnJtRGF0YS5waG9uZVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlucHV0OiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoJGV2ZW50LnRhcmdldC5jb21wb3NpbmcpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVyblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uJHNldChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5mcm1EYXRhLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJwaG9uZVwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJGV2ZW50LnRhcmdldC52YWx1ZVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImxhYmVsXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlyZWN0aXZlczogW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmF3TmFtZTogXCJ2LXNob3dcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5mbGFnLmlzVmFsaWRhdGluZyAmJiAhdmFsaWQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZXhwcmVzc2lvbjpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiZmxhZy5pc1ZhbGlkYXRpbmcgJiYgIXZhbGlkXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImVycm9yXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtfdm0uX3YoX3ZtLl9zKGVycm9yc1swXSkpXVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbnVsbCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRydWVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICApLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1vZGVsOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogX3ZtLmZybURhdGEucGhvbmUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYWxsYmFjazogZnVuY3Rpb24oJCR2KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS4kc2V0KF92bS5mcm1EYXRhLCBcInBob25lXCIsICQkdilcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBleHByZXNzaW9uOiBcImZybURhdGEucGhvbmVcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgfSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwidmFsaWRhdGlvbi1wcm92aWRlclwiLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF0dHJzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiBcIsSQ4buLYSBjaOG7iVwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcnVsZXM6IFwicmVxdWlyZWRcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRhZzogXCJkaXZcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGVkU2xvdHM6IF92bS5fdShcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGtleTogXCJkZWZhdWx0XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZm46IGZ1bmN0aW9uKHJlZikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGVycm9ycyA9IHJlZi5lcnJvcnNcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciB2YWxpZCA9IHJlZi52YWxpZFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJsYWJlbFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImNvbnRyb2wtbGFiZWxcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF0dHJzOiB7IGZvcjogXCJpY29uLWlucHV0XCIgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgW192bS5fdihcIsSQ4buLYSBjaOG7iTpcIildXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiaWNvbi1pbnB1dFwiIH0sIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcImlcIiwge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwibWRpIG1kaS1hY2NvdW50LWJveFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcImlucHV0XCIsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpcmVjdGl2ZXM6IFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogX3ZtLmZybURhdGEuYWRkcmVzcyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBleHByZXNzaW9uOiBcImZybURhdGEuYWRkcmVzc1wiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJmb3JtLWNvbnRyb2xcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5mbGFnLmlzVmFsaWRhdGluZyAmJiAhdmFsaWRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhdHRyczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiBcInRleHRcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI6IFwiQWRkcmVzcy4uLlwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZG9tUHJvcHM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IF92bS5mcm1EYXRhLmFkZHJlc3NcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpbnB1dDogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLiRzZXQoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uZnJtRGF0YSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiYWRkcmVzc1wiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJGV2ZW50LnRhcmdldC52YWx1ZVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImxhYmVsXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGlyZWN0aXZlczogW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmF3TmFtZTogXCJ2LXNob3dcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5mbGFnLmlzVmFsaWRhdGluZyAmJiAhdmFsaWQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZXhwcmVzc2lvbjpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiZmxhZy5pc1ZhbGlkYXRpbmcgJiYgIXZhbGlkXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImVycm9yXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtfdm0uX3YoX3ZtLl9zKGVycm9yc1swXSkpXVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbnVsbCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRydWVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgIH0pLFxuICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImZvcm0tZ3JvdXBcIiB9LCBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImxhYmVsXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImNvbnRyb2wtbGFiZWxcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXR0cnM6IHsgZm9yOiBcImljb24taW5wdXRcIiB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgW192bS5fdihcIlBhc3N3b3JkOlwiKV1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICApLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJpY29uLWlucHV0XCIgfSwgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJpXCIsIHsgc3RhdGljQ2xhc3M6IFwibWRpIG1kaS1hY2NvdW50LWtleVwiIH0pLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwiaW5wdXRcIiwge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXJlY3RpdmVzOiBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogXCJtb2RlbFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogX3ZtLmZybURhdGEucGFzc3dvcmQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBleHByZXNzaW9uOiBcImZybURhdGEucGFzc3dvcmRcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF0dHJzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogXCJwYXNzd29yZFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyOiBcIlBhc3N3b3JkLi4uXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZG9tUHJvcHM6IHsgdmFsdWU6IF92bS5mcm1EYXRhLnBhc3N3b3JkIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaW5wdXQ6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCRldmVudC50YXJnZXQuY29tcG9zaW5nKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVyblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLiRzZXQoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5mcm1EYXRhLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInBhc3N3b3JkXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRldmVudC50YXJnZXQudmFsdWVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgXSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwibS10LTI1XCIgfSwgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJidXR0b25cIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1kZWZhdWx0XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF0dHJzOiB7IGlkOiBcImVkaXRcIiwgdHlwZTogXCJidXR0b25cIiB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsaWNrOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRldmVudC5wcmV2ZW50RGVmYXVsdCgpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gX3ZtLmJhY2soJGV2ZW50KVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtfdm0uX3YoXCJCYWNrXFxuICAgICAgICAgICAgXCIpXVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiYnV0dG9uXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tc3VjY2Vzc1wiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhdHRyczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkOiBcInNhdmVcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiBcImJ1dHRvblwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpc2FibGVkOiBfdm0uZmxhZy5pc1VwZGF0aW5nXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xpY2s6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJGV2ZW50LnByZXZlbnREZWZhdWx0KClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBfdm0udXBkYXRlKGludmFsaWQpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uZmxhZy5pc1VwZGF0aW5nXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPyBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwiaVwiLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImZhIGZhLXNwaW5uZXIgZmEtcHVsc2UgZmEtZncgbS1yLTVcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiVXBkYXRpbmdcXG4gICAgICAgICAgICAgIFwiKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDogW192bS5fdihcIlVwZGF0ZVwiKV1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgICAgICAgICAgICAgXSxcbiAgICAgICAgICAgICAgICAgICAgICAgIDFcbiAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdKVxuICAgICAgICB9KVxuICAgICAgXSxcbiAgICAgIDFcbiAgICApXG4gIF0pXG59XG52YXIgc3RhdGljUmVuZGVyRm5zID0gW1xuICBmdW5jdGlvbigpIHtcbiAgICB2YXIgX3ZtID0gdGhpc1xuICAgIHZhciBfaCA9IF92bS4kY3JlYXRlRWxlbWVudFxuICAgIHZhciBfYyA9IF92bS5fc2VsZi5fYyB8fCBfaFxuICAgIHJldHVybiBfYyhcbiAgICAgIFwiYVwiLFxuICAgICAgeyBzdGF0aWNDbGFzczogXCJicmVhZGNydW1iLWl0ZW1cIiwgYXR0cnM6IHsgaHJlZjogXCIvYmFja2VuZFwiIH0gfSxcbiAgICAgIFtfYyhcImlcIiwgeyBzdGF0aWNDbGFzczogXCJ0aS1ob21lIHAtci01XCIgfSksIF92bS5fdihcIkjhu4cgdGjhu5FuZ1wiKV1cbiAgICApXG4gIH0sXG4gIGZ1bmN0aW9uKCkge1xuICAgIHZhciBfdm0gPSB0aGlzXG4gICAgdmFyIF9oID0gX3ZtLiRjcmVhdGVFbGVtZW50XG4gICAgdmFyIF9jID0gX3ZtLl9zZWxmLl9jIHx8IF9oXG4gICAgcmV0dXJuIF9jKFwic3BhblwiLCB7IHN0YXRpY0NsYXNzOiBcImJyZWFkY3J1bWItaXRlbSBhY3RpdmVcIiB9LCBbXG4gICAgICBfYyhcImlcIiwgeyBzdGF0aWNDbGFzczogXCJ0aS1wZW5jaWwtYWx0IHAtci01XCIgfSksXG4gICAgICBfdm0uX3YoXCJD4bqtcCBuaOG6rXQgbmfGsOG7nWkgZMO5bmdcIilcbiAgICBdKVxuICB9XG5dXG5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcblxuZXhwb3J0IHsgcmVuZGVyLCBzdGF0aWNSZW5kZXJGbnMgfSIsInZhciByZW5kZXIgPSBmdW5jdGlvbigpIHtcbiAgdmFyIF92bSA9IHRoaXNcbiAgdmFyIF9oID0gX3ZtLiRjcmVhdGVFbGVtZW50XG4gIHZhciBfYyA9IF92bS5fc2VsZi5fYyB8fCBfaFxuICByZXR1cm4gX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjb250YWluZXItZmx1aWRcIiB9LCBbXG4gICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJwYWdlLWhlYWRlclwiIH0sIFtcbiAgICAgIF9jKFwiaDJcIiwgeyBzdGF0aWNDbGFzczogXCJoZWFkZXItdGl0bGVcIiB9LCBbX3ZtLl92KFwiTmfGsOG7nWkgZMO5bmdcIildKSxcbiAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImhlYWRlci1zdWItdGl0bGVcIiB9LCBbXG4gICAgICAgIF9jKFwibmF2XCIsIHsgc3RhdGljQ2xhc3M6IFwiYnJlYWRjcnVtYiBicmVhZGNydW1iLWRhc2hcIiB9LCBbXG4gICAgICAgICAgX3ZtLl9tKDApLFxuICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgX2MoXG4gICAgICAgICAgICBcImFcIixcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiYnJlYWRjcnVtYi1pdGVtXCIsXG4gICAgICAgICAgICAgIGF0dHJzOiB7IGhyZWY6IFwiI1wiIH0sXG4gICAgICAgICAgICAgIG9uOiB7XG4gICAgICAgICAgICAgICAgY2xpY2s6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgICAgICAgICAgJGV2ZW50LnByZXZlbnREZWZhdWx0KClcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBbX2MoXCJpXCIsIHsgc3RhdGljQ2xhc3M6IFwidGktdXNlciBwLXItNVwiIH0pLCBfdm0uX3YoXCJOZ8aw4budaSBkw7luZ1wiKV1cbiAgICAgICAgICApLFxuICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgX3ZtLl9tKDEpXG4gICAgICAgIF0pXG4gICAgICBdKSxcbiAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcIl9oZWFkZXItYnV0dG9uXCIgfSwgW1xuICAgICAgICBfYyhcbiAgICAgICAgICBcImJ1dHRvblwiLFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tZ3JhZGllbnQtc3VjY2Vzc1wiLFxuICAgICAgICAgICAgb246IHtcbiAgICAgICAgICAgICAgY2xpY2s6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgICAgICAgICRldmVudC5wcmV2ZW50RGVmYXVsdCgpXG4gICAgICAgICAgICAgICAgcmV0dXJuIF92bS5nb0NyZWF0ZSgpXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9LFxuICAgICAgICAgIFtfdm0uX3YoXCJU4bqhbyBuZ8aw4budaSBkw7luZ1xcbiAgICAgIFwiKV1cbiAgICAgICAgKVxuICAgICAgXSlcbiAgICBdKSxcbiAgICBfdm0uX3YoXCIgXCIpLFxuICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY2FyZFwiIH0sIFtcbiAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY2FyZC1ib2R5XCIgfSwgW19jKFwidXNlci10YWJsZVwiKV0sIDEpXG4gICAgXSlcbiAgXSlcbn1cbnZhciBzdGF0aWNSZW5kZXJGbnMgPSBbXG4gIGZ1bmN0aW9uKCkge1xuICAgIHZhciBfdm0gPSB0aGlzXG4gICAgdmFyIF9oID0gX3ZtLiRjcmVhdGVFbGVtZW50XG4gICAgdmFyIF9jID0gX3ZtLl9zZWxmLl9jIHx8IF9oXG4gICAgcmV0dXJuIF9jKFxuICAgICAgXCJhXCIsXG4gICAgICB7IHN0YXRpY0NsYXNzOiBcImJyZWFkY3J1bWItaXRlbVwiLCBhdHRyczogeyBocmVmOiBcIi9iYWNrZW5kXCIgfSB9LFxuICAgICAgW19jKFwiaVwiLCB7IHN0YXRpY0NsYXNzOiBcInRpLWhvbWUgcC1yLTVcIiB9KSwgX3ZtLl92KFwiSOG7hyB0aOG7kW5nXCIpXVxuICAgIClcbiAgfSxcbiAgZnVuY3Rpb24oKSB7XG4gICAgdmFyIF92bSA9IHRoaXNcbiAgICB2YXIgX2ggPSBfdm0uJGNyZWF0ZUVsZW1lbnRcbiAgICB2YXIgX2MgPSBfdm0uX3NlbGYuX2MgfHwgX2hcbiAgICByZXR1cm4gX2MoXCJzcGFuXCIsIHsgc3RhdGljQ2xhc3M6IFwiYnJlYWRjcnVtYi1pdGVtIGFjdGl2ZVwiIH0sIFtcbiAgICAgIF9jKFwiaVwiLCB7IHN0YXRpY0NsYXNzOiBcInRpLWxpc3QgcC1yLTVcIiB9KSxcbiAgICAgIF92bS5fdihcIkRhbmggc8OhY2hcIilcbiAgICBdKVxuICB9XG5dXG5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcblxuZXhwb3J0IHsgcmVuZGVyLCBzdGF0aWNSZW5kZXJGbnMgfSIsInZhciByZW5kZXIgPSBmdW5jdGlvbigpIHtcbiAgdmFyIF92bSA9IHRoaXNcbiAgdmFyIF9oID0gX3ZtLiRjcmVhdGVFbGVtZW50XG4gIHZhciBfYyA9IF92bS5fc2VsZi5fYyB8fCBfaFxuICByZXR1cm4gX2MoXG4gICAgXCJkaXZcIixcbiAgICB7IHN0YXRpY0NsYXNzOiBcInRhYmxlLW92ZXJmbG93XCIsIGF0dHJzOiB7IGlkOiBcInVzZXItdGFibGVcIiB9IH0sXG4gICAgW1xuICAgICAgX2MoXG4gICAgICAgIFwidGFibGVcIixcbiAgICAgICAgeyBzdGF0aWNDbGFzczogXCJ0YWJsZSB0YWJsZS1ob3ZlciB0YWJsZS14bFwiLCBhdHRyczogeyBpZDogXCJkdC1vcHRcIiB9IH0sXG4gICAgICAgIFtcbiAgICAgICAgICBfdm0uX20oMCksXG4gICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICBfYyhcbiAgICAgICAgICAgIFwidGJvZHlcIixcbiAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgX3ZtLl9sKF92bS5hcnJFbnRpdHlMaXN0LCBmdW5jdGlvbihvYmpFbnRpdHksIGluZGV4KSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIFtcbiAgICAgICAgICAgICAgICAgIF9jKFwidXNlci10YWJsZS1yb3dcIiwge1xuICAgICAgICAgICAgICAgICAgICBhdHRyczogeyBpbmRleDogaW5kZXgsIFwib2JqLWVudGl0eVwiOiBvYmpFbnRpdHkgfVxuICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgMlxuICAgICAgICAgIClcbiAgICAgICAgXVxuICAgICAgKVxuICAgIF1cbiAgKVxufVxudmFyIHN0YXRpY1JlbmRlckZucyA9IFtcbiAgZnVuY3Rpb24oKSB7XG4gICAgdmFyIF92bSA9IHRoaXNcbiAgICB2YXIgX2ggPSBfdm0uJGNyZWF0ZUVsZW1lbnRcbiAgICB2YXIgX2MgPSBfdm0uX3NlbGYuX2MgfHwgX2hcbiAgICByZXR1cm4gX2MoXCJ0aGVhZFwiLCBbXG4gICAgICBfYyhcInRyXCIsIFtcbiAgICAgICAgX2MoXCJ0aFwiLCBbXG4gICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjaGVja2JveCBwLTBcIiB9LCBbXG4gICAgICAgICAgICBfYyhcImlucHV0XCIsIHtcbiAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiY2hlY2tBbGxcIixcbiAgICAgICAgICAgICAgYXR0cnM6IHsgaWQ6IFwic2VsZWN0YWJsZTFcIiwgdHlwZTogXCJjaGVja2JveFwiLCBuYW1lOiBcImNoZWNrQWxsXCIgfVxuICAgICAgICAgICAgfSksXG4gICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgX2MoXCJsYWJlbFwiLCB7IGF0dHJzOiB7IGZvcjogXCJzZWxlY3RhYmxlMVwiIH0gfSlcbiAgICAgICAgICBdKVxuICAgICAgICBdKSxcbiAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgX2MoXCJ0aFwiLCBbX3ZtLl92KFwiRlVMTE5BTUVcIildKSxcbiAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgX2MoXCJ0aFwiLCBbX3ZtLl92KFwiRU1BSUxcIildKSxcbiAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgX2MoXCJ0aFwiLCBbX3ZtLl92KFwiU1RBVFVTXCIpXSksXG4gICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgIF9jKFwidGhcIiwgW192bS5fdihcIiNcIildKSxcbiAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgX2MoXCJ0aFwiKVxuICAgICAgXSlcbiAgICBdKVxuICB9XG5dXG5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcblxuZXhwb3J0IHsgcmVuZGVyLCBzdGF0aWNSZW5kZXJGbnMgfSIsInZhciByZW5kZXIgPSBmdW5jdGlvbigpIHtcbiAgdmFyIF92bSA9IHRoaXNcbiAgdmFyIF9oID0gX3ZtLiRjcmVhdGVFbGVtZW50XG4gIHZhciBfYyA9IF92bS5fc2VsZi5fYyB8fCBfaFxuICByZXR1cm4gX2MoXCJ0clwiLCBbXG4gICAgX3ZtLl9tKDApLFxuICAgIF92bS5fdihcIiBcIiksXG4gICAgX2MoXCJ0ZFwiLCBbXG4gICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImxpc3QtbWVkaWFcIiB9LCBbXG4gICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwibGlzdC1pdGVtXCIgfSwgW1xuICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwibWVkaWEtaW1nXCIgfSwgW1xuICAgICAgICAgICAgX2MoXCJpbWdcIiwgeyBhdHRyczogeyBzcmM6IF92bS5wcm9maWxlSW1hZ2UsIGFsdDogXCJcIiB9IH0pXG4gICAgICAgICAgXSksXG4gICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImluZm9cIiB9LCBbXG4gICAgICAgICAgICBfYyhcInNwYW5cIiwgeyBzdGF0aWNDbGFzczogXCJ0aXRsZVwiIH0sIFtcbiAgICAgICAgICAgICAgX3ZtLl92KF92bS5fcyhfdm0ub2JqRW50aXR5LmZ1bGxuYW1lKSlcbiAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgIF9jKFwic3BhblwiLCB7IHN0YXRpY0NsYXNzOiBcInN1Yi10aXRsZVwiIH0sIFtcbiAgICAgICAgICAgICAgX3ZtLl92KFwiSUQgXCIgKyBfdm0uX3MoX3ZtLm9iakVudGl0eS51c2VyX2lkKSlcbiAgICAgICAgICAgIF0pXG4gICAgICAgICAgXSlcbiAgICAgICAgXSlcbiAgICAgIF0pXG4gICAgXSksXG4gICAgX3ZtLl92KFwiIFwiKSxcbiAgICBfYyhcInRkXCIsIFtfdm0uX3YoX3ZtLl9zKF92bS5vYmpFbnRpdHkuZW1haWwpKV0pLFxuICAgIF92bS5fdihcIiBcIiksXG4gICAgX2MoXG4gICAgICBcInRkXCIsXG4gICAgICBbXG4gICAgICAgIF92bS5vYmpFbnRpdHkuaXNfYWN0aXZlZCA9PSAxXG4gICAgICAgICAgPyBbXG4gICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgIFwic3BhblwiLFxuICAgICAgICAgICAgICAgIHsgc3RhdGljQ2xhc3M6IFwiYmFkZ2UgYmFkZ2UtcGlsbCBiYWRnZS1ncmFkaWVudC1zdWNjZXNzXCIgfSxcbiAgICAgICAgICAgICAgICBbX3ZtLl92KFwixJDDoyBrw61jaCBob+G6oXRcIildXG4gICAgICAgICAgICAgIClcbiAgICAgICAgICAgIF1cbiAgICAgICAgICA6IFtcbiAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgXCJzcGFuXCIsXG4gICAgICAgICAgICAgICAgeyBzdGF0aWNDbGFzczogXCJiYWRnZSBiYWRnZS1waWxsIGJhZGdlLWdyYWRpZW50LWRhbmdlclwiIH0sXG4gICAgICAgICAgICAgICAgW192bS5fdihcIkNoxrBhIGvDrWNoIGhv4bqhdFwiKV1cbiAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgXVxuICAgICAgXSxcbiAgICAgIDJcbiAgICApLFxuICAgIF92bS5fdihcIiBcIiksXG4gICAgX2MoXCJ0ZFwiLCB7IHN0YXRpY0NsYXNzOiBcInRleHQtY2VudGVyIGZvbnQtc2l6ZS0xOFwiIH0sIFtcbiAgICAgIF9jKFxuICAgICAgICBcImFcIixcbiAgICAgICAge1xuICAgICAgICAgIHN0YXRpY0NsYXNzOiBcInRleHQtZ3JheSBtLXItMTVcIixcbiAgICAgICAgICBvbjoge1xuICAgICAgICAgICAgY2xpY2s6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgICAgICByZXR1cm4gX3ZtLmdvVXBkYXRlKClcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIFtfYyhcImlcIiwgeyBzdGF0aWNDbGFzczogXCJ0aS1wZW5jaWxcIiB9KV1cbiAgICAgICksXG4gICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgX2MoXG4gICAgICAgIFwiYVwiLFxuICAgICAgICB7XG4gICAgICAgICAgc3RhdGljQ2xhc3M6IFwidGV4dC1ncmF5XCIsXG4gICAgICAgICAgb246IHtcbiAgICAgICAgICAgIGNsaWNrOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIF92bS5yZW1vdmUoX3ZtLm9iakVudGl0eS51c2VyX2lkKVxuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgW1xuICAgICAgICAgIF9jKFwiaVwiLCB7XG4gICAgICAgICAgICBjbGFzczoge1xuICAgICAgICAgICAgICBcImZhIGZhLXNwaW5uZXIgZmEtcHVsc2UgZmEtZndcIjogX3ZtLmZsYWcuaXNEZWxldGluZyxcbiAgICAgICAgICAgICAgXCJ0aS10cmFzaFwiOiAhX3ZtLmZsYWcuaXNEZWxldGluZ1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0pXG4gICAgICAgIF1cbiAgICAgIClcbiAgICBdKVxuICBdKVxufVxudmFyIHN0YXRpY1JlbmRlckZucyA9IFtcbiAgZnVuY3Rpb24oKSB7XG4gICAgdmFyIF92bSA9IHRoaXNcbiAgICB2YXIgX2ggPSBfdm0uJGNyZWF0ZUVsZW1lbnRcbiAgICB2YXIgX2MgPSBfdm0uX3NlbGYuX2MgfHwgX2hcbiAgICByZXR1cm4gX2MoXCJ0ZFwiLCBbXG4gICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImNoZWNrYm94XCIgfSwgW1xuICAgICAgICBfYyhcImlucHV0XCIsIHsgYXR0cnM6IHsgaWQ6IFwic2VsZWN0YWJsZTJcIiwgdHlwZTogXCJjaGVja2JveFwiIH0gfSksXG4gICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgIF9jKFwibGFiZWxcIiwgeyBhdHRyczogeyBmb3I6IFwic2VsZWN0YWJsZTJcIiB9IH0pXG4gICAgICBdKVxuICAgIF0pXG4gIH1cbl1cbnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuXG5leHBvcnQgeyByZW5kZXIsIHN0YXRpY1JlbmRlckZucyB9IiwiLyohXG4gICogdnVlLXJvdXRlciB2My4xLjNcbiAgKiAoYykgMjAxOSBFdmFuIFlvdVxuICAqIEBsaWNlbnNlIE1JVFxuICAqL1xuLyogICovXG5cbmZ1bmN0aW9uIGFzc2VydCAoY29uZGl0aW9uLCBtZXNzYWdlKSB7XG4gIGlmICghY29uZGl0aW9uKSB7XG4gICAgdGhyb3cgbmV3IEVycm9yKChcIlt2dWUtcm91dGVyXSBcIiArIG1lc3NhZ2UpKVxuICB9XG59XG5cbmZ1bmN0aW9uIHdhcm4gKGNvbmRpdGlvbiwgbWVzc2FnZSkge1xuICBpZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJyAmJiAhY29uZGl0aW9uKSB7XG4gICAgdHlwZW9mIGNvbnNvbGUgIT09ICd1bmRlZmluZWQnICYmIGNvbnNvbGUud2FybigoXCJbdnVlLXJvdXRlcl0gXCIgKyBtZXNzYWdlKSk7XG4gIH1cbn1cblxuZnVuY3Rpb24gaXNFcnJvciAoZXJyKSB7XG4gIHJldHVybiBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwoZXJyKS5pbmRleE9mKCdFcnJvcicpID4gLTFcbn1cblxuZnVuY3Rpb24gaXNFeHRlbmRlZEVycm9yIChjb25zdHJ1Y3RvciwgZXJyKSB7XG4gIHJldHVybiAoXG4gICAgZXJyIGluc3RhbmNlb2YgY29uc3RydWN0b3IgfHxcbiAgICAvLyBfbmFtZSBpcyB0byBzdXBwb3J0IElFOSB0b29cbiAgICAoZXJyICYmIChlcnIubmFtZSA9PT0gY29uc3RydWN0b3IubmFtZSB8fCBlcnIuX25hbWUgPT09IGNvbnN0cnVjdG9yLl9uYW1lKSlcbiAgKVxufVxuXG5mdW5jdGlvbiBleHRlbmQgKGEsIGIpIHtcbiAgZm9yICh2YXIga2V5IGluIGIpIHtcbiAgICBhW2tleV0gPSBiW2tleV07XG4gIH1cbiAgcmV0dXJuIGFcbn1cblxudmFyIFZpZXcgPSB7XG4gIG5hbWU6ICdSb3V0ZXJWaWV3JyxcbiAgZnVuY3Rpb25hbDogdHJ1ZSxcbiAgcHJvcHM6IHtcbiAgICBuYW1lOiB7XG4gICAgICB0eXBlOiBTdHJpbmcsXG4gICAgICBkZWZhdWx0OiAnZGVmYXVsdCdcbiAgICB9XG4gIH0sXG4gIHJlbmRlcjogZnVuY3Rpb24gcmVuZGVyIChfLCByZWYpIHtcbiAgICB2YXIgcHJvcHMgPSByZWYucHJvcHM7XG4gICAgdmFyIGNoaWxkcmVuID0gcmVmLmNoaWxkcmVuO1xuICAgIHZhciBwYXJlbnQgPSByZWYucGFyZW50O1xuICAgIHZhciBkYXRhID0gcmVmLmRhdGE7XG5cbiAgICAvLyB1c2VkIGJ5IGRldnRvb2xzIHRvIGRpc3BsYXkgYSByb3V0ZXItdmlldyBiYWRnZVxuICAgIGRhdGEucm91dGVyVmlldyA9IHRydWU7XG5cbiAgICAvLyBkaXJlY3RseSB1c2UgcGFyZW50IGNvbnRleHQncyBjcmVhdGVFbGVtZW50KCkgZnVuY3Rpb25cbiAgICAvLyBzbyB0aGF0IGNvbXBvbmVudHMgcmVuZGVyZWQgYnkgcm91dGVyLXZpZXcgY2FuIHJlc29sdmUgbmFtZWQgc2xvdHNcbiAgICB2YXIgaCA9IHBhcmVudC4kY3JlYXRlRWxlbWVudDtcbiAgICB2YXIgbmFtZSA9IHByb3BzLm5hbWU7XG4gICAgdmFyIHJvdXRlID0gcGFyZW50LiRyb3V0ZTtcbiAgICB2YXIgY2FjaGUgPSBwYXJlbnQuX3JvdXRlclZpZXdDYWNoZSB8fCAocGFyZW50Ll9yb3V0ZXJWaWV3Q2FjaGUgPSB7fSk7XG5cbiAgICAvLyBkZXRlcm1pbmUgY3VycmVudCB2aWV3IGRlcHRoLCBhbHNvIGNoZWNrIHRvIHNlZSBpZiB0aGUgdHJlZVxuICAgIC8vIGhhcyBiZWVuIHRvZ2dsZWQgaW5hY3RpdmUgYnV0IGtlcHQtYWxpdmUuXG4gICAgdmFyIGRlcHRoID0gMDtcbiAgICB2YXIgaW5hY3RpdmUgPSBmYWxzZTtcbiAgICB3aGlsZSAocGFyZW50ICYmIHBhcmVudC5fcm91dGVyUm9vdCAhPT0gcGFyZW50KSB7XG4gICAgICB2YXIgdm5vZGVEYXRhID0gcGFyZW50LiR2bm9kZSAmJiBwYXJlbnQuJHZub2RlLmRhdGE7XG4gICAgICBpZiAodm5vZGVEYXRhKSB7XG4gICAgICAgIGlmICh2bm9kZURhdGEucm91dGVyVmlldykge1xuICAgICAgICAgIGRlcHRoKys7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHZub2RlRGF0YS5rZWVwQWxpdmUgJiYgcGFyZW50Ll9pbmFjdGl2ZSkge1xuICAgICAgICAgIGluYWN0aXZlID0gdHJ1ZTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgcGFyZW50ID0gcGFyZW50LiRwYXJlbnQ7XG4gICAgfVxuICAgIGRhdGEucm91dGVyVmlld0RlcHRoID0gZGVwdGg7XG5cbiAgICAvLyByZW5kZXIgcHJldmlvdXMgdmlldyBpZiB0aGUgdHJlZSBpcyBpbmFjdGl2ZSBhbmQga2VwdC1hbGl2ZVxuICAgIGlmIChpbmFjdGl2ZSkge1xuICAgICAgcmV0dXJuIGgoY2FjaGVbbmFtZV0sIGRhdGEsIGNoaWxkcmVuKVxuICAgIH1cblxuICAgIHZhciBtYXRjaGVkID0gcm91dGUubWF0Y2hlZFtkZXB0aF07XG4gICAgLy8gcmVuZGVyIGVtcHR5IG5vZGUgaWYgbm8gbWF0Y2hlZCByb3V0ZVxuICAgIGlmICghbWF0Y2hlZCkge1xuICAgICAgY2FjaGVbbmFtZV0gPSBudWxsO1xuICAgICAgcmV0dXJuIGgoKVxuICAgIH1cblxuICAgIHZhciBjb21wb25lbnQgPSBjYWNoZVtuYW1lXSA9IG1hdGNoZWQuY29tcG9uZW50c1tuYW1lXTtcblxuICAgIC8vIGF0dGFjaCBpbnN0YW5jZSByZWdpc3RyYXRpb24gaG9va1xuICAgIC8vIHRoaXMgd2lsbCBiZSBjYWxsZWQgaW4gdGhlIGluc3RhbmNlJ3MgaW5qZWN0ZWQgbGlmZWN5Y2xlIGhvb2tzXG4gICAgZGF0YS5yZWdpc3RlclJvdXRlSW5zdGFuY2UgPSBmdW5jdGlvbiAodm0sIHZhbCkge1xuICAgICAgLy8gdmFsIGNvdWxkIGJlIHVuZGVmaW5lZCBmb3IgdW5yZWdpc3RyYXRpb25cbiAgICAgIHZhciBjdXJyZW50ID0gbWF0Y2hlZC5pbnN0YW5jZXNbbmFtZV07XG4gICAgICBpZiAoXG4gICAgICAgICh2YWwgJiYgY3VycmVudCAhPT0gdm0pIHx8XG4gICAgICAgICghdmFsICYmIGN1cnJlbnQgPT09IHZtKVxuICAgICAgKSB7XG4gICAgICAgIG1hdGNoZWQuaW5zdGFuY2VzW25hbWVdID0gdmFsO1xuICAgICAgfVxuICAgIH1cblxuICAgIC8vIGFsc28gcmVnaXN0ZXIgaW5zdGFuY2UgaW4gcHJlcGF0Y2ggaG9va1xuICAgIC8vIGluIGNhc2UgdGhlIHNhbWUgY29tcG9uZW50IGluc3RhbmNlIGlzIHJldXNlZCBhY3Jvc3MgZGlmZmVyZW50IHJvdXRlc1xuICAgIDsoZGF0YS5ob29rIHx8IChkYXRhLmhvb2sgPSB7fSkpLnByZXBhdGNoID0gZnVuY3Rpb24gKF8sIHZub2RlKSB7XG4gICAgICBtYXRjaGVkLmluc3RhbmNlc1tuYW1lXSA9IHZub2RlLmNvbXBvbmVudEluc3RhbmNlO1xuICAgIH07XG5cbiAgICAvLyByZWdpc3RlciBpbnN0YW5jZSBpbiBpbml0IGhvb2tcbiAgICAvLyBpbiBjYXNlIGtlcHQtYWxpdmUgY29tcG9uZW50IGJlIGFjdGl2ZWQgd2hlbiByb3V0ZXMgY2hhbmdlZFxuICAgIGRhdGEuaG9vay5pbml0ID0gZnVuY3Rpb24gKHZub2RlKSB7XG4gICAgICBpZiAodm5vZGUuZGF0YS5rZWVwQWxpdmUgJiZcbiAgICAgICAgdm5vZGUuY29tcG9uZW50SW5zdGFuY2UgJiZcbiAgICAgICAgdm5vZGUuY29tcG9uZW50SW5zdGFuY2UgIT09IG1hdGNoZWQuaW5zdGFuY2VzW25hbWVdXG4gICAgICApIHtcbiAgICAgICAgbWF0Y2hlZC5pbnN0YW5jZXNbbmFtZV0gPSB2bm9kZS5jb21wb25lbnRJbnN0YW5jZTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgLy8gcmVzb2x2ZSBwcm9wc1xuICAgIHZhciBwcm9wc1RvUGFzcyA9IGRhdGEucHJvcHMgPSByZXNvbHZlUHJvcHMocm91dGUsIG1hdGNoZWQucHJvcHMgJiYgbWF0Y2hlZC5wcm9wc1tuYW1lXSk7XG4gICAgaWYgKHByb3BzVG9QYXNzKSB7XG4gICAgICAvLyBjbG9uZSB0byBwcmV2ZW50IG11dGF0aW9uXG4gICAgICBwcm9wc1RvUGFzcyA9IGRhdGEucHJvcHMgPSBleHRlbmQoe30sIHByb3BzVG9QYXNzKTtcbiAgICAgIC8vIHBhc3Mgbm9uLWRlY2xhcmVkIHByb3BzIGFzIGF0dHJzXG4gICAgICB2YXIgYXR0cnMgPSBkYXRhLmF0dHJzID0gZGF0YS5hdHRycyB8fCB7fTtcbiAgICAgIGZvciAodmFyIGtleSBpbiBwcm9wc1RvUGFzcykge1xuICAgICAgICBpZiAoIWNvbXBvbmVudC5wcm9wcyB8fCAhKGtleSBpbiBjb21wb25lbnQucHJvcHMpKSB7XG4gICAgICAgICAgYXR0cnNba2V5XSA9IHByb3BzVG9QYXNzW2tleV07XG4gICAgICAgICAgZGVsZXRlIHByb3BzVG9QYXNzW2tleV07XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gaChjb21wb25lbnQsIGRhdGEsIGNoaWxkcmVuKVxuICB9XG59O1xuXG5mdW5jdGlvbiByZXNvbHZlUHJvcHMgKHJvdXRlLCBjb25maWcpIHtcbiAgc3dpdGNoICh0eXBlb2YgY29uZmlnKSB7XG4gICAgY2FzZSAndW5kZWZpbmVkJzpcbiAgICAgIHJldHVyblxuICAgIGNhc2UgJ29iamVjdCc6XG4gICAgICByZXR1cm4gY29uZmlnXG4gICAgY2FzZSAnZnVuY3Rpb24nOlxuICAgICAgcmV0dXJuIGNvbmZpZyhyb3V0ZSlcbiAgICBjYXNlICdib29sZWFuJzpcbiAgICAgIHJldHVybiBjb25maWcgPyByb3V0ZS5wYXJhbXMgOiB1bmRlZmluZWRcbiAgICBkZWZhdWx0OlxuICAgICAgaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcbiAgICAgICAgd2FybihcbiAgICAgICAgICBmYWxzZSxcbiAgICAgICAgICBcInByb3BzIGluIFxcXCJcIiArIChyb3V0ZS5wYXRoKSArIFwiXFxcIiBpcyBhIFwiICsgKHR5cGVvZiBjb25maWcpICsgXCIsIFwiICtcbiAgICAgICAgICBcImV4cGVjdGluZyBhbiBvYmplY3QsIGZ1bmN0aW9uIG9yIGJvb2xlYW4uXCJcbiAgICAgICAgKTtcbiAgICAgIH1cbiAgfVxufVxuXG4vKiAgKi9cblxudmFyIGVuY29kZVJlc2VydmVSRSA9IC9bIScoKSpdL2c7XG52YXIgZW5jb2RlUmVzZXJ2ZVJlcGxhY2VyID0gZnVuY3Rpb24gKGMpIHsgcmV0dXJuICclJyArIGMuY2hhckNvZGVBdCgwKS50b1N0cmluZygxNik7IH07XG52YXIgY29tbWFSRSA9IC8lMkMvZztcblxuLy8gZml4ZWQgZW5jb2RlVVJJQ29tcG9uZW50IHdoaWNoIGlzIG1vcmUgY29uZm9ybWFudCB0byBSRkMzOTg2OlxuLy8gLSBlc2NhcGVzIFshJygpKl1cbi8vIC0gcHJlc2VydmUgY29tbWFzXG52YXIgZW5jb2RlID0gZnVuY3Rpb24gKHN0cikgeyByZXR1cm4gZW5jb2RlVVJJQ29tcG9uZW50KHN0cilcbiAgLnJlcGxhY2UoZW5jb2RlUmVzZXJ2ZVJFLCBlbmNvZGVSZXNlcnZlUmVwbGFjZXIpXG4gIC5yZXBsYWNlKGNvbW1hUkUsICcsJyk7IH07XG5cbnZhciBkZWNvZGUgPSBkZWNvZGVVUklDb21wb25lbnQ7XG5cbmZ1bmN0aW9uIHJlc29sdmVRdWVyeSAoXG4gIHF1ZXJ5LFxuICBleHRyYVF1ZXJ5LFxuICBfcGFyc2VRdWVyeVxuKSB7XG4gIGlmICggZXh0cmFRdWVyeSA9PT0gdm9pZCAwICkgZXh0cmFRdWVyeSA9IHt9O1xuXG4gIHZhciBwYXJzZSA9IF9wYXJzZVF1ZXJ5IHx8IHBhcnNlUXVlcnk7XG4gIHZhciBwYXJzZWRRdWVyeTtcbiAgdHJ5IHtcbiAgICBwYXJzZWRRdWVyeSA9IHBhcnNlKHF1ZXJ5IHx8ICcnKTtcbiAgfSBjYXRjaCAoZSkge1xuICAgIHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicgJiYgd2FybihmYWxzZSwgZS5tZXNzYWdlKTtcbiAgICBwYXJzZWRRdWVyeSA9IHt9O1xuICB9XG4gIGZvciAodmFyIGtleSBpbiBleHRyYVF1ZXJ5KSB7XG4gICAgcGFyc2VkUXVlcnlba2V5XSA9IGV4dHJhUXVlcnlba2V5XTtcbiAgfVxuICByZXR1cm4gcGFyc2VkUXVlcnlcbn1cblxuZnVuY3Rpb24gcGFyc2VRdWVyeSAocXVlcnkpIHtcbiAgdmFyIHJlcyA9IHt9O1xuXG4gIHF1ZXJ5ID0gcXVlcnkudHJpbSgpLnJlcGxhY2UoL14oXFw/fCN8JikvLCAnJyk7XG5cbiAgaWYgKCFxdWVyeSkge1xuICAgIHJldHVybiByZXNcbiAgfVxuXG4gIHF1ZXJ5LnNwbGl0KCcmJykuZm9yRWFjaChmdW5jdGlvbiAocGFyYW0pIHtcbiAgICB2YXIgcGFydHMgPSBwYXJhbS5yZXBsYWNlKC9cXCsvZywgJyAnKS5zcGxpdCgnPScpO1xuICAgIHZhciBrZXkgPSBkZWNvZGUocGFydHMuc2hpZnQoKSk7XG4gICAgdmFyIHZhbCA9IHBhcnRzLmxlbmd0aCA+IDBcbiAgICAgID8gZGVjb2RlKHBhcnRzLmpvaW4oJz0nKSlcbiAgICAgIDogbnVsbDtcblxuICAgIGlmIChyZXNba2V5XSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICByZXNba2V5XSA9IHZhbDtcbiAgICB9IGVsc2UgaWYgKEFycmF5LmlzQXJyYXkocmVzW2tleV0pKSB7XG4gICAgICByZXNba2V5XS5wdXNoKHZhbCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJlc1trZXldID0gW3Jlc1trZXldLCB2YWxdO1xuICAgIH1cbiAgfSk7XG5cbiAgcmV0dXJuIHJlc1xufVxuXG5mdW5jdGlvbiBzdHJpbmdpZnlRdWVyeSAob2JqKSB7XG4gIHZhciByZXMgPSBvYmogPyBPYmplY3Qua2V5cyhvYmopLm1hcChmdW5jdGlvbiAoa2V5KSB7XG4gICAgdmFyIHZhbCA9IG9ialtrZXldO1xuXG4gICAgaWYgKHZhbCA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICByZXR1cm4gJydcbiAgICB9XG5cbiAgICBpZiAodmFsID09PSBudWxsKSB7XG4gICAgICByZXR1cm4gZW5jb2RlKGtleSlcbiAgICB9XG5cbiAgICBpZiAoQXJyYXkuaXNBcnJheSh2YWwpKSB7XG4gICAgICB2YXIgcmVzdWx0ID0gW107XG4gICAgICB2YWwuZm9yRWFjaChmdW5jdGlvbiAodmFsMikge1xuICAgICAgICBpZiAodmFsMiA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgcmV0dXJuXG4gICAgICAgIH1cbiAgICAgICAgaWYgKHZhbDIgPT09IG51bGwpIHtcbiAgICAgICAgICByZXN1bHQucHVzaChlbmNvZGUoa2V5KSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgcmVzdWx0LnB1c2goZW5jb2RlKGtleSkgKyAnPScgKyBlbmNvZGUodmFsMikpO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICAgIHJldHVybiByZXN1bHQuam9pbignJicpXG4gICAgfVxuXG4gICAgcmV0dXJuIGVuY29kZShrZXkpICsgJz0nICsgZW5jb2RlKHZhbClcbiAgfSkuZmlsdGVyKGZ1bmN0aW9uICh4KSB7IHJldHVybiB4Lmxlbmd0aCA+IDA7IH0pLmpvaW4oJyYnKSA6IG51bGw7XG4gIHJldHVybiByZXMgPyAoXCI/XCIgKyByZXMpIDogJydcbn1cblxuLyogICovXG5cbnZhciB0cmFpbGluZ1NsYXNoUkUgPSAvXFwvPyQvO1xuXG5mdW5jdGlvbiBjcmVhdGVSb3V0ZSAoXG4gIHJlY29yZCxcbiAgbG9jYXRpb24sXG4gIHJlZGlyZWN0ZWRGcm9tLFxuICByb3V0ZXJcbikge1xuICB2YXIgc3RyaW5naWZ5UXVlcnkgPSByb3V0ZXIgJiYgcm91dGVyLm9wdGlvbnMuc3RyaW5naWZ5UXVlcnk7XG5cbiAgdmFyIHF1ZXJ5ID0gbG9jYXRpb24ucXVlcnkgfHwge307XG4gIHRyeSB7XG4gICAgcXVlcnkgPSBjbG9uZShxdWVyeSk7XG4gIH0gY2F0Y2ggKGUpIHt9XG5cbiAgdmFyIHJvdXRlID0ge1xuICAgIG5hbWU6IGxvY2F0aW9uLm5hbWUgfHwgKHJlY29yZCAmJiByZWNvcmQubmFtZSksXG4gICAgbWV0YTogKHJlY29yZCAmJiByZWNvcmQubWV0YSkgfHwge30sXG4gICAgcGF0aDogbG9jYXRpb24ucGF0aCB8fCAnLycsXG4gICAgaGFzaDogbG9jYXRpb24uaGFzaCB8fCAnJyxcbiAgICBxdWVyeTogcXVlcnksXG4gICAgcGFyYW1zOiBsb2NhdGlvbi5wYXJhbXMgfHwge30sXG4gICAgZnVsbFBhdGg6IGdldEZ1bGxQYXRoKGxvY2F0aW9uLCBzdHJpbmdpZnlRdWVyeSksXG4gICAgbWF0Y2hlZDogcmVjb3JkID8gZm9ybWF0TWF0Y2gocmVjb3JkKSA6IFtdXG4gIH07XG4gIGlmIChyZWRpcmVjdGVkRnJvbSkge1xuICAgIHJvdXRlLnJlZGlyZWN0ZWRGcm9tID0gZ2V0RnVsbFBhdGgocmVkaXJlY3RlZEZyb20sIHN0cmluZ2lmeVF1ZXJ5KTtcbiAgfVxuICByZXR1cm4gT2JqZWN0LmZyZWV6ZShyb3V0ZSlcbn1cblxuZnVuY3Rpb24gY2xvbmUgKHZhbHVlKSB7XG4gIGlmIChBcnJheS5pc0FycmF5KHZhbHVlKSkge1xuICAgIHJldHVybiB2YWx1ZS5tYXAoY2xvbmUpXG4gIH0gZWxzZSBpZiAodmFsdWUgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0Jykge1xuICAgIHZhciByZXMgPSB7fTtcbiAgICBmb3IgKHZhciBrZXkgaW4gdmFsdWUpIHtcbiAgICAgIHJlc1trZXldID0gY2xvbmUodmFsdWVba2V5XSk7XG4gICAgfVxuICAgIHJldHVybiByZXNcbiAgfSBlbHNlIHtcbiAgICByZXR1cm4gdmFsdWVcbiAgfVxufVxuXG4vLyB0aGUgc3RhcnRpbmcgcm91dGUgdGhhdCByZXByZXNlbnRzIHRoZSBpbml0aWFsIHN0YXRlXG52YXIgU1RBUlQgPSBjcmVhdGVSb3V0ZShudWxsLCB7XG4gIHBhdGg6ICcvJ1xufSk7XG5cbmZ1bmN0aW9uIGZvcm1hdE1hdGNoIChyZWNvcmQpIHtcbiAgdmFyIHJlcyA9IFtdO1xuICB3aGlsZSAocmVjb3JkKSB7XG4gICAgcmVzLnVuc2hpZnQocmVjb3JkKTtcbiAgICByZWNvcmQgPSByZWNvcmQucGFyZW50O1xuICB9XG4gIHJldHVybiByZXNcbn1cblxuZnVuY3Rpb24gZ2V0RnVsbFBhdGggKFxuICByZWYsXG4gIF9zdHJpbmdpZnlRdWVyeVxuKSB7XG4gIHZhciBwYXRoID0gcmVmLnBhdGg7XG4gIHZhciBxdWVyeSA9IHJlZi5xdWVyeTsgaWYgKCBxdWVyeSA9PT0gdm9pZCAwICkgcXVlcnkgPSB7fTtcbiAgdmFyIGhhc2ggPSByZWYuaGFzaDsgaWYgKCBoYXNoID09PSB2b2lkIDAgKSBoYXNoID0gJyc7XG5cbiAgdmFyIHN0cmluZ2lmeSA9IF9zdHJpbmdpZnlRdWVyeSB8fCBzdHJpbmdpZnlRdWVyeTtcbiAgcmV0dXJuIChwYXRoIHx8ICcvJykgKyBzdHJpbmdpZnkocXVlcnkpICsgaGFzaFxufVxuXG5mdW5jdGlvbiBpc1NhbWVSb3V0ZSAoYSwgYikge1xuICBpZiAoYiA9PT0gU1RBUlQpIHtcbiAgICByZXR1cm4gYSA9PT0gYlxuICB9IGVsc2UgaWYgKCFiKSB7XG4gICAgcmV0dXJuIGZhbHNlXG4gIH0gZWxzZSBpZiAoYS5wYXRoICYmIGIucGF0aCkge1xuICAgIHJldHVybiAoXG4gICAgICBhLnBhdGgucmVwbGFjZSh0cmFpbGluZ1NsYXNoUkUsICcnKSA9PT0gYi5wYXRoLnJlcGxhY2UodHJhaWxpbmdTbGFzaFJFLCAnJykgJiZcbiAgICAgIGEuaGFzaCA9PT0gYi5oYXNoICYmXG4gICAgICBpc09iamVjdEVxdWFsKGEucXVlcnksIGIucXVlcnkpXG4gICAgKVxuICB9IGVsc2UgaWYgKGEubmFtZSAmJiBiLm5hbWUpIHtcbiAgICByZXR1cm4gKFxuICAgICAgYS5uYW1lID09PSBiLm5hbWUgJiZcbiAgICAgIGEuaGFzaCA9PT0gYi5oYXNoICYmXG4gICAgICBpc09iamVjdEVxdWFsKGEucXVlcnksIGIucXVlcnkpICYmXG4gICAgICBpc09iamVjdEVxdWFsKGEucGFyYW1zLCBiLnBhcmFtcylcbiAgICApXG4gIH0gZWxzZSB7XG4gICAgcmV0dXJuIGZhbHNlXG4gIH1cbn1cblxuZnVuY3Rpb24gaXNPYmplY3RFcXVhbCAoYSwgYikge1xuICBpZiAoIGEgPT09IHZvaWQgMCApIGEgPSB7fTtcbiAgaWYgKCBiID09PSB2b2lkIDAgKSBiID0ge307XG5cbiAgLy8gaGFuZGxlIG51bGwgdmFsdWUgIzE1NjZcbiAgaWYgKCFhIHx8ICFiKSB7IHJldHVybiBhID09PSBiIH1cbiAgdmFyIGFLZXlzID0gT2JqZWN0LmtleXMoYSk7XG4gIHZhciBiS2V5cyA9IE9iamVjdC5rZXlzKGIpO1xuICBpZiAoYUtleXMubGVuZ3RoICE9PSBiS2V5cy5sZW5ndGgpIHtcbiAgICByZXR1cm4gZmFsc2VcbiAgfVxuICByZXR1cm4gYUtleXMuZXZlcnkoZnVuY3Rpb24gKGtleSkge1xuICAgIHZhciBhVmFsID0gYVtrZXldO1xuICAgIHZhciBiVmFsID0gYltrZXldO1xuICAgIC8vIGNoZWNrIG5lc3RlZCBlcXVhbGl0eVxuICAgIGlmICh0eXBlb2YgYVZhbCA9PT0gJ29iamVjdCcgJiYgdHlwZW9mIGJWYWwgPT09ICdvYmplY3QnKSB7XG4gICAgICByZXR1cm4gaXNPYmplY3RFcXVhbChhVmFsLCBiVmFsKVxuICAgIH1cbiAgICByZXR1cm4gU3RyaW5nKGFWYWwpID09PSBTdHJpbmcoYlZhbClcbiAgfSlcbn1cblxuZnVuY3Rpb24gaXNJbmNsdWRlZFJvdXRlIChjdXJyZW50LCB0YXJnZXQpIHtcbiAgcmV0dXJuIChcbiAgICBjdXJyZW50LnBhdGgucmVwbGFjZSh0cmFpbGluZ1NsYXNoUkUsICcvJykuaW5kZXhPZihcbiAgICAgIHRhcmdldC5wYXRoLnJlcGxhY2UodHJhaWxpbmdTbGFzaFJFLCAnLycpXG4gICAgKSA9PT0gMCAmJlxuICAgICghdGFyZ2V0Lmhhc2ggfHwgY3VycmVudC5oYXNoID09PSB0YXJnZXQuaGFzaCkgJiZcbiAgICBxdWVyeUluY2x1ZGVzKGN1cnJlbnQucXVlcnksIHRhcmdldC5xdWVyeSlcbiAgKVxufVxuXG5mdW5jdGlvbiBxdWVyeUluY2x1ZGVzIChjdXJyZW50LCB0YXJnZXQpIHtcbiAgZm9yICh2YXIga2V5IGluIHRhcmdldCkge1xuICAgIGlmICghKGtleSBpbiBjdXJyZW50KSkge1xuICAgICAgcmV0dXJuIGZhbHNlXG4gICAgfVxuICB9XG4gIHJldHVybiB0cnVlXG59XG5cbi8qICAqL1xuXG5mdW5jdGlvbiByZXNvbHZlUGF0aCAoXG4gIHJlbGF0aXZlLFxuICBiYXNlLFxuICBhcHBlbmRcbikge1xuICB2YXIgZmlyc3RDaGFyID0gcmVsYXRpdmUuY2hhckF0KDApO1xuICBpZiAoZmlyc3RDaGFyID09PSAnLycpIHtcbiAgICByZXR1cm4gcmVsYXRpdmVcbiAgfVxuXG4gIGlmIChmaXJzdENoYXIgPT09ICc/JyB8fCBmaXJzdENoYXIgPT09ICcjJykge1xuICAgIHJldHVybiBiYXNlICsgcmVsYXRpdmVcbiAgfVxuXG4gIHZhciBzdGFjayA9IGJhc2Uuc3BsaXQoJy8nKTtcblxuICAvLyByZW1vdmUgdHJhaWxpbmcgc2VnbWVudCBpZjpcbiAgLy8gLSBub3QgYXBwZW5kaW5nXG4gIC8vIC0gYXBwZW5kaW5nIHRvIHRyYWlsaW5nIHNsYXNoIChsYXN0IHNlZ21lbnQgaXMgZW1wdHkpXG4gIGlmICghYXBwZW5kIHx8ICFzdGFja1tzdGFjay5sZW5ndGggLSAxXSkge1xuICAgIHN0YWNrLnBvcCgpO1xuICB9XG5cbiAgLy8gcmVzb2x2ZSByZWxhdGl2ZSBwYXRoXG4gIHZhciBzZWdtZW50cyA9IHJlbGF0aXZlLnJlcGxhY2UoL15cXC8vLCAnJykuc3BsaXQoJy8nKTtcbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBzZWdtZW50cy5sZW5ndGg7IGkrKykge1xuICAgIHZhciBzZWdtZW50ID0gc2VnbWVudHNbaV07XG4gICAgaWYgKHNlZ21lbnQgPT09ICcuLicpIHtcbiAgICAgIHN0YWNrLnBvcCgpO1xuICAgIH0gZWxzZSBpZiAoc2VnbWVudCAhPT0gJy4nKSB7XG4gICAgICBzdGFjay5wdXNoKHNlZ21lbnQpO1xuICAgIH1cbiAgfVxuXG4gIC8vIGVuc3VyZSBsZWFkaW5nIHNsYXNoXG4gIGlmIChzdGFja1swXSAhPT0gJycpIHtcbiAgICBzdGFjay51bnNoaWZ0KCcnKTtcbiAgfVxuXG4gIHJldHVybiBzdGFjay5qb2luKCcvJylcbn1cblxuZnVuY3Rpb24gcGFyc2VQYXRoIChwYXRoKSB7XG4gIHZhciBoYXNoID0gJyc7XG4gIHZhciBxdWVyeSA9ICcnO1xuXG4gIHZhciBoYXNoSW5kZXggPSBwYXRoLmluZGV4T2YoJyMnKTtcbiAgaWYgKGhhc2hJbmRleCA+PSAwKSB7XG4gICAgaGFzaCA9IHBhdGguc2xpY2UoaGFzaEluZGV4KTtcbiAgICBwYXRoID0gcGF0aC5zbGljZSgwLCBoYXNoSW5kZXgpO1xuICB9XG5cbiAgdmFyIHF1ZXJ5SW5kZXggPSBwYXRoLmluZGV4T2YoJz8nKTtcbiAgaWYgKHF1ZXJ5SW5kZXggPj0gMCkge1xuICAgIHF1ZXJ5ID0gcGF0aC5zbGljZShxdWVyeUluZGV4ICsgMSk7XG4gICAgcGF0aCA9IHBhdGguc2xpY2UoMCwgcXVlcnlJbmRleCk7XG4gIH1cblxuICByZXR1cm4ge1xuICAgIHBhdGg6IHBhdGgsXG4gICAgcXVlcnk6IHF1ZXJ5LFxuICAgIGhhc2g6IGhhc2hcbiAgfVxufVxuXG5mdW5jdGlvbiBjbGVhblBhdGggKHBhdGgpIHtcbiAgcmV0dXJuIHBhdGgucmVwbGFjZSgvXFwvXFwvL2csICcvJylcbn1cblxudmFyIGlzYXJyYXkgPSBBcnJheS5pc0FycmF5IHx8IGZ1bmN0aW9uIChhcnIpIHtcbiAgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcuY2FsbChhcnIpID09ICdbb2JqZWN0IEFycmF5XSc7XG59O1xuXG4vKipcbiAqIEV4cG9zZSBgcGF0aFRvUmVnZXhwYC5cbiAqL1xudmFyIHBhdGhUb1JlZ2V4cF8xID0gcGF0aFRvUmVnZXhwO1xudmFyIHBhcnNlXzEgPSBwYXJzZTtcbnZhciBjb21waWxlXzEgPSBjb21waWxlO1xudmFyIHRva2Vuc1RvRnVuY3Rpb25fMSA9IHRva2Vuc1RvRnVuY3Rpb247XG52YXIgdG9rZW5zVG9SZWdFeHBfMSA9IHRva2Vuc1RvUmVnRXhwO1xuXG4vKipcbiAqIFRoZSBtYWluIHBhdGggbWF0Y2hpbmcgcmVnZXhwIHV0aWxpdHkuXG4gKlxuICogQHR5cGUge1JlZ0V4cH1cbiAqL1xudmFyIFBBVEhfUkVHRVhQID0gbmV3IFJlZ0V4cChbXG4gIC8vIE1hdGNoIGVzY2FwZWQgY2hhcmFjdGVycyB0aGF0IHdvdWxkIG90aGVyd2lzZSBhcHBlYXIgaW4gZnV0dXJlIG1hdGNoZXMuXG4gIC8vIFRoaXMgYWxsb3dzIHRoZSB1c2VyIHRvIGVzY2FwZSBzcGVjaWFsIGNoYXJhY3RlcnMgdGhhdCB3b24ndCB0cmFuc2Zvcm0uXG4gICcoXFxcXFxcXFwuKScsXG4gIC8vIE1hdGNoIEV4cHJlc3Mtc3R5bGUgcGFyYW1ldGVycyBhbmQgdW4tbmFtZWQgcGFyYW1ldGVycyB3aXRoIGEgcHJlZml4XG4gIC8vIGFuZCBvcHRpb25hbCBzdWZmaXhlcy4gTWF0Y2hlcyBhcHBlYXIgYXM6XG4gIC8vXG4gIC8vIFwiLzp0ZXN0KFxcXFxkKyk/XCIgPT4gW1wiL1wiLCBcInRlc3RcIiwgXCJcXGQrXCIsIHVuZGVmaW5lZCwgXCI/XCIsIHVuZGVmaW5lZF1cbiAgLy8gXCIvcm91dGUoXFxcXGQrKVwiICA9PiBbdW5kZWZpbmVkLCB1bmRlZmluZWQsIHVuZGVmaW5lZCwgXCJcXGQrXCIsIHVuZGVmaW5lZCwgdW5kZWZpbmVkXVxuICAvLyBcIi8qXCIgICAgICAgICAgICA9PiBbXCIvXCIsIHVuZGVmaW5lZCwgdW5kZWZpbmVkLCB1bmRlZmluZWQsIHVuZGVmaW5lZCwgXCIqXCJdXG4gICcoW1xcXFwvLl0pPyg/Oig/OlxcXFw6KFxcXFx3KykoPzpcXFxcKCgoPzpcXFxcXFxcXC58W15cXFxcXFxcXCgpXSkrKVxcXFwpKT98XFxcXCgoKD86XFxcXFxcXFwufFteXFxcXFxcXFwoKV0pKylcXFxcKSkoWysqP10pP3woXFxcXCopKSdcbl0uam9pbignfCcpLCAnZycpO1xuXG4vKipcbiAqIFBhcnNlIGEgc3RyaW5nIGZvciB0aGUgcmF3IHRva2Vucy5cbiAqXG4gKiBAcGFyYW0gIHtzdHJpbmd9ICBzdHJcbiAqIEBwYXJhbSAge09iamVjdD19IG9wdGlvbnNcbiAqIEByZXR1cm4geyFBcnJheX1cbiAqL1xuZnVuY3Rpb24gcGFyc2UgKHN0ciwgb3B0aW9ucykge1xuICB2YXIgdG9rZW5zID0gW107XG4gIHZhciBrZXkgPSAwO1xuICB2YXIgaW5kZXggPSAwO1xuICB2YXIgcGF0aCA9ICcnO1xuICB2YXIgZGVmYXVsdERlbGltaXRlciA9IG9wdGlvbnMgJiYgb3B0aW9ucy5kZWxpbWl0ZXIgfHwgJy8nO1xuICB2YXIgcmVzO1xuXG4gIHdoaWxlICgocmVzID0gUEFUSF9SRUdFWFAuZXhlYyhzdHIpKSAhPSBudWxsKSB7XG4gICAgdmFyIG0gPSByZXNbMF07XG4gICAgdmFyIGVzY2FwZWQgPSByZXNbMV07XG4gICAgdmFyIG9mZnNldCA9IHJlcy5pbmRleDtcbiAgICBwYXRoICs9IHN0ci5zbGljZShpbmRleCwgb2Zmc2V0KTtcbiAgICBpbmRleCA9IG9mZnNldCArIG0ubGVuZ3RoO1xuXG4gICAgLy8gSWdub3JlIGFscmVhZHkgZXNjYXBlZCBzZXF1ZW5jZXMuXG4gICAgaWYgKGVzY2FwZWQpIHtcbiAgICAgIHBhdGggKz0gZXNjYXBlZFsxXTtcbiAgICAgIGNvbnRpbnVlXG4gICAgfVxuXG4gICAgdmFyIG5leHQgPSBzdHJbaW5kZXhdO1xuICAgIHZhciBwcmVmaXggPSByZXNbMl07XG4gICAgdmFyIG5hbWUgPSByZXNbM107XG4gICAgdmFyIGNhcHR1cmUgPSByZXNbNF07XG4gICAgdmFyIGdyb3VwID0gcmVzWzVdO1xuICAgIHZhciBtb2RpZmllciA9IHJlc1s2XTtcbiAgICB2YXIgYXN0ZXJpc2sgPSByZXNbN107XG5cbiAgICAvLyBQdXNoIHRoZSBjdXJyZW50IHBhdGggb250byB0aGUgdG9rZW5zLlxuICAgIGlmIChwYXRoKSB7XG4gICAgICB0b2tlbnMucHVzaChwYXRoKTtcbiAgICAgIHBhdGggPSAnJztcbiAgICB9XG5cbiAgICB2YXIgcGFydGlhbCA9IHByZWZpeCAhPSBudWxsICYmIG5leHQgIT0gbnVsbCAmJiBuZXh0ICE9PSBwcmVmaXg7XG4gICAgdmFyIHJlcGVhdCA9IG1vZGlmaWVyID09PSAnKycgfHwgbW9kaWZpZXIgPT09ICcqJztcbiAgICB2YXIgb3B0aW9uYWwgPSBtb2RpZmllciA9PT0gJz8nIHx8IG1vZGlmaWVyID09PSAnKic7XG4gICAgdmFyIGRlbGltaXRlciA9IHJlc1syXSB8fCBkZWZhdWx0RGVsaW1pdGVyO1xuICAgIHZhciBwYXR0ZXJuID0gY2FwdHVyZSB8fCBncm91cDtcblxuICAgIHRva2Vucy5wdXNoKHtcbiAgICAgIG5hbWU6IG5hbWUgfHwga2V5KyssXG4gICAgICBwcmVmaXg6IHByZWZpeCB8fCAnJyxcbiAgICAgIGRlbGltaXRlcjogZGVsaW1pdGVyLFxuICAgICAgb3B0aW9uYWw6IG9wdGlvbmFsLFxuICAgICAgcmVwZWF0OiByZXBlYXQsXG4gICAgICBwYXJ0aWFsOiBwYXJ0aWFsLFxuICAgICAgYXN0ZXJpc2s6ICEhYXN0ZXJpc2ssXG4gICAgICBwYXR0ZXJuOiBwYXR0ZXJuID8gZXNjYXBlR3JvdXAocGF0dGVybikgOiAoYXN0ZXJpc2sgPyAnLionIDogJ1teJyArIGVzY2FwZVN0cmluZyhkZWxpbWl0ZXIpICsgJ10rPycpXG4gICAgfSk7XG4gIH1cblxuICAvLyBNYXRjaCBhbnkgY2hhcmFjdGVycyBzdGlsbCByZW1haW5pbmcuXG4gIGlmIChpbmRleCA8IHN0ci5sZW5ndGgpIHtcbiAgICBwYXRoICs9IHN0ci5zdWJzdHIoaW5kZXgpO1xuICB9XG5cbiAgLy8gSWYgdGhlIHBhdGggZXhpc3RzLCBwdXNoIGl0IG9udG8gdGhlIGVuZC5cbiAgaWYgKHBhdGgpIHtcbiAgICB0b2tlbnMucHVzaChwYXRoKTtcbiAgfVxuXG4gIHJldHVybiB0b2tlbnNcbn1cblxuLyoqXG4gKiBDb21waWxlIGEgc3RyaW5nIHRvIGEgdGVtcGxhdGUgZnVuY3Rpb24gZm9yIHRoZSBwYXRoLlxuICpcbiAqIEBwYXJhbSAge3N0cmluZ30gICAgICAgICAgICAgc3RyXG4gKiBAcGFyYW0gIHtPYmplY3Q9fSAgICAgICAgICAgIG9wdGlvbnNcbiAqIEByZXR1cm4geyFmdW5jdGlvbihPYmplY3Q9LCBPYmplY3Q9KX1cbiAqL1xuZnVuY3Rpb24gY29tcGlsZSAoc3RyLCBvcHRpb25zKSB7XG4gIHJldHVybiB0b2tlbnNUb0Z1bmN0aW9uKHBhcnNlKHN0ciwgb3B0aW9ucykpXG59XG5cbi8qKlxuICogUHJldHRpZXIgZW5jb2Rpbmcgb2YgVVJJIHBhdGggc2VnbWVudHMuXG4gKlxuICogQHBhcmFtICB7c3RyaW5nfVxuICogQHJldHVybiB7c3RyaW5nfVxuICovXG5mdW5jdGlvbiBlbmNvZGVVUklDb21wb25lbnRQcmV0dHkgKHN0cikge1xuICByZXR1cm4gZW5jb2RlVVJJKHN0cikucmVwbGFjZSgvW1xcLz8jXS9nLCBmdW5jdGlvbiAoYykge1xuICAgIHJldHVybiAnJScgKyBjLmNoYXJDb2RlQXQoMCkudG9TdHJpbmcoMTYpLnRvVXBwZXJDYXNlKClcbiAgfSlcbn1cblxuLyoqXG4gKiBFbmNvZGUgdGhlIGFzdGVyaXNrIHBhcmFtZXRlci4gU2ltaWxhciB0byBgcHJldHR5YCwgYnV0IGFsbG93cyBzbGFzaGVzLlxuICpcbiAqIEBwYXJhbSAge3N0cmluZ31cbiAqIEByZXR1cm4ge3N0cmluZ31cbiAqL1xuZnVuY3Rpb24gZW5jb2RlQXN0ZXJpc2sgKHN0cikge1xuICByZXR1cm4gZW5jb2RlVVJJKHN0cikucmVwbGFjZSgvWz8jXS9nLCBmdW5jdGlvbiAoYykge1xuICAgIHJldHVybiAnJScgKyBjLmNoYXJDb2RlQXQoMCkudG9TdHJpbmcoMTYpLnRvVXBwZXJDYXNlKClcbiAgfSlcbn1cblxuLyoqXG4gKiBFeHBvc2UgYSBtZXRob2QgZm9yIHRyYW5zZm9ybWluZyB0b2tlbnMgaW50byB0aGUgcGF0aCBmdW5jdGlvbi5cbiAqL1xuZnVuY3Rpb24gdG9rZW5zVG9GdW5jdGlvbiAodG9rZW5zKSB7XG4gIC8vIENvbXBpbGUgYWxsIHRoZSB0b2tlbnMgaW50byByZWdleHBzLlxuICB2YXIgbWF0Y2hlcyA9IG5ldyBBcnJheSh0b2tlbnMubGVuZ3RoKTtcblxuICAvLyBDb21waWxlIGFsbCB0aGUgcGF0dGVybnMgYmVmb3JlIGNvbXBpbGF0aW9uLlxuICBmb3IgKHZhciBpID0gMDsgaSA8IHRva2Vucy5sZW5ndGg7IGkrKykge1xuICAgIGlmICh0eXBlb2YgdG9rZW5zW2ldID09PSAnb2JqZWN0Jykge1xuICAgICAgbWF0Y2hlc1tpXSA9IG5ldyBSZWdFeHAoJ14oPzonICsgdG9rZW5zW2ldLnBhdHRlcm4gKyAnKSQnKTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gZnVuY3Rpb24gKG9iaiwgb3B0cykge1xuICAgIHZhciBwYXRoID0gJyc7XG4gICAgdmFyIGRhdGEgPSBvYmogfHwge307XG4gICAgdmFyIG9wdGlvbnMgPSBvcHRzIHx8IHt9O1xuICAgIHZhciBlbmNvZGUgPSBvcHRpb25zLnByZXR0eSA/IGVuY29kZVVSSUNvbXBvbmVudFByZXR0eSA6IGVuY29kZVVSSUNvbXBvbmVudDtcblxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgdG9rZW5zLmxlbmd0aDsgaSsrKSB7XG4gICAgICB2YXIgdG9rZW4gPSB0b2tlbnNbaV07XG5cbiAgICAgIGlmICh0eXBlb2YgdG9rZW4gPT09ICdzdHJpbmcnKSB7XG4gICAgICAgIHBhdGggKz0gdG9rZW47XG5cbiAgICAgICAgY29udGludWVcbiAgICAgIH1cblxuICAgICAgdmFyIHZhbHVlID0gZGF0YVt0b2tlbi5uYW1lXTtcbiAgICAgIHZhciBzZWdtZW50O1xuXG4gICAgICBpZiAodmFsdWUgPT0gbnVsbCkge1xuICAgICAgICBpZiAodG9rZW4ub3B0aW9uYWwpIHtcbiAgICAgICAgICAvLyBQcmVwZW5kIHBhcnRpYWwgc2VnbWVudCBwcmVmaXhlcy5cbiAgICAgICAgICBpZiAodG9rZW4ucGFydGlhbCkge1xuICAgICAgICAgICAgcGF0aCArPSB0b2tlbi5wcmVmaXg7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgY29udGludWVcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB0aHJvdyBuZXcgVHlwZUVycm9yKCdFeHBlY3RlZCBcIicgKyB0b2tlbi5uYW1lICsgJ1wiIHRvIGJlIGRlZmluZWQnKVxuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGlmIChpc2FycmF5KHZhbHVlKSkge1xuICAgICAgICBpZiAoIXRva2VuLnJlcGVhdCkge1xuICAgICAgICAgIHRocm93IG5ldyBUeXBlRXJyb3IoJ0V4cGVjdGVkIFwiJyArIHRva2VuLm5hbWUgKyAnXCIgdG8gbm90IHJlcGVhdCwgYnV0IHJlY2VpdmVkIGAnICsgSlNPTi5zdHJpbmdpZnkodmFsdWUpICsgJ2AnKVxuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHZhbHVlLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICAgIGlmICh0b2tlbi5vcHRpb25hbCkge1xuICAgICAgICAgICAgY29udGludWVcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcignRXhwZWN0ZWQgXCInICsgdG9rZW4ubmFtZSArICdcIiB0byBub3QgYmUgZW1wdHknKVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIGZvciAodmFyIGogPSAwOyBqIDwgdmFsdWUubGVuZ3RoOyBqKyspIHtcbiAgICAgICAgICBzZWdtZW50ID0gZW5jb2RlKHZhbHVlW2pdKTtcblxuICAgICAgICAgIGlmICghbWF0Y2hlc1tpXS50ZXN0KHNlZ21lbnQpKSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgVHlwZUVycm9yKCdFeHBlY3RlZCBhbGwgXCInICsgdG9rZW4ubmFtZSArICdcIiB0byBtYXRjaCBcIicgKyB0b2tlbi5wYXR0ZXJuICsgJ1wiLCBidXQgcmVjZWl2ZWQgYCcgKyBKU09OLnN0cmluZ2lmeShzZWdtZW50KSArICdgJylcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBwYXRoICs9IChqID09PSAwID8gdG9rZW4ucHJlZml4IDogdG9rZW4uZGVsaW1pdGVyKSArIHNlZ21lbnQ7XG4gICAgICAgIH1cblxuICAgICAgICBjb250aW51ZVxuICAgICAgfVxuXG4gICAgICBzZWdtZW50ID0gdG9rZW4uYXN0ZXJpc2sgPyBlbmNvZGVBc3Rlcmlzayh2YWx1ZSkgOiBlbmNvZGUodmFsdWUpO1xuXG4gICAgICBpZiAoIW1hdGNoZXNbaV0udGVzdChzZWdtZW50KSkge1xuICAgICAgICB0aHJvdyBuZXcgVHlwZUVycm9yKCdFeHBlY3RlZCBcIicgKyB0b2tlbi5uYW1lICsgJ1wiIHRvIG1hdGNoIFwiJyArIHRva2VuLnBhdHRlcm4gKyAnXCIsIGJ1dCByZWNlaXZlZCBcIicgKyBzZWdtZW50ICsgJ1wiJylcbiAgICAgIH1cblxuICAgICAgcGF0aCArPSB0b2tlbi5wcmVmaXggKyBzZWdtZW50O1xuICAgIH1cblxuICAgIHJldHVybiBwYXRoXG4gIH1cbn1cblxuLyoqXG4gKiBFc2NhcGUgYSByZWd1bGFyIGV4cHJlc3Npb24gc3RyaW5nLlxuICpcbiAqIEBwYXJhbSAge3N0cmluZ30gc3RyXG4gKiBAcmV0dXJuIHtzdHJpbmd9XG4gKi9cbmZ1bmN0aW9uIGVzY2FwZVN0cmluZyAoc3RyKSB7XG4gIHJldHVybiBzdHIucmVwbGFjZSgvKFsuKyo/PV4hOiR7fSgpW1xcXXxcXC9cXFxcXSkvZywgJ1xcXFwkMScpXG59XG5cbi8qKlxuICogRXNjYXBlIHRoZSBjYXB0dXJpbmcgZ3JvdXAgYnkgZXNjYXBpbmcgc3BlY2lhbCBjaGFyYWN0ZXJzIGFuZCBtZWFuaW5nLlxuICpcbiAqIEBwYXJhbSAge3N0cmluZ30gZ3JvdXBcbiAqIEByZXR1cm4ge3N0cmluZ31cbiAqL1xuZnVuY3Rpb24gZXNjYXBlR3JvdXAgKGdyb3VwKSB7XG4gIHJldHVybiBncm91cC5yZXBsYWNlKC8oWz0hOiRcXC8oKV0pL2csICdcXFxcJDEnKVxufVxuXG4vKipcbiAqIEF0dGFjaCB0aGUga2V5cyBhcyBhIHByb3BlcnR5IG9mIHRoZSByZWdleHAuXG4gKlxuICogQHBhcmFtICB7IVJlZ0V4cH0gcmVcbiAqIEBwYXJhbSAge0FycmF5fSAgIGtleXNcbiAqIEByZXR1cm4geyFSZWdFeHB9XG4gKi9cbmZ1bmN0aW9uIGF0dGFjaEtleXMgKHJlLCBrZXlzKSB7XG4gIHJlLmtleXMgPSBrZXlzO1xuICByZXR1cm4gcmVcbn1cblxuLyoqXG4gKiBHZXQgdGhlIGZsYWdzIGZvciBhIHJlZ2V4cCBmcm9tIHRoZSBvcHRpb25zLlxuICpcbiAqIEBwYXJhbSAge09iamVjdH0gb3B0aW9uc1xuICogQHJldHVybiB7c3RyaW5nfVxuICovXG5mdW5jdGlvbiBmbGFncyAob3B0aW9ucykge1xuICByZXR1cm4gb3B0aW9ucy5zZW5zaXRpdmUgPyAnJyA6ICdpJ1xufVxuXG4vKipcbiAqIFB1bGwgb3V0IGtleXMgZnJvbSBhIHJlZ2V4cC5cbiAqXG4gKiBAcGFyYW0gIHshUmVnRXhwfSBwYXRoXG4gKiBAcGFyYW0gIHshQXJyYXl9ICBrZXlzXG4gKiBAcmV0dXJuIHshUmVnRXhwfVxuICovXG5mdW5jdGlvbiByZWdleHBUb1JlZ2V4cCAocGF0aCwga2V5cykge1xuICAvLyBVc2UgYSBuZWdhdGl2ZSBsb29rYWhlYWQgdG8gbWF0Y2ggb25seSBjYXB0dXJpbmcgZ3JvdXBzLlxuICB2YXIgZ3JvdXBzID0gcGF0aC5zb3VyY2UubWF0Y2goL1xcKCg/IVxcPykvZyk7XG5cbiAgaWYgKGdyb3Vwcykge1xuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgZ3JvdXBzLmxlbmd0aDsgaSsrKSB7XG4gICAgICBrZXlzLnB1c2goe1xuICAgICAgICBuYW1lOiBpLFxuICAgICAgICBwcmVmaXg6IG51bGwsXG4gICAgICAgIGRlbGltaXRlcjogbnVsbCxcbiAgICAgICAgb3B0aW9uYWw6IGZhbHNlLFxuICAgICAgICByZXBlYXQ6IGZhbHNlLFxuICAgICAgICBwYXJ0aWFsOiBmYWxzZSxcbiAgICAgICAgYXN0ZXJpc2s6IGZhbHNlLFxuICAgICAgICBwYXR0ZXJuOiBudWxsXG4gICAgICB9KTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gYXR0YWNoS2V5cyhwYXRoLCBrZXlzKVxufVxuXG4vKipcbiAqIFRyYW5zZm9ybSBhbiBhcnJheSBpbnRvIGEgcmVnZXhwLlxuICpcbiAqIEBwYXJhbSAgeyFBcnJheX0gIHBhdGhcbiAqIEBwYXJhbSAge0FycmF5fSAgIGtleXNcbiAqIEBwYXJhbSAgeyFPYmplY3R9IG9wdGlvbnNcbiAqIEByZXR1cm4geyFSZWdFeHB9XG4gKi9cbmZ1bmN0aW9uIGFycmF5VG9SZWdleHAgKHBhdGgsIGtleXMsIG9wdGlvbnMpIHtcbiAgdmFyIHBhcnRzID0gW107XG5cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBwYXRoLmxlbmd0aDsgaSsrKSB7XG4gICAgcGFydHMucHVzaChwYXRoVG9SZWdleHAocGF0aFtpXSwga2V5cywgb3B0aW9ucykuc291cmNlKTtcbiAgfVxuXG4gIHZhciByZWdleHAgPSBuZXcgUmVnRXhwKCcoPzonICsgcGFydHMuam9pbignfCcpICsgJyknLCBmbGFncyhvcHRpb25zKSk7XG5cbiAgcmV0dXJuIGF0dGFjaEtleXMocmVnZXhwLCBrZXlzKVxufVxuXG4vKipcbiAqIENyZWF0ZSBhIHBhdGggcmVnZXhwIGZyb20gc3RyaW5nIGlucHV0LlxuICpcbiAqIEBwYXJhbSAge3N0cmluZ30gIHBhdGhcbiAqIEBwYXJhbSAgeyFBcnJheX0gIGtleXNcbiAqIEBwYXJhbSAgeyFPYmplY3R9IG9wdGlvbnNcbiAqIEByZXR1cm4geyFSZWdFeHB9XG4gKi9cbmZ1bmN0aW9uIHN0cmluZ1RvUmVnZXhwIChwYXRoLCBrZXlzLCBvcHRpb25zKSB7XG4gIHJldHVybiB0b2tlbnNUb1JlZ0V4cChwYXJzZShwYXRoLCBvcHRpb25zKSwga2V5cywgb3B0aW9ucylcbn1cblxuLyoqXG4gKiBFeHBvc2UgYSBmdW5jdGlvbiBmb3IgdGFraW5nIHRva2VucyBhbmQgcmV0dXJuaW5nIGEgUmVnRXhwLlxuICpcbiAqIEBwYXJhbSAgeyFBcnJheX0gICAgICAgICAgdG9rZW5zXG4gKiBAcGFyYW0gIHsoQXJyYXl8T2JqZWN0KT19IGtleXNcbiAqIEBwYXJhbSAge09iamVjdD19ICAgICAgICAgb3B0aW9uc1xuICogQHJldHVybiB7IVJlZ0V4cH1cbiAqL1xuZnVuY3Rpb24gdG9rZW5zVG9SZWdFeHAgKHRva2Vucywga2V5cywgb3B0aW9ucykge1xuICBpZiAoIWlzYXJyYXkoa2V5cykpIHtcbiAgICBvcHRpb25zID0gLyoqIEB0eXBlIHshT2JqZWN0fSAqLyAoa2V5cyB8fCBvcHRpb25zKTtcbiAgICBrZXlzID0gW107XG4gIH1cblxuICBvcHRpb25zID0gb3B0aW9ucyB8fCB7fTtcblxuICB2YXIgc3RyaWN0ID0gb3B0aW9ucy5zdHJpY3Q7XG4gIHZhciBlbmQgPSBvcHRpb25zLmVuZCAhPT0gZmFsc2U7XG4gIHZhciByb3V0ZSA9ICcnO1xuXG4gIC8vIEl0ZXJhdGUgb3ZlciB0aGUgdG9rZW5zIGFuZCBjcmVhdGUgb3VyIHJlZ2V4cCBzdHJpbmcuXG4gIGZvciAodmFyIGkgPSAwOyBpIDwgdG9rZW5zLmxlbmd0aDsgaSsrKSB7XG4gICAgdmFyIHRva2VuID0gdG9rZW5zW2ldO1xuXG4gICAgaWYgKHR5cGVvZiB0b2tlbiA9PT0gJ3N0cmluZycpIHtcbiAgICAgIHJvdXRlICs9IGVzY2FwZVN0cmluZyh0b2tlbik7XG4gICAgfSBlbHNlIHtcbiAgICAgIHZhciBwcmVmaXggPSBlc2NhcGVTdHJpbmcodG9rZW4ucHJlZml4KTtcbiAgICAgIHZhciBjYXB0dXJlID0gJyg/OicgKyB0b2tlbi5wYXR0ZXJuICsgJyknO1xuXG4gICAgICBrZXlzLnB1c2godG9rZW4pO1xuXG4gICAgICBpZiAodG9rZW4ucmVwZWF0KSB7XG4gICAgICAgIGNhcHR1cmUgKz0gJyg/OicgKyBwcmVmaXggKyBjYXB0dXJlICsgJykqJztcbiAgICAgIH1cblxuICAgICAgaWYgKHRva2VuLm9wdGlvbmFsKSB7XG4gICAgICAgIGlmICghdG9rZW4ucGFydGlhbCkge1xuICAgICAgICAgIGNhcHR1cmUgPSAnKD86JyArIHByZWZpeCArICcoJyArIGNhcHR1cmUgKyAnKSk/JztcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBjYXB0dXJlID0gcHJlZml4ICsgJygnICsgY2FwdHVyZSArICcpPyc7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGNhcHR1cmUgPSBwcmVmaXggKyAnKCcgKyBjYXB0dXJlICsgJyknO1xuICAgICAgfVxuXG4gICAgICByb3V0ZSArPSBjYXB0dXJlO1xuICAgIH1cbiAgfVxuXG4gIHZhciBkZWxpbWl0ZXIgPSBlc2NhcGVTdHJpbmcob3B0aW9ucy5kZWxpbWl0ZXIgfHwgJy8nKTtcbiAgdmFyIGVuZHNXaXRoRGVsaW1pdGVyID0gcm91dGUuc2xpY2UoLWRlbGltaXRlci5sZW5ndGgpID09PSBkZWxpbWl0ZXI7XG5cbiAgLy8gSW4gbm9uLXN0cmljdCBtb2RlIHdlIGFsbG93IGEgc2xhc2ggYXQgdGhlIGVuZCBvZiBtYXRjaC4gSWYgdGhlIHBhdGggdG9cbiAgLy8gbWF0Y2ggYWxyZWFkeSBlbmRzIHdpdGggYSBzbGFzaCwgd2UgcmVtb3ZlIGl0IGZvciBjb25zaXN0ZW5jeS4gVGhlIHNsYXNoXG4gIC8vIGlzIHZhbGlkIGF0IHRoZSBlbmQgb2YgYSBwYXRoIG1hdGNoLCBub3QgaW4gdGhlIG1pZGRsZS4gVGhpcyBpcyBpbXBvcnRhbnRcbiAgLy8gaW4gbm9uLWVuZGluZyBtb2RlLCB3aGVyZSBcIi90ZXN0L1wiIHNob3VsZG4ndCBtYXRjaCBcIi90ZXN0Ly9yb3V0ZVwiLlxuICBpZiAoIXN0cmljdCkge1xuICAgIHJvdXRlID0gKGVuZHNXaXRoRGVsaW1pdGVyID8gcm91dGUuc2xpY2UoMCwgLWRlbGltaXRlci5sZW5ndGgpIDogcm91dGUpICsgJyg/OicgKyBkZWxpbWl0ZXIgKyAnKD89JCkpPyc7XG4gIH1cblxuICBpZiAoZW5kKSB7XG4gICAgcm91dGUgKz0gJyQnO1xuICB9IGVsc2Uge1xuICAgIC8vIEluIG5vbi1lbmRpbmcgbW9kZSwgd2UgbmVlZCB0aGUgY2FwdHVyaW5nIGdyb3VwcyB0byBtYXRjaCBhcyBtdWNoIGFzXG4gICAgLy8gcG9zc2libGUgYnkgdXNpbmcgYSBwb3NpdGl2ZSBsb29rYWhlYWQgdG8gdGhlIGVuZCBvciBuZXh0IHBhdGggc2VnbWVudC5cbiAgICByb3V0ZSArPSBzdHJpY3QgJiYgZW5kc1dpdGhEZWxpbWl0ZXIgPyAnJyA6ICcoPz0nICsgZGVsaW1pdGVyICsgJ3wkKSc7XG4gIH1cblxuICByZXR1cm4gYXR0YWNoS2V5cyhuZXcgUmVnRXhwKCdeJyArIHJvdXRlLCBmbGFncyhvcHRpb25zKSksIGtleXMpXG59XG5cbi8qKlxuICogTm9ybWFsaXplIHRoZSBnaXZlbiBwYXRoIHN0cmluZywgcmV0dXJuaW5nIGEgcmVndWxhciBleHByZXNzaW9uLlxuICpcbiAqIEFuIGVtcHR5IGFycmF5IGNhbiBiZSBwYXNzZWQgaW4gZm9yIHRoZSBrZXlzLCB3aGljaCB3aWxsIGhvbGQgdGhlXG4gKiBwbGFjZWhvbGRlciBrZXkgZGVzY3JpcHRpb25zLiBGb3IgZXhhbXBsZSwgdXNpbmcgYC91c2VyLzppZGAsIGBrZXlzYCB3aWxsXG4gKiBjb250YWluIGBbeyBuYW1lOiAnaWQnLCBkZWxpbWl0ZXI6ICcvJywgb3B0aW9uYWw6IGZhbHNlLCByZXBlYXQ6IGZhbHNlIH1dYC5cbiAqXG4gKiBAcGFyYW0gIHsoc3RyaW5nfFJlZ0V4cHxBcnJheSl9IHBhdGhcbiAqIEBwYXJhbSAgeyhBcnJheXxPYmplY3QpPX0gICAgICAga2V5c1xuICogQHBhcmFtICB7T2JqZWN0PX0gICAgICAgICAgICAgICBvcHRpb25zXG4gKiBAcmV0dXJuIHshUmVnRXhwfVxuICovXG5mdW5jdGlvbiBwYXRoVG9SZWdleHAgKHBhdGgsIGtleXMsIG9wdGlvbnMpIHtcbiAgaWYgKCFpc2FycmF5KGtleXMpKSB7XG4gICAgb3B0aW9ucyA9IC8qKiBAdHlwZSB7IU9iamVjdH0gKi8gKGtleXMgfHwgb3B0aW9ucyk7XG4gICAga2V5cyA9IFtdO1xuICB9XG5cbiAgb3B0aW9ucyA9IG9wdGlvbnMgfHwge307XG5cbiAgaWYgKHBhdGggaW5zdGFuY2VvZiBSZWdFeHApIHtcbiAgICByZXR1cm4gcmVnZXhwVG9SZWdleHAocGF0aCwgLyoqIEB0eXBlIHshQXJyYXl9ICovIChrZXlzKSlcbiAgfVxuXG4gIGlmIChpc2FycmF5KHBhdGgpKSB7XG4gICAgcmV0dXJuIGFycmF5VG9SZWdleHAoLyoqIEB0eXBlIHshQXJyYXl9ICovIChwYXRoKSwgLyoqIEB0eXBlIHshQXJyYXl9ICovIChrZXlzKSwgb3B0aW9ucylcbiAgfVxuXG4gIHJldHVybiBzdHJpbmdUb1JlZ2V4cCgvKiogQHR5cGUge3N0cmluZ30gKi8gKHBhdGgpLCAvKiogQHR5cGUgeyFBcnJheX0gKi8gKGtleXMpLCBvcHRpb25zKVxufVxucGF0aFRvUmVnZXhwXzEucGFyc2UgPSBwYXJzZV8xO1xucGF0aFRvUmVnZXhwXzEuY29tcGlsZSA9IGNvbXBpbGVfMTtcbnBhdGhUb1JlZ2V4cF8xLnRva2Vuc1RvRnVuY3Rpb24gPSB0b2tlbnNUb0Z1bmN0aW9uXzE7XG5wYXRoVG9SZWdleHBfMS50b2tlbnNUb1JlZ0V4cCA9IHRva2Vuc1RvUmVnRXhwXzE7XG5cbi8qICAqL1xuXG4vLyAkZmxvdy1kaXNhYmxlLWxpbmVcbnZhciByZWdleHBDb21waWxlQ2FjaGUgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuXG5mdW5jdGlvbiBmaWxsUGFyYW1zIChcbiAgcGF0aCxcbiAgcGFyYW1zLFxuICByb3V0ZU1zZ1xuKSB7XG4gIHBhcmFtcyA9IHBhcmFtcyB8fCB7fTtcbiAgdHJ5IHtcbiAgICB2YXIgZmlsbGVyID1cbiAgICAgIHJlZ2V4cENvbXBpbGVDYWNoZVtwYXRoXSB8fFxuICAgICAgKHJlZ2V4cENvbXBpbGVDYWNoZVtwYXRoXSA9IHBhdGhUb1JlZ2V4cF8xLmNvbXBpbGUocGF0aCkpO1xuXG4gICAgLy8gRml4ICMyNTA1IHJlc29sdmluZyBhc3RlcmlzayByb3V0ZXMgeyBuYW1lOiAnbm90LWZvdW5kJywgcGFyYW1zOiB7IHBhdGhNYXRjaDogJy9ub3QtZm91bmQnIH19XG4gICAgaWYgKHBhcmFtcy5wYXRoTWF0Y2gpIHsgcGFyYW1zWzBdID0gcGFyYW1zLnBhdGhNYXRjaDsgfVxuXG4gICAgcmV0dXJuIGZpbGxlcihwYXJhbXMsIHsgcHJldHR5OiB0cnVlIH0pXG4gIH0gY2F0Y2ggKGUpIHtcbiAgICBpZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJykge1xuICAgICAgd2FybihmYWxzZSwgKFwibWlzc2luZyBwYXJhbSBmb3IgXCIgKyByb3V0ZU1zZyArIFwiOiBcIiArIChlLm1lc3NhZ2UpKSk7XG4gICAgfVxuICAgIHJldHVybiAnJ1xuICB9IGZpbmFsbHkge1xuICAgIC8vIGRlbGV0ZSB0aGUgMCBpZiBpdCB3YXMgYWRkZWRcbiAgICBkZWxldGUgcGFyYW1zWzBdO1xuICB9XG59XG5cbi8qICAqL1xuXG5mdW5jdGlvbiBub3JtYWxpemVMb2NhdGlvbiAoXG4gIHJhdyxcbiAgY3VycmVudCxcbiAgYXBwZW5kLFxuICByb3V0ZXJcbikge1xuICB2YXIgbmV4dCA9IHR5cGVvZiByYXcgPT09ICdzdHJpbmcnID8geyBwYXRoOiByYXcgfSA6IHJhdztcbiAgLy8gbmFtZWQgdGFyZ2V0XG4gIGlmIChuZXh0Ll9ub3JtYWxpemVkKSB7XG4gICAgcmV0dXJuIG5leHRcbiAgfSBlbHNlIGlmIChuZXh0Lm5hbWUpIHtcbiAgICByZXR1cm4gZXh0ZW5kKHt9LCByYXcpXG4gIH1cblxuICAvLyByZWxhdGl2ZSBwYXJhbXNcbiAgaWYgKCFuZXh0LnBhdGggJiYgbmV4dC5wYXJhbXMgJiYgY3VycmVudCkge1xuICAgIG5leHQgPSBleHRlbmQoe30sIG5leHQpO1xuICAgIG5leHQuX25vcm1hbGl6ZWQgPSB0cnVlO1xuICAgIHZhciBwYXJhbXMgPSBleHRlbmQoZXh0ZW5kKHt9LCBjdXJyZW50LnBhcmFtcyksIG5leHQucGFyYW1zKTtcbiAgICBpZiAoY3VycmVudC5uYW1lKSB7XG4gICAgICBuZXh0Lm5hbWUgPSBjdXJyZW50Lm5hbWU7XG4gICAgICBuZXh0LnBhcmFtcyA9IHBhcmFtcztcbiAgICB9IGVsc2UgaWYgKGN1cnJlbnQubWF0Y2hlZC5sZW5ndGgpIHtcbiAgICAgIHZhciByYXdQYXRoID0gY3VycmVudC5tYXRjaGVkW2N1cnJlbnQubWF0Y2hlZC5sZW5ndGggLSAxXS5wYXRoO1xuICAgICAgbmV4dC5wYXRoID0gZmlsbFBhcmFtcyhyYXdQYXRoLCBwYXJhbXMsIChcInBhdGggXCIgKyAoY3VycmVudC5wYXRoKSkpO1xuICAgIH0gZWxzZSBpZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJykge1xuICAgICAgd2FybihmYWxzZSwgXCJyZWxhdGl2ZSBwYXJhbXMgbmF2aWdhdGlvbiByZXF1aXJlcyBhIGN1cnJlbnQgcm91dGUuXCIpO1xuICAgIH1cbiAgICByZXR1cm4gbmV4dFxuICB9XG5cbiAgdmFyIHBhcnNlZFBhdGggPSBwYXJzZVBhdGgobmV4dC5wYXRoIHx8ICcnKTtcbiAgdmFyIGJhc2VQYXRoID0gKGN1cnJlbnQgJiYgY3VycmVudC5wYXRoKSB8fCAnLyc7XG4gIHZhciBwYXRoID0gcGFyc2VkUGF0aC5wYXRoXG4gICAgPyByZXNvbHZlUGF0aChwYXJzZWRQYXRoLnBhdGgsIGJhc2VQYXRoLCBhcHBlbmQgfHwgbmV4dC5hcHBlbmQpXG4gICAgOiBiYXNlUGF0aDtcblxuICB2YXIgcXVlcnkgPSByZXNvbHZlUXVlcnkoXG4gICAgcGFyc2VkUGF0aC5xdWVyeSxcbiAgICBuZXh0LnF1ZXJ5LFxuICAgIHJvdXRlciAmJiByb3V0ZXIub3B0aW9ucy5wYXJzZVF1ZXJ5XG4gICk7XG5cbiAgdmFyIGhhc2ggPSBuZXh0Lmhhc2ggfHwgcGFyc2VkUGF0aC5oYXNoO1xuICBpZiAoaGFzaCAmJiBoYXNoLmNoYXJBdCgwKSAhPT0gJyMnKSB7XG4gICAgaGFzaCA9IFwiI1wiICsgaGFzaDtcbiAgfVxuXG4gIHJldHVybiB7XG4gICAgX25vcm1hbGl6ZWQ6IHRydWUsXG4gICAgcGF0aDogcGF0aCxcbiAgICBxdWVyeTogcXVlcnksXG4gICAgaGFzaDogaGFzaFxuICB9XG59XG5cbi8qICAqL1xuXG4vLyB3b3JrIGFyb3VuZCB3ZWlyZCBmbG93IGJ1Z1xudmFyIHRvVHlwZXMgPSBbU3RyaW5nLCBPYmplY3RdO1xudmFyIGV2ZW50VHlwZXMgPSBbU3RyaW5nLCBBcnJheV07XG5cbnZhciBub29wID0gZnVuY3Rpb24gKCkge307XG5cbnZhciBMaW5rID0ge1xuICBuYW1lOiAnUm91dGVyTGluaycsXG4gIHByb3BzOiB7XG4gICAgdG86IHtcbiAgICAgIHR5cGU6IHRvVHlwZXMsXG4gICAgICByZXF1aXJlZDogdHJ1ZVxuICAgIH0sXG4gICAgdGFnOiB7XG4gICAgICB0eXBlOiBTdHJpbmcsXG4gICAgICBkZWZhdWx0OiAnYSdcbiAgICB9LFxuICAgIGV4YWN0OiBCb29sZWFuLFxuICAgIGFwcGVuZDogQm9vbGVhbixcbiAgICByZXBsYWNlOiBCb29sZWFuLFxuICAgIGFjdGl2ZUNsYXNzOiBTdHJpbmcsXG4gICAgZXhhY3RBY3RpdmVDbGFzczogU3RyaW5nLFxuICAgIGV2ZW50OiB7XG4gICAgICB0eXBlOiBldmVudFR5cGVzLFxuICAgICAgZGVmYXVsdDogJ2NsaWNrJ1xuICAgIH1cbiAgfSxcbiAgcmVuZGVyOiBmdW5jdGlvbiByZW5kZXIgKGgpIHtcbiAgICB2YXIgdGhpcyQxID0gdGhpcztcblxuICAgIHZhciByb3V0ZXIgPSB0aGlzLiRyb3V0ZXI7XG4gICAgdmFyIGN1cnJlbnQgPSB0aGlzLiRyb3V0ZTtcbiAgICB2YXIgcmVmID0gcm91dGVyLnJlc29sdmUoXG4gICAgICB0aGlzLnRvLFxuICAgICAgY3VycmVudCxcbiAgICAgIHRoaXMuYXBwZW5kXG4gICAgKTtcbiAgICB2YXIgbG9jYXRpb24gPSByZWYubG9jYXRpb247XG4gICAgdmFyIHJvdXRlID0gcmVmLnJvdXRlO1xuICAgIHZhciBocmVmID0gcmVmLmhyZWY7XG5cbiAgICB2YXIgY2xhc3NlcyA9IHt9O1xuICAgIHZhciBnbG9iYWxBY3RpdmVDbGFzcyA9IHJvdXRlci5vcHRpb25zLmxpbmtBY3RpdmVDbGFzcztcbiAgICB2YXIgZ2xvYmFsRXhhY3RBY3RpdmVDbGFzcyA9IHJvdXRlci5vcHRpb25zLmxpbmtFeGFjdEFjdGl2ZUNsYXNzO1xuICAgIC8vIFN1cHBvcnQgZ2xvYmFsIGVtcHR5IGFjdGl2ZSBjbGFzc1xuICAgIHZhciBhY3RpdmVDbGFzc0ZhbGxiYWNrID1cbiAgICAgIGdsb2JhbEFjdGl2ZUNsYXNzID09IG51bGwgPyAncm91dGVyLWxpbmstYWN0aXZlJyA6IGdsb2JhbEFjdGl2ZUNsYXNzO1xuICAgIHZhciBleGFjdEFjdGl2ZUNsYXNzRmFsbGJhY2sgPVxuICAgICAgZ2xvYmFsRXhhY3RBY3RpdmVDbGFzcyA9PSBudWxsXG4gICAgICAgID8gJ3JvdXRlci1saW5rLWV4YWN0LWFjdGl2ZSdcbiAgICAgICAgOiBnbG9iYWxFeGFjdEFjdGl2ZUNsYXNzO1xuICAgIHZhciBhY3RpdmVDbGFzcyA9XG4gICAgICB0aGlzLmFjdGl2ZUNsYXNzID09IG51bGwgPyBhY3RpdmVDbGFzc0ZhbGxiYWNrIDogdGhpcy5hY3RpdmVDbGFzcztcbiAgICB2YXIgZXhhY3RBY3RpdmVDbGFzcyA9XG4gICAgICB0aGlzLmV4YWN0QWN0aXZlQ2xhc3MgPT0gbnVsbFxuICAgICAgICA/IGV4YWN0QWN0aXZlQ2xhc3NGYWxsYmFja1xuICAgICAgICA6IHRoaXMuZXhhY3RBY3RpdmVDbGFzcztcblxuICAgIHZhciBjb21wYXJlVGFyZ2V0ID0gcm91dGUucmVkaXJlY3RlZEZyb21cbiAgICAgID8gY3JlYXRlUm91dGUobnVsbCwgbm9ybWFsaXplTG9jYXRpb24ocm91dGUucmVkaXJlY3RlZEZyb20pLCBudWxsLCByb3V0ZXIpXG4gICAgICA6IHJvdXRlO1xuXG4gICAgY2xhc3Nlc1tleGFjdEFjdGl2ZUNsYXNzXSA9IGlzU2FtZVJvdXRlKGN1cnJlbnQsIGNvbXBhcmVUYXJnZXQpO1xuICAgIGNsYXNzZXNbYWN0aXZlQ2xhc3NdID0gdGhpcy5leGFjdFxuICAgICAgPyBjbGFzc2VzW2V4YWN0QWN0aXZlQ2xhc3NdXG4gICAgICA6IGlzSW5jbHVkZWRSb3V0ZShjdXJyZW50LCBjb21wYXJlVGFyZ2V0KTtcblxuICAgIHZhciBoYW5kbGVyID0gZnVuY3Rpb24gKGUpIHtcbiAgICAgIGlmIChndWFyZEV2ZW50KGUpKSB7XG4gICAgICAgIGlmICh0aGlzJDEucmVwbGFjZSkge1xuICAgICAgICAgIHJvdXRlci5yZXBsYWNlKGxvY2F0aW9uLCBub29wKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICByb3V0ZXIucHVzaChsb2NhdGlvbiwgbm9vcCk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9O1xuXG4gICAgdmFyIG9uID0geyBjbGljazogZ3VhcmRFdmVudCB9O1xuICAgIGlmIChBcnJheS5pc0FycmF5KHRoaXMuZXZlbnQpKSB7XG4gICAgICB0aGlzLmV2ZW50LmZvckVhY2goZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgb25bZV0gPSBoYW5kbGVyO1xuICAgICAgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIG9uW3RoaXMuZXZlbnRdID0gaGFuZGxlcjtcbiAgICB9XG5cbiAgICB2YXIgZGF0YSA9IHsgY2xhc3M6IGNsYXNzZXMgfTtcblxuICAgIHZhciBzY29wZWRTbG90ID1cbiAgICAgICF0aGlzLiRzY29wZWRTbG90cy4kaGFzTm9ybWFsICYmXG4gICAgICB0aGlzLiRzY29wZWRTbG90cy5kZWZhdWx0ICYmXG4gICAgICB0aGlzLiRzY29wZWRTbG90cy5kZWZhdWx0KHtcbiAgICAgICAgaHJlZjogaHJlZixcbiAgICAgICAgcm91dGU6IHJvdXRlLFxuICAgICAgICBuYXZpZ2F0ZTogaGFuZGxlcixcbiAgICAgICAgaXNBY3RpdmU6IGNsYXNzZXNbYWN0aXZlQ2xhc3NdLFxuICAgICAgICBpc0V4YWN0QWN0aXZlOiBjbGFzc2VzW2V4YWN0QWN0aXZlQ2xhc3NdXG4gICAgICB9KTtcblxuICAgIGlmIChzY29wZWRTbG90KSB7XG4gICAgICBpZiAoc2NvcGVkU2xvdC5sZW5ndGggPT09IDEpIHtcbiAgICAgICAgcmV0dXJuIHNjb3BlZFNsb3RbMF1cbiAgICAgIH0gZWxzZSBpZiAoc2NvcGVkU2xvdC5sZW5ndGggPiAxIHx8ICFzY29wZWRTbG90Lmxlbmd0aCkge1xuICAgICAgICBpZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJykge1xuICAgICAgICAgIHdhcm4oXG4gICAgICAgICAgICBmYWxzZSxcbiAgICAgICAgICAgIChcIlJvdXRlckxpbmsgd2l0aCB0bz1cXFwiXCIgKyAodGhpcy5wcm9wcy50bykgKyBcIlxcXCIgaXMgdHJ5aW5nIHRvIHVzZSBhIHNjb3BlZCBzbG90IGJ1dCBpdCBkaWRuJ3QgcHJvdmlkZSBleGFjdGx5IG9uZSBjaGlsZC5cIilcbiAgICAgICAgICApO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBzY29wZWRTbG90Lmxlbmd0aCA9PT0gMCA/IGgoKSA6IGgoJ3NwYW4nLCB7fSwgc2NvcGVkU2xvdClcbiAgICAgIH1cbiAgICB9XG5cbiAgICBpZiAodGhpcy50YWcgPT09ICdhJykge1xuICAgICAgZGF0YS5vbiA9IG9uO1xuICAgICAgZGF0YS5hdHRycyA9IHsgaHJlZjogaHJlZiB9O1xuICAgIH0gZWxzZSB7XG4gICAgICAvLyBmaW5kIHRoZSBmaXJzdCA8YT4gY2hpbGQgYW5kIGFwcGx5IGxpc3RlbmVyIGFuZCBocmVmXG4gICAgICB2YXIgYSA9IGZpbmRBbmNob3IodGhpcy4kc2xvdHMuZGVmYXVsdCk7XG4gICAgICBpZiAoYSkge1xuICAgICAgICAvLyBpbiBjYXNlIHRoZSA8YT4gaXMgYSBzdGF0aWMgbm9kZVxuICAgICAgICBhLmlzU3RhdGljID0gZmFsc2U7XG4gICAgICAgIHZhciBhRGF0YSA9IChhLmRhdGEgPSBleHRlbmQoe30sIGEuZGF0YSkpO1xuICAgICAgICBhRGF0YS5vbiA9IGFEYXRhLm9uIHx8IHt9O1xuICAgICAgICAvLyB0cmFuc2Zvcm0gZXhpc3RpbmcgZXZlbnRzIGluIGJvdGggb2JqZWN0cyBpbnRvIGFycmF5cyBzbyB3ZSBjYW4gcHVzaCBsYXRlclxuICAgICAgICBmb3IgKHZhciBldmVudCBpbiBhRGF0YS5vbikge1xuICAgICAgICAgIHZhciBoYW5kbGVyJDEgPSBhRGF0YS5vbltldmVudF07XG4gICAgICAgICAgaWYgKGV2ZW50IGluIG9uKSB7XG4gICAgICAgICAgICBhRGF0YS5vbltldmVudF0gPSBBcnJheS5pc0FycmF5KGhhbmRsZXIkMSkgPyBoYW5kbGVyJDEgOiBbaGFuZGxlciQxXTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgLy8gYXBwZW5kIG5ldyBsaXN0ZW5lcnMgZm9yIHJvdXRlci1saW5rXG4gICAgICAgIGZvciAodmFyIGV2ZW50JDEgaW4gb24pIHtcbiAgICAgICAgICBpZiAoZXZlbnQkMSBpbiBhRGF0YS5vbikge1xuICAgICAgICAgICAgLy8gb25bZXZlbnRdIGlzIGFsd2F5cyBhIGZ1bmN0aW9uXG4gICAgICAgICAgICBhRGF0YS5vbltldmVudCQxXS5wdXNoKG9uW2V2ZW50JDFdKTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgYURhdGEub25bZXZlbnQkMV0gPSBoYW5kbGVyO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIHZhciBhQXR0cnMgPSAoYS5kYXRhLmF0dHJzID0gZXh0ZW5kKHt9LCBhLmRhdGEuYXR0cnMpKTtcbiAgICAgICAgYUF0dHJzLmhyZWYgPSBocmVmO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgLy8gZG9lc24ndCBoYXZlIDxhPiBjaGlsZCwgYXBwbHkgbGlzdGVuZXIgdG8gc2VsZlxuICAgICAgICBkYXRhLm9uID0gb247XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIGgodGhpcy50YWcsIGRhdGEsIHRoaXMuJHNsb3RzLmRlZmF1bHQpXG4gIH1cbn07XG5cbmZ1bmN0aW9uIGd1YXJkRXZlbnQgKGUpIHtcbiAgLy8gZG9uJ3QgcmVkaXJlY3Qgd2l0aCBjb250cm9sIGtleXNcbiAgaWYgKGUubWV0YUtleSB8fCBlLmFsdEtleSB8fCBlLmN0cmxLZXkgfHwgZS5zaGlmdEtleSkgeyByZXR1cm4gfVxuICAvLyBkb24ndCByZWRpcmVjdCB3aGVuIHByZXZlbnREZWZhdWx0IGNhbGxlZFxuICBpZiAoZS5kZWZhdWx0UHJldmVudGVkKSB7IHJldHVybiB9XG4gIC8vIGRvbid0IHJlZGlyZWN0IG9uIHJpZ2h0IGNsaWNrXG4gIGlmIChlLmJ1dHRvbiAhPT0gdW5kZWZpbmVkICYmIGUuYnV0dG9uICE9PSAwKSB7IHJldHVybiB9XG4gIC8vIGRvbid0IHJlZGlyZWN0IGlmIGB0YXJnZXQ9XCJfYmxhbmtcImBcbiAgaWYgKGUuY3VycmVudFRhcmdldCAmJiBlLmN1cnJlbnRUYXJnZXQuZ2V0QXR0cmlidXRlKSB7XG4gICAgdmFyIHRhcmdldCA9IGUuY3VycmVudFRhcmdldC5nZXRBdHRyaWJ1dGUoJ3RhcmdldCcpO1xuICAgIGlmICgvXFxiX2JsYW5rXFxiL2kudGVzdCh0YXJnZXQpKSB7IHJldHVybiB9XG4gIH1cbiAgLy8gdGhpcyBtYXkgYmUgYSBXZWV4IGV2ZW50IHdoaWNoIGRvZXNuJ3QgaGF2ZSB0aGlzIG1ldGhvZFxuICBpZiAoZS5wcmV2ZW50RGVmYXVsdCkge1xuICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgfVxuICByZXR1cm4gdHJ1ZVxufVxuXG5mdW5jdGlvbiBmaW5kQW5jaG9yIChjaGlsZHJlbikge1xuICBpZiAoY2hpbGRyZW4pIHtcbiAgICB2YXIgY2hpbGQ7XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBjaGlsZHJlbi5sZW5ndGg7IGkrKykge1xuICAgICAgY2hpbGQgPSBjaGlsZHJlbltpXTtcbiAgICAgIGlmIChjaGlsZC50YWcgPT09ICdhJykge1xuICAgICAgICByZXR1cm4gY2hpbGRcbiAgICAgIH1cbiAgICAgIGlmIChjaGlsZC5jaGlsZHJlbiAmJiAoY2hpbGQgPSBmaW5kQW5jaG9yKGNoaWxkLmNoaWxkcmVuKSkpIHtcbiAgICAgICAgcmV0dXJuIGNoaWxkXG4gICAgICB9XG4gICAgfVxuICB9XG59XG5cbnZhciBfVnVlO1xuXG5mdW5jdGlvbiBpbnN0YWxsIChWdWUpIHtcbiAgaWYgKGluc3RhbGwuaW5zdGFsbGVkICYmIF9WdWUgPT09IFZ1ZSkgeyByZXR1cm4gfVxuICBpbnN0YWxsLmluc3RhbGxlZCA9IHRydWU7XG5cbiAgX1Z1ZSA9IFZ1ZTtcblxuICB2YXIgaXNEZWYgPSBmdW5jdGlvbiAodikgeyByZXR1cm4gdiAhPT0gdW5kZWZpbmVkOyB9O1xuXG4gIHZhciByZWdpc3Rlckluc3RhbmNlID0gZnVuY3Rpb24gKHZtLCBjYWxsVmFsKSB7XG4gICAgdmFyIGkgPSB2bS4kb3B0aW9ucy5fcGFyZW50Vm5vZGU7XG4gICAgaWYgKGlzRGVmKGkpICYmIGlzRGVmKGkgPSBpLmRhdGEpICYmIGlzRGVmKGkgPSBpLnJlZ2lzdGVyUm91dGVJbnN0YW5jZSkpIHtcbiAgICAgIGkodm0sIGNhbGxWYWwpO1xuICAgIH1cbiAgfTtcblxuICBWdWUubWl4aW4oe1xuICAgIGJlZm9yZUNyZWF0ZTogZnVuY3Rpb24gYmVmb3JlQ3JlYXRlICgpIHtcbiAgICAgIGlmIChpc0RlZih0aGlzLiRvcHRpb25zLnJvdXRlcikpIHtcbiAgICAgICAgdGhpcy5fcm91dGVyUm9vdCA9IHRoaXM7XG4gICAgICAgIHRoaXMuX3JvdXRlciA9IHRoaXMuJG9wdGlvbnMucm91dGVyO1xuICAgICAgICB0aGlzLl9yb3V0ZXIuaW5pdCh0aGlzKTtcbiAgICAgICAgVnVlLnV0aWwuZGVmaW5lUmVhY3RpdmUodGhpcywgJ19yb3V0ZScsIHRoaXMuX3JvdXRlci5oaXN0b3J5LmN1cnJlbnQpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5fcm91dGVyUm9vdCA9ICh0aGlzLiRwYXJlbnQgJiYgdGhpcy4kcGFyZW50Ll9yb3V0ZXJSb290KSB8fCB0aGlzO1xuICAgICAgfVxuICAgICAgcmVnaXN0ZXJJbnN0YW5jZSh0aGlzLCB0aGlzKTtcbiAgICB9LFxuICAgIGRlc3Ryb3llZDogZnVuY3Rpb24gZGVzdHJveWVkICgpIHtcbiAgICAgIHJlZ2lzdGVySW5zdGFuY2UodGhpcyk7XG4gICAgfVxuICB9KTtcblxuICBPYmplY3QuZGVmaW5lUHJvcGVydHkoVnVlLnByb3RvdHlwZSwgJyRyb3V0ZXInLCB7XG4gICAgZ2V0OiBmdW5jdGlvbiBnZXQgKCkgeyByZXR1cm4gdGhpcy5fcm91dGVyUm9vdC5fcm91dGVyIH1cbiAgfSk7XG5cbiAgT2JqZWN0LmRlZmluZVByb3BlcnR5KFZ1ZS5wcm90b3R5cGUsICckcm91dGUnLCB7XG4gICAgZ2V0OiBmdW5jdGlvbiBnZXQgKCkgeyByZXR1cm4gdGhpcy5fcm91dGVyUm9vdC5fcm91dGUgfVxuICB9KTtcblxuICBWdWUuY29tcG9uZW50KCdSb3V0ZXJWaWV3JywgVmlldyk7XG4gIFZ1ZS5jb21wb25lbnQoJ1JvdXRlckxpbmsnLCBMaW5rKTtcblxuICB2YXIgc3RyYXRzID0gVnVlLmNvbmZpZy5vcHRpb25NZXJnZVN0cmF0ZWdpZXM7XG4gIC8vIHVzZSB0aGUgc2FtZSBob29rIG1lcmdpbmcgc3RyYXRlZ3kgZm9yIHJvdXRlIGhvb2tzXG4gIHN0cmF0cy5iZWZvcmVSb3V0ZUVudGVyID0gc3RyYXRzLmJlZm9yZVJvdXRlTGVhdmUgPSBzdHJhdHMuYmVmb3JlUm91dGVVcGRhdGUgPSBzdHJhdHMuY3JlYXRlZDtcbn1cblxuLyogICovXG5cbnZhciBpbkJyb3dzZXIgPSB0eXBlb2Ygd2luZG93ICE9PSAndW5kZWZpbmVkJztcblxuLyogICovXG5cbmZ1bmN0aW9uIGNyZWF0ZVJvdXRlTWFwIChcbiAgcm91dGVzLFxuICBvbGRQYXRoTGlzdCxcbiAgb2xkUGF0aE1hcCxcbiAgb2xkTmFtZU1hcFxuKSB7XG4gIC8vIHRoZSBwYXRoIGxpc3QgaXMgdXNlZCB0byBjb250cm9sIHBhdGggbWF0Y2hpbmcgcHJpb3JpdHlcbiAgdmFyIHBhdGhMaXN0ID0gb2xkUGF0aExpc3QgfHwgW107XG4gIC8vICRmbG93LWRpc2FibGUtbGluZVxuICB2YXIgcGF0aE1hcCA9IG9sZFBhdGhNYXAgfHwgT2JqZWN0LmNyZWF0ZShudWxsKTtcbiAgLy8gJGZsb3ctZGlzYWJsZS1saW5lXG4gIHZhciBuYW1lTWFwID0gb2xkTmFtZU1hcCB8fCBPYmplY3QuY3JlYXRlKG51bGwpO1xuXG4gIHJvdXRlcy5mb3JFYWNoKGZ1bmN0aW9uIChyb3V0ZSkge1xuICAgIGFkZFJvdXRlUmVjb3JkKHBhdGhMaXN0LCBwYXRoTWFwLCBuYW1lTWFwLCByb3V0ZSk7XG4gIH0pO1xuXG4gIC8vIGVuc3VyZSB3aWxkY2FyZCByb3V0ZXMgYXJlIGFsd2F5cyBhdCB0aGUgZW5kXG4gIGZvciAodmFyIGkgPSAwLCBsID0gcGF0aExpc3QubGVuZ3RoOyBpIDwgbDsgaSsrKSB7XG4gICAgaWYgKHBhdGhMaXN0W2ldID09PSAnKicpIHtcbiAgICAgIHBhdGhMaXN0LnB1c2gocGF0aExpc3Quc3BsaWNlKGksIDEpWzBdKTtcbiAgICAgIGwtLTtcbiAgICAgIGktLTtcbiAgICB9XG4gIH1cblxuICBpZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgPT09ICdkZXZlbG9wbWVudCcpIHtcbiAgICAvLyB3YXJuIGlmIHJvdXRlcyBkbyBub3QgaW5jbHVkZSBsZWFkaW5nIHNsYXNoZXNcbiAgICB2YXIgZm91bmQgPSBwYXRoTGlzdFxuICAgIC8vIGNoZWNrIGZvciBtaXNzaW5nIGxlYWRpbmcgc2xhc2hcbiAgICAgIC5maWx0ZXIoZnVuY3Rpb24gKHBhdGgpIHsgcmV0dXJuIHBhdGggJiYgcGF0aC5jaGFyQXQoMCkgIT09ICcqJyAmJiBwYXRoLmNoYXJBdCgwKSAhPT0gJy8nOyB9KTtcblxuICAgIGlmIChmb3VuZC5sZW5ndGggPiAwKSB7XG4gICAgICB2YXIgcGF0aE5hbWVzID0gZm91bmQubWFwKGZ1bmN0aW9uIChwYXRoKSB7IHJldHVybiAoXCItIFwiICsgcGF0aCk7IH0pLmpvaW4oJ1xcbicpO1xuICAgICAgd2FybihmYWxzZSwgKFwiTm9uLW5lc3RlZCByb3V0ZXMgbXVzdCBpbmNsdWRlIGEgbGVhZGluZyBzbGFzaCBjaGFyYWN0ZXIuIEZpeCB0aGUgZm9sbG93aW5nIHJvdXRlczogXFxuXCIgKyBwYXRoTmFtZXMpKTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4ge1xuICAgIHBhdGhMaXN0OiBwYXRoTGlzdCxcbiAgICBwYXRoTWFwOiBwYXRoTWFwLFxuICAgIG5hbWVNYXA6IG5hbWVNYXBcbiAgfVxufVxuXG5mdW5jdGlvbiBhZGRSb3V0ZVJlY29yZCAoXG4gIHBhdGhMaXN0LFxuICBwYXRoTWFwLFxuICBuYW1lTWFwLFxuICByb3V0ZSxcbiAgcGFyZW50LFxuICBtYXRjaEFzXG4pIHtcbiAgdmFyIHBhdGggPSByb3V0ZS5wYXRoO1xuICB2YXIgbmFtZSA9IHJvdXRlLm5hbWU7XG4gIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XG4gICAgYXNzZXJ0KHBhdGggIT0gbnVsbCwgXCJcXFwicGF0aFxcXCIgaXMgcmVxdWlyZWQgaW4gYSByb3V0ZSBjb25maWd1cmF0aW9uLlwiKTtcbiAgICBhc3NlcnQoXG4gICAgICB0eXBlb2Ygcm91dGUuY29tcG9uZW50ICE9PSAnc3RyaW5nJyxcbiAgICAgIFwicm91dGUgY29uZmlnIFxcXCJjb21wb25lbnRcXFwiIGZvciBwYXRoOiBcIiArIChTdHJpbmcoXG4gICAgICAgIHBhdGggfHwgbmFtZVxuICAgICAgKSkgKyBcIiBjYW5ub3QgYmUgYSBcIiArIFwic3RyaW5nIGlkLiBVc2UgYW4gYWN0dWFsIGNvbXBvbmVudCBpbnN0ZWFkLlwiXG4gICAgKTtcbiAgfVxuXG4gIHZhciBwYXRoVG9SZWdleHBPcHRpb25zID1cbiAgICByb3V0ZS5wYXRoVG9SZWdleHBPcHRpb25zIHx8IHt9O1xuICB2YXIgbm9ybWFsaXplZFBhdGggPSBub3JtYWxpemVQYXRoKHBhdGgsIHBhcmVudCwgcGF0aFRvUmVnZXhwT3B0aW9ucy5zdHJpY3QpO1xuXG4gIGlmICh0eXBlb2Ygcm91dGUuY2FzZVNlbnNpdGl2ZSA9PT0gJ2Jvb2xlYW4nKSB7XG4gICAgcGF0aFRvUmVnZXhwT3B0aW9ucy5zZW5zaXRpdmUgPSByb3V0ZS5jYXNlU2Vuc2l0aXZlO1xuICB9XG5cbiAgdmFyIHJlY29yZCA9IHtcbiAgICBwYXRoOiBub3JtYWxpemVkUGF0aCxcbiAgICByZWdleDogY29tcGlsZVJvdXRlUmVnZXgobm9ybWFsaXplZFBhdGgsIHBhdGhUb1JlZ2V4cE9wdGlvbnMpLFxuICAgIGNvbXBvbmVudHM6IHJvdXRlLmNvbXBvbmVudHMgfHwgeyBkZWZhdWx0OiByb3V0ZS5jb21wb25lbnQgfSxcbiAgICBpbnN0YW5jZXM6IHt9LFxuICAgIG5hbWU6IG5hbWUsXG4gICAgcGFyZW50OiBwYXJlbnQsXG4gICAgbWF0Y2hBczogbWF0Y2hBcyxcbiAgICByZWRpcmVjdDogcm91dGUucmVkaXJlY3QsXG4gICAgYmVmb3JlRW50ZXI6IHJvdXRlLmJlZm9yZUVudGVyLFxuICAgIG1ldGE6IHJvdXRlLm1ldGEgfHwge30sXG4gICAgcHJvcHM6XG4gICAgICByb3V0ZS5wcm9wcyA9PSBudWxsXG4gICAgICAgID8ge31cbiAgICAgICAgOiByb3V0ZS5jb21wb25lbnRzXG4gICAgICAgICAgPyByb3V0ZS5wcm9wc1xuICAgICAgICAgIDogeyBkZWZhdWx0OiByb3V0ZS5wcm9wcyB9XG4gIH07XG5cbiAgaWYgKHJvdXRlLmNoaWxkcmVuKSB7XG4gICAgLy8gV2FybiBpZiByb3V0ZSBpcyBuYW1lZCwgZG9lcyBub3QgcmVkaXJlY3QgYW5kIGhhcyBhIGRlZmF1bHQgY2hpbGQgcm91dGUuXG4gICAgLy8gSWYgdXNlcnMgbmF2aWdhdGUgdG8gdGhpcyByb3V0ZSBieSBuYW1lLCB0aGUgZGVmYXVsdCBjaGlsZCB3aWxsXG4gICAgLy8gbm90IGJlIHJlbmRlcmVkIChHSCBJc3N1ZSAjNjI5KVxuICAgIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XG4gICAgICBpZiAoXG4gICAgICAgIHJvdXRlLm5hbWUgJiZcbiAgICAgICAgIXJvdXRlLnJlZGlyZWN0ICYmXG4gICAgICAgIHJvdXRlLmNoaWxkcmVuLnNvbWUoZnVuY3Rpb24gKGNoaWxkKSB7IHJldHVybiAvXlxcLz8kLy50ZXN0KGNoaWxkLnBhdGgpOyB9KVxuICAgICAgKSB7XG4gICAgICAgIHdhcm4oXG4gICAgICAgICAgZmFsc2UsXG4gICAgICAgICAgXCJOYW1lZCBSb3V0ZSAnXCIgKyAocm91dGUubmFtZSkgKyBcIicgaGFzIGEgZGVmYXVsdCBjaGlsZCByb3V0ZS4gXCIgK1xuICAgICAgICAgICAgXCJXaGVuIG5hdmlnYXRpbmcgdG8gdGhpcyBuYW1lZCByb3V0ZSAoOnRvPVxcXCJ7bmFtZTogJ1wiICsgKHJvdXRlLm5hbWUpICsgXCInXFxcIiksIFwiICtcbiAgICAgICAgICAgIFwidGhlIGRlZmF1bHQgY2hpbGQgcm91dGUgd2lsbCBub3QgYmUgcmVuZGVyZWQuIFJlbW92ZSB0aGUgbmFtZSBmcm9tIFwiICtcbiAgICAgICAgICAgIFwidGhpcyByb3V0ZSBhbmQgdXNlIHRoZSBuYW1lIG9mIHRoZSBkZWZhdWx0IGNoaWxkIHJvdXRlIGZvciBuYW1lZCBcIiArXG4gICAgICAgICAgICBcImxpbmtzIGluc3RlYWQuXCJcbiAgICAgICAgKTtcbiAgICAgIH1cbiAgICB9XG4gICAgcm91dGUuY2hpbGRyZW4uZm9yRWFjaChmdW5jdGlvbiAoY2hpbGQpIHtcbiAgICAgIHZhciBjaGlsZE1hdGNoQXMgPSBtYXRjaEFzXG4gICAgICAgID8gY2xlYW5QYXRoKChtYXRjaEFzICsgXCIvXCIgKyAoY2hpbGQucGF0aCkpKVxuICAgICAgICA6IHVuZGVmaW5lZDtcbiAgICAgIGFkZFJvdXRlUmVjb3JkKHBhdGhMaXN0LCBwYXRoTWFwLCBuYW1lTWFwLCBjaGlsZCwgcmVjb3JkLCBjaGlsZE1hdGNoQXMpO1xuICAgIH0pO1xuICB9XG5cbiAgaWYgKCFwYXRoTWFwW3JlY29yZC5wYXRoXSkge1xuICAgIHBhdGhMaXN0LnB1c2gocmVjb3JkLnBhdGgpO1xuICAgIHBhdGhNYXBbcmVjb3JkLnBhdGhdID0gcmVjb3JkO1xuICB9XG5cbiAgaWYgKHJvdXRlLmFsaWFzICE9PSB1bmRlZmluZWQpIHtcbiAgICB2YXIgYWxpYXNlcyA9IEFycmF5LmlzQXJyYXkocm91dGUuYWxpYXMpID8gcm91dGUuYWxpYXMgOiBbcm91dGUuYWxpYXNdO1xuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgYWxpYXNlcy5sZW5ndGg7ICsraSkge1xuICAgICAgdmFyIGFsaWFzID0gYWxpYXNlc1tpXTtcbiAgICAgIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nICYmIGFsaWFzID09PSBwYXRoKSB7XG4gICAgICAgIHdhcm4oXG4gICAgICAgICAgZmFsc2UsXG4gICAgICAgICAgKFwiRm91bmQgYW4gYWxpYXMgd2l0aCB0aGUgc2FtZSB2YWx1ZSBhcyB0aGUgcGF0aDogXFxcIlwiICsgcGF0aCArIFwiXFxcIi4gWW91IGhhdmUgdG8gcmVtb3ZlIHRoYXQgYWxpYXMuIEl0IHdpbGwgYmUgaWdub3JlZCBpbiBkZXZlbG9wbWVudC5cIilcbiAgICAgICAgKTtcbiAgICAgICAgLy8gc2tpcCBpbiBkZXYgdG8gbWFrZSBpdCB3b3JrXG4gICAgICAgIGNvbnRpbnVlXG4gICAgICB9XG5cbiAgICAgIHZhciBhbGlhc1JvdXRlID0ge1xuICAgICAgICBwYXRoOiBhbGlhcyxcbiAgICAgICAgY2hpbGRyZW46IHJvdXRlLmNoaWxkcmVuXG4gICAgICB9O1xuICAgICAgYWRkUm91dGVSZWNvcmQoXG4gICAgICAgIHBhdGhMaXN0LFxuICAgICAgICBwYXRoTWFwLFxuICAgICAgICBuYW1lTWFwLFxuICAgICAgICBhbGlhc1JvdXRlLFxuICAgICAgICBwYXJlbnQsXG4gICAgICAgIHJlY29yZC5wYXRoIHx8ICcvJyAvLyBtYXRjaEFzXG4gICAgICApO1xuICAgIH1cbiAgfVxuXG4gIGlmIChuYW1lKSB7XG4gICAgaWYgKCFuYW1lTWFwW25hbWVdKSB7XG4gICAgICBuYW1lTWFwW25hbWVdID0gcmVjb3JkO1xuICAgIH0gZWxzZSBpZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJyAmJiAhbWF0Y2hBcykge1xuICAgICAgd2FybihcbiAgICAgICAgZmFsc2UsXG4gICAgICAgIFwiRHVwbGljYXRlIG5hbWVkIHJvdXRlcyBkZWZpbml0aW9uOiBcIiArXG4gICAgICAgICAgXCJ7IG5hbWU6IFxcXCJcIiArIG5hbWUgKyBcIlxcXCIsIHBhdGg6IFxcXCJcIiArIChyZWNvcmQucGF0aCkgKyBcIlxcXCIgfVwiXG4gICAgICApO1xuICAgIH1cbiAgfVxufVxuXG5mdW5jdGlvbiBjb21waWxlUm91dGVSZWdleCAoXG4gIHBhdGgsXG4gIHBhdGhUb1JlZ2V4cE9wdGlvbnNcbikge1xuICB2YXIgcmVnZXggPSBwYXRoVG9SZWdleHBfMShwYXRoLCBbXSwgcGF0aFRvUmVnZXhwT3B0aW9ucyk7XG4gIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XG4gICAgdmFyIGtleXMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuICAgIHJlZ2V4LmtleXMuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7XG4gICAgICB3YXJuKFxuICAgICAgICAha2V5c1trZXkubmFtZV0sXG4gICAgICAgIChcIkR1cGxpY2F0ZSBwYXJhbSBrZXlzIGluIHJvdXRlIHdpdGggcGF0aDogXFxcIlwiICsgcGF0aCArIFwiXFxcIlwiKVxuICAgICAgKTtcbiAgICAgIGtleXNba2V5Lm5hbWVdID0gdHJ1ZTtcbiAgICB9KTtcbiAgfVxuICByZXR1cm4gcmVnZXhcbn1cblxuZnVuY3Rpb24gbm9ybWFsaXplUGF0aCAoXG4gIHBhdGgsXG4gIHBhcmVudCxcbiAgc3RyaWN0XG4pIHtcbiAgaWYgKCFzdHJpY3QpIHsgcGF0aCA9IHBhdGgucmVwbGFjZSgvXFwvJC8sICcnKTsgfVxuICBpZiAocGF0aFswXSA9PT0gJy8nKSB7IHJldHVybiBwYXRoIH1cbiAgaWYgKHBhcmVudCA9PSBudWxsKSB7IHJldHVybiBwYXRoIH1cbiAgcmV0dXJuIGNsZWFuUGF0aCgoKHBhcmVudC5wYXRoKSArIFwiL1wiICsgcGF0aCkpXG59XG5cbi8qICAqL1xuXG5cblxuZnVuY3Rpb24gY3JlYXRlTWF0Y2hlciAoXG4gIHJvdXRlcyxcbiAgcm91dGVyXG4pIHtcbiAgdmFyIHJlZiA9IGNyZWF0ZVJvdXRlTWFwKHJvdXRlcyk7XG4gIHZhciBwYXRoTGlzdCA9IHJlZi5wYXRoTGlzdDtcbiAgdmFyIHBhdGhNYXAgPSByZWYucGF0aE1hcDtcbiAgdmFyIG5hbWVNYXAgPSByZWYubmFtZU1hcDtcblxuICBmdW5jdGlvbiBhZGRSb3V0ZXMgKHJvdXRlcykge1xuICAgIGNyZWF0ZVJvdXRlTWFwKHJvdXRlcywgcGF0aExpc3QsIHBhdGhNYXAsIG5hbWVNYXApO1xuICB9XG5cbiAgZnVuY3Rpb24gbWF0Y2ggKFxuICAgIHJhdyxcbiAgICBjdXJyZW50Um91dGUsXG4gICAgcmVkaXJlY3RlZEZyb21cbiAgKSB7XG4gICAgdmFyIGxvY2F0aW9uID0gbm9ybWFsaXplTG9jYXRpb24ocmF3LCBjdXJyZW50Um91dGUsIGZhbHNlLCByb3V0ZXIpO1xuICAgIHZhciBuYW1lID0gbG9jYXRpb24ubmFtZTtcblxuICAgIGlmIChuYW1lKSB7XG4gICAgICB2YXIgcmVjb3JkID0gbmFtZU1hcFtuYW1lXTtcbiAgICAgIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XG4gICAgICAgIHdhcm4ocmVjb3JkLCAoXCJSb3V0ZSB3aXRoIG5hbWUgJ1wiICsgbmFtZSArIFwiJyBkb2VzIG5vdCBleGlzdFwiKSk7XG4gICAgICB9XG4gICAgICBpZiAoIXJlY29yZCkgeyByZXR1cm4gX2NyZWF0ZVJvdXRlKG51bGwsIGxvY2F0aW9uKSB9XG4gICAgICB2YXIgcGFyYW1OYW1lcyA9IHJlY29yZC5yZWdleC5rZXlzXG4gICAgICAgIC5maWx0ZXIoZnVuY3Rpb24gKGtleSkgeyByZXR1cm4gIWtleS5vcHRpb25hbDsgfSlcbiAgICAgICAgLm1hcChmdW5jdGlvbiAoa2V5KSB7IHJldHVybiBrZXkubmFtZTsgfSk7XG5cbiAgICAgIGlmICh0eXBlb2YgbG9jYXRpb24ucGFyYW1zICE9PSAnb2JqZWN0Jykge1xuICAgICAgICBsb2NhdGlvbi5wYXJhbXMgPSB7fTtcbiAgICAgIH1cblxuICAgICAgaWYgKGN1cnJlbnRSb3V0ZSAmJiB0eXBlb2YgY3VycmVudFJvdXRlLnBhcmFtcyA9PT0gJ29iamVjdCcpIHtcbiAgICAgICAgZm9yICh2YXIga2V5IGluIGN1cnJlbnRSb3V0ZS5wYXJhbXMpIHtcbiAgICAgICAgICBpZiAoIShrZXkgaW4gbG9jYXRpb24ucGFyYW1zKSAmJiBwYXJhbU5hbWVzLmluZGV4T2Yoa2V5KSA+IC0xKSB7XG4gICAgICAgICAgICBsb2NhdGlvbi5wYXJhbXNba2V5XSA9IGN1cnJlbnRSb3V0ZS5wYXJhbXNba2V5XTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgbG9jYXRpb24ucGF0aCA9IGZpbGxQYXJhbXMocmVjb3JkLnBhdGgsIGxvY2F0aW9uLnBhcmFtcywgKFwibmFtZWQgcm91dGUgXFxcIlwiICsgbmFtZSArIFwiXFxcIlwiKSk7XG4gICAgICByZXR1cm4gX2NyZWF0ZVJvdXRlKHJlY29yZCwgbG9jYXRpb24sIHJlZGlyZWN0ZWRGcm9tKVxuICAgIH0gZWxzZSBpZiAobG9jYXRpb24ucGF0aCkge1xuICAgICAgbG9jYXRpb24ucGFyYW1zID0ge307XG4gICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHBhdGhMaXN0Lmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIHZhciBwYXRoID0gcGF0aExpc3RbaV07XG4gICAgICAgIHZhciByZWNvcmQkMSA9IHBhdGhNYXBbcGF0aF07XG4gICAgICAgIGlmIChtYXRjaFJvdXRlKHJlY29yZCQxLnJlZ2V4LCBsb2NhdGlvbi5wYXRoLCBsb2NhdGlvbi5wYXJhbXMpKSB7XG4gICAgICAgICAgcmV0dXJuIF9jcmVhdGVSb3V0ZShyZWNvcmQkMSwgbG9jYXRpb24sIHJlZGlyZWN0ZWRGcm9tKVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICAgIC8vIG5vIG1hdGNoXG4gICAgcmV0dXJuIF9jcmVhdGVSb3V0ZShudWxsLCBsb2NhdGlvbilcbiAgfVxuXG4gIGZ1bmN0aW9uIHJlZGlyZWN0IChcbiAgICByZWNvcmQsXG4gICAgbG9jYXRpb25cbiAgKSB7XG4gICAgdmFyIG9yaWdpbmFsUmVkaXJlY3QgPSByZWNvcmQucmVkaXJlY3Q7XG4gICAgdmFyIHJlZGlyZWN0ID0gdHlwZW9mIG9yaWdpbmFsUmVkaXJlY3QgPT09ICdmdW5jdGlvbidcbiAgICAgID8gb3JpZ2luYWxSZWRpcmVjdChjcmVhdGVSb3V0ZShyZWNvcmQsIGxvY2F0aW9uLCBudWxsLCByb3V0ZXIpKVxuICAgICAgOiBvcmlnaW5hbFJlZGlyZWN0O1xuXG4gICAgaWYgKHR5cGVvZiByZWRpcmVjdCA9PT0gJ3N0cmluZycpIHtcbiAgICAgIHJlZGlyZWN0ID0geyBwYXRoOiByZWRpcmVjdCB9O1xuICAgIH1cblxuICAgIGlmICghcmVkaXJlY3QgfHwgdHlwZW9mIHJlZGlyZWN0ICE9PSAnb2JqZWN0Jykge1xuICAgICAgaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcbiAgICAgICAgd2FybihcbiAgICAgICAgICBmYWxzZSwgKFwiaW52YWxpZCByZWRpcmVjdCBvcHRpb246IFwiICsgKEpTT04uc3RyaW5naWZ5KHJlZGlyZWN0KSkpXG4gICAgICAgICk7XG4gICAgICB9XG4gICAgICByZXR1cm4gX2NyZWF0ZVJvdXRlKG51bGwsIGxvY2F0aW9uKVxuICAgIH1cblxuICAgIHZhciByZSA9IHJlZGlyZWN0O1xuICAgIHZhciBuYW1lID0gcmUubmFtZTtcbiAgICB2YXIgcGF0aCA9IHJlLnBhdGg7XG4gICAgdmFyIHF1ZXJ5ID0gbG9jYXRpb24ucXVlcnk7XG4gICAgdmFyIGhhc2ggPSBsb2NhdGlvbi5oYXNoO1xuICAgIHZhciBwYXJhbXMgPSBsb2NhdGlvbi5wYXJhbXM7XG4gICAgcXVlcnkgPSByZS5oYXNPd25Qcm9wZXJ0eSgncXVlcnknKSA/IHJlLnF1ZXJ5IDogcXVlcnk7XG4gICAgaGFzaCA9IHJlLmhhc093blByb3BlcnR5KCdoYXNoJykgPyByZS5oYXNoIDogaGFzaDtcbiAgICBwYXJhbXMgPSByZS5oYXNPd25Qcm9wZXJ0eSgncGFyYW1zJykgPyByZS5wYXJhbXMgOiBwYXJhbXM7XG5cbiAgICBpZiAobmFtZSkge1xuICAgICAgLy8gcmVzb2x2ZWQgbmFtZWQgZGlyZWN0XG4gICAgICB2YXIgdGFyZ2V0UmVjb3JkID0gbmFtZU1hcFtuYW1lXTtcbiAgICAgIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XG4gICAgICAgIGFzc2VydCh0YXJnZXRSZWNvcmQsIChcInJlZGlyZWN0IGZhaWxlZDogbmFtZWQgcm91dGUgXFxcIlwiICsgbmFtZSArIFwiXFxcIiBub3QgZm91bmQuXCIpKTtcbiAgICAgIH1cbiAgICAgIHJldHVybiBtYXRjaCh7XG4gICAgICAgIF9ub3JtYWxpemVkOiB0cnVlLFxuICAgICAgICBuYW1lOiBuYW1lLFxuICAgICAgICBxdWVyeTogcXVlcnksXG4gICAgICAgIGhhc2g6IGhhc2gsXG4gICAgICAgIHBhcmFtczogcGFyYW1zXG4gICAgICB9LCB1bmRlZmluZWQsIGxvY2F0aW9uKVxuICAgIH0gZWxzZSBpZiAocGF0aCkge1xuICAgICAgLy8gMS4gcmVzb2x2ZSByZWxhdGl2ZSByZWRpcmVjdFxuICAgICAgdmFyIHJhd1BhdGggPSByZXNvbHZlUmVjb3JkUGF0aChwYXRoLCByZWNvcmQpO1xuICAgICAgLy8gMi4gcmVzb2x2ZSBwYXJhbXNcbiAgICAgIHZhciByZXNvbHZlZFBhdGggPSBmaWxsUGFyYW1zKHJhd1BhdGgsIHBhcmFtcywgKFwicmVkaXJlY3Qgcm91dGUgd2l0aCBwYXRoIFxcXCJcIiArIHJhd1BhdGggKyBcIlxcXCJcIikpO1xuICAgICAgLy8gMy4gcmVtYXRjaCB3aXRoIGV4aXN0aW5nIHF1ZXJ5IGFuZCBoYXNoXG4gICAgICByZXR1cm4gbWF0Y2goe1xuICAgICAgICBfbm9ybWFsaXplZDogdHJ1ZSxcbiAgICAgICAgcGF0aDogcmVzb2x2ZWRQYXRoLFxuICAgICAgICBxdWVyeTogcXVlcnksXG4gICAgICAgIGhhc2g6IGhhc2hcbiAgICAgIH0sIHVuZGVmaW5lZCwgbG9jYXRpb24pXG4gICAgfSBlbHNlIHtcbiAgICAgIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XG4gICAgICAgIHdhcm4oZmFsc2UsIChcImludmFsaWQgcmVkaXJlY3Qgb3B0aW9uOiBcIiArIChKU09OLnN0cmluZ2lmeShyZWRpcmVjdCkpKSk7XG4gICAgICB9XG4gICAgICByZXR1cm4gX2NyZWF0ZVJvdXRlKG51bGwsIGxvY2F0aW9uKVxuICAgIH1cbiAgfVxuXG4gIGZ1bmN0aW9uIGFsaWFzIChcbiAgICByZWNvcmQsXG4gICAgbG9jYXRpb24sXG4gICAgbWF0Y2hBc1xuICApIHtcbiAgICB2YXIgYWxpYXNlZFBhdGggPSBmaWxsUGFyYW1zKG1hdGNoQXMsIGxvY2F0aW9uLnBhcmFtcywgKFwiYWxpYXNlZCByb3V0ZSB3aXRoIHBhdGggXFxcIlwiICsgbWF0Y2hBcyArIFwiXFxcIlwiKSk7XG4gICAgdmFyIGFsaWFzZWRNYXRjaCA9IG1hdGNoKHtcbiAgICAgIF9ub3JtYWxpemVkOiB0cnVlLFxuICAgICAgcGF0aDogYWxpYXNlZFBhdGhcbiAgICB9KTtcbiAgICBpZiAoYWxpYXNlZE1hdGNoKSB7XG4gICAgICB2YXIgbWF0Y2hlZCA9IGFsaWFzZWRNYXRjaC5tYXRjaGVkO1xuICAgICAgdmFyIGFsaWFzZWRSZWNvcmQgPSBtYXRjaGVkW21hdGNoZWQubGVuZ3RoIC0gMV07XG4gICAgICBsb2NhdGlvbi5wYXJhbXMgPSBhbGlhc2VkTWF0Y2gucGFyYW1zO1xuICAgICAgcmV0dXJuIF9jcmVhdGVSb3V0ZShhbGlhc2VkUmVjb3JkLCBsb2NhdGlvbilcbiAgICB9XG4gICAgcmV0dXJuIF9jcmVhdGVSb3V0ZShudWxsLCBsb2NhdGlvbilcbiAgfVxuXG4gIGZ1bmN0aW9uIF9jcmVhdGVSb3V0ZSAoXG4gICAgcmVjb3JkLFxuICAgIGxvY2F0aW9uLFxuICAgIHJlZGlyZWN0ZWRGcm9tXG4gICkge1xuICAgIGlmIChyZWNvcmQgJiYgcmVjb3JkLnJlZGlyZWN0KSB7XG4gICAgICByZXR1cm4gcmVkaXJlY3QocmVjb3JkLCByZWRpcmVjdGVkRnJvbSB8fCBsb2NhdGlvbilcbiAgICB9XG4gICAgaWYgKHJlY29yZCAmJiByZWNvcmQubWF0Y2hBcykge1xuICAgICAgcmV0dXJuIGFsaWFzKHJlY29yZCwgbG9jYXRpb24sIHJlY29yZC5tYXRjaEFzKVxuICAgIH1cbiAgICByZXR1cm4gY3JlYXRlUm91dGUocmVjb3JkLCBsb2NhdGlvbiwgcmVkaXJlY3RlZEZyb20sIHJvdXRlcilcbiAgfVxuXG4gIHJldHVybiB7XG4gICAgbWF0Y2g6IG1hdGNoLFxuICAgIGFkZFJvdXRlczogYWRkUm91dGVzXG4gIH1cbn1cblxuZnVuY3Rpb24gbWF0Y2hSb3V0ZSAoXG4gIHJlZ2V4LFxuICBwYXRoLFxuICBwYXJhbXNcbikge1xuICB2YXIgbSA9IHBhdGgubWF0Y2gocmVnZXgpO1xuXG4gIGlmICghbSkge1xuICAgIHJldHVybiBmYWxzZVxuICB9IGVsc2UgaWYgKCFwYXJhbXMpIHtcbiAgICByZXR1cm4gdHJ1ZVxuICB9XG5cbiAgZm9yICh2YXIgaSA9IDEsIGxlbiA9IG0ubGVuZ3RoOyBpIDwgbGVuOyArK2kpIHtcbiAgICB2YXIga2V5ID0gcmVnZXgua2V5c1tpIC0gMV07XG4gICAgdmFyIHZhbCA9IHR5cGVvZiBtW2ldID09PSAnc3RyaW5nJyA/IGRlY29kZVVSSUNvbXBvbmVudChtW2ldKSA6IG1baV07XG4gICAgaWYgKGtleSkge1xuICAgICAgLy8gRml4ICMxOTk0OiB1c2luZyAqIHdpdGggcHJvcHM6IHRydWUgZ2VuZXJhdGVzIGEgcGFyYW0gbmFtZWQgMFxuICAgICAgcGFyYW1zW2tleS5uYW1lIHx8ICdwYXRoTWF0Y2gnXSA9IHZhbDtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gdHJ1ZVxufVxuXG5mdW5jdGlvbiByZXNvbHZlUmVjb3JkUGF0aCAocGF0aCwgcmVjb3JkKSB7XG4gIHJldHVybiByZXNvbHZlUGF0aChwYXRoLCByZWNvcmQucGFyZW50ID8gcmVjb3JkLnBhcmVudC5wYXRoIDogJy8nLCB0cnVlKVxufVxuXG4vKiAgKi9cblxuLy8gdXNlIFVzZXIgVGltaW5nIGFwaSAoaWYgcHJlc2VudCkgZm9yIG1vcmUgYWNjdXJhdGUga2V5IHByZWNpc2lvblxudmFyIFRpbWUgPVxuICBpbkJyb3dzZXIgJiYgd2luZG93LnBlcmZvcm1hbmNlICYmIHdpbmRvdy5wZXJmb3JtYW5jZS5ub3dcbiAgICA/IHdpbmRvdy5wZXJmb3JtYW5jZVxuICAgIDogRGF0ZTtcblxuZnVuY3Rpb24gZ2VuU3RhdGVLZXkgKCkge1xuICByZXR1cm4gVGltZS5ub3coKS50b0ZpeGVkKDMpXG59XG5cbnZhciBfa2V5ID0gZ2VuU3RhdGVLZXkoKTtcblxuZnVuY3Rpb24gZ2V0U3RhdGVLZXkgKCkge1xuICByZXR1cm4gX2tleVxufVxuXG5mdW5jdGlvbiBzZXRTdGF0ZUtleSAoa2V5KSB7XG4gIHJldHVybiAoX2tleSA9IGtleSlcbn1cblxuLyogICovXG5cbnZhciBwb3NpdGlvblN0b3JlID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcblxuZnVuY3Rpb24gc2V0dXBTY3JvbGwgKCkge1xuICAvLyBGaXggZm9yICMxNTg1IGZvciBGaXJlZm94XG4gIC8vIEZpeCBmb3IgIzIxOTUgQWRkIG9wdGlvbmFsIHRoaXJkIGF0dHJpYnV0ZSB0byB3b3JrYXJvdW5kIGEgYnVnIGluIHNhZmFyaSBodHRwczovL2J1Z3Mud2Via2l0Lm9yZy9zaG93X2J1Zy5jZ2k/aWQ9MTgyNjc4XG4gIC8vIEZpeCBmb3IgIzI3NzQgU3VwcG9ydCBmb3IgYXBwcyBsb2FkZWQgZnJvbSBXaW5kb3dzIGZpbGUgc2hhcmVzIG5vdCBtYXBwZWQgdG8gbmV0d29yayBkcml2ZXM6IHJlcGxhY2VkIGxvY2F0aW9uLm9yaWdpbiB3aXRoXG4gIC8vIHdpbmRvdy5sb2NhdGlvbi5wcm90b2NvbCArICcvLycgKyB3aW5kb3cubG9jYXRpb24uaG9zdFxuICAvLyBsb2NhdGlvbi5ob3N0IGNvbnRhaW5zIHRoZSBwb3J0IGFuZCBsb2NhdGlvbi5ob3N0bmFtZSBkb2Vzbid0XG4gIHZhciBwcm90b2NvbEFuZFBhdGggPSB3aW5kb3cubG9jYXRpb24ucHJvdG9jb2wgKyAnLy8nICsgd2luZG93LmxvY2F0aW9uLmhvc3Q7XG4gIHZhciBhYnNvbHV0ZVBhdGggPSB3aW5kb3cubG9jYXRpb24uaHJlZi5yZXBsYWNlKHByb3RvY29sQW5kUGF0aCwgJycpO1xuICB3aW5kb3cuaGlzdG9yeS5yZXBsYWNlU3RhdGUoeyBrZXk6IGdldFN0YXRlS2V5KCkgfSwgJycsIGFic29sdXRlUGF0aCk7XG4gIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdwb3BzdGF0ZScsIGZ1bmN0aW9uIChlKSB7XG4gICAgc2F2ZVNjcm9sbFBvc2l0aW9uKCk7XG4gICAgaWYgKGUuc3RhdGUgJiYgZS5zdGF0ZS5rZXkpIHtcbiAgICAgIHNldFN0YXRlS2V5KGUuc3RhdGUua2V5KTtcbiAgICB9XG4gIH0pO1xufVxuXG5mdW5jdGlvbiBoYW5kbGVTY3JvbGwgKFxuICByb3V0ZXIsXG4gIHRvLFxuICBmcm9tLFxuICBpc1BvcFxuKSB7XG4gIGlmICghcm91dGVyLmFwcCkge1xuICAgIHJldHVyblxuICB9XG5cbiAgdmFyIGJlaGF2aW9yID0gcm91dGVyLm9wdGlvbnMuc2Nyb2xsQmVoYXZpb3I7XG4gIGlmICghYmVoYXZpb3IpIHtcbiAgICByZXR1cm5cbiAgfVxuXG4gIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XG4gICAgYXNzZXJ0KHR5cGVvZiBiZWhhdmlvciA9PT0gJ2Z1bmN0aW9uJywgXCJzY3JvbGxCZWhhdmlvciBtdXN0IGJlIGEgZnVuY3Rpb25cIik7XG4gIH1cblxuICAvLyB3YWl0IHVudGlsIHJlLXJlbmRlciBmaW5pc2hlcyBiZWZvcmUgc2Nyb2xsaW5nXG4gIHJvdXRlci5hcHAuJG5leHRUaWNrKGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgcG9zaXRpb24gPSBnZXRTY3JvbGxQb3NpdGlvbigpO1xuICAgIHZhciBzaG91bGRTY3JvbGwgPSBiZWhhdmlvci5jYWxsKFxuICAgICAgcm91dGVyLFxuICAgICAgdG8sXG4gICAgICBmcm9tLFxuICAgICAgaXNQb3AgPyBwb3NpdGlvbiA6IG51bGxcbiAgICApO1xuXG4gICAgaWYgKCFzaG91bGRTY3JvbGwpIHtcbiAgICAgIHJldHVyblxuICAgIH1cblxuICAgIGlmICh0eXBlb2Ygc2hvdWxkU2Nyb2xsLnRoZW4gPT09ICdmdW5jdGlvbicpIHtcbiAgICAgIHNob3VsZFNjcm9sbFxuICAgICAgICAudGhlbihmdW5jdGlvbiAoc2hvdWxkU2Nyb2xsKSB7XG4gICAgICAgICAgc2Nyb2xsVG9Qb3NpdGlvbigoc2hvdWxkU2Nyb2xsKSwgcG9zaXRpb24pO1xuICAgICAgICB9KVxuICAgICAgICAuY2F0Y2goZnVuY3Rpb24gKGVycikge1xuICAgICAgICAgIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XG4gICAgICAgICAgICBhc3NlcnQoZmFsc2UsIGVyci50b1N0cmluZygpKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICBzY3JvbGxUb1Bvc2l0aW9uKHNob3VsZFNjcm9sbCwgcG9zaXRpb24pO1xuICAgIH1cbiAgfSk7XG59XG5cbmZ1bmN0aW9uIHNhdmVTY3JvbGxQb3NpdGlvbiAoKSB7XG4gIHZhciBrZXkgPSBnZXRTdGF0ZUtleSgpO1xuICBpZiAoa2V5KSB7XG4gICAgcG9zaXRpb25TdG9yZVtrZXldID0ge1xuICAgICAgeDogd2luZG93LnBhZ2VYT2Zmc2V0LFxuICAgICAgeTogd2luZG93LnBhZ2VZT2Zmc2V0XG4gICAgfTtcbiAgfVxufVxuXG5mdW5jdGlvbiBnZXRTY3JvbGxQb3NpdGlvbiAoKSB7XG4gIHZhciBrZXkgPSBnZXRTdGF0ZUtleSgpO1xuICBpZiAoa2V5KSB7XG4gICAgcmV0dXJuIHBvc2l0aW9uU3RvcmVba2V5XVxuICB9XG59XG5cbmZ1bmN0aW9uIGdldEVsZW1lbnRQb3NpdGlvbiAoZWwsIG9mZnNldCkge1xuICB2YXIgZG9jRWwgPSBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQ7XG4gIHZhciBkb2NSZWN0ID0gZG9jRWwuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XG4gIHZhciBlbFJlY3QgPSBlbC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKTtcbiAgcmV0dXJuIHtcbiAgICB4OiBlbFJlY3QubGVmdCAtIGRvY1JlY3QubGVmdCAtIG9mZnNldC54LFxuICAgIHk6IGVsUmVjdC50b3AgLSBkb2NSZWN0LnRvcCAtIG9mZnNldC55XG4gIH1cbn1cblxuZnVuY3Rpb24gaXNWYWxpZFBvc2l0aW9uIChvYmopIHtcbiAgcmV0dXJuIGlzTnVtYmVyKG9iai54KSB8fCBpc051bWJlcihvYmoueSlcbn1cblxuZnVuY3Rpb24gbm9ybWFsaXplUG9zaXRpb24gKG9iaikge1xuICByZXR1cm4ge1xuICAgIHg6IGlzTnVtYmVyKG9iai54KSA/IG9iai54IDogd2luZG93LnBhZ2VYT2Zmc2V0LFxuICAgIHk6IGlzTnVtYmVyKG9iai55KSA/IG9iai55IDogd2luZG93LnBhZ2VZT2Zmc2V0XG4gIH1cbn1cblxuZnVuY3Rpb24gbm9ybWFsaXplT2Zmc2V0IChvYmopIHtcbiAgcmV0dXJuIHtcbiAgICB4OiBpc051bWJlcihvYmoueCkgPyBvYmoueCA6IDAsXG4gICAgeTogaXNOdW1iZXIob2JqLnkpID8gb2JqLnkgOiAwXG4gIH1cbn1cblxuZnVuY3Rpb24gaXNOdW1iZXIgKHYpIHtcbiAgcmV0dXJuIHR5cGVvZiB2ID09PSAnbnVtYmVyJ1xufVxuXG52YXIgaGFzaFN0YXJ0c1dpdGhOdW1iZXJSRSA9IC9eI1xcZC87XG5cbmZ1bmN0aW9uIHNjcm9sbFRvUG9zaXRpb24gKHNob3VsZFNjcm9sbCwgcG9zaXRpb24pIHtcbiAgdmFyIGlzT2JqZWN0ID0gdHlwZW9mIHNob3VsZFNjcm9sbCA9PT0gJ29iamVjdCc7XG4gIGlmIChpc09iamVjdCAmJiB0eXBlb2Ygc2hvdWxkU2Nyb2xsLnNlbGVjdG9yID09PSAnc3RyaW5nJykge1xuICAgIC8vIGdldEVsZW1lbnRCeUlkIHdvdWxkIHN0aWxsIGZhaWwgaWYgdGhlIHNlbGVjdG9yIGNvbnRhaW5zIGEgbW9yZSBjb21wbGljYXRlZCBxdWVyeSBsaWtlICNtYWluW2RhdGEtYXR0cl1cbiAgICAvLyBidXQgYXQgdGhlIHNhbWUgdGltZSwgaXQgZG9lc24ndCBtYWtlIG11Y2ggc2Vuc2UgdG8gc2VsZWN0IGFuIGVsZW1lbnQgd2l0aCBhbiBpZCBhbmQgYW4gZXh0cmEgc2VsZWN0b3JcbiAgICB2YXIgZWwgPSBoYXNoU3RhcnRzV2l0aE51bWJlclJFLnRlc3Qoc2hvdWxkU2Nyb2xsLnNlbGVjdG9yKSAvLyAkZmxvdy1kaXNhYmxlLWxpbmVcbiAgICAgID8gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoc2hvdWxkU2Nyb2xsLnNlbGVjdG9yLnNsaWNlKDEpKSAvLyAkZmxvdy1kaXNhYmxlLWxpbmVcbiAgICAgIDogZG9jdW1lbnQucXVlcnlTZWxlY3RvcihzaG91bGRTY3JvbGwuc2VsZWN0b3IpO1xuXG4gICAgaWYgKGVsKSB7XG4gICAgICB2YXIgb2Zmc2V0ID1cbiAgICAgICAgc2hvdWxkU2Nyb2xsLm9mZnNldCAmJiB0eXBlb2Ygc2hvdWxkU2Nyb2xsLm9mZnNldCA9PT0gJ29iamVjdCdcbiAgICAgICAgICA/IHNob3VsZFNjcm9sbC5vZmZzZXRcbiAgICAgICAgICA6IHt9O1xuICAgICAgb2Zmc2V0ID0gbm9ybWFsaXplT2Zmc2V0KG9mZnNldCk7XG4gICAgICBwb3NpdGlvbiA9IGdldEVsZW1lbnRQb3NpdGlvbihlbCwgb2Zmc2V0KTtcbiAgICB9IGVsc2UgaWYgKGlzVmFsaWRQb3NpdGlvbihzaG91bGRTY3JvbGwpKSB7XG4gICAgICBwb3NpdGlvbiA9IG5vcm1hbGl6ZVBvc2l0aW9uKHNob3VsZFNjcm9sbCk7XG4gICAgfVxuICB9IGVsc2UgaWYgKGlzT2JqZWN0ICYmIGlzVmFsaWRQb3NpdGlvbihzaG91bGRTY3JvbGwpKSB7XG4gICAgcG9zaXRpb24gPSBub3JtYWxpemVQb3NpdGlvbihzaG91bGRTY3JvbGwpO1xuICB9XG5cbiAgaWYgKHBvc2l0aW9uKSB7XG4gICAgd2luZG93LnNjcm9sbFRvKHBvc2l0aW9uLngsIHBvc2l0aW9uLnkpO1xuICB9XG59XG5cbi8qICAqL1xuXG52YXIgc3VwcG9ydHNQdXNoU3RhdGUgPVxuICBpbkJyb3dzZXIgJiZcbiAgKGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgdWEgPSB3aW5kb3cubmF2aWdhdG9yLnVzZXJBZ2VudDtcblxuICAgIGlmIChcbiAgICAgICh1YS5pbmRleE9mKCdBbmRyb2lkIDIuJykgIT09IC0xIHx8IHVhLmluZGV4T2YoJ0FuZHJvaWQgNC4wJykgIT09IC0xKSAmJlxuICAgICAgdWEuaW5kZXhPZignTW9iaWxlIFNhZmFyaScpICE9PSAtMSAmJlxuICAgICAgdWEuaW5kZXhPZignQ2hyb21lJykgPT09IC0xICYmXG4gICAgICB1YS5pbmRleE9mKCdXaW5kb3dzIFBob25lJykgPT09IC0xXG4gICAgKSB7XG4gICAgICByZXR1cm4gZmFsc2VcbiAgICB9XG5cbiAgICByZXR1cm4gd2luZG93Lmhpc3RvcnkgJiYgJ3B1c2hTdGF0ZScgaW4gd2luZG93Lmhpc3RvcnlcbiAgfSkoKTtcblxuZnVuY3Rpb24gcHVzaFN0YXRlICh1cmwsIHJlcGxhY2UpIHtcbiAgc2F2ZVNjcm9sbFBvc2l0aW9uKCk7XG4gIC8vIHRyeS4uLmNhdGNoIHRoZSBwdXNoU3RhdGUgY2FsbCB0byBnZXQgYXJvdW5kIFNhZmFyaVxuICAvLyBET00gRXhjZXB0aW9uIDE4IHdoZXJlIGl0IGxpbWl0cyB0byAxMDAgcHVzaFN0YXRlIGNhbGxzXG4gIHZhciBoaXN0b3J5ID0gd2luZG93Lmhpc3Rvcnk7XG4gIHRyeSB7XG4gICAgaWYgKHJlcGxhY2UpIHtcbiAgICAgIGhpc3RvcnkucmVwbGFjZVN0YXRlKHsga2V5OiBnZXRTdGF0ZUtleSgpIH0sICcnLCB1cmwpO1xuICAgIH0gZWxzZSB7XG4gICAgICBoaXN0b3J5LnB1c2hTdGF0ZSh7IGtleTogc2V0U3RhdGVLZXkoZ2VuU3RhdGVLZXkoKSkgfSwgJycsIHVybCk7XG4gICAgfVxuICB9IGNhdGNoIChlKSB7XG4gICAgd2luZG93LmxvY2F0aW9uW3JlcGxhY2UgPyAncmVwbGFjZScgOiAnYXNzaWduJ10odXJsKTtcbiAgfVxufVxuXG5mdW5jdGlvbiByZXBsYWNlU3RhdGUgKHVybCkge1xuICBwdXNoU3RhdGUodXJsLCB0cnVlKTtcbn1cblxuLyogICovXG5cbmZ1bmN0aW9uIHJ1blF1ZXVlIChxdWV1ZSwgZm4sIGNiKSB7XG4gIHZhciBzdGVwID0gZnVuY3Rpb24gKGluZGV4KSB7XG4gICAgaWYgKGluZGV4ID49IHF1ZXVlLmxlbmd0aCkge1xuICAgICAgY2IoKTtcbiAgICB9IGVsc2Uge1xuICAgICAgaWYgKHF1ZXVlW2luZGV4XSkge1xuICAgICAgICBmbihxdWV1ZVtpbmRleF0sIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICBzdGVwKGluZGV4ICsgMSk7XG4gICAgICAgIH0pO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgc3RlcChpbmRleCArIDEpO1xuICAgICAgfVxuICAgIH1cbiAgfTtcbiAgc3RlcCgwKTtcbn1cblxuLyogICovXG5cbmZ1bmN0aW9uIHJlc29sdmVBc3luY0NvbXBvbmVudHMgKG1hdGNoZWQpIHtcbiAgcmV0dXJuIGZ1bmN0aW9uICh0bywgZnJvbSwgbmV4dCkge1xuICAgIHZhciBoYXNBc3luYyA9IGZhbHNlO1xuICAgIHZhciBwZW5kaW5nID0gMDtcbiAgICB2YXIgZXJyb3IgPSBudWxsO1xuXG4gICAgZmxhdE1hcENvbXBvbmVudHMobWF0Y2hlZCwgZnVuY3Rpb24gKGRlZiwgXywgbWF0Y2gsIGtleSkge1xuICAgICAgLy8gaWYgaXQncyBhIGZ1bmN0aW9uIGFuZCBkb2Vzbid0IGhhdmUgY2lkIGF0dGFjaGVkLFxuICAgICAgLy8gYXNzdW1lIGl0J3MgYW4gYXN5bmMgY29tcG9uZW50IHJlc29sdmUgZnVuY3Rpb24uXG4gICAgICAvLyB3ZSBhcmUgbm90IHVzaW5nIFZ1ZSdzIGRlZmF1bHQgYXN5bmMgcmVzb2x2aW5nIG1lY2hhbmlzbSBiZWNhdXNlXG4gICAgICAvLyB3ZSB3YW50IHRvIGhhbHQgdGhlIG5hdmlnYXRpb24gdW50aWwgdGhlIGluY29taW5nIGNvbXBvbmVudCBoYXMgYmVlblxuICAgICAgLy8gcmVzb2x2ZWQuXG4gICAgICBpZiAodHlwZW9mIGRlZiA9PT0gJ2Z1bmN0aW9uJyAmJiBkZWYuY2lkID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgaGFzQXN5bmMgPSB0cnVlO1xuICAgICAgICBwZW5kaW5nKys7XG5cbiAgICAgICAgdmFyIHJlc29sdmUgPSBvbmNlKGZ1bmN0aW9uIChyZXNvbHZlZERlZikge1xuICAgICAgICAgIGlmIChpc0VTTW9kdWxlKHJlc29sdmVkRGVmKSkge1xuICAgICAgICAgICAgcmVzb2x2ZWREZWYgPSByZXNvbHZlZERlZi5kZWZhdWx0O1xuICAgICAgICAgIH1cbiAgICAgICAgICAvLyBzYXZlIHJlc29sdmVkIG9uIGFzeW5jIGZhY3RvcnkgaW4gY2FzZSBpdCdzIHVzZWQgZWxzZXdoZXJlXG4gICAgICAgICAgZGVmLnJlc29sdmVkID0gdHlwZW9mIHJlc29sdmVkRGVmID09PSAnZnVuY3Rpb24nXG4gICAgICAgICAgICA/IHJlc29sdmVkRGVmXG4gICAgICAgICAgICA6IF9WdWUuZXh0ZW5kKHJlc29sdmVkRGVmKTtcbiAgICAgICAgICBtYXRjaC5jb21wb25lbnRzW2tleV0gPSByZXNvbHZlZERlZjtcbiAgICAgICAgICBwZW5kaW5nLS07XG4gICAgICAgICAgaWYgKHBlbmRpbmcgPD0gMCkge1xuICAgICAgICAgICAgbmV4dCgpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG5cbiAgICAgICAgdmFyIHJlamVjdCA9IG9uY2UoZnVuY3Rpb24gKHJlYXNvbikge1xuICAgICAgICAgIHZhciBtc2cgPSBcIkZhaWxlZCB0byByZXNvbHZlIGFzeW5jIGNvbXBvbmVudCBcIiArIGtleSArIFwiOiBcIiArIHJlYXNvbjtcbiAgICAgICAgICBwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nICYmIHdhcm4oZmFsc2UsIG1zZyk7XG4gICAgICAgICAgaWYgKCFlcnJvcikge1xuICAgICAgICAgICAgZXJyb3IgPSBpc0Vycm9yKHJlYXNvbilcbiAgICAgICAgICAgICAgPyByZWFzb25cbiAgICAgICAgICAgICAgOiBuZXcgRXJyb3IobXNnKTtcbiAgICAgICAgICAgIG5leHQoZXJyb3IpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG5cbiAgICAgICAgdmFyIHJlcztcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICByZXMgPSBkZWYocmVzb2x2ZSwgcmVqZWN0KTtcbiAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICAgIHJlamVjdChlKTtcbiAgICAgICAgfVxuICAgICAgICBpZiAocmVzKSB7XG4gICAgICAgICAgaWYgKHR5cGVvZiByZXMudGhlbiA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgICAgcmVzLnRoZW4ocmVzb2x2ZSwgcmVqZWN0KTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgLy8gbmV3IHN5bnRheCBpbiBWdWUgMi4zXG4gICAgICAgICAgICB2YXIgY29tcCA9IHJlcy5jb21wb25lbnQ7XG4gICAgICAgICAgICBpZiAoY29tcCAmJiB0eXBlb2YgY29tcC50aGVuID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgICAgIGNvbXAudGhlbihyZXNvbHZlLCByZWplY3QpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0pO1xuXG4gICAgaWYgKCFoYXNBc3luYykgeyBuZXh0KCk7IH1cbiAgfVxufVxuXG5mdW5jdGlvbiBmbGF0TWFwQ29tcG9uZW50cyAoXG4gIG1hdGNoZWQsXG4gIGZuXG4pIHtcbiAgcmV0dXJuIGZsYXR0ZW4obWF0Y2hlZC5tYXAoZnVuY3Rpb24gKG0pIHtcbiAgICByZXR1cm4gT2JqZWN0LmtleXMobS5jb21wb25lbnRzKS5tYXAoZnVuY3Rpb24gKGtleSkgeyByZXR1cm4gZm4oXG4gICAgICBtLmNvbXBvbmVudHNba2V5XSxcbiAgICAgIG0uaW5zdGFuY2VzW2tleV0sXG4gICAgICBtLCBrZXlcbiAgICApOyB9KVxuICB9KSlcbn1cblxuZnVuY3Rpb24gZmxhdHRlbiAoYXJyKSB7XG4gIHJldHVybiBBcnJheS5wcm90b3R5cGUuY29uY2F0LmFwcGx5KFtdLCBhcnIpXG59XG5cbnZhciBoYXNTeW1ib2wgPVxuICB0eXBlb2YgU3ltYm9sID09PSAnZnVuY3Rpb24nICYmXG4gIHR5cGVvZiBTeW1ib2wudG9TdHJpbmdUYWcgPT09ICdzeW1ib2wnO1xuXG5mdW5jdGlvbiBpc0VTTW9kdWxlIChvYmopIHtcbiAgcmV0dXJuIG9iai5fX2VzTW9kdWxlIHx8IChoYXNTeW1ib2wgJiYgb2JqW1N5bWJvbC50b1N0cmluZ1RhZ10gPT09ICdNb2R1bGUnKVxufVxuXG4vLyBpbiBXZWJwYWNrIDIsIHJlcXVpcmUuZW5zdXJlIG5vdyBhbHNvIHJldHVybnMgYSBQcm9taXNlXG4vLyBzbyB0aGUgcmVzb2x2ZS9yZWplY3QgZnVuY3Rpb25zIG1heSBnZXQgY2FsbGVkIGFuIGV4dHJhIHRpbWVcbi8vIGlmIHRoZSB1c2VyIHVzZXMgYW4gYXJyb3cgZnVuY3Rpb24gc2hvcnRoYW5kIHRoYXQgaGFwcGVucyB0b1xuLy8gcmV0dXJuIHRoYXQgUHJvbWlzZS5cbmZ1bmN0aW9uIG9uY2UgKGZuKSB7XG4gIHZhciBjYWxsZWQgPSBmYWxzZTtcbiAgcmV0dXJuIGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgYXJncyA9IFtdLCBsZW4gPSBhcmd1bWVudHMubGVuZ3RoO1xuICAgIHdoaWxlICggbGVuLS0gKSBhcmdzWyBsZW4gXSA9IGFyZ3VtZW50c1sgbGVuIF07XG5cbiAgICBpZiAoY2FsbGVkKSB7IHJldHVybiB9XG4gICAgY2FsbGVkID0gdHJ1ZTtcbiAgICByZXR1cm4gZm4uYXBwbHkodGhpcywgYXJncylcbiAgfVxufVxuXG52YXIgTmF2aWdhdGlvbkR1cGxpY2F0ZWQgPSAvKkBfX1BVUkVfXyovKGZ1bmN0aW9uIChFcnJvcikge1xuICBmdW5jdGlvbiBOYXZpZ2F0aW9uRHVwbGljYXRlZCAobm9ybWFsaXplZExvY2F0aW9uKSB7XG4gICAgRXJyb3IuY2FsbCh0aGlzKTtcbiAgICB0aGlzLm5hbWUgPSB0aGlzLl9uYW1lID0gJ05hdmlnYXRpb25EdXBsaWNhdGVkJztcbiAgICAvLyBwYXNzaW5nIHRoZSBtZXNzYWdlIHRvIHN1cGVyKCkgZG9lc24ndCBzZWVtIHRvIHdvcmsgaW4gdGhlIHRyYW5zcGlsZWQgdmVyc2lvblxuICAgIHRoaXMubWVzc2FnZSA9IFwiTmF2aWdhdGluZyB0byBjdXJyZW50IGxvY2F0aW9uIChcXFwiXCIgKyAobm9ybWFsaXplZExvY2F0aW9uLmZ1bGxQYXRoKSArIFwiXFxcIikgaXMgbm90IGFsbG93ZWRcIjtcbiAgICAvLyBhZGQgYSBzdGFjayBwcm9wZXJ0eSBzbyBzZXJ2aWNlcyBsaWtlIFNlbnRyeSBjYW4gY29ycmVjdGx5IGRpc3BsYXkgaXRcbiAgICBPYmplY3QuZGVmaW5lUHJvcGVydHkodGhpcywgJ3N0YWNrJywge1xuICAgICAgdmFsdWU6IG5ldyBFcnJvcigpLnN0YWNrLFxuICAgICAgd3JpdGFibGU6IHRydWUsXG4gICAgICBjb25maWd1cmFibGU6IHRydWVcbiAgICB9KTtcbiAgICAvLyB3ZSBjb3VsZCBhbHNvIGhhdmUgdXNlZFxuICAgIC8vIEVycm9yLmNhcHR1cmVTdGFja1RyYWNlKHRoaXMsIHRoaXMuY29uc3RydWN0b3IpXG4gICAgLy8gYnV0IGl0IG9ubHkgZXhpc3RzIG9uIG5vZGUgYW5kIGNocm9tZVxuICB9XG5cbiAgaWYgKCBFcnJvciApIE5hdmlnYXRpb25EdXBsaWNhdGVkLl9fcHJvdG9fXyA9IEVycm9yO1xuICBOYXZpZ2F0aW9uRHVwbGljYXRlZC5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKCBFcnJvciAmJiBFcnJvci5wcm90b3R5cGUgKTtcbiAgTmF2aWdhdGlvbkR1cGxpY2F0ZWQucHJvdG90eXBlLmNvbnN0cnVjdG9yID0gTmF2aWdhdGlvbkR1cGxpY2F0ZWQ7XG5cbiAgcmV0dXJuIE5hdmlnYXRpb25EdXBsaWNhdGVkO1xufShFcnJvcikpO1xuXG4vLyBzdXBwb3J0IElFOVxuTmF2aWdhdGlvbkR1cGxpY2F0ZWQuX25hbWUgPSAnTmF2aWdhdGlvbkR1cGxpY2F0ZWQnO1xuXG4vKiAgKi9cblxudmFyIEhpc3RvcnkgPSBmdW5jdGlvbiBIaXN0b3J5IChyb3V0ZXIsIGJhc2UpIHtcbiAgdGhpcy5yb3V0ZXIgPSByb3V0ZXI7XG4gIHRoaXMuYmFzZSA9IG5vcm1hbGl6ZUJhc2UoYmFzZSk7XG4gIC8vIHN0YXJ0IHdpdGggYSByb3V0ZSBvYmplY3QgdGhhdCBzdGFuZHMgZm9yIFwibm93aGVyZVwiXG4gIHRoaXMuY3VycmVudCA9IFNUQVJUO1xuICB0aGlzLnBlbmRpbmcgPSBudWxsO1xuICB0aGlzLnJlYWR5ID0gZmFsc2U7XG4gIHRoaXMucmVhZHlDYnMgPSBbXTtcbiAgdGhpcy5yZWFkeUVycm9yQ2JzID0gW107XG4gIHRoaXMuZXJyb3JDYnMgPSBbXTtcbn07XG5cbkhpc3RvcnkucHJvdG90eXBlLmxpc3RlbiA9IGZ1bmN0aW9uIGxpc3RlbiAoY2IpIHtcbiAgdGhpcy5jYiA9IGNiO1xufTtcblxuSGlzdG9yeS5wcm90b3R5cGUub25SZWFkeSA9IGZ1bmN0aW9uIG9uUmVhZHkgKGNiLCBlcnJvckNiKSB7XG4gIGlmICh0aGlzLnJlYWR5KSB7XG4gICAgY2IoKTtcbiAgfSBlbHNlIHtcbiAgICB0aGlzLnJlYWR5Q2JzLnB1c2goY2IpO1xuICAgIGlmIChlcnJvckNiKSB7XG4gICAgICB0aGlzLnJlYWR5RXJyb3JDYnMucHVzaChlcnJvckNiKTtcbiAgICB9XG4gIH1cbn07XG5cbkhpc3RvcnkucHJvdG90eXBlLm9uRXJyb3IgPSBmdW5jdGlvbiBvbkVycm9yIChlcnJvckNiKSB7XG4gIHRoaXMuZXJyb3JDYnMucHVzaChlcnJvckNiKTtcbn07XG5cbkhpc3RvcnkucHJvdG90eXBlLnRyYW5zaXRpb25UbyA9IGZ1bmN0aW9uIHRyYW5zaXRpb25UbyAoXG4gIGxvY2F0aW9uLFxuICBvbkNvbXBsZXRlLFxuICBvbkFib3J0XG4pIHtcbiAgICB2YXIgdGhpcyQxID0gdGhpcztcblxuICB2YXIgcm91dGUgPSB0aGlzLnJvdXRlci5tYXRjaChsb2NhdGlvbiwgdGhpcy5jdXJyZW50KTtcbiAgdGhpcy5jb25maXJtVHJhbnNpdGlvbihcbiAgICByb3V0ZSxcbiAgICBmdW5jdGlvbiAoKSB7XG4gICAgICB0aGlzJDEudXBkYXRlUm91dGUocm91dGUpO1xuICAgICAgb25Db21wbGV0ZSAmJiBvbkNvbXBsZXRlKHJvdXRlKTtcbiAgICAgIHRoaXMkMS5lbnN1cmVVUkwoKTtcblxuICAgICAgLy8gZmlyZSByZWFkeSBjYnMgb25jZVxuICAgICAgaWYgKCF0aGlzJDEucmVhZHkpIHtcbiAgICAgICAgdGhpcyQxLnJlYWR5ID0gdHJ1ZTtcbiAgICAgICAgdGhpcyQxLnJlYWR5Q2JzLmZvckVhY2goZnVuY3Rpb24gKGNiKSB7XG4gICAgICAgICAgY2Iocm91dGUpO1xuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9LFxuICAgIGZ1bmN0aW9uIChlcnIpIHtcbiAgICAgIGlmIChvbkFib3J0KSB7XG4gICAgICAgIG9uQWJvcnQoZXJyKTtcbiAgICAgIH1cbiAgICAgIGlmIChlcnIgJiYgIXRoaXMkMS5yZWFkeSkge1xuICAgICAgICB0aGlzJDEucmVhZHkgPSB0cnVlO1xuICAgICAgICB0aGlzJDEucmVhZHlFcnJvckNicy5mb3JFYWNoKGZ1bmN0aW9uIChjYikge1xuICAgICAgICAgIGNiKGVycik7XG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgIH1cbiAgKTtcbn07XG5cbkhpc3RvcnkucHJvdG90eXBlLmNvbmZpcm1UcmFuc2l0aW9uID0gZnVuY3Rpb24gY29uZmlybVRyYW5zaXRpb24gKHJvdXRlLCBvbkNvbXBsZXRlLCBvbkFib3J0KSB7XG4gICAgdmFyIHRoaXMkMSA9IHRoaXM7XG5cbiAgdmFyIGN1cnJlbnQgPSB0aGlzLmN1cnJlbnQ7XG4gIHZhciBhYm9ydCA9IGZ1bmN0aW9uIChlcnIpIHtcbiAgICAvLyBhZnRlciBtZXJnaW5nIGh0dHBzOi8vZ2l0aHViLmNvbS92dWVqcy92dWUtcm91dGVyL3B1bGwvMjc3MSB3ZVxuICAgIC8vIFdoZW4gdGhlIHVzZXIgbmF2aWdhdGVzIHRocm91Z2ggaGlzdG9yeSB0aHJvdWdoIGJhY2svZm9yd2FyZCBidXR0b25zXG4gICAgLy8gd2UgZG8gbm90IHdhbnQgdG8gdGhyb3cgdGhlIGVycm9yLiBXZSBvbmx5IHRocm93IGl0IGlmIGRpcmVjdGx5IGNhbGxpbmdcbiAgICAvLyBwdXNoL3JlcGxhY2UuIFRoYXQncyB3aHkgaXQncyBub3QgaW5jbHVkZWQgaW4gaXNFcnJvclxuICAgIGlmICghaXNFeHRlbmRlZEVycm9yKE5hdmlnYXRpb25EdXBsaWNhdGVkLCBlcnIpICYmIGlzRXJyb3IoZXJyKSkge1xuICAgICAgaWYgKHRoaXMkMS5lcnJvckNicy5sZW5ndGgpIHtcbiAgICAgICAgdGhpcyQxLmVycm9yQ2JzLmZvckVhY2goZnVuY3Rpb24gKGNiKSB7XG4gICAgICAgICAgY2IoZXJyKTtcbiAgICAgICAgfSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB3YXJuKGZhbHNlLCAndW5jYXVnaHQgZXJyb3IgZHVyaW5nIHJvdXRlIG5hdmlnYXRpb246Jyk7XG4gICAgICAgIGNvbnNvbGUuZXJyb3IoZXJyKTtcbiAgICAgIH1cbiAgICB9XG4gICAgb25BYm9ydCAmJiBvbkFib3J0KGVycik7XG4gIH07XG4gIGlmIChcbiAgICBpc1NhbWVSb3V0ZShyb3V0ZSwgY3VycmVudCkgJiZcbiAgICAvLyBpbiB0aGUgY2FzZSB0aGUgcm91dGUgbWFwIGhhcyBiZWVuIGR5bmFtaWNhbGx5IGFwcGVuZGVkIHRvXG4gICAgcm91dGUubWF0Y2hlZC5sZW5ndGggPT09IGN1cnJlbnQubWF0Y2hlZC5sZW5ndGhcbiAgKSB7XG4gICAgdGhpcy5lbnN1cmVVUkwoKTtcbiAgICByZXR1cm4gYWJvcnQobmV3IE5hdmlnYXRpb25EdXBsaWNhdGVkKHJvdXRlKSlcbiAgfVxuXG4gIHZhciByZWYgPSByZXNvbHZlUXVldWUoXG4gICAgdGhpcy5jdXJyZW50Lm1hdGNoZWQsXG4gICAgcm91dGUubWF0Y2hlZFxuICApO1xuICAgIHZhciB1cGRhdGVkID0gcmVmLnVwZGF0ZWQ7XG4gICAgdmFyIGRlYWN0aXZhdGVkID0gcmVmLmRlYWN0aXZhdGVkO1xuICAgIHZhciBhY3RpdmF0ZWQgPSByZWYuYWN0aXZhdGVkO1xuXG4gIHZhciBxdWV1ZSA9IFtdLmNvbmNhdChcbiAgICAvLyBpbi1jb21wb25lbnQgbGVhdmUgZ3VhcmRzXG4gICAgZXh0cmFjdExlYXZlR3VhcmRzKGRlYWN0aXZhdGVkKSxcbiAgICAvLyBnbG9iYWwgYmVmb3JlIGhvb2tzXG4gICAgdGhpcy5yb3V0ZXIuYmVmb3JlSG9va3MsXG4gICAgLy8gaW4tY29tcG9uZW50IHVwZGF0ZSBob29rc1xuICAgIGV4dHJhY3RVcGRhdGVIb29rcyh1cGRhdGVkKSxcbiAgICAvLyBpbi1jb25maWcgZW50ZXIgZ3VhcmRzXG4gICAgYWN0aXZhdGVkLm1hcChmdW5jdGlvbiAobSkgeyByZXR1cm4gbS5iZWZvcmVFbnRlcjsgfSksXG4gICAgLy8gYXN5bmMgY29tcG9uZW50c1xuICAgIHJlc29sdmVBc3luY0NvbXBvbmVudHMoYWN0aXZhdGVkKVxuICApO1xuXG4gIHRoaXMucGVuZGluZyA9IHJvdXRlO1xuICB2YXIgaXRlcmF0b3IgPSBmdW5jdGlvbiAoaG9vaywgbmV4dCkge1xuICAgIGlmICh0aGlzJDEucGVuZGluZyAhPT0gcm91dGUpIHtcbiAgICAgIHJldHVybiBhYm9ydCgpXG4gICAgfVxuICAgIHRyeSB7XG4gICAgICBob29rKHJvdXRlLCBjdXJyZW50LCBmdW5jdGlvbiAodG8pIHtcbiAgICAgICAgaWYgKHRvID09PSBmYWxzZSB8fCBpc0Vycm9yKHRvKSkge1xuICAgICAgICAgIC8vIG5leHQoZmFsc2UpIC0+IGFib3J0IG5hdmlnYXRpb24sIGVuc3VyZSBjdXJyZW50IFVSTFxuICAgICAgICAgIHRoaXMkMS5lbnN1cmVVUkwodHJ1ZSk7XG4gICAgICAgICAgYWJvcnQodG8pO1xuICAgICAgICB9IGVsc2UgaWYgKFxuICAgICAgICAgIHR5cGVvZiB0byA9PT0gJ3N0cmluZycgfHxcbiAgICAgICAgICAodHlwZW9mIHRvID09PSAnb2JqZWN0JyAmJlxuICAgICAgICAgICAgKHR5cGVvZiB0by5wYXRoID09PSAnc3RyaW5nJyB8fCB0eXBlb2YgdG8ubmFtZSA9PT0gJ3N0cmluZycpKVxuICAgICAgICApIHtcbiAgICAgICAgICAvLyBuZXh0KCcvJykgb3IgbmV4dCh7IHBhdGg6ICcvJyB9KSAtPiByZWRpcmVjdFxuICAgICAgICAgIGFib3J0KCk7XG4gICAgICAgICAgaWYgKHR5cGVvZiB0byA9PT0gJ29iamVjdCcgJiYgdG8ucmVwbGFjZSkge1xuICAgICAgICAgICAgdGhpcyQxLnJlcGxhY2UodG8pO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aGlzJDEucHVzaCh0byk7XG4gICAgICAgICAgfVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIC8vIGNvbmZpcm0gdHJhbnNpdGlvbiBhbmQgcGFzcyBvbiB0aGUgdmFsdWVcbiAgICAgICAgICBuZXh0KHRvKTtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgfSBjYXRjaCAoZSkge1xuICAgICAgYWJvcnQoZSk7XG4gICAgfVxuICB9O1xuXG4gIHJ1blF1ZXVlKHF1ZXVlLCBpdGVyYXRvciwgZnVuY3Rpb24gKCkge1xuICAgIHZhciBwb3N0RW50ZXJDYnMgPSBbXTtcbiAgICB2YXIgaXNWYWxpZCA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIHRoaXMkMS5jdXJyZW50ID09PSByb3V0ZTsgfTtcbiAgICAvLyB3YWl0IHVudGlsIGFzeW5jIGNvbXBvbmVudHMgYXJlIHJlc29sdmVkIGJlZm9yZVxuICAgIC8vIGV4dHJhY3RpbmcgaW4tY29tcG9uZW50IGVudGVyIGd1YXJkc1xuICAgIHZhciBlbnRlckd1YXJkcyA9IGV4dHJhY3RFbnRlckd1YXJkcyhhY3RpdmF0ZWQsIHBvc3RFbnRlckNicywgaXNWYWxpZCk7XG4gICAgdmFyIHF1ZXVlID0gZW50ZXJHdWFyZHMuY29uY2F0KHRoaXMkMS5yb3V0ZXIucmVzb2x2ZUhvb2tzKTtcbiAgICBydW5RdWV1ZShxdWV1ZSwgaXRlcmF0b3IsIGZ1bmN0aW9uICgpIHtcbiAgICAgIGlmICh0aGlzJDEucGVuZGluZyAhPT0gcm91dGUpIHtcbiAgICAgICAgcmV0dXJuIGFib3J0KClcbiAgICAgIH1cbiAgICAgIHRoaXMkMS5wZW5kaW5nID0gbnVsbDtcbiAgICAgIG9uQ29tcGxldGUocm91dGUpO1xuICAgICAgaWYgKHRoaXMkMS5yb3V0ZXIuYXBwKSB7XG4gICAgICAgIHRoaXMkMS5yb3V0ZXIuYXBwLiRuZXh0VGljayhmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgcG9zdEVudGVyQ2JzLmZvckVhY2goZnVuY3Rpb24gKGNiKSB7XG4gICAgICAgICAgICBjYigpO1xuICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfSk7XG59O1xuXG5IaXN0b3J5LnByb3RvdHlwZS51cGRhdGVSb3V0ZSA9IGZ1bmN0aW9uIHVwZGF0ZVJvdXRlIChyb3V0ZSkge1xuICB2YXIgcHJldiA9IHRoaXMuY3VycmVudDtcbiAgdGhpcy5jdXJyZW50ID0gcm91dGU7XG4gIHRoaXMuY2IgJiYgdGhpcy5jYihyb3V0ZSk7XG4gIHRoaXMucm91dGVyLmFmdGVySG9va3MuZm9yRWFjaChmdW5jdGlvbiAoaG9vaykge1xuICAgIGhvb2sgJiYgaG9vayhyb3V0ZSwgcHJldik7XG4gIH0pO1xufTtcblxuZnVuY3Rpb24gbm9ybWFsaXplQmFzZSAoYmFzZSkge1xuICBpZiAoIWJhc2UpIHtcbiAgICBpZiAoaW5Ccm93c2VyKSB7XG4gICAgICAvLyByZXNwZWN0IDxiYXNlPiB0YWdcbiAgICAgIHZhciBiYXNlRWwgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCdiYXNlJyk7XG4gICAgICBiYXNlID0gKGJhc2VFbCAmJiBiYXNlRWwuZ2V0QXR0cmlidXRlKCdocmVmJykpIHx8ICcvJztcbiAgICAgIC8vIHN0cmlwIGZ1bGwgVVJMIG9yaWdpblxuICAgICAgYmFzZSA9IGJhc2UucmVwbGFjZSgvXmh0dHBzPzpcXC9cXC9bXlxcL10rLywgJycpO1xuICAgIH0gZWxzZSB7XG4gICAgICBiYXNlID0gJy8nO1xuICAgIH1cbiAgfVxuICAvLyBtYWtlIHN1cmUgdGhlcmUncyB0aGUgc3RhcnRpbmcgc2xhc2hcbiAgaWYgKGJhc2UuY2hhckF0KDApICE9PSAnLycpIHtcbiAgICBiYXNlID0gJy8nICsgYmFzZTtcbiAgfVxuICAvLyByZW1vdmUgdHJhaWxpbmcgc2xhc2hcbiAgcmV0dXJuIGJhc2UucmVwbGFjZSgvXFwvJC8sICcnKVxufVxuXG5mdW5jdGlvbiByZXNvbHZlUXVldWUgKFxuICBjdXJyZW50LFxuICBuZXh0XG4pIHtcbiAgdmFyIGk7XG4gIHZhciBtYXggPSBNYXRoLm1heChjdXJyZW50Lmxlbmd0aCwgbmV4dC5sZW5ndGgpO1xuICBmb3IgKGkgPSAwOyBpIDwgbWF4OyBpKyspIHtcbiAgICBpZiAoY3VycmVudFtpXSAhPT0gbmV4dFtpXSkge1xuICAgICAgYnJlYWtcbiAgICB9XG4gIH1cbiAgcmV0dXJuIHtcbiAgICB1cGRhdGVkOiBuZXh0LnNsaWNlKDAsIGkpLFxuICAgIGFjdGl2YXRlZDogbmV4dC5zbGljZShpKSxcbiAgICBkZWFjdGl2YXRlZDogY3VycmVudC5zbGljZShpKVxuICB9XG59XG5cbmZ1bmN0aW9uIGV4dHJhY3RHdWFyZHMgKFxuICByZWNvcmRzLFxuICBuYW1lLFxuICBiaW5kLFxuICByZXZlcnNlXG4pIHtcbiAgdmFyIGd1YXJkcyA9IGZsYXRNYXBDb21wb25lbnRzKHJlY29yZHMsIGZ1bmN0aW9uIChkZWYsIGluc3RhbmNlLCBtYXRjaCwga2V5KSB7XG4gICAgdmFyIGd1YXJkID0gZXh0cmFjdEd1YXJkKGRlZiwgbmFtZSk7XG4gICAgaWYgKGd1YXJkKSB7XG4gICAgICByZXR1cm4gQXJyYXkuaXNBcnJheShndWFyZClcbiAgICAgICAgPyBndWFyZC5tYXAoZnVuY3Rpb24gKGd1YXJkKSB7IHJldHVybiBiaW5kKGd1YXJkLCBpbnN0YW5jZSwgbWF0Y2gsIGtleSk7IH0pXG4gICAgICAgIDogYmluZChndWFyZCwgaW5zdGFuY2UsIG1hdGNoLCBrZXkpXG4gICAgfVxuICB9KTtcbiAgcmV0dXJuIGZsYXR0ZW4ocmV2ZXJzZSA/IGd1YXJkcy5yZXZlcnNlKCkgOiBndWFyZHMpXG59XG5cbmZ1bmN0aW9uIGV4dHJhY3RHdWFyZCAoXG4gIGRlZixcbiAga2V5XG4pIHtcbiAgaWYgKHR5cGVvZiBkZWYgIT09ICdmdW5jdGlvbicpIHtcbiAgICAvLyBleHRlbmQgbm93IHNvIHRoYXQgZ2xvYmFsIG1peGlucyBhcmUgYXBwbGllZC5cbiAgICBkZWYgPSBfVnVlLmV4dGVuZChkZWYpO1xuICB9XG4gIHJldHVybiBkZWYub3B0aW9uc1trZXldXG59XG5cbmZ1bmN0aW9uIGV4dHJhY3RMZWF2ZUd1YXJkcyAoZGVhY3RpdmF0ZWQpIHtcbiAgcmV0dXJuIGV4dHJhY3RHdWFyZHMoZGVhY3RpdmF0ZWQsICdiZWZvcmVSb3V0ZUxlYXZlJywgYmluZEd1YXJkLCB0cnVlKVxufVxuXG5mdW5jdGlvbiBleHRyYWN0VXBkYXRlSG9va3MgKHVwZGF0ZWQpIHtcbiAgcmV0dXJuIGV4dHJhY3RHdWFyZHModXBkYXRlZCwgJ2JlZm9yZVJvdXRlVXBkYXRlJywgYmluZEd1YXJkKVxufVxuXG5mdW5jdGlvbiBiaW5kR3VhcmQgKGd1YXJkLCBpbnN0YW5jZSkge1xuICBpZiAoaW5zdGFuY2UpIHtcbiAgICByZXR1cm4gZnVuY3Rpb24gYm91bmRSb3V0ZUd1YXJkICgpIHtcbiAgICAgIHJldHVybiBndWFyZC5hcHBseShpbnN0YW5jZSwgYXJndW1lbnRzKVxuICAgIH1cbiAgfVxufVxuXG5mdW5jdGlvbiBleHRyYWN0RW50ZXJHdWFyZHMgKFxuICBhY3RpdmF0ZWQsXG4gIGNicyxcbiAgaXNWYWxpZFxuKSB7XG4gIHJldHVybiBleHRyYWN0R3VhcmRzKFxuICAgIGFjdGl2YXRlZCxcbiAgICAnYmVmb3JlUm91dGVFbnRlcicsXG4gICAgZnVuY3Rpb24gKGd1YXJkLCBfLCBtYXRjaCwga2V5KSB7XG4gICAgICByZXR1cm4gYmluZEVudGVyR3VhcmQoZ3VhcmQsIG1hdGNoLCBrZXksIGNicywgaXNWYWxpZClcbiAgICB9XG4gIClcbn1cblxuZnVuY3Rpb24gYmluZEVudGVyR3VhcmQgKFxuICBndWFyZCxcbiAgbWF0Y2gsXG4gIGtleSxcbiAgY2JzLFxuICBpc1ZhbGlkXG4pIHtcbiAgcmV0dXJuIGZ1bmN0aW9uIHJvdXRlRW50ZXJHdWFyZCAodG8sIGZyb20sIG5leHQpIHtcbiAgICByZXR1cm4gZ3VhcmQodG8sIGZyb20sIGZ1bmN0aW9uIChjYikge1xuICAgICAgaWYgKHR5cGVvZiBjYiA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICBjYnMucHVzaChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgLy8gIzc1MFxuICAgICAgICAgIC8vIGlmIGEgcm91dGVyLXZpZXcgaXMgd3JhcHBlZCB3aXRoIGFuIG91dC1pbiB0cmFuc2l0aW9uLFxuICAgICAgICAgIC8vIHRoZSBpbnN0YW5jZSBtYXkgbm90IGhhdmUgYmVlbiByZWdpc3RlcmVkIGF0IHRoaXMgdGltZS5cbiAgICAgICAgICAvLyB3ZSB3aWxsIG5lZWQgdG8gcG9sbCBmb3IgcmVnaXN0cmF0aW9uIHVudGlsIGN1cnJlbnQgcm91dGVcbiAgICAgICAgICAvLyBpcyBubyBsb25nZXIgdmFsaWQuXG4gICAgICAgICAgcG9sbChjYiwgbWF0Y2guaW5zdGFuY2VzLCBrZXksIGlzVmFsaWQpO1xuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICAgIG5leHQoY2IpO1xuICAgIH0pXG4gIH1cbn1cblxuZnVuY3Rpb24gcG9sbCAoXG4gIGNiLCAvLyBzb21laG93IGZsb3cgY2Fubm90IGluZmVyIHRoaXMgaXMgYSBmdW5jdGlvblxuICBpbnN0YW5jZXMsXG4gIGtleSxcbiAgaXNWYWxpZFxuKSB7XG4gIGlmIChcbiAgICBpbnN0YW5jZXNba2V5XSAmJlxuICAgICFpbnN0YW5jZXNba2V5XS5faXNCZWluZ0Rlc3Ryb3llZCAvLyBkbyBub3QgcmV1c2UgYmVpbmcgZGVzdHJveWVkIGluc3RhbmNlXG4gICkge1xuICAgIGNiKGluc3RhbmNlc1trZXldKTtcbiAgfSBlbHNlIGlmIChpc1ZhbGlkKCkpIHtcbiAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcbiAgICAgIHBvbGwoY2IsIGluc3RhbmNlcywga2V5LCBpc1ZhbGlkKTtcbiAgICB9LCAxNik7XG4gIH1cbn1cblxuLyogICovXG5cbnZhciBIVE1MNUhpc3RvcnkgPSAvKkBfX1BVUkVfXyovKGZ1bmN0aW9uIChIaXN0b3J5KSB7XG4gIGZ1bmN0aW9uIEhUTUw1SGlzdG9yeSAocm91dGVyLCBiYXNlKSB7XG4gICAgdmFyIHRoaXMkMSA9IHRoaXM7XG5cbiAgICBIaXN0b3J5LmNhbGwodGhpcywgcm91dGVyLCBiYXNlKTtcblxuICAgIHZhciBleHBlY3RTY3JvbGwgPSByb3V0ZXIub3B0aW9ucy5zY3JvbGxCZWhhdmlvcjtcbiAgICB2YXIgc3VwcG9ydHNTY3JvbGwgPSBzdXBwb3J0c1B1c2hTdGF0ZSAmJiBleHBlY3RTY3JvbGw7XG5cbiAgICBpZiAoc3VwcG9ydHNTY3JvbGwpIHtcbiAgICAgIHNldHVwU2Nyb2xsKCk7XG4gICAgfVxuXG4gICAgdmFyIGluaXRMb2NhdGlvbiA9IGdldExvY2F0aW9uKHRoaXMuYmFzZSk7XG4gICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ3BvcHN0YXRlJywgZnVuY3Rpb24gKGUpIHtcbiAgICAgIHZhciBjdXJyZW50ID0gdGhpcyQxLmN1cnJlbnQ7XG5cbiAgICAgIC8vIEF2b2lkaW5nIGZpcnN0IGBwb3BzdGF0ZWAgZXZlbnQgZGlzcGF0Y2hlZCBpbiBzb21lIGJyb3dzZXJzIGJ1dCBmaXJzdFxuICAgICAgLy8gaGlzdG9yeSByb3V0ZSBub3QgdXBkYXRlZCBzaW5jZSBhc3luYyBndWFyZCBhdCB0aGUgc2FtZSB0aW1lLlxuICAgICAgdmFyIGxvY2F0aW9uID0gZ2V0TG9jYXRpb24odGhpcyQxLmJhc2UpO1xuICAgICAgaWYgKHRoaXMkMS5jdXJyZW50ID09PSBTVEFSVCAmJiBsb2NhdGlvbiA9PT0gaW5pdExvY2F0aW9uKSB7XG4gICAgICAgIHJldHVyblxuICAgICAgfVxuXG4gICAgICB0aGlzJDEudHJhbnNpdGlvblRvKGxvY2F0aW9uLCBmdW5jdGlvbiAocm91dGUpIHtcbiAgICAgICAgaWYgKHN1cHBvcnRzU2Nyb2xsKSB7XG4gICAgICAgICAgaGFuZGxlU2Nyb2xsKHJvdXRlciwgcm91dGUsIGN1cnJlbnQsIHRydWUpO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxuXG4gIGlmICggSGlzdG9yeSApIEhUTUw1SGlzdG9yeS5fX3Byb3RvX18gPSBIaXN0b3J5O1xuICBIVE1MNUhpc3RvcnkucHJvdG90eXBlID0gT2JqZWN0LmNyZWF0ZSggSGlzdG9yeSAmJiBIaXN0b3J5LnByb3RvdHlwZSApO1xuICBIVE1MNUhpc3RvcnkucHJvdG90eXBlLmNvbnN0cnVjdG9yID0gSFRNTDVIaXN0b3J5O1xuXG4gIEhUTUw1SGlzdG9yeS5wcm90b3R5cGUuZ28gPSBmdW5jdGlvbiBnbyAobikge1xuICAgIHdpbmRvdy5oaXN0b3J5LmdvKG4pO1xuICB9O1xuXG4gIEhUTUw1SGlzdG9yeS5wcm90b3R5cGUucHVzaCA9IGZ1bmN0aW9uIHB1c2ggKGxvY2F0aW9uLCBvbkNvbXBsZXRlLCBvbkFib3J0KSB7XG4gICAgdmFyIHRoaXMkMSA9IHRoaXM7XG5cbiAgICB2YXIgcmVmID0gdGhpcztcbiAgICB2YXIgZnJvbVJvdXRlID0gcmVmLmN1cnJlbnQ7XG4gICAgdGhpcy50cmFuc2l0aW9uVG8obG9jYXRpb24sIGZ1bmN0aW9uIChyb3V0ZSkge1xuICAgICAgcHVzaFN0YXRlKGNsZWFuUGF0aCh0aGlzJDEuYmFzZSArIHJvdXRlLmZ1bGxQYXRoKSk7XG4gICAgICBoYW5kbGVTY3JvbGwodGhpcyQxLnJvdXRlciwgcm91dGUsIGZyb21Sb3V0ZSwgZmFsc2UpO1xuICAgICAgb25Db21wbGV0ZSAmJiBvbkNvbXBsZXRlKHJvdXRlKTtcbiAgICB9LCBvbkFib3J0KTtcbiAgfTtcblxuICBIVE1MNUhpc3RvcnkucHJvdG90eXBlLnJlcGxhY2UgPSBmdW5jdGlvbiByZXBsYWNlIChsb2NhdGlvbiwgb25Db21wbGV0ZSwgb25BYm9ydCkge1xuICAgIHZhciB0aGlzJDEgPSB0aGlzO1xuXG4gICAgdmFyIHJlZiA9IHRoaXM7XG4gICAgdmFyIGZyb21Sb3V0ZSA9IHJlZi5jdXJyZW50O1xuICAgIHRoaXMudHJhbnNpdGlvblRvKGxvY2F0aW9uLCBmdW5jdGlvbiAocm91dGUpIHtcbiAgICAgIHJlcGxhY2VTdGF0ZShjbGVhblBhdGgodGhpcyQxLmJhc2UgKyByb3V0ZS5mdWxsUGF0aCkpO1xuICAgICAgaGFuZGxlU2Nyb2xsKHRoaXMkMS5yb3V0ZXIsIHJvdXRlLCBmcm9tUm91dGUsIGZhbHNlKTtcbiAgICAgIG9uQ29tcGxldGUgJiYgb25Db21wbGV0ZShyb3V0ZSk7XG4gICAgfSwgb25BYm9ydCk7XG4gIH07XG5cbiAgSFRNTDVIaXN0b3J5LnByb3RvdHlwZS5lbnN1cmVVUkwgPSBmdW5jdGlvbiBlbnN1cmVVUkwgKHB1c2gpIHtcbiAgICBpZiAoZ2V0TG9jYXRpb24odGhpcy5iYXNlKSAhPT0gdGhpcy5jdXJyZW50LmZ1bGxQYXRoKSB7XG4gICAgICB2YXIgY3VycmVudCA9IGNsZWFuUGF0aCh0aGlzLmJhc2UgKyB0aGlzLmN1cnJlbnQuZnVsbFBhdGgpO1xuICAgICAgcHVzaCA/IHB1c2hTdGF0ZShjdXJyZW50KSA6IHJlcGxhY2VTdGF0ZShjdXJyZW50KTtcbiAgICB9XG4gIH07XG5cbiAgSFRNTDVIaXN0b3J5LnByb3RvdHlwZS5nZXRDdXJyZW50TG9jYXRpb24gPSBmdW5jdGlvbiBnZXRDdXJyZW50TG9jYXRpb24gKCkge1xuICAgIHJldHVybiBnZXRMb2NhdGlvbih0aGlzLmJhc2UpXG4gIH07XG5cbiAgcmV0dXJuIEhUTUw1SGlzdG9yeTtcbn0oSGlzdG9yeSkpO1xuXG5mdW5jdGlvbiBnZXRMb2NhdGlvbiAoYmFzZSkge1xuICB2YXIgcGF0aCA9IGRlY29kZVVSSSh3aW5kb3cubG9jYXRpb24ucGF0aG5hbWUpO1xuICBpZiAoYmFzZSAmJiBwYXRoLmluZGV4T2YoYmFzZSkgPT09IDApIHtcbiAgICBwYXRoID0gcGF0aC5zbGljZShiYXNlLmxlbmd0aCk7XG4gIH1cbiAgcmV0dXJuIChwYXRoIHx8ICcvJykgKyB3aW5kb3cubG9jYXRpb24uc2VhcmNoICsgd2luZG93LmxvY2F0aW9uLmhhc2hcbn1cblxuLyogICovXG5cbnZhciBIYXNoSGlzdG9yeSA9IC8qQF9fUFVSRV9fKi8oZnVuY3Rpb24gKEhpc3RvcnkpIHtcbiAgZnVuY3Rpb24gSGFzaEhpc3RvcnkgKHJvdXRlciwgYmFzZSwgZmFsbGJhY2spIHtcbiAgICBIaXN0b3J5LmNhbGwodGhpcywgcm91dGVyLCBiYXNlKTtcbiAgICAvLyBjaGVjayBoaXN0b3J5IGZhbGxiYWNrIGRlZXBsaW5raW5nXG4gICAgaWYgKGZhbGxiYWNrICYmIGNoZWNrRmFsbGJhY2sodGhpcy5iYXNlKSkge1xuICAgICAgcmV0dXJuXG4gICAgfVxuICAgIGVuc3VyZVNsYXNoKCk7XG4gIH1cblxuICBpZiAoIEhpc3RvcnkgKSBIYXNoSGlzdG9yeS5fX3Byb3RvX18gPSBIaXN0b3J5O1xuICBIYXNoSGlzdG9yeS5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKCBIaXN0b3J5ICYmIEhpc3RvcnkucHJvdG90eXBlICk7XG4gIEhhc2hIaXN0b3J5LnByb3RvdHlwZS5jb25zdHJ1Y3RvciA9IEhhc2hIaXN0b3J5O1xuXG4gIC8vIHRoaXMgaXMgZGVsYXllZCB1bnRpbCB0aGUgYXBwIG1vdW50c1xuICAvLyB0byBhdm9pZCB0aGUgaGFzaGNoYW5nZSBsaXN0ZW5lciBiZWluZyBmaXJlZCB0b28gZWFybHlcbiAgSGFzaEhpc3RvcnkucHJvdG90eXBlLnNldHVwTGlzdGVuZXJzID0gZnVuY3Rpb24gc2V0dXBMaXN0ZW5lcnMgKCkge1xuICAgIHZhciB0aGlzJDEgPSB0aGlzO1xuXG4gICAgdmFyIHJvdXRlciA9IHRoaXMucm91dGVyO1xuICAgIHZhciBleHBlY3RTY3JvbGwgPSByb3V0ZXIub3B0aW9ucy5zY3JvbGxCZWhhdmlvcjtcbiAgICB2YXIgc3VwcG9ydHNTY3JvbGwgPSBzdXBwb3J0c1B1c2hTdGF0ZSAmJiBleHBlY3RTY3JvbGw7XG5cbiAgICBpZiAoc3VwcG9ydHNTY3JvbGwpIHtcbiAgICAgIHNldHVwU2Nyb2xsKCk7XG4gICAgfVxuXG4gICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoXG4gICAgICBzdXBwb3J0c1B1c2hTdGF0ZSA/ICdwb3BzdGF0ZScgOiAnaGFzaGNoYW5nZScsXG4gICAgICBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBjdXJyZW50ID0gdGhpcyQxLmN1cnJlbnQ7XG4gICAgICAgIGlmICghZW5zdXJlU2xhc2goKSkge1xuICAgICAgICAgIHJldHVyblxuICAgICAgICB9XG4gICAgICAgIHRoaXMkMS50cmFuc2l0aW9uVG8oZ2V0SGFzaCgpLCBmdW5jdGlvbiAocm91dGUpIHtcbiAgICAgICAgICBpZiAoc3VwcG9ydHNTY3JvbGwpIHtcbiAgICAgICAgICAgIGhhbmRsZVNjcm9sbCh0aGlzJDEucm91dGVyLCByb3V0ZSwgY3VycmVudCwgdHJ1ZSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmICghc3VwcG9ydHNQdXNoU3RhdGUpIHtcbiAgICAgICAgICAgIHJlcGxhY2VIYXNoKHJvdXRlLmZ1bGxQYXRoKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgICk7XG4gIH07XG5cbiAgSGFzaEhpc3RvcnkucHJvdG90eXBlLnB1c2ggPSBmdW5jdGlvbiBwdXNoIChsb2NhdGlvbiwgb25Db21wbGV0ZSwgb25BYm9ydCkge1xuICAgIHZhciB0aGlzJDEgPSB0aGlzO1xuXG4gICAgdmFyIHJlZiA9IHRoaXM7XG4gICAgdmFyIGZyb21Sb3V0ZSA9IHJlZi5jdXJyZW50O1xuICAgIHRoaXMudHJhbnNpdGlvblRvKFxuICAgICAgbG9jYXRpb24sXG4gICAgICBmdW5jdGlvbiAocm91dGUpIHtcbiAgICAgICAgcHVzaEhhc2gocm91dGUuZnVsbFBhdGgpO1xuICAgICAgICBoYW5kbGVTY3JvbGwodGhpcyQxLnJvdXRlciwgcm91dGUsIGZyb21Sb3V0ZSwgZmFsc2UpO1xuICAgICAgICBvbkNvbXBsZXRlICYmIG9uQ29tcGxldGUocm91dGUpO1xuICAgICAgfSxcbiAgICAgIG9uQWJvcnRcbiAgICApO1xuICB9O1xuXG4gIEhhc2hIaXN0b3J5LnByb3RvdHlwZS5yZXBsYWNlID0gZnVuY3Rpb24gcmVwbGFjZSAobG9jYXRpb24sIG9uQ29tcGxldGUsIG9uQWJvcnQpIHtcbiAgICB2YXIgdGhpcyQxID0gdGhpcztcblxuICAgIHZhciByZWYgPSB0aGlzO1xuICAgIHZhciBmcm9tUm91dGUgPSByZWYuY3VycmVudDtcbiAgICB0aGlzLnRyYW5zaXRpb25UbyhcbiAgICAgIGxvY2F0aW9uLFxuICAgICAgZnVuY3Rpb24gKHJvdXRlKSB7XG4gICAgICAgIHJlcGxhY2VIYXNoKHJvdXRlLmZ1bGxQYXRoKTtcbiAgICAgICAgaGFuZGxlU2Nyb2xsKHRoaXMkMS5yb3V0ZXIsIHJvdXRlLCBmcm9tUm91dGUsIGZhbHNlKTtcbiAgICAgICAgb25Db21wbGV0ZSAmJiBvbkNvbXBsZXRlKHJvdXRlKTtcbiAgICAgIH0sXG4gICAgICBvbkFib3J0XG4gICAgKTtcbiAgfTtcblxuICBIYXNoSGlzdG9yeS5wcm90b3R5cGUuZ28gPSBmdW5jdGlvbiBnbyAobikge1xuICAgIHdpbmRvdy5oaXN0b3J5LmdvKG4pO1xuICB9O1xuXG4gIEhhc2hIaXN0b3J5LnByb3RvdHlwZS5lbnN1cmVVUkwgPSBmdW5jdGlvbiBlbnN1cmVVUkwgKHB1c2gpIHtcbiAgICB2YXIgY3VycmVudCA9IHRoaXMuY3VycmVudC5mdWxsUGF0aDtcbiAgICBpZiAoZ2V0SGFzaCgpICE9PSBjdXJyZW50KSB7XG4gICAgICBwdXNoID8gcHVzaEhhc2goY3VycmVudCkgOiByZXBsYWNlSGFzaChjdXJyZW50KTtcbiAgICB9XG4gIH07XG5cbiAgSGFzaEhpc3RvcnkucHJvdG90eXBlLmdldEN1cnJlbnRMb2NhdGlvbiA9IGZ1bmN0aW9uIGdldEN1cnJlbnRMb2NhdGlvbiAoKSB7XG4gICAgcmV0dXJuIGdldEhhc2goKVxuICB9O1xuXG4gIHJldHVybiBIYXNoSGlzdG9yeTtcbn0oSGlzdG9yeSkpO1xuXG5mdW5jdGlvbiBjaGVja0ZhbGxiYWNrIChiYXNlKSB7XG4gIHZhciBsb2NhdGlvbiA9IGdldExvY2F0aW9uKGJhc2UpO1xuICBpZiAoIS9eXFwvIy8udGVzdChsb2NhdGlvbikpIHtcbiAgICB3aW5kb3cubG9jYXRpb24ucmVwbGFjZShjbGVhblBhdGgoYmFzZSArICcvIycgKyBsb2NhdGlvbikpO1xuICAgIHJldHVybiB0cnVlXG4gIH1cbn1cblxuZnVuY3Rpb24gZW5zdXJlU2xhc2ggKCkge1xuICB2YXIgcGF0aCA9IGdldEhhc2goKTtcbiAgaWYgKHBhdGguY2hhckF0KDApID09PSAnLycpIHtcbiAgICByZXR1cm4gdHJ1ZVxuICB9XG4gIHJlcGxhY2VIYXNoKCcvJyArIHBhdGgpO1xuICByZXR1cm4gZmFsc2Vcbn1cblxuZnVuY3Rpb24gZ2V0SGFzaCAoKSB7XG4gIC8vIFdlIGNhbid0IHVzZSB3aW5kb3cubG9jYXRpb24uaGFzaCBoZXJlIGJlY2F1c2UgaXQncyBub3RcbiAgLy8gY29uc2lzdGVudCBhY3Jvc3MgYnJvd3NlcnMgLSBGaXJlZm94IHdpbGwgcHJlLWRlY29kZSBpdCFcbiAgdmFyIGhyZWYgPSB3aW5kb3cubG9jYXRpb24uaHJlZjtcbiAgdmFyIGluZGV4ID0gaHJlZi5pbmRleE9mKCcjJyk7XG4gIC8vIGVtcHR5IHBhdGhcbiAgaWYgKGluZGV4IDwgMCkgeyByZXR1cm4gJycgfVxuXG4gIGhyZWYgPSBocmVmLnNsaWNlKGluZGV4ICsgMSk7XG4gIC8vIGRlY29kZSB0aGUgaGFzaCBidXQgbm90IHRoZSBzZWFyY2ggb3IgaGFzaFxuICAvLyBhcyBzZWFyY2gocXVlcnkpIGlzIGFscmVhZHkgZGVjb2RlZFxuICAvLyBodHRwczovL2dpdGh1Yi5jb20vdnVlanMvdnVlLXJvdXRlci9pc3N1ZXMvMjcwOFxuICB2YXIgc2VhcmNoSW5kZXggPSBocmVmLmluZGV4T2YoJz8nKTtcbiAgaWYgKHNlYXJjaEluZGV4IDwgMCkge1xuICAgIHZhciBoYXNoSW5kZXggPSBocmVmLmluZGV4T2YoJyMnKTtcbiAgICBpZiAoaGFzaEluZGV4ID4gLTEpIHtcbiAgICAgIGhyZWYgPSBkZWNvZGVVUkkoaHJlZi5zbGljZSgwLCBoYXNoSW5kZXgpKSArIGhyZWYuc2xpY2UoaGFzaEluZGV4KTtcbiAgICB9IGVsc2UgeyBocmVmID0gZGVjb2RlVVJJKGhyZWYpOyB9XG4gIH0gZWxzZSB7XG4gICAgaWYgKHNlYXJjaEluZGV4ID4gLTEpIHtcbiAgICAgIGhyZWYgPSBkZWNvZGVVUkkoaHJlZi5zbGljZSgwLCBzZWFyY2hJbmRleCkpICsgaHJlZi5zbGljZShzZWFyY2hJbmRleCk7XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIGhyZWZcbn1cblxuZnVuY3Rpb24gZ2V0VXJsIChwYXRoKSB7XG4gIHZhciBocmVmID0gd2luZG93LmxvY2F0aW9uLmhyZWY7XG4gIHZhciBpID0gaHJlZi5pbmRleE9mKCcjJyk7XG4gIHZhciBiYXNlID0gaSA+PSAwID8gaHJlZi5zbGljZSgwLCBpKSA6IGhyZWY7XG4gIHJldHVybiAoYmFzZSArIFwiI1wiICsgcGF0aClcbn1cblxuZnVuY3Rpb24gcHVzaEhhc2ggKHBhdGgpIHtcbiAgaWYgKHN1cHBvcnRzUHVzaFN0YXRlKSB7XG4gICAgcHVzaFN0YXRlKGdldFVybChwYXRoKSk7XG4gIH0gZWxzZSB7XG4gICAgd2luZG93LmxvY2F0aW9uLmhhc2ggPSBwYXRoO1xuICB9XG59XG5cbmZ1bmN0aW9uIHJlcGxhY2VIYXNoIChwYXRoKSB7XG4gIGlmIChzdXBwb3J0c1B1c2hTdGF0ZSkge1xuICAgIHJlcGxhY2VTdGF0ZShnZXRVcmwocGF0aCkpO1xuICB9IGVsc2Uge1xuICAgIHdpbmRvdy5sb2NhdGlvbi5yZXBsYWNlKGdldFVybChwYXRoKSk7XG4gIH1cbn1cblxuLyogICovXG5cbnZhciBBYnN0cmFjdEhpc3RvcnkgPSAvKkBfX1BVUkVfXyovKGZ1bmN0aW9uIChIaXN0b3J5KSB7XG4gIGZ1bmN0aW9uIEFic3RyYWN0SGlzdG9yeSAocm91dGVyLCBiYXNlKSB7XG4gICAgSGlzdG9yeS5jYWxsKHRoaXMsIHJvdXRlciwgYmFzZSk7XG4gICAgdGhpcy5zdGFjayA9IFtdO1xuICAgIHRoaXMuaW5kZXggPSAtMTtcbiAgfVxuXG4gIGlmICggSGlzdG9yeSApIEFic3RyYWN0SGlzdG9yeS5fX3Byb3RvX18gPSBIaXN0b3J5O1xuICBBYnN0cmFjdEhpc3RvcnkucHJvdG90eXBlID0gT2JqZWN0LmNyZWF0ZSggSGlzdG9yeSAmJiBIaXN0b3J5LnByb3RvdHlwZSApO1xuICBBYnN0cmFjdEhpc3RvcnkucHJvdG90eXBlLmNvbnN0cnVjdG9yID0gQWJzdHJhY3RIaXN0b3J5O1xuXG4gIEFic3RyYWN0SGlzdG9yeS5wcm90b3R5cGUucHVzaCA9IGZ1bmN0aW9uIHB1c2ggKGxvY2F0aW9uLCBvbkNvbXBsZXRlLCBvbkFib3J0KSB7XG4gICAgdmFyIHRoaXMkMSA9IHRoaXM7XG5cbiAgICB0aGlzLnRyYW5zaXRpb25UbyhcbiAgICAgIGxvY2F0aW9uLFxuICAgICAgZnVuY3Rpb24gKHJvdXRlKSB7XG4gICAgICAgIHRoaXMkMS5zdGFjayA9IHRoaXMkMS5zdGFjay5zbGljZSgwLCB0aGlzJDEuaW5kZXggKyAxKS5jb25jYXQocm91dGUpO1xuICAgICAgICB0aGlzJDEuaW5kZXgrKztcbiAgICAgICAgb25Db21wbGV0ZSAmJiBvbkNvbXBsZXRlKHJvdXRlKTtcbiAgICAgIH0sXG4gICAgICBvbkFib3J0XG4gICAgKTtcbiAgfTtcblxuICBBYnN0cmFjdEhpc3RvcnkucHJvdG90eXBlLnJlcGxhY2UgPSBmdW5jdGlvbiByZXBsYWNlIChsb2NhdGlvbiwgb25Db21wbGV0ZSwgb25BYm9ydCkge1xuICAgIHZhciB0aGlzJDEgPSB0aGlzO1xuXG4gICAgdGhpcy50cmFuc2l0aW9uVG8oXG4gICAgICBsb2NhdGlvbixcbiAgICAgIGZ1bmN0aW9uIChyb3V0ZSkge1xuICAgICAgICB0aGlzJDEuc3RhY2sgPSB0aGlzJDEuc3RhY2suc2xpY2UoMCwgdGhpcyQxLmluZGV4KS5jb25jYXQocm91dGUpO1xuICAgICAgICBvbkNvbXBsZXRlICYmIG9uQ29tcGxldGUocm91dGUpO1xuICAgICAgfSxcbiAgICAgIG9uQWJvcnRcbiAgICApO1xuICB9O1xuXG4gIEFic3RyYWN0SGlzdG9yeS5wcm90b3R5cGUuZ28gPSBmdW5jdGlvbiBnbyAobikge1xuICAgIHZhciB0aGlzJDEgPSB0aGlzO1xuXG4gICAgdmFyIHRhcmdldEluZGV4ID0gdGhpcy5pbmRleCArIG47XG4gICAgaWYgKHRhcmdldEluZGV4IDwgMCB8fCB0YXJnZXRJbmRleCA+PSB0aGlzLnN0YWNrLmxlbmd0aCkge1xuICAgICAgcmV0dXJuXG4gICAgfVxuICAgIHZhciByb3V0ZSA9IHRoaXMuc3RhY2tbdGFyZ2V0SW5kZXhdO1xuICAgIHRoaXMuY29uZmlybVRyYW5zaXRpb24oXG4gICAgICByb3V0ZSxcbiAgICAgIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdGhpcyQxLmluZGV4ID0gdGFyZ2V0SW5kZXg7XG4gICAgICAgIHRoaXMkMS51cGRhdGVSb3V0ZShyb3V0ZSk7XG4gICAgICB9LFxuICAgICAgZnVuY3Rpb24gKGVycikge1xuICAgICAgICBpZiAoaXNFeHRlbmRlZEVycm9yKE5hdmlnYXRpb25EdXBsaWNhdGVkLCBlcnIpKSB7XG4gICAgICAgICAgdGhpcyQxLmluZGV4ID0gdGFyZ2V0SW5kZXg7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICApO1xuICB9O1xuXG4gIEFic3RyYWN0SGlzdG9yeS5wcm90b3R5cGUuZ2V0Q3VycmVudExvY2F0aW9uID0gZnVuY3Rpb24gZ2V0Q3VycmVudExvY2F0aW9uICgpIHtcbiAgICB2YXIgY3VycmVudCA9IHRoaXMuc3RhY2tbdGhpcy5zdGFjay5sZW5ndGggLSAxXTtcbiAgICByZXR1cm4gY3VycmVudCA/IGN1cnJlbnQuZnVsbFBhdGggOiAnLydcbiAgfTtcblxuICBBYnN0cmFjdEhpc3RvcnkucHJvdG90eXBlLmVuc3VyZVVSTCA9IGZ1bmN0aW9uIGVuc3VyZVVSTCAoKSB7XG4gICAgLy8gbm9vcFxuICB9O1xuXG4gIHJldHVybiBBYnN0cmFjdEhpc3Rvcnk7XG59KEhpc3RvcnkpKTtcblxuLyogICovXG5cblxuXG52YXIgVnVlUm91dGVyID0gZnVuY3Rpb24gVnVlUm91dGVyIChvcHRpb25zKSB7XG4gIGlmICggb3B0aW9ucyA9PT0gdm9pZCAwICkgb3B0aW9ucyA9IHt9O1xuXG4gIHRoaXMuYXBwID0gbnVsbDtcbiAgdGhpcy5hcHBzID0gW107XG4gIHRoaXMub3B0aW9ucyA9IG9wdGlvbnM7XG4gIHRoaXMuYmVmb3JlSG9va3MgPSBbXTtcbiAgdGhpcy5yZXNvbHZlSG9va3MgPSBbXTtcbiAgdGhpcy5hZnRlckhvb2tzID0gW107XG4gIHRoaXMubWF0Y2hlciA9IGNyZWF0ZU1hdGNoZXIob3B0aW9ucy5yb3V0ZXMgfHwgW10sIHRoaXMpO1xuXG4gIHZhciBtb2RlID0gb3B0aW9ucy5tb2RlIHx8ICdoYXNoJztcbiAgdGhpcy5mYWxsYmFjayA9IG1vZGUgPT09ICdoaXN0b3J5JyAmJiAhc3VwcG9ydHNQdXNoU3RhdGUgJiYgb3B0aW9ucy5mYWxsYmFjayAhPT0gZmFsc2U7XG4gIGlmICh0aGlzLmZhbGxiYWNrKSB7XG4gICAgbW9kZSA9ICdoYXNoJztcbiAgfVxuICBpZiAoIWluQnJvd3Nlcikge1xuICAgIG1vZGUgPSAnYWJzdHJhY3QnO1xuICB9XG4gIHRoaXMubW9kZSA9IG1vZGU7XG5cbiAgc3dpdGNoIChtb2RlKSB7XG4gICAgY2FzZSAnaGlzdG9yeSc6XG4gICAgICB0aGlzLmhpc3RvcnkgPSBuZXcgSFRNTDVIaXN0b3J5KHRoaXMsIG9wdGlvbnMuYmFzZSk7XG4gICAgICBicmVha1xuICAgIGNhc2UgJ2hhc2gnOlxuICAgICAgdGhpcy5oaXN0b3J5ID0gbmV3IEhhc2hIaXN0b3J5KHRoaXMsIG9wdGlvbnMuYmFzZSwgdGhpcy5mYWxsYmFjayk7XG4gICAgICBicmVha1xuICAgIGNhc2UgJ2Fic3RyYWN0JzpcbiAgICAgIHRoaXMuaGlzdG9yeSA9IG5ldyBBYnN0cmFjdEhpc3RvcnkodGhpcywgb3B0aW9ucy5iYXNlKTtcbiAgICAgIGJyZWFrXG4gICAgZGVmYXVsdDpcbiAgICAgIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XG4gICAgICAgIGFzc2VydChmYWxzZSwgKFwiaW52YWxpZCBtb2RlOiBcIiArIG1vZGUpKTtcbiAgICAgIH1cbiAgfVxufTtcblxudmFyIHByb3RvdHlwZUFjY2Vzc29ycyA9IHsgY3VycmVudFJvdXRlOiB7IGNvbmZpZ3VyYWJsZTogdHJ1ZSB9IH07XG5cblZ1ZVJvdXRlci5wcm90b3R5cGUubWF0Y2ggPSBmdW5jdGlvbiBtYXRjaCAoXG4gIHJhdyxcbiAgY3VycmVudCxcbiAgcmVkaXJlY3RlZEZyb21cbikge1xuICByZXR1cm4gdGhpcy5tYXRjaGVyLm1hdGNoKHJhdywgY3VycmVudCwgcmVkaXJlY3RlZEZyb20pXG59O1xuXG5wcm90b3R5cGVBY2Nlc3NvcnMuY3VycmVudFJvdXRlLmdldCA9IGZ1bmN0aW9uICgpIHtcbiAgcmV0dXJuIHRoaXMuaGlzdG9yeSAmJiB0aGlzLmhpc3RvcnkuY3VycmVudFxufTtcblxuVnVlUm91dGVyLnByb3RvdHlwZS5pbml0ID0gZnVuY3Rpb24gaW5pdCAoYXBwIC8qIFZ1ZSBjb21wb25lbnQgaW5zdGFuY2UgKi8pIHtcbiAgICB2YXIgdGhpcyQxID0gdGhpcztcblxuICBwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nICYmIGFzc2VydChcbiAgICBpbnN0YWxsLmluc3RhbGxlZCxcbiAgICBcIm5vdCBpbnN0YWxsZWQuIE1ha2Ugc3VyZSB0byBjYWxsIGBWdWUudXNlKFZ1ZVJvdXRlcilgIFwiICtcbiAgICBcImJlZm9yZSBjcmVhdGluZyByb290IGluc3RhbmNlLlwiXG4gICk7XG5cbiAgdGhpcy5hcHBzLnB1c2goYXBwKTtcblxuICAvLyBzZXQgdXAgYXBwIGRlc3Ryb3llZCBoYW5kbGVyXG4gIC8vIGh0dHBzOi8vZ2l0aHViLmNvbS92dWVqcy92dWUtcm91dGVyL2lzc3Vlcy8yNjM5XG4gIGFwcC4kb25jZSgnaG9vazpkZXN0cm95ZWQnLCBmdW5jdGlvbiAoKSB7XG4gICAgLy8gY2xlYW4gb3V0IGFwcCBmcm9tIHRoaXMuYXBwcyBhcnJheSBvbmNlIGRlc3Ryb3llZFxuICAgIHZhciBpbmRleCA9IHRoaXMkMS5hcHBzLmluZGV4T2YoYXBwKTtcbiAgICBpZiAoaW5kZXggPiAtMSkgeyB0aGlzJDEuYXBwcy5zcGxpY2UoaW5kZXgsIDEpOyB9XG4gICAgLy8gZW5zdXJlIHdlIHN0aWxsIGhhdmUgYSBtYWluIGFwcCBvciBudWxsIGlmIG5vIGFwcHNcbiAgICAvLyB3ZSBkbyBub3QgcmVsZWFzZSB0aGUgcm91dGVyIHNvIGl0IGNhbiBiZSByZXVzZWRcbiAgICBpZiAodGhpcyQxLmFwcCA9PT0gYXBwKSB7IHRoaXMkMS5hcHAgPSB0aGlzJDEuYXBwc1swXSB8fCBudWxsOyB9XG4gIH0pO1xuXG4gIC8vIG1haW4gYXBwIHByZXZpb3VzbHkgaW5pdGlhbGl6ZWRcbiAgLy8gcmV0dXJuIGFzIHdlIGRvbid0IG5lZWQgdG8gc2V0IHVwIG5ldyBoaXN0b3J5IGxpc3RlbmVyXG4gIGlmICh0aGlzLmFwcCkge1xuICAgIHJldHVyblxuICB9XG5cbiAgdGhpcy5hcHAgPSBhcHA7XG5cbiAgdmFyIGhpc3RvcnkgPSB0aGlzLmhpc3Rvcnk7XG5cbiAgaWYgKGhpc3RvcnkgaW5zdGFuY2VvZiBIVE1MNUhpc3RvcnkpIHtcbiAgICBoaXN0b3J5LnRyYW5zaXRpb25UbyhoaXN0b3J5LmdldEN1cnJlbnRMb2NhdGlvbigpKTtcbiAgfSBlbHNlIGlmIChoaXN0b3J5IGluc3RhbmNlb2YgSGFzaEhpc3RvcnkpIHtcbiAgICB2YXIgc2V0dXBIYXNoTGlzdGVuZXIgPSBmdW5jdGlvbiAoKSB7XG4gICAgICBoaXN0b3J5LnNldHVwTGlzdGVuZXJzKCk7XG4gICAgfTtcbiAgICBoaXN0b3J5LnRyYW5zaXRpb25UbyhcbiAgICAgIGhpc3RvcnkuZ2V0Q3VycmVudExvY2F0aW9uKCksXG4gICAgICBzZXR1cEhhc2hMaXN0ZW5lcixcbiAgICAgIHNldHVwSGFzaExpc3RlbmVyXG4gICAgKTtcbiAgfVxuXG4gIGhpc3RvcnkubGlzdGVuKGZ1bmN0aW9uIChyb3V0ZSkge1xuICAgIHRoaXMkMS5hcHBzLmZvckVhY2goZnVuY3Rpb24gKGFwcCkge1xuICAgICAgYXBwLl9yb3V0ZSA9IHJvdXRlO1xuICAgIH0pO1xuICB9KTtcbn07XG5cblZ1ZVJvdXRlci5wcm90b3R5cGUuYmVmb3JlRWFjaCA9IGZ1bmN0aW9uIGJlZm9yZUVhY2ggKGZuKSB7XG4gIHJldHVybiByZWdpc3Rlckhvb2sodGhpcy5iZWZvcmVIb29rcywgZm4pXG59O1xuXG5WdWVSb3V0ZXIucHJvdG90eXBlLmJlZm9yZVJlc29sdmUgPSBmdW5jdGlvbiBiZWZvcmVSZXNvbHZlIChmbikge1xuICByZXR1cm4gcmVnaXN0ZXJIb29rKHRoaXMucmVzb2x2ZUhvb2tzLCBmbilcbn07XG5cblZ1ZVJvdXRlci5wcm90b3R5cGUuYWZ0ZXJFYWNoID0gZnVuY3Rpb24gYWZ0ZXJFYWNoIChmbikge1xuICByZXR1cm4gcmVnaXN0ZXJIb29rKHRoaXMuYWZ0ZXJIb29rcywgZm4pXG59O1xuXG5WdWVSb3V0ZXIucHJvdG90eXBlLm9uUmVhZHkgPSBmdW5jdGlvbiBvblJlYWR5IChjYiwgZXJyb3JDYikge1xuICB0aGlzLmhpc3Rvcnkub25SZWFkeShjYiwgZXJyb3JDYik7XG59O1xuXG5WdWVSb3V0ZXIucHJvdG90eXBlLm9uRXJyb3IgPSBmdW5jdGlvbiBvbkVycm9yIChlcnJvckNiKSB7XG4gIHRoaXMuaGlzdG9yeS5vbkVycm9yKGVycm9yQ2IpO1xufTtcblxuVnVlUm91dGVyLnByb3RvdHlwZS5wdXNoID0gZnVuY3Rpb24gcHVzaCAobG9jYXRpb24sIG9uQ29tcGxldGUsIG9uQWJvcnQpIHtcbiAgICB2YXIgdGhpcyQxID0gdGhpcztcblxuICAvLyAkZmxvdy1kaXNhYmxlLWxpbmVcbiAgaWYgKCFvbkNvbXBsZXRlICYmICFvbkFib3J0ICYmIHR5cGVvZiBQcm9taXNlICE9PSAndW5kZWZpbmVkJykge1xuICAgIHJldHVybiBuZXcgUHJvbWlzZShmdW5jdGlvbiAocmVzb2x2ZSwgcmVqZWN0KSB7XG4gICAgICB0aGlzJDEuaGlzdG9yeS5wdXNoKGxvY2F0aW9uLCByZXNvbHZlLCByZWplY3QpO1xuICAgIH0pXG4gIH0gZWxzZSB7XG4gICAgdGhpcy5oaXN0b3J5LnB1c2gobG9jYXRpb24sIG9uQ29tcGxldGUsIG9uQWJvcnQpO1xuICB9XG59O1xuXG5WdWVSb3V0ZXIucHJvdG90eXBlLnJlcGxhY2UgPSBmdW5jdGlvbiByZXBsYWNlIChsb2NhdGlvbiwgb25Db21wbGV0ZSwgb25BYm9ydCkge1xuICAgIHZhciB0aGlzJDEgPSB0aGlzO1xuXG4gIC8vICRmbG93LWRpc2FibGUtbGluZVxuICBpZiAoIW9uQ29tcGxldGUgJiYgIW9uQWJvcnQgJiYgdHlwZW9mIFByb21pc2UgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKGZ1bmN0aW9uIChyZXNvbHZlLCByZWplY3QpIHtcbiAgICAgIHRoaXMkMS5oaXN0b3J5LnJlcGxhY2UobG9jYXRpb24sIHJlc29sdmUsIHJlamVjdCk7XG4gICAgfSlcbiAgfSBlbHNlIHtcbiAgICB0aGlzLmhpc3RvcnkucmVwbGFjZShsb2NhdGlvbiwgb25Db21wbGV0ZSwgb25BYm9ydCk7XG4gIH1cbn07XG5cblZ1ZVJvdXRlci5wcm90b3R5cGUuZ28gPSBmdW5jdGlvbiBnbyAobikge1xuICB0aGlzLmhpc3RvcnkuZ28obik7XG59O1xuXG5WdWVSb3V0ZXIucHJvdG90eXBlLmJhY2sgPSBmdW5jdGlvbiBiYWNrICgpIHtcbiAgdGhpcy5nbygtMSk7XG59O1xuXG5WdWVSb3V0ZXIucHJvdG90eXBlLmZvcndhcmQgPSBmdW5jdGlvbiBmb3J3YXJkICgpIHtcbiAgdGhpcy5nbygxKTtcbn07XG5cblZ1ZVJvdXRlci5wcm90b3R5cGUuZ2V0TWF0Y2hlZENvbXBvbmVudHMgPSBmdW5jdGlvbiBnZXRNYXRjaGVkQ29tcG9uZW50cyAodG8pIHtcbiAgdmFyIHJvdXRlID0gdG9cbiAgICA/IHRvLm1hdGNoZWRcbiAgICAgID8gdG9cbiAgICAgIDogdGhpcy5yZXNvbHZlKHRvKS5yb3V0ZVxuICAgIDogdGhpcy5jdXJyZW50Um91dGU7XG4gIGlmICghcm91dGUpIHtcbiAgICByZXR1cm4gW11cbiAgfVxuICByZXR1cm4gW10uY29uY2F0LmFwcGx5KFtdLCByb3V0ZS5tYXRjaGVkLm1hcChmdW5jdGlvbiAobSkge1xuICAgIHJldHVybiBPYmplY3Qua2V5cyhtLmNvbXBvbmVudHMpLm1hcChmdW5jdGlvbiAoa2V5KSB7XG4gICAgICByZXR1cm4gbS5jb21wb25lbnRzW2tleV1cbiAgICB9KVxuICB9KSlcbn07XG5cblZ1ZVJvdXRlci5wcm90b3R5cGUucmVzb2x2ZSA9IGZ1bmN0aW9uIHJlc29sdmUgKFxuICB0byxcbiAgY3VycmVudCxcbiAgYXBwZW5kXG4pIHtcbiAgY3VycmVudCA9IGN1cnJlbnQgfHwgdGhpcy5oaXN0b3J5LmN1cnJlbnQ7XG4gIHZhciBsb2NhdGlvbiA9IG5vcm1hbGl6ZUxvY2F0aW9uKFxuICAgIHRvLFxuICAgIGN1cnJlbnQsXG4gICAgYXBwZW5kLFxuICAgIHRoaXNcbiAgKTtcbiAgdmFyIHJvdXRlID0gdGhpcy5tYXRjaChsb2NhdGlvbiwgY3VycmVudCk7XG4gIHZhciBmdWxsUGF0aCA9IHJvdXRlLnJlZGlyZWN0ZWRGcm9tIHx8IHJvdXRlLmZ1bGxQYXRoO1xuICB2YXIgYmFzZSA9IHRoaXMuaGlzdG9yeS5iYXNlO1xuICB2YXIgaHJlZiA9IGNyZWF0ZUhyZWYoYmFzZSwgZnVsbFBhdGgsIHRoaXMubW9kZSk7XG4gIHJldHVybiB7XG4gICAgbG9jYXRpb246IGxvY2F0aW9uLFxuICAgIHJvdXRlOiByb3V0ZSxcbiAgICBocmVmOiBocmVmLFxuICAgIC8vIGZvciBiYWNrd2FyZHMgY29tcGF0XG4gICAgbm9ybWFsaXplZFRvOiBsb2NhdGlvbixcbiAgICByZXNvbHZlZDogcm91dGVcbiAgfVxufTtcblxuVnVlUm91dGVyLnByb3RvdHlwZS5hZGRSb3V0ZXMgPSBmdW5jdGlvbiBhZGRSb3V0ZXMgKHJvdXRlcykge1xuICB0aGlzLm1hdGNoZXIuYWRkUm91dGVzKHJvdXRlcyk7XG4gIGlmICh0aGlzLmhpc3RvcnkuY3VycmVudCAhPT0gU1RBUlQpIHtcbiAgICB0aGlzLmhpc3RvcnkudHJhbnNpdGlvblRvKHRoaXMuaGlzdG9yeS5nZXRDdXJyZW50TG9jYXRpb24oKSk7XG4gIH1cbn07XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0aWVzKCBWdWVSb3V0ZXIucHJvdG90eXBlLCBwcm90b3R5cGVBY2Nlc3NvcnMgKTtcblxuZnVuY3Rpb24gcmVnaXN0ZXJIb29rIChsaXN0LCBmbikge1xuICBsaXN0LnB1c2goZm4pO1xuICByZXR1cm4gZnVuY3Rpb24gKCkge1xuICAgIHZhciBpID0gbGlzdC5pbmRleE9mKGZuKTtcbiAgICBpZiAoaSA+IC0xKSB7IGxpc3Quc3BsaWNlKGksIDEpOyB9XG4gIH1cbn1cblxuZnVuY3Rpb24gY3JlYXRlSHJlZiAoYmFzZSwgZnVsbFBhdGgsIG1vZGUpIHtcbiAgdmFyIHBhdGggPSBtb2RlID09PSAnaGFzaCcgPyAnIycgKyBmdWxsUGF0aCA6IGZ1bGxQYXRoO1xuICByZXR1cm4gYmFzZSA/IGNsZWFuUGF0aChiYXNlICsgJy8nICsgcGF0aCkgOiBwYXRoXG59XG5cblZ1ZVJvdXRlci5pbnN0YWxsID0gaW5zdGFsbDtcblZ1ZVJvdXRlci52ZXJzaW9uID0gJzMuMS4zJztcblxuaWYgKGluQnJvd3NlciAmJiB3aW5kb3cuVnVlKSB7XG4gIHdpbmRvdy5WdWUudXNlKFZ1ZVJvdXRlcik7XG59XG5cbmV4cG9ydCBkZWZhdWx0IFZ1ZVJvdXRlcjtcbiIsInZhciBtYXAgPSB7XG5cdFwiLi9sb2dcIjogXCIuL25vZGVfbW9kdWxlcy93ZWJwYWNrL2hvdC9sb2cuanNcIlxufTtcblxuXG5mdW5jdGlvbiB3ZWJwYWNrQ29udGV4dChyZXEpIHtcblx0dmFyIGlkID0gd2VicGFja0NvbnRleHRSZXNvbHZlKHJlcSk7XG5cdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKGlkKTtcbn1cbmZ1bmN0aW9uIHdlYnBhY2tDb250ZXh0UmVzb2x2ZShyZXEpIHtcblx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhtYXAsIHJlcSkpIHtcblx0XHR2YXIgZSA9IG5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIgKyByZXEgKyBcIidcIik7XG5cdFx0ZS5jb2RlID0gJ01PRFVMRV9OT1RfRk9VTkQnO1xuXHRcdHRocm93IGU7XG5cdH1cblx0cmV0dXJuIG1hcFtyZXFdO1xufVxud2VicGFja0NvbnRleHQua2V5cyA9IGZ1bmN0aW9uIHdlYnBhY2tDb250ZXh0S2V5cygpIHtcblx0cmV0dXJuIE9iamVjdC5rZXlzKG1hcCk7XG59O1xud2VicGFja0NvbnRleHQucmVzb2x2ZSA9IHdlYnBhY2tDb250ZXh0UmVzb2x2ZTtcbm1vZHVsZS5leHBvcnRzID0gd2VicGFja0NvbnRleHQ7XG53ZWJwYWNrQ29udGV4dC5pZCA9IFwiLi9ub2RlX21vZHVsZXMvd2VicGFjay9ob3Qgc3luYyBeXFxcXC5cXFxcL2xvZyRcIjsiLCJleHBvcnQgZGVmYXVsdCB7XHJcbiAgbmFtZTogJ3ZpJyxcclxuICBtZXNzYWdlczoge1xyXG4gICAgX2RlZmF1bHQ6IGZ1bmN0aW9uIChuKSB7XHJcbiAgICAgIHJldHVybiAnR2nDoSB0cuG7iyBj4bunYSAnICsgbiArICcga2jDtG5nIMSRw7puZy4nO1xyXG4gICAgfSxcclxuICAgIGFmdGVyOiBmdW5jdGlvbiAobiwgdCkge1xyXG4gICAgICByZXR1cm4gbiArICcgcGjhuqNpIHh14bqldCBoaeG7h24gc2F1ICcgKyB0Lmxlbmd0aCArICcuJztcclxuICAgIH0sXHJcbiAgICBhbHBoYV9kYXNoOiBmdW5jdGlvbiAobikge1xyXG4gICAgICByZXR1cm4gbiArICcgY8OzIHRo4buDIGNo4bupYSBjw6FjIGvDrSB04buxIGNo4buvIChBLVogYS16KSwgc+G7kSAoMC05KSwgZ+G6oWNoIG5nYW5nICgtKSB2w6AgZ+G6oWNoIGTGsOG7m2kgKF8pLic7XHJcbiAgICB9LFxyXG4gICAgYWxwaGFfbnVtOiBmdW5jdGlvbiAobikge1xyXG4gICAgICByZXR1cm4gbiArICcgY2jhu4kgY8OzIHRo4buDIGNo4bupYSBjw6FjIGvDrSB04buxIGNo4buvIHbDoCBz4buRLic7XHJcbiAgICB9LFxyXG4gICAgYWxwaGFfc3BhY2VzOiBmdW5jdGlvbiAobikge1xyXG4gICAgICByZXR1cm4gbiArICcgY2jhu4kgY8OzIHRo4bq/IGNo4bupYSBjw6FjIGvDrSB04buxIHbDoCBraG/huqNuZyB0cuG6r25nJztcclxuICAgIH0sXHJcbiAgICBhbHBoYTogZnVuY3Rpb24gKG4pIHtcclxuICAgICAgcmV0dXJuIG4gKyAnIGNo4buJIGPDsyB0aOG7gyBjaOG7qWEgY8OhYyBrw60gdOG7sSBjaOG7ry4nO1xyXG4gICAgfSxcclxuICAgIGJlZm9yZTogZnVuY3Rpb24gKG4sIHQpIHtcclxuICAgICAgcmV0dXJuIG4gKyAnIHBo4bqjaSB4deG6pXQgaGnhu4duIHRyxrDhu5tjICcgKyB0Lmxlbmd0aCArICcuJztcclxuICAgIH0sXHJcbiAgICBiZXR3ZWVuOiBmdW5jdGlvbiAobiwgdCkge1xyXG4gICAgICByZXR1cm4gbiArICcgcGjhuqNpIGPDsyBnacOhIHRy4buLIG7hurFtIHRyb25nIGtob+G6o25nIGdp4buvYSAnICsgdC5sZW5ndGggKyAnIHbDoCAnICsgdFsgMSBdICsgJy4nO1xyXG4gICAgfSxcclxuICAgIGNvbmZpcm1lZDogZnVuY3Rpb24gKG4sIHQpIHtcclxuICAgICAgcmV0dXJuIG4gKyAnIGtow6FjIHbhu5tpICcgKyB0Lmxlbmd0aCArICcuJztcclxuICAgIH0sXHJcbiAgICBjcmVkaXRfY2FyZDogZnVuY3Rpb24gKG4pIHtcclxuICAgICAgcmV0dXJuICfEkMOjIMSRaeG7gW4gJyArIG4gKyAnIGtow7RuZyBjaMOtbmggeMOhYy4nO1xyXG4gICAgfSxcclxuICAgIGRhdGVfYmV0d2VlbjogZnVuY3Rpb24gKG4sIHQpIHtcclxuICAgICAgcmV0dXJuIG4gKyAnIHBo4bqjaSBjw7MgZ2nDoSB0cuG7iyBu4bqxbSB0cm9uZyBraG/huqNuZyBnaeG7r2EgICcgKyB0Lmxlbmd0aCArICcgdsOgICcgKyB0WyAxIF0gKyAnLic7XHJcbiAgICB9LFxyXG4gICAgZGF0ZV9mb3JtYXQ6IGZ1bmN0aW9uIChuLCB0KSB7XHJcbiAgICAgIHJldHVybiBuICsgJyBwaOG6o2kgY8OzIGdpw6EgdHLhu4sgZMaw4bubaSDEkeG7i25oIGThuqFuZyAnICsgdC5sZW5ndGggKyAnLic7XHJcbiAgICB9LFxyXG4gICAgZGVjaW1hbDogZnVuY3Rpb24gKG4sIHQpIHtcclxuICAgICAgdm9pZCAwID09PSB0ICYmICh0ID0gW10pO1xyXG4gICAgICB2YXIgYyA9IHQubGVuZ3RoO1xyXG4gICAgICByZXR1cm4gdm9pZCAwID09PSBjICYmIChjID0gJyonKSwgbiArICcgY2jhu4kgY8OzIHRo4buDIGNo4bupYSBjw6FjIGvDrSB04buxIHPhu5EgdsOgIGThuqV1IHRo4bqtcCBwaMOibiAnICsgKCcqJyA9PT0gYyA/ICcnIDogYykgKyAnLic7XHJcbiAgICB9LFxyXG4gICAgZGlnaXRzOiBmdW5jdGlvbiAobiwgdCkge1xyXG4gICAgICByZXR1cm4gJ1RyxrDhu51uZyAnICsgbiArICcgY2jhu4kgY8OzIHRo4buDIGNo4bupYSBjw6FjIGvDrSB04buxIHPhu5EgdsOgIGLhuq90IGJ14buZYyBwaOG6o2kgY8OzIMSR4buZIGTDoGkgbMOgICcgKyB0Lmxlbmd0aCArICcuJztcclxuICAgIH0sXHJcbiAgICBkaW1lbnNpb25zOiBmdW5jdGlvbiAobiwgdCkge1xyXG4gICAgICByZXR1cm4gbiArICcgcGjhuqNpIGPDsyBjaGnhu4F1IHLhu5luZyAnICsgdC5sZW5ndGggKyAnIHBpeGVscyB2w6AgY2hp4buBdSBjYW8gJyArIHRbIDEgXSArICcgcGl4ZWxzLic7XHJcbiAgICB9LFxyXG4gICAgZW1haWw6IGZ1bmN0aW9uIChuKSB7XHJcbiAgICAgIHJldHVybiBuICsgJyBwaOG6o2kgbMOgIG3hu5l0IMSR4buLYSBjaOG7iSBlbWFpbCBo4bujcCBs4buHLic7XHJcbiAgICB9LFxyXG4gICAgZXh0OiBmdW5jdGlvbiAobikge1xyXG4gICAgICByZXR1cm4gbiArICcgcGjhuqNpIGzDoCBt4buZdCB04buHcC4nO1xyXG4gICAgfSxcclxuICAgIGltYWdlOiBmdW5jdGlvbiAobikge1xyXG4gICAgICByZXR1cm4gJ1RyxrDhu51uZyAnICsgbiArICcgcGjhuqNpIGzDoCBt4buZdCDhuqNuaC4nO1xyXG4gICAgfSxcclxuICAgIGluY2x1ZGVkOiBmdW5jdGlvbiAobikge1xyXG4gICAgICByZXR1cm4gbiArICcgcGjhuqNpIGzDoCBt4buZdCBnacOhIHRy4buLLic7XHJcbiAgICB9LFxyXG4gICAgaXA6IGZ1bmN0aW9uIChuKSB7XHJcbiAgICAgIHJldHVybiBuICsgJyBwaOG6o2kgbMOgIG3hu5l0IMSR4buLYSBjaOG7iSBpcCBo4bujcCBs4buHLic7XHJcbiAgICB9LFxyXG4gICAgbWF4OiBmdW5jdGlvbiAobiwgdCkge1xyXG4gICAgICByZXR1cm4gbiArICcga2jDtG5nIHRo4buDIGPDsyBuaGnhu4F1IGjGoW4gJyArIHQubGVuZ3RoICsgJyBrw60gdOG7sS4nO1xyXG4gICAgfSxcclxuICAgIG1heF92YWx1ZTogZnVuY3Rpb24gKG4sIHQpIHtcclxuICAgICAgcmV0dXJuIG4gKyAnIHBo4bqjaSBuaOG7jyBoxqFuIGhv4bq3YyBi4bqxbmcgJyArIHQubGVuZ3RoICsgJy4nO1xyXG4gICAgfSxcclxuICAgIG1pbWVzOiBmdW5jdGlvbiAobikge1xyXG4gICAgICByZXR1cm4gbiArICcgcGjhuqNpIGNo4bupYSBraeG7g3UgdOG7h3AgcGjDuSBo4bujcC4nO1xyXG4gICAgfSxcclxuICAgIG1pbjogZnVuY3Rpb24gKG4sIHQpIHtcclxuICAgICAgcmV0dXJuIG4gKyAnIHBo4bqjaSBjaOG7qWEgw610IG5o4bqldCAnICsgdC5sZW5ndGggKyAnIGvDrSB04buxLic7XHJcbiAgICB9LFxyXG4gICAgbWluX3ZhbHVlOiBmdW5jdGlvbiAobiwgdCkge1xyXG4gICAgICByZXR1cm4gbiArICcgcGjhuqNpIGzhu5tuIGjGoW4gaG/hurdjIGLhurFuZyAnICsgdC5sZW5ndGggKyAnLic7XHJcbiAgICB9LFxyXG4gICAgZXhjbHVkZWQ6IGZ1bmN0aW9uIChuKSB7XHJcbiAgICAgIHJldHVybiBuICsgJyBwaOG6o2kgY2jhu6lhIG3hu5l0IGdpw6EgdHLhu4sgaOG7o3AgbOG7hy4nO1xyXG4gICAgfSxcclxuICAgIG51bWVyaWM6IGZ1bmN0aW9uIChuKSB7XHJcbiAgICAgIHJldHVybiBuICsgJyBjaOG7iSBjw7MgdGjhu4MgY8OzIGPDoWMga8OtIHThu7Egc+G7kS4nO1xyXG4gICAgfSxcclxuICAgIHJlZ2V4OiBmdW5jdGlvbiAobikge1xyXG4gICAgICByZXR1cm4gbiArICcgY8OzIMSR4buLbmggZOG6oW5nIGtow7RuZyDEkcO6bmcuJztcclxuICAgIH0sXHJcbiAgICByZXF1aXJlZDogZnVuY3Rpb24gKG4pIHtcclxuICAgICAgcmV0dXJuIG4gKyAnIGtow7RuZyDEkcaw4bujYyDEkeG7gyB0cuG7kW5nLic7XHJcbiAgICB9LFxyXG4gICAgc2l6ZTogZnVuY3Rpb24gKG4sIHQpIHtcclxuICAgICAgdmFyIGMsXHJcbiAgICAgICAgICBlLFxyXG4gICAgICAgICAgaSxcclxuICAgICAgICAgIGggPSB0Lmxlbmd0aDtcclxuICAgICAgcmV0dXJuIChcclxuICAgICAgICAgIG4gK1xyXG4gICAgICAgICAgJyBjaOG7iSBjw7MgdGjhu4MgY2jhu6lhIHThu4dwIG5o4buPIGjGoW4gJyArXHJcbiAgICAgICAgICAoKGMgPSBoKSxcclxuICAgICAgICAgICAgICAoZSA9IDEwMjQpLFxyXG4gICAgICAgICAgICAgIChpID0gMCA9PSAoYyA9IE51bWJlcihjKSAqIGUpID8gMCA6IE1hdGguZmxvb3IoTWF0aC5sb2coYykgLyBNYXRoLmxvZyhlKSkpLFxyXG4gICAgICAgICAgMSAqIChjIC8gTWF0aC5wb3coZSwgaSkpLnRvRml4ZWQoMikgKyAnICcgKyBbICdCeXRlJywgJ0tCJywgJ01CJywgJ0dCJywgJ1RCJywgJ1BCJywgJ0VCJywgJ1pCJywgJ1lCJyBdWyBpIF0pICtcclxuICAgICAgICAgICcuJ1xyXG4gICAgICApO1xyXG4gICAgfSxcclxuICAgIHVybDogZnVuY3Rpb24gKG4pIHtcclxuICAgICAgcmV0dXJuIG4gKyAnIGtow7RuZyBwaOG6o2kgbMOgIG3hu5l0IMSR4buLYSBjaOG7iSBVUkwgaOG7o3AgbOG7hy4nO1xyXG4gICAgfSxcclxuICB9LFxyXG59OyIsImltcG9ydCAkIGZyb20gXCJqcXVlcnlcIlxyXG5pbXBvcnQgVnVlIGZyb20gJ3Z1ZSdcclxuaW1wb3J0IFZ1ZXggZnJvbSAndnVleCdcclxuaW1wb3J0IFZ1ZVJvdXRlciBmcm9tICd2dWUtcm91dGVyJ1xyXG5cclxuaW1wb3J0IEFwcCBmcm9tICckdnVlUGFydGlhbFBhdGgvdXNlci9BcHAnXHJcbmltcG9ydCBVc2VySW5kZXhTZWN0aW9uIGZyb20gJyR2dWVQYXJ0aWFsUGF0aC91c2VyL1VzZXJJbmRleFNlY3Rpb24nXHJcbmltcG9ydCBVc2VyQWRkU2VjdGlvbiBmcm9tICckdnVlUGFydGlhbFBhdGgvdXNlci9Vc2VyQWRkU2VjdGlvbidcclxuaW1wb3J0IFVzZXJFZGl0U2VjdGlvbiBmcm9tICckdnVlUGFydGlhbFBhdGgvdXNlci9Vc2VyRWRpdFNlY3Rpb24nXHJcblxyXG5pbXBvcnQgVXNlclRhYmxlIGZyb20gJyR2dWVQYXJ0aWFsUGF0aC91c2VyL1VzZXJUYWJsZSdcclxuXHJcbmltcG9ydCBTdG9yZSBmcm9tICckanNTdG9yZVBhdGgvdXNlcidcclxuXHJcblZ1ZS51c2UoVnVleClcclxuVnVlLnVzZShWdWVSb3V0ZXIpXHJcblxyXG5jb25zdCBzdG9yZSA9IG5ldyBWdWV4LlN0b3JlKFN0b3JlKVxyXG5jb25zdCByb3V0ZXIgPSBuZXcgVnVlUm91dGVyKHtcclxuICBtb2RlOiAnaGlzdG9yeScsXHJcbiAgcm91dGVzOiBbXHJcbiAgICB7IG5hbWU6ICd1c2VyOmluZGV4JywgcGF0aDogJy9iYWNrZW5kL3VzZXIvaW5kZXgvOnBhZ2UnLCBjb21wb25lbnQ6IFVzZXJJbmRleFNlY3Rpb24gfSxcclxuICAgIHsgbmFtZTogJ3VzZXI6YWRkJywgcGF0aDogJy9iYWNrZW5kL3VzZXIvYWRkLzpwYWdlJywgY29tcG9uZW50OiBVc2VyQWRkU2VjdGlvbiB9LFxyXG4gICAgeyBuYW1lOiAndXNlcjplZGl0JywgcGF0aDogJy9iYWNrZW5kL3VzZXIvZWRpdC86aWQvOnBhZ2UnLCBjb21wb25lbnQ6IFVzZXJFZGl0U2VjdGlvbiB9LFxyXG4gIF1cclxufSk7XHJcblxyXG5jb25zdCBPYmplY3QgPSAoKCkgPT4ge1xyXG4gIGNvbnN0IF9yZW5kZXJVc2VyVGFibGUgPSAoKSA9PiB7XHJcbiAgICBsZXQgJGVsID0gJCgnI3VzZXItc2VjdGlvbicpXHJcblxyXG4gICAgbmV3IFZ1ZSh7XHJcbiAgICAgIHJvdXRlcixcclxuICAgICAgc3RvcmUsXHJcbiAgICAgIHJlbmRlcjogaCA9PiBoKEFwcClcclxuICAgIH0pLiRtb3VudCgkZWxbIDAgXSlcclxuICB9XHJcblxyXG4gIHJldHVybiB7XHJcbiAgICBpbml0KCkge1xyXG4gICAgICBfcmVuZGVyVXNlclRhYmxlKClcclxuICAgIH1cclxuICB9XHJcbn0pKClcclxuXHJcbmRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJET01Db250ZW50TG9hZGVkXCIsIGZ1bmN0aW9uICgpIHtcclxuICBPYmplY3QuaW5pdCgpXHJcbn0pOyIsImltcG9ydCB7IGZvckluIGFzIF9mb3JJbiwgaXNFbXB0eSBhcyBfaXNFbXB0eSB9IGZyb20gJ2xvZGFzaCc7XHJcblxyXG5pbXBvcnQgKiBhcyBBUEkgZnJvbSAnJGpzQ29tbW9uUGF0aC9hcGknXHJcbmltcG9ydCB7IGlzRXJyb3JBUEksIHRyaWdnZXJNb2RhbCB9IGZyb20gXCIkanNDb21tb25QYXRoL3V0aWwvR2VuZXJhbFV0aWxcIjtcclxuaW1wb3J0IHsgX2ludCB9IGZyb20gXCIkanNDb21tb25QYXRoL3V0aWwvU2FmZVV0aWxcIjtcclxuXHJcbmxldCBfb2JqRW50aXR5ID0ge1xyXG4gIHVzZXJfaWQ6IG51bGwsXHJcbiAgcHJvZmlsZV9pbWFnZTogbnVsbCxcclxuICBmdWxsbmFtZTogbnVsbCxcclxuICBlbWFpbDogbnVsbCxcclxuICBwaG9uZTogbnVsbCxcclxuICBpc19hY3RpdmVkOiAwLFxyXG59XHJcblxyXG5jbGFzcyBTdG9yZSB7XHJcbiAgY29uc3RydWN0b3IoKSB7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICBzdGF0ZToge1xyXG4gICAgICAgIGZsYWc6IHtcclxuICAgICAgICAgIGlzR2V0OiBmYWxzZVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgYXJyRW50aXR5TGlzdDogW10sXHJcbiAgICAgICAgb2JqRW50aXR5U2VsZWN0OiB7IC4uLl9vYmpFbnRpdHkgfSxcclxuICAgICAgfSxcclxuICAgICAgbXV0YXRpb25zOiB7XHJcbiAgICAgICAgcmVzZXRFbnRpdHkoc3RhdGUpIHtcclxuICAgICAgICAgIHN0YXRlLm9iakVudGl0eVNlbGVjdCA9IHsgLi4uX29iakVudGl0eSB9XHJcbiAgICAgICAgfSxcclxuICAgICAgICB1cGRhdGVGbGFnKHN0YXRlLCBwYXlsb2FkKSB7XHJcbiAgICAgICAgICBzdGF0ZS5mbGFnWyBwYXlsb2FkLmZsYWcgXSA9IHBheWxvYWQudmFsdWVcclxuICAgICAgICB9LFxyXG4gICAgICAgIHNldEVudGl0aWVzKHN0YXRlLCBwYXlsb2FkKSB7XHJcbiAgICAgICAgICBzdGF0ZS5hcnJFbnRpdHlMaXN0ID0gcGF5bG9hZC5hcnJFbnRpdHlMaXN0XHJcbiAgICAgICAgfSxcclxuICAgICAgICBzZXRFbnRpdHkoc3RhdGUsIHBheWxvYWQpIHtcclxuICAgICAgICAgIGlmIChfaXNFbXB0eShwYXlsb2FkLm9iakVudGl0eVNlbGVjdCkpXHJcbiAgICAgICAgICAgIHJldHVyblxyXG5cclxuICAgICAgICAgIGxldCB7IG9iakVudGl0eVNlbGVjdCB9ID0geyAuLi5wYXlsb2FkIH1cclxuXHJcbiAgICAgICAgICBvYmpFbnRpdHlTZWxlY3QucGFzc3dvcmQgPSBudWxsXHJcblxyXG4gICAgICAgICAgc3RhdGUub2JqRW50aXR5U2VsZWN0ID0gb2JqRW50aXR5U2VsZWN0XHJcbiAgICAgICAgfSxcclxuICAgICAgICB1cGRhdGVFbnRpdHkoc3RhdGUsIHBheWxvYWQpIHtcclxuICAgICAgICAgIGxldCB7IHR5cGUsIGluZGV4LCBhcnJFbnRpdHkgfSA9IHsgLi4ucGF5bG9hZCB9XHJcblxyXG4gICAgICAgICAgc3dpdGNoICh0eXBlKSB7XHJcbiAgICAgICAgICAgIGNhc2UgJ2NyZWF0ZSc6XHJcbiAgICAgICAgICAgICAgc3RhdGUuYXJyRW50aXR5TGlzdC5wdXNoKGFyckVudGl0eSlcclxuICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSAndXBkYXRlJzpcclxuICAgICAgICAgICAgICBzdGF0ZS5hcnJFbnRpdHlMaXN0WyBpbmRleCBdID0gYXJyRW50aXR5XHJcbiAgICAgICAgICAgICAgc3RhdGUub2JqRW50aXR5U2VsZWN0ID0gYXJyRW50aXR5XHJcblxyXG4gICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICdkZWxldGUnOlxyXG4gICAgICAgICAgICAgIHN0YXRlLmFyckVudGl0eUxpc3Quc3BsaWNlKGluZGV4LCAxKVxyXG4gICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfSxcclxuICAgICAgYWN0aW9uczoge1xyXG4gICAgICAgIGdldEVudGl0eUFjdCh7IGNvbW1pdCB9LCBwYXlsb2FkKSB7XHJcbiAgICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoZnVuY3Rpb24gKHJlc29sdmUsIHJlamVjdCkge1xyXG4gICAgICAgICAgICBsZXQgcmVxUGFyYW1zID0geyAuLi5wYXlsb2FkIH1cclxuICAgICAgICAgICAgQVBJLmdldFVzZXJSZXEocmVxUGFyYW1zKS5kb25lKChyZXMpID0+IHtcclxuICAgICAgICAgICAgICBpZiAoaXNFcnJvckFQSShyZXMpKSB7XHJcbiAgICAgICAgICAgICAgICByZWplY3QocmVzLnBheWxvYWQpXHJcbiAgICAgICAgICAgICAgICByZXR1cm5cclxuICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgIGxldCB7IGFyclVzZXJEZXRhaWwgfSA9IHsgLi4ucmVzLnBheWxvYWQgfVxyXG5cclxuICAgICAgICAgICAgICBjb21taXQoJ3NldEVudGl0eScsIHsgb2JqRW50aXR5U2VsZWN0OiBhcnJVc2VyRGV0YWlsIH0pXHJcblxyXG4gICAgICAgICAgICAgIHJlc29sdmUocmVzLnBheWxvYWQpXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICB9KVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgcmVhZEVudGl0eUFjdCh7IGNvbW1pdCB9KSB7XHJcbiAgICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoZnVuY3Rpb24gKHJlc29sdmUsIHJlamVjdCkge1xyXG4gICAgICAgICAgICBsZXQgcmVxUGFyYW1zID0ge31cclxuICAgICAgICAgICAgQVBJLnJlYWRVc2VyUmVxKHJlcVBhcmFtcykuZG9uZSgocmVzKSA9PiB7XHJcbiAgICAgICAgICAgICAgaWYgKGlzRXJyb3JBUEkocmVzKSkge1xyXG4gICAgICAgICAgICAgICAgcmVqZWN0KHJlcy5wYXlsb2FkKVxyXG4gICAgICAgICAgICAgICAgcmV0dXJuXHJcbiAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICBsZXQgeyBhcnJVc2VyTGlzdCB9ID0geyAuLi5yZXMucGF5bG9hZCB9XHJcblxyXG4gICAgICAgICAgICAgIGNvbW1pdCgnc2V0RW50aXRpZXMnLCB7IGFyckVudGl0eUxpc3Q6IGFyclVzZXJMaXN0IH0pXHJcblxyXG4gICAgICAgICAgICAgIHJlc29sdmUocmVzLnBheWxvYWQpXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICB9KVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgdXBsb2FkRmlsZUFjdCh7IGNvbW1pdCB9LCBwYXlsb2FkKSB7XHJcbiAgICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoZnVuY3Rpb24gKHJlc29sdmUsIHJlamVjdCkge1xyXG4gICAgICAgICAgICBsZXQgZm9ybURhdGEgPSBuZXcgRm9ybURhdGFcclxuXHJcbiAgICAgICAgICAgIGZvcm1EYXRhLmFwcGVuZCgnY29udHJvbGxlcicsIHBheWxvYWQuY29udHJvbGxlcilcclxuICAgICAgICAgICAgZm9ybURhdGEuYXBwZW5kKCdmaWxlJywgcGF5bG9hZC5maWxlKVxyXG5cclxuICAgICAgICAgICAgQVBJLnVwbG9hZFJlcShmb3JtRGF0YSwge1xyXG4gICAgICAgICAgICAgIHByb2Nlc3NEYXRhOiBmYWxzZSxcclxuICAgICAgICAgICAgICBjb250ZW50VHlwZTogZmFsc2UsXHJcbiAgICAgICAgICAgIH0pLmRvbmUocmVzID0+IHtcclxuICAgICAgICAgICAgICBpZiAoaXNFcnJvckFQSShyZXMpKSB7XHJcbiAgICAgICAgICAgICAgICB0cmlnZ2VyTW9kYWwocmVzLm1lc3NhZ2UpXHJcblxyXG4gICAgICAgICAgICAgICAgcmVqZWN0KHJlcy5wYXlsb2FkKVxyXG4gICAgICAgICAgICAgICAgcmV0dXJuXHJcbiAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICByZXNvbHZlKHJlcy5wYXlsb2FkKVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgfSlcclxuICAgICAgICB9LFxyXG4gICAgICAgIHNldEVudGl0eUFjdCh7IGNvbW1pdCwgZ2V0dGVycyB9LCBwYXlsb2FkKSB7XHJcbiAgICAgICAgICBpZiAoIXBheWxvYWQuaGFzT3duUHJvcGVydHkoJ2luZGV4JykpXHJcbiAgICAgICAgICAgIHJldHVyblxyXG5cclxuICAgICAgICAgIGxldCB7IGluZGV4IH0gPSB7IC4uLnBheWxvYWQgfVxyXG5cclxuICAgICAgICAgIGNvbW1pdCgnc2V0RW50aXR5JywgeyBvYmpFbnRpdHlTZWxlY3Q6IGdldHRlcnMuZ2V0RW50aXR5QnlJbmRleChpbmRleClbIDAgXSB9KVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgY3JlYXRlRW50aXR5QWN0KHsgY29tbWl0LCBnZXR0ZXJzIH0sIHBheWxvYWQpIHtcclxuICAgICAgICAgIHJldHVybiBuZXcgUHJvbWlzZShmdW5jdGlvbiAocmVzb2x2ZSwgcmVqZWN0KSB7XHJcbiAgICAgICAgICAgIGxldCByZXFQYXJhbXMgPSB7IC4uLnBheWxvYWQgfVxyXG4gICAgICAgICAgICBBUEkuY3JlYXRlVXNlclJlcShyZXFQYXJhbXMpLmRvbmUoKHJlcykgPT4ge1xyXG4gICAgICAgICAgICAgIHRyaWdnZXJNb2RhbChyZXMubWVzc2FnZSlcclxuICAgICAgICAgICAgICBpZiAoaXNFcnJvckFQSShyZXMpKSB7XHJcbiAgICAgICAgICAgICAgICByZWplY3QocmVzLnBheWxvYWQpXHJcbiAgICAgICAgICAgICAgICByZXR1cm5cclxuICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgIGxldCB7IGFyclVzZXJEZXRhaWwgfSA9IHsgLi4ucmVzLnBheWxvYWQgfVxyXG5cclxuICAgICAgICAgICAgICBjb21taXQoJ3VwZGF0ZUVudGl0eScsIHtcclxuICAgICAgICAgICAgICAgIHR5cGU6ICdjcmVhdGUnLFxyXG4gICAgICAgICAgICAgICAgYXJyRW50aXR5OiBhcnJVc2VyRGV0YWlsXHJcbiAgICAgICAgICAgICAgfSlcclxuXHJcbiAgICAgICAgICAgICAgcmVzb2x2ZShyZXMucGF5bG9hZClcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgIH0pXHJcbiAgICAgICAgfSxcclxuICAgICAgICB1cGRhdGVFbnRpdHlBY3QoeyBjb21taXQsIGdldHRlcnMgfSwgcGF5bG9hZCkge1xyXG4gICAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKGZ1bmN0aW9uIChyZXNvbHZlLCByZWplY3QpIHtcclxuICAgICAgICAgICAgbGV0IHJlcVBhcmFtcyA9IHsgLi4ucGF5bG9hZCB9XHJcblxyXG4gICAgICAgICAgICBBUEkudXBkYXRlVXNlclJlcShyZXFQYXJhbXMpLmRvbmUoKHJlcykgPT4ge1xyXG4gICAgICAgICAgICAgIHRyaWdnZXJNb2RhbChyZXMubWVzc2FnZSlcclxuICAgICAgICAgICAgICBpZiAoaXNFcnJvckFQSShyZXMpKSB7XHJcbiAgICAgICAgICAgICAgICByZWplY3QocmVzLnBheWxvYWQpXHJcbiAgICAgICAgICAgICAgICByZXR1cm5cclxuICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgIGxldCB7IGFyclVzZXJEZXRhaWwgfSA9IHsgLi4ucmVzLnBheWxvYWQgfVxyXG5cclxuICAgICAgICAgICAgICBjb21taXQoJ3VwZGF0ZUVudGl0eScsIHtcclxuICAgICAgICAgICAgICAgIHR5cGU6ICd1cGRhdGUnLFxyXG4gICAgICAgICAgICAgICAgaW5kZXg6IGdldHRlcnMuZ2V0SW5kZXhCeUlEKGFyclVzZXJEZXRhaWwudXNlcl9pZCksXHJcbiAgICAgICAgICAgICAgICBhcnJFbnRpdHk6IGFyclVzZXJEZXRhaWxcclxuICAgICAgICAgICAgICB9KVxyXG5cclxuICAgICAgICAgICAgICByZXNvbHZlKHJlcy5wYXlsb2FkKVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgfSlcclxuICAgICAgICB9LFxyXG4gICAgICAgIGRlbGV0ZUVudGl0eUFjdCh7IGNvbW1pdCwgZ2V0dGVycyB9LCBwYXlsb2FkKSB7XHJcbiAgICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoZnVuY3Rpb24gKHJlc29sdmUsIHJlamVjdCkge1xyXG4gICAgICAgICAgICBsZXQgcmVxUGFyYW1zID0geyAuLi5wYXlsb2FkIH1cclxuXHJcbiAgICAgICAgICAgIEFQSS5kZWxldGVVc2VyUmVxKHJlcVBhcmFtcykuZG9uZShyZXMgPT4ge1xyXG4gICAgICAgICAgICAgIHRyaWdnZXJNb2RhbChyZXMubWVzc2FnZSlcclxuICAgICAgICAgICAgICBpZiAoaXNFcnJvckFQSShyZXMpKSB7XHJcbiAgICAgICAgICAgICAgICByZWplY3QocmVzLnBheWxvYWQpXHJcblxyXG4gICAgICAgICAgICAgICAgcmV0dXJuXHJcbiAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICBsZXQgeyBpZCB9ID0geyAuLi5yZXMucGF5bG9hZCB9XHJcblxyXG4gICAgICAgICAgICAgIGNvbnNvbGUubG9nKGlkKVxyXG5cclxuICAgICAgICAgICAgICBjb21taXQoJ3VwZGF0ZUVudGl0eScsIHtcclxuICAgICAgICAgICAgICAgIHR5cGU6ICdkZWxldGUnLFxyXG4gICAgICAgICAgICAgICAgaW5kZXg6IGdldHRlcnMuZ2V0SW5kZXhCeUlEKGlkKVxyXG4gICAgICAgICAgICAgIH0pXHJcblxyXG4gICAgICAgICAgICAgIHJlc29sdmUocmVzLnBheWxvYWQpXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICB9KVxyXG4gICAgICAgIH1cclxuICAgICAgfSxcclxuICAgICAgZ2V0dGVyczoge1xyXG4gICAgICAgIGdldEVudGl0eUJ5SW5kZXg6IHN0YXRlID0+IGluZGV4ID0+IHtcclxuICAgICAgICAgIHJldHVybiBzdGF0ZS5hcnJFbnRpdHlMaXN0LmZpbHRlcigob2JqRW50aXR5LCBjdXJyZW50SW5kZXgpID0+IGN1cnJlbnRJbmRleCA9PSBpbmRleClcclxuICAgICAgICB9LFxyXG4gICAgICAgIGdldEluZGV4QnlJRDogc3RhdGUgPT4gaWQgPT4ge1xyXG4gICAgICAgICAgbGV0IGluZGV4ID0gLTFcclxuICAgICAgICAgIF9mb3JJbihzdGF0ZS5hcnJFbnRpdHlMaXN0LCAob2JqRW50aXR5LCBjdXJyZW50SW5kZXgpID0+IHtcclxuICAgICAgICAgICAgaWYgKG9iakVudGl0eS51c2VyX2lkICE9IGlkKVxyXG4gICAgICAgICAgICAgIHJldHVyblxyXG5cclxuICAgICAgICAgICAgaW5kZXggPSBjdXJyZW50SW5kZXhcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlXHJcbiAgICAgICAgICB9KVxyXG5cclxuICAgICAgICAgIHJldHVybiBpbmRleFxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgKG5ldyBTdG9yZSgpKTtcclxuXHJcblxyXG4iLCJpbXBvcnQgeyByZW5kZXIsIHN0YXRpY1JlbmRlckZucyB9IGZyb20gXCIuL1RoZU1vZGFsRnVsbFNjcmVlbi52dWU/dnVlJnR5cGU9dGVtcGxhdGUmaWQ9MWU2OWNjNjMmXCJcbmltcG9ydCBzY3JpcHQgZnJvbSBcIi4vVGhlTW9kYWxGdWxsU2NyZWVuLnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIlxuZXhwb3J0ICogZnJvbSBcIi4vVGhlTW9kYWxGdWxsU2NyZWVuLnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIlxuXG5cbi8qIG5vcm1hbGl6ZSBjb21wb25lbnQgKi9cbmltcG9ydCBub3JtYWxpemVyIGZyb20gXCIhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3J1bnRpbWUvY29tcG9uZW50Tm9ybWFsaXplci5qc1wiXG52YXIgY29tcG9uZW50ID0gbm9ybWFsaXplcihcbiAgc2NyaXB0LFxuICByZW5kZXIsXG4gIHN0YXRpY1JlbmRlckZucyxcbiAgZmFsc2UsXG4gIG51bGwsXG4gIG51bGwsXG4gIG51bGxcbiAgXG4pXG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7XG4gIHZhciBhcGkgPSByZXF1aXJlKFwiRDpcXFxcUHJvamVjdFxcXFxzaGFyZVxcXFxvdXRzb3VjZV90aHVlbmhhXFxcXGh0bWxcXFxcc3RhdGljXFxcXGJhY2tlbmRcXFxcdjFcXFxcbm9kZV9tb2R1bGVzXFxcXHZ1ZS1ob3QtcmVsb2FkLWFwaVxcXFxkaXN0XFxcXGluZGV4LmpzXCIpXG4gIGFwaS5pbnN0YWxsKHJlcXVpcmUoJ3Z1ZScpKVxuICBpZiAoYXBpLmNvbXBhdGlibGUpIHtcbiAgICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gICAgaWYgKCFhcGkuaXNSZWNvcmRlZCgnMWU2OWNjNjMnKSkge1xuICAgICAgYXBpLmNyZWF0ZVJlY29yZCgnMWU2OWNjNjMnLCBjb21wb25lbnQub3B0aW9ucylcbiAgICB9IGVsc2Uge1xuICAgICAgYXBpLnJlbG9hZCgnMWU2OWNjNjMnLCBjb21wb25lbnQub3B0aW9ucylcbiAgICB9XG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoXCIuL1RoZU1vZGFsRnVsbFNjcmVlbi52dWU/dnVlJnR5cGU9dGVtcGxhdGUmaWQ9MWU2OWNjNjMmXCIsIGZ1bmN0aW9uICgpIHtcbiAgICAgIGFwaS5yZXJlbmRlcignMWU2OWNjNjMnLCB7XG4gICAgICAgIHJlbmRlcjogcmVuZGVyLFxuICAgICAgICBzdGF0aWNSZW5kZXJGbnM6IHN0YXRpY1JlbmRlckZuc1xuICAgICAgfSlcbiAgICB9KVxuICB9XG59XG5jb21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcInNyYy92dWUvcGFydGlhbC9nbG9iYWwvVGhlTW9kYWxGdWxsU2NyZWVuLnZ1ZVwiXG5leHBvcnQgZGVmYXVsdCBjb21wb25lbnQuZXhwb3J0cyIsImltcG9ydCBtb2QgZnJvbSBcIi0hLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2JhYmVsLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvaW5kZXguanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuL1RoZU1vZGFsRnVsbFNjcmVlbi52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCI7IGV4cG9ydCBkZWZhdWx0IG1vZDsgZXhwb3J0ICogZnJvbSBcIi0hLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2JhYmVsLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvaW5kZXguanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuL1RoZU1vZGFsRnVsbFNjcmVlbi52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCIiLCJleHBvcnQgKiBmcm9tIFwiLSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvbG9hZGVycy90ZW1wbGF0ZUxvYWRlci5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9pbmRleC5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4vVGhlTW9kYWxGdWxsU2NyZWVuLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD0xZTY5Y2M2MyZcIiIsImltcG9ydCB7IHJlbmRlciwgc3RhdGljUmVuZGVyRm5zIH0gZnJvbSBcIi4vQXBwLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD05YmUwM2Q2NiZcIlxuaW1wb3J0IHNjcmlwdCBmcm9tIFwiLi9BcHAudnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiXG5leHBvcnQgKiBmcm9tIFwiLi9BcHAudnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiXG5cblxuLyogbm9ybWFsaXplIGNvbXBvbmVudCAqL1xuaW1wb3J0IG5vcm1hbGl6ZXIgZnJvbSBcIiEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvcnVudGltZS9jb21wb25lbnROb3JtYWxpemVyLmpzXCJcbnZhciBjb21wb25lbnQgPSBub3JtYWxpemVyKFxuICBzY3JpcHQsXG4gIHJlbmRlcixcbiAgc3RhdGljUmVuZGVyRm5zLFxuICBmYWxzZSxcbiAgbnVsbCxcbiAgbnVsbCxcbiAgbnVsbFxuICBcbilcblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHtcbiAgdmFyIGFwaSA9IHJlcXVpcmUoXCJEOlxcXFxQcm9qZWN0XFxcXHNoYXJlXFxcXG91dHNvdWNlX3RodWVuaGFcXFxcaHRtbFxcXFxzdGF0aWNcXFxcYmFja2VuZFxcXFx2MVxcXFxub2RlX21vZHVsZXNcXFxcdnVlLWhvdC1yZWxvYWQtYXBpXFxcXGRpc3RcXFxcaW5kZXguanNcIilcbiAgYXBpLmluc3RhbGwocmVxdWlyZSgndnVlJykpXG4gIGlmIChhcGkuY29tcGF0aWJsZSkge1xuICAgIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgICBpZiAoIWFwaS5pc1JlY29yZGVkKCc5YmUwM2Q2NicpKSB7XG4gICAgICBhcGkuY3JlYXRlUmVjb3JkKCc5YmUwM2Q2NicsIGNvbXBvbmVudC5vcHRpb25zKVxuICAgIH0gZWxzZSB7XG4gICAgICBhcGkucmVsb2FkKCc5YmUwM2Q2NicsIGNvbXBvbmVudC5vcHRpb25zKVxuICAgIH1cbiAgICBtb2R1bGUuaG90LmFjY2VwdChcIi4vQXBwLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD05YmUwM2Q2NiZcIiwgZnVuY3Rpb24gKCkge1xuICAgICAgYXBpLnJlcmVuZGVyKCc5YmUwM2Q2NicsIHtcbiAgICAgICAgcmVuZGVyOiByZW5kZXIsXG4gICAgICAgIHN0YXRpY1JlbmRlckZuczogc3RhdGljUmVuZGVyRm5zXG4gICAgICB9KVxuICAgIH0pXG4gIH1cbn1cbmNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwic3JjL3Z1ZS9wYXJ0aWFsL3VzZXIvQXBwLnZ1ZVwiXG5leHBvcnQgZGVmYXVsdCBjb21wb25lbnQuZXhwb3J0cyIsImltcG9ydCBtb2QgZnJvbSBcIi0hLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2JhYmVsLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvaW5kZXguanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuL0FwcC52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCI7IGV4cG9ydCBkZWZhdWx0IG1vZDsgZXhwb3J0ICogZnJvbSBcIi0hLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2JhYmVsLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvaW5kZXguanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuL0FwcC52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCIiLCJleHBvcnQgKiBmcm9tIFwiLSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvbG9hZGVycy90ZW1wbGF0ZUxvYWRlci5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9pbmRleC5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4vQXBwLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD05YmUwM2Q2NiZcIiIsImltcG9ydCB7IHJlbmRlciwgc3RhdGljUmVuZGVyRm5zIH0gZnJvbSBcIi4vVXNlckFkZFNlY3Rpb24udnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPWRmZTk4MjlhJlwiXG5pbXBvcnQgc2NyaXB0IGZyb20gXCIuL1VzZXJBZGRTZWN0aW9uLnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIlxuZXhwb3J0ICogZnJvbSBcIi4vVXNlckFkZFNlY3Rpb24udnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiXG5cblxuLyogbm9ybWFsaXplIGNvbXBvbmVudCAqL1xuaW1wb3J0IG5vcm1hbGl6ZXIgZnJvbSBcIiEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvcnVudGltZS9jb21wb25lbnROb3JtYWxpemVyLmpzXCJcbnZhciBjb21wb25lbnQgPSBub3JtYWxpemVyKFxuICBzY3JpcHQsXG4gIHJlbmRlcixcbiAgc3RhdGljUmVuZGVyRm5zLFxuICBmYWxzZSxcbiAgbnVsbCxcbiAgbnVsbCxcbiAgbnVsbFxuICBcbilcblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHtcbiAgdmFyIGFwaSA9IHJlcXVpcmUoXCJEOlxcXFxQcm9qZWN0XFxcXHNoYXJlXFxcXG91dHNvdWNlX3RodWVuaGFcXFxcaHRtbFxcXFxzdGF0aWNcXFxcYmFja2VuZFxcXFx2MVxcXFxub2RlX21vZHVsZXNcXFxcdnVlLWhvdC1yZWxvYWQtYXBpXFxcXGRpc3RcXFxcaW5kZXguanNcIilcbiAgYXBpLmluc3RhbGwocmVxdWlyZSgndnVlJykpXG4gIGlmIChhcGkuY29tcGF0aWJsZSkge1xuICAgIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgICBpZiAoIWFwaS5pc1JlY29yZGVkKCdkZmU5ODI5YScpKSB7XG4gICAgICBhcGkuY3JlYXRlUmVjb3JkKCdkZmU5ODI5YScsIGNvbXBvbmVudC5vcHRpb25zKVxuICAgIH0gZWxzZSB7XG4gICAgICBhcGkucmVsb2FkKCdkZmU5ODI5YScsIGNvbXBvbmVudC5vcHRpb25zKVxuICAgIH1cbiAgICBtb2R1bGUuaG90LmFjY2VwdChcIi4vVXNlckFkZFNlY3Rpb24udnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPWRmZTk4MjlhJlwiLCBmdW5jdGlvbiAoKSB7XG4gICAgICBhcGkucmVyZW5kZXIoJ2RmZTk4MjlhJywge1xuICAgICAgICByZW5kZXI6IHJlbmRlcixcbiAgICAgICAgc3RhdGljUmVuZGVyRm5zOiBzdGF0aWNSZW5kZXJGbnNcbiAgICAgIH0pXG4gICAgfSlcbiAgfVxufVxuY29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJzcmMvdnVlL3BhcnRpYWwvdXNlci9Vc2VyQWRkU2VjdGlvbi52dWVcIlxuZXhwb3J0IGRlZmF1bHQgY29tcG9uZW50LmV4cG9ydHMiLCJpbXBvcnQgbW9kIGZyb20gXCItIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9iYWJlbC1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2luZGV4LmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi9Vc2VyQWRkU2VjdGlvbi52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCI7IGV4cG9ydCBkZWZhdWx0IG1vZDsgZXhwb3J0ICogZnJvbSBcIi0hLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2JhYmVsLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvaW5kZXguanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuL1VzZXJBZGRTZWN0aW9uLnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIiIsImV4cG9ydCAqIGZyb20gXCItIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9sb2FkZXJzL3RlbXBsYXRlTG9hZGVyLmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2luZGV4LmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi9Vc2VyQWRkU2VjdGlvbi52dWU/dnVlJnR5cGU9dGVtcGxhdGUmaWQ9ZGZlOTgyOWEmXCIiLCJpbXBvcnQgeyByZW5kZXIsIHN0YXRpY1JlbmRlckZucyB9IGZyb20gXCIuL1VzZXJFZGl0U2VjdGlvbi52dWU/dnVlJnR5cGU9dGVtcGxhdGUmaWQ9NTU5NDMyNWMmXCJcbmltcG9ydCBzY3JpcHQgZnJvbSBcIi4vVXNlckVkaXRTZWN0aW9uLnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIlxuZXhwb3J0ICogZnJvbSBcIi4vVXNlckVkaXRTZWN0aW9uLnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIlxuXG5cbi8qIG5vcm1hbGl6ZSBjb21wb25lbnQgKi9cbmltcG9ydCBub3JtYWxpemVyIGZyb20gXCIhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3J1bnRpbWUvY29tcG9uZW50Tm9ybWFsaXplci5qc1wiXG52YXIgY29tcG9uZW50ID0gbm9ybWFsaXplcihcbiAgc2NyaXB0LFxuICByZW5kZXIsXG4gIHN0YXRpY1JlbmRlckZucyxcbiAgZmFsc2UsXG4gIG51bGwsXG4gIG51bGwsXG4gIG51bGxcbiAgXG4pXG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7XG4gIHZhciBhcGkgPSByZXF1aXJlKFwiRDpcXFxcUHJvamVjdFxcXFxzaGFyZVxcXFxvdXRzb3VjZV90aHVlbmhhXFxcXGh0bWxcXFxcc3RhdGljXFxcXGJhY2tlbmRcXFxcdjFcXFxcbm9kZV9tb2R1bGVzXFxcXHZ1ZS1ob3QtcmVsb2FkLWFwaVxcXFxkaXN0XFxcXGluZGV4LmpzXCIpXG4gIGFwaS5pbnN0YWxsKHJlcXVpcmUoJ3Z1ZScpKVxuICBpZiAoYXBpLmNvbXBhdGlibGUpIHtcbiAgICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gICAgaWYgKCFhcGkuaXNSZWNvcmRlZCgnNTU5NDMyNWMnKSkge1xuICAgICAgYXBpLmNyZWF0ZVJlY29yZCgnNTU5NDMyNWMnLCBjb21wb25lbnQub3B0aW9ucylcbiAgICB9IGVsc2Uge1xuICAgICAgYXBpLnJlbG9hZCgnNTU5NDMyNWMnLCBjb21wb25lbnQub3B0aW9ucylcbiAgICB9XG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoXCIuL1VzZXJFZGl0U2VjdGlvbi52dWU/dnVlJnR5cGU9dGVtcGxhdGUmaWQ9NTU5NDMyNWMmXCIsIGZ1bmN0aW9uICgpIHtcbiAgICAgIGFwaS5yZXJlbmRlcignNTU5NDMyNWMnLCB7XG4gICAgICAgIHJlbmRlcjogcmVuZGVyLFxuICAgICAgICBzdGF0aWNSZW5kZXJGbnM6IHN0YXRpY1JlbmRlckZuc1xuICAgICAgfSlcbiAgICB9KVxuICB9XG59XG5jb21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcInNyYy92dWUvcGFydGlhbC91c2VyL1VzZXJFZGl0U2VjdGlvbi52dWVcIlxuZXhwb3J0IGRlZmF1bHQgY29tcG9uZW50LmV4cG9ydHMiLCJpbXBvcnQgbW9kIGZyb20gXCItIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9iYWJlbC1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2luZGV4LmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi9Vc2VyRWRpdFNlY3Rpb24udnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiOyBleHBvcnQgZGVmYXVsdCBtb2Q7IGV4cG9ydCAqIGZyb20gXCItIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9iYWJlbC1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2luZGV4LmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi9Vc2VyRWRpdFNlY3Rpb24udnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiIiwiZXhwb3J0ICogZnJvbSBcIi0hLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2xvYWRlcnMvdGVtcGxhdGVMb2FkZXIuanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvaW5kZXguanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuL1VzZXJFZGl0U2VjdGlvbi52dWU/dnVlJnR5cGU9dGVtcGxhdGUmaWQ9NTU5NDMyNWMmXCIiLCJpbXBvcnQgeyByZW5kZXIsIHN0YXRpY1JlbmRlckZucyB9IGZyb20gXCIuL1VzZXJJbmRleFNlY3Rpb24udnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPTE0NmE2N2JjJlwiXG5pbXBvcnQgc2NyaXB0IGZyb20gXCIuL1VzZXJJbmRleFNlY3Rpb24udnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiXG5leHBvcnQgKiBmcm9tIFwiLi9Vc2VySW5kZXhTZWN0aW9uLnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIlxuXG5cbi8qIG5vcm1hbGl6ZSBjb21wb25lbnQgKi9cbmltcG9ydCBub3JtYWxpemVyIGZyb20gXCIhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3J1bnRpbWUvY29tcG9uZW50Tm9ybWFsaXplci5qc1wiXG52YXIgY29tcG9uZW50ID0gbm9ybWFsaXplcihcbiAgc2NyaXB0LFxuICByZW5kZXIsXG4gIHN0YXRpY1JlbmRlckZucyxcbiAgZmFsc2UsXG4gIG51bGwsXG4gIG51bGwsXG4gIG51bGxcbiAgXG4pXG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7XG4gIHZhciBhcGkgPSByZXF1aXJlKFwiRDpcXFxcUHJvamVjdFxcXFxzaGFyZVxcXFxvdXRzb3VjZV90aHVlbmhhXFxcXGh0bWxcXFxcc3RhdGljXFxcXGJhY2tlbmRcXFxcdjFcXFxcbm9kZV9tb2R1bGVzXFxcXHZ1ZS1ob3QtcmVsb2FkLWFwaVxcXFxkaXN0XFxcXGluZGV4LmpzXCIpXG4gIGFwaS5pbnN0YWxsKHJlcXVpcmUoJ3Z1ZScpKVxuICBpZiAoYXBpLmNvbXBhdGlibGUpIHtcbiAgICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gICAgaWYgKCFhcGkuaXNSZWNvcmRlZCgnMTQ2YTY3YmMnKSkge1xuICAgICAgYXBpLmNyZWF0ZVJlY29yZCgnMTQ2YTY3YmMnLCBjb21wb25lbnQub3B0aW9ucylcbiAgICB9IGVsc2Uge1xuICAgICAgYXBpLnJlbG9hZCgnMTQ2YTY3YmMnLCBjb21wb25lbnQub3B0aW9ucylcbiAgICB9XG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoXCIuL1VzZXJJbmRleFNlY3Rpb24udnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPTE0NmE2N2JjJlwiLCBmdW5jdGlvbiAoKSB7XG4gICAgICBhcGkucmVyZW5kZXIoJzE0NmE2N2JjJywge1xuICAgICAgICByZW5kZXI6IHJlbmRlcixcbiAgICAgICAgc3RhdGljUmVuZGVyRm5zOiBzdGF0aWNSZW5kZXJGbnNcbiAgICAgIH0pXG4gICAgfSlcbiAgfVxufVxuY29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJzcmMvdnVlL3BhcnRpYWwvdXNlci9Vc2VySW5kZXhTZWN0aW9uLnZ1ZVwiXG5leHBvcnQgZGVmYXVsdCBjb21wb25lbnQuZXhwb3J0cyIsImltcG9ydCBtb2QgZnJvbSBcIi0hLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2JhYmVsLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvaW5kZXguanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuL1VzZXJJbmRleFNlY3Rpb24udnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiOyBleHBvcnQgZGVmYXVsdCBtb2Q7IGV4cG9ydCAqIGZyb20gXCItIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9iYWJlbC1sb2FkZXIvbGliL2luZGV4LmpzPz9yZWYtLTEhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2luZGV4LmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi9Vc2VySW5kZXhTZWN0aW9uLnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIiIsImV4cG9ydCAqIGZyb20gXCItIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9sb2FkZXJzL3RlbXBsYXRlTG9hZGVyLmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2luZGV4LmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi9Vc2VySW5kZXhTZWN0aW9uLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD0xNDZhNjdiYyZcIiIsImltcG9ydCB7IHJlbmRlciwgc3RhdGljUmVuZGVyRm5zIH0gZnJvbSBcIi4vVXNlclRhYmxlLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD03NWIzMmRlZiZcIlxuaW1wb3J0IHNjcmlwdCBmcm9tIFwiLi9Vc2VyVGFibGUudnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiXG5leHBvcnQgKiBmcm9tIFwiLi9Vc2VyVGFibGUudnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiXG5cblxuLyogbm9ybWFsaXplIGNvbXBvbmVudCAqL1xuaW1wb3J0IG5vcm1hbGl6ZXIgZnJvbSBcIiEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvcnVudGltZS9jb21wb25lbnROb3JtYWxpemVyLmpzXCJcbnZhciBjb21wb25lbnQgPSBub3JtYWxpemVyKFxuICBzY3JpcHQsXG4gIHJlbmRlcixcbiAgc3RhdGljUmVuZGVyRm5zLFxuICBmYWxzZSxcbiAgbnVsbCxcbiAgbnVsbCxcbiAgbnVsbFxuICBcbilcblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHtcbiAgdmFyIGFwaSA9IHJlcXVpcmUoXCJEOlxcXFxQcm9qZWN0XFxcXHNoYXJlXFxcXG91dHNvdWNlX3RodWVuaGFcXFxcaHRtbFxcXFxzdGF0aWNcXFxcYmFja2VuZFxcXFx2MVxcXFxub2RlX21vZHVsZXNcXFxcdnVlLWhvdC1yZWxvYWQtYXBpXFxcXGRpc3RcXFxcaW5kZXguanNcIilcbiAgYXBpLmluc3RhbGwocmVxdWlyZSgndnVlJykpXG4gIGlmIChhcGkuY29tcGF0aWJsZSkge1xuICAgIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgICBpZiAoIWFwaS5pc1JlY29yZGVkKCc3NWIzMmRlZicpKSB7XG4gICAgICBhcGkuY3JlYXRlUmVjb3JkKCc3NWIzMmRlZicsIGNvbXBvbmVudC5vcHRpb25zKVxuICAgIH0gZWxzZSB7XG4gICAgICBhcGkucmVsb2FkKCc3NWIzMmRlZicsIGNvbXBvbmVudC5vcHRpb25zKVxuICAgIH1cbiAgICBtb2R1bGUuaG90LmFjY2VwdChcIi4vVXNlclRhYmxlLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD03NWIzMmRlZiZcIiwgZnVuY3Rpb24gKCkge1xuICAgICAgYXBpLnJlcmVuZGVyKCc3NWIzMmRlZicsIHtcbiAgICAgICAgcmVuZGVyOiByZW5kZXIsXG4gICAgICAgIHN0YXRpY1JlbmRlckZuczogc3RhdGljUmVuZGVyRm5zXG4gICAgICB9KVxuICAgIH0pXG4gIH1cbn1cbmNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwic3JjL3Z1ZS9wYXJ0aWFsL3VzZXIvVXNlclRhYmxlLnZ1ZVwiXG5leHBvcnQgZGVmYXVsdCBjb21wb25lbnQuZXhwb3J0cyIsImltcG9ydCBtb2QgZnJvbSBcIi0hLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2JhYmVsLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvaW5kZXguanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuL1VzZXJUYWJsZS52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCI7IGV4cG9ydCBkZWZhdWx0IG1vZDsgZXhwb3J0ICogZnJvbSBcIi0hLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2JhYmVsLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvaW5kZXguanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuL1VzZXJUYWJsZS52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCIiLCJleHBvcnQgKiBmcm9tIFwiLSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvbG9hZGVycy90ZW1wbGF0ZUxvYWRlci5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9pbmRleC5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4vVXNlclRhYmxlLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD03NWIzMmRlZiZcIiIsImltcG9ydCB7IHJlbmRlciwgc3RhdGljUmVuZGVyRm5zIH0gZnJvbSBcIi4vVXNlclRhYmxlUm93LnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD00MmMxYjg5YiZcIlxuaW1wb3J0IHNjcmlwdCBmcm9tIFwiLi9Vc2VyVGFibGVSb3cudnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiXG5leHBvcnQgKiBmcm9tIFwiLi9Vc2VyVGFibGVSb3cudnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiXG5cblxuLyogbm9ybWFsaXplIGNvbXBvbmVudCAqL1xuaW1wb3J0IG5vcm1hbGl6ZXIgZnJvbSBcIiEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvcnVudGltZS9jb21wb25lbnROb3JtYWxpemVyLmpzXCJcbnZhciBjb21wb25lbnQgPSBub3JtYWxpemVyKFxuICBzY3JpcHQsXG4gIHJlbmRlcixcbiAgc3RhdGljUmVuZGVyRm5zLFxuICBmYWxzZSxcbiAgbnVsbCxcbiAgbnVsbCxcbiAgbnVsbFxuICBcbilcblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHtcbiAgdmFyIGFwaSA9IHJlcXVpcmUoXCJEOlxcXFxQcm9qZWN0XFxcXHNoYXJlXFxcXG91dHNvdWNlX3RodWVuaGFcXFxcaHRtbFxcXFxzdGF0aWNcXFxcYmFja2VuZFxcXFx2MVxcXFxub2RlX21vZHVsZXNcXFxcdnVlLWhvdC1yZWxvYWQtYXBpXFxcXGRpc3RcXFxcaW5kZXguanNcIilcbiAgYXBpLmluc3RhbGwocmVxdWlyZSgndnVlJykpXG4gIGlmIChhcGkuY29tcGF0aWJsZSkge1xuICAgIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgICBpZiAoIWFwaS5pc1JlY29yZGVkKCc0MmMxYjg5YicpKSB7XG4gICAgICBhcGkuY3JlYXRlUmVjb3JkKCc0MmMxYjg5YicsIGNvbXBvbmVudC5vcHRpb25zKVxuICAgIH0gZWxzZSB7XG4gICAgICBhcGkucmVsb2FkKCc0MmMxYjg5YicsIGNvbXBvbmVudC5vcHRpb25zKVxuICAgIH1cbiAgICBtb2R1bGUuaG90LmFjY2VwdChcIi4vVXNlclRhYmxlUm93LnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD00MmMxYjg5YiZcIiwgZnVuY3Rpb24gKCkge1xuICAgICAgYXBpLnJlcmVuZGVyKCc0MmMxYjg5YicsIHtcbiAgICAgICAgcmVuZGVyOiByZW5kZXIsXG4gICAgICAgIHN0YXRpY1JlbmRlckZuczogc3RhdGljUmVuZGVyRm5zXG4gICAgICB9KVxuICAgIH0pXG4gIH1cbn1cbmNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwic3JjL3Z1ZS9wYXJ0aWFsL3VzZXIvVXNlclRhYmxlUm93LnZ1ZVwiXG5leHBvcnQgZGVmYXVsdCBjb21wb25lbnQuZXhwb3J0cyIsImltcG9ydCBtb2QgZnJvbSBcIi0hLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2JhYmVsLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvaW5kZXguanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuL1VzZXJUYWJsZVJvdy52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCI7IGV4cG9ydCBkZWZhdWx0IG1vZDsgZXhwb3J0ICogZnJvbSBcIi0hLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2JhYmVsLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvaW5kZXguanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuL1VzZXJUYWJsZVJvdy52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCIiLCJleHBvcnQgKiBmcm9tIFwiLSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvbG9hZGVycy90ZW1wbGF0ZUxvYWRlci5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9pbmRleC5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4vVXNlclRhYmxlUm93LnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD00MmMxYjg5YiZcIiJdLCJzb3VyY2VSb290IjoiIn0=
