/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(Object.prototype.hasOwnProperty.call(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"role": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push([6,"vendor"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/global/TheModalFullScreen.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--1!./node_modules/vue-loader/lib??vue-loader-options!./src/vue/partial/global/TheModalFullScreen.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function($) {/* harmony import */ var $jsBasePath_define__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! $jsBasePath/define */ "./src/js/base/define.js");
/* harmony import */ var $jsBasePath_EventBus__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! $jsBasePath/EventBus */ "./src/js/base/EventBus.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'modal',
  components: {},
  data: function data() {
    return {
      title: 'THÔNG BÁO !',
      content: 'We are lucky to live in a glorious age that gives us everything as a human race.'
    };
  },
  computed: {},
  created: function created() {
    var self = this;
    $jsBasePath_EventBus__WEBPACK_IMPORTED_MODULE_1__["default"].on($jsBasePath_define__WEBPACK_IMPORTED_MODULE_0__["EVENT_BUS_KEY"].UPDATE_CONTENT_MODAL, function (content) {
      self.content = content;
    });
    $(document).on('keyup', function (e) {
      var keyCode = e.which;
      if (keyCode != 13 && keyCode != 27) return;
      var flag = $('#modal-fs').hasClass('show');
      if (!flag) return;
      $('#modal-fs-btn').trigger('click');
      $jsBasePath_EventBus__WEBPACK_IMPORTED_MODULE_1__["default"].emit($jsBasePath_define__WEBPACK_IMPORTED_MODULE_0__["EVENT_BUS_KEY"].CONFIRM_MODAL, true);
    });
  },
  methods: {
    confirm: function confirm() {
      $('#modal-fs-btn').trigger('click');
      $jsBasePath_EventBus__WEBPACK_IMPORTED_MODULE_1__["default"].emit($jsBasePath_define__WEBPACK_IMPORTED_MODULE_0__["EVENT_BUS_KEY"].CONFIRM_MODAL, true);
    }
  }
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js-exposed")))

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/role/App.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--1!./node_modules/vue-loader/lib??vue-loader-options!./src/vue/partial/role/App.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'role-app',
  components: {},
  data: function data() {
    return {};
  },
  computed: {},
  created: function created() {},
  methods: {}
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/role/RoleAddSection.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--1!./node_modules/vue-loader/lib??vue-loader-options!./src/vue/partial/role/RoleAddSection.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vee_validate__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vee-validate */ "./node_modules/vee-validate/dist/vee-validate.esm.js");
/* harmony import */ var vee_validate_dist_rules__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vee-validate/dist/rules */ "./node_modules/vee-validate/dist/rules.js");
/* harmony import */ var $jsBasePath_define__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! $jsBasePath/define */ "./src/js/base/define.js");
/* harmony import */ var $jsBasePath_EventBus__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! $jsBasePath/EventBus */ "./src/js/base/EventBus.js");
/* harmony import */ var $jsCommonPath_util_VeeValidateMessage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! $jsCommonPath/util/VeeValidateMessage */ "./src/js/common/util/VeeValidateMessage.js");
/* harmony import */ var $jsCommonPath_util_SafeUtil__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! $jsCommonPath/util/SafeUtil */ "./src/js/common/util/SafeUtil.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//







var objMessage = $jsCommonPath_util_VeeValidateMessage__WEBPACK_IMPORTED_MODULE_5__["default"].messages;
Object(vee_validate__WEBPACK_IMPORTED_MODULE_1__["extend"])('required', _objectSpread({}, vee_validate_dist_rules__WEBPACK_IMPORTED_MODULE_2__["required"], {
  message: objMessage.required
}));
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'role-add-section',
  components: {
    'validation-observer': vee_validate__WEBPACK_IMPORTED_MODULE_1__["ValidationObserver"],
    'validation-provider': vee_validate__WEBPACK_IMPORTED_MODULE_1__["ValidationProvider"]
  },
  data: function data() {
    return {
      url: {
        resourceURL: CDATA.url.resourceURL
      },
      flag: {
        isValidating: false,
        isUpdating: false
      },
      frmData: {}
    };
  },
  computed: {
    profileImage: function profileImage() {
      if (Object(lodash__WEBPACK_IMPORTED_MODULE_0__["isEmpty"])(this.frmData.profile_image)) return this.url.resourceURL + '/global/image/notFound.png';
      return this.frmData.profile_image;
    },
    objEntitySelect: function objEntitySelect() {
      return this.$store.state.entity.objEntitySelect;
    }
  },
  created: function created() {
    var _this = this;

    var self = this;
    $jsBasePath_EventBus__WEBPACK_IMPORTED_MODULE_4__["default"].off($jsBasePath_define__WEBPACK_IMPORTED_MODULE_3__["EVENT_BUS_KEY"].CONFIRM_MODAL);
    $jsBasePath_EventBus__WEBPACK_IMPORTED_MODULE_4__["default"].on($jsBasePath_define__WEBPACK_IMPORTED_MODULE_3__["EVENT_BUS_KEY"].CONFIRM_MODAL, function (payload) {
      _this.$store.commit('resetEntity');

      self.flag.isValidating = false;
      self.flag.isUpdating = false;
      _this.frmData = _this.objEntitySelect;
    });
    this.$store.commit('resetEntity');
    this.frmData = {...this.objEntitySelect};
  },
  methods: {
    goRead: function goRead() {
      this.$router.push({
        name: 'role:index'
      });
    },
    goCreate: function goCreate() {
      this.$router.push({
        name: 'role:add',
        params: {
          page: 1
        }
      });
    },
    back: function back() {
      return this.$router.back();
    },
    update: function update(invalid) {
      this.flag.isValidating = true;

      if (invalid) {
        this.$refs.observer.validate();
        return;
      }

      this.flag.isUpdating = true;
      this.$store.dispatch('createEntityAct', this.frmData);
    },
    upload: function upload(e) {
      var _this2 = this;

      var file = e.target.files[0];
      this.$store.dispatch('uploadFileAct', {
        controller: CDATA.common.controller,
        file: file
      }).then(function (payload) {
        var objImage = payload.arrImage[0];

        if (Object(lodash__WEBPACK_IMPORTED_MODULE_0__["isEmpty"])(objImage)) {
          return;
        }

        _this2.frmData.profile_image = objImage.image;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/role/RoleEditSection.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--1!./node_modules/vue-loader/lib??vue-loader-options!./src/vue/partial/role/RoleEditSection.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vee_validate__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vee-validate */ "./node_modules/vee-validate/dist/vee-validate.esm.js");
/* harmony import */ var vee_validate_dist_rules__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vee-validate/dist/rules */ "./node_modules/vee-validate/dist/rules.js");
/* harmony import */ var $jsBasePath_define__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! $jsBasePath/define */ "./src/js/base/define.js");
/* harmony import */ var $jsBasePath_EventBus__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! $jsBasePath/EventBus */ "./src/js/base/EventBus.js");
/* harmony import */ var $jsCommonPath_util_VeeValidateMessage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! $jsCommonPath/util/VeeValidateMessage */ "./src/js/common/util/VeeValidateMessage.js");
/* harmony import */ var $jsCommonPath_util_SafeUtil__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! $jsCommonPath/util/SafeUtil */ "./src/js/common/util/SafeUtil.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//







var objMessage = $jsCommonPath_util_VeeValidateMessage__WEBPACK_IMPORTED_MODULE_5__["default"].messages;
Object(vee_validate__WEBPACK_IMPORTED_MODULE_1__["extend"])('required', _objectSpread({}, vee_validate_dist_rules__WEBPACK_IMPORTED_MODULE_2__["required"], {
  message: objMessage.required
}));
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'role-add-section',
  components: {
    'validation-observer': vee_validate__WEBPACK_IMPORTED_MODULE_1__["ValidationObserver"],
    'validation-provider': vee_validate__WEBPACK_IMPORTED_MODULE_1__["ValidationProvider"]
  },
  data: function data() {
    return {
      url: {
        resourceURL: CDATA.url.resourceURL
      },
      flag: {
        isValidating: false,
        isUpdating: false
      },
      frmData: {}
    };
  },
  computed: {
    profileImage: function profileImage() {
      if (Object(lodash__WEBPACK_IMPORTED_MODULE_0__["isEmpty"])(this.frmData.profile_image)) return this.url.resourceURL + '/global/image/notFound.png';
      return this.frmData.profile_image;
    },
    objEntitySelect: function objEntitySelect() {
      return this.$store.state.entity.objEntitySelect;
    }
  },
  created: function created() {
    var _this = this;

    var self = this;
    $jsBasePath_EventBus__WEBPACK_IMPORTED_MODULE_4__["default"].off($jsBasePath_define__WEBPACK_IMPORTED_MODULE_3__["EVENT_BUS_KEY"].CONFIRM_MODAL);
    $jsBasePath_EventBus__WEBPACK_IMPORTED_MODULE_4__["default"].on($jsBasePath_define__WEBPACK_IMPORTED_MODULE_3__["EVENT_BUS_KEY"].CONFIRM_MODAL, function (payload) {
      self.flag.isValidating = false;
      self.flag.isUpdating = false;
      _this.frmData = _this.objEntitySelect;
      _this.frmData.password = null;
    });

    if (!this.$store.state.flag.isRead) {
      var _this$$route$params = this.$route.params,
          id = _this$$route$params.id,
          page = _this$$route$params.page;
      if (Object($jsCommonPath_util_SafeUtil__WEBPACK_IMPORTED_MODULE_6__["_int"])(id) <= 0) return;
      this.get(id);
    } else {
      this.frmData = {...this.objEntitySelect};
    }
  },
  methods: {
    goRead: function goRead() {
      this.$router.push({
        name: 'role:index'
      });
    },
    goCreate: function goCreate() {
      this.$router.push({
        name: 'role:add',
        params: {
          page: 1
        }
      });
    },
    back: function back() {
      return this.$router.back();
    },
    get: function get(id) {
      var _this2 = this;

      this.$store.dispatch('getEntityAct', {
        id: id
      }).then(function (payload) {
        _this2.frmData = _this2.objEntitySelect;
      });
    },
    update: function update(invalid) {
      this.flag.isValidating = true;

      if (invalid) {
        this.$refs.observer.validate();
        return;
      }

      this.flag.isUpdating = true;
      var id = this.objEntitySelect.role_id;
      this.$store.dispatch('updateEntityAct', _objectSpread({
        id: id
      }, this.frmData));
    },
    upload: function upload(e) {
      var _this3 = this;

      var file = e.target.files[0];
      this.$store.dispatch('uploadFileAct', {
        controller: CDATA.common.controller,
        file: file
      }).then(function (payload) {
        var objImage = payload.arrImage[0];

        if (Object(lodash__WEBPACK_IMPORTED_MODULE_0__["isEmpty"])(objImage)) {
          return;
        }

        _this3.frmData.profile_image = objImage.image;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/role/RoleIndexSection.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--1!./node_modules/vue-loader/lib??vue-loader-options!./src/vue/partial/role/RoleIndexSection.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var $vuePartialPath_role_RoleTable__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! $vuePartialPath/role/RoleTable */ "./src/vue/partial/role/RoleTable.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'role-index-section',
  components: {
    'role-table': $vuePartialPath_role_RoleTable__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {};
  },
  computed: {},
  created: function created() {
    if (!this.$store.state.flag.isRead) {
      this.$store.commit('updateFlag', {
        flag: 'isRead',
        value: true
      });
      this.$store.dispatch('readEntityAct');
    }
  },
  methods: {
    goRead: function goRead() {
      this.$router.push({
        name: 'role:index'
      });
    },
    goCreate: function goCreate() {
      this.$router.push({
        name: 'role:add',
        params: {
          page: 1
        }
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/role/RoleTable.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--1!./node_modules/vue-loader/lib??vue-loader-options!./src/vue/partial/role/RoleTable.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var $vuePartialPath_role_RoleTableRow__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! $vuePartialPath/role/RoleTableRow */ "./src/vue/partial/role/RoleTableRow.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'role-table',
  components: {
    'role-table-row': $vuePartialPath_role_RoleTableRow__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {};
  },
  computed: {
    arrEntityList: function arrEntityList() {
      return this.$store.state.entity.arrEntityList;
    }
  },
  created: function created() {},
  methods: {}
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/role/RoleTableRow.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--1!./node_modules/vue-loader/lib??vue-loader-options!./src/vue/partial/role/RoleTableRow.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var $jsCommonPath_util_SafeUtil__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! $jsCommonPath/util/SafeUtil */ "./src/js/common/util/SafeUtil.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'role-table-row',
  props: {
    index: {
      default: {},
      type: Number
    },
    objEntity: {
      default: {},
      type: Object
    }
  },
  components: {},
  data: function data() {
    return {
      url: {
        resourceURL: CDATA.url.resourceURL
      },
      flag: {
        isDeleting: false
      }
    };
  },
  computed: {
    profileImage: function profileImage() {
      if (Object($jsCommonPath_util_SafeUtil__WEBPACK_IMPORTED_MODULE_0__["_isEmptyString"])(this.objEntity.profile_image)) return this.url.resourceURL + '/global/image/notFound.png';
      return this.objEntity.profile_image;
    }
  },
  created: function created() {},
  methods: {
    goUpdate: function goUpdate() {
      this.$store.dispatch('setEntityAct', {
        index: this.index
      });
      this.$router.push({
        name: 'role:edit',
        params: {
          id: this.objEntity.role_id,
          page: 1
        }
      });
    },
    remove: function remove(id) {
      var _this = this;

      if (!id) return;
      this.flag.isDeleting = true;
      this.$store.dispatch('deleteEntityAct', {
        id: id
      }).then(function (res) {
        _this.flag.isDeleting = false;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vee-validate/dist/rules.js":
/*!*************************************************!*\
  !*** ./node_modules/vee-validate/dist/rules.js ***!
  \*************************************************/
/*! exports provided: alpha, alpha_dash, alpha_num, alpha_spaces, between, confirmed, digits, dimensions, email, excluded, ext, image, integer, is, is_not, length, max, max_value, mimes, min, min_value, numeric, oneOf, regex, required, required_if, size */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "alpha", function() { return alpha$1; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "alpha_dash", function() { return alpha_dash; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "alpha_num", function() { return alpha_num; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "alpha_spaces", function() { return alpha_spaces; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "between", function() { return between; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "confirmed", function() { return confirmed; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "digits", function() { return digits; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "dimensions", function() { return dimensions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "email", function() { return email; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "excluded", function() { return excluded; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ext", function() { return ext; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "image", function() { return image; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "integer", function() { return integer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "is", function() { return is; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "is_not", function() { return is_not; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "length", function() { return length; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "max", function() { return max; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "max_value", function() { return max_value; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mimes", function() { return mimes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "min", function() { return min; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "min_value", function() { return min_value; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "numeric", function() { return numeric; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "oneOf", function() { return oneOf; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "regex", function() { return regex; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "required", function() { return required; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "required_if", function() { return required_if; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "size", function() { return size; });
/**
  * vee-validate v3.0.11
  * (c) 2019 Abdelrahman Awad
  * @license MIT
  */
/**
 * Some Alpha Regex helpers.
 * https://github.com/chriso/validator.js/blob/master/src/lib/alpha.js
 */
var alpha = {
    en: /^[A-Z]*$/i,
    cs: /^[A-ZÁČĎÉĚÍŇÓŘŠŤÚŮÝŽ]*$/i,
    da: /^[A-ZÆØÅ]*$/i,
    de: /^[A-ZÄÖÜß]*$/i,
    es: /^[A-ZÁÉÍÑÓÚÜ]*$/i,
    fr: /^[A-ZÀÂÆÇÉÈÊËÏÎÔŒÙÛÜŸ]*$/i,
    it: /^[A-Z\xC0-\xFF]*$/i,
    lt: /^[A-ZĄČĘĖĮŠŲŪŽ]*$/i,
    nl: /^[A-ZÉËÏÓÖÜ]*$/i,
    hu: /^[A-ZÁÉÍÓÖŐÚÜŰ]*$/i,
    pl: /^[A-ZĄĆĘŚŁŃÓŻŹ]*$/i,
    pt: /^[A-ZÃÁÀÂÇÉÊÍÕÓÔÚÜ]*$/i,
    ru: /^[А-ЯЁ]*$/i,
    sk: /^[A-ZÁÄČĎÉÍĹĽŇÓŔŠŤÚÝŽ]*$/i,
    sr: /^[A-ZČĆŽŠĐ]*$/i,
    sv: /^[A-ZÅÄÖ]*$/i,
    tr: /^[A-ZÇĞİıÖŞÜ]*$/i,
    uk: /^[А-ЩЬЮЯЄІЇҐ]*$/i,
    ar: /^[ءآأؤإئابةتثجحخدذرزسشصضطظعغفقكلمنهوىيًٌٍَُِّْٰ]*$/,
    az: /^[A-ZÇƏĞİıÖŞÜ]*$/i
};
var alphaSpaces = {
    en: /^[A-Z\s]*$/i,
    cs: /^[A-ZÁČĎÉĚÍŇÓŘŠŤÚŮÝŽ\s]*$/i,
    da: /^[A-ZÆØÅ\s]*$/i,
    de: /^[A-ZÄÖÜß\s]*$/i,
    es: /^[A-ZÁÉÍÑÓÚÜ\s]*$/i,
    fr: /^[A-ZÀÂÆÇÉÈÊËÏÎÔŒÙÛÜŸ\s]*$/i,
    it: /^[A-Z\xC0-\xFF\s]*$/i,
    lt: /^[A-ZĄČĘĖĮŠŲŪŽ\s]*$/i,
    nl: /^[A-ZÉËÏÓÖÜ\s]*$/i,
    hu: /^[A-ZÁÉÍÓÖŐÚÜŰ\s]*$/i,
    pl: /^[A-ZĄĆĘŚŁŃÓŻŹ\s]*$/i,
    pt: /^[A-ZÃÁÀÂÇÉÊÍÕÓÔÚÜ\s]*$/i,
    ru: /^[А-ЯЁ\s]*$/i,
    sk: /^[A-ZÁÄČĎÉÍĹĽŇÓŔŠŤÚÝŽ\s]*$/i,
    sr: /^[A-ZČĆŽŠĐ\s]*$/i,
    sv: /^[A-ZÅÄÖ\s]*$/i,
    tr: /^[A-ZÇĞİıÖŞÜ\s]*$/i,
    uk: /^[А-ЩЬЮЯЄІЇҐ\s]*$/i,
    ar: /^[ءآأؤإئابةتثجحخدذرزسشصضطظعغفقكلمنهوىيًٌٍَُِّْٰ\s]*$/,
    az: /^[A-ZÇƏĞİıÖŞÜ\s]*$/i
};
var alphanumeric = {
    en: /^[0-9A-Z]*$/i,
    cs: /^[0-9A-ZÁČĎÉĚÍŇÓŘŠŤÚŮÝŽ]*$/i,
    da: /^[0-9A-ZÆØÅ]$/i,
    de: /^[0-9A-ZÄÖÜß]*$/i,
    es: /^[0-9A-ZÁÉÍÑÓÚÜ]*$/i,
    fr: /^[0-9A-ZÀÂÆÇÉÈÊËÏÎÔŒÙÛÜŸ]*$/i,
    it: /^[0-9A-Z\xC0-\xFF]*$/i,
    lt: /^[0-9A-ZĄČĘĖĮŠŲŪŽ]*$/i,
    hu: /^[0-9A-ZÁÉÍÓÖŐÚÜŰ]*$/i,
    nl: /^[0-9A-ZÉËÏÓÖÜ]*$/i,
    pl: /^[0-9A-ZĄĆĘŚŁŃÓŻŹ]*$/i,
    pt: /^[0-9A-ZÃÁÀÂÇÉÊÍÕÓÔÚÜ]*$/i,
    ru: /^[0-9А-ЯЁ]*$/i,
    sk: /^[0-9A-ZÁÄČĎÉÍĹĽŇÓŔŠŤÚÝŽ]*$/i,
    sr: /^[0-9A-ZČĆŽŠĐ]*$/i,
    sv: /^[0-9A-ZÅÄÖ]*$/i,
    tr: /^[0-9A-ZÇĞİıÖŞÜ]*$/i,
    uk: /^[0-9А-ЩЬЮЯЄІЇҐ]*$/i,
    ar: /^[٠١٢٣٤٥٦٧٨٩0-9ءآأؤإئابةتثجحخدذرزسشصضطظعغفقكلمنهوىيًٌٍَُِّْٰ]*$/,
    az: /^[0-9A-ZÇƏĞİıÖŞÜ]*$/i
};
var alphaDash = {
    en: /^[0-9A-Z_-]*$/i,
    cs: /^[0-9A-ZÁČĎÉĚÍŇÓŘŠŤÚŮÝŽ_-]*$/i,
    da: /^[0-9A-ZÆØÅ_-]*$/i,
    de: /^[0-9A-ZÄÖÜß_-]*$/i,
    es: /^[0-9A-ZÁÉÍÑÓÚÜ_-]*$/i,
    fr: /^[0-9A-ZÀÂÆÇÉÈÊËÏÎÔŒÙÛÜŸ_-]*$/i,
    it: /^[0-9A-Z\xC0-\xFF_-]*$/i,
    lt: /^[0-9A-ZĄČĘĖĮŠŲŪŽ_-]*$/i,
    nl: /^[0-9A-ZÉËÏÓÖÜ_-]*$/i,
    hu: /^[0-9A-ZÁÉÍÓÖŐÚÜŰ_-]*$/i,
    pl: /^[0-9A-ZĄĆĘŚŁŃÓŻŹ_-]*$/i,
    pt: /^[0-9A-ZÃÁÀÂÇÉÊÍÕÓÔÚÜ_-]*$/i,
    ru: /^[0-9А-ЯЁ_-]*$/i,
    sk: /^[0-9A-ZÁÄČĎÉÍĹĽŇÓŔŠŤÚÝŽ_-]*$/i,
    sr: /^[0-9A-ZČĆŽŠĐ_-]*$/i,
    sv: /^[0-9A-ZÅÄÖ_-]*$/i,
    tr: /^[0-9A-ZÇĞİıÖŞÜ_-]*$/i,
    uk: /^[0-9А-ЩЬЮЯЄІЇҐ_-]*$/i,
    ar: /^[٠١٢٣٤٥٦٧٨٩0-9ءآأؤإئابةتثجحخدذرزسشصضطظعغفقكلمنهوىيًٌٍَُِّْٰ_-]*$/,
    az: /^[0-9A-ZÇƏĞİıÖŞÜ_-]*$/i
};

var validate = function (value, _a) {
    var _b = (_a === void 0 ? {} : _a).locale, locale = _b === void 0 ? '' : _b;
    if (Array.isArray(value)) {
        return value.every(function (val) { return validate(val, { locale: locale }); });
    }
    // Match at least one locale.
    if (!locale) {
        return Object.keys(alpha).some(function (loc) { return alpha[loc].test(value); });
    }
    return (alpha[locale] || alpha.en).test(value);
};
var params = [
    {
        name: 'locale'
    }
];
var alpha$1 = {
    validate: validate,
    params: params
};

var validate$1 = function (value, _a) {
    var _b = (_a === void 0 ? {} : _a).locale, locale = _b === void 0 ? '' : _b;
    if (Array.isArray(value)) {
        return value.every(function (val) { return validate$1(val, { locale: locale }); });
    }
    // Match at least one locale.
    if (!locale) {
        return Object.keys(alphaDash).some(function (loc) { return alphaDash[loc].test(value); });
    }
    return (alphaDash[locale] || alphaDash.en).test(value);
};
var params$1 = [
    {
        name: 'locale'
    }
];
var alpha_dash = {
    validate: validate$1,
    params: params$1
};

var validate$2 = function (value, _a) {
    var _b = (_a === void 0 ? {} : _a).locale, locale = _b === void 0 ? '' : _b;
    if (Array.isArray(value)) {
        return value.every(function (val) { return validate$2(val, { locale: locale }); });
    }
    // Match at least one locale.
    if (!locale) {
        return Object.keys(alphanumeric).some(function (loc) { return alphanumeric[loc].test(value); });
    }
    return (alphanumeric[locale] || alphanumeric.en).test(value);
};
var params$2 = [
    {
        name: 'locale'
    }
];
var alpha_num = {
    validate: validate$2,
    params: params$2
};

var validate$3 = function (value, _a) {
    var _b = (_a === void 0 ? {} : _a).locale, locale = _b === void 0 ? '' : _b;
    if (Array.isArray(value)) {
        return value.every(function (val) { return validate$3(val, { locale: locale }); });
    }
    // Match at least one locale.
    if (!locale) {
        return Object.keys(alphaSpaces).some(function (loc) { return alphaSpaces[loc].test(value); });
    }
    return (alphaSpaces[locale] || alphaSpaces.en).test(value);
};
var params$3 = [
    {
        name: 'locale'
    }
];
var alpha_spaces = {
    validate: validate$3,
    params: params$3
};

var validate$4 = function (value, _a) {
    var _b = _a === void 0 ? {} : _a, min = _b.min, max = _b.max;
    if (Array.isArray(value)) {
        return value.every(function (val) { return !!validate$4(val, { min: min, max: max }); });
    }
    return Number(min) <= value && Number(max) >= value;
};
var params$4 = [
    {
        name: 'min'
    },
    {
        name: 'max'
    }
];
var between = {
    validate: validate$4,
    params: params$4
};

var validate$5 = function (value, _a) {
    var target = _a.target;
    return String(value) === String(target);
};
var params$5 = [
    {
        name: 'target',
        isTarget: true
    }
];
var confirmed = {
    validate: validate$5,
    params: params$5
};

var validate$6 = function (value, _a) {
    var length = _a.length;
    if (Array.isArray(value)) {
        return value.every(function (val) { return validate$6(val, { length: length }); });
    }
    var strVal = String(value);
    return /^[0-9]*$/.test(strVal) && strVal.length === length;
};
var params$6 = [
    {
        name: 'length',
        cast: function (value) {
            return Number(value);
        }
    }
];
var digits = {
    validate: validate$6,
    params: params$6
};

var validateImage = function (file, width, height) {
    var URL = window.URL || window.webkitURL;
    return new Promise(function (resolve) {
        var image = new Image();
        image.onerror = function () { return resolve(false); };
        image.onload = function () { return resolve(image.width === width && image.height === height); };
        image.src = URL.createObjectURL(file);
    });
};
var validate$7 = function (files, _a) {
    var width = _a.width, height = _a.height;
    var list = [];
    files = Array.isArray(files) ? files : [files];
    for (var i = 0; i < files.length; i++) {
        // if file is not an image, reject.
        if (!/\.(jpg|svg|jpeg|png|bmp|gif)$/i.test(files[i].name)) {
            return Promise.resolve(false);
        }
        list.push(files[i]);
    }
    return Promise.all(list.map(function (file) { return validateImage(file, width, height); })).then(function (values) {
        return values.every(function (v) { return v; });
    });
};
var params$7 = [
    {
        name: 'width',
        cast: function (value) {
            return Number(value);
        }
    },
    {
        name: 'height',
        cast: function (value) {
            return Number(value);
        }
    }
];
var dimensions = {
    validate: validate$7,
    params: params$7
};

var validate$8 = function (value, _a) {
    var multiple = (_a === void 0 ? {} : _a).multiple;
    // eslint-disable-next-line
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (multiple && !Array.isArray(value)) {
        value = String(value)
            .split(',')
            .map(function (emailStr) { return emailStr.trim(); });
    }
    if (Array.isArray(value)) {
        return value.every(function (val) { return re.test(String(val)); });
    }
    return re.test(String(value));
};
var params$8 = [
    {
        name: 'multiple',
        default: false
    }
];
var email = {
    validate: validate$8,
    params: params$8
};

/**
 * Checks if the values are either null or undefined.
 */
var isNullOrUndefined = function (value) {
    return value === null || value === undefined;
};
var includes = function (collection, item) {
    return collection.indexOf(item) !== -1;
};
/**
 * Checks if a function is callable.
 */
var isCallable = function (func) { return typeof func === 'function'; };
/* istanbul ignore next */
function _copyArray(arrayLike) {
    var array = [];
    var length = arrayLike.length;
    for (var i = 0; i < length; i++) {
        array.push(arrayLike[i]);
    }
    return array;
}
/**
 * Converts an array-like object to array, provides a simple polyfill for Array.from
 */
function toArray(arrayLike) {
    if (isCallable(Array.from)) {
        return Array.from(arrayLike);
    }
    /* istanbul ignore next */
    return _copyArray(arrayLike);
}
var isEmptyArray = function (arr) {
    return Array.isArray(arr) && arr.length === 0;
};

var validate$9 = function (value, options) {
    if (Array.isArray(value)) {
        return value.every(function (val) { return validate$9(val, options); });
    }
    return toArray(options).some(function (item) {
        // eslint-disable-next-line
        return item == value;
    });
};
var oneOf = {
    validate: validate$9
};

var validate$a = function (value, args) {
    return !validate$9(value, args);
};
var excluded = {
    validate: validate$a
};

var validate$b = function (files, extensions) {
    var regex = new RegExp(".(" + extensions.join('|') + ")$", 'i');
    if (Array.isArray(files)) {
        return files.every(function (file) { return regex.test(file.name); });
    }
    return regex.test(files.name);
};
var ext = {
    validate: validate$b
};

var validate$c = function (files) {
    var regex = /\.(jpg|svg|jpeg|png|bmp|gif)$/i;
    if (Array.isArray(files)) {
        return files.every(function (file) { return regex.test(file.name); });
    }
    return regex.test(files.name);
};
var image = {
    validate: validate$c
};

var validate$d = function (value) {
    if (Array.isArray(value)) {
        return value.every(function (val) { return /^-?[0-9]+$/.test(String(val)); });
    }
    return /^-?[0-9]+$/.test(String(value));
};
var integer = {
    validate: validate$d
};

var validate$e = function (value, _a) {
    var other = _a.other;
    return value === other;
};
var params$9 = [
    {
        name: 'other'
    }
];
var is = {
    validate: validate$e,
    params: params$9
};

var validate$f = function (value, _a) {
    var other = _a.other;
    return value !== other;
};
var params$a = [
    {
        name: 'other'
    }
];
var is_not = {
    validate: validate$f,
    params: params$a
};

var validate$g = function (value, _a) {
    var length = _a.length;
    if (isNullOrUndefined(value)) {
        return false;
    }
    if (typeof value === 'number') {
        value = String(value);
    }
    if (!value.length) {
        value = toArray(value);
    }
    return value.length === length;
};
var params$b = [
    {
        name: 'length',
        cast: function (value) { return Number(value); }
    }
];
var length = {
    validate: validate$g,
    params: params$b
};

var validate$h = function (value, _a) {
    var length = _a.length;
    if (isNullOrUndefined(value)) {
        return length >= 0;
    }
    if (Array.isArray(value)) {
        return value.every(function (val) { return validate$h(val, { length: length }); });
    }
    return String(value).length <= length;
};
var params$c = [
    {
        name: 'length',
        cast: function (value) {
            return Number(value);
        }
    }
];
var max = {
    validate: validate$h,
    params: params$c
};

var validate$i = function (value, _a) {
    var max = _a.max;
    if (isNullOrUndefined(value) || value === '') {
        return false;
    }
    if (Array.isArray(value)) {
        return value.length > 0 && value.every(function (val) { return validate$i(val, { max: max }); });
    }
    return Number(value) <= max;
};
var params$d = [
    {
        name: 'max',
        cast: function (value) {
            return Number(value);
        }
    }
];
var max_value = {
    validate: validate$i,
    params: params$d
};

var validate$j = function (files, mimes) {
    var regex = new RegExp(mimes.join('|').replace('*', '.+') + "$", 'i');
    if (Array.isArray(files)) {
        return files.every(function (file) { return regex.test(file.type); });
    }
    return regex.test(files.type);
};
var mimes = {
    validate: validate$j
};

var validate$k = function (value, _a) {
    var length = _a.length;
    if (isNullOrUndefined(value)) {
        return false;
    }
    if (Array.isArray(value)) {
        return value.every(function (val) { return validate$k(val, { length: length }); });
    }
    return String(value).length >= length;
};
var params$e = [
    {
        name: 'length',
        cast: function (value) {
            return Number(value);
        }
    }
];
var min = {
    validate: validate$k,
    params: params$e
};

var validate$l = function (value, _a) {
    var min = _a.min;
    if (isNullOrUndefined(value) || value === '') {
        return false;
    }
    if (Array.isArray(value)) {
        return value.length > 0 && value.every(function (val) { return validate$l(val, { min: min }); });
    }
    return Number(value) >= min;
};
var params$f = [
    {
        name: 'min',
        cast: function (value) {
            return Number(value);
        }
    }
];
var min_value = {
    validate: validate$l,
    params: params$f
};

var ar = /^[٠١٢٣٤٥٦٧٨٩]+$/;
var en = /^[0-9]+$/;
var validate$m = function (value) {
    var testValue = function (val) {
        var strValue = String(val);
        return en.test(strValue) || ar.test(strValue);
    };
    if (Array.isArray(value)) {
        return value.every(testValue);
    }
    return testValue(value);
};
var numeric = {
    validate: validate$m
};

var validate$n = function (value, _a) {
    var regex = _a.regex;
    if (Array.isArray(value)) {
        return value.every(function (val) { return validate$n(val, { regex: regex }); });
    }
    return regex.test(String(value));
};
var params$g = [
    {
        name: 'regex',
        cast: function (value) {
            if (typeof value === 'string') {
                return new RegExp(value);
            }
            return value;
        }
    }
];
var regex = {
    validate: validate$n,
    params: params$g
};

var validate$o = function (value, _a) {
    var allowFalse = (_a === void 0 ? { allowFalse: true } : _a).allowFalse;
    var result = {
        valid: false,
        required: true
    };
    if (isNullOrUndefined(value) || isEmptyArray(value)) {
        return result;
    }
    // incase a field considers `false` as an empty value like checkboxes.
    if (value === false && !allowFalse) {
        return result;
    }
    result.valid = !!String(value).trim().length;
    return result;
};
var computesRequired = true;
var params$h = [
    {
        name: 'allowFalse',
        default: true
    }
];
var required = {
    validate: validate$o,
    params: params$h,
    computesRequired: computesRequired
};

var testEmpty = function (value) {
    return isEmptyArray(value) || includes([false, null, undefined], value) || !String(value).trim().length;
};
var validate$p = function (value, _a) {
    var target = _a.target, values = _a.values;
    var required;
    if (values && values.length) {
        if (!Array.isArray(values) && typeof values === 'string') {
            values = [values];
        }
        // eslint-disable-next-line
        required = values.some(function (val) { return val == String(target).trim(); });
    }
    else {
        required = !testEmpty(target);
    }
    if (!required) {
        return {
            valid: true,
            required: required
        };
    }
    return {
        valid: !testEmpty(value),
        required: required
    };
};
var params$i = [
    {
        name: 'target',
        isTarget: true
    },
    {
        name: 'values'
    }
];
var computesRequired$1 = true;
var required_if = {
    validate: validate$p,
    params: params$i,
    computesRequired: computesRequired$1
};

var validate$q = function (files, _a) {
    var size = _a.size;
    if (isNaN(size)) {
        return false;
    }
    var nSize = size * 1024;
    if (!Array.isArray(files)) {
        return files.size <= nSize;
    }
    for (var i = 0; i < files.length; i++) {
        if (files[i].size > nSize) {
            return false;
        }
    }
    return true;
};
var params$j = [
    {
        name: 'size',
        cast: function (value) {
            return Number(value);
        }
    }
];
var size = {
    validate: validate$q,
    params: params$j
};




/***/ }),

/***/ "./node_modules/vee-validate/dist/vee-validate.esm.js":
/*!************************************************************!*\
  !*** ./node_modules/vee-validate/dist/vee-validate.esm.js ***!
  \************************************************************/
/*! exports provided: ValidationObserver, ValidationProvider, configure, extend, install, localize, setInteractionMode, validate, version, withValidation */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ValidationObserver", function() { return ValidationObserver; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ValidationProvider", function() { return ValidationProvider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "configure", function() { return configure; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "extend", function() { return extend; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "install", function() { return install; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "localize", function() { return localize; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setInteractionMode", function() { return setInteractionMode; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "validate", function() { return validate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "version", function() { return version; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "withValidation", function() { return withValidation; });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.runtime.esm.js");
/**
  * vee-validate v3.0.11
  * (c) 2019 Abdelrahman Awad
  * @license MIT
  */


/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};

function __awaiter(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function __generator(thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
}

function __spreadArrays() {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
}

var isNaN = function (value) {
    // NaN is the one value that does not equal itself.
    // eslint-disable-next-line
    return value !== value;
};
/**
 * Checks if the values are either null or undefined.
 */
var isNullOrUndefined = function (value) {
    return value === null || value === undefined;
};
/**
 * Creates the default flags object.
 */
var createFlags = function () { return ({
    untouched: true,
    touched: false,
    dirty: false,
    pristine: true,
    valid: false,
    invalid: false,
    validated: false,
    pending: false,
    required: false,
    changed: false
}); };
/**
 * Checks if the value is an object.
 */
var isObject = function (obj) {
    return obj !== null && obj && typeof obj === 'object' && !Array.isArray(obj);
};
function identity(x) {
    return x;
}
/**
 * Shallow object comparison.
 */
var isEqual = function (lhs, rhs) {
    if (lhs instanceof RegExp && rhs instanceof RegExp) {
        return isEqual(lhs.source, rhs.source) && isEqual(lhs.flags, rhs.flags);
    }
    if (Array.isArray(lhs) && Array.isArray(rhs)) {
        if (lhs.length !== rhs.length)
            return false;
        for (var i = 0; i < lhs.length; i++) {
            if (!isEqual(lhs[i], rhs[i])) {
                return false;
            }
        }
        return true;
    }
    // if both are objects, compare each key recursively.
    if (isObject(lhs) && isObject(rhs)) {
        return (Object.keys(lhs).every(function (key) {
            return isEqual(lhs[key], rhs[key]);
        }) &&
            Object.keys(rhs).every(function (key) {
                return isEqual(lhs[key], rhs[key]);
            }));
    }
    if (isNaN(lhs) && isNaN(rhs)) {
        return true;
    }
    return lhs === rhs;
};
var includes = function (collection, item) {
    return collection.indexOf(item) !== -1;
};
/**
 * Parses a rule string expression.
 */
var parseRule = function (rule) {
    var params = [];
    var name = rule.split(':')[0];
    if (includes(rule, ':')) {
        params = rule
            .split(':')
            .slice(1)
            .join(':')
            .split(',');
    }
    return { name: name, params: params };
};
/**
 * Debounces a function.
 */
var debounce = function (fn, wait, token) {
    if (wait === void 0) { wait = 0; }
    if (token === void 0) { token = { cancelled: false }; }
    if (wait === 0) {
        return fn;
    }
    var timeout;
    return function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        var later = function () {
            timeout = undefined;
            // check if the fn call was cancelled.
            if (!token.cancelled)
                fn.apply(void 0, args);
        };
        // because we might want to use Node.js setTimout for SSR.
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
    };
};
/**
 * Emits a warning to the console.
 */
var warn = function (message) {
    console.warn("[vee-validate] " + message);
};
/**
 * Normalizes the given rules expression.
 */
var normalizeRules = function (rules) {
    // if falsy value return an empty object.
    var acc = {};
    Object.defineProperty(acc, '_$$isNormalized', {
        value: true,
        writable: false,
        enumerable: false,
        configurable: false
    });
    if (!rules) {
        return acc;
    }
    // Object is already normalized, skip.
    if (isObject(rules) && rules._$$isNormalized) {
        return rules;
    }
    if (isObject(rules)) {
        return Object.keys(rules).reduce(function (prev, curr) {
            var params = [];
            if (rules[curr] === true) {
                params = [];
            }
            else if (Array.isArray(rules[curr])) {
                params = rules[curr];
            }
            else if (isObject(rules[curr])) {
                params = rules[curr];
            }
            else {
                params = [rules[curr]];
            }
            if (rules[curr] !== false) {
                prev[curr] = params;
            }
            return prev;
        }, acc);
    }
    /* istanbul ignore if */
    if (typeof rules !== 'string') {
        warn('rules must be either a string or an object.');
        return acc;
    }
    return rules.split('|').reduce(function (prev, rule) {
        var parsedRule = parseRule(rule);
        if (!parsedRule.name) {
            return prev;
        }
        prev[parsedRule.name] = parsedRule.params;
        return prev;
    }, acc);
};
/**
 * Checks if a function is callable.
 */
var isCallable = function (func) { return typeof func === 'function'; };
function computeClassObj(names, flags) {
    var acc = {};
    var keys = Object.keys(flags);
    var length = keys.length;
    var _loop_1 = function (i) {
        var flag = keys[i];
        var className = (names && names[flag]) || flag;
        var value = flags[flag];
        if (isNullOrUndefined(value)) {
            return "continue";
        }
        if ((flag === 'valid' || flag === 'invalid') && !flags.validated) {
            return "continue";
        }
        if (typeof className === 'string') {
            acc[className] = value;
        }
        else if (Array.isArray(className)) {
            className.forEach(function (cls) {
                acc[cls] = value;
            });
        }
    };
    for (var i = 0; i < length; i++) {
        _loop_1(i);
    }
    return acc;
}
/* istanbul ignore next */
function _copyArray(arrayLike) {
    var array = [];
    var length = arrayLike.length;
    for (var i = 0; i < length; i++) {
        array.push(arrayLike[i]);
    }
    return array;
}
/**
 * Converts an array-like object to array, provides a simple polyfill for Array.from
 */
function toArray(arrayLike) {
    if (isCallable(Array.from)) {
        return Array.from(arrayLike);
    }
    /* istanbul ignore next */
    return _copyArray(arrayLike);
}
function findIndex(arrayLike, predicate) {
    var array = Array.isArray(arrayLike) ? arrayLike : toArray(arrayLike);
    if (isCallable(array.findIndex)) {
        return array.findIndex(predicate);
    }
    /* istanbul ignore next */
    for (var i = 0; i < array.length; i++) {
        if (predicate(array[i], i)) {
            return i;
        }
    }
    /* istanbul ignore next */
    return -1;
}
/**
 * finds the first element that satisfies the predicate callback, polyfills array.find
 */
function find(arrayLike, predicate) {
    var array = Array.isArray(arrayLike) ? arrayLike : toArray(arrayLike);
    var idx = findIndex(array, predicate);
    return idx === -1 ? undefined : array[idx];
}
function merge(target, source) {
    Object.keys(source).forEach(function (key) {
        if (isObject(source[key])) {
            if (!target[key]) {
                target[key] = {};
            }
            merge(target[key], source[key]);
            return;
        }
        target[key] = source[key];
    });
    return target;
}
function values(obj) {
    if (isCallable(Object.values)) {
        return Object.values(obj);
    }
    // fallback to keys()
    /* istanbul ignore next */
    return Object.keys(obj).map(function (k) { return obj[k]; });
}
var isEmptyArray = function (arr) {
    return Array.isArray(arr) && arr.length === 0;
};
var interpolate = function (template, values) {
    return template.replace(/\{([^}]+)\}/g, function (_, p) {
        return p in values ? values[p] : "{" + p + "}";
    });
};
// Checks if a given value is not an empty string or null or undefined.
var isSpecified = function (val) {
    if (val === '') {
        return false;
    }
    return !isNullOrUndefined(val);
};

var RULES = {};
function normalizeSchema(schema) {
    if (schema.params && schema.params.length) {
        schema.params = schema.params.map(function (param) {
            if (typeof param === 'string') {
                return { name: param };
            }
            return param;
        });
    }
    return schema;
}
var RuleContainer = /** @class */ (function () {
    function RuleContainer() {
    }
    RuleContainer.extend = function (name, schema) {
        // if rule already exists, overwrite it.
        var rule = normalizeSchema(schema);
        if (RULES[name]) {
            RULES[name] = merge(RULES[name], schema);
            return;
        }
        RULES[name] = __assign({ lazy: false, computesRequired: false }, rule);
    };
    RuleContainer.iterate = function (fn) {
        var keys = Object.keys(RULES);
        var length = keys.length;
        for (var i = 0; i < length; i++) {
            fn(keys[i], RULES[keys[i]]);
        }
    };
    RuleContainer.isLazy = function (name) {
        return !!(RULES[name] && RULES[name].lazy);
    };
    RuleContainer.isRequireRule = function (name) {
        return !!(RULES[name] && RULES[name].computesRequired);
    };
    RuleContainer.isTargetRule = function (name) {
        var definition = RuleContainer.getRuleDefinition(name);
        if (!definition || !definition.params) {
            return false;
        }
        return definition.params.some(function (param) { return !!param.isTarget; });
    };
    RuleContainer.getTargetParamNames = function (rule, params) {
        var definition = RuleContainer.getRuleDefinition(rule);
        if (Array.isArray(params)) {
            return params.filter(function (_, idx) {
                return definition.params && find(definition.params, function (p, i) { return !!p.isTarget && i === idx; });
            });
        }
        return Object.keys(params)
            .filter(function (key) {
            return definition.params && find(definition.params, function (p) { return !!p.isTarget && p.name === key; });
        })
            .map(function (key) { return params[key]; });
    };
    RuleContainer.getRuleDefinition = function (ruleName) {
        return RULES[ruleName];
    };
    return RuleContainer;
}());
/**
 * Adds a custom validator to the list of validation rules.
 */
function extend(name, schema) {
    // makes sure new rules are properly formatted.
    guardExtend(name, schema);
    // Full schema object.
    if (typeof schema === 'object') {
        RuleContainer.extend(name, schema);
        return;
    }
    RuleContainer.extend(name, {
        validate: schema
    });
}
/**
 * Guards from extension violations.
 */
function guardExtend(name, validator) {
    if (isCallable(validator)) {
        return;
    }
    if (isCallable(validator.validate)) {
        return;
    }
    if (RuleContainer.getRuleDefinition(name)) {
        return;
    }
    throw new Error("Extension Error: The validator '" + name + "' must be a function or have a 'validate' method.");
}

var DEFAULT_CONFIG = {
    defaultMessage: "{_field_} is not valid.",
    skipOptional: true,
    classes: {
        touched: 'touched',
        untouched: 'untouched',
        valid: 'valid',
        invalid: 'invalid',
        pristine: 'pristine',
        dirty: 'dirty' // control has been interacted with
    },
    bails: true,
    mode: 'aggressive',
    useConstraintAttrs: true
};
var currentConfig = __assign({}, DEFAULT_CONFIG);
var getConfig = function () { return currentConfig; };
var setConfig = function (newConf) {
    currentConfig = __assign(__assign({}, currentConfig), newConf);
};
var configure = function (cfg) {
    setConfig(cfg);
};

/**
 * Validates a value against the rules.
 */
function validate(value, rules, options) {
    if (options === void 0) { options = {}; }
    return __awaiter(this, void 0, void 0, function () {
        var shouldBail, skipIfEmpty, field, result, errors, ruleMap;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    shouldBail = options && options.bails;
                    skipIfEmpty = options && options.skipIfEmpty;
                    field = {
                        name: (options && options.name) || '{field}',
                        rules: normalizeRules(rules),
                        bails: isNullOrUndefined(shouldBail) ? true : shouldBail,
                        skipIfEmpty: isNullOrUndefined(skipIfEmpty) ? true : skipIfEmpty,
                        forceRequired: false,
                        crossTable: (options && options.values) || {},
                        names: (options && options.names) || {},
                        customMessages: (options && options.customMessages) || {}
                    };
                    return [4 /*yield*/, _validate(field, value, options)];
                case 1:
                    result = _a.sent();
                    errors = [];
                    ruleMap = {};
                    result.errors.forEach(function (e) {
                        errors.push(e.msg);
                        ruleMap[e.rule] = e.msg;
                    });
                    return [2 /*return*/, {
                            valid: result.valid,
                            errors: errors,
                            failedRules: ruleMap
                        }];
            }
        });
    });
}
/**
 * Starts the validation process.
 */
function _validate(field, value, _a) {
    var _b = (_a === void 0 ? {} : _a).isInitial, isInitial = _b === void 0 ? false : _b;
    return __awaiter(this, void 0, void 0, function () {
        var _c, shouldSkip, errors, rules, length, i, rule, result;
        return __generator(this, function (_d) {
            switch (_d.label) {
                case 0: return [4 /*yield*/, _shouldSkip(field, value)];
                case 1:
                    _c = _d.sent(), shouldSkip = _c.shouldSkip, errors = _c.errors;
                    if (shouldSkip) {
                        return [2 /*return*/, {
                                valid: !errors.length,
                                errors: errors
                            }];
                    }
                    rules = Object.keys(field.rules).filter(function (rule) { return !RuleContainer.isRequireRule(rule); });
                    length = rules.length;
                    i = 0;
                    _d.label = 2;
                case 2:
                    if (!(i < length)) return [3 /*break*/, 5];
                    if (isInitial && RuleContainer.isLazy(rules[i])) {
                        return [3 /*break*/, 4];
                    }
                    rule = rules[i];
                    return [4 /*yield*/, _test(field, value, {
                            name: rule,
                            params: field.rules[rule]
                        })];
                case 3:
                    result = _d.sent();
                    if (!result.valid && result.error) {
                        errors.push(result.error);
                        if (field.bails) {
                            return [2 /*return*/, {
                                    valid: false,
                                    errors: errors
                                }];
                        }
                    }
                    _d.label = 4;
                case 4:
                    i++;
                    return [3 /*break*/, 2];
                case 5: return [2 /*return*/, {
                        valid: !errors.length,
                        errors: errors
                    }];
            }
        });
    });
}
function _shouldSkip(field, value) {
    return __awaiter(this, void 0, void 0, function () {
        var requireRules, length, errors, isEmpty, isEmptyAndOptional, isRequired, i, rule, result;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    requireRules = Object.keys(field.rules).filter(RuleContainer.isRequireRule);
                    length = requireRules.length;
                    errors = [];
                    isEmpty = isNullOrUndefined(value) || value === '' || isEmptyArray(value);
                    isEmptyAndOptional = isEmpty && field.skipIfEmpty;
                    isRequired = false;
                    i = 0;
                    _a.label = 1;
                case 1:
                    if (!(i < length)) return [3 /*break*/, 4];
                    rule = requireRules[i];
                    return [4 /*yield*/, _test(field, value, {
                            name: rule,
                            params: field.rules[rule]
                        })];
                case 2:
                    result = _a.sent();
                    if (!isObject(result)) {
                        throw new Error('Require rules has to return an object (see docs)');
                    }
                    if (result.required) {
                        isRequired = true;
                    }
                    if (!result.valid && result.error) {
                        errors.push(result.error);
                        // Exit early as the field is required and failed validation.
                        if (field.bails) {
                            return [2 /*return*/, {
                                    shouldSkip: true,
                                    errors: errors
                                }];
                        }
                    }
                    _a.label = 3;
                case 3:
                    i++;
                    return [3 /*break*/, 1];
                case 4:
                    if (isEmpty && !isRequired && !field.skipIfEmpty) {
                        return [2 /*return*/, {
                                shouldSkip: false,
                                errors: errors
                            }];
                    }
                    // field is configured to run through the pipeline regardless
                    if (!field.bails && !isEmptyAndOptional) {
                        return [2 /*return*/, {
                                shouldSkip: false,
                                errors: errors
                            }];
                    }
                    // skip if the field is not required and has an empty value.
                    return [2 /*return*/, {
                            shouldSkip: !isRequired && isEmpty,
                            errors: errors
                        }];
            }
        });
    });
}
/**
 * Tests a single input value against a rule.
 */
function _test(field, value, rule) {
    return __awaiter(this, void 0, void 0, function () {
        var ruleSchema, params, normalizedValue, result, values;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    ruleSchema = RuleContainer.getRuleDefinition(rule.name);
                    if (!ruleSchema || !ruleSchema.validate) {
                        throw new Error("No such validator '" + rule.name + "' exists.");
                    }
                    params = _buildParams(rule.params, ruleSchema.params, field.crossTable);
                    normalizedValue = ruleSchema.castValue ? ruleSchema.castValue(value) : value;
                    return [4 /*yield*/, ruleSchema.validate(normalizedValue, params)];
                case 1:
                    result = _a.sent();
                    if (typeof result === 'string') {
                        values = __assign(__assign({}, (params || {})), { _field_: field.name, _value_: value, _rule_: rule.name });
                        return [2 /*return*/, {
                                valid: false,
                                error: { rule: rule.name, msg: interpolate(result, values) }
                            }];
                    }
                    if (!isObject(result)) {
                        result = { valid: result, data: {} };
                    }
                    return [2 /*return*/, {
                            valid: result.valid,
                            required: result.required,
                            data: result.data || {},
                            error: result.valid ? undefined : _generateFieldError(field, value, ruleSchema, rule.name, params, result.data)
                        }];
            }
        });
    });
}
/**
 * Generates error messages.
 */
function _generateFieldError(field, value, ruleSchema, ruleName, params, data) {
    var values = __assign(__assign(__assign(__assign({}, (params || {})), (data || {})), { _field_: field.name, _value_: value, _rule_: ruleName }), _getTargetNames(field, ruleSchema, ruleName));
    if (Object.prototype.hasOwnProperty.call(field.customMessages, ruleName) &&
        typeof field.customMessages[ruleName] === 'string') {
        return {
            msg: _normalizeMessage(field.customMessages[ruleName], field.name, values),
            rule: ruleName
        };
    }
    if (ruleSchema.message) {
        return {
            msg: _normalizeMessage(ruleSchema.message, field.name, values),
            rule: ruleName
        };
    }
    return {
        msg: _normalizeMessage(getConfig().defaultMessage, field.name, values),
        rule: ruleName
    };
}
function _getTargetNames(field, ruleSchema, ruleName) {
    if (ruleSchema.params) {
        var numTargets = ruleSchema.params.filter(function (param) { return param.isTarget; }).length;
        if (numTargets > 0) {
            var names = {};
            for (var index = 0; index < ruleSchema.params.length; index++) {
                var param = ruleSchema.params[index];
                if (param.isTarget) {
                    var key = field.rules[ruleName][index];
                    var name_1 = field.names[key] || key;
                    if (numTargets === 1) {
                        names._target_ = name_1;
                        break;
                    }
                    else {
                        names["_" + param.name + "Target_"] = name_1;
                    }
                }
            }
            return names;
        }
    }
    return {};
}
function _normalizeMessage(template, field, values) {
    if (typeof template === 'function') {
        return template(field, values);
    }
    return interpolate(template, __assign(__assign({}, values), { _field_: field }));
}
function _buildParams(provided, defined, crossTable) {
    var params = {};
    if (!defined && !Array.isArray(provided)) {
        throw new Error('You provided an object params to a rule that has no defined schema.');
    }
    // Rule probably uses an array for their args, keep it as is.
    if (Array.isArray(provided) && !defined) {
        return provided;
    }
    var definedRules;
    // collect the params schema.
    if (!defined || (defined.length < provided.length && Array.isArray(provided))) {
        var lastDefinedParam_1;
        // collect any additional parameters in the last item.
        definedRules = provided.map(function (_, idx) {
            var param = defined && defined[idx];
            lastDefinedParam_1 = param || lastDefinedParam_1;
            if (!param) {
                param = lastDefinedParam_1;
            }
            return param;
        });
    }
    else {
        definedRules = defined;
    }
    // Match the provided array length with a temporary schema.
    for (var i = 0; i < definedRules.length; i++) {
        var options = definedRules[i];
        var value = options.default;
        // if the provided is an array, map element value.
        if (Array.isArray(provided)) {
            if (i in provided) {
                value = provided[i];
            }
        }
        else {
            // If the param exists in the provided object.
            if (options.name in provided) {
                value = provided[options.name];
                // if the provided is the first param value.
            }
            else if (definedRules.length === 1) {
                value = provided;
            }
        }
        // if the param is a target, resolve the target value.
        if (options.isTarget) {
            value = crossTable[value];
        }
        // If there is a transformer defined.
        if (options.cast) {
            value = options.cast(value);
        }
        // already been set, probably multiple values.
        if (params[options.name]) {
            params[options.name] = Array.isArray(params[options.name]) ? params[options.name] : [params[options.name]];
            params[options.name].push(value);
        }
        else {
            // set the value.
            params[options.name] = value;
        }
    }
    return params;
}

function install(_, config) {
    setConfig(config);
}

var aggressive = function () { return ({
    on: ['input', 'blur']
}); };
var lazy = function () { return ({
    on: ['change']
}); };
var eager = function (_a) {
    var errors = _a.errors;
    if (errors.length) {
        return {
            on: ['input', 'change']
        };
    }
    return {
        on: ['change', 'blur']
    };
};
var passive = function () { return ({
    on: []
}); };
var modes = {
    aggressive: aggressive,
    eager: eager,
    passive: passive,
    lazy: lazy
};
var setInteractionMode = function (mode, implementation) {
    setConfig({ mode: mode });
    if (!implementation) {
        return;
    }
    if (!isCallable(implementation)) {
        throw new Error('A mode implementation must be a function');
    }
    modes[mode] = implementation;
};

var Dictionary = /** @class */ (function () {
    function Dictionary(locale, dictionary) {
        this.container = {};
        this.locale = locale;
        this.merge(dictionary);
    }
    Dictionary.prototype.resolve = function (field, rule, values) {
        return this.format(this.locale, field, rule, values);
    };
    Dictionary.prototype._hasLocale = function (locale) {
        return !!this.container[locale];
    };
    Dictionary.prototype.format = function (locale, field, rule, values) {
        var message;
        // find if specific message for that field was specified.
        var dict = this.container[locale] && this.container[locale].fields && this.container[locale].fields[field];
        if (dict && dict[rule]) {
            message = dict[rule];
        }
        if (!message && this._hasLocale(locale) && this._hasMessage(locale, rule)) {
            message = this.container[locale].messages[rule];
        }
        if (!message) {
            message = getConfig().defaultMessage;
        }
        if (this._hasName(locale, field)) {
            field = this.getName(locale, field);
        }
        return isCallable(message) ? message(field, values) : interpolate(message, __assign(__assign({}, values), { _field_: field }));
    };
    Dictionary.prototype.merge = function (dictionary) {
        merge(this.container, dictionary);
    };
    Dictionary.prototype.hasRule = function (name) {
        var locale = this.container[this.locale];
        if (!locale)
            return false;
        return !!(locale.messages && locale.messages[name]);
    };
    Dictionary.prototype.getName = function (locale, key) {
        return this.container[locale].names[key];
    };
    Dictionary.prototype._hasMessage = function (locale, key) {
        return !!(this._hasLocale(locale) && this.container[locale].messages && this.container[locale].messages[key]);
    };
    Dictionary.prototype._hasName = function (locale, key) {
        return !!(this._hasLocale(locale) && this.container[locale].names && this.container[locale].names[key]);
    };
    return Dictionary;
}());
var DICTIONARY;
var INSTALLED = false;
function updateRules() {
    if (INSTALLED) {
        return;
    }
    RuleContainer.iterate(function (name, schema) {
        var _a, _b;
        if (schema.message && !DICTIONARY.hasRule(name)) {
            DICTIONARY.merge((_a = {},
                _a[DICTIONARY.locale] = {
                    messages: (_b = {},
                        _b[name] = schema.message,
                        _b)
                },
                _a));
        }
        extend(name, {
            message: function (field, values) {
                return DICTIONARY.resolve(field, name, values || {});
            }
        });
    });
    INSTALLED = true;
}
function localize(locale, dictionary) {
    var _a;
    if (!DICTIONARY) {
        DICTIONARY = new Dictionary('en', {});
    }
    if (typeof locale === 'string') {
        DICTIONARY.locale = locale;
        if (dictionary) {
            DICTIONARY.merge((_a = {}, _a[locale] = dictionary, _a));
        }
        updateRules();
        return;
    }
    DICTIONARY.merge(locale);
    updateRules();
}

var isEvent = function (evt) {
    if (!evt) {
        return false;
    }
    if (typeof Event !== 'undefined' && isCallable(Event) && evt instanceof Event) {
        return true;
    }
    // this is for IE
    /* istanbul ignore next */
    if (evt && evt.srcElement) {
        return true;
    }
    return false;
};
function normalizeEventValue(value) {
    if (!isEvent(value)) {
        return value;
    }
    var input = value.target;
    if (input.type === 'file' && input.files) {
        return toArray(input.files);
    }
    // If the input has a `v-model.number` modifier applied.
    if (input._vModifiers && input._vModifiers.number) {
        // as per the spec the v-model.number uses parseFloat
        var valueAsNumber = parseFloat(input.value);
        if (isNaN(valueAsNumber)) {
            return input.value;
        }
        return valueAsNumber;
    }
    if (input._vModifiers && input._vModifiers.trim) {
        var trimmedValue = typeof input.value === 'string' ? input.value.trim() : input.value;
        return trimmedValue;
    }
    return input.value;
}

var isTextInput = function (vnode) {
    var attrs = (vnode.data && vnode.data.attrs) || vnode.elm;
    // it will fallback to being a text input per browsers spec.
    if (vnode.tag === 'input' && (!attrs || !attrs.type)) {
        return true;
    }
    if (vnode.tag === 'textarea') {
        return true;
    }
    return includes(['text', 'password', 'search', 'email', 'tel', 'url', 'number'], attrs && attrs.type);
};
// export const isCheckboxOrRadioInput = (vnode: VNode): boolean => {
//   const attrs = (vnode.data && vnode.data.attrs) || vnode.elm;
//   return includes(['radio', 'checkbox'], attrs && attrs.type);
// };
// Gets the model object on the vnode.
function findModel(vnode) {
    if (!vnode.data) {
        return undefined;
    }
    // Component Model
    // THIS IS NOT TYPED IN OFFICIAL VUE TYPINGS
    // eslint-disable-next-line
    var nonStandardVNodeData = vnode.data;
    if ('model' in nonStandardVNodeData) {
        return nonStandardVNodeData.model;
    }
    if (!vnode.data.directives) {
        return undefined;
    }
    return find(vnode.data.directives, function (d) { return d.name === 'model'; });
}
function findValue(vnode) {
    var model = findModel(vnode);
    if (model) {
        return { value: model.value };
    }
    var config = findModelConfig(vnode);
    var prop = (config && config.prop) || 'value';
    if (vnode.componentOptions && vnode.componentOptions.propsData && prop in vnode.componentOptions.propsData) {
        var propsDataWithValue = vnode.componentOptions.propsData;
        return { value: propsDataWithValue[prop] };
    }
    if (vnode.data && vnode.data.domProps && 'value' in vnode.data.domProps) {
        return { value: vnode.data.domProps.value };
    }
    return undefined;
}
function extractChildren(vnode) {
    if (Array.isArray(vnode)) {
        return vnode;
    }
    if (Array.isArray(vnode.children)) {
        return vnode.children;
    }
    /* istanbul ignore next */
    if (vnode.componentOptions && Array.isArray(vnode.componentOptions.children)) {
        return vnode.componentOptions.children;
    }
    return [];
}
function extractVNodes(vnode) {
    if (!Array.isArray(vnode) && findValue(vnode) !== undefined) {
        return [vnode];
    }
    var children = extractChildren(vnode);
    return children.reduce(function (nodes, node) {
        var candidates = extractVNodes(node);
        if (candidates.length) {
            nodes.push.apply(nodes, candidates);
        }
        return nodes;
    }, []);
}
// Resolves v-model config if exists.
function findModelConfig(vnode) {
    /* istanbul ignore next */
    if (!vnode.componentOptions)
        return null;
    // This is also not typed in the standard Vue TS.
    return vnode.componentOptions.Ctor.options.model;
}
// Adds a listener to vnode listener object.
function mergeVNodeListeners(obj, eventName, handler) {
    // no listener at all.
    if (isNullOrUndefined(obj[eventName])) {
        obj[eventName] = [handler];
        return;
    }
    // Is an invoker.
    if (isCallable(obj[eventName]) && obj[eventName].fns) {
        var invoker = obj[eventName];
        invoker.fns = Array.isArray(invoker.fns) ? invoker.fns : [invoker.fns];
        if (!includes(invoker.fns, handler)) {
            invoker.fns.push(handler);
        }
        return;
    }
    if (isCallable(obj[eventName])) {
        var prev = obj[eventName];
        obj[eventName] = [prev];
    }
    if (Array.isArray(obj[eventName]) && !includes(obj[eventName], handler)) {
        obj[eventName].push(handler);
    }
}
// Adds a listener to a native HTML vnode.
function addNativeNodeListener(node, eventName, handler) {
    /* istanbul ignore next */
    if (!node.data) {
        node.data = {};
    }
    if (isNullOrUndefined(node.data.on)) {
        node.data.on = {};
    }
    mergeVNodeListeners(node.data.on, eventName, handler);
}
// Adds a listener to a Vue component vnode.
function addComponentNodeListener(node, eventName, handler) {
    /* istanbul ignore next */
    if (!node.componentOptions) {
        return;
    }
    /* istanbul ignore next */
    if (!node.componentOptions.listeners) {
        node.componentOptions.listeners = {};
    }
    mergeVNodeListeners(node.componentOptions.listeners, eventName, handler);
}
function addVNodeListener(vnode, eventName, handler) {
    if (vnode.componentOptions) {
        addComponentNodeListener(vnode, eventName, handler);
        return;
    }
    addNativeNodeListener(vnode, eventName, handler);
}
// Determines if `change` should be used over `input` for listeners.
function getInputEventName(vnode, model) {
    // Is a component.
    if (vnode.componentOptions) {
        var event_1 = (findModelConfig(vnode) || { event: 'input' }).event;
        return event_1;
    }
    // Lazy Models typically use change event
    if (model && model.modifiers && model.modifiers.lazy) {
        return 'change';
    }
    // is a textual-type input.
    if (isTextInput(vnode)) {
        return 'input';
    }
    return 'change';
}
// TODO: Type this one properly.
function normalizeSlots(slots, ctx) {
    var acc = [];
    return Object.keys(slots).reduce(function (arr, key) {
        slots[key].forEach(function (vnode) {
            if (!vnode.context) {
                slots[key].context = ctx;
                if (!vnode.data) {
                    vnode.data = {};
                }
                vnode.data.slot = key;
            }
        });
        return arr.concat(slots[key]);
    }, acc);
}
function resolveTextualRules(vnode) {
    var attrs = vnode.data && vnode.data.attrs;
    var rules = {};
    if (!attrs)
        return rules;
    if (attrs.type === 'email') {
        rules.email = ['multiple' in attrs];
    }
    if (attrs.pattern) {
        rules.regex = attrs.pattern;
    }
    if (attrs.maxlength >= 0) {
        rules.max = attrs.maxlength;
    }
    if (attrs.minlength >= 0) {
        rules.min = attrs.minlength;
    }
    if (attrs.type === 'number') {
        if (isSpecified(attrs.min)) {
            rules.min_value = Number(attrs.min);
        }
        if (isSpecified(attrs.max)) {
            rules.max_value = Number(attrs.max);
        }
    }
    return rules;
}
function resolveRules(vnode) {
    var htmlTags = ['input', 'select'];
    var attrs = vnode.data && vnode.data.attrs;
    if (!includes(htmlTags, vnode.tag) || !attrs) {
        return {};
    }
    var rules = {};
    if ('required' in attrs && attrs.required !== false) {
        rules.required = attrs.type === 'checkbox' ? [true] : true;
    }
    if (isTextInput(vnode)) {
        return normalizeRules(__assign(__assign({}, rules), resolveTextualRules(vnode)));
    }
    return normalizeRules(rules);
}
function normalizeChildren(context, slotProps) {
    if (context.$scopedSlots.default) {
        return context.$scopedSlots.default(slotProps) || [];
    }
    return context.$slots.default || [];
}

/**
 * Determines if a provider needs to run validation.
 */
function shouldValidate(ctx, value) {
    // when an immediate/initial validation is needed and wasn't done before.
    if (!ctx._ignoreImmediate && ctx.immediate) {
        return true;
    }
    // when the value changes for whatever reason.
    if (ctx.value !== value && ctx.normalizedEvents.length) {
        return true;
    }
    // when it needs validation due to props/cross-fields changes.
    if (ctx._needsValidation) {
        return true;
    }
    // when the initial value is undefined and the field wasn't rendered yet.
    if (!ctx.initialized && value === undefined) {
        return true;
    }
    return false;
}
function createValidationCtx(ctx) {
    return __assign(__assign({}, ctx.flags), { errors: ctx.messages, classes: ctx.classes, failedRules: ctx.failedRules, reset: function () { return ctx.reset(); }, validate: function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            return ctx.validate.apply(ctx, args);
        }, ariaInput: {
            'aria-invalid': ctx.flags.invalid ? 'true' : 'false',
            'aria-required': ctx.isRequired ? 'true' : 'false',
            'aria-errormessage': "vee_" + ctx.id
        }, ariaMsg: {
            id: "vee_" + ctx.id,
            'aria-live': ctx.messages.length ? 'assertive' : 'off'
        } });
}
function onRenderUpdate(vm, value) {
    if (!vm.initialized) {
        vm.initialValue = value;
    }
    var validateNow = shouldValidate(vm, value);
    vm._needsValidation = false;
    vm.value = value;
    vm._ignoreImmediate = true;
    if (!validateNow) {
        return;
    }
    vm.validateSilent().then(vm.immediate || vm.flags.validated ? vm.applyResult : identity);
}
function computeModeSetting(ctx) {
    var compute = (isCallable(ctx.mode) ? ctx.mode : modes[ctx.mode]);
    return compute({
        errors: ctx.messages,
        value: ctx.value,
        flags: ctx.flags
    });
}
// Creates the common handlers for a validatable context.
function createCommonHandlers(vm) {
    if (!vm.$veeOnInput) {
        vm.$veeOnInput = function (e) {
            vm.syncValue(e); // track and keep the value updated.
            vm.setFlags({ dirty: true, pristine: false });
        };
    }
    var onInput = vm.$veeOnInput;
    if (!vm.$veeOnBlur) {
        vm.$veeOnBlur = function () {
            vm.setFlags({ touched: true, untouched: false });
        };
    }
    // Blur event listener.
    var onBlur = vm.$veeOnBlur;
    var onValidate = vm.$veeHandler;
    var mode = computeModeSetting(vm);
    // Handle debounce changes.
    if (!onValidate || vm.$veeDebounce !== vm.debounce) {
        onValidate = debounce(function () {
            vm.$nextTick(function () {
                var pendingPromise = vm.validateSilent();
                // avoids race conditions between successive validations.
                vm._pendingValidation = pendingPromise;
                pendingPromise.then(function (result) {
                    if (pendingPromise === vm._pendingValidation) {
                        vm.applyResult(result);
                        vm._pendingValidation = undefined;
                    }
                });
            });
        }, mode.debounce || vm.debounce);
        // Cache the handler so we don't create it each time.
        vm.$veeHandler = onValidate;
        // cache the debounce value so we detect if it was changed.
        vm.$veeDebounce = vm.debounce;
    }
    return { onInput: onInput, onBlur: onBlur, onValidate: onValidate };
}
// Adds all plugin listeners to the vnode.
function addListeners(vm, node) {
    var value = findValue(node);
    // cache the input eventName.
    vm._inputEventName = vm._inputEventName || getInputEventName(node, findModel(node));
    onRenderUpdate(vm, value && value.value);
    var _a = createCommonHandlers(vm), onInput = _a.onInput, onBlur = _a.onBlur, onValidate = _a.onValidate;
    addVNodeListener(node, vm._inputEventName, onInput);
    addVNodeListener(node, 'blur', onBlur);
    // add the validation listeners.
    vm.normalizedEvents.forEach(function (evt) {
        addVNodeListener(node, evt, onValidate);
    });
    vm.initialized = true;
}

var PROVIDER_COUNTER = 0;
function data() {
    var messages = [];
    var defaultValues = {
        messages: messages,
        value: undefined,
        initialized: false,
        initialValue: undefined,
        flags: createFlags(),
        failedRules: {},
        isDeactivated: false,
        id: ''
    };
    return defaultValues;
}
var ValidationProvider = vue__WEBPACK_IMPORTED_MODULE_0__["default"].extend({
    inject: {
        $_veeObserver: {
            from: '$_veeObserver',
            default: function () {
                if (!this.$vnode.context.$_veeObserver) {
                    this.$vnode.context.$_veeObserver = createObserver();
                }
                return this.$vnode.context.$_veeObserver;
            }
        }
    },
    props: {
        vid: {
            type: String,
            default: ''
        },
        name: {
            type: String,
            default: null
        },
        mode: {
            type: [String, Function],
            default: function () {
                return getConfig().mode;
            }
        },
        rules: {
            type: [Object, String],
            default: null
        },
        immediate: {
            type: Boolean,
            default: false
        },
        persist: {
            type: Boolean,
            default: false
        },
        bails: {
            type: Boolean,
            default: function () { return getConfig().bails; }
        },
        skipIfEmpty: {
            type: Boolean,
            default: function () { return getConfig().skipOptional; }
        },
        debounce: {
            type: Number,
            default: 0
        },
        tag: {
            type: String,
            default: 'span'
        },
        slim: {
            type: Boolean,
            default: false
        },
        disabled: {
            type: Boolean,
            default: false
        },
        customMessages: {
            type: Object,
            default: function () {
                return {};
            }
        }
    },
    watch: {
        rules: {
            deep: true,
            handler: function (val, oldVal) {
                this._needsValidation = !isEqual(val, oldVal);
            }
        }
    },
    data: data,
    computed: {
        fieldDeps: function () {
            var _this = this;
            return Object.keys(this.normalizedRules)
                .filter(RuleContainer.isTargetRule)
                .reduce(function (acc, rule) {
                var deps = RuleContainer.getTargetParamNames(rule, _this.normalizedRules[rule]);
                acc.push.apply(acc, deps);
                deps.forEach(function (depName) {
                    watchCrossFieldDep(_this, depName);
                });
                return acc;
            }, []);
        },
        normalizedEvents: function () {
            var _this = this;
            var on = computeModeSetting(this).on;
            return (on || []).map(function (e) {
                if (e === 'input') {
                    return _this._inputEventName;
                }
                return e;
            });
        },
        isRequired: function () {
            var rules = __assign(__assign({}, this._resolvedRules), this.normalizedRules);
            var isRequired = Object.keys(rules).some(RuleContainer.isRequireRule);
            this.flags.required = !!isRequired;
            return isRequired;
        },
        classes: function () {
            var names = getConfig().classes;
            return computeClassObj(names, this.flags);
        },
        normalizedRules: function () {
            return normalizeRules(this.rules);
        }
    },
    render: function (h) {
        var _this = this;
        this.registerField();
        var ctx = createValidationCtx(this);
        var children = normalizeChildren(this, ctx);
        // Handle single-root slot.
        extractVNodes(children).forEach(function (input) {
            _this._resolvedRules = getConfig().useConstraintAttrs ? resolveRules(input) : {};
            addListeners(_this, input);
        });
        return this.slim && children.length <= 1 ? children[0] : h(this.tag, children);
    },
    beforeDestroy: function () {
        // cleanup reference.
        this.$_veeObserver.unsubscribe(this.id);
    },
    activated: function () {
        this.$_veeObserver.subscribe(this);
        this.isDeactivated = false;
    },
    deactivated: function () {
        this.$_veeObserver.unsubscribe(this.id);
        this.isDeactivated = true;
    },
    methods: {
        setFlags: function (flags) {
            var _this = this;
            Object.keys(flags).forEach(function (flag) {
                _this.flags[flag] = flags[flag];
            });
        },
        syncValue: function (v) {
            var value = normalizeEventValue(v);
            this.value = value;
            this.flags.changed = this.initialValue !== value;
        },
        reset: function () {
            this.messages = [];
            this.initialValue = this.value;
            var flags = createFlags();
            flags.required = this.isRequired;
            this.setFlags(flags);
            this.validateSilent();
        },
        validate: function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            return __awaiter(this, void 0, void 0, function () {
                var result;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (args.length > 0) {
                                this.syncValue(args[0]);
                            }
                            return [4 /*yield*/, this.validateSilent()];
                        case 1:
                            result = _a.sent();
                            this.applyResult(result);
                            return [2 /*return*/, result];
                    }
                });
            });
        },
        validateSilent: function () {
            return __awaiter(this, void 0, void 0, function () {
                var rules, result;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            this.setFlags({ pending: true });
                            rules = __assign(__assign({}, this._resolvedRules), this.normalizedRules);
                            Object.defineProperty(rules, '_$$isNormalized', {
                                value: true,
                                writable: false,
                                enumerable: false,
                                configurable: false
                            });
                            return [4 /*yield*/, validate(this.value, rules, {
                                    name: this.name,
                                    values: createValuesLookup(this),
                                    bails: this.bails,
                                    skipIfEmpty: this.skipIfEmpty,
                                    isInitial: !this.initialized,
                                    customMessages: this.customMessages
                                })];
                        case 1:
                            result = _a.sent();
                            this.setFlags({ pending: false });
                            this.setFlags({ valid: result.valid, invalid: !result.valid });
                            return [2 /*return*/, result];
                    }
                });
            });
        },
        setErrors: function (errors) {
            this.applyResult({ errors: errors, failedRules: {} });
        },
        applyResult: function (_a) {
            var errors = _a.errors, failedRules = _a.failedRules;
            this.messages = errors;
            this.failedRules = __assign({}, (failedRules || {}));
            this.setFlags({
                valid: !errors.length,
                changed: this.value !== this.initialValue,
                invalid: !!errors.length,
                validated: true
            });
        },
        registerField: function () {
            updateRenderingContextRefs(this);
        }
    }
});
function createValuesLookup(vm) {
    var providers = vm.$_veeObserver.refs;
    var reduced = {};
    return vm.fieldDeps.reduce(function (acc, depName) {
        if (!providers[depName]) {
            return acc;
        }
        acc[depName] = providers[depName].value;
        return acc;
    }, reduced);
}
function extractId(vm) {
    if (vm.vid) {
        return vm.vid;
    }
    if (vm.name) {
        return vm.name;
    }
    if (vm.id) {
        return vm.id;
    }
    PROVIDER_COUNTER++;
    return "_vee_" + PROVIDER_COUNTER;
}
function updateRenderingContextRefs(vm) {
    var providedId = extractId(vm);
    var id = vm.id;
    // Nothing has changed.
    if (vm.isDeactivated || (id === providedId && vm.$_veeObserver.refs[id])) {
        return;
    }
    // vid was changed.
    if (id !== providedId && vm.$_veeObserver.refs[id] === vm) {
        vm.$_veeObserver.unsubscribe(id);
    }
    vm.id = providedId;
    vm.$_veeObserver.subscribe(vm);
}
function createObserver() {
    return {
        refs: {},
        subscribe: function (vm) {
            this.refs[vm.id] = vm;
        },
        unsubscribe: function (id) {
            delete this.refs[id];
        }
    };
}
function watchCrossFieldDep(ctx, depName, withHooks) {
    if (withHooks === void 0) { withHooks = true; }
    var providers = ctx.$_veeObserver.refs;
    if (!ctx._veeWatchers) {
        ctx._veeWatchers = {};
    }
    if (!providers[depName] && withHooks) {
        return ctx.$once('hook:mounted', function () {
            watchCrossFieldDep(ctx, depName, false);
        });
    }
    if (!isCallable(ctx._veeWatchers[depName]) && providers[depName]) {
        ctx._veeWatchers[depName] = providers[depName].$watch('value', function () {
            if (ctx.flags.validated) {
                ctx._needsValidation = true;
                ctx.validate();
            }
        });
    }
}

var flagMergingStrategy = {
    pristine: 'every',
    dirty: 'some',
    touched: 'some',
    untouched: 'every',
    valid: 'every',
    invalid: 'some',
    pending: 'some',
    validated: 'every',
    changed: 'some'
};
function mergeFlags(lhs, rhs, strategy) {
    var stratName = flagMergingStrategy[strategy];
    return [lhs, rhs][stratName](function (f) { return f; });
}
var OBSERVER_COUNTER = 0;
function data$1() {
    var refs = {};
    var refsByName = {};
    var inactiveRefs = {};
    // FIXME: Not sure of this one can be typed, circular type reference.
    var observers = [];
    return {
        id: '',
        refs: refs,
        refsByName: refsByName,
        observers: observers,
        inactiveRefs: inactiveRefs
    };
}
var ValidationObserver = vue__WEBPACK_IMPORTED_MODULE_0__["default"].extend({
    name: 'ValidationObserver',
    provide: function () {
        return {
            $_veeObserver: this
        };
    },
    inject: {
        $_veeObserver: {
            from: '$_veeObserver',
            default: function () {
                if (!this.$vnode.context.$_veeObserver) {
                    return null;
                }
                return this.$vnode.context.$_veeObserver;
            }
        }
    },
    props: {
        tag: {
            type: String,
            default: 'span'
        },
        vid: {
            type: String,
            default: function () {
                return "obs_" + OBSERVER_COUNTER++;
            }
        },
        slim: {
            type: Boolean,
            default: false
        },
        disabled: {
            type: Boolean,
            default: false
        }
    },
    data: data$1,
    computed: {
        ctx: function () {
            var _this = this;
            var ctx = {
                errors: {},
                passes: function (cb) {
                    return _this.validate().then(function (result) {
                        if (result) {
                            return cb();
                        }
                    });
                },
                validate: function () {
                    var args = [];
                    for (var _i = 0; _i < arguments.length; _i++) {
                        args[_i] = arguments[_i];
                    }
                    return _this.validate.apply(_this, args);
                },
                reset: function () { return _this.reset(); }
            };
            return __spreadArrays(values(this.refs), Object.keys(this.inactiveRefs).map(function (key) {
                return {
                    vid: key,
                    flags: _this.inactiveRefs[key].flags,
                    messages: _this.inactiveRefs[key].errors
                };
            }), this.observers).reduce(function (acc, provider) {
                Object.keys(flagMergingStrategy).forEach(function (flag) {
                    var flags = provider.flags || provider.ctx;
                    if (!(flag in acc)) {
                        acc[flag] = flags[flag];
                        return;
                    }
                    acc[flag] = mergeFlags(acc[flag], flags[flag], flag);
                });
                acc.errors[provider.id] =
                    provider.messages ||
                        values(provider.ctx.errors).reduce(function (errs, obsErrors) {
                            return errs.concat(obsErrors);
                        }, []);
                return acc;
            }, ctx);
        }
    },
    created: function () {
        this.id = this.vid;
        if (this.$_veeObserver) {
            this.$_veeObserver.subscribe(this, 'observer');
        }
    },
    activated: function () {
        if (this.$_veeObserver) {
            this.$_veeObserver.subscribe(this, 'observer');
        }
    },
    deactivated: function () {
        if (this.$_veeObserver) {
            this.$_veeObserver.unsubscribe(this.id, 'observer');
        }
    },
    beforeDestroy: function () {
        if (this.$_veeObserver) {
            this.$_veeObserver.unsubscribe(this.id, 'observer');
        }
    },
    render: function (h) {
        var children = normalizeChildren(this, this.ctx);
        return this.slim && children.length <= 1 ? children[0] : h(this.tag, { on: this.$listeners }, children);
    },
    methods: {
        subscribe: function (subscriber, kind) {
            var _a, _b;
            if (kind === void 0) { kind = 'provider'; }
            if (kind === 'observer') {
                this.observers.push(subscriber);
                return;
            }
            this.refs = __assign(__assign({}, this.refs), (_a = {}, _a[subscriber.id] = subscriber, _a));
            this.refsByName = __assign(__assign({}, this.refsByName), (_b = {}, _b[subscriber.name] = subscriber, _b));
            if (subscriber.persist) {
                this.restoreProviderState(subscriber);
            }
        },
        unsubscribe: function (id, kind) {
            if (kind === void 0) { kind = 'provider'; }
            if (kind === 'provider') {
                this.removeProvider(id);
                return;
            }
            var idx = findIndex(this.observers, function (o) { return o.id === id; });
            if (idx !== -1) {
                this.observers.splice(idx, 1);
            }
        },
        validate: function (_a) {
            var _b = (_a === void 0 ? {} : _a).silent, silent = _b === void 0 ? false : _b;
            return __awaiter(this, void 0, void 0, function () {
                var results;
                return __generator(this, function (_c) {
                    switch (_c.label) {
                        case 0: return [4 /*yield*/, Promise.all(__spreadArrays(values(this.refs)
                                .filter(function (r) { return !r.disabled; })
                                .map(function (ref) { return ref[silent ? 'validateSilent' : 'validate']().then(function (r) { return r.valid; }); }), this.observers.filter(function (o) { return !o.disabled; }).map(function (obs) { return obs.validate({ silent: silent }); })))];
                        case 1:
                            results = _c.sent();
                            return [2 /*return*/, results.every(function (r) { return r; })];
                    }
                });
            });
        },
        reset: function () {
            var _this = this;
            Object.keys(this.inactiveRefs).forEach(function (key) {
                _this.$delete(_this.inactiveRefs, key);
            });
            return __spreadArrays(values(this.refs), this.observers).forEach(function (ref) { return ref.reset(); });
        },
        restoreProviderState: function (provider) {
            var id = provider.id;
            var state = this.inactiveRefs[id];
            if (!state) {
                return;
            }
            provider.setFlags(state.flags);
            provider.applyResult(state);
            this.$delete(this.inactiveRefs, provider.id);
        },
        removeProvider: function (id) {
            var provider = this.refs[id];
            if (!provider) {
                // FIXME: inactive refs are not being cleaned up.
                return;
            }
            if (provider.persist) {
                /* istanbul ignore next */
                if (true) {
                    if (id.indexOf('_vee_') === 0) {
                        warn('Please provide a `vid` or a `name` prop when using `persist`, there might be unexpected issues otherwise.');
                    }
                }
                // save it for the next time.
                this.inactiveRefs[id] = {
                    flags: provider.flags,
                    errors: provider.messages,
                    failedRules: provider.failedRules
                };
            }
            this.$delete(this.refs, id);
            this.$delete(this.refsByName, provider.name);
        },
        setErrors: function (errors) {
            var _this = this;
            Object.keys(errors).forEach(function (key) {
                var provider = _this.refs[key] || _this.refsByName[key];
                if (!provider)
                    return;
                provider.setErrors(errors[key] || []);
            });
            this.observers.forEach(function (observer) {
                observer.setErrors(errors);
            });
        }
    }
});

function withValidation(component, mapProps) {
    if (mapProps === void 0) { mapProps = identity; }
    var options = 'options' in component ? component.options : component;
    var providerOpts = ValidationProvider.options;
    var hoc = {
        name: (options.name || 'AnonymousHoc') + "WithValidation",
        props: __assign({}, providerOpts.props),
        data: providerOpts.data,
        computed: __assign({}, providerOpts.computed),
        methods: __assign({}, providerOpts.methods),
        beforeDestroy: providerOpts.beforeDestroy,
        inject: providerOpts.inject
    };
    var eventName = (options.model && options.model.event) || 'input';
    hoc.render = function (h) {
        var _a;
        this.registerField();
        var vctx = createValidationCtx(this);
        var listeners = __assign({}, this.$listeners);
        var model = findModel(this.$vnode);
        this._inputEventName = this._inputEventName || getInputEventName(this.$vnode, model);
        var value = findValue(this.$vnode);
        onRenderUpdate(this, value && value.value);
        var _b = createCommonHandlers(this), onInput = _b.onInput, onBlur = _b.onBlur, onValidate = _b.onValidate;
        mergeVNodeListeners(listeners, eventName, onInput);
        mergeVNodeListeners(listeners, 'blur', onBlur);
        this.normalizedEvents.forEach(function (evt) {
            mergeVNodeListeners(listeners, evt, onValidate);
        });
        // Props are any attrs not associated with ValidationProvider Plus the model prop.
        // WARNING: Accidental prop overwrite will probably happen.
        var prop = (findModelConfig(this.$vnode) || { prop: 'value' }).prop;
        var props = __assign(__assign(__assign({}, this.$attrs), (_a = {}, _a[prop] = model && model.value, _a)), mapProps(vctx));
        return h(options, {
            attrs: this.$attrs,
            props: props,
            on: listeners
        }, normalizeSlots(this.$slots, this.$vnode.context));
    };
    return hoc;
}

var version = '3.0.11';




/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/global/TheModalFullScreen.vue?vue&type=template&id=1e69cc63&":
/*!********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./src/vue/partial/global/TheModalFullScreen.vue?vue&type=template&id=1e69cc63& ***!
  \********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "modal fade modal-fs", attrs: { id: "modal-fs" } },
    [
      _c("div", { staticClass: "modal-dialog", attrs: { role: "document" } }, [
        _c("div", { staticClass: "modal-content" }, [
          _c("div", { staticClass: "modal-body" }, [
            _c("div", { staticClass: "container text-center" }, [
              _c("div", { staticClass: "row full-height align-items-center" }, [
                _c("div", { staticClass: "col-md-5 m-h-auto" }, [
                  _c("div", { staticClass: "p-h-30 p-b-50" }, [
                    _vm._m(0),
                    _vm._v(" "),
                    _c("h1", { staticClass: "text-thin m-v-15" }, [
                      _vm._v(_vm._s(_vm.title))
                    ]),
                    _vm._v(" "),
                    _c("p", { staticClass: "m-b-25 lead" }, [
                      _vm._v(_vm._s(_vm.content))
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "text-center" }, [
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-default btn-lg",
                          on: {
                            click: function($event) {
                              $event.preventDefault()
                              return _vm.confirm($event)
                            }
                          }
                        },
                        [_vm._v("Confirm\n                  ")]
                      )
                    ])
                  ])
                ])
              ])
            ]),
            _vm._v(" "),
            _c(
              "a",
              {
                staticClass: "modal-close",
                attrs: { href: "#", "data-dismiss": "modal" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                    return _vm.confirm($event)
                  }
                }
              },
              [_c("i", { staticClass: "ti-close" })]
            )
          ])
        ])
      ])
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "text-center font-size-70" }, [
      _c("i", {
        staticClass:
          "mdi mdi-checkbox-marked-circle-outline icon-gradient-success"
      })
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/role/App.vue?vue&type=template&id=6df09af8&":
/*!***************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./src/vue/partial/role/App.vue?vue&type=template&id=6df09af8& ***!
  \***************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("router-view")
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/role/RoleAddSection.vue?vue&type=template&id=36d1e993&":
/*!**************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./src/vue/partial/role/RoleAddSection.vue?vue&type=template&id=36d1e993& ***!
  \**************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container-fluid" }, [
    _c("div", { staticClass: "page-header" }, [
      _c("h2", { staticClass: "header-title" }, [_vm._v("Người dùng")]),
      _vm._v(" "),
      _c("div", { staticClass: "header-sub-title" }, [
        _c("nav", { staticClass: "breadcrumb breadcrumb-dash" }, [
          _vm._m(0),
          _vm._v(" "),
          _c(
            "a",
            {
              staticClass: "breadcrumb-item",
              attrs: { href: "#" },
              on: {
                click: function($event) {
                  $event.preventDefault()
                  return _vm.goRead()
                }
              }
            },
            [_c("i", { staticClass: "ti-role p-r-5" }), _vm._v("Nhóm")]
          ),
          _vm._v(" "),
          _vm._m(1)
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "_header-button" }, [
        _c(
          "button",
          {
            staticClass: "btn btn-gradient-success",
            on: {
              click: function($event) {
                $event.preventDefault()
                return _vm.goCreate()
              }
            }
          },
          [_vm._v("Tạo người dùng\n      ")]
        )
      ])
    ]),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "card" },
      [
        _c("validation-observer", {
          ref: "observer",
          staticClass: "card-body",
          attrs: { tag: "div" },
          scopedSlots: _vm._u([
            {
              key: "default",
              fn: function(ref) {
                var invalid = ref.invalid
                return [
                  _c(
                    "div",
                    {
                      staticClass: "row m-v-30",
                      on: {
                        keyup: function($event) {
                          if (
                            !$event.type.indexOf("key") &&
                            _vm._k(
                              $event.keyCode,
                              "enter",
                              13,
                              $event.key,
                              "Enter"
                            )
                          ) {
                            return null
                          }
                          return _vm.update(invalid)
                        }
                      }
                    },
                    [
                      _c("div", {
                        staticClass: "col-sm-3 text-center text-sm-left"
                      }),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-6 text-center text-sm-left" },
                        [
                          _c("validation-provider", {
                            staticClass: "form-group",
                            attrs: {
                              name: "Tên nhóm quyền",
                              rules: "required",
                              tag: "div"
                            },
                            scopedSlots: _vm._u(
                              [
                                {
                                  key: "default",
                                  fn: function(ref) {
                                    var errors = ref.errors
                                    var valid = ref.valid
                                    return [
                                      _c(
                                        "label",
                                        {
                                          staticClass: "control-label",
                                          attrs: { for: "icon-input" }
                                        },
                                        [_vm._v("Name:")]
                                      ),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "icon-input" }, [
                                        _c("i", {
                                          staticClass: "mdi mdi-account"
                                        }),
                                        _vm._v(" "),
                                        _c("input", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: _vm.frmData.role_name,
                                              expression: "frmData.role_name"
                                            }
                                          ],
                                          staticClass: "form-control",
                                          class: {
                                            error:
                                              _vm.flag.isValidating && !valid
                                          },
                                          attrs: {
                                            type: "text",
                                            placeholder: "Role name..."
                                          },
                                          domProps: {
                                            value: _vm.frmData.role_name
                                          },
                                          on: {
                                            input: function($event) {
                                              if ($event.target.composing) {
                                                return
                                              }
                                              _vm.$set(
                                                _vm.frmData,
                                                "role_name",
                                                $event.target.value
                                              )
                                            }
                                          }
                                        })
                                      ]),
                                      _vm._v(" "),
                                      _c(
                                        "label",
                                        {
                                          directives: [
                                            {
                                              name: "show",
                                              rawName: "v-show",
                                              value:
                                                _vm.flag.isValidating && !valid,
                                              expression:
                                                "flag.isValidating && !valid"
                                            }
                                          ],
                                          staticClass: "error"
                                        },
                                        [_vm._v(_vm._s(errors[0]))]
                                      )
                                    ]
                                  }
                                }
                              ],
                              null,
                              true
                            )
                          }),
                          _vm._v(" "),
                          _c("div", { staticClass: "checkbox" }, [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.frmData.is_fullaccess,
                                  expression: "frmData.is_fullaccess"
                                }
                              ],
                              attrs: {
                                id: "checkbox1",
                                type: "checkbox",
                                "true-value": "1",
                                "false-value": "0"
                              },
                              domProps: {
                                checked: Array.isArray(
                                  _vm.frmData.is_fullaccess
                                )
                                  ? _vm._i(_vm.frmData.is_fullaccess, null) > -1
                                  : _vm._q(_vm.frmData.is_fullaccess, "1")
                              },
                              on: {
                                change: function($event) {
                                  var $$a = _vm.frmData.is_fullaccess,
                                    $$el = $event.target,
                                    $$c = $$el.checked ? "1" : "0"
                                  if (Array.isArray($$a)) {
                                    var $$v = null,
                                      $$i = _vm._i($$a, $$v)
                                    if ($$el.checked) {
                                      $$i < 0 &&
                                        _vm.$set(
                                          _vm.frmData,
                                          "is_fullaccess",
                                          $$a.concat([$$v])
                                        )
                                    } else {
                                      $$i > -1 &&
                                        _vm.$set(
                                          _vm.frmData,
                                          "is_fullaccess",
                                          $$a
                                            .slice(0, $$i)
                                            .concat($$a.slice($$i + 1))
                                        )
                                    }
                                  } else {
                                    _vm.$set(_vm.frmData, "is_fullaccess", $$c)
                                  }
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c("label", { attrs: { for: "checkbox1" } }, [
                              _vm._v("Kích hoạt")
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "m-t-25" }, [
                            _c(
                              "button",
                              {
                                staticClass: "btn btn-success",
                                attrs: {
                                  id: "save",
                                  type: "button",
                                  disabled: _vm.flag.isUpdating
                                },
                                on: {
                                  click: function($event) {
                                    $event.preventDefault()
                                    return _vm.update(invalid)
                                  }
                                }
                              },
                              [
                                _vm.flag.isUpdating
                                  ? [
                                      _c("i", {
                                        staticClass:
                                          "fa fa-spinner fa-pulse fa-fw m-r-5"
                                      }),
                                      _vm._v("Updating\n              ")
                                    ]
                                  : [_vm._v("Update")]
                              ],
                              2
                            ),
                            _vm._v(" "),
                            _c(
                              "button",
                              {
                                staticClass: "btn btn-default",
                                attrs: { id: "edit", type: "button" },
                                on: {
                                  click: function($event) {
                                    $event.preventDefault()
                                    return _vm.back($event)
                                  }
                                }
                              },
                              [
                                _c("i", { staticClass: "ti-back-left p-r-10" }),
                                _vm._v("Back\n            ")
                              ]
                            )
                          ])
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c("div", { staticClass: "col" })
                    ]
                  )
                ]
              }
            }
          ])
        })
      ],
      1
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "a",
      { staticClass: "breadcrumb-item", attrs: { href: "/backend" } },
      [_c("i", { staticClass: "ti-home p-r-5" }), _vm._v("Hệ thống")]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "breadcrumb-item active" }, [
      _c("i", { staticClass: "ti-pencil-alt p-r-5" }),
      _vm._v("Tạo Nhóm")
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/role/RoleEditSection.vue?vue&type=template&id=f0ba3908&":
/*!***************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./src/vue/partial/role/RoleEditSection.vue?vue&type=template&id=f0ba3908& ***!
  \***************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container-fluid" }, [
    _c("div", { staticClass: "page-header" }, [
      _c("h2", { staticClass: "header-title" }, [_vm._v("Người dùng")]),
      _vm._v(" "),
      _c("div", { staticClass: "header-sub-title" }, [
        _c("nav", { staticClass: "breadcrumb breadcrumb-dash" }, [
          _vm._m(0),
          _vm._v(" "),
          _c(
            "a",
            {
              staticClass: "breadcrumb-item",
              attrs: { href: "#" },
              on: {
                click: function($event) {
                  $event.preventDefault()
                  return _vm.goRead()
                }
              }
            },
            [_c("i", { staticClass: "ti-role p-r-5" }), _vm._v("Nhóm")]
          ),
          _vm._v(" "),
          _vm._m(1)
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "_header-button" }, [
        _c(
          "button",
          {
            staticClass: "btn btn-gradient-success",
            on: {
              click: function($event) {
                $event.preventDefault()
                return _vm.goCreate()
              }
            }
          },
          [_vm._v("Tạo nhóm\n      ")]
        )
      ])
    ]),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "card" },
      [
        _c("validation-observer", {
          ref: "observer",
          staticClass: "card-body",
          attrs: { tag: "div" },
          scopedSlots: _vm._u([
            {
              key: "default",
              fn: function(ref) {
                var invalid = ref.invalid
                return [
                  _c(
                    "div",
                    {
                      staticClass: "row m-v-30",
                      on: {
                        keyup: function($event) {
                          if (
                            !$event.type.indexOf("key") &&
                            _vm._k(
                              $event.keyCode,
                              "enter",
                              13,
                              $event.key,
                              "Enter"
                            )
                          ) {
                            return null
                          }
                          return _vm.update(invalid)
                        }
                      }
                    },
                    [
                      _c("div", {
                        staticClass: "col-sm-3 text-center text-sm-left"
                      }),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-sm-6 text-center text-sm-left" },
                        [
                          _c("validation-provider", {
                            staticClass: "form-group",
                            attrs: {
                              name: "Tên nhóm quyền",
                              rules: "required",
                              tag: "div"
                            },
                            scopedSlots: _vm._u(
                              [
                                {
                                  key: "default",
                                  fn: function(ref) {
                                    var errors = ref.errors
                                    var valid = ref.valid
                                    return [
                                      _c(
                                        "label",
                                        {
                                          staticClass: "control-label",
                                          attrs: { for: "icon-input" }
                                        },
                                        [_vm._v("Name:")]
                                      ),
                                      _vm._v(" "),
                                      _c("div", { staticClass: "icon-input" }, [
                                        _c("i", {
                                          staticClass: "mdi mdi-account"
                                        }),
                                        _vm._v(" "),
                                        _c("input", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: _vm.frmData.role_name,
                                              expression: "frmData.role_name"
                                            }
                                          ],
                                          staticClass: "form-control",
                                          class: {
                                            error:
                                              _vm.flag.isValidating && !valid
                                          },
                                          attrs: {
                                            type: "text",
                                            placeholder: "Role name..."
                                          },
                                          domProps: {
                                            value: _vm.frmData.role_name
                                          },
                                          on: {
                                            input: function($event) {
                                              if ($event.target.composing) {
                                                return
                                              }
                                              _vm.$set(
                                                _vm.frmData,
                                                "role_name",
                                                $event.target.value
                                              )
                                            }
                                          }
                                        })
                                      ]),
                                      _vm._v(" "),
                                      _c(
                                        "label",
                                        {
                                          directives: [
                                            {
                                              name: "show",
                                              rawName: "v-show",
                                              value:
                                                _vm.flag.isValidating && !valid,
                                              expression:
                                                "flag.isValidating && !valid"
                                            }
                                          ],
                                          staticClass: "error"
                                        },
                                        [_vm._v(_vm._s(errors[0]))]
                                      )
                                    ]
                                  }
                                }
                              ],
                              null,
                              true
                            )
                          }),
                          _vm._v(" "),
                          _c("div", { staticClass: "checkbox" }, [
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.frmData.is_fullaccess,
                                  expression: "frmData.is_fullaccess"
                                }
                              ],
                              attrs: {
                                id: "checkbox1",
                                type: "checkbox",
                                "true-value": "1",
                                "false-value": "0"
                              },
                              domProps: {
                                checked: Array.isArray(
                                  _vm.frmData.is_fullaccess
                                )
                                  ? _vm._i(_vm.frmData.is_fullaccess, null) > -1
                                  : _vm._q(_vm.frmData.is_fullaccess, "1")
                              },
                              on: {
                                change: function($event) {
                                  var $$a = _vm.frmData.is_fullaccess,
                                    $$el = $event.target,
                                    $$c = $$el.checked ? "1" : "0"
                                  if (Array.isArray($$a)) {
                                    var $$v = null,
                                      $$i = _vm._i($$a, $$v)
                                    if ($$el.checked) {
                                      $$i < 0 &&
                                        _vm.$set(
                                          _vm.frmData,
                                          "is_fullaccess",
                                          $$a.concat([$$v])
                                        )
                                    } else {
                                      $$i > -1 &&
                                        _vm.$set(
                                          _vm.frmData,
                                          "is_fullaccess",
                                          $$a
                                            .slice(0, $$i)
                                            .concat($$a.slice($$i + 1))
                                        )
                                    }
                                  } else {
                                    _vm.$set(_vm.frmData, "is_fullaccess", $$c)
                                  }
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c("label", { attrs: { for: "checkbox1" } }, [
                              _vm._v("Kích hoạt")
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "m-t-25" }, [
                            _c(
                              "button",
                              {
                                staticClass: "btn btn-success",
                                attrs: {
                                  id: "save",
                                  type: "button",
                                  disabled: _vm.flag.isUpdating
                                },
                                on: {
                                  click: function($event) {
                                    $event.preventDefault()
                                    return _vm.update(invalid)
                                  }
                                }
                              },
                              [
                                _vm.flag.isUpdating
                                  ? [
                                      _c("i", {
                                        staticClass:
                                          "fa fa-spinner fa-pulse fa-fw m-r-5"
                                      }),
                                      _vm._v("Updating\n              ")
                                    ]
                                  : [_vm._v("Update")]
                              ],
                              2
                            ),
                            _vm._v(" "),
                            _c(
                              "button",
                              {
                                staticClass: "btn btn-default",
                                attrs: { id: "edit", type: "button" },
                                on: {
                                  click: function($event) {
                                    $event.preventDefault()
                                    return _vm.back($event)
                                  }
                                }
                              },
                              [
                                _c("i", { staticClass: "ti-back-left p-r-10" }),
                                _vm._v("Back\n            ")
                              ]
                            )
                          ])
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c("div", { staticClass: "col" })
                    ]
                  )
                ]
              }
            }
          ])
        })
      ],
      1
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "a",
      { staticClass: "breadcrumb-item", attrs: { href: "/backend" } },
      [_c("i", { staticClass: "ti-home p-r-5" }), _vm._v("Hệ thống")]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "breadcrumb-item active" }, [
      _c("i", { staticClass: "ti-pencil-alt p-r-5" }),
      _vm._v("Cập nhật Nhóm")
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/role/RoleIndexSection.vue?vue&type=template&id=05923f02&":
/*!****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./src/vue/partial/role/RoleIndexSection.vue?vue&type=template&id=05923f02& ***!
  \****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container-fluid" }, [
    _c("div", { staticClass: "page-header" }, [
      _c("h2", { staticClass: "header-title" }, [_vm._v("Người dùng")]),
      _vm._v(" "),
      _c("div", { staticClass: "header-sub-title" }, [
        _c("nav", { staticClass: "breadcrumb breadcrumb-dash" }, [
          _vm._m(0),
          _vm._v(" "),
          _c(
            "a",
            {
              staticClass: "breadcrumb-item",
              attrs: { href: "#" },
              on: {
                click: function($event) {
                  $event.preventDefault()
                }
              }
            },
            [_c("i", { staticClass: "ti-role p-r-5" }), _vm._v("Nhóm")]
          ),
          _vm._v(" "),
          _vm._m(1)
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "_header-button" }, [
        _c(
          "button",
          {
            staticClass: "btn btn-gradient-success",
            on: {
              click: function($event) {
                $event.preventDefault()
                return _vm.goCreate()
              }
            }
          },
          [_vm._v("Tạo nhóm\n      ")]
        )
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "card" }, [
      _c("div", { staticClass: "card-body" }, [_c("role-table")], 1)
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "a",
      { staticClass: "breadcrumb-item", attrs: { href: "/backend" } },
      [_c("i", { staticClass: "ti-home p-r-5" }), _vm._v("Hệ thống")]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "breadcrumb-item active" }, [
      _c("i", { staticClass: "ti-list p-r-5" }),
      _vm._v("Danh sách")
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/role/RoleTable.vue?vue&type=template&id=4065370f&":
/*!*********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./src/vue/partial/role/RoleTable.vue?vue&type=template&id=4065370f& ***!
  \*********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "table-overflow", attrs: { id: "role-table" } },
    [
      _c(
        "table",
        { staticClass: "table table-hover table-xl", attrs: { id: "dt-opt" } },
        [
          _vm._m(0),
          _vm._v(" "),
          _c(
            "tbody",
            [
              _vm._l(_vm.arrEntityList, function(objEntity, index) {
                return [
                  _c("role-table-row", {
                    attrs: { index: index, "obj-entity": objEntity }
                  })
                ]
              })
            ],
            2
          )
        ]
      )
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [
          _c("div", { staticClass: "checkbox p-0" }, [
            _c("input", {
              staticClass: "checkAll",
              attrs: { id: "selectable1", type: "checkbox", name: "checkAll" }
            }),
            _vm._v(" "),
            _c("label", { attrs: { for: "selectable1" } })
          ])
        ]),
        _vm._v(" "),
        _c("th", [_vm._v("ROLE")]),
        _vm._v(" "),
        _c("th", [_vm._v("STATUS")]),
        _vm._v(" "),
        _c("th", [_vm._v("#")]),
        _vm._v(" "),
        _c("th")
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/role/RoleTableRow.vue?vue&type=template&id=26f59b7b&":
/*!************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./src/vue/partial/role/RoleTableRow.vue?vue&type=template&id=26f59b7b& ***!
  \************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("tr", [
    _vm._m(0),
    _vm._v(" "),
    _c("td", [_vm._v(_vm._s(_vm.objEntity.role_name))]),
    _vm._v(" "),
    _c(
      "td",
      [
        _vm.objEntity.is_fullaccess == 1
          ? [
              _c(
                "span",
                { staticClass: "badge badge-pill badge-gradient-success" },
                [_vm._v("Đầy đủ quyền hạn")]
              )
            ]
          : [
              _c(
                "span",
                { staticClass: "badge badge-pill badge-gradient-danger" },
                [_vm._v("Chưa đầy đủ quyền hạn")]
              )
            ]
      ],
      2
    ),
    _vm._v(" "),
    _c("td", { staticClass: "text-center font-size-18" }, [
      _c(
        "a",
        {
          staticClass: "text-gray m-r-15",
          on: {
            click: function($event) {
              return _vm.goUpdate()
            }
          }
        },
        [_c("i", { staticClass: "ti-pencil" })]
      ),
      _vm._v(" "),
      _c(
        "a",
        {
          staticClass: "text-gray",
          on: {
            click: function($event) {
              return _vm.remove(_vm.objEntity.role_id)
            }
          }
        },
        [
          _c("i", {
            class: {
              "fa fa-spinner fa-pulse fa-fw": _vm.flag.isDeleting,
              "ti-trash": !_vm.flag.isDeleting
            }
          })
        ]
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("td", [
      _c("div", { staticClass: "checkbox" }, [
        _c("input", { attrs: { id: "selectable2", type: "checkbox" } }),
        _vm._v(" "),
        _c("label", { attrs: { for: "selectable2" } })
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-router/dist/vue-router.esm.js":
/*!********************************************************!*\
  !*** ./node_modules/vue-router/dist/vue-router.esm.js ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/*!
  * vue-router v3.1.3
  * (c) 2019 Evan You
  * @license MIT
  */
/*  */

function assert (condition, message) {
  if (!condition) {
    throw new Error(("[vue-router] " + message))
  }
}

function warn (condition, message) {
  if ( true && !condition) {
    typeof console !== 'undefined' && console.warn(("[vue-router] " + message));
  }
}

function isError (err) {
  return Object.prototype.toString.call(err).indexOf('Error') > -1
}

function isExtendedError (constructor, err) {
  return (
    err instanceof constructor ||
    // _name is to support IE9 too
    (err && (err.name === constructor.name || err._name === constructor._name))
  )
}

function extend (a, b) {
  for (var key in b) {
    a[key] = b[key];
  }
  return a
}

var View = {
  name: 'RouterView',
  functional: true,
  props: {
    name: {
      type: String,
      default: 'default'
    }
  },
  render: function render (_, ref) {
    var props = ref.props;
    var children = ref.children;
    var parent = ref.parent;
    var data = ref.data;

    // used by devtools to display a router-view badge
    data.routerView = true;

    // directly use parent context's createElement() function
    // so that components rendered by router-view can resolve named slots
    var h = parent.$createElement;
    var name = props.name;
    var route = parent.$route;
    var cache = parent._routerViewCache || (parent._routerViewCache = {});

    // determine current view depth, also check to see if the tree
    // has been toggled inactive but kept-alive.
    var depth = 0;
    var inactive = false;
    while (parent && parent._routerRoot !== parent) {
      var vnodeData = parent.$vnode && parent.$vnode.data;
      if (vnodeData) {
        if (vnodeData.routerView) {
          depth++;
        }
        if (vnodeData.keepAlive && parent._inactive) {
          inactive = true;
        }
      }
      parent = parent.$parent;
    }
    data.routerViewDepth = depth;

    // render previous view if the tree is inactive and kept-alive
    if (inactive) {
      return h(cache[name], data, children)
    }

    var matched = route.matched[depth];
    // render empty node if no matched route
    if (!matched) {
      cache[name] = null;
      return h()
    }

    var component = cache[name] = matched.components[name];

    // attach instance registration hook
    // this will be called in the instance's injected lifecycle hooks
    data.registerRouteInstance = function (vm, val) {
      // val could be undefined for unregistration
      var current = matched.instances[name];
      if (
        (val && current !== vm) ||
        (!val && current === vm)
      ) {
        matched.instances[name] = val;
      }
    }

    // also register instance in prepatch hook
    // in case the same component instance is reused across different routes
    ;(data.hook || (data.hook = {})).prepatch = function (_, vnode) {
      matched.instances[name] = vnode.componentInstance;
    };

    // register instance in init hook
    // in case kept-alive component be actived when routes changed
    data.hook.init = function (vnode) {
      if (vnode.data.keepAlive &&
        vnode.componentInstance &&
        vnode.componentInstance !== matched.instances[name]
      ) {
        matched.instances[name] = vnode.componentInstance;
      }
    };

    // resolve props
    var propsToPass = data.props = resolveProps(route, matched.props && matched.props[name]);
    if (propsToPass) {
      // clone to prevent mutation
      propsToPass = data.props = extend({}, propsToPass);
      // pass non-declared props as attrs
      var attrs = data.attrs = data.attrs || {};
      for (var key in propsToPass) {
        if (!component.props || !(key in component.props)) {
          attrs[key] = propsToPass[key];
          delete propsToPass[key];
        }
      }
    }

    return h(component, data, children)
  }
};

function resolveProps (route, config) {
  switch (typeof config) {
    case 'undefined':
      return
    case 'object':
      return config
    case 'function':
      return config(route)
    case 'boolean':
      return config ? route.params : undefined
    default:
      if (true) {
        warn(
          false,
          "props in \"" + (route.path) + "\" is a " + (typeof config) + ", " +
          "expecting an object, function or boolean."
        );
      }
  }
}

/*  */

var encodeReserveRE = /[!'()*]/g;
var encodeReserveReplacer = function (c) { return '%' + c.charCodeAt(0).toString(16); };
var commaRE = /%2C/g;

// fixed encodeURIComponent which is more conformant to RFC3986:
// - escapes [!'()*]
// - preserve commas
var encode = function (str) { return encodeURIComponent(str)
  .replace(encodeReserveRE, encodeReserveReplacer)
  .replace(commaRE, ','); };

var decode = decodeURIComponent;

function resolveQuery (
  query,
  extraQuery,
  _parseQuery
) {
  if ( extraQuery === void 0 ) extraQuery = {};

  var parse = _parseQuery || parseQuery;
  var parsedQuery;
  try {
    parsedQuery = parse(query || '');
  } catch (e) {
     true && warn(false, e.message);
    parsedQuery = {};
  }
  for (var key in extraQuery) {
    parsedQuery[key] = extraQuery[key];
  }
  return parsedQuery
}

function parseQuery (query) {
  var res = {};

  query = query.trim().replace(/^(\?|#|&)/, '');

  if (!query) {
    return res
  }

  query.split('&').forEach(function (param) {
    var parts = param.replace(/\+/g, ' ').split('=');
    var key = decode(parts.shift());
    var val = parts.length > 0
      ? decode(parts.join('='))
      : null;

    if (res[key] === undefined) {
      res[key] = val;
    } else if (Array.isArray(res[key])) {
      res[key].push(val);
    } else {
      res[key] = [res[key], val];
    }
  });

  return res
}

function stringifyQuery (obj) {
  var res = obj ? Object.keys(obj).map(function (key) {
    var val = obj[key];

    if (val === undefined) {
      return ''
    }

    if (val === null) {
      return encode(key)
    }

    if (Array.isArray(val)) {
      var result = [];
      val.forEach(function (val2) {
        if (val2 === undefined) {
          return
        }
        if (val2 === null) {
          result.push(encode(key));
        } else {
          result.push(encode(key) + '=' + encode(val2));
        }
      });
      return result.join('&')
    }

    return encode(key) + '=' + encode(val)
  }).filter(function (x) { return x.length > 0; }).join('&') : null;
  return res ? ("?" + res) : ''
}

/*  */

var trailingSlashRE = /\/?$/;

function createRoute (
  record,
  location,
  redirectedFrom,
  router
) {
  var stringifyQuery = router && router.options.stringifyQuery;

  var query = location.query || {};
  try {
    query = clone(query);
  } catch (e) {}

  var route = {
    name: location.name || (record && record.name),
    meta: (record && record.meta) || {},
    path: location.path || '/',
    hash: location.hash || '',
    query: query,
    params: location.params || {},
    fullPath: getFullPath(location, stringifyQuery),
    matched: record ? formatMatch(record) : []
  };
  if (redirectedFrom) {
    route.redirectedFrom = getFullPath(redirectedFrom, stringifyQuery);
  }
  return Object.freeze(route)
}

function clone (value) {
  if (Array.isArray(value)) {
    return value.map(clone)
  } else if (value && typeof value === 'object') {
    var res = {};
    for (var key in value) {
      res[key] = clone(value[key]);
    }
    return res
  } else {
    return value
  }
}

// the starting route that represents the initial state
var START = createRoute(null, {
  path: '/'
});

function formatMatch (record) {
  var res = [];
  while (record) {
    res.unshift(record);
    record = record.parent;
  }
  return res
}

function getFullPath (
  ref,
  _stringifyQuery
) {
  var path = ref.path;
  var query = ref.query; if ( query === void 0 ) query = {};
  var hash = ref.hash; if ( hash === void 0 ) hash = '';

  var stringify = _stringifyQuery || stringifyQuery;
  return (path || '/') + stringify(query) + hash
}

function isSameRoute (a, b) {
  if (b === START) {
    return a === b
  } else if (!b) {
    return false
  } else if (a.path && b.path) {
    return (
      a.path.replace(trailingSlashRE, '') === b.path.replace(trailingSlashRE, '') &&
      a.hash === b.hash &&
      isObjectEqual(a.query, b.query)
    )
  } else if (a.name && b.name) {
    return (
      a.name === b.name &&
      a.hash === b.hash &&
      isObjectEqual(a.query, b.query) &&
      isObjectEqual(a.params, b.params)
    )
  } else {
    return false
  }
}

function isObjectEqual (a, b) {
  if ( a === void 0 ) a = {};
  if ( b === void 0 ) b = {};

  // handle null value #1566
  if (!a || !b) { return a === b }
  var aKeys = Object.keys(a);
  var bKeys = Object.keys(b);
  if (aKeys.length !== bKeys.length) {
    return false
  }
  return aKeys.every(function (key) {
    var aVal = a[key];
    var bVal = b[key];
    // check nested equality
    if (typeof aVal === 'object' && typeof bVal === 'object') {
      return isObjectEqual(aVal, bVal)
    }
    return String(aVal) === String(bVal)
  })
}

function isIncludedRoute (current, target) {
  return (
    current.path.replace(trailingSlashRE, '/').indexOf(
      target.path.replace(trailingSlashRE, '/')
    ) === 0 &&
    (!target.hash || current.hash === target.hash) &&
    queryIncludes(current.query, target.query)
  )
}

function queryIncludes (current, target) {
  for (var key in target) {
    if (!(key in current)) {
      return false
    }
  }
  return true
}

/*  */

function resolvePath (
  relative,
  base,
  append
) {
  var firstChar = relative.charAt(0);
  if (firstChar === '/') {
    return relative
  }

  if (firstChar === '?' || firstChar === '#') {
    return base + relative
  }

  var stack = base.split('/');

  // remove trailing segment if:
  // - not appending
  // - appending to trailing slash (last segment is empty)
  if (!append || !stack[stack.length - 1]) {
    stack.pop();
  }

  // resolve relative path
  var segments = relative.replace(/^\//, '').split('/');
  for (var i = 0; i < segments.length; i++) {
    var segment = segments[i];
    if (segment === '..') {
      stack.pop();
    } else if (segment !== '.') {
      stack.push(segment);
    }
  }

  // ensure leading slash
  if (stack[0] !== '') {
    stack.unshift('');
  }

  return stack.join('/')
}

function parsePath (path) {
  var hash = '';
  var query = '';

  var hashIndex = path.indexOf('#');
  if (hashIndex >= 0) {
    hash = path.slice(hashIndex);
    path = path.slice(0, hashIndex);
  }

  var queryIndex = path.indexOf('?');
  if (queryIndex >= 0) {
    query = path.slice(queryIndex + 1);
    path = path.slice(0, queryIndex);
  }

  return {
    path: path,
    query: query,
    hash: hash
  }
}

function cleanPath (path) {
  return path.replace(/\/\//g, '/')
}

var isarray = Array.isArray || function (arr) {
  return Object.prototype.toString.call(arr) == '[object Array]';
};

/**
 * Expose `pathToRegexp`.
 */
var pathToRegexp_1 = pathToRegexp;
var parse_1 = parse;
var compile_1 = compile;
var tokensToFunction_1 = tokensToFunction;
var tokensToRegExp_1 = tokensToRegExp;

/**
 * The main path matching regexp utility.
 *
 * @type {RegExp}
 */
var PATH_REGEXP = new RegExp([
  // Match escaped characters that would otherwise appear in future matches.
  // This allows the user to escape special characters that won't transform.
  '(\\\\.)',
  // Match Express-style parameters and un-named parameters with a prefix
  // and optional suffixes. Matches appear as:
  //
  // "/:test(\\d+)?" => ["/", "test", "\d+", undefined, "?", undefined]
  // "/route(\\d+)"  => [undefined, undefined, undefined, "\d+", undefined, undefined]
  // "/*"            => ["/", undefined, undefined, undefined, undefined, "*"]
  '([\\/.])?(?:(?:\\:(\\w+)(?:\\(((?:\\\\.|[^\\\\()])+)\\))?|\\(((?:\\\\.|[^\\\\()])+)\\))([+*?])?|(\\*))'
].join('|'), 'g');

/**
 * Parse a string for the raw tokens.
 *
 * @param  {string}  str
 * @param  {Object=} options
 * @return {!Array}
 */
function parse (str, options) {
  var tokens = [];
  var key = 0;
  var index = 0;
  var path = '';
  var defaultDelimiter = options && options.delimiter || '/';
  var res;

  while ((res = PATH_REGEXP.exec(str)) != null) {
    var m = res[0];
    var escaped = res[1];
    var offset = res.index;
    path += str.slice(index, offset);
    index = offset + m.length;

    // Ignore already escaped sequences.
    if (escaped) {
      path += escaped[1];
      continue
    }

    var next = str[index];
    var prefix = res[2];
    var name = res[3];
    var capture = res[4];
    var group = res[5];
    var modifier = res[6];
    var asterisk = res[7];

    // Push the current path onto the tokens.
    if (path) {
      tokens.push(path);
      path = '';
    }

    var partial = prefix != null && next != null && next !== prefix;
    var repeat = modifier === '+' || modifier === '*';
    var optional = modifier === '?' || modifier === '*';
    var delimiter = res[2] || defaultDelimiter;
    var pattern = capture || group;

    tokens.push({
      name: name || key++,
      prefix: prefix || '',
      delimiter: delimiter,
      optional: optional,
      repeat: repeat,
      partial: partial,
      asterisk: !!asterisk,
      pattern: pattern ? escapeGroup(pattern) : (asterisk ? '.*' : '[^' + escapeString(delimiter) + ']+?')
    });
  }

  // Match any characters still remaining.
  if (index < str.length) {
    path += str.substr(index);
  }

  // If the path exists, push it onto the end.
  if (path) {
    tokens.push(path);
  }

  return tokens
}

/**
 * Compile a string to a template function for the path.
 *
 * @param  {string}             str
 * @param  {Object=}            options
 * @return {!function(Object=, Object=)}
 */
function compile (str, options) {
  return tokensToFunction(parse(str, options))
}

/**
 * Prettier encoding of URI path segments.
 *
 * @param  {string}
 * @return {string}
 */
function encodeURIComponentPretty (str) {
  return encodeURI(str).replace(/[\/?#]/g, function (c) {
    return '%' + c.charCodeAt(0).toString(16).toUpperCase()
  })
}

/**
 * Encode the asterisk parameter. Similar to `pretty`, but allows slashes.
 *
 * @param  {string}
 * @return {string}
 */
function encodeAsterisk (str) {
  return encodeURI(str).replace(/[?#]/g, function (c) {
    return '%' + c.charCodeAt(0).toString(16).toUpperCase()
  })
}

/**
 * Expose a method for transforming tokens into the path function.
 */
function tokensToFunction (tokens) {
  // Compile all the tokens into regexps.
  var matches = new Array(tokens.length);

  // Compile all the patterns before compilation.
  for (var i = 0; i < tokens.length; i++) {
    if (typeof tokens[i] === 'object') {
      matches[i] = new RegExp('^(?:' + tokens[i].pattern + ')$');
    }
  }

  return function (obj, opts) {
    var path = '';
    var data = obj || {};
    var options = opts || {};
    var encode = options.pretty ? encodeURIComponentPretty : encodeURIComponent;

    for (var i = 0; i < tokens.length; i++) {
      var token = tokens[i];

      if (typeof token === 'string') {
        path += token;

        continue
      }

      var value = data[token.name];
      var segment;

      if (value == null) {
        if (token.optional) {
          // Prepend partial segment prefixes.
          if (token.partial) {
            path += token.prefix;
          }

          continue
        } else {
          throw new TypeError('Expected "' + token.name + '" to be defined')
        }
      }

      if (isarray(value)) {
        if (!token.repeat) {
          throw new TypeError('Expected "' + token.name + '" to not repeat, but received `' + JSON.stringify(value) + '`')
        }

        if (value.length === 0) {
          if (token.optional) {
            continue
          } else {
            throw new TypeError('Expected "' + token.name + '" to not be empty')
          }
        }

        for (var j = 0; j < value.length; j++) {
          segment = encode(value[j]);

          if (!matches[i].test(segment)) {
            throw new TypeError('Expected all "' + token.name + '" to match "' + token.pattern + '", but received `' + JSON.stringify(segment) + '`')
          }

          path += (j === 0 ? token.prefix : token.delimiter) + segment;
        }

        continue
      }

      segment = token.asterisk ? encodeAsterisk(value) : encode(value);

      if (!matches[i].test(segment)) {
        throw new TypeError('Expected "' + token.name + '" to match "' + token.pattern + '", but received "' + segment + '"')
      }

      path += token.prefix + segment;
    }

    return path
  }
}

/**
 * Escape a regular expression string.
 *
 * @param  {string} str
 * @return {string}
 */
function escapeString (str) {
  return str.replace(/([.+*?=^!:${}()[\]|\/\\])/g, '\\$1')
}

/**
 * Escape the capturing group by escaping special characters and meaning.
 *
 * @param  {string} group
 * @return {string}
 */
function escapeGroup (group) {
  return group.replace(/([=!:$\/()])/g, '\\$1')
}

/**
 * Attach the keys as a property of the regexp.
 *
 * @param  {!RegExp} re
 * @param  {Array}   keys
 * @return {!RegExp}
 */
function attachKeys (re, keys) {
  re.keys = keys;
  return re
}

/**
 * Get the flags for a regexp from the options.
 *
 * @param  {Object} options
 * @return {string}
 */
function flags (options) {
  return options.sensitive ? '' : 'i'
}

/**
 * Pull out keys from a regexp.
 *
 * @param  {!RegExp} path
 * @param  {!Array}  keys
 * @return {!RegExp}
 */
function regexpToRegexp (path, keys) {
  // Use a negative lookahead to match only capturing groups.
  var groups = path.source.match(/\((?!\?)/g);

  if (groups) {
    for (var i = 0; i < groups.length; i++) {
      keys.push({
        name: i,
        prefix: null,
        delimiter: null,
        optional: false,
        repeat: false,
        partial: false,
        asterisk: false,
        pattern: null
      });
    }
  }

  return attachKeys(path, keys)
}

/**
 * Transform an array into a regexp.
 *
 * @param  {!Array}  path
 * @param  {Array}   keys
 * @param  {!Object} options
 * @return {!RegExp}
 */
function arrayToRegexp (path, keys, options) {
  var parts = [];

  for (var i = 0; i < path.length; i++) {
    parts.push(pathToRegexp(path[i], keys, options).source);
  }

  var regexp = new RegExp('(?:' + parts.join('|') + ')', flags(options));

  return attachKeys(regexp, keys)
}

/**
 * Create a path regexp from string input.
 *
 * @param  {string}  path
 * @param  {!Array}  keys
 * @param  {!Object} options
 * @return {!RegExp}
 */
function stringToRegexp (path, keys, options) {
  return tokensToRegExp(parse(path, options), keys, options)
}

/**
 * Expose a function for taking tokens and returning a RegExp.
 *
 * @param  {!Array}          tokens
 * @param  {(Array|Object)=} keys
 * @param  {Object=}         options
 * @return {!RegExp}
 */
function tokensToRegExp (tokens, keys, options) {
  if (!isarray(keys)) {
    options = /** @type {!Object} */ (keys || options);
    keys = [];
  }

  options = options || {};

  var strict = options.strict;
  var end = options.end !== false;
  var route = '';

  // Iterate over the tokens and create our regexp string.
  for (var i = 0; i < tokens.length; i++) {
    var token = tokens[i];

    if (typeof token === 'string') {
      route += escapeString(token);
    } else {
      var prefix = escapeString(token.prefix);
      var capture = '(?:' + token.pattern + ')';

      keys.push(token);

      if (token.repeat) {
        capture += '(?:' + prefix + capture + ')*';
      }

      if (token.optional) {
        if (!token.partial) {
          capture = '(?:' + prefix + '(' + capture + '))?';
        } else {
          capture = prefix + '(' + capture + ')?';
        }
      } else {
        capture = prefix + '(' + capture + ')';
      }

      route += capture;
    }
  }

  var delimiter = escapeString(options.delimiter || '/');
  var endsWithDelimiter = route.slice(-delimiter.length) === delimiter;

  // In non-strict mode we allow a slash at the end of match. If the path to
  // match already ends with a slash, we remove it for consistency. The slash
  // is valid at the end of a path match, not in the middle. This is important
  // in non-ending mode, where "/test/" shouldn't match "/test//route".
  if (!strict) {
    route = (endsWithDelimiter ? route.slice(0, -delimiter.length) : route) + '(?:' + delimiter + '(?=$))?';
  }

  if (end) {
    route += '$';
  } else {
    // In non-ending mode, we need the capturing groups to match as much as
    // possible by using a positive lookahead to the end or next path segment.
    route += strict && endsWithDelimiter ? '' : '(?=' + delimiter + '|$)';
  }

  return attachKeys(new RegExp('^' + route, flags(options)), keys)
}

/**
 * Normalize the given path string, returning a regular expression.
 *
 * An empty array can be passed in for the keys, which will hold the
 * placeholder key descriptions. For example, using `/user/:id`, `keys` will
 * contain `[{ name: 'id', delimiter: '/', optional: false, repeat: false }]`.
 *
 * @param  {(string|RegExp|Array)} path
 * @param  {(Array|Object)=}       keys
 * @param  {Object=}               options
 * @return {!RegExp}
 */
function pathToRegexp (path, keys, options) {
  if (!isarray(keys)) {
    options = /** @type {!Object} */ (keys || options);
    keys = [];
  }

  options = options || {};

  if (path instanceof RegExp) {
    return regexpToRegexp(path, /** @type {!Array} */ (keys))
  }

  if (isarray(path)) {
    return arrayToRegexp(/** @type {!Array} */ (path), /** @type {!Array} */ (keys), options)
  }

  return stringToRegexp(/** @type {string} */ (path), /** @type {!Array} */ (keys), options)
}
pathToRegexp_1.parse = parse_1;
pathToRegexp_1.compile = compile_1;
pathToRegexp_1.tokensToFunction = tokensToFunction_1;
pathToRegexp_1.tokensToRegExp = tokensToRegExp_1;

/*  */

// $flow-disable-line
var regexpCompileCache = Object.create(null);

function fillParams (
  path,
  params,
  routeMsg
) {
  params = params || {};
  try {
    var filler =
      regexpCompileCache[path] ||
      (regexpCompileCache[path] = pathToRegexp_1.compile(path));

    // Fix #2505 resolving asterisk routes { name: 'not-found', params: { pathMatch: '/not-found' }}
    if (params.pathMatch) { params[0] = params.pathMatch; }

    return filler(params, { pretty: true })
  } catch (e) {
    if (true) {
      warn(false, ("missing param for " + routeMsg + ": " + (e.message)));
    }
    return ''
  } finally {
    // delete the 0 if it was added
    delete params[0];
  }
}

/*  */

function normalizeLocation (
  raw,
  current,
  append,
  router
) {
  var next = typeof raw === 'string' ? { path: raw } : raw;
  // named target
  if (next._normalized) {
    return next
  } else if (next.name) {
    return extend({}, raw)
  }

  // relative params
  if (!next.path && next.params && current) {
    next = extend({}, next);
    next._normalized = true;
    var params = extend(extend({}, current.params), next.params);
    if (current.name) {
      next.name = current.name;
      next.params = params;
    } else if (current.matched.length) {
      var rawPath = current.matched[current.matched.length - 1].path;
      next.path = fillParams(rawPath, params, ("path " + (current.path)));
    } else if (true) {
      warn(false, "relative params navigation requires a current route.");
    }
    return next
  }

  var parsedPath = parsePath(next.path || '');
  var basePath = (current && current.path) || '/';
  var path = parsedPath.path
    ? resolvePath(parsedPath.path, basePath, append || next.append)
    : basePath;

  var query = resolveQuery(
    parsedPath.query,
    next.query,
    router && router.options.parseQuery
  );

  var hash = next.hash || parsedPath.hash;
  if (hash && hash.charAt(0) !== '#') {
    hash = "#" + hash;
  }

  return {
    _normalized: true,
    path: path,
    query: query,
    hash: hash
  }
}

/*  */

// work around weird flow bug
var toTypes = [String, Object];
var eventTypes = [String, Array];

var noop = function () {};

var Link = {
  name: 'RouterLink',
  props: {
    to: {
      type: toTypes,
      required: true
    },
    tag: {
      type: String,
      default: 'a'
    },
    exact: Boolean,
    append: Boolean,
    replace: Boolean,
    activeClass: String,
    exactActiveClass: String,
    event: {
      type: eventTypes,
      default: 'click'
    }
  },
  render: function render (h) {
    var this$1 = this;

    var router = this.$router;
    var current = this.$route;
    var ref = router.resolve(
      this.to,
      current,
      this.append
    );
    var location = ref.location;
    var route = ref.route;
    var href = ref.href;

    var classes = {};
    var globalActiveClass = router.options.linkActiveClass;
    var globalExactActiveClass = router.options.linkExactActiveClass;
    // Support global empty active class
    var activeClassFallback =
      globalActiveClass == null ? 'router-link-active' : globalActiveClass;
    var exactActiveClassFallback =
      globalExactActiveClass == null
        ? 'router-link-exact-active'
        : globalExactActiveClass;
    var activeClass =
      this.activeClass == null ? activeClassFallback : this.activeClass;
    var exactActiveClass =
      this.exactActiveClass == null
        ? exactActiveClassFallback
        : this.exactActiveClass;

    var compareTarget = route.redirectedFrom
      ? createRoute(null, normalizeLocation(route.redirectedFrom), null, router)
      : route;

    classes[exactActiveClass] = isSameRoute(current, compareTarget);
    classes[activeClass] = this.exact
      ? classes[exactActiveClass]
      : isIncludedRoute(current, compareTarget);

    var handler = function (e) {
      if (guardEvent(e)) {
        if (this$1.replace) {
          router.replace(location, noop);
        } else {
          router.push(location, noop);
        }
      }
    };

    var on = { click: guardEvent };
    if (Array.isArray(this.event)) {
      this.event.forEach(function (e) {
        on[e] = handler;
      });
    } else {
      on[this.event] = handler;
    }

    var data = { class: classes };

    var scopedSlot =
      !this.$scopedSlots.$hasNormal &&
      this.$scopedSlots.default &&
      this.$scopedSlots.default({
        href: href,
        route: route,
        navigate: handler,
        isActive: classes[activeClass],
        isExactActive: classes[exactActiveClass]
      });

    if (scopedSlot) {
      if (scopedSlot.length === 1) {
        return scopedSlot[0]
      } else if (scopedSlot.length > 1 || !scopedSlot.length) {
        if (true) {
          warn(
            false,
            ("RouterLink with to=\"" + (this.props.to) + "\" is trying to use a scoped slot but it didn't provide exactly one child.")
          );
        }
        return scopedSlot.length === 0 ? h() : h('span', {}, scopedSlot)
      }
    }

    if (this.tag === 'a') {
      data.on = on;
      data.attrs = { href: href };
    } else {
      // find the first <a> child and apply listener and href
      var a = findAnchor(this.$slots.default);
      if (a) {
        // in case the <a> is a static node
        a.isStatic = false;
        var aData = (a.data = extend({}, a.data));
        aData.on = aData.on || {};
        // transform existing events in both objects into arrays so we can push later
        for (var event in aData.on) {
          var handler$1 = aData.on[event];
          if (event in on) {
            aData.on[event] = Array.isArray(handler$1) ? handler$1 : [handler$1];
          }
        }
        // append new listeners for router-link
        for (var event$1 in on) {
          if (event$1 in aData.on) {
            // on[event] is always a function
            aData.on[event$1].push(on[event$1]);
          } else {
            aData.on[event$1] = handler;
          }
        }

        var aAttrs = (a.data.attrs = extend({}, a.data.attrs));
        aAttrs.href = href;
      } else {
        // doesn't have <a> child, apply listener to self
        data.on = on;
      }
    }

    return h(this.tag, data, this.$slots.default)
  }
};

function guardEvent (e) {
  // don't redirect with control keys
  if (e.metaKey || e.altKey || e.ctrlKey || e.shiftKey) { return }
  // don't redirect when preventDefault called
  if (e.defaultPrevented) { return }
  // don't redirect on right click
  if (e.button !== undefined && e.button !== 0) { return }
  // don't redirect if `target="_blank"`
  if (e.currentTarget && e.currentTarget.getAttribute) {
    var target = e.currentTarget.getAttribute('target');
    if (/\b_blank\b/i.test(target)) { return }
  }
  // this may be a Weex event which doesn't have this method
  if (e.preventDefault) {
    e.preventDefault();
  }
  return true
}

function findAnchor (children) {
  if (children) {
    var child;
    for (var i = 0; i < children.length; i++) {
      child = children[i];
      if (child.tag === 'a') {
        return child
      }
      if (child.children && (child = findAnchor(child.children))) {
        return child
      }
    }
  }
}

var _Vue;

function install (Vue) {
  if (install.installed && _Vue === Vue) { return }
  install.installed = true;

  _Vue = Vue;

  var isDef = function (v) { return v !== undefined; };

  var registerInstance = function (vm, callVal) {
    var i = vm.$options._parentVnode;
    if (isDef(i) && isDef(i = i.data) && isDef(i = i.registerRouteInstance)) {
      i(vm, callVal);
    }
  };

  Vue.mixin({
    beforeCreate: function beforeCreate () {
      if (isDef(this.$options.router)) {
        this._routerRoot = this;
        this._router = this.$options.router;
        this._router.init(this);
        Vue.util.defineReactive(this, '_route', this._router.history.current);
      } else {
        this._routerRoot = (this.$parent && this.$parent._routerRoot) || this;
      }
      registerInstance(this, this);
    },
    destroyed: function destroyed () {
      registerInstance(this);
    }
  });

  Object.defineProperty(Vue.prototype, '$router', {
    get: function get () { return this._routerRoot._router }
  });

  Object.defineProperty(Vue.prototype, '$route', {
    get: function get () { return this._routerRoot._route }
  });

  Vue.component('RouterView', View);
  Vue.component('RouterLink', Link);

  var strats = Vue.config.optionMergeStrategies;
  // use the same hook merging strategy for route hooks
  strats.beforeRouteEnter = strats.beforeRouteLeave = strats.beforeRouteUpdate = strats.created;
}

/*  */

var inBrowser = typeof window !== 'undefined';

/*  */

function createRouteMap (
  routes,
  oldPathList,
  oldPathMap,
  oldNameMap
) {
  // the path list is used to control path matching priority
  var pathList = oldPathList || [];
  // $flow-disable-line
  var pathMap = oldPathMap || Object.create(null);
  // $flow-disable-line
  var nameMap = oldNameMap || Object.create(null);

  routes.forEach(function (route) {
    addRouteRecord(pathList, pathMap, nameMap, route);
  });

  // ensure wildcard routes are always at the end
  for (var i = 0, l = pathList.length; i < l; i++) {
    if (pathList[i] === '*') {
      pathList.push(pathList.splice(i, 1)[0]);
      l--;
      i--;
    }
  }

  if (true) {
    // warn if routes do not include leading slashes
    var found = pathList
    // check for missing leading slash
      .filter(function (path) { return path && path.charAt(0) !== '*' && path.charAt(0) !== '/'; });

    if (found.length > 0) {
      var pathNames = found.map(function (path) { return ("- " + path); }).join('\n');
      warn(false, ("Non-nested routes must include a leading slash character. Fix the following routes: \n" + pathNames));
    }
  }

  return {
    pathList: pathList,
    pathMap: pathMap,
    nameMap: nameMap
  }
}

function addRouteRecord (
  pathList,
  pathMap,
  nameMap,
  route,
  parent,
  matchAs
) {
  var path = route.path;
  var name = route.name;
  if (true) {
    assert(path != null, "\"path\" is required in a route configuration.");
    assert(
      typeof route.component !== 'string',
      "route config \"component\" for path: " + (String(
        path || name
      )) + " cannot be a " + "string id. Use an actual component instead."
    );
  }

  var pathToRegexpOptions =
    route.pathToRegexpOptions || {};
  var normalizedPath = normalizePath(path, parent, pathToRegexpOptions.strict);

  if (typeof route.caseSensitive === 'boolean') {
    pathToRegexpOptions.sensitive = route.caseSensitive;
  }

  var record = {
    path: normalizedPath,
    regex: compileRouteRegex(normalizedPath, pathToRegexpOptions),
    components: route.components || { default: route.component },
    instances: {},
    name: name,
    parent: parent,
    matchAs: matchAs,
    redirect: route.redirect,
    beforeEnter: route.beforeEnter,
    meta: route.meta || {},
    props:
      route.props == null
        ? {}
        : route.components
          ? route.props
          : { default: route.props }
  };

  if (route.children) {
    // Warn if route is named, does not redirect and has a default child route.
    // If users navigate to this route by name, the default child will
    // not be rendered (GH Issue #629)
    if (true) {
      if (
        route.name &&
        !route.redirect &&
        route.children.some(function (child) { return /^\/?$/.test(child.path); })
      ) {
        warn(
          false,
          "Named Route '" + (route.name) + "' has a default child route. " +
            "When navigating to this named route (:to=\"{name: '" + (route.name) + "'\"), " +
            "the default child route will not be rendered. Remove the name from " +
            "this route and use the name of the default child route for named " +
            "links instead."
        );
      }
    }
    route.children.forEach(function (child) {
      var childMatchAs = matchAs
        ? cleanPath((matchAs + "/" + (child.path)))
        : undefined;
      addRouteRecord(pathList, pathMap, nameMap, child, record, childMatchAs);
    });
  }

  if (!pathMap[record.path]) {
    pathList.push(record.path);
    pathMap[record.path] = record;
  }

  if (route.alias !== undefined) {
    var aliases = Array.isArray(route.alias) ? route.alias : [route.alias];
    for (var i = 0; i < aliases.length; ++i) {
      var alias = aliases[i];
      if ( true && alias === path) {
        warn(
          false,
          ("Found an alias with the same value as the path: \"" + path + "\". You have to remove that alias. It will be ignored in development.")
        );
        // skip in dev to make it work
        continue
      }

      var aliasRoute = {
        path: alias,
        children: route.children
      };
      addRouteRecord(
        pathList,
        pathMap,
        nameMap,
        aliasRoute,
        parent,
        record.path || '/' // matchAs
      );
    }
  }

  if (name) {
    if (!nameMap[name]) {
      nameMap[name] = record;
    } else if ( true && !matchAs) {
      warn(
        false,
        "Duplicate named routes definition: " +
          "{ name: \"" + name + "\", path: \"" + (record.path) + "\" }"
      );
    }
  }
}

function compileRouteRegex (
  path,
  pathToRegexpOptions
) {
  var regex = pathToRegexp_1(path, [], pathToRegexpOptions);
  if (true) {
    var keys = Object.create(null);
    regex.keys.forEach(function (key) {
      warn(
        !keys[key.name],
        ("Duplicate param keys in route with path: \"" + path + "\"")
      );
      keys[key.name] = true;
    });
  }
  return regex
}

function normalizePath (
  path,
  parent,
  strict
) {
  if (!strict) { path = path.replace(/\/$/, ''); }
  if (path[0] === '/') { return path }
  if (parent == null) { return path }
  return cleanPath(((parent.path) + "/" + path))
}

/*  */



function createMatcher (
  routes,
  router
) {
  var ref = createRouteMap(routes);
  var pathList = ref.pathList;
  var pathMap = ref.pathMap;
  var nameMap = ref.nameMap;

  function addRoutes (routes) {
    createRouteMap(routes, pathList, pathMap, nameMap);
  }

  function match (
    raw,
    currentRoute,
    redirectedFrom
  ) {
    var location = normalizeLocation(raw, currentRoute, false, router);
    var name = location.name;

    if (name) {
      var record = nameMap[name];
      if (true) {
        warn(record, ("Route with name '" + name + "' does not exist"));
      }
      if (!record) { return _createRoute(null, location) }
      var paramNames = record.regex.keys
        .filter(function (key) { return !key.optional; })
        .map(function (key) { return key.name; });

      if (typeof location.params !== 'object') {
        location.params = {};
      }

      if (currentRoute && typeof currentRoute.params === 'object') {
        for (var key in currentRoute.params) {
          if (!(key in location.params) && paramNames.indexOf(key) > -1) {
            location.params[key] = currentRoute.params[key];
          }
        }
      }

      location.path = fillParams(record.path, location.params, ("named route \"" + name + "\""));
      return _createRoute(record, location, redirectedFrom)
    } else if (location.path) {
      location.params = {};
      for (var i = 0; i < pathList.length; i++) {
        var path = pathList[i];
        var record$1 = pathMap[path];
        if (matchRoute(record$1.regex, location.path, location.params)) {
          return _createRoute(record$1, location, redirectedFrom)
        }
      }
    }
    // no match
    return _createRoute(null, location)
  }

  function redirect (
    record,
    location
  ) {
    var originalRedirect = record.redirect;
    var redirect = typeof originalRedirect === 'function'
      ? originalRedirect(createRoute(record, location, null, router))
      : originalRedirect;

    if (typeof redirect === 'string') {
      redirect = { path: redirect };
    }

    if (!redirect || typeof redirect !== 'object') {
      if (true) {
        warn(
          false, ("invalid redirect option: " + (JSON.stringify(redirect)))
        );
      }
      return _createRoute(null, location)
    }

    var re = redirect;
    var name = re.name;
    var path = re.path;
    var query = location.query;
    var hash = location.hash;
    var params = location.params;
    query = re.hasOwnProperty('query') ? re.query : query;
    hash = re.hasOwnProperty('hash') ? re.hash : hash;
    params = re.hasOwnProperty('params') ? re.params : params;

    if (name) {
      // resolved named direct
      var targetRecord = nameMap[name];
      if (true) {
        assert(targetRecord, ("redirect failed: named route \"" + name + "\" not found."));
      }
      return match({
        _normalized: true,
        name: name,
        query: query,
        hash: hash,
        params: params
      }, undefined, location)
    } else if (path) {
      // 1. resolve relative redirect
      var rawPath = resolveRecordPath(path, record);
      // 2. resolve params
      var resolvedPath = fillParams(rawPath, params, ("redirect route with path \"" + rawPath + "\""));
      // 3. rematch with existing query and hash
      return match({
        _normalized: true,
        path: resolvedPath,
        query: query,
        hash: hash
      }, undefined, location)
    } else {
      if (true) {
        warn(false, ("invalid redirect option: " + (JSON.stringify(redirect))));
      }
      return _createRoute(null, location)
    }
  }

  function alias (
    record,
    location,
    matchAs
  ) {
    var aliasedPath = fillParams(matchAs, location.params, ("aliased route with path \"" + matchAs + "\""));
    var aliasedMatch = match({
      _normalized: true,
      path: aliasedPath
    });
    if (aliasedMatch) {
      var matched = aliasedMatch.matched;
      var aliasedRecord = matched[matched.length - 1];
      location.params = aliasedMatch.params;
      return _createRoute(aliasedRecord, location)
    }
    return _createRoute(null, location)
  }

  function _createRoute (
    record,
    location,
    redirectedFrom
  ) {
    if (record && record.redirect) {
      return redirect(record, redirectedFrom || location)
    }
    if (record && record.matchAs) {
      return alias(record, location, record.matchAs)
    }
    return createRoute(record, location, redirectedFrom, router)
  }

  return {
    match: match,
    addRoutes: addRoutes
  }
}

function matchRoute (
  regex,
  path,
  params
) {
  var m = path.match(regex);

  if (!m) {
    return false
  } else if (!params) {
    return true
  }

  for (var i = 1, len = m.length; i < len; ++i) {
    var key = regex.keys[i - 1];
    var val = typeof m[i] === 'string' ? decodeURIComponent(m[i]) : m[i];
    if (key) {
      // Fix #1994: using * with props: true generates a param named 0
      params[key.name || 'pathMatch'] = val;
    }
  }

  return true
}

function resolveRecordPath (path, record) {
  return resolvePath(path, record.parent ? record.parent.path : '/', true)
}

/*  */

// use User Timing api (if present) for more accurate key precision
var Time =
  inBrowser && window.performance && window.performance.now
    ? window.performance
    : Date;

function genStateKey () {
  return Time.now().toFixed(3)
}

var _key = genStateKey();

function getStateKey () {
  return _key
}

function setStateKey (key) {
  return (_key = key)
}

/*  */

var positionStore = Object.create(null);

function setupScroll () {
  // Fix for #1585 for Firefox
  // Fix for #2195 Add optional third attribute to workaround a bug in safari https://bugs.webkit.org/show_bug.cgi?id=182678
  // Fix for #2774 Support for apps loaded from Windows file shares not mapped to network drives: replaced location.origin with
  // window.location.protocol + '//' + window.location.host
  // location.host contains the port and location.hostname doesn't
  var protocolAndPath = window.location.protocol + '//' + window.location.host;
  var absolutePath = window.location.href.replace(protocolAndPath, '');
  window.history.replaceState({ key: getStateKey() }, '', absolutePath);
  window.addEventListener('popstate', function (e) {
    saveScrollPosition();
    if (e.state && e.state.key) {
      setStateKey(e.state.key);
    }
  });
}

function handleScroll (
  router,
  to,
  from,
  isPop
) {
  if (!router.app) {
    return
  }

  var behavior = router.options.scrollBehavior;
  if (!behavior) {
    return
  }

  if (true) {
    assert(typeof behavior === 'function', "scrollBehavior must be a function");
  }

  // wait until re-render finishes before scrolling
  router.app.$nextTick(function () {
    var position = getScrollPosition();
    var shouldScroll = behavior.call(
      router,
      to,
      from,
      isPop ? position : null
    );

    if (!shouldScroll) {
      return
    }

    if (typeof shouldScroll.then === 'function') {
      shouldScroll
        .then(function (shouldScroll) {
          scrollToPosition((shouldScroll), position);
        })
        .catch(function (err) {
          if (true) {
            assert(false, err.toString());
          }
        });
    } else {
      scrollToPosition(shouldScroll, position);
    }
  });
}

function saveScrollPosition () {
  var key = getStateKey();
  if (key) {
    positionStore[key] = {
      x: window.pageXOffset,
      y: window.pageYOffset
    };
  }
}

function getScrollPosition () {
  var key = getStateKey();
  if (key) {
    return positionStore[key]
  }
}

function getElementPosition (el, offset) {
  var docEl = document.documentElement;
  var docRect = docEl.getBoundingClientRect();
  var elRect = el.getBoundingClientRect();
  return {
    x: elRect.left - docRect.left - offset.x,
    y: elRect.top - docRect.top - offset.y
  }
}

function isValidPosition (obj) {
  return isNumber(obj.x) || isNumber(obj.y)
}

function normalizePosition (obj) {
  return {
    x: isNumber(obj.x) ? obj.x : window.pageXOffset,
    y: isNumber(obj.y) ? obj.y : window.pageYOffset
  }
}

function normalizeOffset (obj) {
  return {
    x: isNumber(obj.x) ? obj.x : 0,
    y: isNumber(obj.y) ? obj.y : 0
  }
}

function isNumber (v) {
  return typeof v === 'number'
}

var hashStartsWithNumberRE = /^#\d/;

function scrollToPosition (shouldScroll, position) {
  var isObject = typeof shouldScroll === 'object';
  if (isObject && typeof shouldScroll.selector === 'string') {
    // getElementById would still fail if the selector contains a more complicated query like #main[data-attr]
    // but at the same time, it doesn't make much sense to select an element with an id and an extra selector
    var el = hashStartsWithNumberRE.test(shouldScroll.selector) // $flow-disable-line
      ? document.getElementById(shouldScroll.selector.slice(1)) // $flow-disable-line
      : document.querySelector(shouldScroll.selector);

    if (el) {
      var offset =
        shouldScroll.offset && typeof shouldScroll.offset === 'object'
          ? shouldScroll.offset
          : {};
      offset = normalizeOffset(offset);
      position = getElementPosition(el, offset);
    } else if (isValidPosition(shouldScroll)) {
      position = normalizePosition(shouldScroll);
    }
  } else if (isObject && isValidPosition(shouldScroll)) {
    position = normalizePosition(shouldScroll);
  }

  if (position) {
    window.scrollTo(position.x, position.y);
  }
}

/*  */

var supportsPushState =
  inBrowser &&
  (function () {
    var ua = window.navigator.userAgent;

    if (
      (ua.indexOf('Android 2.') !== -1 || ua.indexOf('Android 4.0') !== -1) &&
      ua.indexOf('Mobile Safari') !== -1 &&
      ua.indexOf('Chrome') === -1 &&
      ua.indexOf('Windows Phone') === -1
    ) {
      return false
    }

    return window.history && 'pushState' in window.history
  })();

function pushState (url, replace) {
  saveScrollPosition();
  // try...catch the pushState call to get around Safari
  // DOM Exception 18 where it limits to 100 pushState calls
  var history = window.history;
  try {
    if (replace) {
      history.replaceState({ key: getStateKey() }, '', url);
    } else {
      history.pushState({ key: setStateKey(genStateKey()) }, '', url);
    }
  } catch (e) {
    window.location[replace ? 'replace' : 'assign'](url);
  }
}

function replaceState (url) {
  pushState(url, true);
}

/*  */

function runQueue (queue, fn, cb) {
  var step = function (index) {
    if (index >= queue.length) {
      cb();
    } else {
      if (queue[index]) {
        fn(queue[index], function () {
          step(index + 1);
        });
      } else {
        step(index + 1);
      }
    }
  };
  step(0);
}

/*  */

function resolveAsyncComponents (matched) {
  return function (to, from, next) {
    var hasAsync = false;
    var pending = 0;
    var error = null;

    flatMapComponents(matched, function (def, _, match, key) {
      // if it's a function and doesn't have cid attached,
      // assume it's an async component resolve function.
      // we are not using Vue's default async resolving mechanism because
      // we want to halt the navigation until the incoming component has been
      // resolved.
      if (typeof def === 'function' && def.cid === undefined) {
        hasAsync = true;
        pending++;

        var resolve = once(function (resolvedDef) {
          if (isESModule(resolvedDef)) {
            resolvedDef = resolvedDef.default;
          }
          // save resolved on async factory in case it's used elsewhere
          def.resolved = typeof resolvedDef === 'function'
            ? resolvedDef
            : _Vue.extend(resolvedDef);
          match.components[key] = resolvedDef;
          pending--;
          if (pending <= 0) {
            next();
          }
        });

        var reject = once(function (reason) {
          var msg = "Failed to resolve async component " + key + ": " + reason;
           true && warn(false, msg);
          if (!error) {
            error = isError(reason)
              ? reason
              : new Error(msg);
            next(error);
          }
        });

        var res;
        try {
          res = def(resolve, reject);
        } catch (e) {
          reject(e);
        }
        if (res) {
          if (typeof res.then === 'function') {
            res.then(resolve, reject);
          } else {
            // new syntax in Vue 2.3
            var comp = res.component;
            if (comp && typeof comp.then === 'function') {
              comp.then(resolve, reject);
            }
          }
        }
      }
    });

    if (!hasAsync) { next(); }
  }
}

function flatMapComponents (
  matched,
  fn
) {
  return flatten(matched.map(function (m) {
    return Object.keys(m.components).map(function (key) { return fn(
      m.components[key],
      m.instances[key],
      m, key
    ); })
  }))
}

function flatten (arr) {
  return Array.prototype.concat.apply([], arr)
}

var hasSymbol =
  typeof Symbol === 'function' &&
  typeof Symbol.toStringTag === 'symbol';

function isESModule (obj) {
  return obj.__esModule || (hasSymbol && obj[Symbol.toStringTag] === 'Module')
}

// in Webpack 2, require.ensure now also returns a Promise
// so the resolve/reject functions may get called an extra time
// if the user uses an arrow function shorthand that happens to
// return that Promise.
function once (fn) {
  var called = false;
  return function () {
    var args = [], len = arguments.length;
    while ( len-- ) args[ len ] = arguments[ len ];

    if (called) { return }
    called = true;
    return fn.apply(this, args)
  }
}

var NavigationDuplicated = /*@__PURE__*/(function (Error) {
  function NavigationDuplicated (normalizedLocation) {
    Error.call(this);
    this.name = this._name = 'NavigationDuplicated';
    // passing the message to super() doesn't seem to work in the transpiled version
    this.message = "Navigating to current location (\"" + (normalizedLocation.fullPath) + "\") is not allowed";
    // add a stack property so services like Sentry can correctly display it
    Object.defineProperty(this, 'stack', {
      value: new Error().stack,
      writable: true,
      configurable: true
    });
    // we could also have used
    // Error.captureStackTrace(this, this.constructor)
    // but it only exists on node and chrome
  }

  if ( Error ) NavigationDuplicated.__proto__ = Error;
  NavigationDuplicated.prototype = Object.create( Error && Error.prototype );
  NavigationDuplicated.prototype.constructor = NavigationDuplicated;

  return NavigationDuplicated;
}(Error));

// support IE9
NavigationDuplicated._name = 'NavigationDuplicated';

/*  */

var History = function History (router, base) {
  this.router = router;
  this.base = normalizeBase(base);
  // start with a route object that stands for "nowhere"
  this.current = START;
  this.pending = null;
  this.ready = false;
  this.readyCbs = [];
  this.readyErrorCbs = [];
  this.errorCbs = [];
};

History.prototype.listen = function listen (cb) {
  this.cb = cb;
};

History.prototype.onReady = function onReady (cb, errorCb) {
  if (this.ready) {
    cb();
  } else {
    this.readyCbs.push(cb);
    if (errorCb) {
      this.readyErrorCbs.push(errorCb);
    }
  }
};

History.prototype.onError = function onError (errorCb) {
  this.errorCbs.push(errorCb);
};

History.prototype.transitionTo = function transitionTo (
  location,
  onComplete,
  onAbort
) {
    var this$1 = this;

  var route = this.router.match(location, this.current);
  this.confirmTransition(
    route,
    function () {
      this$1.updateRoute(route);
      onComplete && onComplete(route);
      this$1.ensureURL();

      // fire ready cbs once
      if (!this$1.ready) {
        this$1.ready = true;
        this$1.readyCbs.forEach(function (cb) {
          cb(route);
        });
      }
    },
    function (err) {
      if (onAbort) {
        onAbort(err);
      }
      if (err && !this$1.ready) {
        this$1.ready = true;
        this$1.readyErrorCbs.forEach(function (cb) {
          cb(err);
        });
      }
    }
  );
};

History.prototype.confirmTransition = function confirmTransition (route, onComplete, onAbort) {
    var this$1 = this;

  var current = this.current;
  var abort = function (err) {
    // after merging https://github.com/vuejs/vue-router/pull/2771 we
    // When the user navigates through history through back/forward buttons
    // we do not want to throw the error. We only throw it if directly calling
    // push/replace. That's why it's not included in isError
    if (!isExtendedError(NavigationDuplicated, err) && isError(err)) {
      if (this$1.errorCbs.length) {
        this$1.errorCbs.forEach(function (cb) {
          cb(err);
        });
      } else {
        warn(false, 'uncaught error during route navigation:');
        console.error(err);
      }
    }
    onAbort && onAbort(err);
  };
  if (
    isSameRoute(route, current) &&
    // in the case the route map has been dynamically appended to
    route.matched.length === current.matched.length
  ) {
    this.ensureURL();
    return abort(new NavigationDuplicated(route))
  }

  var ref = resolveQueue(
    this.current.matched,
    route.matched
  );
    var updated = ref.updated;
    var deactivated = ref.deactivated;
    var activated = ref.activated;

  var queue = [].concat(
    // in-component leave guards
    extractLeaveGuards(deactivated),
    // global before hooks
    this.router.beforeHooks,
    // in-component update hooks
    extractUpdateHooks(updated),
    // in-config enter guards
    activated.map(function (m) { return m.beforeEnter; }),
    // async components
    resolveAsyncComponents(activated)
  );

  this.pending = route;
  var iterator = function (hook, next) {
    if (this$1.pending !== route) {
      return abort()
    }
    try {
      hook(route, current, function (to) {
        if (to === false || isError(to)) {
          // next(false) -> abort navigation, ensure current URL
          this$1.ensureURL(true);
          abort(to);
        } else if (
          typeof to === 'string' ||
          (typeof to === 'object' &&
            (typeof to.path === 'string' || typeof to.name === 'string'))
        ) {
          // next('/') or next({ path: '/' }) -> redirect
          abort();
          if (typeof to === 'object' && to.replace) {
            this$1.replace(to);
          } else {
            this$1.push(to);
          }
        } else {
          // confirm transition and pass on the value
          next(to);
        }
      });
    } catch (e) {
      abort(e);
    }
  };

  runQueue(queue, iterator, function () {
    var postEnterCbs = [];
    var isValid = function () { return this$1.current === route; };
    // wait until async components are resolved before
    // extracting in-component enter guards
    var enterGuards = extractEnterGuards(activated, postEnterCbs, isValid);
    var queue = enterGuards.concat(this$1.router.resolveHooks);
    runQueue(queue, iterator, function () {
      if (this$1.pending !== route) {
        return abort()
      }
      this$1.pending = null;
      onComplete(route);
      if (this$1.router.app) {
        this$1.router.app.$nextTick(function () {
          postEnterCbs.forEach(function (cb) {
            cb();
          });
        });
      }
    });
  });
};

History.prototype.updateRoute = function updateRoute (route) {
  var prev = this.current;
  this.current = route;
  this.cb && this.cb(route);
  this.router.afterHooks.forEach(function (hook) {
    hook && hook(route, prev);
  });
};

function normalizeBase (base) {
  if (!base) {
    if (inBrowser) {
      // respect <base> tag
      var baseEl = document.querySelector('base');
      base = (baseEl && baseEl.getAttribute('href')) || '/';
      // strip full URL origin
      base = base.replace(/^https?:\/\/[^\/]+/, '');
    } else {
      base = '/';
    }
  }
  // make sure there's the starting slash
  if (base.charAt(0) !== '/') {
    base = '/' + base;
  }
  // remove trailing slash
  return base.replace(/\/$/, '')
}

function resolveQueue (
  current,
  next
) {
  var i;
  var max = Math.max(current.length, next.length);
  for (i = 0; i < max; i++) {
    if (current[i] !== next[i]) {
      break
    }
  }
  return {
    updated: next.slice(0, i),
    activated: next.slice(i),
    deactivated: current.slice(i)
  }
}

function extractGuards (
  records,
  name,
  bind,
  reverse
) {
  var guards = flatMapComponents(records, function (def, instance, match, key) {
    var guard = extractGuard(def, name);
    if (guard) {
      return Array.isArray(guard)
        ? guard.map(function (guard) { return bind(guard, instance, match, key); })
        : bind(guard, instance, match, key)
    }
  });
  return flatten(reverse ? guards.reverse() : guards)
}

function extractGuard (
  def,
  key
) {
  if (typeof def !== 'function') {
    // extend now so that global mixins are applied.
    def = _Vue.extend(def);
  }
  return def.options[key]
}

function extractLeaveGuards (deactivated) {
  return extractGuards(deactivated, 'beforeRouteLeave', bindGuard, true)
}

function extractUpdateHooks (updated) {
  return extractGuards(updated, 'beforeRouteUpdate', bindGuard)
}

function bindGuard (guard, instance) {
  if (instance) {
    return function boundRouteGuard () {
      return guard.apply(instance, arguments)
    }
  }
}

function extractEnterGuards (
  activated,
  cbs,
  isValid
) {
  return extractGuards(
    activated,
    'beforeRouteEnter',
    function (guard, _, match, key) {
      return bindEnterGuard(guard, match, key, cbs, isValid)
    }
  )
}

function bindEnterGuard (
  guard,
  match,
  key,
  cbs,
  isValid
) {
  return function routeEnterGuard (to, from, next) {
    return guard(to, from, function (cb) {
      if (typeof cb === 'function') {
        cbs.push(function () {
          // #750
          // if a router-view is wrapped with an out-in transition,
          // the instance may not have been registered at this time.
          // we will need to poll for registration until current route
          // is no longer valid.
          poll(cb, match.instances, key, isValid);
        });
      }
      next(cb);
    })
  }
}

function poll (
  cb, // somehow flow cannot infer this is a function
  instances,
  key,
  isValid
) {
  if (
    instances[key] &&
    !instances[key]._isBeingDestroyed // do not reuse being destroyed instance
  ) {
    cb(instances[key]);
  } else if (isValid()) {
    setTimeout(function () {
      poll(cb, instances, key, isValid);
    }, 16);
  }
}

/*  */

var HTML5History = /*@__PURE__*/(function (History) {
  function HTML5History (router, base) {
    var this$1 = this;

    History.call(this, router, base);

    var expectScroll = router.options.scrollBehavior;
    var supportsScroll = supportsPushState && expectScroll;

    if (supportsScroll) {
      setupScroll();
    }

    var initLocation = getLocation(this.base);
    window.addEventListener('popstate', function (e) {
      var current = this$1.current;

      // Avoiding first `popstate` event dispatched in some browsers but first
      // history route not updated since async guard at the same time.
      var location = getLocation(this$1.base);
      if (this$1.current === START && location === initLocation) {
        return
      }

      this$1.transitionTo(location, function (route) {
        if (supportsScroll) {
          handleScroll(router, route, current, true);
        }
      });
    });
  }

  if ( History ) HTML5History.__proto__ = History;
  HTML5History.prototype = Object.create( History && History.prototype );
  HTML5History.prototype.constructor = HTML5History;

  HTML5History.prototype.go = function go (n) {
    window.history.go(n);
  };

  HTML5History.prototype.push = function push (location, onComplete, onAbort) {
    var this$1 = this;

    var ref = this;
    var fromRoute = ref.current;
    this.transitionTo(location, function (route) {
      pushState(cleanPath(this$1.base + route.fullPath));
      handleScroll(this$1.router, route, fromRoute, false);
      onComplete && onComplete(route);
    }, onAbort);
  };

  HTML5History.prototype.replace = function replace (location, onComplete, onAbort) {
    var this$1 = this;

    var ref = this;
    var fromRoute = ref.current;
    this.transitionTo(location, function (route) {
      replaceState(cleanPath(this$1.base + route.fullPath));
      handleScroll(this$1.router, route, fromRoute, false);
      onComplete && onComplete(route);
    }, onAbort);
  };

  HTML5History.prototype.ensureURL = function ensureURL (push) {
    if (getLocation(this.base) !== this.current.fullPath) {
      var current = cleanPath(this.base + this.current.fullPath);
      push ? pushState(current) : replaceState(current);
    }
  };

  HTML5History.prototype.getCurrentLocation = function getCurrentLocation () {
    return getLocation(this.base)
  };

  return HTML5History;
}(History));

function getLocation (base) {
  var path = decodeURI(window.location.pathname);
  if (base && path.indexOf(base) === 0) {
    path = path.slice(base.length);
  }
  return (path || '/') + window.location.search + window.location.hash
}

/*  */

var HashHistory = /*@__PURE__*/(function (History) {
  function HashHistory (router, base, fallback) {
    History.call(this, router, base);
    // check history fallback deeplinking
    if (fallback && checkFallback(this.base)) {
      return
    }
    ensureSlash();
  }

  if ( History ) HashHistory.__proto__ = History;
  HashHistory.prototype = Object.create( History && History.prototype );
  HashHistory.prototype.constructor = HashHistory;

  // this is delayed until the app mounts
  // to avoid the hashchange listener being fired too early
  HashHistory.prototype.setupListeners = function setupListeners () {
    var this$1 = this;

    var router = this.router;
    var expectScroll = router.options.scrollBehavior;
    var supportsScroll = supportsPushState && expectScroll;

    if (supportsScroll) {
      setupScroll();
    }

    window.addEventListener(
      supportsPushState ? 'popstate' : 'hashchange',
      function () {
        var current = this$1.current;
        if (!ensureSlash()) {
          return
        }
        this$1.transitionTo(getHash(), function (route) {
          if (supportsScroll) {
            handleScroll(this$1.router, route, current, true);
          }
          if (!supportsPushState) {
            replaceHash(route.fullPath);
          }
        });
      }
    );
  };

  HashHistory.prototype.push = function push (location, onComplete, onAbort) {
    var this$1 = this;

    var ref = this;
    var fromRoute = ref.current;
    this.transitionTo(
      location,
      function (route) {
        pushHash(route.fullPath);
        handleScroll(this$1.router, route, fromRoute, false);
        onComplete && onComplete(route);
      },
      onAbort
    );
  };

  HashHistory.prototype.replace = function replace (location, onComplete, onAbort) {
    var this$1 = this;

    var ref = this;
    var fromRoute = ref.current;
    this.transitionTo(
      location,
      function (route) {
        replaceHash(route.fullPath);
        handleScroll(this$1.router, route, fromRoute, false);
        onComplete && onComplete(route);
      },
      onAbort
    );
  };

  HashHistory.prototype.go = function go (n) {
    window.history.go(n);
  };

  HashHistory.prototype.ensureURL = function ensureURL (push) {
    var current = this.current.fullPath;
    if (getHash() !== current) {
      push ? pushHash(current) : replaceHash(current);
    }
  };

  HashHistory.prototype.getCurrentLocation = function getCurrentLocation () {
    return getHash()
  };

  return HashHistory;
}(History));

function checkFallback (base) {
  var location = getLocation(base);
  if (!/^\/#/.test(location)) {
    window.location.replace(cleanPath(base + '/#' + location));
    return true
  }
}

function ensureSlash () {
  var path = getHash();
  if (path.charAt(0) === '/') {
    return true
  }
  replaceHash('/' + path);
  return false
}

function getHash () {
  // We can't use window.location.hash here because it's not
  // consistent across browsers - Firefox will pre-decode it!
  var href = window.location.href;
  var index = href.indexOf('#');
  // empty path
  if (index < 0) { return '' }

  href = href.slice(index + 1);
  // decode the hash but not the search or hash
  // as search(query) is already decoded
  // https://github.com/vuejs/vue-router/issues/2708
  var searchIndex = href.indexOf('?');
  if (searchIndex < 0) {
    var hashIndex = href.indexOf('#');
    if (hashIndex > -1) {
      href = decodeURI(href.slice(0, hashIndex)) + href.slice(hashIndex);
    } else { href = decodeURI(href); }
  } else {
    if (searchIndex > -1) {
      href = decodeURI(href.slice(0, searchIndex)) + href.slice(searchIndex);
    }
  }

  return href
}

function getUrl (path) {
  var href = window.location.href;
  var i = href.indexOf('#');
  var base = i >= 0 ? href.slice(0, i) : href;
  return (base + "#" + path)
}

function pushHash (path) {
  if (supportsPushState) {
    pushState(getUrl(path));
  } else {
    window.location.hash = path;
  }
}

function replaceHash (path) {
  if (supportsPushState) {
    replaceState(getUrl(path));
  } else {
    window.location.replace(getUrl(path));
  }
}

/*  */

var AbstractHistory = /*@__PURE__*/(function (History) {
  function AbstractHistory (router, base) {
    History.call(this, router, base);
    this.stack = [];
    this.index = -1;
  }

  if ( History ) AbstractHistory.__proto__ = History;
  AbstractHistory.prototype = Object.create( History && History.prototype );
  AbstractHistory.prototype.constructor = AbstractHistory;

  AbstractHistory.prototype.push = function push (location, onComplete, onAbort) {
    var this$1 = this;

    this.transitionTo(
      location,
      function (route) {
        this$1.stack = this$1.stack.slice(0, this$1.index + 1).concat(route);
        this$1.index++;
        onComplete && onComplete(route);
      },
      onAbort
    );
  };

  AbstractHistory.prototype.replace = function replace (location, onComplete, onAbort) {
    var this$1 = this;

    this.transitionTo(
      location,
      function (route) {
        this$1.stack = this$1.stack.slice(0, this$1.index).concat(route);
        onComplete && onComplete(route);
      },
      onAbort
    );
  };

  AbstractHistory.prototype.go = function go (n) {
    var this$1 = this;

    var targetIndex = this.index + n;
    if (targetIndex < 0 || targetIndex >= this.stack.length) {
      return
    }
    var route = this.stack[targetIndex];
    this.confirmTransition(
      route,
      function () {
        this$1.index = targetIndex;
        this$1.updateRoute(route);
      },
      function (err) {
        if (isExtendedError(NavigationDuplicated, err)) {
          this$1.index = targetIndex;
        }
      }
    );
  };

  AbstractHistory.prototype.getCurrentLocation = function getCurrentLocation () {
    var current = this.stack[this.stack.length - 1];
    return current ? current.fullPath : '/'
  };

  AbstractHistory.prototype.ensureURL = function ensureURL () {
    // noop
  };

  return AbstractHistory;
}(History));

/*  */



var VueRouter = function VueRouter (options) {
  if ( options === void 0 ) options = {};

  this.app = null;
  this.apps = [];
  this.options = options;
  this.beforeHooks = [];
  this.resolveHooks = [];
  this.afterHooks = [];
  this.matcher = createMatcher(options.routes || [], this);

  var mode = options.mode || 'hash';
  this.fallback = mode === 'history' && !supportsPushState && options.fallback !== false;
  if (this.fallback) {
    mode = 'hash';
  }
  if (!inBrowser) {
    mode = 'abstract';
  }
  this.mode = mode;

  switch (mode) {
    case 'history':
      this.history = new HTML5History(this, options.base);
      break
    case 'hash':
      this.history = new HashHistory(this, options.base, this.fallback);
      break
    case 'abstract':
      this.history = new AbstractHistory(this, options.base);
      break
    default:
      if (true) {
        assert(false, ("invalid mode: " + mode));
      }
  }
};

var prototypeAccessors = { currentRoute: { configurable: true } };

VueRouter.prototype.match = function match (
  raw,
  current,
  redirectedFrom
) {
  return this.matcher.match(raw, current, redirectedFrom)
};

prototypeAccessors.currentRoute.get = function () {
  return this.history && this.history.current
};

VueRouter.prototype.init = function init (app /* Vue component instance */) {
    var this$1 = this;

   true && assert(
    install.installed,
    "not installed. Make sure to call `Vue.use(VueRouter)` " +
    "before creating root instance."
  );

  this.apps.push(app);

  // set up app destroyed handler
  // https://github.com/vuejs/vue-router/issues/2639
  app.$once('hook:destroyed', function () {
    // clean out app from this.apps array once destroyed
    var index = this$1.apps.indexOf(app);
    if (index > -1) { this$1.apps.splice(index, 1); }
    // ensure we still have a main app or null if no apps
    // we do not release the router so it can be reused
    if (this$1.app === app) { this$1.app = this$1.apps[0] || null; }
  });

  // main app previously initialized
  // return as we don't need to set up new history listener
  if (this.app) {
    return
  }

  this.app = app;

  var history = this.history;

  if (history instanceof HTML5History) {
    history.transitionTo(history.getCurrentLocation());
  } else if (history instanceof HashHistory) {
    var setupHashListener = function () {
      history.setupListeners();
    };
    history.transitionTo(
      history.getCurrentLocation(),
      setupHashListener,
      setupHashListener
    );
  }

  history.listen(function (route) {
    this$1.apps.forEach(function (app) {
      app._route = route;
    });
  });
};

VueRouter.prototype.beforeEach = function beforeEach (fn) {
  return registerHook(this.beforeHooks, fn)
};

VueRouter.prototype.beforeResolve = function beforeResolve (fn) {
  return registerHook(this.resolveHooks, fn)
};

VueRouter.prototype.afterEach = function afterEach (fn) {
  return registerHook(this.afterHooks, fn)
};

VueRouter.prototype.onReady = function onReady (cb, errorCb) {
  this.history.onReady(cb, errorCb);
};

VueRouter.prototype.onError = function onError (errorCb) {
  this.history.onError(errorCb);
};

VueRouter.prototype.push = function push (location, onComplete, onAbort) {
    var this$1 = this;

  // $flow-disable-line
  if (!onComplete && !onAbort && typeof Promise !== 'undefined') {
    return new Promise(function (resolve, reject) {
      this$1.history.push(location, resolve, reject);
    })
  } else {
    this.history.push(location, onComplete, onAbort);
  }
};

VueRouter.prototype.replace = function replace (location, onComplete, onAbort) {
    var this$1 = this;

  // $flow-disable-line
  if (!onComplete && !onAbort && typeof Promise !== 'undefined') {
    return new Promise(function (resolve, reject) {
      this$1.history.replace(location, resolve, reject);
    })
  } else {
    this.history.replace(location, onComplete, onAbort);
  }
};

VueRouter.prototype.go = function go (n) {
  this.history.go(n);
};

VueRouter.prototype.back = function back () {
  this.go(-1);
};

VueRouter.prototype.forward = function forward () {
  this.go(1);
};

VueRouter.prototype.getMatchedComponents = function getMatchedComponents (to) {
  var route = to
    ? to.matched
      ? to
      : this.resolve(to).route
    : this.currentRoute;
  if (!route) {
    return []
  }
  return [].concat.apply([], route.matched.map(function (m) {
    return Object.keys(m.components).map(function (key) {
      return m.components[key]
    })
  }))
};

VueRouter.prototype.resolve = function resolve (
  to,
  current,
  append
) {
  current = current || this.history.current;
  var location = normalizeLocation(
    to,
    current,
    append,
    this
  );
  var route = this.match(location, current);
  var fullPath = route.redirectedFrom || route.fullPath;
  var base = this.history.base;
  var href = createHref(base, fullPath, this.mode);
  return {
    location: location,
    route: route,
    href: href,
    // for backwards compat
    normalizedTo: location,
    resolved: route
  }
};

VueRouter.prototype.addRoutes = function addRoutes (routes) {
  this.matcher.addRoutes(routes);
  if (this.history.current !== START) {
    this.history.transitionTo(this.history.getCurrentLocation());
  }
};

Object.defineProperties( VueRouter.prototype, prototypeAccessors );

function registerHook (list, fn) {
  list.push(fn);
  return function () {
    var i = list.indexOf(fn);
    if (i > -1) { list.splice(i, 1); }
  }
}

function createHref (base, fullPath, mode) {
  var path = mode === 'hash' ? '#' + fullPath : fullPath;
  return base ? cleanPath(base + '/' + path) : path
}

VueRouter.install = install;
VueRouter.version = '3.1.3';

if (inBrowser && window.Vue) {
  window.Vue.use(VueRouter);
}

/* harmony default export */ __webpack_exports__["default"] = (VueRouter);


/***/ }),

/***/ "./node_modules/webpack/hot sync ^\\.\\/log$":
/*!*************************************************!*\
  !*** (webpack)/hot sync nonrecursive ^\.\/log$ ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./log": "./node_modules/webpack/hot/log.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/webpack/hot sync ^\\.\\/log$";

/***/ }),

/***/ "./src/js/common/util/VeeValidateMessage.js":
/*!**************************************************!*\
  !*** ./src/js/common/util/VeeValidateMessage.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'vi',
  messages: {
    _default: function _default(n) {
      return 'Giá trị của ' + n + ' không đúng.';
    },
    after: function after(n, t) {
      return n + ' phải xuất hiện sau ' + t.length + '.';
    },
    alpha_dash: function alpha_dash(n) {
      return n + ' có thể chứa các kí tự chữ (A-Z a-z), số (0-9), gạch ngang (-) và gạch dưới (_).';
    },
    alpha_num: function alpha_num(n) {
      return n + ' chỉ có thể chứa các kí tự chữ và số.';
    },
    alpha_spaces: function alpha_spaces(n) {
      return n + ' chỉ có thế chứa các kí tự và khoảng trắng';
    },
    alpha: function alpha(n) {
      return n + ' chỉ có thể chứa các kí tự chữ.';
    },
    before: function before(n, t) {
      return n + ' phải xuất hiện trước ' + t.length + '.';
    },
    between: function between(n, t) {
      return n + ' phải có giá trị nằm trong khoảng giữa ' + t.length + ' và ' + t[1] + '.';
    },
    confirmed: function confirmed(n, t) {
      return n + ' khác với ' + t.length + '.';
    },
    credit_card: function credit_card(n) {
      return 'Đã điền ' + n + ' không chính xác.';
    },
    date_between: function date_between(n, t) {
      return n + ' phải có giá trị nằm trong khoảng giữa  ' + t.length + ' và ' + t[1] + '.';
    },
    date_format: function date_format(n, t) {
      return n + ' phải có giá trị dưới định dạng ' + t.length + '.';
    },
    decimal: function decimal(n, t) {
      void 0 === t && (t = []);
      var c = t.length;
      return void 0 === c && (c = '*'), n + ' chỉ có thể chứa các kí tự số và dấu thập phân ' + ('*' === c ? '' : c) + '.';
    },
    digits: function digits(n, t) {
      return 'Trường ' + n + ' chỉ có thể chứa các kí tự số và bắt buộc phải có độ dài là ' + t.length + '.';
    },
    dimensions: function dimensions(n, t) {
      return n + ' phải có chiều rộng ' + t.length + ' pixels và chiều cao ' + t[1] + ' pixels.';
    },
    email: function email(n) {
      return n + ' phải là một địa chỉ email hợp lệ.';
    },
    ext: function ext(n) {
      return n + ' phải là một tệp.';
    },
    image: function image(n) {
      return 'Trường ' + n + ' phải là một ảnh.';
    },
    included: function included(n) {
      return n + ' phải là một giá trị.';
    },
    ip: function ip(n) {
      return n + ' phải là một địa chỉ ip hợp lệ.';
    },
    max: function max(n, t) {
      return n + ' không thể có nhiều hơn ' + t.length + ' kí tự.';
    },
    max_value: function max_value(n, t) {
      return n + ' phải nhỏ hơn hoặc bằng ' + t.length + '.';
    },
    mimes: function mimes(n) {
      return n + ' phải chứa kiểu tệp phù hợp.';
    },
    min: function min(n, t) {
      return n + ' phải chứa ít nhất ' + t.length + ' kí tự.';
    },
    min_value: function min_value(n, t) {
      return n + ' phải lớn hơn hoặc bằng ' + t.length + '.';
    },
    excluded: function excluded(n) {
      return n + ' phải chứa một giá trị hợp lệ.';
    },
    numeric: function numeric(n) {
      return n + ' chỉ có thể có các kí tự số.';
    },
    regex: function regex(n) {
      return n + ' có định dạng không đúng.';
    },
    required: function required(n) {
      return n + ' không được để trống.';
    },
    size: function size(n, t) {
      var c,
          e,
          i,
          h = t.length;
      return n + ' chỉ có thể chứa tệp nhỏ hơn ' + (c = h, e = 1024, i = 0 == (c = Number(c) * e) ? 0 : Math.floor(Math.log(c) / Math.log(e)), 1 * (c / Math.pow(e, i)).toFixed(2) + ' ' + ['Byte', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'][i]) + '.';
    },
    url: function url(n) {
      return n + ' không phải là một địa chỉ URL hợp lệ.';
    }
  }
});

/***/ }),

/***/ "./src/js/partial/role/role.js":
/*!*************************************!*\
  !*** ./src/js/partial/role/role.js ***!
  \*************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js-exposed");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.runtime.esm.js");
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var vue_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! vue-router */ "./node_modules/vue-router/dist/vue-router.esm.js");
/* harmony import */ var $vuePartialPath_role_App__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! $vuePartialPath/role/App */ "./src/vue/partial/role/App.vue");
/* harmony import */ var $vuePartialPath_role_RoleIndexSection__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! $vuePartialPath/role/RoleIndexSection */ "./src/vue/partial/role/RoleIndexSection.vue");
/* harmony import */ var $vuePartialPath_role_RoleAddSection__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! $vuePartialPath/role/RoleAddSection */ "./src/vue/partial/role/RoleAddSection.vue");
/* harmony import */ var $vuePartialPath_role_RoleEditSection__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! $vuePartialPath/role/RoleEditSection */ "./src/vue/partial/role/RoleEditSection.vue");
/* harmony import */ var $vuePartialPath_role_RoleTable__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! $vuePartialPath/role/RoleTable */ "./src/vue/partial/role/RoleTable.vue");
/* harmony import */ var $jsStorePath_role__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! $jsStorePath/role */ "./src/js/store/role.js");










vue__WEBPACK_IMPORTED_MODULE_1__["default"].use(vuex__WEBPACK_IMPORTED_MODULE_2__["default"]);
vue__WEBPACK_IMPORTED_MODULE_1__["default"].use(vue_router__WEBPACK_IMPORTED_MODULE_3__["default"]);
var store = new vuex__WEBPACK_IMPORTED_MODULE_2__["default"].Store($jsStorePath_role__WEBPACK_IMPORTED_MODULE_9__["default"]);
var router = new vue_router__WEBPACK_IMPORTED_MODULE_3__["default"]({
  mode: 'history',
  routes: [{
    name: 'role:index',
    path: '/backend/role/index/:page',
    component: $vuePartialPath_role_RoleIndexSection__WEBPACK_IMPORTED_MODULE_5__["default"]
  }, {
    name: 'role:add',
    path: '/backend/role/add/:page',
    component: $vuePartialPath_role_RoleAddSection__WEBPACK_IMPORTED_MODULE_6__["default"]
  }, {
    name: 'role:edit',
    path: '/backend/role/edit/:id/:page',
    component: $vuePartialPath_role_RoleEditSection__WEBPACK_IMPORTED_MODULE_7__["default"]
  }]
});

var Object = function () {
  var _renderRoleTable = function _renderRoleTable() {
    var $el = jquery__WEBPACK_IMPORTED_MODULE_0___default()('#role-section');
    new vue__WEBPACK_IMPORTED_MODULE_1__["default"]({
      router: router,
      store: store,
      render: function render(h) {
        return h($vuePartialPath_role_App__WEBPACK_IMPORTED_MODULE_4__["default"]);
      }
    }).$mount($el[0]);
  };

  return {
    init: function init() {
      _renderRoleTable();
    }
  };
}();

document.addEventListener("DOMContentLoaded", function () {
  Object.init();
});

/***/ }),

/***/ "./src/js/store/role.js":
/*!******************************!*\
  !*** ./src/js/store/role.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var $jsCommonPath_api__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! $jsCommonPath/api */ "./src/js/common/api/index.js");
/* harmony import */ var $jsCommonPath_util_GeneralUtil__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! $jsCommonPath/util/GeneralUtil */ "./src/js/common/util/GeneralUtil.js");
/* harmony import */ var $jsCommonPath_util_SafeUtil__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! $jsCommonPath/util/SafeUtil */ "./src/js/common/util/SafeUtil.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }





var _objEntity = {
  role_id: null,
  role_name: null,
  is_fullaccess: null,
  is_deleted: null,
  created_at: 0,
  updated_at: 0
};

var Store = function Store() {
  _classCallCheck(this, Store);

  return {
    state: {
      flag: {
        isRead: false
      },
      arrEntityList: [],
      objEntitySelect: _objectSpread({}, _objEntity)
    },
    mutations: {
      resetEntity: function resetEntity(state) {
        state.entity.objEntitySelect = _objectSpread({}, _objEntity);
      },
      updateFlag: function updateFlag(state, payload) {
        state.flag[payload.flag] = payload.value;
      },
      setEntities: function setEntities(state, payload) {
        state.entity.arrEntityList = payload.arrEntityList;
      },
      setEntity: function setEntity(state, payload) {
        if (Object(lodash__WEBPACK_IMPORTED_MODULE_0__["isEmpty"])(payload.objEntitySelect)) return;

        var _payload = _objectSpread({}, payload),
            objEntitySelect = _payload.objEntitySelect;

        objEntitySelect.password = null;
        state.entity.objEntitySelect = objEntitySelect;
      },
      updateEntity: function updateEntity(state, payload) {
        var _payload2 = _objectSpread({}, payload),
            type = _payload2.type,
            index = _payload2.index,
            arrEntity = _payload2.arrEntity;

        switch (type) {
          case 'create':
            state.entity.arrEntityList.unshift(arrEntity);
            break;

          case 'update':
            state.entity.arrEntityList[index] = arrEntity;
            state.entity.objEntitySelect = arrEntity;
            break;

          case 'delete':
            state.entity.arrEntityList.splice(index, 1);
            break;
        }
      }
    },
    actions: {
      getEntityAct: function getEntityAct(_ref, payload) {
        var commit = _ref.commit;
        return new Promise(function (resolve, reject) {
          var reqParams = _objectSpread({}, payload);

          $jsCommonPath_api__WEBPACK_IMPORTED_MODULE_1__["getRoleReq"](reqParams).done(function (res) {
            if (Object($jsCommonPath_util_GeneralUtil__WEBPACK_IMPORTED_MODULE_2__["isErrorAPI"])(res)) {
              reject(res.payload);
              return;
            }

            var _res$payload = _objectSpread({}, res.payload),
                arrRoleDetail = _res$payload.arrRoleDetail;

            commit('setEntity', {
              objEntitySelect: arrRoleDetail
            });
            resolve(res.payload);
          });
        });
      },
      readEntityAct: function readEntityAct(_ref2) {
        var commit = _ref2.commit;
        return new Promise(function (resolve, reject) {
          var reqParams = {};
          $jsCommonPath_api__WEBPACK_IMPORTED_MODULE_1__["readRoleReq"](reqParams).done(function (res) {
            if (Object($jsCommonPath_util_GeneralUtil__WEBPACK_IMPORTED_MODULE_2__["isErrorAPI"])(res)) {
              reject(res.payload);
              return;
            }

            var _res$payload2 = _objectSpread({}, res.payload),
                arrRoleList = _res$payload2.arrRoleList;

            commit('setEntities', {
              arrEntityList: arrRoleList
            });
            resolve(res.payload);
          });
        });
      },
      uploadFileAct: function uploadFileAct(_ref3, payload) {
        var commit = _ref3.commit;
        return new Promise(function (resolve, reject) {
          var formData = new FormData();
          formData.append('controller', payload.controller);
          formData.append('file', payload.file);
          $jsCommonPath_api__WEBPACK_IMPORTED_MODULE_1__["uploadReq"](formData, {
            processData: false,
            contentType: false
          }).done(function (res) {
            if (Object($jsCommonPath_util_GeneralUtil__WEBPACK_IMPORTED_MODULE_2__["isErrorAPI"])(res)) {
              Object($jsCommonPath_util_GeneralUtil__WEBPACK_IMPORTED_MODULE_2__["triggerModal"])(res.message);
              reject(res.payload);
              return;
            }

            resolve(res.payload);
          });
        });
      },
      setEntityAct: function setEntityAct(_ref4, payload) {
        var commit = _ref4.commit,
            getters = _ref4.getters;
        if (!payload.hasOwnProperty('index')) return;

        var _payload3 = _objectSpread({}, payload),
            index = _payload3.index;

        commit('setEntity', {
          objEntitySelect: getters.getEntityByIndex(index)[0]
        });
      },
      createEntityAct: function createEntityAct(_ref5, payload) {
        var commit = _ref5.commit,
            getters = _ref5.getters;
        return new Promise(function (resolve, reject) {
          var reqParams = _objectSpread({}, payload);

          $jsCommonPath_api__WEBPACK_IMPORTED_MODULE_1__["createRoleReq"](reqParams).done(function (res) {
            Object($jsCommonPath_util_GeneralUtil__WEBPACK_IMPORTED_MODULE_2__["triggerModal"])(res.message);

            if (Object($jsCommonPath_util_GeneralUtil__WEBPACK_IMPORTED_MODULE_2__["isErrorAPI"])(res)) {
              reject(res.payload);
              return;
            }

            var _res$payload3 = _objectSpread({}, res.payload),
                arrRoleDetail = _res$payload3.arrRoleDetail;

            commit('updateEntity', {
              type: 'create',
              arrEntity: arrRoleDetail
            });
            resolve(res.payload);
          });
        });
      },
      updateEntityAct: function updateEntityAct(_ref6, payload) {
        var commit = _ref6.commit,
            getters = _ref6.getters;
        return new Promise(function (resolve, reject) {
          var reqParams = _objectSpread({}, payload);

          $jsCommonPath_api__WEBPACK_IMPORTED_MODULE_1__["updateRoleReq"](reqParams).done(function (res) {
            Object($jsCommonPath_util_GeneralUtil__WEBPACK_IMPORTED_MODULE_2__["triggerModal"])(res.message);

            if (Object($jsCommonPath_util_GeneralUtil__WEBPACK_IMPORTED_MODULE_2__["isErrorAPI"])(res)) {
              reject(res.payload);
              return;
            }

            var _res$payload4 = _objectSpread({}, res.payload),
                arrRoleDetail = _res$payload4.arrRoleDetail;

            commit('updateEntity', {
              type: 'update',
              index: getters.getIndexByID(arrRoleDetail.role_id),
              arrEntity: arrRoleDetail
            });
            resolve(res.payload);
          });
        });
      },
      deleteEntityAct: function deleteEntityAct(_ref7, payload) {
        var commit = _ref7.commit,
            getters = _ref7.getters;
        return new Promise(function (resolve, reject) {
          var reqParams = _objectSpread({}, payload);

          $jsCommonPath_api__WEBPACK_IMPORTED_MODULE_1__["deleteRoleReq"](reqParams).done(function (res) {
            Object($jsCommonPath_util_GeneralUtil__WEBPACK_IMPORTED_MODULE_2__["triggerModal"])(res.message);

            if (Object($jsCommonPath_util_GeneralUtil__WEBPACK_IMPORTED_MODULE_2__["isErrorAPI"])(res)) {
              reject(res.payload);
              return;
            }

            var _res$payload5 = _objectSpread({}, res.payload),
                id = _res$payload5.id;

            commit('updateEntity', {
              type: 'delete',
              index: getters.getIndexByID(id)
            });
            resolve(res.payload);
          });
        });
      }
    },
    getters: {
      getEntityByIndex: function getEntityByIndex(state) {
        return function (index) {
          return state.entity.arrEntityList.filter(function (objEntity, currentIndex) {
            return currentIndex == index;
          });
        };
      },
      getIndexByID: function getIndexByID(state) {
        return function (id) {
          var index = -1;

          Object(lodash__WEBPACK_IMPORTED_MODULE_0__["forIn"])(state.entity.arrEntityList, function (objEntity, currentIndex) {
            if (objEntity.role_id != id) return;
            index = currentIndex;
            return false;
          });

          return index;
        };
      }
    }
  };
};

/* harmony default export */ __webpack_exports__["default"] = (new Store());

/***/ }),

/***/ "./src/vue/partial/global/TheModalFullScreen.vue":
/*!*******************************************************!*\
  !*** ./src/vue/partial/global/TheModalFullScreen.vue ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _TheModalFullScreen_vue_vue_type_template_id_1e69cc63___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./TheModalFullScreen.vue?vue&type=template&id=1e69cc63& */ "./src/vue/partial/global/TheModalFullScreen.vue?vue&type=template&id=1e69cc63&");
/* harmony import */ var _TheModalFullScreen_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./TheModalFullScreen.vue?vue&type=script&lang=js& */ "./src/vue/partial/global/TheModalFullScreen.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _TheModalFullScreen_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _TheModalFullScreen_vue_vue_type_template_id_1e69cc63___WEBPACK_IMPORTED_MODULE_0__["render"],
  _TheModalFullScreen_vue_vue_type_template_id_1e69cc63___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null

)

/* hot reload */
if (false) { var api; }
component.options.__file = "src/vue/partial/global/TheModalFullScreen.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./src/vue/partial/global/TheModalFullScreen.vue?vue&type=script&lang=js&":
/*!********************************************************************************!*\
  !*** ./src/vue/partial/global/TheModalFullScreen.vue?vue&type=script&lang=js& ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_1_node_modules_vue_loader_lib_index_js_vue_loader_options_TheModalFullScreen_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--1!../../../../node_modules/vue-loader/lib??vue-loader-options!./TheModalFullScreen.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/global/TheModalFullScreen.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_1_node_modules_vue_loader_lib_index_js_vue_loader_options_TheModalFullScreen_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]);

/***/ }),

/***/ "./src/vue/partial/global/TheModalFullScreen.vue?vue&type=template&id=1e69cc63&":
/*!**************************************************************************************!*\
  !*** ./src/vue/partial/global/TheModalFullScreen.vue?vue&type=template&id=1e69cc63& ***!
  \**************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TheModalFullScreen_vue_vue_type_template_id_1e69cc63___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./TheModalFullScreen.vue?vue&type=template&id=1e69cc63& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/global/TheModalFullScreen.vue?vue&type=template&id=1e69cc63&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TheModalFullScreen_vue_vue_type_template_id_1e69cc63___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TheModalFullScreen_vue_vue_type_template_id_1e69cc63___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./src/vue/partial/role/App.vue":
/*!**************************************!*\
  !*** ./src/vue/partial/role/App.vue ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _App_vue_vue_type_template_id_6df09af8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./App.vue?vue&type=template&id=6df09af8& */ "./src/vue/partial/role/App.vue?vue&type=template&id=6df09af8&");
/* harmony import */ var _App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./App.vue?vue&type=script&lang=js& */ "./src/vue/partial/role/App.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _App_vue_vue_type_template_id_6df09af8___WEBPACK_IMPORTED_MODULE_0__["render"],
  _App_vue_vue_type_template_id_6df09af8___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null

)

/* hot reload */
if (false) { var api; }
component.options.__file = "src/vue/partial/role/App.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./src/vue/partial/role/App.vue?vue&type=script&lang=js&":
/*!***************************************************************!*\
  !*** ./src/vue/partial/role/App.vue?vue&type=script&lang=js& ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_1_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--1!../../../../node_modules/vue-loader/lib??vue-loader-options!./App.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/role/App.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_1_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]);

/***/ }),

/***/ "./src/vue/partial/role/App.vue?vue&type=template&id=6df09af8&":
/*!*********************************************************************!*\
  !*** ./src/vue/partial/role/App.vue?vue&type=template&id=6df09af8& ***!
  \*********************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_template_id_6df09af8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./App.vue?vue&type=template&id=6df09af8& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/role/App.vue?vue&type=template&id=6df09af8&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_template_id_6df09af8___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_template_id_6df09af8___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./src/vue/partial/role/RoleAddSection.vue":
/*!*************************************************!*\
  !*** ./src/vue/partial/role/RoleAddSection.vue ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _RoleAddSection_vue_vue_type_template_id_36d1e993___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./RoleAddSection.vue?vue&type=template&id=36d1e993& */ "./src/vue/partial/role/RoleAddSection.vue?vue&type=template&id=36d1e993&");
/* harmony import */ var _RoleAddSection_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./RoleAddSection.vue?vue&type=script&lang=js& */ "./src/vue/partial/role/RoleAddSection.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _RoleAddSection_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _RoleAddSection_vue_vue_type_template_id_36d1e993___WEBPACK_IMPORTED_MODULE_0__["render"],
  _RoleAddSection_vue_vue_type_template_id_36d1e993___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null

)

/* hot reload */
if (false) { var api; }
component.options.__file = "src/vue/partial/role/RoleAddSection.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./src/vue/partial/role/RoleAddSection.vue?vue&type=script&lang=js&":
/*!**************************************************************************!*\
  !*** ./src/vue/partial/role/RoleAddSection.vue?vue&type=script&lang=js& ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_1_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleAddSection_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--1!../../../../node_modules/vue-loader/lib??vue-loader-options!./RoleAddSection.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/role/RoleAddSection.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_1_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleAddSection_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]);

/***/ }),

/***/ "./src/vue/partial/role/RoleAddSection.vue?vue&type=template&id=36d1e993&":
/*!********************************************************************************!*\
  !*** ./src/vue/partial/role/RoleAddSection.vue?vue&type=template&id=36d1e993& ***!
  \********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleAddSection_vue_vue_type_template_id_36d1e993___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./RoleAddSection.vue?vue&type=template&id=36d1e993& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/role/RoleAddSection.vue?vue&type=template&id=36d1e993&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleAddSection_vue_vue_type_template_id_36d1e993___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleAddSection_vue_vue_type_template_id_36d1e993___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./src/vue/partial/role/RoleEditSection.vue":
/*!**************************************************!*\
  !*** ./src/vue/partial/role/RoleEditSection.vue ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _RoleEditSection_vue_vue_type_template_id_f0ba3908___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./RoleEditSection.vue?vue&type=template&id=f0ba3908& */ "./src/vue/partial/role/RoleEditSection.vue?vue&type=template&id=f0ba3908&");
/* harmony import */ var _RoleEditSection_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./RoleEditSection.vue?vue&type=script&lang=js& */ "./src/vue/partial/role/RoleEditSection.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _RoleEditSection_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _RoleEditSection_vue_vue_type_template_id_f0ba3908___WEBPACK_IMPORTED_MODULE_0__["render"],
  _RoleEditSection_vue_vue_type_template_id_f0ba3908___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null

)

/* hot reload */
if (false) { var api; }
component.options.__file = "src/vue/partial/role/RoleEditSection.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./src/vue/partial/role/RoleEditSection.vue?vue&type=script&lang=js&":
/*!***************************************************************************!*\
  !*** ./src/vue/partial/role/RoleEditSection.vue?vue&type=script&lang=js& ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_1_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleEditSection_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--1!../../../../node_modules/vue-loader/lib??vue-loader-options!./RoleEditSection.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/role/RoleEditSection.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_1_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleEditSection_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]);

/***/ }),

/***/ "./src/vue/partial/role/RoleEditSection.vue?vue&type=template&id=f0ba3908&":
/*!*********************************************************************************!*\
  !*** ./src/vue/partial/role/RoleEditSection.vue?vue&type=template&id=f0ba3908& ***!
  \*********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleEditSection_vue_vue_type_template_id_f0ba3908___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./RoleEditSection.vue?vue&type=template&id=f0ba3908& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/role/RoleEditSection.vue?vue&type=template&id=f0ba3908&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleEditSection_vue_vue_type_template_id_f0ba3908___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleEditSection_vue_vue_type_template_id_f0ba3908___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./src/vue/partial/role/RoleIndexSection.vue":
/*!***************************************************!*\
  !*** ./src/vue/partial/role/RoleIndexSection.vue ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _RoleIndexSection_vue_vue_type_template_id_05923f02___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./RoleIndexSection.vue?vue&type=template&id=05923f02& */ "./src/vue/partial/role/RoleIndexSection.vue?vue&type=template&id=05923f02&");
/* harmony import */ var _RoleIndexSection_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./RoleIndexSection.vue?vue&type=script&lang=js& */ "./src/vue/partial/role/RoleIndexSection.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _RoleIndexSection_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _RoleIndexSection_vue_vue_type_template_id_05923f02___WEBPACK_IMPORTED_MODULE_0__["render"],
  _RoleIndexSection_vue_vue_type_template_id_05923f02___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null

)

/* hot reload */
if (false) { var api; }
component.options.__file = "src/vue/partial/role/RoleIndexSection.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./src/vue/partial/role/RoleIndexSection.vue?vue&type=script&lang=js&":
/*!****************************************************************************!*\
  !*** ./src/vue/partial/role/RoleIndexSection.vue?vue&type=script&lang=js& ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_1_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleIndexSection_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--1!../../../../node_modules/vue-loader/lib??vue-loader-options!./RoleIndexSection.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/role/RoleIndexSection.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_1_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleIndexSection_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]);

/***/ }),

/***/ "./src/vue/partial/role/RoleIndexSection.vue?vue&type=template&id=05923f02&":
/*!**********************************************************************************!*\
  !*** ./src/vue/partial/role/RoleIndexSection.vue?vue&type=template&id=05923f02& ***!
  \**********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleIndexSection_vue_vue_type_template_id_05923f02___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./RoleIndexSection.vue?vue&type=template&id=05923f02& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/role/RoleIndexSection.vue?vue&type=template&id=05923f02&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleIndexSection_vue_vue_type_template_id_05923f02___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleIndexSection_vue_vue_type_template_id_05923f02___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./src/vue/partial/role/RoleTable.vue":
/*!********************************************!*\
  !*** ./src/vue/partial/role/RoleTable.vue ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _RoleTable_vue_vue_type_template_id_4065370f___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./RoleTable.vue?vue&type=template&id=4065370f& */ "./src/vue/partial/role/RoleTable.vue?vue&type=template&id=4065370f&");
/* harmony import */ var _RoleTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./RoleTable.vue?vue&type=script&lang=js& */ "./src/vue/partial/role/RoleTable.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _RoleTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _RoleTable_vue_vue_type_template_id_4065370f___WEBPACK_IMPORTED_MODULE_0__["render"],
  _RoleTable_vue_vue_type_template_id_4065370f___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null

)

/* hot reload */
if (false) { var api; }
component.options.__file = "src/vue/partial/role/RoleTable.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./src/vue/partial/role/RoleTable.vue?vue&type=script&lang=js&":
/*!*********************************************************************!*\
  !*** ./src/vue/partial/role/RoleTable.vue?vue&type=script&lang=js& ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_1_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--1!../../../../node_modules/vue-loader/lib??vue-loader-options!./RoleTable.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/role/RoleTable.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_1_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]);

/***/ }),

/***/ "./src/vue/partial/role/RoleTable.vue?vue&type=template&id=4065370f&":
/*!***************************************************************************!*\
  !*** ./src/vue/partial/role/RoleTable.vue?vue&type=template&id=4065370f& ***!
  \***************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleTable_vue_vue_type_template_id_4065370f___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./RoleTable.vue?vue&type=template&id=4065370f& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/role/RoleTable.vue?vue&type=template&id=4065370f&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleTable_vue_vue_type_template_id_4065370f___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleTable_vue_vue_type_template_id_4065370f___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./src/vue/partial/role/RoleTableRow.vue":
/*!***********************************************!*\
  !*** ./src/vue/partial/role/RoleTableRow.vue ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _RoleTableRow_vue_vue_type_template_id_26f59b7b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./RoleTableRow.vue?vue&type=template&id=26f59b7b& */ "./src/vue/partial/role/RoleTableRow.vue?vue&type=template&id=26f59b7b&");
/* harmony import */ var _RoleTableRow_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./RoleTableRow.vue?vue&type=script&lang=js& */ "./src/vue/partial/role/RoleTableRow.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _RoleTableRow_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _RoleTableRow_vue_vue_type_template_id_26f59b7b___WEBPACK_IMPORTED_MODULE_0__["render"],
  _RoleTableRow_vue_vue_type_template_id_26f59b7b___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null

)

/* hot reload */
if (false) { var api; }
component.options.__file = "src/vue/partial/role/RoleTableRow.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./src/vue/partial/role/RoleTableRow.vue?vue&type=script&lang=js&":
/*!************************************************************************!*\
  !*** ./src/vue/partial/role/RoleTableRow.vue?vue&type=script&lang=js& ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_1_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleTableRow_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--1!../../../../node_modules/vue-loader/lib??vue-loader-options!./RoleTableRow.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/role/RoleTableRow.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_1_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleTableRow_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]);

/***/ }),

/***/ "./src/vue/partial/role/RoleTableRow.vue?vue&type=template&id=26f59b7b&":
/*!******************************************************************************!*\
  !*** ./src/vue/partial/role/RoleTableRow.vue?vue&type=template&id=26f59b7b& ***!
  \******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleTableRow_vue_vue_type_template_id_26f59b7b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./RoleTableRow.vue?vue&type=template&id=26f59b7b& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/vue/partial/role/RoleTableRow.vue?vue&type=template&id=26f59b7b&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleTableRow_vue_vue_type_template_id_26f59b7b___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_RoleTableRow_vue_vue_type_template_id_26f59b7b___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ 6:
/*!****************************************************************************************************************!*\
  !*** multi (webpack)-dev-server/client?http://localhost:1236 ./src/js/vendor.js ./src/js/partial/role/role.js ***!
  \****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! D:\Project\share\outsouce_thuenha\html\static\backend\v1\node_modules\webpack-dev-server\client\index.js?http://localhost:1236 */"./node_modules/webpack-dev-server/client/index.js?http://localhost:1236");
__webpack_require__(/*! D:\Project\share\outsouce_thuenha\html\static\backend\v1\src/js/vendor.js */"./src/js/vendor.js");
module.exports = __webpack_require__(/*! D:\Project\share\outsouce_thuenha\html\static\backend\v1\src/js/partial/role/role.js */"./src/js/partial/role/role.js");


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vL3NyYy92dWUvcGFydGlhbC9nbG9iYWwvVGhlTW9kYWxGdWxsU2NyZWVuLnZ1ZSIsIndlYnBhY2s6Ly8vc3JjL3Z1ZS9wYXJ0aWFsL3JvbGUvQXBwLnZ1ZSIsIndlYnBhY2s6Ly8vc3JjL3Z1ZS9wYXJ0aWFsL3JvbGUvUm9sZUFkZFNlY3Rpb24udnVlIiwid2VicGFjazovLy9zcmMvdnVlL3BhcnRpYWwvcm9sZS9Sb2xlRWRpdFNlY3Rpb24udnVlIiwid2VicGFjazovLy9zcmMvdnVlL3BhcnRpYWwvcm9sZS9Sb2xlSW5kZXhTZWN0aW9uLnZ1ZSIsIndlYnBhY2s6Ly8vc3JjL3Z1ZS9wYXJ0aWFsL3JvbGUvUm9sZVRhYmxlLnZ1ZSIsIndlYnBhY2s6Ly8vc3JjL3Z1ZS9wYXJ0aWFsL3JvbGUvUm9sZVRhYmxlUm93LnZ1ZSIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvdmVlLXZhbGlkYXRlL2Rpc3QvcnVsZXMuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3ZlZS12YWxpZGF0ZS9kaXN0L3ZlZS12YWxpZGF0ZS5lc20uanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3Z1ZS9wYXJ0aWFsL2dsb2JhbC9UaGVNb2RhbEZ1bGxTY3JlZW4udnVlPzNjMjQiLCJ3ZWJwYWNrOi8vLy4vc3JjL3Z1ZS9wYXJ0aWFsL3JvbGUvQXBwLnZ1ZT8zYTY4Iiwid2VicGFjazovLy8uL3NyYy92dWUvcGFydGlhbC9yb2xlL1JvbGVBZGRTZWN0aW9uLnZ1ZT9kZmNmIiwid2VicGFjazovLy8uL3NyYy92dWUvcGFydGlhbC9yb2xlL1JvbGVFZGl0U2VjdGlvbi52dWU/YWQ0ZCIsIndlYnBhY2s6Ly8vLi9zcmMvdnVlL3BhcnRpYWwvcm9sZS9Sb2xlSW5kZXhTZWN0aW9uLnZ1ZT9hZWIwIiwid2VicGFjazovLy8uL3NyYy92dWUvcGFydGlhbC9yb2xlL1JvbGVUYWJsZS52dWU/OTVhZiIsIndlYnBhY2s6Ly8vLi9zcmMvdnVlL3BhcnRpYWwvcm9sZS9Sb2xlVGFibGVSb3cudnVlPzE0NzciLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3Z1ZS1yb3V0ZXIvZGlzdC92dWUtcm91dGVyLmVzbS5qcyIsIndlYnBhY2s6Ly8vKHdlYnBhY2spL2hvdCBzeW5jIG5vbnJlY3Vyc2l2ZSBeXFwuXFwvbG9nJCIsIndlYnBhY2s6Ly8vLi9zcmMvanMvY29tbW9uL3V0aWwvVmVlVmFsaWRhdGVNZXNzYWdlLmpzIiwid2VicGFjazovLy8uL3NyYy9qcy9wYXJ0aWFsL3JvbGUvcm9sZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvanMvc3RvcmUvcm9sZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvdnVlL3BhcnRpYWwvZ2xvYmFsL1RoZU1vZGFsRnVsbFNjcmVlbi52dWUiLCJ3ZWJwYWNrOi8vLy4vc3JjL3Z1ZS9wYXJ0aWFsL2dsb2JhbC9UaGVNb2RhbEZ1bGxTY3JlZW4udnVlP2EzY2EiLCJ3ZWJwYWNrOi8vLy4vc3JjL3Z1ZS9wYXJ0aWFsL2dsb2JhbC9UaGVNb2RhbEZ1bGxTY3JlZW4udnVlPzBkZTAiLCJ3ZWJwYWNrOi8vLy4vc3JjL3Z1ZS9wYXJ0aWFsL3JvbGUvQXBwLnZ1ZSIsIndlYnBhY2s6Ly8vLi9zcmMvdnVlL3BhcnRpYWwvcm9sZS9BcHAudnVlPzYzMDkiLCJ3ZWJwYWNrOi8vLy4vc3JjL3Z1ZS9wYXJ0aWFsL3JvbGUvQXBwLnZ1ZT8wZDZlIiwid2VicGFjazovLy8uL3NyYy92dWUvcGFydGlhbC9yb2xlL1JvbGVBZGRTZWN0aW9uLnZ1ZSIsIndlYnBhY2s6Ly8vLi9zcmMvdnVlL3BhcnRpYWwvcm9sZS9Sb2xlQWRkU2VjdGlvbi52dWU/Yjk5MCIsIndlYnBhY2s6Ly8vLi9zcmMvdnVlL3BhcnRpYWwvcm9sZS9Sb2xlQWRkU2VjdGlvbi52dWU/Yjc0YiIsIndlYnBhY2s6Ly8vLi9zcmMvdnVlL3BhcnRpYWwvcm9sZS9Sb2xlRWRpdFNlY3Rpb24udnVlIiwid2VicGFjazovLy8uL3NyYy92dWUvcGFydGlhbC9yb2xlL1JvbGVFZGl0U2VjdGlvbi52dWU/NjMxZiIsIndlYnBhY2s6Ly8vLi9zcmMvdnVlL3BhcnRpYWwvcm9sZS9Sb2xlRWRpdFNlY3Rpb24udnVlPzhlMWMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3Z1ZS9wYXJ0aWFsL3JvbGUvUm9sZUluZGV4U2VjdGlvbi52dWUiLCJ3ZWJwYWNrOi8vLy4vc3JjL3Z1ZS9wYXJ0aWFsL3JvbGUvUm9sZUluZGV4U2VjdGlvbi52dWU/MGQ4YiIsIndlYnBhY2s6Ly8vLi9zcmMvdnVlL3BhcnRpYWwvcm9sZS9Sb2xlSW5kZXhTZWN0aW9uLnZ1ZT84Zjg5Iiwid2VicGFjazovLy8uL3NyYy92dWUvcGFydGlhbC9yb2xlL1JvbGVUYWJsZS52dWUiLCJ3ZWJwYWNrOi8vLy4vc3JjL3Z1ZS9wYXJ0aWFsL3JvbGUvUm9sZVRhYmxlLnZ1ZT84Y2ZkIiwid2VicGFjazovLy8uL3NyYy92dWUvcGFydGlhbC9yb2xlL1JvbGVUYWJsZS52dWU/ZmI4YiIsIndlYnBhY2s6Ly8vLi9zcmMvdnVlL3BhcnRpYWwvcm9sZS9Sb2xlVGFibGVSb3cudnVlIiwid2VicGFjazovLy8uL3NyYy92dWUvcGFydGlhbC9yb2xlL1JvbGVUYWJsZVJvdy52dWU/OGYyZCIsIndlYnBhY2s6Ly8vLi9zcmMvdnVlL3BhcnRpYWwvcm9sZS9Sb2xlVGFibGVSb3cudnVlPzY1NzkiXSwibmFtZXMiOlsibmFtZSIsIm1lc3NhZ2VzIiwiX2RlZmF1bHQiLCJuIiwiYWZ0ZXIiLCJ0IiwibGVuZ3RoIiwiYWxwaGFfZGFzaCIsImFscGhhX251bSIsImFscGhhX3NwYWNlcyIsImFscGhhIiwiYmVmb3JlIiwiYmV0d2VlbiIsImNvbmZpcm1lZCIsImNyZWRpdF9jYXJkIiwiZGF0ZV9iZXR3ZWVuIiwiZGF0ZV9mb3JtYXQiLCJkZWNpbWFsIiwiYyIsImRpZ2l0cyIsImRpbWVuc2lvbnMiLCJlbWFpbCIsImV4dCIsImltYWdlIiwiaW5jbHVkZWQiLCJpcCIsIm1heCIsIm1heF92YWx1ZSIsIm1pbWVzIiwibWluIiwibWluX3ZhbHVlIiwiZXhjbHVkZWQiLCJudW1lcmljIiwicmVnZXgiLCJyZXF1aXJlZCIsInNpemUiLCJlIiwiaSIsImgiLCJOdW1iZXIiLCJNYXRoIiwiZmxvb3IiLCJsb2ciLCJwb3ciLCJ0b0ZpeGVkIiwidXJsIiwiVnVlIiwidXNlIiwiVnVleCIsIlZ1ZVJvdXRlciIsInN0b3JlIiwiU3RvcmUiLCJyb3V0ZXIiLCJtb2RlIiwicm91dGVzIiwicGF0aCIsImNvbXBvbmVudCIsIlJvbGVJbmRleFNlY3Rpb24iLCJSb2xlQWRkU2VjdGlvbiIsIlJvbGVFZGl0U2VjdGlvbiIsIk9iamVjdCIsIl9yZW5kZXJSb2xlVGFibGUiLCIkZWwiLCIkIiwicmVuZGVyIiwiQXBwIiwiJG1vdW50IiwiaW5pdCIsImRvY3VtZW50IiwiYWRkRXZlbnRMaXN0ZW5lciIsIl9vYmpFbnRpdHkiLCJyb2xlX2lkIiwicm9sZV9uYW1lIiwiaXNfZnVsbGFjY2VzcyIsImlzX2RlbGV0ZWQiLCJjcmVhdGVkX2F0IiwidXBkYXRlZF9hdCIsInN0YXRlIiwiZmxhZyIsImlzR2V0IiwiYXJyRW50aXR5TGlzdCIsIm9iakVudGl0eVNlbGVjdCIsIm11dGF0aW9ucyIsInJlc2V0RW50aXR5IiwidXBkYXRlRmxhZyIsInBheWxvYWQiLCJ2YWx1ZSIsInNldEVudGl0aWVzIiwic2V0RW50aXR5IiwiX2lzRW1wdHkiLCJwYXNzd29yZCIsInVwZGF0ZUVudGl0eSIsInR5cGUiLCJpbmRleCIsImFyckVudGl0eSIsInB1c2giLCJzcGxpY2UiLCJhY3Rpb25zIiwiZ2V0RW50aXR5QWN0IiwiY29tbWl0IiwiUHJvbWlzZSIsInJlc29sdmUiLCJyZWplY3QiLCJyZXFQYXJhbXMiLCJBUEkiLCJkb25lIiwicmVzIiwiaXNFcnJvckFQSSIsImFyclJvbGVEZXRhaWwiLCJyZWFkRW50aXR5QWN0IiwiYXJyUm9sZUxpc3QiLCJ1cGxvYWRGaWxlQWN0IiwiZm9ybURhdGEiLCJGb3JtRGF0YSIsImFwcGVuZCIsImNvbnRyb2xsZXIiLCJmaWxlIiwicHJvY2Vzc0RhdGEiLCJjb250ZW50VHlwZSIsInRyaWdnZXJNb2RhbCIsIm1lc3NhZ2UiLCJzZXRFbnRpdHlBY3QiLCJnZXR0ZXJzIiwiaGFzT3duUHJvcGVydHkiLCJnZXRFbnRpdHlCeUluZGV4IiwiY3JlYXRlRW50aXR5QWN0IiwidXBkYXRlRW50aXR5QWN0IiwiZ2V0SW5kZXhCeUlEIiwiZGVsZXRlRW50aXR5QWN0IiwiaWQiLCJmaWx0ZXIiLCJvYmpFbnRpdHkiLCJjdXJyZW50SW5kZXgiLCJfZm9ySW4iXSwibWFwcGluZ3MiOiI7UUFBQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLFFBQVEsb0JBQW9CO1FBQzVCO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EsaUJBQWlCLDRCQUE0QjtRQUM3QztRQUNBO1FBQ0Esa0JBQWtCLDJCQUEyQjtRQUM3QztRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBOzs7UUFHQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMENBQTBDLGdDQUFnQztRQUMxRTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLHdEQUF3RCxrQkFBa0I7UUFDMUU7UUFDQSxpREFBaUQsY0FBYztRQUMvRDs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EseUNBQXlDLGlDQUFpQztRQUMxRSxnSEFBZ0gsbUJBQW1CLEVBQUU7UUFDckk7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwyQkFBMkIsMEJBQTBCLEVBQUU7UUFDdkQsaUNBQWlDLGVBQWU7UUFDaEQ7UUFDQTtRQUNBOztRQUVBO1FBQ0Esc0RBQXNELCtEQUErRDs7UUFFckg7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLGdCQUFnQix1QkFBdUI7UUFDdkM7OztRQUdBO1FBQ0E7UUFDQTtRQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDL0dBO0FBQ0E7QUFFQTtBQUNBLGVBREE7QUFFQSxnQkFGQTtBQUdBLE1BSEEsa0JBR0E7QUFDQTtBQUNBLDBCQURBO0FBRUE7QUFGQTtBQUlBLEdBUkE7QUFTQSxjQVRBO0FBVUEsU0FWQSxxQkFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBRkE7QUFJQTtBQUNBO0FBRUEsMENBQ0E7QUFFQTtBQUNBLGlCQUNBO0FBRUE7QUFDQTtBQUNBLEtBWkE7QUFhQSxHQTdCQTtBQThCQTtBQUNBLFdBREEscUJBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQTlCQSxHOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN0Q0E7QUFDQSxrQkFEQTtBQUVBLGdCQUZBO0FBR0EsTUFIQSxrQkFHQTtBQUNBO0FBQ0EsR0FMQTtBQU1BLGNBTkE7QUFPQSxTQVBBLHFCQU9BLENBRUEsQ0FUQTtBQVVBO0FBVkEsRzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNpR0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFHQTtBQUVBO0FBQUE7QUFBQTtBQUVBO0FBQ0EsMEJBREE7QUFFQTtBQUNBLDBGQURBO0FBRUE7QUFGQSxHQUZBO0FBTUEsTUFOQSxrQkFNQTtBQUNBO0FBQ0E7QUFDQTtBQURBLE9BREE7QUFJQTtBQUNBLDJCQURBO0FBRUE7QUFGQSxPQUpBO0FBUUE7QUFSQTtBQVVBLEdBakJBO0FBa0JBO0FBQ0EsZ0JBREEsMEJBQ0E7QUFDQSw4RkFDQTtBQUVBO0FBQ0EsS0FOQTtBQU9BLG1CQVBBLDZCQU9BO0FBQ0E7QUFDQTtBQVRBLEdBbEJBO0FBNkJBLFNBN0JBLHFCQTZCQTtBQUFBOztBQUNBO0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFFQTtBQUNBLEtBUEE7QUFTQTtBQUNBO0FBQ0EsR0E1Q0E7QUE2Q0E7QUFDQSxVQURBLG9CQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0EsS0FIQTtBQUlBLFlBSkEsc0JBSUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQSxLQU5BO0FBT0EsUUFQQSxrQkFPQTtBQUNBO0FBQ0EsS0FUQTtBQVVBLFVBVkEsa0JBVUEsT0FWQSxFQVVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEtBcEJBO0FBcUJBLFVBckJBLGtCQXFCQSxDQXJCQSxFQXFCQTtBQUFBOztBQUNBO0FBRUE7QUFDQSxvQ0FEQTtBQUVBO0FBRkEsU0FHQSxJQUhBLENBR0E7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxPQVZBO0FBV0E7QUFuQ0E7QUE3Q0EsRzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNmQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBRUE7QUFBQTtBQUFBO0FBRUE7QUFDQSwwQkFEQTtBQUVBO0FBQ0EsMEZBREE7QUFFQTtBQUZBLEdBRkE7QUFNQSxNQU5BLGtCQU1BO0FBQ0E7QUFDQTtBQUNBO0FBREEsT0FEQTtBQUlBO0FBQ0EsMkJBREE7QUFFQTtBQUZBLE9BSkE7QUFRQTtBQVJBO0FBVUEsR0FqQkE7QUFrQkE7QUFDQSxnQkFEQSwwQkFDQTtBQUNBLDhGQUNBO0FBRUE7QUFDQSxLQU5BO0FBT0EsbUJBUEEsNkJBT0E7QUFDQTtBQUNBO0FBVEEsR0FsQkE7QUE2QkEsU0E3QkEscUJBNkJBO0FBQUE7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBRUE7QUFDQSxLQVBBOztBQVNBO0FBQUEsZ0NBQ0Esa0JBREE7QUFBQSxVQUNBLEVBREEsdUJBQ0EsRUFEQTtBQUFBLFVBQ0EsSUFEQSx1QkFDQSxJQURBO0FBRUEsNkZBQ0E7QUFFQTtBQUNBLEtBTkEsTUFNQTtBQUNBO0FBQ0E7QUFDQSxHQWxEQTtBQW1EQTtBQUNBLFVBREEsb0JBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQSxLQUhBO0FBSUEsWUFKQSxzQkFJQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBLEtBTkE7QUFPQSxRQVBBLGtCQU9BO0FBQ0E7QUFDQSxLQVRBO0FBVUEsT0FWQSxlQVVBLEVBVkEsRUFVQTtBQUFBOztBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0EsT0FGQTtBQUdBLEtBZEE7QUFlQSxVQWZBLGtCQWVBLE9BZkEsRUFlQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFDQSxLQTNCQTtBQTRCQSxVQTVCQSxrQkE0QkEsQ0E1QkEsRUE0QkE7QUFBQTs7QUFDQTtBQUVBO0FBQ0Esb0NBREE7QUFFQTtBQUZBLFNBR0EsSUFIQSxDQUdBO0FBQ0E7O0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsT0FWQTtBQVdBO0FBMUNBO0FBbkRBLEc7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3BGQTtBQUVBO0FBQ0EsNEJBREE7QUFFQTtBQUNBO0FBREEsR0FGQTtBQUtBLE1BTEEsa0JBS0E7QUFDQTtBQUNBLEdBUEE7QUFRQSxjQVJBO0FBU0EsU0FUQSxxQkFTQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0EsR0FkQTtBQWVBO0FBQ0EsVUFEQSxvQkFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBLEtBSEE7QUFJQSxZQUpBLHNCQUlBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFOQTtBQWZBLEc7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNKQTtBQUVBO0FBQ0Esb0JBREE7QUFFQTtBQUNBO0FBREEsR0FGQTtBQUtBLE1BTEEsa0JBS0E7QUFDQTtBQUNBLEdBUEE7QUFRQTtBQUNBLGlCQURBLDJCQUNBO0FBQ0E7QUFDQTtBQUhBLEdBUkE7QUFhQSxTQWJBLHFCQWFBLENBRUEsQ0FmQTtBQWdCQTtBQWhCQSxHOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0tBO0FBRUE7QUFDQSx3QkFEQTtBQUVBO0FBQ0E7QUFDQSxpQkFEQTtBQUVBO0FBRkEsS0FEQTtBQUtBO0FBQ0EsaUJBREE7QUFFQTtBQUZBO0FBTEEsR0FGQTtBQVlBLGdCQVpBO0FBYUEsTUFiQSxrQkFhQTtBQUNBO0FBQ0E7QUFDQTtBQURBLE9BREE7QUFJQTtBQUNBO0FBREE7QUFKQTtBQVFBLEdBdEJBO0FBdUJBO0FBQ0EsZ0JBREEsMEJBQ0E7QUFDQSw0SEFDQTtBQUVBO0FBQ0E7QUFOQSxHQXZCQTtBQStCQSxTQS9CQSxxQkErQkEsQ0FDQSxDQWhDQTtBQWlDQTtBQUNBLFlBREEsc0JBQ0E7QUFDQTtBQUFBO0FBQUE7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBLEtBTEE7QUFNQSxVQU5BLGtCQU1BLEVBTkEsRUFNQTtBQUFBOztBQUNBLGVBQ0E7QUFFQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0EsT0FGQTtBQUdBO0FBZEE7QUFqQ0EsRzs7Ozs7Ozs7Ozs7O0FDdkNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxnQ0FBZ0M7QUFDaEM7QUFDQSwyQ0FBMkMsdUJBQXVCLGlCQUFpQixFQUFFLEVBQUU7QUFDdkY7QUFDQTtBQUNBO0FBQ0EsdURBQXVELCtCQUErQixFQUFFO0FBQ3hGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLGdDQUFnQztBQUNoQztBQUNBLDJDQUEyQyx5QkFBeUIsaUJBQWlCLEVBQUUsRUFBRTtBQUN6RjtBQUNBO0FBQ0E7QUFDQSwyREFBMkQsbUNBQW1DLEVBQUU7QUFDaEc7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsZ0NBQWdDO0FBQ2hDO0FBQ0EsMkNBQTJDLHlCQUF5QixpQkFBaUIsRUFBRSxFQUFFO0FBQ3pGO0FBQ0E7QUFDQTtBQUNBLDhEQUE4RCxzQ0FBc0MsRUFBRTtBQUN0RztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxnQ0FBZ0M7QUFDaEM7QUFDQSwyQ0FBMkMseUJBQXlCLGlCQUFpQixFQUFFLEVBQUU7QUFDekY7QUFDQTtBQUNBO0FBQ0EsNkRBQTZELHFDQUFxQyxFQUFFO0FBQ3BHO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLCtCQUErQjtBQUMvQjtBQUNBLDJDQUEyQywyQkFBMkIscUJBQXFCLEVBQUUsRUFBRTtBQUMvRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLDJDQUEyQyx5QkFBeUIsaUJBQWlCLEVBQUUsRUFBRTtBQUN6RjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFDQUFxQyx1QkFBdUI7QUFDNUQsb0NBQW9DLGtFQUFrRTtBQUN0RztBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CLGtCQUFrQjtBQUNyQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpREFBaUQsMkNBQTJDLEVBQUU7QUFDOUYsMENBQTBDLFVBQVUsRUFBRTtBQUN0RCxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLHNDQUFzQztBQUN0QztBQUNBLGdDQUFnQyx5QkFBeUIsNkJBQTZCLElBQUksUUFBUSxJQUFJLFFBQVEsSUFBSSxRQUFRLElBQUksaUNBQWlDLEdBQUc7QUFDbEs7QUFDQTtBQUNBO0FBQ0Esc0NBQXNDLHdCQUF3QixFQUFFO0FBQ2hFO0FBQ0E7QUFDQSwyQ0FBMkMsNkJBQTZCLEVBQUU7QUFDMUU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esa0NBQWtDLG1DQUFtQztBQUNyRTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQixZQUFZO0FBQy9CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLDJDQUEyQyxpQ0FBaUMsRUFBRTtBQUM5RTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsNENBQTRDLDhCQUE4QixFQUFFO0FBQzVFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSw0Q0FBNEMsOEJBQThCLEVBQUU7QUFDNUU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSwyQ0FBMkMsdUNBQXVDLEVBQUU7QUFDcEY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0NBQWdDLHNCQUFzQjtBQUN0RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMkNBQTJDLHlCQUF5QixpQkFBaUIsRUFBRSxFQUFFO0FBQ3pGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrREFBK0QseUJBQXlCLFdBQVcsRUFBRSxFQUFFO0FBQ3ZHO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSw0Q0FBNEMsOEJBQThCLEVBQUU7QUFDNUU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDJDQUEyQyx5QkFBeUIsaUJBQWlCLEVBQUUsRUFBRTtBQUN6RjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0RBQStELHlCQUF5QixXQUFXLEVBQUUsRUFBRTtBQUN2RztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLDJDQUEyQyx5QkFBeUIsZUFBZSxFQUFFLEVBQUU7QUFDdkY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsdUNBQXVDLG1CQUFtQjtBQUMxRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrQ0FBK0MscUNBQXFDLEVBQUU7QUFDdEY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CLGtCQUFrQjtBQUNyQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRThQOzs7Ozs7Ozs7Ozs7O0FDanJCOVA7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNzQjs7QUFFdEI7QUFDQTtBQUNBLCtEQUErRDtBQUMvRDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsZ0RBQWdELE9BQU87QUFDdkQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLG1DQUFtQyxNQUFNLDZCQUE2QixFQUFFLFlBQVksV0FBVyxFQUFFO0FBQ2pHLGtDQUFrQyxNQUFNLGlDQUFpQyxFQUFFLFlBQVksV0FBVyxFQUFFO0FBQ3BHLCtCQUErQixpRUFBaUUsdUJBQXVCLEVBQUUsNEJBQTRCO0FBQ3JKO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0EsYUFBYSw2QkFBNkIsMEJBQTBCLGFBQWEsRUFBRSxxQkFBcUI7QUFDeEcsZ0JBQWdCLHFEQUFxRCxvRUFBb0UsYUFBYSxFQUFFO0FBQ3hKLHNCQUFzQixzQkFBc0IscUJBQXFCLEdBQUc7QUFDcEU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdUNBQXVDO0FBQ3ZDLGtDQUFrQyxTQUFTO0FBQzNDLGtDQUFrQyxXQUFXLFVBQVU7QUFDdkQseUNBQXlDLGNBQWM7QUFDdkQ7QUFDQSw2R0FBNkcsT0FBTyxVQUFVO0FBQzlILGdGQUFnRixpQkFBaUIsT0FBTztBQUN4Ryx3REFBd0QsZ0JBQWdCLFFBQVEsT0FBTztBQUN2Riw4Q0FBOEMsZ0JBQWdCLGdCQUFnQixPQUFPO0FBQ3JGO0FBQ0EsaUNBQWlDO0FBQ2pDO0FBQ0E7QUFDQSxTQUFTLFlBQVksYUFBYSxPQUFPLEVBQUUsVUFBVSxXQUFXO0FBQ2hFLG1DQUFtQyxTQUFTO0FBQzVDO0FBQ0E7O0FBRUE7QUFDQSxpREFBaUQsUUFBUTtBQUN6RCx3Q0FBd0MsUUFBUTtBQUNoRCx3REFBd0QsUUFBUTtBQUNoRTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtCQUErQjtBQUMvQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUMsRUFBRTtBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdUJBQXVCLGdCQUFnQjtBQUN2QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsWUFBWTtBQUNaO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwwQkFBMEIsVUFBVTtBQUNwQywyQkFBMkIsVUFBVSxvQkFBb0I7QUFDekQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esd0JBQXdCLHVCQUF1QjtBQUMvQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLGtDQUFrQyxtQ0FBbUM7QUFDckU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0EsbUJBQW1CLFlBQVk7QUFDL0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQixZQUFZO0FBQy9CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUIsa0JBQWtCO0FBQ3JDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw4Q0FBOEMsZUFBZSxFQUFFO0FBQy9EO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrQkFBK0IsSUFBSSxLQUFLO0FBQ3hDLDJDQUEyQyxVQUFVO0FBQ3JELEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx3QkFBd0I7QUFDeEI7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdDQUFnQyx1Q0FBdUM7QUFDdkU7QUFDQTtBQUNBO0FBQ0E7QUFDQSx1QkFBdUIsWUFBWTtBQUNuQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esd0RBQXdELHlCQUF5QixFQUFFO0FBQ25GO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxRkFBcUYsa0NBQWtDLEVBQUU7QUFDekgsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBLDhFQUE4RSx1Q0FBdUMsRUFBRTtBQUN2SCxTQUFTO0FBQ1QsaUNBQWlDLG9CQUFvQixFQUFFO0FBQ3ZEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxzQkFBc0IsUUFBUTtBQUM5QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0JBQStCO0FBQy9CLDZCQUE2QixzQkFBc0I7QUFDbkQ7QUFDQSx3Q0FBd0M7QUFDeEM7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSw2QkFBNkIsY0FBYztBQUMzQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNkRBQTZELE1BQU07QUFDbkU7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxRUFBcUU7QUFDckUsK0RBQStEO0FBQy9EO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUJBQXlCO0FBQ3pCO0FBQ0EsU0FBUztBQUNULEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0NBQWdDO0FBQ2hDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw2QkFBNkI7QUFDN0I7QUFDQSw2RUFBNkUsMkNBQTJDLEVBQUU7QUFDMUg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUJBQXlCO0FBQ3pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQ0FBaUM7QUFDakM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCO0FBQ0EsU0FBUztBQUNULEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx5QkFBeUI7QUFDekI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUNBQWlDO0FBQ2pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw2QkFBNkI7QUFDN0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNkJBQTZCO0FBQzdCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx5QkFBeUI7QUFDekI7QUFDQSxTQUFTO0FBQ1QsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxREFBcUQsZUFBZSxLQUFLLHlEQUF5RDtBQUNsSTtBQUNBO0FBQ0Esd0NBQXdDO0FBQ3hDLDZCQUE2QjtBQUM3QjtBQUNBO0FBQ0Esa0NBQWtDLHdCQUF3QjtBQUMxRDtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1EQUFtRDtBQUNuRDtBQUNBLHlCQUF5QjtBQUN6QjtBQUNBLFNBQVM7QUFDVCxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVEQUF1RCxlQUFlLGVBQWUsS0FBSyx3REFBd0Q7QUFDbEo7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG9FQUFvRSx1QkFBdUIsRUFBRTtBQUM3RjtBQUNBO0FBQ0EsK0JBQStCLGtDQUFrQztBQUNqRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFEQUFxRCxZQUFZLGlCQUFpQjtBQUNsRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CLHlCQUF5QjtBQUM1QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSw4QkFBOEI7QUFDOUI7QUFDQSxDQUFDLEVBQUU7QUFDSCx3QkFBd0I7QUFDeEI7QUFDQSxDQUFDLEVBQUU7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMkJBQTJCO0FBQzNCO0FBQ0EsQ0FBQyxFQUFFO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLGFBQWE7QUFDNUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVHQUF1RyxZQUFZLGlCQUFpQjtBQUNwSTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFDQUFxQztBQUNyQztBQUNBLHNDQUFzQztBQUN0QztBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUVBQW1FO0FBQ25FO0FBQ0EsU0FBUztBQUNULEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNENBQTRDO0FBQzVDO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUNBQXFDO0FBQ3JDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFEQUFxRCwyQkFBMkIsRUFBRTtBQUNsRjtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdCQUFnQjtBQUNoQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0JBQWdCO0FBQ2hCO0FBQ0E7QUFDQSxnQkFBZ0I7QUFDaEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esa0RBQWtELGlCQUFpQjtBQUNuRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esa0RBQWtEO0FBQ2xEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0JBQStCLGVBQWUsK0ZBQStGLG9CQUFvQixFQUFFO0FBQ25LO0FBQ0EsNEJBQTRCLHVCQUF1QjtBQUNuRDtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxTQUFTLEVBQUU7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw0QkFBNEI7QUFDNUIseUJBQXlCLCtCQUErQjtBQUN4RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUJBQXlCLGtDQUFrQztBQUMzRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakIsYUFBYTtBQUNiLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsWUFBWTtBQUNaO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdUJBQXVCO0FBQ3ZCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx5QkFBeUIsMkNBQUc7QUFDNUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLGtDQUFrQywwQkFBMEI7QUFDNUQsU0FBUztBQUNUO0FBQ0E7QUFDQSxrQ0FBa0MsaUNBQWlDO0FBQ25FLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQSxhQUFhO0FBQ2IsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2IsU0FBUztBQUNUO0FBQ0EsNENBQTRDO0FBQzVDO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsNEJBQTRCLHVCQUF1QjtBQUNuRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQixhQUFhO0FBQ2IsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDJDQUEyQyxnQkFBZ0I7QUFDM0Qsd0RBQXdEO0FBQ3hEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw2QkFBNkI7QUFDN0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQ0FBaUM7QUFDakM7QUFDQTtBQUNBLDJDQUEyQyxpQkFBaUI7QUFDNUQsMkNBQTJDLDhDQUE4QztBQUN6RjtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCLGFBQWE7QUFDYixTQUFTO0FBQ1Q7QUFDQSw4QkFBOEIsZ0NBQWdDLEVBQUU7QUFDaEUsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBLDBDQUEwQyxvQkFBb0I7QUFDOUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYixTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdCQUFnQjtBQUNoQjtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtCQUErQixrQkFBa0I7QUFDakQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtDQUErQyxVQUFVLEVBQUU7QUFDM0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx5QkFBeUIsMkNBQUc7QUFDNUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMEJBQTBCO0FBQzFCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckIsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQSxvQ0FBb0MsdUJBQXVCO0FBQzNEO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQixvQ0FBb0Msc0JBQXNCO0FBQzFEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUJBQXlCO0FBQ3pCO0FBQ0EsYUFBYTtBQUNiO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLDhFQUE4RSxzQkFBc0I7QUFDcEcsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLGtDQUFrQyxtQkFBbUI7QUFDckQ7QUFDQTtBQUNBO0FBQ0E7QUFDQSw0Q0FBNEMsc0JBQXNCO0FBQ2xFLGtEQUFrRCw0QkFBNEI7QUFDOUU7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0Esa0NBQWtDLG1CQUFtQjtBQUNyRDtBQUNBO0FBQ0E7QUFDQTtBQUNBLDhEQUE4RCxvQkFBb0IsRUFBRTtBQUNwRjtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQSx3Q0FBd0M7QUFDeEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNEQUFzRCxvQkFBb0IsRUFBRTtBQUM1RSxxREFBcUQseUVBQXlFLGdCQUFnQixFQUFFLEVBQUUsRUFBRSx1Q0FBdUMsb0JBQW9CLEVBQUUsc0JBQXNCLHNCQUFzQixpQkFBaUIsRUFBRSxFQUFFO0FBQ2xSO0FBQ0E7QUFDQSw4RUFBOEUsVUFBVSxFQUFFO0FBQzFGO0FBQ0EsaUJBQWlCO0FBQ2pCLGFBQWE7QUFDYixTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2IsNkZBQTZGLG9CQUFvQixFQUFFO0FBQ25ILFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG9CQUFvQixJQUFxQztBQUN6RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQSxDQUFDOztBQUVEO0FBQ0EsOEJBQThCLHFCQUFxQjtBQUNuRDtBQUNBO0FBQ0E7QUFDQTtBQUNBLDBCQUEwQjtBQUMxQjtBQUNBLDZCQUE2QjtBQUM3Qiw0QkFBNEI7QUFDNUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1DQUFtQztBQUNuQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLHFEQUFxRCxnQkFBZ0I7QUFDckUsaURBQWlELHdCQUF3QjtBQUN6RTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7O0FBRUE7O0FBRStJOzs7Ozs7Ozs7Ozs7O0FDdjJEL0k7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSyw2Q0FBNkMsaUJBQWlCLEVBQUU7QUFDckU7QUFDQSxpQkFBaUIsc0NBQXNDLG1CQUFtQixFQUFFO0FBQzVFLG1CQUFtQiwrQkFBK0I7QUFDbEQscUJBQXFCLDRCQUE0QjtBQUNqRCx1QkFBdUIsdUNBQXVDO0FBQzlELHlCQUF5QixvREFBb0Q7QUFDN0UsMkJBQTJCLG1DQUFtQztBQUM5RCw2QkFBNkIsK0JBQStCO0FBQzVEO0FBQ0E7QUFDQSw4QkFBOEIsa0NBQWtDO0FBQ2hFO0FBQ0E7QUFDQTtBQUNBLDZCQUE2Qiw2QkFBNkI7QUFDMUQ7QUFDQTtBQUNBO0FBQ0EsK0JBQStCLDZCQUE2QjtBQUM1RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlCQUF5QjtBQUN6QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx3QkFBd0IscUNBQXFDO0FBQzdEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWU7QUFDZix3QkFBd0IsMEJBQTBCO0FBQ2xEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNCQUFzQiwwQ0FBMEM7QUFDaEU7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7OztBQzlFQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7OztBQ1BBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esb0JBQW9CLGlDQUFpQztBQUNyRCxlQUFlLDZCQUE2QjtBQUM1QyxnQkFBZ0IsOEJBQThCO0FBQzlDO0FBQ0EsaUJBQWlCLGtDQUFrQztBQUNuRCxtQkFBbUIsNENBQTRDO0FBQy9EO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNCQUFzQixZQUFZO0FBQ2xDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYixzQkFBc0IsK0JBQStCO0FBQ3JEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixnQ0FBZ0M7QUFDakQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPLHNCQUFzQjtBQUM3QjtBQUNBO0FBQ0E7QUFDQTtBQUNBLGtCQUFrQixhQUFhO0FBQy9CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQjtBQUNBO0FBQ0E7QUFDQSx1QkFBdUI7QUFDdkI7QUFDQTtBQUNBO0FBQ0EseUJBQXlCLG1EQUFtRDtBQUM1RTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDZCQUE2QjtBQUM3QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrREFBa0Q7QUFDbEQseUNBQXlDO0FBQ3pDO0FBQ0E7QUFDQTtBQUNBLGlEQUFpRCw0QkFBNEI7QUFDN0U7QUFDQTtBQUNBLHlDQUF5QztBQUN6QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMkNBQTJDO0FBQzNDO0FBQ0E7QUFDQTtBQUNBLDJDQUEyQztBQUMzQztBQUNBO0FBQ0EsMkNBQTJDO0FBQzNDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlDQUF5QztBQUN6QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlDQUF5QztBQUN6QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwyQkFBMkI7QUFDM0I7QUFDQSxxQ0FBcUMsMEJBQTBCO0FBQy9EO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrQkFBK0I7QUFDL0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0JBQStCO0FBQy9CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFDQUFxQztBQUNyQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBbUM7QUFDbkM7QUFDQTtBQUNBO0FBQ0E7QUFDQSw2QkFBNkI7QUFDN0I7QUFDQSx5Q0FBeUMsU0FBUyxtQkFBbUIsRUFBRTtBQUN2RTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFDQUFxQyx3QkFBd0I7QUFDN0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlDQUFpQztBQUNqQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrQkFBK0I7QUFDL0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdUNBQXVDO0FBQ3ZDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx3Q0FBd0MsNkJBQTZCO0FBQ3JFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtCQUErQjtBQUMvQjtBQUNBLHlDQUF5QyxxQ0FBcUM7QUFDOUU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlDQUFpQyxxQkFBcUI7QUFDdEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU8seUNBQXlDLG1CQUFtQixFQUFFO0FBQ3JFLGdCQUFnQiwrQkFBK0I7QUFDL0M7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSx1QkFBdUIsd0NBQXdDO0FBQy9ELGVBQWUscUNBQXFDO0FBQ3BEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7O0FDM1VBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esb0JBQW9CLGlDQUFpQztBQUNyRCxlQUFlLDZCQUE2QjtBQUM1QyxnQkFBZ0IsOEJBQThCO0FBQzlDO0FBQ0EsaUJBQWlCLGtDQUFrQztBQUNuRCxtQkFBbUIsNENBQTRDO0FBQy9EO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNCQUFzQixZQUFZO0FBQ2xDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYixzQkFBc0IsK0JBQStCO0FBQ3JEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixnQ0FBZ0M7QUFDakQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPLHNCQUFzQjtBQUM3QjtBQUNBO0FBQ0E7QUFDQTtBQUNBLGtCQUFrQixhQUFhO0FBQy9CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQjtBQUNBO0FBQ0E7QUFDQSx1QkFBdUI7QUFDdkI7QUFDQTtBQUNBO0FBQ0EseUJBQXlCLG1EQUFtRDtBQUM1RTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDZCQUE2QjtBQUM3QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrREFBa0Q7QUFDbEQseUNBQXlDO0FBQ3pDO0FBQ0E7QUFDQTtBQUNBLGlEQUFpRCw0QkFBNEI7QUFDN0U7QUFDQTtBQUNBLHlDQUF5QztBQUN6QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMkNBQTJDO0FBQzNDO0FBQ0E7QUFDQTtBQUNBLDJDQUEyQztBQUMzQztBQUNBO0FBQ0EsMkNBQTJDO0FBQzNDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlDQUF5QztBQUN6QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlDQUF5QztBQUN6QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwyQkFBMkI7QUFDM0I7QUFDQSxxQ0FBcUMsMEJBQTBCO0FBQy9EO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrQkFBK0I7QUFDL0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0JBQStCO0FBQy9CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFDQUFxQztBQUNyQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBbUM7QUFDbkM7QUFDQTtBQUNBO0FBQ0E7QUFDQSw2QkFBNkI7QUFDN0I7QUFDQSx5Q0FBeUMsU0FBUyxtQkFBbUIsRUFBRTtBQUN2RTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFDQUFxQyx3QkFBd0I7QUFDN0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlDQUFpQztBQUNqQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrQkFBK0I7QUFDL0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdUNBQXVDO0FBQ3ZDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx3Q0FBd0MsNkJBQTZCO0FBQ3JFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtCQUErQjtBQUMvQjtBQUNBLHlDQUF5QyxxQ0FBcUM7QUFDOUU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlDQUFpQyxxQkFBcUI7QUFDdEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU8seUNBQXlDLG1CQUFtQixFQUFFO0FBQ3JFLGdCQUFnQiwrQkFBK0I7QUFDL0M7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSx1QkFBdUIsd0NBQXdDO0FBQy9ELGVBQWUscUNBQXFDO0FBQ3BEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7O0FDM1VBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esb0JBQW9CLGlDQUFpQztBQUNyRCxlQUFlLDZCQUE2QjtBQUM1QyxnQkFBZ0IsOEJBQThCO0FBQzlDO0FBQ0EsaUJBQWlCLGtDQUFrQztBQUNuRCxtQkFBbUIsNENBQTRDO0FBQy9EO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNCQUFzQixZQUFZO0FBQ2xDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2Isc0JBQXNCLCtCQUErQjtBQUNyRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsZ0NBQWdDO0FBQ2pEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLHNCQUFzQjtBQUNyQyxpQkFBaUIsMkJBQTJCO0FBQzVDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTyx5Q0FBeUMsbUJBQW1CLEVBQUU7QUFDckUsZ0JBQWdCLCtCQUErQjtBQUMvQztBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVCQUF1Qix3Q0FBd0M7QUFDL0QsZUFBZSwrQkFBK0I7QUFDOUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7QUN6RUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSyx3Q0FBd0MsbUJBQW1CLEVBQUU7QUFDbEU7QUFDQTtBQUNBO0FBQ0EsU0FBUyxvREFBb0QsZUFBZSxFQUFFO0FBQzlFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDRCQUE0QjtBQUM1QixtQkFBbUI7QUFDbkI7QUFDQSxlQUFlO0FBQ2Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUIsOEJBQThCO0FBQ25EO0FBQ0E7QUFDQSxzQkFBc0I7QUFDdEIsYUFBYTtBQUNiO0FBQ0EseUJBQXlCLFNBQVMscUJBQXFCLEVBQUU7QUFDekQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7OztBQzdEQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQix5REFBeUQ7QUFDMUU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLHdEQUF3RDtBQUN6RTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGNBQWMsMENBQTBDO0FBQ3hEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVCxrQkFBa0IsMkJBQTJCO0FBQzdDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsMEJBQTBCO0FBQzNDLHFCQUFxQixTQUFTLHNDQUFzQyxFQUFFO0FBQ3RFO0FBQ0EscUJBQXFCLFNBQVMscUJBQXFCLEVBQUU7QUFDckQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7QUNqRkE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLE1BQU0sS0FBcUM7QUFDM0M7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHdFQUF3RTs7QUFFeEU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxLQUFLLDZCQUE2QjtBQUNsQztBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMENBQTBDO0FBQzFDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFVBQVUsSUFBcUM7QUFDL0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBLDBDQUEwQywyQ0FBMkM7QUFDckY7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsNkJBQTZCO0FBQzdCO0FBQ0EseUJBQXlCOztBQUV6Qjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0gsSUFBSSxLQUFxQztBQUN6QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBOztBQUVBO0FBQ0EsR0FBRyx1QkFBdUIscUJBQXFCLEVBQUU7QUFDakQ7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQSx1Q0FBdUM7QUFDdkM7QUFDQTtBQUNBO0FBQ0EsaUNBQWlDO0FBQ2pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esd0JBQXdCO0FBQ3hCLHNCQUFzQjs7QUFFdEI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxpQkFBaUIscUJBQXFCO0FBQ3RDO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsVUFBVTtBQUNWO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFlBQVksT0FBTztBQUNuQixZQUFZLFFBQVE7QUFDcEIsWUFBWTtBQUNaO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFlBQVksT0FBTztBQUNuQixZQUFZLFFBQVE7QUFDcEIsWUFBWTtBQUNaO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFlBQVk7QUFDWixZQUFZO0FBQ1o7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsWUFBWTtBQUNaLFlBQVk7QUFDWjtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxpQkFBaUIsbUJBQW1CO0FBQ3BDO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsbUJBQW1CLG1CQUFtQjtBQUN0Qzs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7O0FBRUEsdUJBQXVCLGtCQUFrQjtBQUN6Qzs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxZQUFZLE9BQU87QUFDbkIsWUFBWTtBQUNaO0FBQ0E7QUFDQSxtQ0FBbUM7QUFDbkM7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsWUFBWSxPQUFPO0FBQ25CLFlBQVk7QUFDWjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxZQUFZLFFBQVE7QUFDcEIsWUFBWSxNQUFNO0FBQ2xCLFlBQVk7QUFDWjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFlBQVksT0FBTztBQUNuQixZQUFZO0FBQ1o7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsWUFBWSxRQUFRO0FBQ3BCLFlBQVksT0FBTztBQUNuQixZQUFZO0FBQ1o7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxtQkFBbUIsbUJBQW1CO0FBQ3RDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsWUFBWSxPQUFPO0FBQ25CLFlBQVksTUFBTTtBQUNsQixZQUFZLFFBQVE7QUFDcEIsWUFBWTtBQUNaO0FBQ0E7QUFDQTs7QUFFQSxpQkFBaUIsaUJBQWlCO0FBQ2xDO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxZQUFZLE9BQU87QUFDbkIsWUFBWSxPQUFPO0FBQ25CLFlBQVksUUFBUTtBQUNwQixZQUFZO0FBQ1o7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsWUFBWSxPQUFPO0FBQ25CLFlBQVksZ0JBQWdCO0FBQzVCLFlBQVksUUFBUTtBQUNwQixZQUFZO0FBQ1o7QUFDQTtBQUNBO0FBQ0EseUJBQXlCLFFBQVE7QUFDakM7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxpQkFBaUIsbUJBQW1CO0FBQ3BDOztBQUVBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGNBQWMsNkRBQTZEO0FBQzNFO0FBQ0EsWUFBWSxzQkFBc0I7QUFDbEMsWUFBWSxnQkFBZ0I7QUFDNUIsWUFBWSxRQUFRO0FBQ3BCLFlBQVk7QUFDWjtBQUNBO0FBQ0E7QUFDQSx5QkFBeUIsUUFBUTtBQUNqQztBQUNBOztBQUVBOztBQUVBO0FBQ0EsMkNBQTJDLE9BQU87QUFDbEQ7O0FBRUE7QUFDQSxvQ0FBb0MsT0FBTyx1QkFBdUIsT0FBTztBQUN6RTs7QUFFQSxtQ0FBbUMsT0FBTyx1QkFBdUIsT0FBTztBQUN4RTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsNENBQTRDLDZCQUE2QjtBQUN6RSwyQkFBMkIsOEJBQThCOztBQUV6RCwyQkFBMkIsZUFBZTtBQUMxQyxHQUFHO0FBQ0gsUUFBUSxJQUFxQztBQUM3QztBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esd0NBQXdDLFlBQVk7QUFDcEQ7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNILG9CQUFvQjtBQUNwQjs7QUFFQTtBQUNBO0FBQ0Esb0JBQW9CO0FBQ3BCO0FBQ0EsaUNBQWlDO0FBQ2pDO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSyxVQUFVLElBQXFDO0FBQ3BEO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsY0FBYztBQUNkO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUCxLQUFLO0FBQ0w7QUFDQTs7QUFFQSxnQkFBZ0I7O0FBRWhCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87O0FBRVA7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQLFlBQVksSUFBcUM7QUFDakQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDJEQUEyRDtBQUMzRDtBQUNBOztBQUVBO0FBQ0E7QUFDQSxvQkFBb0I7QUFDcEIsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx1Q0FBdUM7QUFDdkM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBOztBQUVBLDhDQUE4QztBQUM5QztBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLHlEQUF5RDtBQUN6RDtBQUNBLDJCQUEyQjtBQUMzQjtBQUNBLGlEQUFpRDtBQUNqRDtBQUNBO0FBQ0E7QUFDQSxxQ0FBcUM7QUFDckM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CLHFCQUFxQjtBQUN4QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBLDBDQUEwQztBQUMxQzs7QUFFQTs7QUFFQSw0QkFBNEIsd0JBQXdCOztBQUVwRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQSwwQkFBMEI7QUFDMUIsR0FBRzs7QUFFSDtBQUNBLDBCQUEwQjtBQUMxQixHQUFHOztBQUVIO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0Esc0NBQXNDLE9BQU87QUFDN0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLE1BQU0sSUFBc0M7QUFDNUM7QUFDQTtBQUNBO0FBQ0EsK0JBQStCLGlFQUFpRSxFQUFFOztBQUVsRztBQUNBLGlEQUFpRCxzQkFBc0IsRUFBRTtBQUN6RTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsTUFBTSxJQUFxQztBQUMzQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EscUNBQXFDLDJCQUEyQjtBQUNoRSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDBCQUEwQjtBQUMxQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsUUFBUSxJQUFxQztBQUM3QztBQUNBO0FBQ0E7QUFDQSw4Q0FBOEMsaUNBQWlDLEVBQUU7QUFDakY7QUFDQTtBQUNBO0FBQ0E7QUFDQSx5REFBeUQ7QUFDekQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsbUJBQW1CLG9CQUFvQjtBQUN2QztBQUNBLFVBQVUsS0FBcUM7QUFDL0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUFLLFVBQVUsS0FBcUM7QUFDcEQ7QUFDQTtBQUNBO0FBQ0EsWUFBWSwwREFBMEQ7QUFDdEU7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE1BQU0sSUFBcUM7QUFDM0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxnQkFBZ0IsZ0NBQWdDO0FBQ2hELHdCQUF3QjtBQUN4Qix1QkFBdUI7QUFDdkI7QUFDQTs7QUFFQTs7OztBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxVQUFVLElBQXFDO0FBQy9DO0FBQ0E7QUFDQSxvQkFBb0I7QUFDcEI7QUFDQSxnQ0FBZ0Msc0JBQXNCLEVBQUU7QUFDeEQsNkJBQTZCLGlCQUFpQixFQUFFOztBQUVoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLHFCQUFxQixxQkFBcUI7QUFDMUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLGtCQUFrQjtBQUNsQjs7QUFFQTtBQUNBLFVBQVUsSUFBcUM7QUFDL0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxVQUFVLElBQXFDO0FBQy9DO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1AsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQLEtBQUs7QUFDTCxVQUFVLElBQXFDO0FBQy9DO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBOztBQUVBLGlDQUFpQyxTQUFTO0FBQzFDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtCQUErQixxQkFBcUI7QUFDcEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsTUFBTSxJQUFxQztBQUMzQztBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQSxjQUFjLElBQXFDO0FBQ25EO0FBQ0E7QUFDQSxTQUFTO0FBQ1QsS0FBSztBQUNMO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNEJBQTRCLHFCQUFxQjtBQUNqRCxLQUFLO0FBQ0wseUJBQXlCLGtDQUFrQztBQUMzRDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVCxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUzs7QUFFVDtBQUNBO0FBQ0EsVUFBVSxLQUFxQztBQUMvQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTOztBQUVUO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLOztBQUVMLG9CQUFvQixRQUFRO0FBQzVCO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlEQUF5RDtBQUN6RDtBQUNBO0FBQ0E7QUFDQSxNQUFNLEVBQUU7QUFDUixHQUFHO0FBQ0g7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLENBQUM7O0FBRUQ7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1QsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdDQUFnQyxzQkFBc0IsRUFBRTtBQUN4RDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdDQUFnQyxZQUFZO0FBQzVDO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUCxLQUFLO0FBQ0w7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSwrQkFBK0IsaUNBQWlDO0FBQ2hFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1gsU0FBUztBQUNUO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLFNBQVM7QUFDdEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxzQ0FBc0MsMENBQTBDLEVBQUU7QUFDbEY7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1AsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrQkFBa0I7O0FBRWxCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUssT0FBTyx3QkFBd0I7QUFDcEMsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0EsQ0FBQzs7QUFFRDs7OztBQUlBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxVQUFVLElBQXFDO0FBQy9DO0FBQ0E7QUFDQTtBQUNBOztBQUVBLDBCQUEwQixnQkFBZ0IscUJBQXFCOztBQUUvRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxFQUFFLEtBQXFDO0FBQ3ZDO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUIsOEJBQThCO0FBQ25EO0FBQ0E7QUFDQSw2QkFBNkIscUNBQXFDO0FBQ2xFLEdBQUc7O0FBRUg7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixtQkFBbUI7QUFDcEM7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFZSx3RUFBUyxFQUFDOzs7Ozs7Ozs7Ozs7QUNqMEZ6QjtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrRTs7Ozs7Ozs7Ozs7O0FDdEJBO0FBQWU7QUFDYkEsTUFBSSxFQUFFLElBRE87QUFFYkMsVUFBUSxFQUFFO0FBQ1JDLFlBQVEsRUFBRSxrQkFBVUMsQ0FBVixFQUFhO0FBQ3JCLGFBQU8saUJBQWlCQSxDQUFqQixHQUFxQixjQUE1QjtBQUNELEtBSE87QUFJUkMsU0FBSyxFQUFFLGVBQVVELENBQVYsRUFBYUUsQ0FBYixFQUFnQjtBQUNyQixhQUFPRixDQUFDLEdBQUcsc0JBQUosR0FBNkJFLENBQUMsQ0FBQ0MsTUFBL0IsR0FBd0MsR0FBL0M7QUFDRCxLQU5PO0FBT1JDLGNBQVUsRUFBRSxvQkFBVUosQ0FBVixFQUFhO0FBQ3ZCLGFBQU9BLENBQUMsR0FBRyxrRkFBWDtBQUNELEtBVE87QUFVUkssYUFBUyxFQUFFLG1CQUFVTCxDQUFWLEVBQWE7QUFDdEIsYUFBT0EsQ0FBQyxHQUFHLHVDQUFYO0FBQ0QsS0FaTztBQWFSTSxnQkFBWSxFQUFFLHNCQUFVTixDQUFWLEVBQWE7QUFDekIsYUFBT0EsQ0FBQyxHQUFHLDRDQUFYO0FBQ0QsS0FmTztBQWdCUk8sU0FBSyxFQUFFLGVBQVVQLENBQVYsRUFBYTtBQUNsQixhQUFPQSxDQUFDLEdBQUcsaUNBQVg7QUFDRCxLQWxCTztBQW1CUlEsVUFBTSxFQUFFLGdCQUFVUixDQUFWLEVBQWFFLENBQWIsRUFBZ0I7QUFDdEIsYUFBT0YsQ0FBQyxHQUFHLHdCQUFKLEdBQStCRSxDQUFDLENBQUNDLE1BQWpDLEdBQTBDLEdBQWpEO0FBQ0QsS0FyQk87QUFzQlJNLFdBQU8sRUFBRSxpQkFBVVQsQ0FBVixFQUFhRSxDQUFiLEVBQWdCO0FBQ3ZCLGFBQU9GLENBQUMsR0FBRyx5Q0FBSixHQUFnREUsQ0FBQyxDQUFDQyxNQUFsRCxHQUEyRCxNQUEzRCxHQUFvRUQsQ0FBQyxDQUFFLENBQUYsQ0FBckUsR0FBNkUsR0FBcEY7QUFDRCxLQXhCTztBQXlCUlEsYUFBUyxFQUFFLG1CQUFVVixDQUFWLEVBQWFFLENBQWIsRUFBZ0I7QUFDekIsYUFBT0YsQ0FBQyxHQUFHLFlBQUosR0FBbUJFLENBQUMsQ0FBQ0MsTUFBckIsR0FBOEIsR0FBckM7QUFDRCxLQTNCTztBQTRCUlEsZUFBVyxFQUFFLHFCQUFVWCxDQUFWLEVBQWE7QUFDeEIsYUFBTyxhQUFhQSxDQUFiLEdBQWlCLG1CQUF4QjtBQUNELEtBOUJPO0FBK0JSWSxnQkFBWSxFQUFFLHNCQUFVWixDQUFWLEVBQWFFLENBQWIsRUFBZ0I7QUFDNUIsYUFBT0YsQ0FBQyxHQUFHLDBDQUFKLEdBQWlERSxDQUFDLENBQUNDLE1BQW5ELEdBQTRELE1BQTVELEdBQXFFRCxDQUFDLENBQUUsQ0FBRixDQUF0RSxHQUE4RSxHQUFyRjtBQUNELEtBakNPO0FBa0NSVyxlQUFXLEVBQUUscUJBQVViLENBQVYsRUFBYUUsQ0FBYixFQUFnQjtBQUMzQixhQUFPRixDQUFDLEdBQUcsa0NBQUosR0FBeUNFLENBQUMsQ0FBQ0MsTUFBM0MsR0FBb0QsR0FBM0Q7QUFDRCxLQXBDTztBQXFDUlcsV0FBTyxFQUFFLGlCQUFVZCxDQUFWLEVBQWFFLENBQWIsRUFBZ0I7QUFDdkIsV0FBSyxDQUFMLEtBQVdBLENBQVgsS0FBaUJBLENBQUMsR0FBRyxFQUFyQjtBQUNBLFVBQUlhLENBQUMsR0FBR2IsQ0FBQyxDQUFDQyxNQUFWO0FBQ0EsYUFBTyxLQUFLLENBQUwsS0FBV1ksQ0FBWCxLQUFpQkEsQ0FBQyxHQUFHLEdBQXJCLEdBQTJCZixDQUFDLEdBQUcsaURBQUosSUFBeUQsUUFBUWUsQ0FBUixHQUFZLEVBQVosR0FBaUJBLENBQTFFLElBQStFLEdBQWpIO0FBQ0QsS0F6Q087QUEwQ1JDLFVBQU0sRUFBRSxnQkFBVWhCLENBQVYsRUFBYUUsQ0FBYixFQUFnQjtBQUN0QixhQUFPLFlBQVlGLENBQVosR0FBZ0IsOERBQWhCLEdBQWlGRSxDQUFDLENBQUNDLE1BQW5GLEdBQTRGLEdBQW5HO0FBQ0QsS0E1Q087QUE2Q1JjLGNBQVUsRUFBRSxvQkFBVWpCLENBQVYsRUFBYUUsQ0FBYixFQUFnQjtBQUMxQixhQUFPRixDQUFDLEdBQUcsc0JBQUosR0FBNkJFLENBQUMsQ0FBQ0MsTUFBL0IsR0FBd0MsdUJBQXhDLEdBQWtFRCxDQUFDLENBQUUsQ0FBRixDQUFuRSxHQUEyRSxVQUFsRjtBQUNELEtBL0NPO0FBZ0RSZ0IsU0FBSyxFQUFFLGVBQVVsQixDQUFWLEVBQWE7QUFDbEIsYUFBT0EsQ0FBQyxHQUFHLG9DQUFYO0FBQ0QsS0FsRE87QUFtRFJtQixPQUFHLEVBQUUsYUFBVW5CLENBQVYsRUFBYTtBQUNoQixhQUFPQSxDQUFDLEdBQUcsbUJBQVg7QUFDRCxLQXJETztBQXNEUm9CLFNBQUssRUFBRSxlQUFVcEIsQ0FBVixFQUFhO0FBQ2xCLGFBQU8sWUFBWUEsQ0FBWixHQUFnQixtQkFBdkI7QUFDRCxLQXhETztBQXlEUnFCLFlBQVEsRUFBRSxrQkFBVXJCLENBQVYsRUFBYTtBQUNyQixhQUFPQSxDQUFDLEdBQUcsdUJBQVg7QUFDRCxLQTNETztBQTREUnNCLE1BQUUsRUFBRSxZQUFVdEIsQ0FBVixFQUFhO0FBQ2YsYUFBT0EsQ0FBQyxHQUFHLGlDQUFYO0FBQ0QsS0E5RE87QUErRFJ1QixPQUFHLEVBQUUsYUFBVXZCLENBQVYsRUFBYUUsQ0FBYixFQUFnQjtBQUNuQixhQUFPRixDQUFDLEdBQUcsMEJBQUosR0FBaUNFLENBQUMsQ0FBQ0MsTUFBbkMsR0FBNEMsU0FBbkQ7QUFDRCxLQWpFTztBQWtFUnFCLGFBQVMsRUFBRSxtQkFBVXhCLENBQVYsRUFBYUUsQ0FBYixFQUFnQjtBQUN6QixhQUFPRixDQUFDLEdBQUcsMEJBQUosR0FBaUNFLENBQUMsQ0FBQ0MsTUFBbkMsR0FBNEMsR0FBbkQ7QUFDRCxLQXBFTztBQXFFUnNCLFNBQUssRUFBRSxlQUFVekIsQ0FBVixFQUFhO0FBQ2xCLGFBQU9BLENBQUMsR0FBRyw4QkFBWDtBQUNELEtBdkVPO0FBd0VSMEIsT0FBRyxFQUFFLGFBQVUxQixDQUFWLEVBQWFFLENBQWIsRUFBZ0I7QUFDbkIsYUFBT0YsQ0FBQyxHQUFHLHFCQUFKLEdBQTRCRSxDQUFDLENBQUNDLE1BQTlCLEdBQXVDLFNBQTlDO0FBQ0QsS0ExRU87QUEyRVJ3QixhQUFTLEVBQUUsbUJBQVUzQixDQUFWLEVBQWFFLENBQWIsRUFBZ0I7QUFDekIsYUFBT0YsQ0FBQyxHQUFHLDBCQUFKLEdBQWlDRSxDQUFDLENBQUNDLE1BQW5DLEdBQTRDLEdBQW5EO0FBQ0QsS0E3RU87QUE4RVJ5QixZQUFRLEVBQUUsa0JBQVU1QixDQUFWLEVBQWE7QUFDckIsYUFBT0EsQ0FBQyxHQUFHLGdDQUFYO0FBQ0QsS0FoRk87QUFpRlI2QixXQUFPLEVBQUUsaUJBQVU3QixDQUFWLEVBQWE7QUFDcEIsYUFBT0EsQ0FBQyxHQUFHLDhCQUFYO0FBQ0QsS0FuRk87QUFvRlI4QixTQUFLLEVBQUUsZUFBVTlCLENBQVYsRUFBYTtBQUNsQixhQUFPQSxDQUFDLEdBQUcsMkJBQVg7QUFDRCxLQXRGTztBQXVGUitCLFlBQVEsRUFBRSxrQkFBVS9CLENBQVYsRUFBYTtBQUNyQixhQUFPQSxDQUFDLEdBQUcsdUJBQVg7QUFDRCxLQXpGTztBQTBGUmdDLFFBQUksRUFBRSxjQUFVaEMsQ0FBVixFQUFhRSxDQUFiLEVBQWdCO0FBQ3BCLFVBQUlhLENBQUo7QUFBQSxVQUNJa0IsQ0FESjtBQUFBLFVBRUlDLENBRko7QUFBQSxVQUdJQyxDQUFDLEdBQUdqQyxDQUFDLENBQUNDLE1BSFY7QUFJQSxhQUNJSCxDQUFDLEdBQ0QsK0JBREEsSUFFRWUsQ0FBQyxHQUFHb0IsQ0FBTCxFQUNJRixDQUFDLEdBQUcsSUFEUixFQUVJQyxDQUFDLEdBQUcsTUFBTW5CLENBQUMsR0FBR3FCLE1BQU0sQ0FBQ3JCLENBQUQsQ0FBTixHQUFZa0IsQ0FBdEIsSUFBMkIsQ0FBM0IsR0FBK0JJLElBQUksQ0FBQ0MsS0FBTCxDQUFXRCxJQUFJLENBQUNFLEdBQUwsQ0FBU3hCLENBQVQsSUFBY3NCLElBQUksQ0FBQ0UsR0FBTCxDQUFTTixDQUFULENBQXpCLENBRnZDLEVBR0QsSUFBSSxDQUFDbEIsQ0FBQyxHQUFHc0IsSUFBSSxDQUFDRyxHQUFMLENBQVNQLENBQVQsRUFBWUMsQ0FBWixDQUFMLEVBQXFCTyxPQUFyQixDQUE2QixDQUE3QixDQUFKLEdBQXNDLEdBQXRDLEdBQTRDLENBQUUsTUFBRixFQUFVLElBQVYsRUFBZ0IsSUFBaEIsRUFBc0IsSUFBdEIsRUFBNEIsSUFBNUIsRUFBa0MsSUFBbEMsRUFBd0MsSUFBeEMsRUFBOEMsSUFBOUMsRUFBb0QsSUFBcEQsRUFBNERQLENBQTVELENBTDVDLElBTUEsR0FQSjtBQVNELEtBeEdPO0FBeUdSUSxPQUFHLEVBQUUsYUFBVTFDLENBQVYsRUFBYTtBQUNoQixhQUFPQSxDQUFDLEdBQUcsd0NBQVg7QUFDRDtBQTNHTztBQUZHLENBQWYsRTs7Ozs7Ozs7Ozs7O0FDQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBRUE7QUFFQTJDLDJDQUFHLENBQUNDLEdBQUosQ0FBUUMsNENBQVI7QUFDQUYsMkNBQUcsQ0FBQ0MsR0FBSixDQUFRRSxrREFBUjtBQUVBLElBQU1DLEtBQUssR0FBRyxJQUFJRiw0Q0FBSSxDQUFDRyxLQUFULENBQWVBLHlEQUFmLENBQWQ7QUFDQSxJQUFNQyxNQUFNLEdBQUcsSUFBSUgsa0RBQUosQ0FBYztBQUMzQkksTUFBSSxFQUFFLFNBRHFCO0FBRTNCQyxRQUFNLEVBQUUsQ0FDTjtBQUFFdEQsUUFBSSxFQUFFLFlBQVI7QUFBc0J1RCxRQUFJLEVBQUUsMkJBQTVCO0FBQXlEQyxhQUFTLEVBQUVDLDZFQUFnQkE7QUFBcEYsR0FETSxFQUVOO0FBQUV6RCxRQUFJLEVBQUUsVUFBUjtBQUFvQnVELFFBQUksRUFBRSx5QkFBMUI7QUFBcURDLGFBQVMsRUFBRUUsMkVBQWNBO0FBQTlFLEdBRk0sRUFHTjtBQUFFMUQsUUFBSSxFQUFFLFdBQVI7QUFBcUJ1RCxRQUFJLEVBQUUsOEJBQTNCO0FBQTJEQyxhQUFTLEVBQUVHLDRFQUFlQTtBQUFyRixHQUhNO0FBRm1CLENBQWQsQ0FBZjs7QUFTQSxJQUFNQyxNQUFNLEdBQUksWUFBTTtBQUNwQixNQUFNQyxnQkFBZ0IsR0FBRyxTQUFuQkEsZ0JBQW1CLEdBQU07QUFDN0IsUUFBSUMsR0FBRyxHQUFHQyw2Q0FBQyxDQUFDLGVBQUQsQ0FBWDtBQUVBLFFBQUlqQiwyQ0FBSixDQUFRO0FBQ05NLFlBQU0sRUFBTkEsTUFETTtBQUVORixXQUFLLEVBQUxBLEtBRk07QUFHTmMsWUFBTSxFQUFFLGdCQUFBMUIsQ0FBQztBQUFBLGVBQUlBLENBQUMsQ0FBQzJCLGdFQUFELENBQUw7QUFBQTtBQUhILEtBQVIsRUFJR0MsTUFKSCxDQUlVSixHQUFHLENBQUUsQ0FBRixDQUpiO0FBS0QsR0FSRDs7QUFVQSxTQUFPO0FBQ0xLLFFBREssa0JBQ0U7QUFDTE4sc0JBQWdCO0FBQ2pCO0FBSEksR0FBUDtBQUtELENBaEJjLEVBQWY7O0FBa0JBTyxRQUFRLENBQUNDLGdCQUFULENBQTBCLGtCQUExQixFQUE4QyxZQUFZO0FBQ3hEVCxRQUFNLENBQUNPLElBQVA7QUFDRCxDQUZELEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDN0NBO0FBRUE7QUFDQTtBQUNBO0FBRUEsSUFBSUcsVUFBVSxHQUFHO0FBQ2ZDLFNBQU8sRUFBRSxJQURNO0FBRWZDLFdBQVMsRUFBRSxJQUZJO0FBR2ZDLGVBQWEsRUFBRSxJQUhBO0FBSWZDLFlBQVUsRUFBRSxJQUpHO0FBS2ZDLFlBQVUsRUFBRSxDQUxHO0FBTWZDLFlBQVUsRUFBRTtBQU5HLENBQWpCOztJQVNNekIsSyxHQUNKLGlCQUFjO0FBQUE7O0FBQ1osU0FBTztBQUNMMEIsU0FBSyxFQUFFO0FBQ0xDLFVBQUksRUFBRTtBQUNKQyxhQUFLLEVBQUU7QUFESCxPQUREO0FBSUxDLG1CQUFhLEVBQUUsRUFKVjtBQUtMQyxxQkFBZSxvQkFBT1gsVUFBUDtBQUxWLEtBREY7QUFRTFksYUFBUyxFQUFFO0FBQ1RDLGlCQURTLHVCQUNHTixLQURILEVBQ1U7QUFDakJBLGFBQUssQ0FBQ0ksZUFBTixxQkFBNkJYLFVBQTdCO0FBQ0QsT0FIUTtBQUlUYyxnQkFKUyxzQkFJRVAsS0FKRixFQUlTUSxPQUpULEVBSWtCO0FBQ3pCUixhQUFLLENBQUNDLElBQU4sQ0FBWU8sT0FBTyxDQUFDUCxJQUFwQixJQUE2Qk8sT0FBTyxDQUFDQyxLQUFyQztBQUNELE9BTlE7QUFPVEMsaUJBUFMsdUJBT0dWLEtBUEgsRUFPVVEsT0FQVixFQU9tQjtBQUMxQlIsYUFBSyxDQUFDRyxhQUFOLEdBQXNCSyxPQUFPLENBQUNMLGFBQTlCO0FBQ0QsT0FUUTtBQVVUUSxlQVZTLHFCQVVDWCxLQVZELEVBVVFRLE9BVlIsRUFVaUI7QUFDeEIsWUFBSUksc0RBQVEsQ0FBQ0osT0FBTyxDQUFDSixlQUFULENBQVosRUFDRTs7QUFGc0IseUNBSU9JLE9BSlA7QUFBQSxZQUlsQkosZUFKa0IsWUFJbEJBLGVBSmtCOztBQU14QkEsdUJBQWUsQ0FBQ1MsUUFBaEIsR0FBMkIsSUFBM0I7QUFFQWIsYUFBSyxDQUFDSSxlQUFOLEdBQXdCQSxlQUF4QjtBQUNELE9BbkJRO0FBb0JUVSxrQkFwQlMsd0JBb0JJZCxLQXBCSixFQW9CV1EsT0FwQlgsRUFvQm9CO0FBQUEsMENBQ1dBLE9BRFg7QUFBQSxZQUNyQk8sSUFEcUIsYUFDckJBLElBRHFCO0FBQUEsWUFDZkMsS0FEZSxhQUNmQSxLQURlO0FBQUEsWUFDUkMsU0FEUSxhQUNSQSxTQURROztBQUczQixnQkFBUUYsSUFBUjtBQUNFLGVBQUssUUFBTDtBQUNFZixpQkFBSyxDQUFDRyxhQUFOLENBQW9CZSxJQUFwQixDQUF5QkQsU0FBekI7QUFDQTs7QUFDRixlQUFLLFFBQUw7QUFDRWpCLGlCQUFLLENBQUNHLGFBQU4sQ0FBcUJhLEtBQXJCLElBQStCQyxTQUEvQjtBQUNBakIsaUJBQUssQ0FBQ0ksZUFBTixHQUF3QmEsU0FBeEI7QUFFQTs7QUFDRixlQUFLLFFBQUw7QUFDRWpCLGlCQUFLLENBQUNHLGFBQU4sQ0FBb0JnQixNQUFwQixDQUEyQkgsS0FBM0IsRUFBa0MsQ0FBbEM7QUFDQTtBQVhKO0FBYUQ7QUFwQ1EsS0FSTjtBQThDTEksV0FBTyxFQUFFO0FBQ1BDLGtCQURPLDhCQUNrQmIsT0FEbEIsRUFDMkI7QUFBQSxZQUFuQmMsTUFBbUIsUUFBbkJBLE1BQW1CO0FBQ2hDLGVBQU8sSUFBSUMsT0FBSixDQUFZLFVBQVVDLE9BQVYsRUFBbUJDLE1BQW5CLEVBQTJCO0FBQzVDLGNBQUlDLFNBQVMscUJBQVFsQixPQUFSLENBQWI7O0FBQ0FtQixzRUFBQSxDQUFlRCxTQUFmLEVBQTBCRSxJQUExQixDQUErQixVQUFDQyxHQUFELEVBQVM7QUFDdEMsZ0JBQUlDLGlGQUFVLENBQUNELEdBQUQsQ0FBZCxFQUFxQjtBQUNuQkosb0JBQU0sQ0FBQ0ksR0FBRyxDQUFDckIsT0FBTCxDQUFOO0FBQ0E7QUFDRDs7QUFKcUMsaURBTVRxQixHQUFHLENBQUNyQixPQU5LO0FBQUEsZ0JBTWhDdUIsYUFOZ0MsZ0JBTWhDQSxhQU5nQzs7QUFRdENULGtCQUFNLENBQUMsV0FBRCxFQUFjO0FBQUVsQiw2QkFBZSxFQUFFMkI7QUFBbkIsYUFBZCxDQUFOO0FBRUFQLG1CQUFPLENBQUNLLEdBQUcsQ0FBQ3JCLE9BQUwsQ0FBUDtBQUNELFdBWEQ7QUFZRCxTQWRNLENBQVA7QUFlRCxPQWpCTTtBQWtCUHdCLG1CQWxCTyxnQ0FrQm1CO0FBQUEsWUFBVlYsTUFBVSxTQUFWQSxNQUFVO0FBQ3hCLGVBQU8sSUFBSUMsT0FBSixDQUFZLFVBQVVDLE9BQVYsRUFBbUJDLE1BQW5CLEVBQTJCO0FBQzVDLGNBQUlDLFNBQVMsR0FBRyxFQUFoQjtBQUNBQyx1RUFBQSxDQUFnQkQsU0FBaEIsRUFBMkJFLElBQTNCLENBQWdDLFVBQUNDLEdBQUQsRUFBUztBQUN2QyxnQkFBSUMsaUZBQVUsQ0FBQ0QsR0FBRCxDQUFkLEVBQXFCO0FBQ25CSixvQkFBTSxDQUFDSSxHQUFHLENBQUNyQixPQUFMLENBQU47QUFDQTtBQUNEOztBQUpzQyxrREFNWnFCLEdBQUcsQ0FBQ3JCLE9BTlE7QUFBQSxnQkFNakN5QixXQU5pQyxpQkFNakNBLFdBTmlDOztBQVF2Q1gsa0JBQU0sQ0FBQyxhQUFELEVBQWdCO0FBQUVuQiwyQkFBYSxFQUFFOEI7QUFBakIsYUFBaEIsQ0FBTjtBQUVBVCxtQkFBTyxDQUFDSyxHQUFHLENBQUNyQixPQUFMLENBQVA7QUFDRCxXQVhEO0FBWUQsU0FkTSxDQUFQO0FBZUQsT0FsQ007QUFtQ1AwQixtQkFuQ08sZ0NBbUNtQjFCLE9BbkNuQixFQW1DNEI7QUFBQSxZQUFuQmMsTUFBbUIsU0FBbkJBLE1BQW1CO0FBQ2pDLGVBQU8sSUFBSUMsT0FBSixDQUFZLFVBQVVDLE9BQVYsRUFBbUJDLE1BQW5CLEVBQTJCO0FBQzVDLGNBQUlVLFFBQVEsR0FBRyxJQUFJQyxRQUFKLEVBQWY7QUFFQUQsa0JBQVEsQ0FBQ0UsTUFBVCxDQUFnQixZQUFoQixFQUE4QjdCLE9BQU8sQ0FBQzhCLFVBQXRDO0FBQ0FILGtCQUFRLENBQUNFLE1BQVQsQ0FBZ0IsTUFBaEIsRUFBd0I3QixPQUFPLENBQUMrQixJQUFoQztBQUVBWixxRUFBQSxDQUFjUSxRQUFkLEVBQXdCO0FBQ3RCSyx1QkFBVyxFQUFFLEtBRFM7QUFFdEJDLHVCQUFXLEVBQUU7QUFGUyxXQUF4QixFQUdHYixJQUhILENBR1EsVUFBQUMsR0FBRyxFQUFJO0FBQ2IsZ0JBQUlDLGlGQUFVLENBQUNELEdBQUQsQ0FBZCxFQUFxQjtBQUNuQmEsaUdBQVksQ0FBQ2IsR0FBRyxDQUFDYyxPQUFMLENBQVo7QUFFQWxCLG9CQUFNLENBQUNJLEdBQUcsQ0FBQ3JCLE9BQUwsQ0FBTjtBQUNBO0FBQ0Q7O0FBRURnQixtQkFBTyxDQUFDSyxHQUFHLENBQUNyQixPQUFMLENBQVA7QUFDRCxXQVpEO0FBYUQsU0FuQk0sQ0FBUDtBQW9CRCxPQXhETTtBQXlEUG9DLGtCQXpETywrQkF5RDJCcEMsT0F6RDNCLEVBeURvQztBQUFBLFlBQTVCYyxNQUE0QixTQUE1QkEsTUFBNEI7QUFBQSxZQUFwQnVCLE9BQW9CLFNBQXBCQSxPQUFvQjtBQUN6QyxZQUFJLENBQUNyQyxPQUFPLENBQUNzQyxjQUFSLENBQXVCLE9BQXZCLENBQUwsRUFDRTs7QUFGdUMsMENBSXBCdEMsT0FKb0I7QUFBQSxZQUluQ1EsS0FKbUMsYUFJbkNBLEtBSm1DOztBQU16Q00sY0FBTSxDQUFDLFdBQUQsRUFBYztBQUFFbEIseUJBQWUsRUFBRXlDLE9BQU8sQ0FBQ0UsZ0JBQVIsQ0FBeUIvQixLQUF6QixFQUFpQyxDQUFqQztBQUFuQixTQUFkLENBQU47QUFDRCxPQWhFTTtBQWlFUGdDLHFCQWpFTyxrQ0FpRThCeEMsT0FqRTlCLEVBaUV1QztBQUFBLFlBQTVCYyxNQUE0QixTQUE1QkEsTUFBNEI7QUFBQSxZQUFwQnVCLE9BQW9CLFNBQXBCQSxPQUFvQjtBQUM1QyxlQUFPLElBQUl0QixPQUFKLENBQVksVUFBVUMsT0FBVixFQUFtQkMsTUFBbkIsRUFBMkI7QUFDNUMsY0FBSUMsU0FBUyxxQkFBUWxCLE9BQVIsQ0FBYjs7QUFDQW1CLHlFQUFBLENBQWtCRCxTQUFsQixFQUE2QkUsSUFBN0IsQ0FBa0MsVUFBQ0MsR0FBRCxFQUFTO0FBQ3pDYSwrRkFBWSxDQUFDYixHQUFHLENBQUNjLE9BQUwsQ0FBWjs7QUFDQSxnQkFBSWIsaUZBQVUsQ0FBQ0QsR0FBRCxDQUFkLEVBQXFCO0FBQ25CSixvQkFBTSxDQUFDSSxHQUFHLENBQUNyQixPQUFMLENBQU47QUFDQTtBQUNEOztBQUx3QyxrREFPWnFCLEdBQUcsQ0FBQ3JCLE9BUFE7QUFBQSxnQkFPbkN1QixhQVBtQyxpQkFPbkNBLGFBUG1DOztBQVN6Q1Qsa0JBQU0sQ0FBQyxjQUFELEVBQWlCO0FBQ3JCUCxrQkFBSSxFQUFFLFFBRGU7QUFFckJFLHVCQUFTLEVBQUVjO0FBRlUsYUFBakIsQ0FBTjtBQUtBUCxtQkFBTyxDQUFDSyxHQUFHLENBQUNyQixPQUFMLENBQVA7QUFDRCxXQWZEO0FBZ0JELFNBbEJNLENBQVA7QUFtQkQsT0FyRk07QUFzRlB5QyxxQkF0Rk8sa0NBc0Y4QnpDLE9BdEY5QixFQXNGdUM7QUFBQSxZQUE1QmMsTUFBNEIsU0FBNUJBLE1BQTRCO0FBQUEsWUFBcEJ1QixPQUFvQixTQUFwQkEsT0FBb0I7QUFDNUMsZUFBTyxJQUFJdEIsT0FBSixDQUFZLFVBQVVDLE9BQVYsRUFBbUJDLE1BQW5CLEVBQTJCO0FBQzVDLGNBQUlDLFNBQVMscUJBQVFsQixPQUFSLENBQWI7O0FBRUFtQix5RUFBQSxDQUFrQkQsU0FBbEIsRUFBNkJFLElBQTdCLENBQWtDLFVBQUNDLEdBQUQsRUFBUztBQUN6Q2EsK0ZBQVksQ0FBQ2IsR0FBRyxDQUFDYyxPQUFMLENBQVo7O0FBQ0EsZ0JBQUliLGlGQUFVLENBQUNELEdBQUQsQ0FBZCxFQUFxQjtBQUNuQkosb0JBQU0sQ0FBQ0ksR0FBRyxDQUFDckIsT0FBTCxDQUFOO0FBQ0E7QUFDRDs7QUFMd0Msa0RBT1pxQixHQUFHLENBQUNyQixPQVBRO0FBQUEsZ0JBT25DdUIsYUFQbUMsaUJBT25DQSxhQVBtQzs7QUFTekNULGtCQUFNLENBQUMsY0FBRCxFQUFpQjtBQUNyQlAsa0JBQUksRUFBRSxRQURlO0FBRXJCQyxtQkFBSyxFQUFFNkIsT0FBTyxDQUFDSyxZQUFSLENBQXFCbkIsYUFBYSxDQUFDckMsT0FBbkMsQ0FGYztBQUdyQnVCLHVCQUFTLEVBQUVjO0FBSFUsYUFBakIsQ0FBTjtBQU1BUCxtQkFBTyxDQUFDSyxHQUFHLENBQUNyQixPQUFMLENBQVA7QUFDRCxXQWhCRDtBQWlCRCxTQXBCTSxDQUFQO0FBcUJELE9BNUdNO0FBNkdQMkMscUJBN0dPLGtDQTZHOEIzQyxPQTdHOUIsRUE2R3VDO0FBQUEsWUFBNUJjLE1BQTRCLFNBQTVCQSxNQUE0QjtBQUFBLFlBQXBCdUIsT0FBb0IsU0FBcEJBLE9BQW9CO0FBQzVDLGVBQU8sSUFBSXRCLE9BQUosQ0FBWSxVQUFVQyxPQUFWLEVBQW1CQyxNQUFuQixFQUEyQjtBQUM1QyxjQUFJQyxTQUFTLHFCQUFRbEIsT0FBUixDQUFiOztBQUVBbUIseUVBQUEsQ0FBa0JELFNBQWxCLEVBQTZCRSxJQUE3QixDQUFrQyxVQUFBQyxHQUFHLEVBQUk7QUFDdkNhLCtGQUFZLENBQUNiLEdBQUcsQ0FBQ2MsT0FBTCxDQUFaOztBQUNBLGdCQUFJYixpRkFBVSxDQUFDRCxHQUFELENBQWQsRUFBcUI7QUFDbkJKLG9CQUFNLENBQUNJLEdBQUcsQ0FBQ3JCLE9BQUwsQ0FBTjtBQUVBO0FBQ0Q7O0FBTnNDLGtEQVFyQnFCLEdBQUcsQ0FBQ3JCLE9BUmlCO0FBQUEsZ0JBUWpDNEMsRUFSaUMsaUJBUWpDQSxFQVJpQzs7QUFVdkM5QixrQkFBTSxDQUFDLGNBQUQsRUFBaUI7QUFDckJQLGtCQUFJLEVBQUUsUUFEZTtBQUVyQkMsbUJBQUssRUFBRTZCLE9BQU8sQ0FBQ0ssWUFBUixDQUFxQkUsRUFBckI7QUFGYyxhQUFqQixDQUFOO0FBS0E1QixtQkFBTyxDQUFDSyxHQUFHLENBQUNyQixPQUFMLENBQVA7QUFDRCxXQWhCRDtBQWlCRCxTQXBCTSxDQUFQO0FBcUJEO0FBbklNLEtBOUNKO0FBbUxMcUMsV0FBTyxFQUFFO0FBQ1BFLHNCQUFnQixFQUFFLDBCQUFBL0MsS0FBSztBQUFBLGVBQUksVUFBQWdCLEtBQUssRUFBSTtBQUNsQyxpQkFBT2hCLEtBQUssQ0FBQ0csYUFBTixDQUFvQmtELE1BQXBCLENBQTJCLFVBQUNDLFNBQUQsRUFBWUMsWUFBWjtBQUFBLG1CQUE2QkEsWUFBWSxJQUFJdkMsS0FBN0M7QUFBQSxXQUEzQixDQUFQO0FBQ0QsU0FGc0I7QUFBQSxPQURoQjtBQUlQa0Msa0JBQVksRUFBRSxzQkFBQWxELEtBQUs7QUFBQSxlQUFJLFVBQUFvRCxFQUFFLEVBQUk7QUFDM0IsY0FBSXBDLEtBQUssR0FBRyxDQUFDLENBQWI7O0FBQ0F3Qyw4REFBTSxDQUFDeEQsS0FBSyxDQUFDRyxhQUFQLEVBQXNCLFVBQUNtRCxTQUFELEVBQVlDLFlBQVosRUFBNkI7QUFDdkQsZ0JBQUlELFNBQVMsQ0FBQzVELE9BQVYsSUFBcUIwRCxFQUF6QixFQUNFO0FBRUZwQyxpQkFBSyxHQUFHdUMsWUFBUjtBQUNBLG1CQUFPLEtBQVA7QUFDRCxXQU5LLENBQU47O0FBUUEsaUJBQU92QyxLQUFQO0FBQ0QsU0FYa0I7QUFBQTtBQUpaO0FBbkxKLEdBQVA7QUFxTUQsQzs7QUFHYSxtRUFBSTFDLEtBQUosRUFBaEIsRTs7Ozs7Ozs7Ozs7O0FDek5BO0FBQUE7QUFBQTtBQUFBO0FBQWlHO0FBQzNCO0FBQ0w7OztBQUdqRTtBQUNnRztBQUNoRyxnQkFBZ0IsMkdBQVU7QUFDMUIsRUFBRSx3RkFBTTtBQUNSLEVBQUUsNkZBQU07QUFDUixFQUFFLHNHQUFlO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0EsSUFBSSxLQUFVLEVBQUUsWUFpQmY7QUFDRDtBQUNlLGdGOzs7Ozs7Ozs7Ozs7QUN0Q2Y7QUFBQTtBQUFBLHdDQUFzTSxDQUFnQiw0UEFBRyxFQUFDLEM7Ozs7Ozs7Ozs7OztBQ0ExTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7Ozs7Ozs7Ozs7QUNBQTtBQUFBO0FBQUE7QUFBQTtBQUFrRjtBQUMzQjtBQUNMOzs7QUFHbEQ7QUFDZ0c7QUFDaEcsZ0JBQWdCLDJHQUFVO0FBQzFCLEVBQUUseUVBQU07QUFDUixFQUFFLDhFQUFNO0FBQ1IsRUFBRSx1RkFBZTtBQUNqQjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBLElBQUksS0FBVSxFQUFFLFlBaUJmO0FBQ0Q7QUFDZSxnRjs7Ozs7Ozs7Ozs7O0FDdENmO0FBQUE7QUFBQSx3Q0FBdUwsQ0FBZ0IsNk9BQUcsRUFBQyxDOzs7Ozs7Ozs7Ozs7QUNBM007QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOzs7Ozs7Ozs7Ozs7O0FDQUE7QUFBQTtBQUFBO0FBQUE7QUFBNkY7QUFDM0I7QUFDTDs7O0FBRzdEO0FBQ2dHO0FBQ2hHLGdCQUFnQiwyR0FBVTtBQUMxQixFQUFFLG9GQUFNO0FBQ1IsRUFBRSx5RkFBTTtBQUNSLEVBQUUsa0dBQWU7QUFDakI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQSxJQUFJLEtBQVUsRUFBRSxZQWlCZjtBQUNEO0FBQ2UsZ0Y7Ozs7Ozs7Ozs7OztBQ3RDZjtBQUFBO0FBQUEsd0NBQWtNLENBQWdCLHdQQUFHLEVBQUMsQzs7Ozs7Ozs7Ozs7O0FDQXROO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7Ozs7Ozs7Ozs7OztBQ0FBO0FBQUE7QUFBQTtBQUFBO0FBQThGO0FBQzNCO0FBQ0w7OztBQUc5RDtBQUNnRztBQUNoRyxnQkFBZ0IsMkdBQVU7QUFDMUIsRUFBRSxxRkFBTTtBQUNSLEVBQUUsMEZBQU07QUFDUixFQUFFLG1HQUFlO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0EsSUFBSSxLQUFVLEVBQUUsWUFpQmY7QUFDRDtBQUNlLGdGOzs7Ozs7Ozs7Ozs7QUN0Q2Y7QUFBQTtBQUFBLHdDQUFtTSxDQUFnQix5UEFBRyxFQUFDLEM7Ozs7Ozs7Ozs7OztBQ0F2TjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7Ozs7Ozs7Ozs7QUNBQTtBQUFBO0FBQUE7QUFBQTtBQUErRjtBQUMzQjtBQUNMOzs7QUFHL0Q7QUFDZ0c7QUFDaEcsZ0JBQWdCLDJHQUFVO0FBQzFCLEVBQUUsc0ZBQU07QUFDUixFQUFFLDJGQUFNO0FBQ1IsRUFBRSxvR0FBZTtBQUNqQjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBLElBQUksS0FBVSxFQUFFLFlBaUJmO0FBQ0Q7QUFDZSxnRjs7Ozs7Ozs7Ozs7O0FDdENmO0FBQUE7QUFBQSx3Q0FBb00sQ0FBZ0IsMFBBQUcsRUFBQyxDOzs7Ozs7Ozs7Ozs7QUNBeE47QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOzs7Ozs7Ozs7Ozs7O0FDQUE7QUFBQTtBQUFBO0FBQUE7QUFBd0Y7QUFDM0I7QUFDTDs7O0FBR3hEO0FBQ2dHO0FBQ2hHLGdCQUFnQiwyR0FBVTtBQUMxQixFQUFFLCtFQUFNO0FBQ1IsRUFBRSxvRkFBTTtBQUNSLEVBQUUsNkZBQWU7QUFDakI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQSxJQUFJLEtBQVUsRUFBRSxZQWlCZjtBQUNEO0FBQ2UsZ0Y7Ozs7Ozs7Ozs7OztBQ3RDZjtBQUFBO0FBQUEsd0NBQTZMLENBQWdCLG1QQUFHLEVBQUMsQzs7Ozs7Ozs7Ozs7O0FDQWpOO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7Ozs7Ozs7Ozs7OztBQ0FBO0FBQUE7QUFBQTtBQUFBO0FBQTJGO0FBQzNCO0FBQ0w7OztBQUczRDtBQUNnRztBQUNoRyxnQkFBZ0IsMkdBQVU7QUFDMUIsRUFBRSxrRkFBTTtBQUNSLEVBQUUsdUZBQU07QUFDUixFQUFFLGdHQUFlO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0EsSUFBSSxLQUFVLEVBQUUsWUFpQmY7QUFDRDtBQUNlLGdGOzs7Ozs7Ozs7Ozs7QUN0Q2Y7QUFBQTtBQUFBLHdDQUFnTSxDQUFnQixzUEFBRyxFQUFDLEM7Ozs7Ozs7Ozs7OztBQ0FwTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEiLCJmaWxlIjoianMvcm9sZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIGluc3RhbGwgYSBKU09OUCBjYWxsYmFjayBmb3IgY2h1bmsgbG9hZGluZ1xuIFx0ZnVuY3Rpb24gd2VicGFja0pzb25wQ2FsbGJhY2soZGF0YSkge1xuIFx0XHR2YXIgY2h1bmtJZHMgPSBkYXRhWzBdO1xuIFx0XHR2YXIgbW9yZU1vZHVsZXMgPSBkYXRhWzFdO1xuIFx0XHR2YXIgZXhlY3V0ZU1vZHVsZXMgPSBkYXRhWzJdO1xuXG4gXHRcdC8vIGFkZCBcIm1vcmVNb2R1bGVzXCIgdG8gdGhlIG1vZHVsZXMgb2JqZWN0LFxuIFx0XHQvLyB0aGVuIGZsYWcgYWxsIFwiY2h1bmtJZHNcIiBhcyBsb2FkZWQgYW5kIGZpcmUgY2FsbGJhY2tcbiBcdFx0dmFyIG1vZHVsZUlkLCBjaHVua0lkLCBpID0gMCwgcmVzb2x2ZXMgPSBbXTtcbiBcdFx0Zm9yKDtpIDwgY2h1bmtJZHMubGVuZ3RoOyBpKyspIHtcbiBcdFx0XHRjaHVua0lkID0gY2h1bmtJZHNbaV07XG4gXHRcdFx0aWYoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKGluc3RhbGxlZENodW5rcywgY2h1bmtJZCkgJiYgaW5zdGFsbGVkQ2h1bmtzW2NodW5rSWRdKSB7XG4gXHRcdFx0XHRyZXNvbHZlcy5wdXNoKGluc3RhbGxlZENodW5rc1tjaHVua0lkXVswXSk7XG4gXHRcdFx0fVxuIFx0XHRcdGluc3RhbGxlZENodW5rc1tjaHVua0lkXSA9IDA7XG4gXHRcdH1cbiBcdFx0Zm9yKG1vZHVsZUlkIGluIG1vcmVNb2R1bGVzKSB7XG4gXHRcdFx0aWYoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG1vcmVNb2R1bGVzLCBtb2R1bGVJZCkpIHtcbiBcdFx0XHRcdG1vZHVsZXNbbW9kdWxlSWRdID0gbW9yZU1vZHVsZXNbbW9kdWxlSWRdO1xuIFx0XHRcdH1cbiBcdFx0fVxuIFx0XHRpZihwYXJlbnRKc29ucEZ1bmN0aW9uKSBwYXJlbnRKc29ucEZ1bmN0aW9uKGRhdGEpO1xuXG4gXHRcdHdoaWxlKHJlc29sdmVzLmxlbmd0aCkge1xuIFx0XHRcdHJlc29sdmVzLnNoaWZ0KCkoKTtcbiBcdFx0fVxuXG4gXHRcdC8vIGFkZCBlbnRyeSBtb2R1bGVzIGZyb20gbG9hZGVkIGNodW5rIHRvIGRlZmVycmVkIGxpc3RcbiBcdFx0ZGVmZXJyZWRNb2R1bGVzLnB1c2guYXBwbHkoZGVmZXJyZWRNb2R1bGVzLCBleGVjdXRlTW9kdWxlcyB8fCBbXSk7XG5cbiBcdFx0Ly8gcnVuIGRlZmVycmVkIG1vZHVsZXMgd2hlbiBhbGwgY2h1bmtzIHJlYWR5XG4gXHRcdHJldHVybiBjaGVja0RlZmVycmVkTW9kdWxlcygpO1xuIFx0fTtcbiBcdGZ1bmN0aW9uIGNoZWNrRGVmZXJyZWRNb2R1bGVzKCkge1xuIFx0XHR2YXIgcmVzdWx0O1xuIFx0XHRmb3IodmFyIGkgPSAwOyBpIDwgZGVmZXJyZWRNb2R1bGVzLmxlbmd0aDsgaSsrKSB7XG4gXHRcdFx0dmFyIGRlZmVycmVkTW9kdWxlID0gZGVmZXJyZWRNb2R1bGVzW2ldO1xuIFx0XHRcdHZhciBmdWxmaWxsZWQgPSB0cnVlO1xuIFx0XHRcdGZvcih2YXIgaiA9IDE7IGogPCBkZWZlcnJlZE1vZHVsZS5sZW5ndGg7IGorKykge1xuIFx0XHRcdFx0dmFyIGRlcElkID0gZGVmZXJyZWRNb2R1bGVbal07XG4gXHRcdFx0XHRpZihpbnN0YWxsZWRDaHVua3NbZGVwSWRdICE9PSAwKSBmdWxmaWxsZWQgPSBmYWxzZTtcbiBcdFx0XHR9XG4gXHRcdFx0aWYoZnVsZmlsbGVkKSB7XG4gXHRcdFx0XHRkZWZlcnJlZE1vZHVsZXMuc3BsaWNlKGktLSwgMSk7XG4gXHRcdFx0XHRyZXN1bHQgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IGRlZmVycmVkTW9kdWxlWzBdKTtcbiBcdFx0XHR9XG4gXHRcdH1cblxuIFx0XHRyZXR1cm4gcmVzdWx0O1xuIFx0fVxuXG4gXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBvYmplY3QgdG8gc3RvcmUgbG9hZGVkIGFuZCBsb2FkaW5nIGNodW5rc1xuIFx0Ly8gdW5kZWZpbmVkID0gY2h1bmsgbm90IGxvYWRlZCwgbnVsbCA9IGNodW5rIHByZWxvYWRlZC9wcmVmZXRjaGVkXG4gXHQvLyBQcm9taXNlID0gY2h1bmsgbG9hZGluZywgMCA9IGNodW5rIGxvYWRlZFxuIFx0dmFyIGluc3RhbGxlZENodW5rcyA9IHtcbiBcdFx0XCJyb2xlXCI6IDBcbiBcdH07XG5cbiBcdHZhciBkZWZlcnJlZE1vZHVsZXMgPSBbXTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuIFx0dmFyIGpzb25wQXJyYXkgPSB3aW5kb3dbXCJ3ZWJwYWNrSnNvbnBcIl0gPSB3aW5kb3dbXCJ3ZWJwYWNrSnNvbnBcIl0gfHwgW107XG4gXHR2YXIgb2xkSnNvbnBGdW5jdGlvbiA9IGpzb25wQXJyYXkucHVzaC5iaW5kKGpzb25wQXJyYXkpO1xuIFx0anNvbnBBcnJheS5wdXNoID0gd2VicGFja0pzb25wQ2FsbGJhY2s7XG4gXHRqc29ucEFycmF5ID0ganNvbnBBcnJheS5zbGljZSgpO1xuIFx0Zm9yKHZhciBpID0gMDsgaSA8IGpzb25wQXJyYXkubGVuZ3RoOyBpKyspIHdlYnBhY2tKc29ucENhbGxiYWNrKGpzb25wQXJyYXlbaV0pO1xuIFx0dmFyIHBhcmVudEpzb25wRnVuY3Rpb24gPSBvbGRKc29ucEZ1bmN0aW9uO1xuXG5cbiBcdC8vIGFkZCBlbnRyeSBtb2R1bGUgdG8gZGVmZXJyZWQgbGlzdFxuIFx0ZGVmZXJyZWRNb2R1bGVzLnB1c2goWzYsXCJ2ZW5kb3JcIl0pO1xuIFx0Ly8gcnVuIGRlZmVycmVkIG1vZHVsZXMgd2hlbiByZWFkeVxuIFx0cmV0dXJuIGNoZWNrRGVmZXJyZWRNb2R1bGVzKCk7XG4iLCI8dGVtcGxhdGU+XHJcbiAgPGRpdiBjbGFzcz1cIm1vZGFsIGZhZGUgbW9kYWwtZnNcIiBpZD1cIm1vZGFsLWZzXCI+XHJcbiAgICA8ZGl2IGNsYXNzPVwibW9kYWwtZGlhbG9nXCIgcm9sZT1cImRvY3VtZW50XCI+XHJcbiAgICAgIDxkaXYgY2xhc3M9XCJtb2RhbC1jb250ZW50XCI+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cIm1vZGFsLWJvZHlcIj5cclxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb250YWluZXIgdGV4dC1jZW50ZXJcIj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cInJvdyBmdWxsLWhlaWdodCBhbGlnbi1pdGVtcy1jZW50ZXJcIj5cclxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTUgbS1oLWF1dG9cIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJwLWgtMzAgcC1iLTUwXCI+XHJcbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJ0ZXh0LWNlbnRlciBmb250LXNpemUtNzBcIj5cclxuICAgICAgICAgICAgICAgICAgICA8aSBjbGFzcz1cIm1kaSBtZGktY2hlY2tib3gtbWFya2VkLWNpcmNsZS1vdXRsaW5lIGljb24tZ3JhZGllbnQtc3VjY2Vzc1wiPjwvaT5cclxuICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgIDxoMSBjbGFzcz1cInRleHQtdGhpbiBtLXYtMTVcIj57e3RpdGxlfX08L2gxPlxyXG4gICAgICAgICAgICAgICAgICA8cCBjbGFzcz1cIm0tYi0yNSBsZWFkXCI+e3tjb250ZW50fX08L3A+XHJcbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJ0ZXh0LWNlbnRlclwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxidXR0b25cclxuICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3M9XCJidG4gYnRuLWRlZmF1bHQgYnRuLWxnXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgQGNsaWNrLnByZXZlbnQ9XCJjb25maXJtXCJcclxuICAgICAgICAgICAgICAgICAgICA+Q29uZmlybVxyXG4gICAgICAgICAgICAgICAgICAgIDwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgPGFcclxuICAgICAgICAgICAgICBjbGFzcz1cIm1vZGFsLWNsb3NlXCJcclxuICAgICAgICAgICAgICBocmVmPVwiI1wiXHJcbiAgICAgICAgICAgICAgZGF0YS1kaXNtaXNzPVwibW9kYWxcIlxyXG4gICAgICAgICAgICAgIEBjbGljay5wcmV2ZW50PVwiY29uZmlybVwiXHJcbiAgICAgICAgICA+XHJcbiAgICAgICAgICAgIDxpIGNsYXNzPVwidGktY2xvc2VcIj48L2k+XHJcbiAgICAgICAgICA8L2E+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgPC9kaXY+XHJcbiAgPC9kaXY+XHJcbjwvdGVtcGxhdGU+XHJcblxyXG48c2NyaXB0PlxyXG4gIGltcG9ydCB7IEVWRU5UX0JVU19LRVkgfSBmcm9tICckanNCYXNlUGF0aC9kZWZpbmUnO1xyXG4gIGltcG9ydCBFdmVudEJ1cyBmcm9tICckanNCYXNlUGF0aC9FdmVudEJ1cyc7XHJcblxyXG4gIGV4cG9ydCBkZWZhdWx0IHtcclxuICAgIG5hbWU6ICdtb2RhbCcsXHJcbiAgICBjb21wb25lbnRzOiB7fSxcclxuICAgIGRhdGEoKSB7XHJcbiAgICAgIHJldHVybiB7XHJcbiAgICAgICAgdGl0bGU6ICdUSMOUTkcgQsOBTyAhJyxcclxuICAgICAgICBjb250ZW50OiAnV2UgYXJlIGx1Y2t5IHRvIGxpdmUgaW4gYSBnbG9yaW91cyBhZ2UgdGhhdCBnaXZlcyB1cyBldmVyeXRoaW5nIGFzIGEgaHVtYW4gcmFjZS4nXHJcbiAgICAgIH1cclxuICAgIH0sXHJcbiAgICBjb21wdXRlZDoge30sXHJcbiAgICBjcmVhdGVkKCkge1xyXG4gICAgICBsZXQgc2VsZiA9IHRoaXNcclxuICAgICAgRXZlbnRCdXMub24oRVZFTlRfQlVTX0tFWS5VUERBVEVfQ09OVEVOVF9NT0RBTCwgKGNvbnRlbnQpID0+IHtcclxuICAgICAgICBzZWxmLmNvbnRlbnQgPSBjb250ZW50XHJcbiAgICAgIH0pXHJcblxyXG4gICAgICAkKGRvY3VtZW50KS5vbigna2V5dXAnLCAoZSkgPT4ge1xyXG4gICAgICAgIGxldCBrZXlDb2RlID0gZS53aGljaFxyXG5cclxuICAgICAgICBpZiAoa2V5Q29kZSAhPSAxMyAmJiBrZXlDb2RlICE9IDI3KVxyXG4gICAgICAgICAgcmV0dXJuXHJcblxyXG4gICAgICAgIGxldCBmbGFnID0gJCgnI21vZGFsLWZzJykuaGFzQ2xhc3MoJ3Nob3cnKVxyXG4gICAgICAgIGlmICghZmxhZylcclxuICAgICAgICAgIHJldHVyblxyXG5cclxuICAgICAgICAkKCcjbW9kYWwtZnMtYnRuJykudHJpZ2dlcignY2xpY2snKVxyXG4gICAgICAgIEV2ZW50QnVzLmVtaXQoRVZFTlRfQlVTX0tFWS5DT05GSVJNX01PREFMLCB0cnVlKVxyXG4gICAgICB9KVxyXG4gICAgfSxcclxuICAgIG1ldGhvZHM6IHtcclxuICAgICAgY29uZmlybSgpIHtcclxuICAgICAgICAkKCcjbW9kYWwtZnMtYnRuJykudHJpZ2dlcignY2xpY2snKVxyXG4gICAgICAgIEV2ZW50QnVzLmVtaXQoRVZFTlRfQlVTX0tFWS5DT05GSVJNX01PREFMLCB0cnVlKVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG48L3NjcmlwdD5cclxuXHJcbiIsIjx0ZW1wbGF0ZT5cclxuICA8cm91dGVyLXZpZXc+PC9yb3V0ZXItdmlldz5cclxuPC90ZW1wbGF0ZT5cclxuXHJcbjxzY3JpcHQ+XHJcbiAgZXhwb3J0IGRlZmF1bHQge1xyXG4gICAgbmFtZTogJ3JvbGUtYXBwJyxcclxuICAgIGNvbXBvbmVudHM6IHt9LFxyXG4gICAgZGF0YSgpIHtcclxuICAgICAgcmV0dXJuIHt9XHJcbiAgICB9LFxyXG4gICAgY29tcHV0ZWQ6IHt9LFxyXG4gICAgY3JlYXRlZCgpIHtcclxuXHJcbiAgICB9LFxyXG4gICAgbWV0aG9kczoge31cclxuICB9XHJcbjwvc2NyaXB0PlxyXG5cclxuIiwiPHRlbXBsYXRlPlxyXG4gIDxkaXYgY2xhc3M9XCJjb250YWluZXItZmx1aWRcIj5cclxuICAgIDxkaXYgY2xhc3M9XCJwYWdlLWhlYWRlclwiPlxyXG4gICAgICA8aDIgY2xhc3M9XCJoZWFkZXItdGl0bGVcIj5OZ8aw4budaSBkw7luZzwvaDI+XHJcbiAgICAgIDxkaXYgY2xhc3M9XCJoZWFkZXItc3ViLXRpdGxlXCI+XHJcbiAgICAgICAgPG5hdiBjbGFzcz1cImJyZWFkY3J1bWIgYnJlYWRjcnVtYi1kYXNoXCI+XHJcbiAgICAgICAgICA8YSBocmVmPVwiL2JhY2tlbmRcIiBjbGFzcz1cImJyZWFkY3J1bWItaXRlbVwiPjxpIGNsYXNzPVwidGktaG9tZSBwLXItNVwiPjwvaT5I4buHIHRo4buRbmc8L2E+XHJcbiAgICAgICAgICA8YVxyXG4gICAgICAgICAgICAgIGNsYXNzPVwiYnJlYWRjcnVtYi1pdGVtXCJcclxuICAgICAgICAgICAgICBocmVmPVwiI1wiXHJcbiAgICAgICAgICAgICAgQGNsaWNrLnByZXZlbnQ9XCJnb1JlYWQoKVwiXHJcbiAgICAgICAgICA+PGkgY2xhc3M9XCJ0aS1yb2xlIHAtci01XCI+PC9pPk5ow7NtPC9hPlxyXG4gICAgICAgICAgPHNwYW4gY2xhc3M9XCJicmVhZGNydW1iLWl0ZW0gYWN0aXZlXCI+PGkgY2xhc3M9XCJ0aS1wZW5jaWwtYWx0IHAtci01XCI+PC9pPlThuqFvIE5ow7NtPC9zcGFuPlxyXG4gICAgICAgIDwvbmF2PlxyXG4gICAgICA8L2Rpdj5cclxuICAgICAgPGRpdiBjbGFzcz1cIl9oZWFkZXItYnV0dG9uXCI+XHJcbiAgICAgICAgPGJ1dHRvblxyXG4gICAgICAgICAgICBjbGFzcz1cImJ0biBidG4tZ3JhZGllbnQtc3VjY2Vzc1wiXHJcbiAgICAgICAgICAgIEBjbGljay5wcmV2ZW50PVwiZ29DcmVhdGUoKVwiXHJcbiAgICAgICAgPlThuqFvIG5nxrDhu51pIGTDuW5nXHJcbiAgICAgICAgPC9idXR0b24+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgPC9kaXY+XHJcbiAgICA8ZGl2IGNsYXNzPVwiY2FyZFwiPlxyXG4gICAgICA8dmFsaWRhdGlvbi1vYnNlcnZlclxyXG4gICAgICAgICAgcmVmPVwib2JzZXJ2ZXJcIlxyXG4gICAgICAgICAgdi1zbG90PVwieyBpbnZhbGlkIH1cIlxyXG4gICAgICAgICAgdGFnPVwiZGl2XCJcclxuICAgICAgICAgIGNsYXNzPVwiY2FyZC1ib2R5XCJcclxuICAgICAgPlxyXG4gICAgICAgIDxkaXZcclxuICAgICAgICAgICAgY2xhc3M9XCJyb3cgbS12LTMwXCJcclxuICAgICAgICAgICAgQGtleXVwLmVudGVyPVwidXBkYXRlKGludmFsaWQpXCJcclxuICAgICAgICA+XHJcbiAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXNtLTMgdGV4dC1jZW50ZXIgdGV4dC1zbS1sZWZ0XCI+PC9kaXY+XHJcbiAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXNtLTYgdGV4dC1jZW50ZXIgdGV4dC1zbS1sZWZ0XCI+XHJcbiAgICAgICAgICAgIDwhLS0gcm9sZV9uYW1lIC0tPlxyXG4gICAgICAgICAgICA8dmFsaWRhdGlvbi1wcm92aWRlclxyXG4gICAgICAgICAgICAgICAgbmFtZT1cIlTDqm4gbmjDs20gcXV54buBblwiXHJcbiAgICAgICAgICAgICAgICBydWxlcz1cInJlcXVpcmVkXCJcclxuICAgICAgICAgICAgICAgIHYtc2xvdD1cInsgZXJyb3JzLCB2YWxpZCB9XCJcclxuICAgICAgICAgICAgICAgIHRhZz1cImRpdlwiXHJcbiAgICAgICAgICAgICAgICBjbGFzcz1cImZvcm0tZ3JvdXBcIlxyXG4gICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgPGxhYmVsIGNsYXNzPVwiY29udHJvbC1sYWJlbFwiIGZvcj1cImljb24taW5wdXRcIj5OYW1lOjwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImljb24taW5wdXRcIj5cclxuICAgICAgICAgICAgICAgIDxpIGNsYXNzPVwibWRpIG1kaS1hY2NvdW50XCI+PC9pPlxyXG4gICAgICAgICAgICAgICAgPGlucHV0XHJcbiAgICAgICAgICAgICAgICAgICAgdi1tb2RlbD1cImZybURhdGEucm9sZV9uYW1lXCJcclxuICAgICAgICAgICAgICAgICAgICB0eXBlPVwidGV4dFwiXHJcbiAgICAgICAgICAgICAgICAgICAgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIlxyXG4gICAgICAgICAgICAgICAgICAgIDpjbGFzcz1cInsnZXJyb3InOiBmbGFnLmlzVmFsaWRhdGluZyAmJiAhdmFsaWR9XCJcclxuICAgICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcj1cIlJvbGUgbmFtZS4uLlwiXHJcbiAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgPGxhYmVsXHJcbiAgICAgICAgICAgICAgICAgIHYtc2hvdz1cImZsYWcuaXNWYWxpZGF0aW5nICYmICF2YWxpZFwiXHJcbiAgICAgICAgICAgICAgICAgIGNsYXNzPVwiZXJyb3JcIlxyXG4gICAgICAgICAgICAgID57e2Vycm9yc1swXX19PC9sYWJlbD5cclxuICAgICAgICAgICAgPC92YWxpZGF0aW9uLXByb3ZpZGVyPlxyXG4gICAgICAgICAgICA8IS0tIHJvbGVfbmFtZSBlbmQgLS0+XHJcblxyXG4gICAgICAgICAgICA8IS0tIGlzX2Z1bGxhY2Nlc3MgLS0+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjaGVja2JveFwiPlxyXG4gICAgICAgICAgICAgIDxpbnB1dFxyXG4gICAgICAgICAgICAgICAgICB2LW1vZGVsPVwiZnJtRGF0YS5pc19mdWxsYWNjZXNzXCJcclxuICAgICAgICAgICAgICAgICAgaWQ9XCJjaGVja2JveDFcIlxyXG4gICAgICAgICAgICAgICAgICB0eXBlPVwiY2hlY2tib3hcIlxyXG4gICAgICAgICAgICAgICAgICB0cnVlLXZhbHVlPVwiMVwiXHJcbiAgICAgICAgICAgICAgICAgIGZhbHNlLXZhbHVlPVwiMFwiXHJcbiAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgIDxsYWJlbCBmb3I9XCJjaGVja2JveDFcIj5Lw61jaCBob+G6oXQ8L2xhYmVsPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJtLXQtMjVcIj5cclxuICAgICAgICAgICAgICA8YnV0dG9uXHJcbiAgICAgICAgICAgICAgICAgIGlkPVwic2F2ZVwiXHJcbiAgICAgICAgICAgICAgICAgIGNsYXNzPVwiYnRuIGJ0bi1zdWNjZXNzXCJcclxuICAgICAgICAgICAgICAgICAgdHlwZT1cImJ1dHRvblwiXHJcbiAgICAgICAgICAgICAgICAgIDpkaXNhYmxlZD1cImZsYWcuaXNVcGRhdGluZ1wiXHJcbiAgICAgICAgICAgICAgICAgIEBjbGljay5wcmV2ZW50PVwidXBkYXRlKGludmFsaWQpXCJcclxuICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICA8dGVtcGxhdGUgdi1pZj1cImZsYWcuaXNVcGRhdGluZ1wiPlxyXG4gICAgICAgICAgICAgICAgICA8aSBjbGFzcz1cImZhIGZhLXNwaW5uZXIgZmEtcHVsc2UgZmEtZncgbS1yLTVcIj48L2k+VXBkYXRpbmdcclxuICAgICAgICAgICAgICAgIDwvdGVtcGxhdGU+XHJcbiAgICAgICAgICAgICAgICA8dGVtcGxhdGUgdi1lbHNlPlVwZGF0ZTwvdGVtcGxhdGU+XHJcbiAgICAgICAgICAgICAgPC9idXR0b24+XHJcbiAgICAgICAgICAgICAgPGJ1dHRvblxyXG4gICAgICAgICAgICAgICAgICBpZD1cImVkaXRcIiBjbGFzcz1cImJ0biBidG4tZGVmYXVsdFwiIHR5cGU9XCJidXR0b25cIlxyXG4gICAgICAgICAgICAgICAgICBAY2xpY2sucHJldmVudD1cImJhY2tcIlxyXG4gICAgICAgICAgICAgID48aSBjbGFzcz1cInRpLWJhY2stbGVmdCBwLXItMTBcIj48L2k+QmFja1xyXG4gICAgICAgICAgICAgIDwvYnV0dG9uPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgPGRpdiBjbGFzcz1cImNvbFwiPjwvZGl2PlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICA8L3ZhbGlkYXRpb24tb2JzZXJ2ZXI+XHJcbiAgICA8L2Rpdj5cclxuICA8L2Rpdj5cclxuPC90ZW1wbGF0ZT5cclxuXHJcbjxzY3JpcHQ+XHJcbiAgaW1wb3J0IHsgaXNFbXB0eSBhcyBfaXNFbXB0eSB9IGZyb20gJ2xvZGFzaCdcclxuICBpbXBvcnQgeyBWYWxpZGF0aW9uT2JzZXJ2ZXIsIFZhbGlkYXRpb25Qcm92aWRlciwgZXh0ZW5kIH0gZnJvbSBcInZlZS12YWxpZGF0ZVwiXHJcbiAgaW1wb3J0IHsgcmVxdWlyZWQsIGVtYWlsLCBtaW4sIG1heCwgaW50ZWdlciB9IGZyb20gJ3ZlZS12YWxpZGF0ZS9kaXN0L3J1bGVzJ1xyXG5cclxuICBpbXBvcnQgeyBFVkVOVF9CVVNfS0VZIH0gZnJvbSAnJGpzQmFzZVBhdGgvZGVmaW5lJztcclxuICBpbXBvcnQgRXZlbnRCdXMgZnJvbSAnJGpzQmFzZVBhdGgvRXZlbnRCdXMnO1xyXG5cclxuICBpbXBvcnQgVmVlVmFsaWRhdGVNZXNzYWdlIGZyb20gJyRqc0NvbW1vblBhdGgvdXRpbC9WZWVWYWxpZGF0ZU1lc3NhZ2UnXHJcbiAgaW1wb3J0IHsgX2lzRW1wdHlTdHJpbmcgfSBmcm9tIFwiJGpzQ29tbW9uUGF0aC91dGlsL1NhZmVVdGlsXCJcclxuXHJcblxyXG4gIGxldCBvYmpNZXNzYWdlID0gVmVlVmFsaWRhdGVNZXNzYWdlLm1lc3NhZ2VzXHJcblxyXG4gIGV4dGVuZCgncmVxdWlyZWQnLCB7IC4uLnJlcXVpcmVkLCBtZXNzYWdlOiBvYmpNZXNzYWdlLnJlcXVpcmVkIH0pXHJcblxyXG4gIGV4cG9ydCBkZWZhdWx0IHtcclxuICAgIG5hbWU6ICdyb2xlLWFkZC1zZWN0aW9uJyxcclxuICAgIGNvbXBvbmVudHM6IHtcclxuICAgICAgJ3ZhbGlkYXRpb24tb2JzZXJ2ZXInOiBWYWxpZGF0aW9uT2JzZXJ2ZXIsXHJcbiAgICAgICd2YWxpZGF0aW9uLXByb3ZpZGVyJzogVmFsaWRhdGlvblByb3ZpZGVyXHJcbiAgICB9LFxyXG4gICAgZGF0YSgpIHtcclxuICAgICAgcmV0dXJuIHtcclxuICAgICAgICB1cmw6IHtcclxuICAgICAgICAgIHJlc291cmNlVVJMOiBDREFUQS5yZXNvdXJjZVVSTFxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgZmxhZzoge1xyXG4gICAgICAgICAgaXNWYWxpZGF0aW5nOiBmYWxzZSxcclxuICAgICAgICAgIGlzVXBkYXRpbmc6IGZhbHNlXHJcbiAgICAgICAgfSxcclxuICAgICAgICBmcm1EYXRhOiB7fVxyXG4gICAgICB9XHJcbiAgICB9LFxyXG4gICAgY29tcHV0ZWQ6IHtcclxuICAgICAgcHJvZmlsZUltYWdlKCkge1xyXG4gICAgICAgIGlmIChfaXNFbXB0eSh0aGlzLmZybURhdGEucHJvZmlsZV9pbWFnZSkpXHJcbiAgICAgICAgICByZXR1cm4gdGhpcy51cmwucmVzb3VyY2VVUkwgKyAnL2dsb2JhbC9pbWFnZS9ub3RGb3VuZC5wbmcnXHJcblxyXG4gICAgICAgIHJldHVybiB0aGlzLmZybURhdGEucHJvZmlsZV9pbWFnZVxyXG4gICAgICB9LFxyXG4gICAgICBvYmpFbnRpdHlTZWxlY3QoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuJHN0b3JlLnN0YXRlLm9iakVudGl0eVNlbGVjdFxyXG4gICAgICB9XHJcbiAgICB9LFxyXG4gICAgY3JlYXRlZCgpIHtcclxuICAgICAgbGV0IHNlbGYgPSB0aGlzXHJcblxyXG4gICAgICBFdmVudEJ1cy5vZmYoRVZFTlRfQlVTX0tFWS5DT05GSVJNX01PREFMKVxyXG4gICAgICBFdmVudEJ1cy5vbihFVkVOVF9CVVNfS0VZLkNPTkZJUk1fTU9EQUwsIChwYXlsb2FkKSA9PiB7XHJcbiAgICAgICAgdGhpcy4kc3RvcmUuY29tbWl0KCdyZXNldEVudGl0eScpXHJcblxyXG4gICAgICAgIHNlbGYuZmxhZy5pc1ZhbGlkYXRpbmcgPSBmYWxzZVxyXG4gICAgICAgIHNlbGYuZmxhZy5pc1VwZGF0aW5nID0gZmFsc2VcclxuXHJcbiAgICAgICAgdGhpcy5mcm1EYXRhID0gdGhpcy5vYmpFbnRpdHlTZWxlY3RcclxuICAgICAgfSlcclxuXHJcbiAgICAgIHRoaXMuJHN0b3JlLmNvbW1pdCgncmVzZXRFbnRpdHknKVxyXG4gICAgICB0aGlzLmZybURhdGEgPSB0aGlzLm9iakVudGl0eVNlbGVjdFxyXG4gICAgfSxcclxuICAgIG1ldGhvZHM6IHtcclxuICAgICAgZ29SZWFkKCkge1xyXG4gICAgICAgIHRoaXMuJHJvdXRlci5wdXNoKHsgbmFtZTogJ3JvbGU6aW5kZXgnIH0pXHJcbiAgICAgIH0sXHJcbiAgICAgIGdvQ3JlYXRlKCkge1xyXG4gICAgICAgIHRoaXMuJHJvdXRlci5wdXNoKHsgbmFtZTogJ3JvbGU6YWRkJywgcGFyYW1zOiB7IHBhZ2U6IDEgfSB9KVxyXG4gICAgICB9LFxyXG4gICAgICBiYWNrKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLiRyb3V0ZXIuYmFjaygpXHJcbiAgICAgIH0sXHJcbiAgICAgIHVwZGF0ZShpbnZhbGlkKSB7XHJcbiAgICAgICAgdGhpcy5mbGFnLmlzVmFsaWRhdGluZyA9IHRydWVcclxuXHJcbiAgICAgICAgaWYgKGludmFsaWQpIHtcclxuICAgICAgICAgIHRoaXMuJHJlZnMub2JzZXJ2ZXIudmFsaWRhdGUoKVxyXG4gICAgICAgICAgcmV0dXJuXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLmZsYWcuaXNVcGRhdGluZyA9IHRydWVcclxuICAgICAgICB0aGlzLiRzdG9yZS5kaXNwYXRjaCgnY3JlYXRlRW50aXR5QWN0JywgdGhpcy5mcm1EYXRhKVxyXG4gICAgICB9LFxyXG4gICAgICB1cGxvYWQoZSkge1xyXG4gICAgICAgIGNvbnN0IGZpbGUgPSBlLnRhcmdldC5maWxlc1sgMCBdXHJcblxyXG4gICAgICAgIHRoaXMuJHN0b3JlLmRpc3BhdGNoKCd1cGxvYWRGaWxlQWN0Jywge1xyXG4gICAgICAgICAgY29udHJvbGxlcjogQ0RBVEEuY29udHJvbGxlcixcclxuICAgICAgICAgIGZpbGU6IGZpbGVcclxuICAgICAgICB9KS50aGVuKHBheWxvYWQgPT4ge1xyXG4gICAgICAgICAgbGV0IG9iakltYWdlID0gcGF5bG9hZC5hcnJJbWFnZVsgMCBdXHJcbiAgICAgICAgICBpZiAoX2lzRW1wdHkob2JqSW1hZ2UpKSB7XHJcbiAgICAgICAgICAgIHJldHVyblxyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIHRoaXMuZnJtRGF0YS5wcm9maWxlX2ltYWdlID0gb2JqSW1hZ2UuaW1hZ2VcclxuICAgICAgICB9KVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG48L3NjcmlwdD5cclxuXHJcbiIsIjx0ZW1wbGF0ZT5cclxuICA8ZGl2IGNsYXNzPVwiY29udGFpbmVyLWZsdWlkXCI+XHJcbiAgICA8ZGl2IGNsYXNzPVwicGFnZS1oZWFkZXJcIj5cclxuICAgICAgPGgyIGNsYXNzPVwiaGVhZGVyLXRpdGxlXCI+TmfGsOG7nWkgZMO5bmc8L2gyPlxyXG4gICAgICA8ZGl2IGNsYXNzPVwiaGVhZGVyLXN1Yi10aXRsZVwiPlxyXG4gICAgICAgIDxuYXYgY2xhc3M9XCJicmVhZGNydW1iIGJyZWFkY3J1bWItZGFzaFwiPlxyXG4gICAgICAgICAgPGEgaHJlZj1cIi9iYWNrZW5kXCIgY2xhc3M9XCJicmVhZGNydW1iLWl0ZW1cIj48aSBjbGFzcz1cInRpLWhvbWUgcC1yLTVcIj48L2k+SOG7hyB0aOG7kW5nPC9hPlxyXG4gICAgICAgICAgPGFcclxuICAgICAgICAgICAgICBjbGFzcz1cImJyZWFkY3J1bWItaXRlbVwiXHJcbiAgICAgICAgICAgICAgaHJlZj1cIiNcIlxyXG4gICAgICAgICAgICAgIEBjbGljay5wcmV2ZW50PVwiZ29SZWFkKClcIlxyXG4gICAgICAgICAgPjxpIGNsYXNzPVwidGktcm9sZSBwLXItNVwiPjwvaT5OaMOzbTwvYT5cclxuICAgICAgICAgIDxzcGFuIGNsYXNzPVwiYnJlYWRjcnVtYi1pdGVtIGFjdGl2ZVwiPjxpIGNsYXNzPVwidGktcGVuY2lsLWFsdCBwLXItNVwiPjwvaT5D4bqtcCBuaOG6rXQgTmjDs208L3NwYW4+XHJcbiAgICAgICAgPC9uYXY+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgICA8ZGl2IGNsYXNzPVwiX2hlYWRlci1idXR0b25cIj5cclxuICAgICAgICA8YnV0dG9uXHJcbiAgICAgICAgICAgIGNsYXNzPVwiYnRuIGJ0bi1ncmFkaWVudC1zdWNjZXNzXCJcclxuICAgICAgICAgICAgQGNsaWNrLnByZXZlbnQ9XCJnb0NyZWF0ZSgpXCJcclxuICAgICAgICA+VOG6oW8gbmjDs21cclxuICAgICAgICA8L2J1dHRvbj5cclxuICAgICAgPC9kaXY+XHJcbiAgICA8L2Rpdj5cclxuICAgIDxkaXYgY2xhc3M9XCJjYXJkXCI+XHJcbiAgICAgIDx2YWxpZGF0aW9uLW9ic2VydmVyXHJcbiAgICAgICAgICByZWY9XCJvYnNlcnZlclwiXHJcbiAgICAgICAgICB2LXNsb3Q9XCJ7IGludmFsaWQgfVwiXHJcbiAgICAgICAgICB0YWc9XCJkaXZcIlxyXG4gICAgICAgICAgY2xhc3M9XCJjYXJkLWJvZHlcIlxyXG4gICAgICA+XHJcbiAgICAgICAgPGRpdlxyXG4gICAgICAgICAgICBjbGFzcz1cInJvdyBtLXYtMzBcIlxyXG4gICAgICAgICAgICBAa2V5dXAuZW50ZXI9XCJ1cGRhdGUoaW52YWxpZClcIlxyXG4gICAgICAgID5cclxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtc20tMyB0ZXh0LWNlbnRlciB0ZXh0LXNtLWxlZnRcIj48L2Rpdj5cclxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtc20tNiB0ZXh0LWNlbnRlciB0ZXh0LXNtLWxlZnRcIj5cclxuICAgICAgICAgICAgPCEtLSByb2xlX25hbWUgLS0+XHJcbiAgICAgICAgICAgIDx2YWxpZGF0aW9uLXByb3ZpZGVyXHJcbiAgICAgICAgICAgICAgICBuYW1lPVwiVMOqbiBuaMOzbSBxdXnhu4FuXCJcclxuICAgICAgICAgICAgICAgIHJ1bGVzPVwicmVxdWlyZWRcIlxyXG4gICAgICAgICAgICAgICAgdi1zbG90PVwieyBlcnJvcnMsIHZhbGlkIH1cIlxyXG4gICAgICAgICAgICAgICAgdGFnPVwiZGl2XCJcclxuICAgICAgICAgICAgICAgIGNsYXNzPVwiZm9ybS1ncm91cFwiXHJcbiAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICA8bGFiZWwgY2xhc3M9XCJjb250cm9sLWxhYmVsXCIgZm9yPVwiaWNvbi1pbnB1dFwiPk5hbWU6PC9sYWJlbD5cclxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiaWNvbi1pbnB1dFwiPlxyXG4gICAgICAgICAgICAgICAgPGkgY2xhc3M9XCJtZGkgbWRpLWFjY291bnRcIj48L2k+XHJcbiAgICAgICAgICAgICAgICA8aW5wdXRcclxuICAgICAgICAgICAgICAgICAgICB2LW1vZGVsPVwiZnJtRGF0YS5yb2xlX25hbWVcIlxyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU9XCJ0ZXh0XCJcclxuICAgICAgICAgICAgICAgICAgICBjbGFzcz1cImZvcm0tY29udHJvbFwiXHJcbiAgICAgICAgICAgICAgICAgICAgOmNsYXNzPVwieydlcnJvcic6IGZsYWcuaXNWYWxpZGF0aW5nICYmICF2YWxpZH1cIlxyXG4gICAgICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyPVwiUm9sZSBuYW1lLi4uXCJcclxuICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICA8bGFiZWxcclxuICAgICAgICAgICAgICAgICAgdi1zaG93PVwiZmxhZy5pc1ZhbGlkYXRpbmcgJiYgIXZhbGlkXCJcclxuICAgICAgICAgICAgICAgICAgY2xhc3M9XCJlcnJvclwiXHJcbiAgICAgICAgICAgICAgPnt7ZXJyb3JzWzBdfX08L2xhYmVsPlxyXG4gICAgICAgICAgICA8L3ZhbGlkYXRpb24tcHJvdmlkZXI+XHJcbiAgICAgICAgICAgIDwhLS0gcm9sZV9uYW1lIGVuZCAtLT5cclxuXHJcbiAgICAgICAgICAgIDwhLS0gaXNfZnVsbGFjY2VzcyAtLT5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNoZWNrYm94XCI+XHJcbiAgICAgICAgICAgICAgPGlucHV0XHJcbiAgICAgICAgICAgICAgICAgIHYtbW9kZWw9XCJmcm1EYXRhLmlzX2Z1bGxhY2Nlc3NcIlxyXG4gICAgICAgICAgICAgICAgICBpZD1cImNoZWNrYm94MVwiXHJcbiAgICAgICAgICAgICAgICAgIHR5cGU9XCJjaGVja2JveFwiXHJcbiAgICAgICAgICAgICAgICAgIHRydWUtdmFsdWU9XCIxXCJcclxuICAgICAgICAgICAgICAgICAgZmFsc2UtdmFsdWU9XCIwXCJcclxuICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgPGxhYmVsIGZvcj1cImNoZWNrYm94MVwiPkvDrWNoIGhv4bqhdDwvbGFiZWw+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm0tdC0yNVwiPlxyXG4gICAgICAgICAgICAgIDxidXR0b25cclxuICAgICAgICAgICAgICAgICAgaWQ9XCJzYXZlXCJcclxuICAgICAgICAgICAgICAgICAgY2xhc3M9XCJidG4gYnRuLXN1Y2Nlc3NcIlxyXG4gICAgICAgICAgICAgICAgICB0eXBlPVwiYnV0dG9uXCJcclxuICAgICAgICAgICAgICAgICAgOmRpc2FibGVkPVwiZmxhZy5pc1VwZGF0aW5nXCJcclxuICAgICAgICAgICAgICAgICAgQGNsaWNrLnByZXZlbnQ9XCJ1cGRhdGUoaW52YWxpZClcIlxyXG4gICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgIDx0ZW1wbGF0ZSB2LWlmPVwiZmxhZy5pc1VwZGF0aW5nXCI+XHJcbiAgICAgICAgICAgICAgICAgIDxpIGNsYXNzPVwiZmEgZmEtc3Bpbm5lciBmYS1wdWxzZSBmYS1mdyBtLXItNVwiPjwvaT5VcGRhdGluZ1xyXG4gICAgICAgICAgICAgICAgPC90ZW1wbGF0ZT5cclxuICAgICAgICAgICAgICAgIDx0ZW1wbGF0ZSB2LWVsc2U+VXBkYXRlPC90ZW1wbGF0ZT5cclxuICAgICAgICAgICAgICA8L2J1dHRvbj5cclxuICAgICAgICAgICAgICA8YnV0dG9uXHJcbiAgICAgICAgICAgICAgICAgIGlkPVwiZWRpdFwiIGNsYXNzPVwiYnRuIGJ0bi1kZWZhdWx0XCIgdHlwZT1cImJ1dHRvblwiXHJcbiAgICAgICAgICAgICAgICAgIEBjbGljay5wcmV2ZW50PVwiYmFja1wiXHJcbiAgICAgICAgICAgICAgPjxpIGNsYXNzPVwidGktYmFjay1sZWZ0IHAtci0xMFwiPjwvaT5CYWNrXHJcbiAgICAgICAgICAgICAgPC9idXR0b24+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sXCI+PC9kaXY+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgIDwvdmFsaWRhdGlvbi1vYnNlcnZlcj5cclxuICAgIDwvZGl2PlxyXG4gIDwvZGl2PlxyXG48L3RlbXBsYXRlPlxyXG5cclxuPHNjcmlwdD5cclxuICBpbXBvcnQgeyBpc0VtcHR5IGFzIF9pc0VtcHR5IH0gZnJvbSAnbG9kYXNoJ1xyXG4gIGltcG9ydCB7IFZhbGlkYXRpb25PYnNlcnZlciwgVmFsaWRhdGlvblByb3ZpZGVyLCBleHRlbmQgfSBmcm9tIFwidmVlLXZhbGlkYXRlXCJcclxuICBpbXBvcnQgeyByZXF1aXJlZCwgZW1haWwsIG1pbiwgbWF4LCBpbnRlZ2VyIH0gZnJvbSAndmVlLXZhbGlkYXRlL2Rpc3QvcnVsZXMnXHJcblxyXG4gIGltcG9ydCB7IEVWRU5UX0JVU19LRVkgfSBmcm9tICckanNCYXNlUGF0aC9kZWZpbmUnO1xyXG4gIGltcG9ydCBFdmVudEJ1cyBmcm9tICckanNCYXNlUGF0aC9FdmVudEJ1cyc7XHJcblxyXG4gIGltcG9ydCBWZWVWYWxpZGF0ZU1lc3NhZ2UgZnJvbSAnJGpzQ29tbW9uUGF0aC91dGlsL1ZlZVZhbGlkYXRlTWVzc2FnZSdcclxuICBpbXBvcnQgeyBfaXNFbXB0eVN0cmluZywgX2ludCB9IGZyb20gXCIkanNDb21tb25QYXRoL3V0aWwvU2FmZVV0aWxcIlxyXG5cclxuICBsZXQgb2JqTWVzc2FnZSA9IFZlZVZhbGlkYXRlTWVzc2FnZS5tZXNzYWdlc1xyXG5cclxuICBleHRlbmQoJ3JlcXVpcmVkJywgeyAuLi5yZXF1aXJlZCwgbWVzc2FnZTogb2JqTWVzc2FnZS5yZXF1aXJlZCB9KVxyXG5cclxuICBleHBvcnQgZGVmYXVsdCB7XHJcbiAgICBuYW1lOiAncm9sZS1hZGQtc2VjdGlvbicsXHJcbiAgICBjb21wb25lbnRzOiB7XHJcbiAgICAgICd2YWxpZGF0aW9uLW9ic2VydmVyJzogVmFsaWRhdGlvbk9ic2VydmVyLFxyXG4gICAgICAndmFsaWRhdGlvbi1wcm92aWRlcic6IFZhbGlkYXRpb25Qcm92aWRlclxyXG4gICAgfSxcclxuICAgIGRhdGEoKSB7XHJcbiAgICAgIHJldHVybiB7XHJcbiAgICAgICAgdXJsOiB7XHJcbiAgICAgICAgICByZXNvdXJjZVVSTDogQ0RBVEEucmVzb3VyY2VVUkxcclxuICAgICAgICB9LFxyXG4gICAgICAgIGZsYWc6IHtcclxuICAgICAgICAgIGlzVmFsaWRhdGluZzogZmFsc2UsXHJcbiAgICAgICAgICBpc1VwZGF0aW5nOiBmYWxzZVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgZnJtRGF0YToge31cclxuICAgICAgfVxyXG4gICAgfSxcclxuICAgIGNvbXB1dGVkOiB7XHJcbiAgICAgIHByb2ZpbGVJbWFnZSgpIHtcclxuICAgICAgICBpZiAoX2lzRW1wdHkodGhpcy5mcm1EYXRhLnByb2ZpbGVfaW1hZ2UpKVxyXG4gICAgICAgICAgcmV0dXJuIHRoaXMudXJsLnJlc291cmNlVVJMICsgJy9nbG9iYWwvaW1hZ2Uvbm90Rm91bmQucG5nJ1xyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy5mcm1EYXRhLnByb2ZpbGVfaW1hZ2VcclxuICAgICAgfSxcclxuICAgICAgb2JqRW50aXR5U2VsZWN0KCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLiRzdG9yZS5zdGF0ZS5vYmpFbnRpdHlTZWxlY3RcclxuICAgICAgfVxyXG4gICAgfSxcclxuICAgIGNyZWF0ZWQoKSB7XHJcbiAgICAgIGxldCBzZWxmID0gdGhpc1xyXG4gICAgICBFdmVudEJ1cy5vZmYoRVZFTlRfQlVTX0tFWS5DT05GSVJNX01PREFMKVxyXG4gICAgICBFdmVudEJ1cy5vbihFVkVOVF9CVVNfS0VZLkNPTkZJUk1fTU9EQUwsIChwYXlsb2FkKSA9PiB7XHJcbiAgICAgICAgc2VsZi5mbGFnLmlzVmFsaWRhdGluZyA9IGZhbHNlXHJcbiAgICAgICAgc2VsZi5mbGFnLmlzVXBkYXRpbmcgPSBmYWxzZVxyXG5cclxuICAgICAgICB0aGlzLmZybURhdGEgPSB0aGlzLm9iakVudGl0eVNlbGVjdFxyXG5cclxuICAgICAgICB0aGlzLmZybURhdGEucGFzc3dvcmQgPSBudWxsXHJcbiAgICAgIH0pXHJcblxyXG4gICAgICBpZiAoIXRoaXMuJHN0b3JlLnN0YXRlLmZsYWcuaXNHZXQpIHtcclxuICAgICAgICBsZXQgeyBpZCwgcGFnZSB9ID0gdGhpcy4kcm91dGUucGFyYW1zXHJcbiAgICAgICAgaWYgKF9pbnQoaWQpIDw9IDApXHJcbiAgICAgICAgICByZXR1cm5cclxuXHJcbiAgICAgICAgdGhpcy5nZXQoaWQpXHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5mcm1EYXRhID0gdGhpcy5vYmpFbnRpdHlTZWxlY3RcclxuICAgICAgfVxyXG4gICAgfSxcclxuICAgIG1ldGhvZHM6IHtcclxuICAgICAgZ29SZWFkKCkge1xyXG4gICAgICAgIHRoaXMuJHJvdXRlci5wdXNoKHsgbmFtZTogJ3JvbGU6aW5kZXgnIH0pXHJcbiAgICAgIH0sXHJcbiAgICAgIGdvQ3JlYXRlKCkge1xyXG4gICAgICAgIHRoaXMuJHJvdXRlci5wdXNoKHsgbmFtZTogJ3JvbGU6YWRkJywgcGFyYW1zOiB7IHBhZ2U6IDEgfSB9KVxyXG4gICAgICB9LFxyXG4gICAgICBiYWNrKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLiRyb3V0ZXIuYmFjaygpXHJcbiAgICAgIH0sXHJcbiAgICAgIGdldChpZCkge1xyXG4gICAgICAgIHRoaXMuJHN0b3JlLmRpc3BhdGNoKCdnZXRFbnRpdHlBY3QnLCB7IGlkOiBpZCB9KS50aGVuKHBheWxvYWQgPT4ge1xyXG4gICAgICAgICAgdGhpcy5mcm1EYXRhID0gdGhpcy5vYmpFbnRpdHlTZWxlY3RcclxuICAgICAgICB9KVxyXG4gICAgICB9LFxyXG4gICAgICB1cGRhdGUoaW52YWxpZCkge1xyXG4gICAgICAgIHRoaXMuZmxhZy5pc1ZhbGlkYXRpbmcgPSB0cnVlXHJcblxyXG4gICAgICAgIGlmIChpbnZhbGlkKSB7XHJcbiAgICAgICAgICB0aGlzLiRyZWZzLm9ic2VydmVyLnZhbGlkYXRlKClcclxuICAgICAgICAgIHJldHVyblxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5mbGFnLmlzVXBkYXRpbmcgPSB0cnVlXHJcblxyXG4gICAgICAgIGxldCBpZCA9IHRoaXMub2JqRW50aXR5U2VsZWN0LnJvbGVfaWRcclxuICAgICAgICB0aGlzLiRzdG9yZS5kaXNwYXRjaCgndXBkYXRlRW50aXR5QWN0JywgeyBpZDogaWQsIC4uLnRoaXMuZnJtRGF0YSB9KVxyXG4gICAgICB9LFxyXG4gICAgICB1cGxvYWQoZSkge1xyXG4gICAgICAgIGNvbnN0IGZpbGUgPSBlLnRhcmdldC5maWxlc1sgMCBdXHJcblxyXG4gICAgICAgIHRoaXMuJHN0b3JlLmRpc3BhdGNoKCd1cGxvYWRGaWxlQWN0Jywge1xyXG4gICAgICAgICAgY29udHJvbGxlcjogQ0RBVEEuY29udHJvbGxlcixcclxuICAgICAgICAgIGZpbGU6IGZpbGVcclxuICAgICAgICB9KS50aGVuKHBheWxvYWQgPT4ge1xyXG4gICAgICAgICAgbGV0IG9iakltYWdlID0gcGF5bG9hZC5hcnJJbWFnZVsgMCBdXHJcbiAgICAgICAgICBpZiAoX2lzRW1wdHkob2JqSW1hZ2UpKSB7XHJcbiAgICAgICAgICAgIHJldHVyblxyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIHRoaXMuZnJtRGF0YS5wcm9maWxlX2ltYWdlID0gb2JqSW1hZ2UuaW1hZ2VcclxuICAgICAgICB9KVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG48L3NjcmlwdD5cclxuXHJcbiIsIjx0ZW1wbGF0ZT5cclxuICA8ZGl2IGNsYXNzPVwiY29udGFpbmVyLWZsdWlkXCI+XHJcbiAgICA8ZGl2IGNsYXNzPVwicGFnZS1oZWFkZXJcIj5cclxuICAgICAgPGgyIGNsYXNzPVwiaGVhZGVyLXRpdGxlXCI+TmfGsOG7nWkgZMO5bmc8L2gyPlxyXG4gICAgICA8ZGl2IGNsYXNzPVwiaGVhZGVyLXN1Yi10aXRsZVwiPlxyXG4gICAgICAgIDxuYXYgY2xhc3M9XCJicmVhZGNydW1iIGJyZWFkY3J1bWItZGFzaFwiPlxyXG4gICAgICAgICAgPGEgaHJlZj1cIi9iYWNrZW5kXCIgY2xhc3M9XCJicmVhZGNydW1iLWl0ZW1cIj48aSBjbGFzcz1cInRpLWhvbWUgcC1yLTVcIj48L2k+SOG7hyB0aOG7kW5nPC9hPlxyXG4gICAgICAgICAgPGFcclxuICAgICAgICAgICAgICBjbGFzcz1cImJyZWFkY3J1bWItaXRlbVwiXHJcbiAgICAgICAgICAgICAgaHJlZj1cIiNcIlxyXG4gICAgICAgICAgICAgIEBjbGljay5wcmV2ZW50PVwiXCJcclxuICAgICAgICAgID48aSBjbGFzcz1cInRpLXJvbGUgcC1yLTVcIj48L2k+TmjDs208L2E+XHJcbiAgICAgICAgICA8c3BhbiBjbGFzcz1cImJyZWFkY3J1bWItaXRlbSBhY3RpdmVcIj48aSBjbGFzcz1cInRpLWxpc3QgcC1yLTVcIj48L2k+RGFuaCBzw6FjaDwvc3Bhbj5cclxuICAgICAgICA8L25hdj5cclxuICAgICAgPC9kaXY+XHJcbiAgICAgIDxkaXYgY2xhc3M9XCJfaGVhZGVyLWJ1dHRvblwiPlxyXG4gICAgICAgIDxidXR0b25cclxuICAgICAgICAgICAgY2xhc3M9XCJidG4gYnRuLWdyYWRpZW50LXN1Y2Nlc3NcIlxyXG4gICAgICAgICAgICBAY2xpY2sucHJldmVudD1cImdvQ3JlYXRlKClcIlxyXG4gICAgICAgID5U4bqhbyBuaMOzbVxyXG4gICAgICAgIDwvYnV0dG9uPlxyXG4gICAgICA8L2Rpdj5cclxuICAgIDwvZGl2PlxyXG4gICAgPGRpdiBjbGFzcz1cImNhcmRcIj5cclxuICAgICAgPGRpdiBjbGFzcz1cImNhcmQtYm9keVwiPlxyXG4gICAgICAgIDxyb2xlLXRhYmxlPjwvcm9sZS10YWJsZT5cclxuICAgICAgPC9kaXY+XHJcbiAgICA8L2Rpdj5cclxuICA8L2Rpdj5cclxuPC90ZW1wbGF0ZT5cclxuXHJcbjxzY3JpcHQ+XHJcbiAgaW1wb3J0IFJvbGVUYWJsZSBmcm9tICckdnVlUGFydGlhbFBhdGgvcm9sZS9Sb2xlVGFibGUnXHJcblxyXG4gIGV4cG9ydCBkZWZhdWx0IHtcclxuICAgIG5hbWU6ICdyb2xlLWluZGV4LXNlY3Rpb24nLFxyXG4gICAgY29tcG9uZW50czoge1xyXG4gICAgICAncm9sZS10YWJsZSc6IFJvbGVUYWJsZVxyXG4gICAgfSxcclxuICAgIGRhdGEoKSB7XHJcbiAgICAgIHJldHVybiB7fVxyXG4gICAgfSxcclxuICAgIGNvbXB1dGVkOiB7fSxcclxuICAgIGNyZWF0ZWQoKSB7XHJcbiAgICAgIGlmICghdGhpcy4kc3RvcmUuc3RhdGUuZmxhZy5pc0dldCkge1xyXG4gICAgICAgIHRoaXMuJHN0b3JlLmNvbW1pdCgndXBkYXRlRmxhZycsIHsgZmxhZzogJ2lzR2V0JywgdmFsdWU6IHRydWUgfSlcclxuICAgICAgICB0aGlzLiRzdG9yZS5kaXNwYXRjaCgncmVhZEVudGl0eUFjdCcpXHJcbiAgICAgIH1cclxuICAgIH0sXHJcbiAgICBtZXRob2RzOiB7XHJcbiAgICAgIGdvUmVhZCgpIHtcclxuICAgICAgICB0aGlzLiRyb3V0ZXIucHVzaCh7IG5hbWU6ICdyb2xlOmluZGV4JyB9KVxyXG4gICAgICB9LFxyXG4gICAgICBnb0NyZWF0ZSgpIHtcclxuICAgICAgICB0aGlzLiRyb3V0ZXIucHVzaCh7IG5hbWU6ICdyb2xlOmFkZCcsIHBhcmFtczogeyBwYWdlOiAxIH0gfSlcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuPC9zY3JpcHQ+XHJcblxyXG4iLCI8dGVtcGxhdGU+XHJcbiAgPGRpdiBpZD1cInJvbGUtdGFibGVcIiBjbGFzcz1cInRhYmxlLW92ZXJmbG93XCI+XHJcbiAgICA8dGFibGUgaWQ9XCJkdC1vcHRcIiBjbGFzcz1cInRhYmxlIHRhYmxlLWhvdmVyIHRhYmxlLXhsXCI+XHJcbiAgICAgIDx0aGVhZD5cclxuICAgICAgICA8dHI+XHJcbiAgICAgICAgICA8dGg+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjaGVja2JveCBwLTBcIj5cclxuICAgICAgICAgICAgICA8aW5wdXQgaWQ9XCJzZWxlY3RhYmxlMVwiIHR5cGU9XCJjaGVja2JveFwiIGNsYXNzPVwiY2hlY2tBbGxcIiBuYW1lPVwiY2hlY2tBbGxcIj5cclxuICAgICAgICAgICAgICA8bGFiZWwgZm9yPVwic2VsZWN0YWJsZTFcIj48L2xhYmVsPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgIDwvdGg+XHJcbiAgICAgICAgICA8dGg+Uk9MRTwvdGg+XHJcbiAgICAgICAgICA8dGg+U1RBVFVTPC90aD5cclxuICAgICAgICAgIDx0aD4jPC90aD5cclxuICAgICAgICAgIDx0aD48L3RoPlxyXG4gICAgICAgIDwvdHI+XHJcbiAgICAgIDwvdGhlYWQ+XHJcbiAgICAgIDx0Ym9keT5cclxuICAgICAgICA8dGVtcGxhdGUgdi1mb3I9XCIob2JqRW50aXR5LCBpbmRleCkgaW4gYXJyRW50aXR5TGlzdFwiPlxyXG4gICAgICAgICAgPHJvbGUtdGFibGUtcm93XHJcbiAgICAgICAgICAgICAgOmluZGV4PVwiaW5kZXhcIlxyXG4gICAgICAgICAgICAgIDpvYmotZW50aXR5PVwib2JqRW50aXR5XCJcclxuICAgICAgICAgID48L3JvbGUtdGFibGUtcm93PlxyXG4gICAgICAgIDwvdGVtcGxhdGU+XHJcbiAgICAgIDwvdGJvZHk+XHJcbiAgICA8L3RhYmxlPlxyXG4gIDwvZGl2PlxyXG48L3RlbXBsYXRlPlxyXG5cclxuPHNjcmlwdD5cclxuICBpbXBvcnQgUm9sZVRhYmxlUm93IGZyb20gJyR2dWVQYXJ0aWFsUGF0aC9yb2xlL1JvbGVUYWJsZVJvdydcclxuXHJcbiAgZXhwb3J0IGRlZmF1bHQge1xyXG4gICAgbmFtZTogJ3JvbGUtdGFibGUnLFxyXG4gICAgY29tcG9uZW50czoge1xyXG4gICAgICAncm9sZS10YWJsZS1yb3cnOiBSb2xlVGFibGVSb3dcclxuICAgIH0sXHJcbiAgICBkYXRhKCkge1xyXG4gICAgICByZXR1cm4ge31cclxuICAgIH0sXHJcbiAgICBjb21wdXRlZDoge1xyXG4gICAgICBhcnJFbnRpdHlMaXN0KCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLiRzdG9yZS5zdGF0ZS5hcnJFbnRpdHlMaXN0XHJcbiAgICAgIH1cclxuICAgIH0sXHJcbiAgICBjcmVhdGVkKCkge1xyXG5cclxuICAgIH0sXHJcbiAgICBtZXRob2RzOiB7fVxyXG4gIH1cclxuPC9zY3JpcHQ+XHJcblxyXG4iLCI8dGVtcGxhdGU+XHJcbiAgPHRyPlxyXG4gICAgPHRkPlxyXG4gICAgICA8ZGl2IGNsYXNzPVwiY2hlY2tib3hcIj5cclxuICAgICAgICA8aW5wdXQgaWQ9XCJzZWxlY3RhYmxlMlwiIHR5cGU9XCJjaGVja2JveFwiPlxyXG4gICAgICAgIDxsYWJlbCBmb3I9XCJzZWxlY3RhYmxlMlwiPjwvbGFiZWw+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgPC90ZD5cclxuICAgIDx0ZD57e29iakVudGl0eS5yb2xlX25hbWV9fTwvdGQ+XHJcbiAgICA8dGQ+XHJcbiAgICAgIDx0ZW1wbGF0ZSB2LWlmPVwib2JqRW50aXR5LmlzX2Z1bGxhY2Nlc3MgPT0gMVwiPlxyXG4gICAgICAgIDxzcGFuIGNsYXNzPVwiYmFkZ2UgYmFkZ2UtcGlsbCBiYWRnZS1ncmFkaWVudC1zdWNjZXNzXCI+xJDhuqd5IMSR4bunIHF1eeG7gW4gaOG6oW48L3NwYW4+XHJcbiAgICAgIDwvdGVtcGxhdGU+XHJcbiAgICAgIDx0ZW1wbGF0ZSB2LWVsc2U+XHJcbiAgICAgICAgPHNwYW4gY2xhc3M9XCJiYWRnZSBiYWRnZS1waWxsIGJhZGdlLWdyYWRpZW50LWRhbmdlclwiPkNoxrBhIMSR4bqneSDEkeG7pyBxdXnhu4FuIGjhuqFuPC9zcGFuPlxyXG4gICAgICA8L3RlbXBsYXRlPlxyXG4gICAgPC90ZD5cclxuICAgIDx0ZCBjbGFzcz1cInRleHQtY2VudGVyIGZvbnQtc2l6ZS0xOFwiPlxyXG4gICAgICA8YVxyXG4gICAgICAgICAgY2xhc3M9XCJ0ZXh0LWdyYXkgbS1yLTE1XCJcclxuICAgICAgICAgIEBjbGljaz1cImdvVXBkYXRlKClcIlxyXG4gICAgICA+PGkgY2xhc3M9XCJ0aS1wZW5jaWxcIj48L2k+XHJcbiAgICAgIDwvYT5cclxuICAgICAgPGFcclxuICAgICAgICAgIGNsYXNzPVwidGV4dC1ncmF5XCJcclxuICAgICAgICAgIEBjbGljaz1cInJlbW92ZShvYmpFbnRpdHkucm9sZV9pZClcIlxyXG4gICAgICA+PGlcclxuICAgICAgICAgIDpjbGFzcz1cIntcclxuICAgICAgICAgICdmYSBmYS1zcGlubmVyIGZhLXB1bHNlIGZhLWZ3JzogZmxhZy5pc0RlbGV0aW5nLFxyXG4gICAgICAgICAgJ3RpLXRyYXNoJzogIWZsYWcuaXNEZWxldGluZ1xyXG4gICAgICAgICAgfVwiPlxyXG4gICAgICA8L2k+PC9hPlxyXG4gICAgPC90ZD5cclxuICA8L3RyPlxyXG48L3RlbXBsYXRlPlxyXG5cclxuPHNjcmlwdD5cclxuICBpbXBvcnQgeyBfaXNFbXB0eVN0cmluZyB9IGZyb20gXCIkanNDb21tb25QYXRoL3V0aWwvU2FmZVV0aWxcIlxyXG5cclxuICBleHBvcnQgZGVmYXVsdCB7XHJcbiAgICBuYW1lOiAncm9sZS10YWJsZS1yb3cnLFxyXG4gICAgcHJvcHM6IHtcclxuICAgICAgaW5kZXg6IHtcclxuICAgICAgICBkZWZhdWx0OiB7fSxcclxuICAgICAgICB0eXBlOiBOdW1iZXJcclxuICAgICAgfSxcclxuICAgICAgb2JqRW50aXR5OiB7XHJcbiAgICAgICAgZGVmYXVsdDoge30sXHJcbiAgICAgICAgdHlwZTogT2JqZWN0XHJcbiAgICAgIH0sXHJcbiAgICB9LFxyXG4gICAgY29tcG9uZW50czoge30sXHJcbiAgICBkYXRhKCkge1xyXG4gICAgICByZXR1cm4ge1xyXG4gICAgICAgIHVybDoge1xyXG4gICAgICAgICAgcmVzb3VyY2VVUkw6IENEQVRBLnJlc291cmNlVVJMXHJcbiAgICAgICAgfSxcclxuICAgICAgICBmbGFnOiB7XHJcbiAgICAgICAgICBpc0RlbGV0aW5nOiBmYWxzZVxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfSxcclxuICAgIGNvbXB1dGVkOiB7XHJcbiAgICAgIHByb2ZpbGVJbWFnZSgpIHtcclxuICAgICAgICBpZiAoX2lzRW1wdHlTdHJpbmcodGhpcy5vYmpFbnRpdHkucHJvZmlsZV9pbWFnZSkpXHJcbiAgICAgICAgICByZXR1cm4gdGhpcy51cmwucmVzb3VyY2VVUkwgKyAnL2dsb2JhbC9pbWFnZS9ub3RGb3VuZC5wbmcnXHJcblxyXG4gICAgICAgIHJldHVybiB0aGlzLm9iakVudGl0eS5wcm9maWxlX2ltYWdlXHJcbiAgICAgIH0sXHJcbiAgICB9LFxyXG4gICAgY3JlYXRlZCgpIHtcclxuICAgIH0sXHJcbiAgICBtZXRob2RzOiB7XHJcbiAgICAgIGdvVXBkYXRlKCkge1xyXG4gICAgICAgIHRoaXMuJHN0b3JlLmRpc3BhdGNoKCdzZXRFbnRpdHlBY3QnLCB7IGluZGV4OiB0aGlzLmluZGV4IH0pXHJcblxyXG4gICAgICAgIHRoaXMuJHJvdXRlci5wdXNoKHsgbmFtZTogJ3JvbGU6ZWRpdCcsIHBhcmFtczogeyBpZDogdGhpcy5vYmpFbnRpdHkucm9sZV9pZCwgcGFnZTogMSB9IH0pXHJcbiAgICAgIH0sXHJcbiAgICAgIHJlbW92ZShpZCkge1xyXG4gICAgICAgIGlmICghaWQpXHJcbiAgICAgICAgICByZXR1cm5cclxuXHJcbiAgICAgICAgdGhpcy5mbGFnLmlzRGVsZXRpbmcgPSB0cnVlXHJcbiAgICAgICAgdGhpcy4kc3RvcmUuZGlzcGF0Y2goJ2RlbGV0ZUVudGl0eUFjdCcsIHsgaWQ6IGlkIH0pLnRoZW4ocmVzID0+IHtcclxuICAgICAgICAgIHRoaXMuZmxhZy5pc0RlbGV0aW5nID0gZmFsc2VcclxuICAgICAgICB9KVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG48L3NjcmlwdD5cclxuXHJcbiIsIi8qKlxuICAqIHZlZS12YWxpZGF0ZSB2My4wLjExXG4gICogKGMpIDIwMTkgQWJkZWxyYWhtYW4gQXdhZFxuICAqIEBsaWNlbnNlIE1JVFxuICAqL1xuLyoqXHJcbiAqIFNvbWUgQWxwaGEgUmVnZXggaGVscGVycy5cclxuICogaHR0cHM6Ly9naXRodWIuY29tL2Nocmlzby92YWxpZGF0b3IuanMvYmxvYi9tYXN0ZXIvc3JjL2xpYi9hbHBoYS5qc1xyXG4gKi9cclxudmFyIGFscGhhID0ge1xyXG4gICAgZW46IC9eW0EtWl0qJC9pLFxyXG4gICAgY3M6IC9eW0EtWsOBxIzEjsOJxJrDjcWHw5PFmMWgxaTDmsWuw53FvV0qJC9pLFxyXG4gICAgZGE6IC9eW0EtWsOGw5jDhV0qJC9pLFxyXG4gICAgZGU6IC9eW0EtWsOEw5bDnMOfXSokL2ksXHJcbiAgICBlczogL15bQS1aw4HDicONw5HDk8Oaw5xdKiQvaSxcclxuICAgIGZyOiAvXltBLVrDgMOCw4bDh8OJw4jDisOLw4/DjsOUxZLDmcObw5zFuF0qJC9pLFxyXG4gICAgaXQ6IC9eW0EtWlxceEMwLVxceEZGXSokL2ksXHJcbiAgICBsdDogL15bQS1axITEjMSYxJbErsWgxbLFqsW9XSokL2ksXHJcbiAgICBubDogL15bQS1aw4nDi8OPw5PDlsOcXSokL2ksXHJcbiAgICBodTogL15bQS1aw4HDicONw5PDlsWQw5rDnMWwXSokL2ksXHJcbiAgICBwbDogL15bQS1axITEhsSYxZrFgcWDw5PFu8W5XSokL2ksXHJcbiAgICBwdDogL15bQS1aw4PDgcOAw4LDh8OJw4rDjcOVw5PDlMOaw5xdKiQvaSxcclxuICAgIHJ1OiAvXlvQkC3Qr9CBXSokL2ksXHJcbiAgICBzazogL15bQS1aw4HDhMSMxI7DicONxLnEvcWHw5PFlMWgxaTDmsOdxb1dKiQvaSxcclxuICAgIHNyOiAvXltBLVrEjMSGxb3FoMSQXSokL2ksXHJcbiAgICBzdjogL15bQS1aw4XDhMOWXSokL2ksXHJcbiAgICB0cjogL15bQS1aw4fEnsSwxLHDlsWew5xdKiQvaSxcclxuICAgIHVrOiAvXlvQkC3QqdCs0K7Qr9CE0IbQh9KQXSokL2ksXHJcbiAgICBhcjogL15b2KHYotij2KTYpdim2KfYqNip2KrYq9is2K3Yrtiv2LDYsdiy2LPYtNi12LbYt9i42LnYutmB2YLZg9mE2YXZhtmH2YjZidmK2YvZjNmN2Y7Zj9mQ2ZHZktmwXSokLyxcclxuICAgIGF6OiAvXltBLVrDh8aPxJ7EsMSxw5bFnsOcXSokL2lcclxufTtcclxudmFyIGFscGhhU3BhY2VzID0ge1xyXG4gICAgZW46IC9eW0EtWlxcc10qJC9pLFxyXG4gICAgY3M6IC9eW0EtWsOBxIzEjsOJxJrDjcWHw5PFmMWgxaTDmsWuw53FvVxcc10qJC9pLFxyXG4gICAgZGE6IC9eW0EtWsOGw5jDhVxcc10qJC9pLFxyXG4gICAgZGU6IC9eW0EtWsOEw5bDnMOfXFxzXSokL2ksXHJcbiAgICBlczogL15bQS1aw4HDicONw5HDk8Oaw5xcXHNdKiQvaSxcclxuICAgIGZyOiAvXltBLVrDgMOCw4bDh8OJw4jDisOLw4/DjsOUxZLDmcObw5zFuFxcc10qJC9pLFxyXG4gICAgaXQ6IC9eW0EtWlxceEMwLVxceEZGXFxzXSokL2ksXHJcbiAgICBsdDogL15bQS1axITEjMSYxJbErsWgxbLFqsW9XFxzXSokL2ksXHJcbiAgICBubDogL15bQS1aw4nDi8OPw5PDlsOcXFxzXSokL2ksXHJcbiAgICBodTogL15bQS1aw4HDicONw5PDlsWQw5rDnMWwXFxzXSokL2ksXHJcbiAgICBwbDogL15bQS1axITEhsSYxZrFgcWDw5PFu8W5XFxzXSokL2ksXHJcbiAgICBwdDogL15bQS1aw4PDgcOAw4LDh8OJw4rDjcOVw5PDlMOaw5xcXHNdKiQvaSxcclxuICAgIHJ1OiAvXlvQkC3Qr9CBXFxzXSokL2ksXHJcbiAgICBzazogL15bQS1aw4HDhMSMxI7DicONxLnEvcWHw5PFlMWgxaTDmsOdxb1cXHNdKiQvaSxcclxuICAgIHNyOiAvXltBLVrEjMSGxb3FoMSQXFxzXSokL2ksXHJcbiAgICBzdjogL15bQS1aw4XDhMOWXFxzXSokL2ksXHJcbiAgICB0cjogL15bQS1aw4fEnsSwxLHDlsWew5xcXHNdKiQvaSxcclxuICAgIHVrOiAvXlvQkC3QqdCs0K7Qr9CE0IbQh9KQXFxzXSokL2ksXHJcbiAgICBhcjogL15b2KHYotij2KTYpdim2KfYqNip2KrYq9is2K3Yrtiv2LDYsdiy2LPYtNi12LbYt9i42LnYutmB2YLZg9mE2YXZhtmH2YjZidmK2YvZjNmN2Y7Zj9mQ2ZHZktmwXFxzXSokLyxcclxuICAgIGF6OiAvXltBLVrDh8aPxJ7EsMSxw5bFnsOcXFxzXSokL2lcclxufTtcclxudmFyIGFscGhhbnVtZXJpYyA9IHtcclxuICAgIGVuOiAvXlswLTlBLVpdKiQvaSxcclxuICAgIGNzOiAvXlswLTlBLVrDgcSMxI7DicSaw43Fh8OTxZjFoMWkw5rFrsOdxb1dKiQvaSxcclxuICAgIGRhOiAvXlswLTlBLVrDhsOYw4VdJC9pLFxyXG4gICAgZGU6IC9eWzAtOUEtWsOEw5bDnMOfXSokL2ksXHJcbiAgICBlczogL15bMC05QS1aw4HDicONw5HDk8Oaw5xdKiQvaSxcclxuICAgIGZyOiAvXlswLTlBLVrDgMOCw4bDh8OJw4jDisOLw4/DjsOUxZLDmcObw5zFuF0qJC9pLFxyXG4gICAgaXQ6IC9eWzAtOUEtWlxceEMwLVxceEZGXSokL2ksXHJcbiAgICBsdDogL15bMC05QS1axITEjMSYxJbErsWgxbLFqsW9XSokL2ksXHJcbiAgICBodTogL15bMC05QS1aw4HDicONw5PDlsWQw5rDnMWwXSokL2ksXHJcbiAgICBubDogL15bMC05QS1aw4nDi8OPw5PDlsOcXSokL2ksXHJcbiAgICBwbDogL15bMC05QS1axITEhsSYxZrFgcWDw5PFu8W5XSokL2ksXHJcbiAgICBwdDogL15bMC05QS1aw4PDgcOAw4LDh8OJw4rDjcOVw5PDlMOaw5xdKiQvaSxcclxuICAgIHJ1OiAvXlswLTnQkC3Qr9CBXSokL2ksXHJcbiAgICBzazogL15bMC05QS1aw4HDhMSMxI7DicONxLnEvcWHw5PFlMWgxaTDmsOdxb1dKiQvaSxcclxuICAgIHNyOiAvXlswLTlBLVrEjMSGxb3FoMSQXSokL2ksXHJcbiAgICBzdjogL15bMC05QS1aw4XDhMOWXSokL2ksXHJcbiAgICB0cjogL15bMC05QS1aw4fEnsSwxLHDlsWew5xdKiQvaSxcclxuICAgIHVrOiAvXlswLTnQkC3QqdCs0K7Qr9CE0IbQh9KQXSokL2ksXHJcbiAgICBhcjogL15b2aDZodmi2aPZpNml2abZp9mo2akwLTnYodii2KPYpNil2KbYp9io2KnYqtir2KzYrdiu2K/YsNix2LLYs9i02LXYtti32LjYudi62YHZgtmD2YTZhdmG2YfZiNmJ2YrZi9mM2Y3ZjtmP2ZDZkdmS2bBdKiQvLFxyXG4gICAgYXo6IC9eWzAtOUEtWsOHxo/EnsSwxLHDlsWew5xdKiQvaVxyXG59O1xyXG52YXIgYWxwaGFEYXNoID0ge1xyXG4gICAgZW46IC9eWzAtOUEtWl8tXSokL2ksXHJcbiAgICBjczogL15bMC05QS1aw4HEjMSOw4nEmsONxYfDk8WYxaDFpMOaxa7DncW9Xy1dKiQvaSxcclxuICAgIGRhOiAvXlswLTlBLVrDhsOYw4VfLV0qJC9pLFxyXG4gICAgZGU6IC9eWzAtOUEtWsOEw5bDnMOfXy1dKiQvaSxcclxuICAgIGVzOiAvXlswLTlBLVrDgcOJw43DkcOTw5rDnF8tXSokL2ksXHJcbiAgICBmcjogL15bMC05QS1aw4DDgsOGw4fDicOIw4rDi8OPw47DlMWSw5nDm8OcxbhfLV0qJC9pLFxyXG4gICAgaXQ6IC9eWzAtOUEtWlxceEMwLVxceEZGXy1dKiQvaSxcclxuICAgIGx0OiAvXlswLTlBLVrEhMSMxJjElsSuxaDFssWqxb1fLV0qJC9pLFxyXG4gICAgbmw6IC9eWzAtOUEtWsOJw4vDj8OTw5bDnF8tXSokL2ksXHJcbiAgICBodTogL15bMC05QS1aw4HDicONw5PDlsWQw5rDnMWwXy1dKiQvaSxcclxuICAgIHBsOiAvXlswLTlBLVrEhMSGxJjFmsWBxYPDk8W7xblfLV0qJC9pLFxyXG4gICAgcHQ6IC9eWzAtOUEtWsODw4HDgMOCw4fDicOKw43DlcOTw5TDmsOcXy1dKiQvaSxcclxuICAgIHJ1OiAvXlswLTnQkC3Qr9CBXy1dKiQvaSxcclxuICAgIHNrOiAvXlswLTlBLVrDgcOExIzEjsOJw43EucS9xYfDk8WUxaDFpMOaw53FvV8tXSokL2ksXHJcbiAgICBzcjogL15bMC05QS1axIzEhsW9xaDEkF8tXSokL2ksXHJcbiAgICBzdjogL15bMC05QS1aw4XDhMOWXy1dKiQvaSxcclxuICAgIHRyOiAvXlswLTlBLVrDh8SexLDEscOWxZ7DnF8tXSokL2ksXHJcbiAgICB1azogL15bMC050JAt0KnQrNCu0K/QhNCG0IfSkF8tXSokL2ksXHJcbiAgICBhcjogL15b2aDZodmi2aPZpNml2abZp9mo2akwLTnYodii2KPYpNil2KbYp9io2KnYqtir2KzYrdiu2K/YsNix2LLYs9i02LXYtti32LjYudi62YHZgtmD2YTZhdmG2YfZiNmJ2YrZi9mM2Y3ZjtmP2ZDZkdmS2bBfLV0qJC8sXHJcbiAgICBhejogL15bMC05QS1aw4fGj8SexLDEscOWxZ7DnF8tXSokL2lcclxufTtcblxudmFyIHZhbGlkYXRlID0gZnVuY3Rpb24gKHZhbHVlLCBfYSkge1xyXG4gICAgdmFyIF9iID0gKF9hID09PSB2b2lkIDAgPyB7fSA6IF9hKS5sb2NhbGUsIGxvY2FsZSA9IF9iID09PSB2b2lkIDAgPyAnJyA6IF9iO1xyXG4gICAgaWYgKEFycmF5LmlzQXJyYXkodmFsdWUpKSB7XHJcbiAgICAgICAgcmV0dXJuIHZhbHVlLmV2ZXJ5KGZ1bmN0aW9uICh2YWwpIHsgcmV0dXJuIHZhbGlkYXRlKHZhbCwgeyBsb2NhbGU6IGxvY2FsZSB9KTsgfSk7XHJcbiAgICB9XHJcbiAgICAvLyBNYXRjaCBhdCBsZWFzdCBvbmUgbG9jYWxlLlxyXG4gICAgaWYgKCFsb2NhbGUpIHtcclxuICAgICAgICByZXR1cm4gT2JqZWN0LmtleXMoYWxwaGEpLnNvbWUoZnVuY3Rpb24gKGxvYykgeyByZXR1cm4gYWxwaGFbbG9jXS50ZXN0KHZhbHVlKTsgfSk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gKGFscGhhW2xvY2FsZV0gfHwgYWxwaGEuZW4pLnRlc3QodmFsdWUpO1xyXG59O1xyXG52YXIgcGFyYW1zID0gW1xyXG4gICAge1xyXG4gICAgICAgIG5hbWU6ICdsb2NhbGUnXHJcbiAgICB9XHJcbl07XHJcbnZhciBhbHBoYSQxID0ge1xyXG4gICAgdmFsaWRhdGU6IHZhbGlkYXRlLFxyXG4gICAgcGFyYW1zOiBwYXJhbXNcclxufTtcblxudmFyIHZhbGlkYXRlJDEgPSBmdW5jdGlvbiAodmFsdWUsIF9hKSB7XHJcbiAgICB2YXIgX2IgPSAoX2EgPT09IHZvaWQgMCA/IHt9IDogX2EpLmxvY2FsZSwgbG9jYWxlID0gX2IgPT09IHZvaWQgMCA/ICcnIDogX2I7XHJcbiAgICBpZiAoQXJyYXkuaXNBcnJheSh2YWx1ZSkpIHtcclxuICAgICAgICByZXR1cm4gdmFsdWUuZXZlcnkoZnVuY3Rpb24gKHZhbCkgeyByZXR1cm4gdmFsaWRhdGUkMSh2YWwsIHsgbG9jYWxlOiBsb2NhbGUgfSk7IH0pO1xyXG4gICAgfVxyXG4gICAgLy8gTWF0Y2ggYXQgbGVhc3Qgb25lIGxvY2FsZS5cclxuICAgIGlmICghbG9jYWxlKSB7XHJcbiAgICAgICAgcmV0dXJuIE9iamVjdC5rZXlzKGFscGhhRGFzaCkuc29tZShmdW5jdGlvbiAobG9jKSB7IHJldHVybiBhbHBoYURhc2hbbG9jXS50ZXN0KHZhbHVlKTsgfSk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gKGFscGhhRGFzaFtsb2NhbGVdIHx8IGFscGhhRGFzaC5lbikudGVzdCh2YWx1ZSk7XHJcbn07XHJcbnZhciBwYXJhbXMkMSA9IFtcclxuICAgIHtcclxuICAgICAgICBuYW1lOiAnbG9jYWxlJ1xyXG4gICAgfVxyXG5dO1xyXG52YXIgYWxwaGFfZGFzaCA9IHtcclxuICAgIHZhbGlkYXRlOiB2YWxpZGF0ZSQxLFxyXG4gICAgcGFyYW1zOiBwYXJhbXMkMVxyXG59O1xuXG52YXIgdmFsaWRhdGUkMiA9IGZ1bmN0aW9uICh2YWx1ZSwgX2EpIHtcclxuICAgIHZhciBfYiA9IChfYSA9PT0gdm9pZCAwID8ge30gOiBfYSkubG9jYWxlLCBsb2NhbGUgPSBfYiA9PT0gdm9pZCAwID8gJycgOiBfYjtcclxuICAgIGlmIChBcnJheS5pc0FycmF5KHZhbHVlKSkge1xyXG4gICAgICAgIHJldHVybiB2YWx1ZS5ldmVyeShmdW5jdGlvbiAodmFsKSB7IHJldHVybiB2YWxpZGF0ZSQyKHZhbCwgeyBsb2NhbGU6IGxvY2FsZSB9KTsgfSk7XHJcbiAgICB9XHJcbiAgICAvLyBNYXRjaCBhdCBsZWFzdCBvbmUgbG9jYWxlLlxyXG4gICAgaWYgKCFsb2NhbGUpIHtcclxuICAgICAgICByZXR1cm4gT2JqZWN0LmtleXMoYWxwaGFudW1lcmljKS5zb21lKGZ1bmN0aW9uIChsb2MpIHsgcmV0dXJuIGFscGhhbnVtZXJpY1tsb2NdLnRlc3QodmFsdWUpOyB9KTtcclxuICAgIH1cclxuICAgIHJldHVybiAoYWxwaGFudW1lcmljW2xvY2FsZV0gfHwgYWxwaGFudW1lcmljLmVuKS50ZXN0KHZhbHVlKTtcclxufTtcclxudmFyIHBhcmFtcyQyID0gW1xyXG4gICAge1xyXG4gICAgICAgIG5hbWU6ICdsb2NhbGUnXHJcbiAgICB9XHJcbl07XHJcbnZhciBhbHBoYV9udW0gPSB7XHJcbiAgICB2YWxpZGF0ZTogdmFsaWRhdGUkMixcclxuICAgIHBhcmFtczogcGFyYW1zJDJcclxufTtcblxudmFyIHZhbGlkYXRlJDMgPSBmdW5jdGlvbiAodmFsdWUsIF9hKSB7XHJcbiAgICB2YXIgX2IgPSAoX2EgPT09IHZvaWQgMCA/IHt9IDogX2EpLmxvY2FsZSwgbG9jYWxlID0gX2IgPT09IHZvaWQgMCA/ICcnIDogX2I7XHJcbiAgICBpZiAoQXJyYXkuaXNBcnJheSh2YWx1ZSkpIHtcclxuICAgICAgICByZXR1cm4gdmFsdWUuZXZlcnkoZnVuY3Rpb24gKHZhbCkgeyByZXR1cm4gdmFsaWRhdGUkMyh2YWwsIHsgbG9jYWxlOiBsb2NhbGUgfSk7IH0pO1xyXG4gICAgfVxyXG4gICAgLy8gTWF0Y2ggYXQgbGVhc3Qgb25lIGxvY2FsZS5cclxuICAgIGlmICghbG9jYWxlKSB7XHJcbiAgICAgICAgcmV0dXJuIE9iamVjdC5rZXlzKGFscGhhU3BhY2VzKS5zb21lKGZ1bmN0aW9uIChsb2MpIHsgcmV0dXJuIGFscGhhU3BhY2VzW2xvY10udGVzdCh2YWx1ZSk7IH0pO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIChhbHBoYVNwYWNlc1tsb2NhbGVdIHx8IGFscGhhU3BhY2VzLmVuKS50ZXN0KHZhbHVlKTtcclxufTtcclxudmFyIHBhcmFtcyQzID0gW1xyXG4gICAge1xyXG4gICAgICAgIG5hbWU6ICdsb2NhbGUnXHJcbiAgICB9XHJcbl07XHJcbnZhciBhbHBoYV9zcGFjZXMgPSB7XHJcbiAgICB2YWxpZGF0ZTogdmFsaWRhdGUkMyxcclxuICAgIHBhcmFtczogcGFyYW1zJDNcclxufTtcblxudmFyIHZhbGlkYXRlJDQgPSBmdW5jdGlvbiAodmFsdWUsIF9hKSB7XHJcbiAgICB2YXIgX2IgPSBfYSA9PT0gdm9pZCAwID8ge30gOiBfYSwgbWluID0gX2IubWluLCBtYXggPSBfYi5tYXg7XHJcbiAgICBpZiAoQXJyYXkuaXNBcnJheSh2YWx1ZSkpIHtcclxuICAgICAgICByZXR1cm4gdmFsdWUuZXZlcnkoZnVuY3Rpb24gKHZhbCkgeyByZXR1cm4gISF2YWxpZGF0ZSQ0KHZhbCwgeyBtaW46IG1pbiwgbWF4OiBtYXggfSk7IH0pO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIE51bWJlcihtaW4pIDw9IHZhbHVlICYmIE51bWJlcihtYXgpID49IHZhbHVlO1xyXG59O1xyXG52YXIgcGFyYW1zJDQgPSBbXHJcbiAgICB7XHJcbiAgICAgICAgbmFtZTogJ21pbidcclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgICAgbmFtZTogJ21heCdcclxuICAgIH1cclxuXTtcclxudmFyIGJldHdlZW4gPSB7XHJcbiAgICB2YWxpZGF0ZTogdmFsaWRhdGUkNCxcclxuICAgIHBhcmFtczogcGFyYW1zJDRcclxufTtcblxudmFyIHZhbGlkYXRlJDUgPSBmdW5jdGlvbiAodmFsdWUsIF9hKSB7XHJcbiAgICB2YXIgdGFyZ2V0ID0gX2EudGFyZ2V0O1xyXG4gICAgcmV0dXJuIFN0cmluZyh2YWx1ZSkgPT09IFN0cmluZyh0YXJnZXQpO1xyXG59O1xyXG52YXIgcGFyYW1zJDUgPSBbXHJcbiAgICB7XHJcbiAgICAgICAgbmFtZTogJ3RhcmdldCcsXHJcbiAgICAgICAgaXNUYXJnZXQ6IHRydWVcclxuICAgIH1cclxuXTtcclxudmFyIGNvbmZpcm1lZCA9IHtcclxuICAgIHZhbGlkYXRlOiB2YWxpZGF0ZSQ1LFxyXG4gICAgcGFyYW1zOiBwYXJhbXMkNVxyXG59O1xuXG52YXIgdmFsaWRhdGUkNiA9IGZ1bmN0aW9uICh2YWx1ZSwgX2EpIHtcclxuICAgIHZhciBsZW5ndGggPSBfYS5sZW5ndGg7XHJcbiAgICBpZiAoQXJyYXkuaXNBcnJheSh2YWx1ZSkpIHtcclxuICAgICAgICByZXR1cm4gdmFsdWUuZXZlcnkoZnVuY3Rpb24gKHZhbCkgeyByZXR1cm4gdmFsaWRhdGUkNih2YWwsIHsgbGVuZ3RoOiBsZW5ndGggfSk7IH0pO1xyXG4gICAgfVxyXG4gICAgdmFyIHN0clZhbCA9IFN0cmluZyh2YWx1ZSk7XHJcbiAgICByZXR1cm4gL15bMC05XSokLy50ZXN0KHN0clZhbCkgJiYgc3RyVmFsLmxlbmd0aCA9PT0gbGVuZ3RoO1xyXG59O1xyXG52YXIgcGFyYW1zJDYgPSBbXHJcbiAgICB7XHJcbiAgICAgICAgbmFtZTogJ2xlbmd0aCcsXHJcbiAgICAgICAgY2FzdDogZnVuY3Rpb24gKHZhbHVlKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBOdW1iZXIodmFsdWUpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXTtcclxudmFyIGRpZ2l0cyA9IHtcclxuICAgIHZhbGlkYXRlOiB2YWxpZGF0ZSQ2LFxyXG4gICAgcGFyYW1zOiBwYXJhbXMkNlxyXG59O1xuXG52YXIgdmFsaWRhdGVJbWFnZSA9IGZ1bmN0aW9uIChmaWxlLCB3aWR0aCwgaGVpZ2h0KSB7XHJcbiAgICB2YXIgVVJMID0gd2luZG93LlVSTCB8fCB3aW5kb3cud2Via2l0VVJMO1xyXG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKGZ1bmN0aW9uIChyZXNvbHZlKSB7XHJcbiAgICAgICAgdmFyIGltYWdlID0gbmV3IEltYWdlKCk7XHJcbiAgICAgICAgaW1hZ2Uub25lcnJvciA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIHJlc29sdmUoZmFsc2UpOyB9O1xyXG4gICAgICAgIGltYWdlLm9ubG9hZCA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIHJlc29sdmUoaW1hZ2Uud2lkdGggPT09IHdpZHRoICYmIGltYWdlLmhlaWdodCA9PT0gaGVpZ2h0KTsgfTtcclxuICAgICAgICBpbWFnZS5zcmMgPSBVUkwuY3JlYXRlT2JqZWN0VVJMKGZpbGUpO1xyXG4gICAgfSk7XHJcbn07XHJcbnZhciB2YWxpZGF0ZSQ3ID0gZnVuY3Rpb24gKGZpbGVzLCBfYSkge1xyXG4gICAgdmFyIHdpZHRoID0gX2Eud2lkdGgsIGhlaWdodCA9IF9hLmhlaWdodDtcclxuICAgIHZhciBsaXN0ID0gW107XHJcbiAgICBmaWxlcyA9IEFycmF5LmlzQXJyYXkoZmlsZXMpID8gZmlsZXMgOiBbZmlsZXNdO1xyXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBmaWxlcy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgIC8vIGlmIGZpbGUgaXMgbm90IGFuIGltYWdlLCByZWplY3QuXHJcbiAgICAgICAgaWYgKCEvXFwuKGpwZ3xzdmd8anBlZ3xwbmd8Ym1wfGdpZikkL2kudGVzdChmaWxlc1tpXS5uYW1lKSkge1xyXG4gICAgICAgICAgICByZXR1cm4gUHJvbWlzZS5yZXNvbHZlKGZhbHNlKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGlzdC5wdXNoKGZpbGVzW2ldKTtcclxuICAgIH1cclxuICAgIHJldHVybiBQcm9taXNlLmFsbChsaXN0Lm1hcChmdW5jdGlvbiAoZmlsZSkgeyByZXR1cm4gdmFsaWRhdGVJbWFnZShmaWxlLCB3aWR0aCwgaGVpZ2h0KTsgfSkpLnRoZW4oZnVuY3Rpb24gKHZhbHVlcykge1xyXG4gICAgICAgIHJldHVybiB2YWx1ZXMuZXZlcnkoZnVuY3Rpb24gKHYpIHsgcmV0dXJuIHY7IH0pO1xyXG4gICAgfSk7XHJcbn07XHJcbnZhciBwYXJhbXMkNyA9IFtcclxuICAgIHtcclxuICAgICAgICBuYW1lOiAnd2lkdGgnLFxyXG4gICAgICAgIGNhc3Q6IGZ1bmN0aW9uICh2YWx1ZSkge1xyXG4gICAgICAgICAgICByZXR1cm4gTnVtYmVyKHZhbHVlKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICAgIG5hbWU6ICdoZWlnaHQnLFxyXG4gICAgICAgIGNhc3Q6IGZ1bmN0aW9uICh2YWx1ZSkge1xyXG4gICAgICAgICAgICByZXR1cm4gTnVtYmVyKHZhbHVlKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbl07XHJcbnZhciBkaW1lbnNpb25zID0ge1xyXG4gICAgdmFsaWRhdGU6IHZhbGlkYXRlJDcsXHJcbiAgICBwYXJhbXM6IHBhcmFtcyQ3XHJcbn07XG5cbnZhciB2YWxpZGF0ZSQ4ID0gZnVuY3Rpb24gKHZhbHVlLCBfYSkge1xyXG4gICAgdmFyIG11bHRpcGxlID0gKF9hID09PSB2b2lkIDAgPyB7fSA6IF9hKS5tdWx0aXBsZTtcclxuICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZVxyXG4gICAgdmFyIHJlID0gL14oKFtePD4oKVxcW1xcXVxcXFwuLDs6XFxzQFwiXSsoXFwuW148PigpXFxbXFxdXFxcXC4sOzpcXHNAXCJdKykqKXwoXCIuK1wiKSlAKChcXFtbMC05XXsxLDN9XFwuWzAtOV17MSwzfVxcLlswLTldezEsM31cXC5bMC05XXsxLDN9XFxdKXwoKFthLXpBLVpcXC0wLTldK1xcLikrW2EtekEtWl17Mix9KSkkLztcclxuICAgIGlmIChtdWx0aXBsZSAmJiAhQXJyYXkuaXNBcnJheSh2YWx1ZSkpIHtcclxuICAgICAgICB2YWx1ZSA9IFN0cmluZyh2YWx1ZSlcclxuICAgICAgICAgICAgLnNwbGl0KCcsJylcclxuICAgICAgICAgICAgLm1hcChmdW5jdGlvbiAoZW1haWxTdHIpIHsgcmV0dXJuIGVtYWlsU3RyLnRyaW0oKTsgfSk7XHJcbiAgICB9XHJcbiAgICBpZiAoQXJyYXkuaXNBcnJheSh2YWx1ZSkpIHtcclxuICAgICAgICByZXR1cm4gdmFsdWUuZXZlcnkoZnVuY3Rpb24gKHZhbCkgeyByZXR1cm4gcmUudGVzdChTdHJpbmcodmFsKSk7IH0pO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHJlLnRlc3QoU3RyaW5nKHZhbHVlKSk7XHJcbn07XHJcbnZhciBwYXJhbXMkOCA9IFtcclxuICAgIHtcclxuICAgICAgICBuYW1lOiAnbXVsdGlwbGUnLFxyXG4gICAgICAgIGRlZmF1bHQ6IGZhbHNlXHJcbiAgICB9XHJcbl07XHJcbnZhciBlbWFpbCA9IHtcclxuICAgIHZhbGlkYXRlOiB2YWxpZGF0ZSQ4LFxyXG4gICAgcGFyYW1zOiBwYXJhbXMkOFxyXG59O1xuXG4vKipcclxuICogQ2hlY2tzIGlmIHRoZSB2YWx1ZXMgYXJlIGVpdGhlciBudWxsIG9yIHVuZGVmaW5lZC5cclxuICovXHJcbnZhciBpc051bGxPclVuZGVmaW5lZCA9IGZ1bmN0aW9uICh2YWx1ZSkge1xyXG4gICAgcmV0dXJuIHZhbHVlID09PSBudWxsIHx8IHZhbHVlID09PSB1bmRlZmluZWQ7XHJcbn07XHJcbnZhciBpbmNsdWRlcyA9IGZ1bmN0aW9uIChjb2xsZWN0aW9uLCBpdGVtKSB7XHJcbiAgICByZXR1cm4gY29sbGVjdGlvbi5pbmRleE9mKGl0ZW0pICE9PSAtMTtcclxufTtcclxuLyoqXHJcbiAqIENoZWNrcyBpZiBhIGZ1bmN0aW9uIGlzIGNhbGxhYmxlLlxyXG4gKi9cclxudmFyIGlzQ2FsbGFibGUgPSBmdW5jdGlvbiAoZnVuYykgeyByZXR1cm4gdHlwZW9mIGZ1bmMgPT09ICdmdW5jdGlvbic7IH07XHJcbi8qIGlzdGFuYnVsIGlnbm9yZSBuZXh0ICovXHJcbmZ1bmN0aW9uIF9jb3B5QXJyYXkoYXJyYXlMaWtlKSB7XHJcbiAgICB2YXIgYXJyYXkgPSBbXTtcclxuICAgIHZhciBsZW5ndGggPSBhcnJheUxpa2UubGVuZ3RoO1xyXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBsZW5ndGg7IGkrKykge1xyXG4gICAgICAgIGFycmF5LnB1c2goYXJyYXlMaWtlW2ldKTtcclxuICAgIH1cclxuICAgIHJldHVybiBhcnJheTtcclxufVxyXG4vKipcclxuICogQ29udmVydHMgYW4gYXJyYXktbGlrZSBvYmplY3QgdG8gYXJyYXksIHByb3ZpZGVzIGEgc2ltcGxlIHBvbHlmaWxsIGZvciBBcnJheS5mcm9tXHJcbiAqL1xyXG5mdW5jdGlvbiB0b0FycmF5KGFycmF5TGlrZSkge1xyXG4gICAgaWYgKGlzQ2FsbGFibGUoQXJyYXkuZnJvbSkpIHtcclxuICAgICAgICByZXR1cm4gQXJyYXkuZnJvbShhcnJheUxpa2UpO1xyXG4gICAgfVxyXG4gICAgLyogaXN0YW5idWwgaWdub3JlIG5leHQgKi9cclxuICAgIHJldHVybiBfY29weUFycmF5KGFycmF5TGlrZSk7XHJcbn1cclxudmFyIGlzRW1wdHlBcnJheSA9IGZ1bmN0aW9uIChhcnIpIHtcclxuICAgIHJldHVybiBBcnJheS5pc0FycmF5KGFycikgJiYgYXJyLmxlbmd0aCA9PT0gMDtcclxufTtcblxudmFyIHZhbGlkYXRlJDkgPSBmdW5jdGlvbiAodmFsdWUsIG9wdGlvbnMpIHtcclxuICAgIGlmIChBcnJheS5pc0FycmF5KHZhbHVlKSkge1xyXG4gICAgICAgIHJldHVybiB2YWx1ZS5ldmVyeShmdW5jdGlvbiAodmFsKSB7IHJldHVybiB2YWxpZGF0ZSQ5KHZhbCwgb3B0aW9ucyk7IH0pO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHRvQXJyYXkob3B0aW9ucykuc29tZShmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZVxyXG4gICAgICAgIHJldHVybiBpdGVtID09IHZhbHVlO1xyXG4gICAgfSk7XHJcbn07XHJcbnZhciBvbmVPZiA9IHtcclxuICAgIHZhbGlkYXRlOiB2YWxpZGF0ZSQ5XHJcbn07XG5cbnZhciB2YWxpZGF0ZSRhID0gZnVuY3Rpb24gKHZhbHVlLCBhcmdzKSB7XHJcbiAgICByZXR1cm4gIXZhbGlkYXRlJDkodmFsdWUsIGFyZ3MpO1xyXG59O1xyXG52YXIgZXhjbHVkZWQgPSB7XHJcbiAgICB2YWxpZGF0ZTogdmFsaWRhdGUkYVxyXG59O1xuXG52YXIgdmFsaWRhdGUkYiA9IGZ1bmN0aW9uIChmaWxlcywgZXh0ZW5zaW9ucykge1xyXG4gICAgdmFyIHJlZ2V4ID0gbmV3IFJlZ0V4cChcIi4oXCIgKyBleHRlbnNpb25zLmpvaW4oJ3wnKSArIFwiKSRcIiwgJ2knKTtcclxuICAgIGlmIChBcnJheS5pc0FycmF5KGZpbGVzKSkge1xyXG4gICAgICAgIHJldHVybiBmaWxlcy5ldmVyeShmdW5jdGlvbiAoZmlsZSkgeyByZXR1cm4gcmVnZXgudGVzdChmaWxlLm5hbWUpOyB9KTtcclxuICAgIH1cclxuICAgIHJldHVybiByZWdleC50ZXN0KGZpbGVzLm5hbWUpO1xyXG59O1xyXG52YXIgZXh0ID0ge1xyXG4gICAgdmFsaWRhdGU6IHZhbGlkYXRlJGJcclxufTtcblxudmFyIHZhbGlkYXRlJGMgPSBmdW5jdGlvbiAoZmlsZXMpIHtcclxuICAgIHZhciByZWdleCA9IC9cXC4oanBnfHN2Z3xqcGVnfHBuZ3xibXB8Z2lmKSQvaTtcclxuICAgIGlmIChBcnJheS5pc0FycmF5KGZpbGVzKSkge1xyXG4gICAgICAgIHJldHVybiBmaWxlcy5ldmVyeShmdW5jdGlvbiAoZmlsZSkgeyByZXR1cm4gcmVnZXgudGVzdChmaWxlLm5hbWUpOyB9KTtcclxuICAgIH1cclxuICAgIHJldHVybiByZWdleC50ZXN0KGZpbGVzLm5hbWUpO1xyXG59O1xyXG52YXIgaW1hZ2UgPSB7XHJcbiAgICB2YWxpZGF0ZTogdmFsaWRhdGUkY1xyXG59O1xuXG52YXIgdmFsaWRhdGUkZCA9IGZ1bmN0aW9uICh2YWx1ZSkge1xyXG4gICAgaWYgKEFycmF5LmlzQXJyYXkodmFsdWUpKSB7XHJcbiAgICAgICAgcmV0dXJuIHZhbHVlLmV2ZXJ5KGZ1bmN0aW9uICh2YWwpIHsgcmV0dXJuIC9eLT9bMC05XSskLy50ZXN0KFN0cmluZyh2YWwpKTsgfSk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gL14tP1swLTldKyQvLnRlc3QoU3RyaW5nKHZhbHVlKSk7XHJcbn07XHJcbnZhciBpbnRlZ2VyID0ge1xyXG4gICAgdmFsaWRhdGU6IHZhbGlkYXRlJGRcclxufTtcblxudmFyIHZhbGlkYXRlJGUgPSBmdW5jdGlvbiAodmFsdWUsIF9hKSB7XHJcbiAgICB2YXIgb3RoZXIgPSBfYS5vdGhlcjtcclxuICAgIHJldHVybiB2YWx1ZSA9PT0gb3RoZXI7XHJcbn07XHJcbnZhciBwYXJhbXMkOSA9IFtcclxuICAgIHtcclxuICAgICAgICBuYW1lOiAnb3RoZXInXHJcbiAgICB9XHJcbl07XHJcbnZhciBpcyA9IHtcclxuICAgIHZhbGlkYXRlOiB2YWxpZGF0ZSRlLFxyXG4gICAgcGFyYW1zOiBwYXJhbXMkOVxyXG59O1xuXG52YXIgdmFsaWRhdGUkZiA9IGZ1bmN0aW9uICh2YWx1ZSwgX2EpIHtcclxuICAgIHZhciBvdGhlciA9IF9hLm90aGVyO1xyXG4gICAgcmV0dXJuIHZhbHVlICE9PSBvdGhlcjtcclxufTtcclxudmFyIHBhcmFtcyRhID0gW1xyXG4gICAge1xyXG4gICAgICAgIG5hbWU6ICdvdGhlcidcclxuICAgIH1cclxuXTtcclxudmFyIGlzX25vdCA9IHtcclxuICAgIHZhbGlkYXRlOiB2YWxpZGF0ZSRmLFxyXG4gICAgcGFyYW1zOiBwYXJhbXMkYVxyXG59O1xuXG52YXIgdmFsaWRhdGUkZyA9IGZ1bmN0aW9uICh2YWx1ZSwgX2EpIHtcclxuICAgIHZhciBsZW5ndGggPSBfYS5sZW5ndGg7XHJcbiAgICBpZiAoaXNOdWxsT3JVbmRlZmluZWQodmFsdWUpKSB7XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG4gICAgaWYgKHR5cGVvZiB2YWx1ZSA9PT0gJ251bWJlcicpIHtcclxuICAgICAgICB2YWx1ZSA9IFN0cmluZyh2YWx1ZSk7XHJcbiAgICB9XHJcbiAgICBpZiAoIXZhbHVlLmxlbmd0aCkge1xyXG4gICAgICAgIHZhbHVlID0gdG9BcnJheSh2YWx1ZSk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gdmFsdWUubGVuZ3RoID09PSBsZW5ndGg7XHJcbn07XHJcbnZhciBwYXJhbXMkYiA9IFtcclxuICAgIHtcclxuICAgICAgICBuYW1lOiAnbGVuZ3RoJyxcclxuICAgICAgICBjYXN0OiBmdW5jdGlvbiAodmFsdWUpIHsgcmV0dXJuIE51bWJlcih2YWx1ZSk7IH1cclxuICAgIH1cclxuXTtcclxudmFyIGxlbmd0aCA9IHtcclxuICAgIHZhbGlkYXRlOiB2YWxpZGF0ZSRnLFxyXG4gICAgcGFyYW1zOiBwYXJhbXMkYlxyXG59O1xuXG52YXIgdmFsaWRhdGUkaCA9IGZ1bmN0aW9uICh2YWx1ZSwgX2EpIHtcclxuICAgIHZhciBsZW5ndGggPSBfYS5sZW5ndGg7XHJcbiAgICBpZiAoaXNOdWxsT3JVbmRlZmluZWQodmFsdWUpKSB7XHJcbiAgICAgICAgcmV0dXJuIGxlbmd0aCA+PSAwO1xyXG4gICAgfVxyXG4gICAgaWYgKEFycmF5LmlzQXJyYXkodmFsdWUpKSB7XHJcbiAgICAgICAgcmV0dXJuIHZhbHVlLmV2ZXJ5KGZ1bmN0aW9uICh2YWwpIHsgcmV0dXJuIHZhbGlkYXRlJGgodmFsLCB7IGxlbmd0aDogbGVuZ3RoIH0pOyB9KTtcclxuICAgIH1cclxuICAgIHJldHVybiBTdHJpbmcodmFsdWUpLmxlbmd0aCA8PSBsZW5ndGg7XHJcbn07XHJcbnZhciBwYXJhbXMkYyA9IFtcclxuICAgIHtcclxuICAgICAgICBuYW1lOiAnbGVuZ3RoJyxcclxuICAgICAgICBjYXN0OiBmdW5jdGlvbiAodmFsdWUpIHtcclxuICAgICAgICAgICAgcmV0dXJuIE51bWJlcih2YWx1ZSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5dO1xyXG52YXIgbWF4ID0ge1xyXG4gICAgdmFsaWRhdGU6IHZhbGlkYXRlJGgsXHJcbiAgICBwYXJhbXM6IHBhcmFtcyRjXHJcbn07XG5cbnZhciB2YWxpZGF0ZSRpID0gZnVuY3Rpb24gKHZhbHVlLCBfYSkge1xyXG4gICAgdmFyIG1heCA9IF9hLm1heDtcclxuICAgIGlmIChpc051bGxPclVuZGVmaW5lZCh2YWx1ZSkgfHwgdmFsdWUgPT09ICcnKSB7XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG4gICAgaWYgKEFycmF5LmlzQXJyYXkodmFsdWUpKSB7XHJcbiAgICAgICAgcmV0dXJuIHZhbHVlLmxlbmd0aCA+IDAgJiYgdmFsdWUuZXZlcnkoZnVuY3Rpb24gKHZhbCkgeyByZXR1cm4gdmFsaWRhdGUkaSh2YWwsIHsgbWF4OiBtYXggfSk7IH0pO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIE51bWJlcih2YWx1ZSkgPD0gbWF4O1xyXG59O1xyXG52YXIgcGFyYW1zJGQgPSBbXHJcbiAgICB7XHJcbiAgICAgICAgbmFtZTogJ21heCcsXHJcbiAgICAgICAgY2FzdDogZnVuY3Rpb24gKHZhbHVlKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBOdW1iZXIodmFsdWUpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXTtcclxudmFyIG1heF92YWx1ZSA9IHtcclxuICAgIHZhbGlkYXRlOiB2YWxpZGF0ZSRpLFxyXG4gICAgcGFyYW1zOiBwYXJhbXMkZFxyXG59O1xuXG52YXIgdmFsaWRhdGUkaiA9IGZ1bmN0aW9uIChmaWxlcywgbWltZXMpIHtcclxuICAgIHZhciByZWdleCA9IG5ldyBSZWdFeHAobWltZXMuam9pbignfCcpLnJlcGxhY2UoJyonLCAnLisnKSArIFwiJFwiLCAnaScpO1xyXG4gICAgaWYgKEFycmF5LmlzQXJyYXkoZmlsZXMpKSB7XHJcbiAgICAgICAgcmV0dXJuIGZpbGVzLmV2ZXJ5KGZ1bmN0aW9uIChmaWxlKSB7IHJldHVybiByZWdleC50ZXN0KGZpbGUudHlwZSk7IH0pO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHJlZ2V4LnRlc3QoZmlsZXMudHlwZSk7XHJcbn07XHJcbnZhciBtaW1lcyA9IHtcclxuICAgIHZhbGlkYXRlOiB2YWxpZGF0ZSRqXHJcbn07XG5cbnZhciB2YWxpZGF0ZSRrID0gZnVuY3Rpb24gKHZhbHVlLCBfYSkge1xyXG4gICAgdmFyIGxlbmd0aCA9IF9hLmxlbmd0aDtcclxuICAgIGlmIChpc051bGxPclVuZGVmaW5lZCh2YWx1ZSkpIHtcclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcbiAgICBpZiAoQXJyYXkuaXNBcnJheSh2YWx1ZSkpIHtcclxuICAgICAgICByZXR1cm4gdmFsdWUuZXZlcnkoZnVuY3Rpb24gKHZhbCkgeyByZXR1cm4gdmFsaWRhdGUkayh2YWwsIHsgbGVuZ3RoOiBsZW5ndGggfSk7IH0pO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIFN0cmluZyh2YWx1ZSkubGVuZ3RoID49IGxlbmd0aDtcclxufTtcclxudmFyIHBhcmFtcyRlID0gW1xyXG4gICAge1xyXG4gICAgICAgIG5hbWU6ICdsZW5ndGgnLFxyXG4gICAgICAgIGNhc3Q6IGZ1bmN0aW9uICh2YWx1ZSkge1xyXG4gICAgICAgICAgICByZXR1cm4gTnVtYmVyKHZhbHVlKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbl07XHJcbnZhciBtaW4gPSB7XHJcbiAgICB2YWxpZGF0ZTogdmFsaWRhdGUkayxcclxuICAgIHBhcmFtczogcGFyYW1zJGVcclxufTtcblxudmFyIHZhbGlkYXRlJGwgPSBmdW5jdGlvbiAodmFsdWUsIF9hKSB7XHJcbiAgICB2YXIgbWluID0gX2EubWluO1xyXG4gICAgaWYgKGlzTnVsbE9yVW5kZWZpbmVkKHZhbHVlKSB8fCB2YWx1ZSA9PT0gJycpIHtcclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcbiAgICBpZiAoQXJyYXkuaXNBcnJheSh2YWx1ZSkpIHtcclxuICAgICAgICByZXR1cm4gdmFsdWUubGVuZ3RoID4gMCAmJiB2YWx1ZS5ldmVyeShmdW5jdGlvbiAodmFsKSB7IHJldHVybiB2YWxpZGF0ZSRsKHZhbCwgeyBtaW46IG1pbiB9KTsgfSk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gTnVtYmVyKHZhbHVlKSA+PSBtaW47XHJcbn07XHJcbnZhciBwYXJhbXMkZiA9IFtcclxuICAgIHtcclxuICAgICAgICBuYW1lOiAnbWluJyxcclxuICAgICAgICBjYXN0OiBmdW5jdGlvbiAodmFsdWUpIHtcclxuICAgICAgICAgICAgcmV0dXJuIE51bWJlcih2YWx1ZSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5dO1xyXG52YXIgbWluX3ZhbHVlID0ge1xyXG4gICAgdmFsaWRhdGU6IHZhbGlkYXRlJGwsXHJcbiAgICBwYXJhbXM6IHBhcmFtcyRmXHJcbn07XG5cbnZhciBhciA9IC9eW9mg2aHZotmj2aTZpdmm2afZqNmpXSskLztcclxudmFyIGVuID0gL15bMC05XSskLztcclxudmFyIHZhbGlkYXRlJG0gPSBmdW5jdGlvbiAodmFsdWUpIHtcclxuICAgIHZhciB0ZXN0VmFsdWUgPSBmdW5jdGlvbiAodmFsKSB7XHJcbiAgICAgICAgdmFyIHN0clZhbHVlID0gU3RyaW5nKHZhbCk7XHJcbiAgICAgICAgcmV0dXJuIGVuLnRlc3Qoc3RyVmFsdWUpIHx8IGFyLnRlc3Qoc3RyVmFsdWUpO1xyXG4gICAgfTtcclxuICAgIGlmIChBcnJheS5pc0FycmF5KHZhbHVlKSkge1xyXG4gICAgICAgIHJldHVybiB2YWx1ZS5ldmVyeSh0ZXN0VmFsdWUpO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHRlc3RWYWx1ZSh2YWx1ZSk7XHJcbn07XHJcbnZhciBudW1lcmljID0ge1xyXG4gICAgdmFsaWRhdGU6IHZhbGlkYXRlJG1cclxufTtcblxudmFyIHZhbGlkYXRlJG4gPSBmdW5jdGlvbiAodmFsdWUsIF9hKSB7XHJcbiAgICB2YXIgcmVnZXggPSBfYS5yZWdleDtcclxuICAgIGlmIChBcnJheS5pc0FycmF5KHZhbHVlKSkge1xyXG4gICAgICAgIHJldHVybiB2YWx1ZS5ldmVyeShmdW5jdGlvbiAodmFsKSB7IHJldHVybiB2YWxpZGF0ZSRuKHZhbCwgeyByZWdleDogcmVnZXggfSk7IH0pO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHJlZ2V4LnRlc3QoU3RyaW5nKHZhbHVlKSk7XHJcbn07XHJcbnZhciBwYXJhbXMkZyA9IFtcclxuICAgIHtcclxuICAgICAgICBuYW1lOiAncmVnZXgnLFxyXG4gICAgICAgIGNhc3Q6IGZ1bmN0aW9uICh2YWx1ZSkge1xyXG4gICAgICAgICAgICBpZiAodHlwZW9mIHZhbHVlID09PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBSZWdFeHAodmFsdWUpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybiB2YWx1ZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbl07XHJcbnZhciByZWdleCA9IHtcclxuICAgIHZhbGlkYXRlOiB2YWxpZGF0ZSRuLFxyXG4gICAgcGFyYW1zOiBwYXJhbXMkZ1xyXG59O1xuXG52YXIgdmFsaWRhdGUkbyA9IGZ1bmN0aW9uICh2YWx1ZSwgX2EpIHtcclxuICAgIHZhciBhbGxvd0ZhbHNlID0gKF9hID09PSB2b2lkIDAgPyB7IGFsbG93RmFsc2U6IHRydWUgfSA6IF9hKS5hbGxvd0ZhbHNlO1xyXG4gICAgdmFyIHJlc3VsdCA9IHtcclxuICAgICAgICB2YWxpZDogZmFsc2UsXHJcbiAgICAgICAgcmVxdWlyZWQ6IHRydWVcclxuICAgIH07XHJcbiAgICBpZiAoaXNOdWxsT3JVbmRlZmluZWQodmFsdWUpIHx8IGlzRW1wdHlBcnJheSh2YWx1ZSkpIHtcclxuICAgICAgICByZXR1cm4gcmVzdWx0O1xyXG4gICAgfVxyXG4gICAgLy8gaW5jYXNlIGEgZmllbGQgY29uc2lkZXJzIGBmYWxzZWAgYXMgYW4gZW1wdHkgdmFsdWUgbGlrZSBjaGVja2JveGVzLlxyXG4gICAgaWYgKHZhbHVlID09PSBmYWxzZSAmJiAhYWxsb3dGYWxzZSkge1xyXG4gICAgICAgIHJldHVybiByZXN1bHQ7XHJcbiAgICB9XHJcbiAgICByZXN1bHQudmFsaWQgPSAhIVN0cmluZyh2YWx1ZSkudHJpbSgpLmxlbmd0aDtcclxuICAgIHJldHVybiByZXN1bHQ7XHJcbn07XHJcbnZhciBjb21wdXRlc1JlcXVpcmVkID0gdHJ1ZTtcclxudmFyIHBhcmFtcyRoID0gW1xyXG4gICAge1xyXG4gICAgICAgIG5hbWU6ICdhbGxvd0ZhbHNlJyxcclxuICAgICAgICBkZWZhdWx0OiB0cnVlXHJcbiAgICB9XHJcbl07XHJcbnZhciByZXF1aXJlZCA9IHtcclxuICAgIHZhbGlkYXRlOiB2YWxpZGF0ZSRvLFxyXG4gICAgcGFyYW1zOiBwYXJhbXMkaCxcclxuICAgIGNvbXB1dGVzUmVxdWlyZWQ6IGNvbXB1dGVzUmVxdWlyZWRcclxufTtcblxudmFyIHRlc3RFbXB0eSA9IGZ1bmN0aW9uICh2YWx1ZSkge1xyXG4gICAgcmV0dXJuIGlzRW1wdHlBcnJheSh2YWx1ZSkgfHwgaW5jbHVkZXMoW2ZhbHNlLCBudWxsLCB1bmRlZmluZWRdLCB2YWx1ZSkgfHwgIVN0cmluZyh2YWx1ZSkudHJpbSgpLmxlbmd0aDtcclxufTtcclxudmFyIHZhbGlkYXRlJHAgPSBmdW5jdGlvbiAodmFsdWUsIF9hKSB7XHJcbiAgICB2YXIgdGFyZ2V0ID0gX2EudGFyZ2V0LCB2YWx1ZXMgPSBfYS52YWx1ZXM7XHJcbiAgICB2YXIgcmVxdWlyZWQ7XHJcbiAgICBpZiAodmFsdWVzICYmIHZhbHVlcy5sZW5ndGgpIHtcclxuICAgICAgICBpZiAoIUFycmF5LmlzQXJyYXkodmFsdWVzKSAmJiB0eXBlb2YgdmFsdWVzID09PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgICB2YWx1ZXMgPSBbdmFsdWVzXTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lXHJcbiAgICAgICAgcmVxdWlyZWQgPSB2YWx1ZXMuc29tZShmdW5jdGlvbiAodmFsKSB7IHJldHVybiB2YWwgPT0gU3RyaW5nKHRhcmdldCkudHJpbSgpOyB9KTtcclxuICAgIH1cclxuICAgIGVsc2Uge1xyXG4gICAgICAgIHJlcXVpcmVkID0gIXRlc3RFbXB0eSh0YXJnZXQpO1xyXG4gICAgfVxyXG4gICAgaWYgKCFyZXF1aXJlZCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIHZhbGlkOiB0cnVlLFxyXG4gICAgICAgICAgICByZXF1aXJlZDogcmVxdWlyZWRcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHtcclxuICAgICAgICB2YWxpZDogIXRlc3RFbXB0eSh2YWx1ZSksXHJcbiAgICAgICAgcmVxdWlyZWQ6IHJlcXVpcmVkXHJcbiAgICB9O1xyXG59O1xyXG52YXIgcGFyYW1zJGkgPSBbXHJcbiAgICB7XHJcbiAgICAgICAgbmFtZTogJ3RhcmdldCcsXHJcbiAgICAgICAgaXNUYXJnZXQ6IHRydWVcclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgICAgbmFtZTogJ3ZhbHVlcydcclxuICAgIH1cclxuXTtcclxudmFyIGNvbXB1dGVzUmVxdWlyZWQkMSA9IHRydWU7XHJcbnZhciByZXF1aXJlZF9pZiA9IHtcclxuICAgIHZhbGlkYXRlOiB2YWxpZGF0ZSRwLFxyXG4gICAgcGFyYW1zOiBwYXJhbXMkaSxcclxuICAgIGNvbXB1dGVzUmVxdWlyZWQ6IGNvbXB1dGVzUmVxdWlyZWQkMVxyXG59O1xuXG52YXIgdmFsaWRhdGUkcSA9IGZ1bmN0aW9uIChmaWxlcywgX2EpIHtcclxuICAgIHZhciBzaXplID0gX2Euc2l6ZTtcclxuICAgIGlmIChpc05hTihzaXplKSkge1xyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuICAgIHZhciBuU2l6ZSA9IHNpemUgKiAxMDI0O1xyXG4gICAgaWYgKCFBcnJheS5pc0FycmF5KGZpbGVzKSkge1xyXG4gICAgICAgIHJldHVybiBmaWxlcy5zaXplIDw9IG5TaXplO1xyXG4gICAgfVxyXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBmaWxlcy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgIGlmIChmaWxlc1tpXS5zaXplID4gblNpemUpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIHJldHVybiB0cnVlO1xyXG59O1xyXG52YXIgcGFyYW1zJGogPSBbXHJcbiAgICB7XHJcbiAgICAgICAgbmFtZTogJ3NpemUnLFxyXG4gICAgICAgIGNhc3Q6IGZ1bmN0aW9uICh2YWx1ZSkge1xyXG4gICAgICAgICAgICByZXR1cm4gTnVtYmVyKHZhbHVlKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbl07XHJcbnZhciBzaXplID0ge1xyXG4gICAgdmFsaWRhdGU6IHZhbGlkYXRlJHEsXHJcbiAgICBwYXJhbXM6IHBhcmFtcyRqXHJcbn07XG5cbmV4cG9ydCB7IGFscGhhJDEgYXMgYWxwaGEsIGFscGhhX2Rhc2gsIGFscGhhX251bSwgYWxwaGFfc3BhY2VzLCBiZXR3ZWVuLCBjb25maXJtZWQsIGRpZ2l0cywgZGltZW5zaW9ucywgZW1haWwsIGV4Y2x1ZGVkLCBleHQsIGltYWdlLCBpbnRlZ2VyLCBpcywgaXNfbm90LCBsZW5ndGgsIG1heCwgbWF4X3ZhbHVlLCBtaW1lcywgbWluLCBtaW5fdmFsdWUsIG51bWVyaWMsIG9uZU9mLCByZWdleCwgcmVxdWlyZWQsIHJlcXVpcmVkX2lmLCBzaXplIH07XG4iLCIvKipcbiAgKiB2ZWUtdmFsaWRhdGUgdjMuMC4xMVxuICAqIChjKSAyMDE5IEFiZGVscmFobWFuIEF3YWRcbiAgKiBAbGljZW5zZSBNSVRcbiAgKi9cbmltcG9ydCBWdWUgZnJvbSAndnVlJztcblxuLyohICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXHJcbkNvcHlyaWdodCAoYykgTWljcm9zb2Z0IENvcnBvcmF0aW9uLiBBbGwgcmlnaHRzIHJlc2VydmVkLlxyXG5MaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpOyB5b3UgbWF5IG5vdCB1c2VcclxudGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGVcclxuTGljZW5zZSBhdCBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuXHJcblRISVMgQ09ERSBJUyBQUk9WSURFRCBPTiBBTiAqQVMgSVMqIEJBU0lTLCBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTllcclxuS0lORCwgRUlUSEVSIEVYUFJFU1MgT1IgSU1QTElFRCwgSU5DTFVESU5HIFdJVEhPVVQgTElNSVRBVElPTiBBTlkgSU1QTElFRFxyXG5XQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgVElUTEUsIEZJVE5FU1MgRk9SIEEgUEFSVElDVUxBUiBQVVJQT1NFLFxyXG5NRVJDSEFOVEFCTElUWSBPUiBOT04tSU5GUklOR0VNRU5ULlxyXG5cclxuU2VlIHRoZSBBcGFjaGUgVmVyc2lvbiAyLjAgTGljZW5zZSBmb3Igc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zXHJcbmFuZCBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiogKi9cclxuXHJcbnZhciBfX2Fzc2lnbiA9IGZ1bmN0aW9uKCkge1xyXG4gICAgX19hc3NpZ24gPSBPYmplY3QuYXNzaWduIHx8IGZ1bmN0aW9uIF9fYXNzaWduKHQpIHtcclxuICAgICAgICBmb3IgKHZhciBzLCBpID0gMSwgbiA9IGFyZ3VtZW50cy5sZW5ndGg7IGkgPCBuOyBpKyspIHtcclxuICAgICAgICAgICAgcyA9IGFyZ3VtZW50c1tpXTtcclxuICAgICAgICAgICAgZm9yICh2YXIgcCBpbiBzKSBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHMsIHApKSB0W3BdID0gc1twXTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHQ7XHJcbiAgICB9O1xyXG4gICAgcmV0dXJuIF9fYXNzaWduLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7XHJcbn07XHJcblxyXG5mdW5jdGlvbiBfX2F3YWl0ZXIodGhpc0FyZywgX2FyZ3VtZW50cywgUCwgZ2VuZXJhdG9yKSB7XHJcbiAgICByZXR1cm4gbmV3IChQIHx8IChQID0gUHJvbWlzZSkpKGZ1bmN0aW9uIChyZXNvbHZlLCByZWplY3QpIHtcclxuICAgICAgICBmdW5jdGlvbiBmdWxmaWxsZWQodmFsdWUpIHsgdHJ5IHsgc3RlcChnZW5lcmF0b3IubmV4dCh2YWx1ZSkpOyB9IGNhdGNoIChlKSB7IHJlamVjdChlKTsgfSB9XHJcbiAgICAgICAgZnVuY3Rpb24gcmVqZWN0ZWQodmFsdWUpIHsgdHJ5IHsgc3RlcChnZW5lcmF0b3JbXCJ0aHJvd1wiXSh2YWx1ZSkpOyB9IGNhdGNoIChlKSB7IHJlamVjdChlKTsgfSB9XHJcbiAgICAgICAgZnVuY3Rpb24gc3RlcChyZXN1bHQpIHsgcmVzdWx0LmRvbmUgPyByZXNvbHZlKHJlc3VsdC52YWx1ZSkgOiBuZXcgUChmdW5jdGlvbiAocmVzb2x2ZSkgeyByZXNvbHZlKHJlc3VsdC52YWx1ZSk7IH0pLnRoZW4oZnVsZmlsbGVkLCByZWplY3RlZCk7IH1cclxuICAgICAgICBzdGVwKChnZW5lcmF0b3IgPSBnZW5lcmF0b3IuYXBwbHkodGhpc0FyZywgX2FyZ3VtZW50cyB8fCBbXSkpLm5leHQoKSk7XHJcbiAgICB9KTtcclxufVxyXG5cclxuZnVuY3Rpb24gX19nZW5lcmF0b3IodGhpc0FyZywgYm9keSkge1xyXG4gICAgdmFyIF8gPSB7IGxhYmVsOiAwLCBzZW50OiBmdW5jdGlvbigpIHsgaWYgKHRbMF0gJiAxKSB0aHJvdyB0WzFdOyByZXR1cm4gdFsxXTsgfSwgdHJ5czogW10sIG9wczogW10gfSwgZiwgeSwgdCwgZztcclxuICAgIHJldHVybiBnID0geyBuZXh0OiB2ZXJiKDApLCBcInRocm93XCI6IHZlcmIoMSksIFwicmV0dXJuXCI6IHZlcmIoMikgfSwgdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIChnW1N5bWJvbC5pdGVyYXRvcl0gPSBmdW5jdGlvbigpIHsgcmV0dXJuIHRoaXM7IH0pLCBnO1xyXG4gICAgZnVuY3Rpb24gdmVyYihuKSB7IHJldHVybiBmdW5jdGlvbiAodikgeyByZXR1cm4gc3RlcChbbiwgdl0pOyB9OyB9XHJcbiAgICBmdW5jdGlvbiBzdGVwKG9wKSB7XHJcbiAgICAgICAgaWYgKGYpIHRocm93IG5ldyBUeXBlRXJyb3IoXCJHZW5lcmF0b3IgaXMgYWxyZWFkeSBleGVjdXRpbmcuXCIpO1xyXG4gICAgICAgIHdoaWxlIChfKSB0cnkge1xyXG4gICAgICAgICAgICBpZiAoZiA9IDEsIHkgJiYgKHQgPSBvcFswXSAmIDIgPyB5W1wicmV0dXJuXCJdIDogb3BbMF0gPyB5W1widGhyb3dcIl0gfHwgKCh0ID0geVtcInJldHVyblwiXSkgJiYgdC5jYWxsKHkpLCAwKSA6IHkubmV4dCkgJiYgISh0ID0gdC5jYWxsKHksIG9wWzFdKSkuZG9uZSkgcmV0dXJuIHQ7XHJcbiAgICAgICAgICAgIGlmICh5ID0gMCwgdCkgb3AgPSBbb3BbMF0gJiAyLCB0LnZhbHVlXTtcclxuICAgICAgICAgICAgc3dpdGNoIChvcFswXSkge1xyXG4gICAgICAgICAgICAgICAgY2FzZSAwOiBjYXNlIDE6IHQgPSBvcDsgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICBjYXNlIDQ6IF8ubGFiZWwrKzsgcmV0dXJuIHsgdmFsdWU6IG9wWzFdLCBkb25lOiBmYWxzZSB9O1xyXG4gICAgICAgICAgICAgICAgY2FzZSA1OiBfLmxhYmVsKys7IHkgPSBvcFsxXTsgb3AgPSBbMF07IGNvbnRpbnVlO1xyXG4gICAgICAgICAgICAgICAgY2FzZSA3OiBvcCA9IF8ub3BzLnBvcCgpOyBfLnRyeXMucG9wKCk7IGNvbnRpbnVlO1xyXG4gICAgICAgICAgICAgICAgZGVmYXVsdDpcclxuICAgICAgICAgICAgICAgICAgICBpZiAoISh0ID0gXy50cnlzLCB0ID0gdC5sZW5ndGggPiAwICYmIHRbdC5sZW5ndGggLSAxXSkgJiYgKG9wWzBdID09PSA2IHx8IG9wWzBdID09PSAyKSkgeyBfID0gMDsgY29udGludWU7IH1cclxuICAgICAgICAgICAgICAgICAgICBpZiAob3BbMF0gPT09IDMgJiYgKCF0IHx8IChvcFsxXSA+IHRbMF0gJiYgb3BbMV0gPCB0WzNdKSkpIHsgXy5sYWJlbCA9IG9wWzFdOyBicmVhazsgfVxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChvcFswXSA9PT0gNiAmJiBfLmxhYmVsIDwgdFsxXSkgeyBfLmxhYmVsID0gdFsxXTsgdCA9IG9wOyBicmVhazsgfVxyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0ICYmIF8ubGFiZWwgPCB0WzJdKSB7IF8ubGFiZWwgPSB0WzJdOyBfLm9wcy5wdXNoKG9wKTsgYnJlYWs7IH1cclxuICAgICAgICAgICAgICAgICAgICBpZiAodFsyXSkgXy5vcHMucG9wKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgXy50cnlzLnBvcCgpOyBjb250aW51ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBvcCA9IGJvZHkuY2FsbCh0aGlzQXJnLCBfKTtcclxuICAgICAgICB9IGNhdGNoIChlKSB7IG9wID0gWzYsIGVdOyB5ID0gMDsgfSBmaW5hbGx5IHsgZiA9IHQgPSAwOyB9XHJcbiAgICAgICAgaWYgKG9wWzBdICYgNSkgdGhyb3cgb3BbMV07IHJldHVybiB7IHZhbHVlOiBvcFswXSA/IG9wWzFdIDogdm9pZCAwLCBkb25lOiB0cnVlIH07XHJcbiAgICB9XHJcbn1cclxuXHJcbmZ1bmN0aW9uIF9fc3ByZWFkQXJyYXlzKCkge1xyXG4gICAgZm9yICh2YXIgcyA9IDAsIGkgPSAwLCBpbCA9IGFyZ3VtZW50cy5sZW5ndGg7IGkgPCBpbDsgaSsrKSBzICs9IGFyZ3VtZW50c1tpXS5sZW5ndGg7XHJcbiAgICBmb3IgKHZhciByID0gQXJyYXkocyksIGsgPSAwLCBpID0gMDsgaSA8IGlsOyBpKyspXHJcbiAgICAgICAgZm9yICh2YXIgYSA9IGFyZ3VtZW50c1tpXSwgaiA9IDAsIGpsID0gYS5sZW5ndGg7IGogPCBqbDsgaisrLCBrKyspXHJcbiAgICAgICAgICAgIHJba10gPSBhW2pdO1xyXG4gICAgcmV0dXJuIHI7XHJcbn1cblxudmFyIGlzTmFOID0gZnVuY3Rpb24gKHZhbHVlKSB7XHJcbiAgICAvLyBOYU4gaXMgdGhlIG9uZSB2YWx1ZSB0aGF0IGRvZXMgbm90IGVxdWFsIGl0c2VsZi5cclxuICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZVxyXG4gICAgcmV0dXJuIHZhbHVlICE9PSB2YWx1ZTtcclxufTtcclxuLyoqXHJcbiAqIENoZWNrcyBpZiB0aGUgdmFsdWVzIGFyZSBlaXRoZXIgbnVsbCBvciB1bmRlZmluZWQuXHJcbiAqL1xyXG52YXIgaXNOdWxsT3JVbmRlZmluZWQgPSBmdW5jdGlvbiAodmFsdWUpIHtcclxuICAgIHJldHVybiB2YWx1ZSA9PT0gbnVsbCB8fCB2YWx1ZSA9PT0gdW5kZWZpbmVkO1xyXG59O1xyXG4vKipcclxuICogQ3JlYXRlcyB0aGUgZGVmYXVsdCBmbGFncyBvYmplY3QuXHJcbiAqL1xyXG52YXIgY3JlYXRlRmxhZ3MgPSBmdW5jdGlvbiAoKSB7IHJldHVybiAoe1xyXG4gICAgdW50b3VjaGVkOiB0cnVlLFxyXG4gICAgdG91Y2hlZDogZmFsc2UsXHJcbiAgICBkaXJ0eTogZmFsc2UsXHJcbiAgICBwcmlzdGluZTogdHJ1ZSxcclxuICAgIHZhbGlkOiBmYWxzZSxcclxuICAgIGludmFsaWQ6IGZhbHNlLFxyXG4gICAgdmFsaWRhdGVkOiBmYWxzZSxcclxuICAgIHBlbmRpbmc6IGZhbHNlLFxyXG4gICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgY2hhbmdlZDogZmFsc2VcclxufSk7IH07XHJcbi8qKlxyXG4gKiBDaGVja3MgaWYgdGhlIHZhbHVlIGlzIGFuIG9iamVjdC5cclxuICovXHJcbnZhciBpc09iamVjdCA9IGZ1bmN0aW9uIChvYmopIHtcclxuICAgIHJldHVybiBvYmogIT09IG51bGwgJiYgb2JqICYmIHR5cGVvZiBvYmogPT09ICdvYmplY3QnICYmICFBcnJheS5pc0FycmF5KG9iaik7XHJcbn07XHJcbmZ1bmN0aW9uIGlkZW50aXR5KHgpIHtcclxuICAgIHJldHVybiB4O1xyXG59XHJcbi8qKlxyXG4gKiBTaGFsbG93IG9iamVjdCBjb21wYXJpc29uLlxyXG4gKi9cclxudmFyIGlzRXF1YWwgPSBmdW5jdGlvbiAobGhzLCByaHMpIHtcclxuICAgIGlmIChsaHMgaW5zdGFuY2VvZiBSZWdFeHAgJiYgcmhzIGluc3RhbmNlb2YgUmVnRXhwKSB7XHJcbiAgICAgICAgcmV0dXJuIGlzRXF1YWwobGhzLnNvdXJjZSwgcmhzLnNvdXJjZSkgJiYgaXNFcXVhbChsaHMuZmxhZ3MsIHJocy5mbGFncyk7XHJcbiAgICB9XHJcbiAgICBpZiAoQXJyYXkuaXNBcnJheShsaHMpICYmIEFycmF5LmlzQXJyYXkocmhzKSkge1xyXG4gICAgICAgIGlmIChsaHMubGVuZ3RoICE9PSByaHMubGVuZ3RoKVxyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBsaHMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgaWYgKCFpc0VxdWFsKGxoc1tpXSwgcmhzW2ldKSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG4gICAgLy8gaWYgYm90aCBhcmUgb2JqZWN0cywgY29tcGFyZSBlYWNoIGtleSByZWN1cnNpdmVseS5cclxuICAgIGlmIChpc09iamVjdChsaHMpICYmIGlzT2JqZWN0KHJocykpIHtcclxuICAgICAgICByZXR1cm4gKE9iamVjdC5rZXlzKGxocykuZXZlcnkoZnVuY3Rpb24gKGtleSkge1xyXG4gICAgICAgICAgICByZXR1cm4gaXNFcXVhbChsaHNba2V5XSwgcmhzW2tleV0pO1xyXG4gICAgICAgIH0pICYmXHJcbiAgICAgICAgICAgIE9iamVjdC5rZXlzKHJocykuZXZlcnkoZnVuY3Rpb24gKGtleSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGlzRXF1YWwobGhzW2tleV0sIHJoc1trZXldKTtcclxuICAgICAgICAgICAgfSkpO1xyXG4gICAgfVxyXG4gICAgaWYgKGlzTmFOKGxocykgJiYgaXNOYU4ocmhzKSkge1xyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIGxocyA9PT0gcmhzO1xyXG59O1xyXG52YXIgaW5jbHVkZXMgPSBmdW5jdGlvbiAoY29sbGVjdGlvbiwgaXRlbSkge1xyXG4gICAgcmV0dXJuIGNvbGxlY3Rpb24uaW5kZXhPZihpdGVtKSAhPT0gLTE7XHJcbn07XHJcbi8qKlxyXG4gKiBQYXJzZXMgYSBydWxlIHN0cmluZyBleHByZXNzaW9uLlxyXG4gKi9cclxudmFyIHBhcnNlUnVsZSA9IGZ1bmN0aW9uIChydWxlKSB7XHJcbiAgICB2YXIgcGFyYW1zID0gW107XHJcbiAgICB2YXIgbmFtZSA9IHJ1bGUuc3BsaXQoJzonKVswXTtcclxuICAgIGlmIChpbmNsdWRlcyhydWxlLCAnOicpKSB7XHJcbiAgICAgICAgcGFyYW1zID0gcnVsZVxyXG4gICAgICAgICAgICAuc3BsaXQoJzonKVxyXG4gICAgICAgICAgICAuc2xpY2UoMSlcclxuICAgICAgICAgICAgLmpvaW4oJzonKVxyXG4gICAgICAgICAgICAuc3BsaXQoJywnKTtcclxuICAgIH1cclxuICAgIHJldHVybiB7IG5hbWU6IG5hbWUsIHBhcmFtczogcGFyYW1zIH07XHJcbn07XHJcbi8qKlxyXG4gKiBEZWJvdW5jZXMgYSBmdW5jdGlvbi5cclxuICovXHJcbnZhciBkZWJvdW5jZSA9IGZ1bmN0aW9uIChmbiwgd2FpdCwgdG9rZW4pIHtcclxuICAgIGlmICh3YWl0ID09PSB2b2lkIDApIHsgd2FpdCA9IDA7IH1cclxuICAgIGlmICh0b2tlbiA9PT0gdm9pZCAwKSB7IHRva2VuID0geyBjYW5jZWxsZWQ6IGZhbHNlIH07IH1cclxuICAgIGlmICh3YWl0ID09PSAwKSB7XHJcbiAgICAgICAgcmV0dXJuIGZuO1xyXG4gICAgfVxyXG4gICAgdmFyIHRpbWVvdXQ7XHJcbiAgICByZXR1cm4gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHZhciBhcmdzID0gW107XHJcbiAgICAgICAgZm9yICh2YXIgX2kgPSAwOyBfaSA8IGFyZ3VtZW50cy5sZW5ndGg7IF9pKyspIHtcclxuICAgICAgICAgICAgYXJnc1tfaV0gPSBhcmd1bWVudHNbX2ldO1xyXG4gICAgICAgIH1cclxuICAgICAgICB2YXIgbGF0ZXIgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIHRpbWVvdXQgPSB1bmRlZmluZWQ7XHJcbiAgICAgICAgICAgIC8vIGNoZWNrIGlmIHRoZSBmbiBjYWxsIHdhcyBjYW5jZWxsZWQuXHJcbiAgICAgICAgICAgIGlmICghdG9rZW4uY2FuY2VsbGVkKVxyXG4gICAgICAgICAgICAgICAgZm4uYXBwbHkodm9pZCAwLCBhcmdzKTtcclxuICAgICAgICB9O1xyXG4gICAgICAgIC8vIGJlY2F1c2Ugd2UgbWlnaHQgd2FudCB0byB1c2UgTm9kZS5qcyBzZXRUaW1vdXQgZm9yIFNTUi5cclxuICAgICAgICBjbGVhclRpbWVvdXQodGltZW91dCk7XHJcbiAgICAgICAgdGltZW91dCA9IHNldFRpbWVvdXQobGF0ZXIsIHdhaXQpO1xyXG4gICAgfTtcclxufTtcclxuLyoqXHJcbiAqIEVtaXRzIGEgd2FybmluZyB0byB0aGUgY29uc29sZS5cclxuICovXHJcbnZhciB3YXJuID0gZnVuY3Rpb24gKG1lc3NhZ2UpIHtcclxuICAgIGNvbnNvbGUud2FybihcIlt2ZWUtdmFsaWRhdGVdIFwiICsgbWVzc2FnZSk7XHJcbn07XHJcbi8qKlxyXG4gKiBOb3JtYWxpemVzIHRoZSBnaXZlbiBydWxlcyBleHByZXNzaW9uLlxyXG4gKi9cclxudmFyIG5vcm1hbGl6ZVJ1bGVzID0gZnVuY3Rpb24gKHJ1bGVzKSB7XHJcbiAgICAvLyBpZiBmYWxzeSB2YWx1ZSByZXR1cm4gYW4gZW1wdHkgb2JqZWN0LlxyXG4gICAgdmFyIGFjYyA9IHt9O1xyXG4gICAgT2JqZWN0LmRlZmluZVByb3BlcnR5KGFjYywgJ18kJGlzTm9ybWFsaXplZCcsIHtcclxuICAgICAgICB2YWx1ZTogdHJ1ZSxcclxuICAgICAgICB3cml0YWJsZTogZmFsc2UsXHJcbiAgICAgICAgZW51bWVyYWJsZTogZmFsc2UsXHJcbiAgICAgICAgY29uZmlndXJhYmxlOiBmYWxzZVxyXG4gICAgfSk7XHJcbiAgICBpZiAoIXJ1bGVzKSB7XHJcbiAgICAgICAgcmV0dXJuIGFjYztcclxuICAgIH1cclxuICAgIC8vIE9iamVjdCBpcyBhbHJlYWR5IG5vcm1hbGl6ZWQsIHNraXAuXHJcbiAgICBpZiAoaXNPYmplY3QocnVsZXMpICYmIHJ1bGVzLl8kJGlzTm9ybWFsaXplZCkge1xyXG4gICAgICAgIHJldHVybiBydWxlcztcclxuICAgIH1cclxuICAgIGlmIChpc09iamVjdChydWxlcykpIHtcclxuICAgICAgICByZXR1cm4gT2JqZWN0LmtleXMocnVsZXMpLnJlZHVjZShmdW5jdGlvbiAocHJldiwgY3Vycikge1xyXG4gICAgICAgICAgICB2YXIgcGFyYW1zID0gW107XHJcbiAgICAgICAgICAgIGlmIChydWxlc1tjdXJyXSA9PT0gdHJ1ZSkge1xyXG4gICAgICAgICAgICAgICAgcGFyYW1zID0gW107XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZWxzZSBpZiAoQXJyYXkuaXNBcnJheShydWxlc1tjdXJyXSkpIHtcclxuICAgICAgICAgICAgICAgIHBhcmFtcyA9IHJ1bGVzW2N1cnJdO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGVsc2UgaWYgKGlzT2JqZWN0KHJ1bGVzW2N1cnJdKSkge1xyXG4gICAgICAgICAgICAgICAgcGFyYW1zID0gcnVsZXNbY3Vycl07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBwYXJhbXMgPSBbcnVsZXNbY3Vycl1dO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmIChydWxlc1tjdXJyXSAhPT0gZmFsc2UpIHtcclxuICAgICAgICAgICAgICAgIHByZXZbY3Vycl0gPSBwYXJhbXM7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIHByZXY7XHJcbiAgICAgICAgfSwgYWNjKTtcclxuICAgIH1cclxuICAgIC8qIGlzdGFuYnVsIGlnbm9yZSBpZiAqL1xyXG4gICAgaWYgKHR5cGVvZiBydWxlcyAhPT0gJ3N0cmluZycpIHtcclxuICAgICAgICB3YXJuKCdydWxlcyBtdXN0IGJlIGVpdGhlciBhIHN0cmluZyBvciBhbiBvYmplY3QuJyk7XHJcbiAgICAgICAgcmV0dXJuIGFjYztcclxuICAgIH1cclxuICAgIHJldHVybiBydWxlcy5zcGxpdCgnfCcpLnJlZHVjZShmdW5jdGlvbiAocHJldiwgcnVsZSkge1xyXG4gICAgICAgIHZhciBwYXJzZWRSdWxlID0gcGFyc2VSdWxlKHJ1bGUpO1xyXG4gICAgICAgIGlmICghcGFyc2VkUnVsZS5uYW1lKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBwcmV2O1xyXG4gICAgICAgIH1cclxuICAgICAgICBwcmV2W3BhcnNlZFJ1bGUubmFtZV0gPSBwYXJzZWRSdWxlLnBhcmFtcztcclxuICAgICAgICByZXR1cm4gcHJldjtcclxuICAgIH0sIGFjYyk7XHJcbn07XHJcbi8qKlxyXG4gKiBDaGVja3MgaWYgYSBmdW5jdGlvbiBpcyBjYWxsYWJsZS5cclxuICovXHJcbnZhciBpc0NhbGxhYmxlID0gZnVuY3Rpb24gKGZ1bmMpIHsgcmV0dXJuIHR5cGVvZiBmdW5jID09PSAnZnVuY3Rpb24nOyB9O1xyXG5mdW5jdGlvbiBjb21wdXRlQ2xhc3NPYmoobmFtZXMsIGZsYWdzKSB7XHJcbiAgICB2YXIgYWNjID0ge307XHJcbiAgICB2YXIga2V5cyA9IE9iamVjdC5rZXlzKGZsYWdzKTtcclxuICAgIHZhciBsZW5ndGggPSBrZXlzLmxlbmd0aDtcclxuICAgIHZhciBfbG9vcF8xID0gZnVuY3Rpb24gKGkpIHtcclxuICAgICAgICB2YXIgZmxhZyA9IGtleXNbaV07XHJcbiAgICAgICAgdmFyIGNsYXNzTmFtZSA9IChuYW1lcyAmJiBuYW1lc1tmbGFnXSkgfHwgZmxhZztcclxuICAgICAgICB2YXIgdmFsdWUgPSBmbGFnc1tmbGFnXTtcclxuICAgICAgICBpZiAoaXNOdWxsT3JVbmRlZmluZWQodmFsdWUpKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBcImNvbnRpbnVlXCI7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICgoZmxhZyA9PT0gJ3ZhbGlkJyB8fCBmbGFnID09PSAnaW52YWxpZCcpICYmICFmbGFncy52YWxpZGF0ZWQpIHtcclxuICAgICAgICAgICAgcmV0dXJuIFwiY29udGludWVcIjtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHR5cGVvZiBjbGFzc05hbWUgPT09ICdzdHJpbmcnKSB7XHJcbiAgICAgICAgICAgIGFjY1tjbGFzc05hbWVdID0gdmFsdWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2UgaWYgKEFycmF5LmlzQXJyYXkoY2xhc3NOYW1lKSkge1xyXG4gICAgICAgICAgICBjbGFzc05hbWUuZm9yRWFjaChmdW5jdGlvbiAoY2xzKSB7XHJcbiAgICAgICAgICAgICAgICBhY2NbY2xzXSA9IHZhbHVlO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBsZW5ndGg7IGkrKykge1xyXG4gICAgICAgIF9sb29wXzEoaSk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gYWNjO1xyXG59XHJcbi8qIGlzdGFuYnVsIGlnbm9yZSBuZXh0ICovXHJcbmZ1bmN0aW9uIF9jb3B5QXJyYXkoYXJyYXlMaWtlKSB7XHJcbiAgICB2YXIgYXJyYXkgPSBbXTtcclxuICAgIHZhciBsZW5ndGggPSBhcnJheUxpa2UubGVuZ3RoO1xyXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBsZW5ndGg7IGkrKykge1xyXG4gICAgICAgIGFycmF5LnB1c2goYXJyYXlMaWtlW2ldKTtcclxuICAgIH1cclxuICAgIHJldHVybiBhcnJheTtcclxufVxyXG4vKipcclxuICogQ29udmVydHMgYW4gYXJyYXktbGlrZSBvYmplY3QgdG8gYXJyYXksIHByb3ZpZGVzIGEgc2ltcGxlIHBvbHlmaWxsIGZvciBBcnJheS5mcm9tXHJcbiAqL1xyXG5mdW5jdGlvbiB0b0FycmF5KGFycmF5TGlrZSkge1xyXG4gICAgaWYgKGlzQ2FsbGFibGUoQXJyYXkuZnJvbSkpIHtcclxuICAgICAgICByZXR1cm4gQXJyYXkuZnJvbShhcnJheUxpa2UpO1xyXG4gICAgfVxyXG4gICAgLyogaXN0YW5idWwgaWdub3JlIG5leHQgKi9cclxuICAgIHJldHVybiBfY29weUFycmF5KGFycmF5TGlrZSk7XHJcbn1cclxuZnVuY3Rpb24gZmluZEluZGV4KGFycmF5TGlrZSwgcHJlZGljYXRlKSB7XHJcbiAgICB2YXIgYXJyYXkgPSBBcnJheS5pc0FycmF5KGFycmF5TGlrZSkgPyBhcnJheUxpa2UgOiB0b0FycmF5KGFycmF5TGlrZSk7XHJcbiAgICBpZiAoaXNDYWxsYWJsZShhcnJheS5maW5kSW5kZXgpKSB7XHJcbiAgICAgICAgcmV0dXJuIGFycmF5LmZpbmRJbmRleChwcmVkaWNhdGUpO1xyXG4gICAgfVxyXG4gICAgLyogaXN0YW5idWwgaWdub3JlIG5leHQgKi9cclxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgYXJyYXkubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICBpZiAocHJlZGljYXRlKGFycmF5W2ldLCBpKSkge1xyXG4gICAgICAgICAgICByZXR1cm4gaTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAvKiBpc3RhbmJ1bCBpZ25vcmUgbmV4dCAqL1xyXG4gICAgcmV0dXJuIC0xO1xyXG59XHJcbi8qKlxyXG4gKiBmaW5kcyB0aGUgZmlyc3QgZWxlbWVudCB0aGF0IHNhdGlzZmllcyB0aGUgcHJlZGljYXRlIGNhbGxiYWNrLCBwb2x5ZmlsbHMgYXJyYXkuZmluZFxyXG4gKi9cclxuZnVuY3Rpb24gZmluZChhcnJheUxpa2UsIHByZWRpY2F0ZSkge1xyXG4gICAgdmFyIGFycmF5ID0gQXJyYXkuaXNBcnJheShhcnJheUxpa2UpID8gYXJyYXlMaWtlIDogdG9BcnJheShhcnJheUxpa2UpO1xyXG4gICAgdmFyIGlkeCA9IGZpbmRJbmRleChhcnJheSwgcHJlZGljYXRlKTtcclxuICAgIHJldHVybiBpZHggPT09IC0xID8gdW5kZWZpbmVkIDogYXJyYXlbaWR4XTtcclxufVxyXG5mdW5jdGlvbiBtZXJnZSh0YXJnZXQsIHNvdXJjZSkge1xyXG4gICAgT2JqZWN0LmtleXMoc291cmNlKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcclxuICAgICAgICBpZiAoaXNPYmplY3Qoc291cmNlW2tleV0pKSB7XHJcbiAgICAgICAgICAgIGlmICghdGFyZ2V0W2tleV0pIHtcclxuICAgICAgICAgICAgICAgIHRhcmdldFtrZXldID0ge307XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgbWVyZ2UodGFyZ2V0W2tleV0sIHNvdXJjZVtrZXldKTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0YXJnZXRba2V5XSA9IHNvdXJjZVtrZXldO1xyXG4gICAgfSk7XHJcbiAgICByZXR1cm4gdGFyZ2V0O1xyXG59XHJcbmZ1bmN0aW9uIHZhbHVlcyhvYmopIHtcclxuICAgIGlmIChpc0NhbGxhYmxlKE9iamVjdC52YWx1ZXMpKSB7XHJcbiAgICAgICAgcmV0dXJuIE9iamVjdC52YWx1ZXMob2JqKTtcclxuICAgIH1cclxuICAgIC8vIGZhbGxiYWNrIHRvIGtleXMoKVxyXG4gICAgLyogaXN0YW5idWwgaWdub3JlIG5leHQgKi9cclxuICAgIHJldHVybiBPYmplY3Qua2V5cyhvYmopLm1hcChmdW5jdGlvbiAoaykgeyByZXR1cm4gb2JqW2tdOyB9KTtcclxufVxyXG52YXIgaXNFbXB0eUFycmF5ID0gZnVuY3Rpb24gKGFycikge1xyXG4gICAgcmV0dXJuIEFycmF5LmlzQXJyYXkoYXJyKSAmJiBhcnIubGVuZ3RoID09PSAwO1xyXG59O1xyXG52YXIgaW50ZXJwb2xhdGUgPSBmdW5jdGlvbiAodGVtcGxhdGUsIHZhbHVlcykge1xyXG4gICAgcmV0dXJuIHRlbXBsYXRlLnJlcGxhY2UoL1xceyhbXn1dKylcXH0vZywgZnVuY3Rpb24gKF8sIHApIHtcclxuICAgICAgICByZXR1cm4gcCBpbiB2YWx1ZXMgPyB2YWx1ZXNbcF0gOiBcIntcIiArIHAgKyBcIn1cIjtcclxuICAgIH0pO1xyXG59O1xyXG4vLyBDaGVja3MgaWYgYSBnaXZlbiB2YWx1ZSBpcyBub3QgYW4gZW1wdHkgc3RyaW5nIG9yIG51bGwgb3IgdW5kZWZpbmVkLlxyXG52YXIgaXNTcGVjaWZpZWQgPSBmdW5jdGlvbiAodmFsKSB7XHJcbiAgICBpZiAodmFsID09PSAnJykge1xyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuICAgIHJldHVybiAhaXNOdWxsT3JVbmRlZmluZWQodmFsKTtcclxufTtcblxudmFyIFJVTEVTID0ge307XHJcbmZ1bmN0aW9uIG5vcm1hbGl6ZVNjaGVtYShzY2hlbWEpIHtcclxuICAgIGlmIChzY2hlbWEucGFyYW1zICYmIHNjaGVtYS5wYXJhbXMubGVuZ3RoKSB7XHJcbiAgICAgICAgc2NoZW1hLnBhcmFtcyA9IHNjaGVtYS5wYXJhbXMubWFwKGZ1bmN0aW9uIChwYXJhbSkge1xyXG4gICAgICAgICAgICBpZiAodHlwZW9mIHBhcmFtID09PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHsgbmFtZTogcGFyYW0gfTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gcGFyYW07XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gc2NoZW1hO1xyXG59XHJcbnZhciBSdWxlQ29udGFpbmVyID0gLyoqIEBjbGFzcyAqLyAoZnVuY3Rpb24gKCkge1xyXG4gICAgZnVuY3Rpb24gUnVsZUNvbnRhaW5lcigpIHtcclxuICAgIH1cclxuICAgIFJ1bGVDb250YWluZXIuZXh0ZW5kID0gZnVuY3Rpb24gKG5hbWUsIHNjaGVtYSkge1xyXG4gICAgICAgIC8vIGlmIHJ1bGUgYWxyZWFkeSBleGlzdHMsIG92ZXJ3cml0ZSBpdC5cclxuICAgICAgICB2YXIgcnVsZSA9IG5vcm1hbGl6ZVNjaGVtYShzY2hlbWEpO1xyXG4gICAgICAgIGlmIChSVUxFU1tuYW1lXSkge1xyXG4gICAgICAgICAgICBSVUxFU1tuYW1lXSA9IG1lcmdlKFJVTEVTW25hbWVdLCBzY2hlbWEpO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFJVTEVTW25hbWVdID0gX19hc3NpZ24oeyBsYXp5OiBmYWxzZSwgY29tcHV0ZXNSZXF1aXJlZDogZmFsc2UgfSwgcnVsZSk7XHJcbiAgICB9O1xyXG4gICAgUnVsZUNvbnRhaW5lci5pdGVyYXRlID0gZnVuY3Rpb24gKGZuKSB7XHJcbiAgICAgICAgdmFyIGtleXMgPSBPYmplY3Qua2V5cyhSVUxFUyk7XHJcbiAgICAgICAgdmFyIGxlbmd0aCA9IGtleXMubGVuZ3RoO1xyXG4gICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgbGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgZm4oa2V5c1tpXSwgUlVMRVNba2V5c1tpXV0pO1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcbiAgICBSdWxlQ29udGFpbmVyLmlzTGF6eSA9IGZ1bmN0aW9uIChuYW1lKSB7XHJcbiAgICAgICAgcmV0dXJuICEhKFJVTEVTW25hbWVdICYmIFJVTEVTW25hbWVdLmxhenkpO1xyXG4gICAgfTtcclxuICAgIFJ1bGVDb250YWluZXIuaXNSZXF1aXJlUnVsZSA9IGZ1bmN0aW9uIChuYW1lKSB7XHJcbiAgICAgICAgcmV0dXJuICEhKFJVTEVTW25hbWVdICYmIFJVTEVTW25hbWVdLmNvbXB1dGVzUmVxdWlyZWQpO1xyXG4gICAgfTtcclxuICAgIFJ1bGVDb250YWluZXIuaXNUYXJnZXRSdWxlID0gZnVuY3Rpb24gKG5hbWUpIHtcclxuICAgICAgICB2YXIgZGVmaW5pdGlvbiA9IFJ1bGVDb250YWluZXIuZ2V0UnVsZURlZmluaXRpb24obmFtZSk7XHJcbiAgICAgICAgaWYgKCFkZWZpbml0aW9uIHx8ICFkZWZpbml0aW9uLnBhcmFtcykge1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBkZWZpbml0aW9uLnBhcmFtcy5zb21lKGZ1bmN0aW9uIChwYXJhbSkgeyByZXR1cm4gISFwYXJhbS5pc1RhcmdldDsgfSk7XHJcbiAgICB9O1xyXG4gICAgUnVsZUNvbnRhaW5lci5nZXRUYXJnZXRQYXJhbU5hbWVzID0gZnVuY3Rpb24gKHJ1bGUsIHBhcmFtcykge1xyXG4gICAgICAgIHZhciBkZWZpbml0aW9uID0gUnVsZUNvbnRhaW5lci5nZXRSdWxlRGVmaW5pdGlvbihydWxlKTtcclxuICAgICAgICBpZiAoQXJyYXkuaXNBcnJheShwYXJhbXMpKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBwYXJhbXMuZmlsdGVyKGZ1bmN0aW9uIChfLCBpZHgpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBkZWZpbml0aW9uLnBhcmFtcyAmJiBmaW5kKGRlZmluaXRpb24ucGFyYW1zLCBmdW5jdGlvbiAocCwgaSkgeyByZXR1cm4gISFwLmlzVGFyZ2V0ICYmIGkgPT09IGlkeDsgfSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gT2JqZWN0LmtleXMocGFyYW1zKVxyXG4gICAgICAgICAgICAuZmlsdGVyKGZ1bmN0aW9uIChrZXkpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGRlZmluaXRpb24ucGFyYW1zICYmIGZpbmQoZGVmaW5pdGlvbi5wYXJhbXMsIGZ1bmN0aW9uIChwKSB7IHJldHVybiAhIXAuaXNUYXJnZXQgJiYgcC5uYW1lID09PSBrZXk7IH0pO1xyXG4gICAgICAgIH0pXHJcbiAgICAgICAgICAgIC5tYXAoZnVuY3Rpb24gKGtleSkgeyByZXR1cm4gcGFyYW1zW2tleV07IH0pO1xyXG4gICAgfTtcclxuICAgIFJ1bGVDb250YWluZXIuZ2V0UnVsZURlZmluaXRpb24gPSBmdW5jdGlvbiAocnVsZU5hbWUpIHtcclxuICAgICAgICByZXR1cm4gUlVMRVNbcnVsZU5hbWVdO1xyXG4gICAgfTtcclxuICAgIHJldHVybiBSdWxlQ29udGFpbmVyO1xyXG59KCkpO1xyXG4vKipcclxuICogQWRkcyBhIGN1c3RvbSB2YWxpZGF0b3IgdG8gdGhlIGxpc3Qgb2YgdmFsaWRhdGlvbiBydWxlcy5cclxuICovXHJcbmZ1bmN0aW9uIGV4dGVuZChuYW1lLCBzY2hlbWEpIHtcclxuICAgIC8vIG1ha2VzIHN1cmUgbmV3IHJ1bGVzIGFyZSBwcm9wZXJseSBmb3JtYXR0ZWQuXHJcbiAgICBndWFyZEV4dGVuZChuYW1lLCBzY2hlbWEpO1xyXG4gICAgLy8gRnVsbCBzY2hlbWEgb2JqZWN0LlxyXG4gICAgaWYgKHR5cGVvZiBzY2hlbWEgPT09ICdvYmplY3QnKSB7XHJcbiAgICAgICAgUnVsZUNvbnRhaW5lci5leHRlbmQobmFtZSwgc2NoZW1hKTtcclxuICAgICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICBSdWxlQ29udGFpbmVyLmV4dGVuZChuYW1lLCB7XHJcbiAgICAgICAgdmFsaWRhdGU6IHNjaGVtYVxyXG4gICAgfSk7XHJcbn1cclxuLyoqXHJcbiAqIEd1YXJkcyBmcm9tIGV4dGVuc2lvbiB2aW9sYXRpb25zLlxyXG4gKi9cclxuZnVuY3Rpb24gZ3VhcmRFeHRlbmQobmFtZSwgdmFsaWRhdG9yKSB7XHJcbiAgICBpZiAoaXNDYWxsYWJsZSh2YWxpZGF0b3IpKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgaWYgKGlzQ2FsbGFibGUodmFsaWRhdG9yLnZhbGlkYXRlKSkge1xyXG4gICAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIGlmIChSdWxlQ29udGFpbmVyLmdldFJ1bGVEZWZpbml0aW9uKG5hbWUpKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgdGhyb3cgbmV3IEVycm9yKFwiRXh0ZW5zaW9uIEVycm9yOiBUaGUgdmFsaWRhdG9yICdcIiArIG5hbWUgKyBcIicgbXVzdCBiZSBhIGZ1bmN0aW9uIG9yIGhhdmUgYSAndmFsaWRhdGUnIG1ldGhvZC5cIik7XHJcbn1cblxudmFyIERFRkFVTFRfQ09ORklHID0ge1xyXG4gICAgZGVmYXVsdE1lc3NhZ2U6IFwie19maWVsZF99IGlzIG5vdCB2YWxpZC5cIixcclxuICAgIHNraXBPcHRpb25hbDogdHJ1ZSxcclxuICAgIGNsYXNzZXM6IHtcclxuICAgICAgICB0b3VjaGVkOiAndG91Y2hlZCcsXHJcbiAgICAgICAgdW50b3VjaGVkOiAndW50b3VjaGVkJyxcclxuICAgICAgICB2YWxpZDogJ3ZhbGlkJyxcclxuICAgICAgICBpbnZhbGlkOiAnaW52YWxpZCcsXHJcbiAgICAgICAgcHJpc3RpbmU6ICdwcmlzdGluZScsXHJcbiAgICAgICAgZGlydHk6ICdkaXJ0eScgLy8gY29udHJvbCBoYXMgYmVlbiBpbnRlcmFjdGVkIHdpdGhcclxuICAgIH0sXHJcbiAgICBiYWlsczogdHJ1ZSxcclxuICAgIG1vZGU6ICdhZ2dyZXNzaXZlJyxcclxuICAgIHVzZUNvbnN0cmFpbnRBdHRyczogdHJ1ZVxyXG59O1xyXG52YXIgY3VycmVudENvbmZpZyA9IF9fYXNzaWduKHt9LCBERUZBVUxUX0NPTkZJRyk7XHJcbnZhciBnZXRDb25maWcgPSBmdW5jdGlvbiAoKSB7IHJldHVybiBjdXJyZW50Q29uZmlnOyB9O1xyXG52YXIgc2V0Q29uZmlnID0gZnVuY3Rpb24gKG5ld0NvbmYpIHtcclxuICAgIGN1cnJlbnRDb25maWcgPSBfX2Fzc2lnbihfX2Fzc2lnbih7fSwgY3VycmVudENvbmZpZyksIG5ld0NvbmYpO1xyXG59O1xyXG52YXIgY29uZmlndXJlID0gZnVuY3Rpb24gKGNmZykge1xyXG4gICAgc2V0Q29uZmlnKGNmZyk7XHJcbn07XG5cbi8qKlxyXG4gKiBWYWxpZGF0ZXMgYSB2YWx1ZSBhZ2FpbnN0IHRoZSBydWxlcy5cclxuICovXHJcbmZ1bmN0aW9uIHZhbGlkYXRlKHZhbHVlLCBydWxlcywgb3B0aW9ucykge1xyXG4gICAgaWYgKG9wdGlvbnMgPT09IHZvaWQgMCkgeyBvcHRpb25zID0ge307IH1cclxuICAgIHJldHVybiBfX2F3YWl0ZXIodGhpcywgdm9pZCAwLCB2b2lkIDAsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICB2YXIgc2hvdWxkQmFpbCwgc2tpcElmRW1wdHksIGZpZWxkLCByZXN1bHQsIGVycm9ycywgcnVsZU1hcDtcclxuICAgICAgICByZXR1cm4gX19nZW5lcmF0b3IodGhpcywgZnVuY3Rpb24gKF9hKSB7XHJcbiAgICAgICAgICAgIHN3aXRjaCAoX2EubGFiZWwpIHtcclxuICAgICAgICAgICAgICAgIGNhc2UgMDpcclxuICAgICAgICAgICAgICAgICAgICBzaG91bGRCYWlsID0gb3B0aW9ucyAmJiBvcHRpb25zLmJhaWxzO1xyXG4gICAgICAgICAgICAgICAgICAgIHNraXBJZkVtcHR5ID0gb3B0aW9ucyAmJiBvcHRpb25zLnNraXBJZkVtcHR5O1xyXG4gICAgICAgICAgICAgICAgICAgIGZpZWxkID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiAob3B0aW9ucyAmJiBvcHRpb25zLm5hbWUpIHx8ICd7ZmllbGR9JyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcnVsZXM6IG5vcm1hbGl6ZVJ1bGVzKHJ1bGVzKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgYmFpbHM6IGlzTnVsbE9yVW5kZWZpbmVkKHNob3VsZEJhaWwpID8gdHJ1ZSA6IHNob3VsZEJhaWwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNraXBJZkVtcHR5OiBpc051bGxPclVuZGVmaW5lZChza2lwSWZFbXB0eSkgPyB0cnVlIDogc2tpcElmRW1wdHksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvcmNlUmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjcm9zc1RhYmxlOiAob3B0aW9ucyAmJiBvcHRpb25zLnZhbHVlcykgfHwge30sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG5hbWVzOiAob3B0aW9ucyAmJiBvcHRpb25zLm5hbWVzKSB8fCB7fSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY3VzdG9tTWVzc2FnZXM6IChvcHRpb25zICYmIG9wdGlvbnMuY3VzdG9tTWVzc2FnZXMpIHx8IHt9XHJcbiAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gWzQgLyp5aWVsZCovLCBfdmFsaWRhdGUoZmllbGQsIHZhbHVlLCBvcHRpb25zKV07XHJcbiAgICAgICAgICAgICAgICBjYXNlIDE6XHJcbiAgICAgICAgICAgICAgICAgICAgcmVzdWx0ID0gX2Euc2VudCgpO1xyXG4gICAgICAgICAgICAgICAgICAgIGVycm9ycyA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgIHJ1bGVNYXAgPSB7fTtcclxuICAgICAgICAgICAgICAgICAgICByZXN1bHQuZXJyb3JzLmZvckVhY2goZnVuY3Rpb24gKGUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZXJyb3JzLnB1c2goZS5tc2cpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBydWxlTWFwW2UucnVsZV0gPSBlLm1zZztcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gWzIgLypyZXR1cm4qLywge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsaWQ6IHJlc3VsdC52YWxpZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yczogZXJyb3JzLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZmFpbGVkUnVsZXM6IHJ1bGVNYXBcclxuICAgICAgICAgICAgICAgICAgICAgICAgfV07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH0pO1xyXG59XHJcbi8qKlxyXG4gKiBTdGFydHMgdGhlIHZhbGlkYXRpb24gcHJvY2Vzcy5cclxuICovXHJcbmZ1bmN0aW9uIF92YWxpZGF0ZShmaWVsZCwgdmFsdWUsIF9hKSB7XHJcbiAgICB2YXIgX2IgPSAoX2EgPT09IHZvaWQgMCA/IHt9IDogX2EpLmlzSW5pdGlhbCwgaXNJbml0aWFsID0gX2IgPT09IHZvaWQgMCA/IGZhbHNlIDogX2I7XHJcbiAgICByZXR1cm4gX19hd2FpdGVyKHRoaXMsIHZvaWQgMCwgdm9pZCAwLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdmFyIF9jLCBzaG91bGRTa2lwLCBlcnJvcnMsIHJ1bGVzLCBsZW5ndGgsIGksIHJ1bGUsIHJlc3VsdDtcclxuICAgICAgICByZXR1cm4gX19nZW5lcmF0b3IodGhpcywgZnVuY3Rpb24gKF9kKSB7XHJcbiAgICAgICAgICAgIHN3aXRjaCAoX2QubGFiZWwpIHtcclxuICAgICAgICAgICAgICAgIGNhc2UgMDogcmV0dXJuIFs0IC8qeWllbGQqLywgX3Nob3VsZFNraXAoZmllbGQsIHZhbHVlKV07XHJcbiAgICAgICAgICAgICAgICBjYXNlIDE6XHJcbiAgICAgICAgICAgICAgICAgICAgX2MgPSBfZC5zZW50KCksIHNob3VsZFNraXAgPSBfYy5zaG91bGRTa2lwLCBlcnJvcnMgPSBfYy5lcnJvcnM7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHNob3VsZFNraXApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFsyIC8qcmV0dXJuKi8sIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWxpZDogIWVycm9ycy5sZW5ndGgsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZXJyb3JzOiBlcnJvcnNcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1dO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBydWxlcyA9IE9iamVjdC5rZXlzKGZpZWxkLnJ1bGVzKS5maWx0ZXIoZnVuY3Rpb24gKHJ1bGUpIHsgcmV0dXJuICFSdWxlQ29udGFpbmVyLmlzUmVxdWlyZVJ1bGUocnVsZSk7IH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIGxlbmd0aCA9IHJ1bGVzLmxlbmd0aDtcclxuICAgICAgICAgICAgICAgICAgICBpID0gMDtcclxuICAgICAgICAgICAgICAgICAgICBfZC5sYWJlbCA9IDI7XHJcbiAgICAgICAgICAgICAgICBjYXNlIDI6XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCEoaSA8IGxlbmd0aCkpIHJldHVybiBbMyAvKmJyZWFrKi8sIDVdO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChpc0luaXRpYWwgJiYgUnVsZUNvbnRhaW5lci5pc0xhenkocnVsZXNbaV0pKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBbMyAvKmJyZWFrKi8sIDRdO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBydWxlID0gcnVsZXNbaV07XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFs0IC8qeWllbGQqLywgX3Rlc3QoZmllbGQsIHZhbHVlLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiBydWxlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFyYW1zOiBmaWVsZC5ydWxlc1tydWxlXVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KV07XHJcbiAgICAgICAgICAgICAgICBjYXNlIDM6XHJcbiAgICAgICAgICAgICAgICAgICAgcmVzdWx0ID0gX2Quc2VudCgpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICghcmVzdWx0LnZhbGlkICYmIHJlc3VsdC5lcnJvcikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBlcnJvcnMucHVzaChyZXN1bHQuZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoZmllbGQuYmFpbHMpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBbMiAvKnJldHVybiovLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbGlkOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZXJyb3JzOiBlcnJvcnNcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBfZC5sYWJlbCA9IDQ7XHJcbiAgICAgICAgICAgICAgICBjYXNlIDQ6XHJcbiAgICAgICAgICAgICAgICAgICAgaSsrO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBbMyAvKmJyZWFrKi8sIDJdO1xyXG4gICAgICAgICAgICAgICAgY2FzZSA1OiByZXR1cm4gWzIgLypyZXR1cm4qLywge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWxpZDogIWVycm9ycy5sZW5ndGgsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yczogZXJyb3JzXHJcbiAgICAgICAgICAgICAgICAgICAgfV07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH0pO1xyXG59XHJcbmZ1bmN0aW9uIF9zaG91bGRTa2lwKGZpZWxkLCB2YWx1ZSkge1xyXG4gICAgcmV0dXJuIF9fYXdhaXRlcih0aGlzLCB2b2lkIDAsIHZvaWQgMCwgZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHZhciByZXF1aXJlUnVsZXMsIGxlbmd0aCwgZXJyb3JzLCBpc0VtcHR5LCBpc0VtcHR5QW5kT3B0aW9uYWwsIGlzUmVxdWlyZWQsIGksIHJ1bGUsIHJlc3VsdDtcclxuICAgICAgICByZXR1cm4gX19nZW5lcmF0b3IodGhpcywgZnVuY3Rpb24gKF9hKSB7XHJcbiAgICAgICAgICAgIHN3aXRjaCAoX2EubGFiZWwpIHtcclxuICAgICAgICAgICAgICAgIGNhc2UgMDpcclxuICAgICAgICAgICAgICAgICAgICByZXF1aXJlUnVsZXMgPSBPYmplY3Qua2V5cyhmaWVsZC5ydWxlcykuZmlsdGVyKFJ1bGVDb250YWluZXIuaXNSZXF1aXJlUnVsZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgbGVuZ3RoID0gcmVxdWlyZVJ1bGVzLmxlbmd0aDtcclxuICAgICAgICAgICAgICAgICAgICBlcnJvcnMgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICBpc0VtcHR5ID0gaXNOdWxsT3JVbmRlZmluZWQodmFsdWUpIHx8IHZhbHVlID09PSAnJyB8fCBpc0VtcHR5QXJyYXkodmFsdWUpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlzRW1wdHlBbmRPcHRpb25hbCA9IGlzRW1wdHkgJiYgZmllbGQuc2tpcElmRW1wdHk7XHJcbiAgICAgICAgICAgICAgICAgICAgaXNSZXF1aXJlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIGkgPSAwO1xyXG4gICAgICAgICAgICAgICAgICAgIF9hLmxhYmVsID0gMTtcclxuICAgICAgICAgICAgICAgIGNhc2UgMTpcclxuICAgICAgICAgICAgICAgICAgICBpZiAoIShpIDwgbGVuZ3RoKSkgcmV0dXJuIFszIC8qYnJlYWsqLywgNF07XHJcbiAgICAgICAgICAgICAgICAgICAgcnVsZSA9IHJlcXVpcmVSdWxlc1tpXTtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gWzQgLyp5aWVsZCovLCBfdGVzdChmaWVsZCwgdmFsdWUsIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IHJ1bGUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwYXJhbXM6IGZpZWxkLnJ1bGVzW3J1bGVdXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXTtcclxuICAgICAgICAgICAgICAgIGNhc2UgMjpcclxuICAgICAgICAgICAgICAgICAgICByZXN1bHQgPSBfYS5zZW50KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFpc09iamVjdChyZXN1bHQpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcignUmVxdWlyZSBydWxlcyBoYXMgdG8gcmV0dXJuIGFuIG9iamVjdCAoc2VlIGRvY3MpJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXN1bHQucmVxdWlyZWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaXNSZXF1aXJlZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIGlmICghcmVzdWx0LnZhbGlkICYmIHJlc3VsdC5lcnJvcikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBlcnJvcnMucHVzaChyZXN1bHQuZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBFeGl0IGVhcmx5IGFzIHRoZSBmaWVsZCBpcyByZXF1aXJlZCBhbmQgZmFpbGVkIHZhbGlkYXRpb24uXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChmaWVsZC5iYWlscykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFsyIC8qcmV0dXJuKi8sIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2hvdWxkU2tpcDogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZXJyb3JzOiBlcnJvcnNcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBfYS5sYWJlbCA9IDM7XHJcbiAgICAgICAgICAgICAgICBjYXNlIDM6XHJcbiAgICAgICAgICAgICAgICAgICAgaSsrO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBbMyAvKmJyZWFrKi8sIDFdO1xyXG4gICAgICAgICAgICAgICAgY2FzZSA0OlxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChpc0VtcHR5ICYmICFpc1JlcXVpcmVkICYmICFmaWVsZC5za2lwSWZFbXB0eSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gWzIgLypyZXR1cm4qLywge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNob3VsZFNraXA6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yczogZXJyb3JzXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gZmllbGQgaXMgY29uZmlndXJlZCB0byBydW4gdGhyb3VnaCB0aGUgcGlwZWxpbmUgcmVnYXJkbGVzc1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICghZmllbGQuYmFpbHMgJiYgIWlzRW1wdHlBbmRPcHRpb25hbCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gWzIgLypyZXR1cm4qLywge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNob3VsZFNraXA6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yczogZXJyb3JzXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gc2tpcCBpZiB0aGUgZmllbGQgaXMgbm90IHJlcXVpcmVkIGFuZCBoYXMgYW4gZW1wdHkgdmFsdWUuXHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFsyIC8qcmV0dXJuKi8sIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNob3VsZFNraXA6ICFpc1JlcXVpcmVkICYmIGlzRW1wdHksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBlcnJvcnM6IGVycm9yc1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfSk7XHJcbn1cclxuLyoqXHJcbiAqIFRlc3RzIGEgc2luZ2xlIGlucHV0IHZhbHVlIGFnYWluc3QgYSBydWxlLlxyXG4gKi9cclxuZnVuY3Rpb24gX3Rlc3QoZmllbGQsIHZhbHVlLCBydWxlKSB7XHJcbiAgICByZXR1cm4gX19hd2FpdGVyKHRoaXMsIHZvaWQgMCwgdm9pZCAwLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdmFyIHJ1bGVTY2hlbWEsIHBhcmFtcywgbm9ybWFsaXplZFZhbHVlLCByZXN1bHQsIHZhbHVlcztcclxuICAgICAgICByZXR1cm4gX19nZW5lcmF0b3IodGhpcywgZnVuY3Rpb24gKF9hKSB7XHJcbiAgICAgICAgICAgIHN3aXRjaCAoX2EubGFiZWwpIHtcclxuICAgICAgICAgICAgICAgIGNhc2UgMDpcclxuICAgICAgICAgICAgICAgICAgICBydWxlU2NoZW1hID0gUnVsZUNvbnRhaW5lci5nZXRSdWxlRGVmaW5pdGlvbihydWxlLm5hbWUpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICghcnVsZVNjaGVtYSB8fCAhcnVsZVNjaGVtYS52YWxpZGF0ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJObyBzdWNoIHZhbGlkYXRvciAnXCIgKyBydWxlLm5hbWUgKyBcIicgZXhpc3RzLlwiKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgcGFyYW1zID0gX2J1aWxkUGFyYW1zKHJ1bGUucGFyYW1zLCBydWxlU2NoZW1hLnBhcmFtcywgZmllbGQuY3Jvc3NUYWJsZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgbm9ybWFsaXplZFZhbHVlID0gcnVsZVNjaGVtYS5jYXN0VmFsdWUgPyBydWxlU2NoZW1hLmNhc3RWYWx1ZSh2YWx1ZSkgOiB2YWx1ZTtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gWzQgLyp5aWVsZCovLCBydWxlU2NoZW1hLnZhbGlkYXRlKG5vcm1hbGl6ZWRWYWx1ZSwgcGFyYW1zKV07XHJcbiAgICAgICAgICAgICAgICBjYXNlIDE6XHJcbiAgICAgICAgICAgICAgICAgICAgcmVzdWx0ID0gX2Euc2VudCgpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0eXBlb2YgcmVzdWx0ID09PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZXMgPSBfX2Fzc2lnbihfX2Fzc2lnbih7fSwgKHBhcmFtcyB8fCB7fSkpLCB7IF9maWVsZF86IGZpZWxkLm5hbWUsIF92YWx1ZV86IHZhbHVlLCBfcnVsZV86IHJ1bGUubmFtZSB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFsyIC8qcmV0dXJuKi8sIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWxpZDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZXJyb3I6IHsgcnVsZTogcnVsZS5uYW1lLCBtc2c6IGludGVycG9sYXRlKHJlc3VsdCwgdmFsdWVzKSB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFpc09iamVjdChyZXN1bHQpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3VsdCA9IHsgdmFsaWQ6IHJlc3VsdCwgZGF0YToge30gfTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFsyIC8qcmV0dXJuKi8sIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbGlkOiByZXN1bHQudmFsaWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXF1aXJlZDogcmVzdWx0LnJlcXVpcmVkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0YTogcmVzdWx0LmRhdGEgfHwge30sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBlcnJvcjogcmVzdWx0LnZhbGlkID8gdW5kZWZpbmVkIDogX2dlbmVyYXRlRmllbGRFcnJvcihmaWVsZCwgdmFsdWUsIHJ1bGVTY2hlbWEsIHJ1bGUubmFtZSwgcGFyYW1zLCByZXN1bHQuZGF0YSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgfV07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH0pO1xyXG59XHJcbi8qKlxyXG4gKiBHZW5lcmF0ZXMgZXJyb3IgbWVzc2FnZXMuXHJcbiAqL1xyXG5mdW5jdGlvbiBfZ2VuZXJhdGVGaWVsZEVycm9yKGZpZWxkLCB2YWx1ZSwgcnVsZVNjaGVtYSwgcnVsZU5hbWUsIHBhcmFtcywgZGF0YSkge1xyXG4gICAgdmFyIHZhbHVlcyA9IF9fYXNzaWduKF9fYXNzaWduKF9fYXNzaWduKF9fYXNzaWduKHt9LCAocGFyYW1zIHx8IHt9KSksIChkYXRhIHx8IHt9KSksIHsgX2ZpZWxkXzogZmllbGQubmFtZSwgX3ZhbHVlXzogdmFsdWUsIF9ydWxlXzogcnVsZU5hbWUgfSksIF9nZXRUYXJnZXROYW1lcyhmaWVsZCwgcnVsZVNjaGVtYSwgcnVsZU5hbWUpKTtcclxuICAgIGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoZmllbGQuY3VzdG9tTWVzc2FnZXMsIHJ1bGVOYW1lKSAmJlxyXG4gICAgICAgIHR5cGVvZiBmaWVsZC5jdXN0b21NZXNzYWdlc1tydWxlTmFtZV0gPT09ICdzdHJpbmcnKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgbXNnOiBfbm9ybWFsaXplTWVzc2FnZShmaWVsZC5jdXN0b21NZXNzYWdlc1tydWxlTmFtZV0sIGZpZWxkLm5hbWUsIHZhbHVlcyksXHJcbiAgICAgICAgICAgIHJ1bGU6IHJ1bGVOYW1lXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuICAgIGlmIChydWxlU2NoZW1hLm1lc3NhZ2UpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBtc2c6IF9ub3JtYWxpemVNZXNzYWdlKHJ1bGVTY2hlbWEubWVzc2FnZSwgZmllbGQubmFtZSwgdmFsdWVzKSxcclxuICAgICAgICAgICAgcnVsZTogcnVsZU5hbWVcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHtcclxuICAgICAgICBtc2c6IF9ub3JtYWxpemVNZXNzYWdlKGdldENvbmZpZygpLmRlZmF1bHRNZXNzYWdlLCBmaWVsZC5uYW1lLCB2YWx1ZXMpLFxyXG4gICAgICAgIHJ1bGU6IHJ1bGVOYW1lXHJcbiAgICB9O1xyXG59XHJcbmZ1bmN0aW9uIF9nZXRUYXJnZXROYW1lcyhmaWVsZCwgcnVsZVNjaGVtYSwgcnVsZU5hbWUpIHtcclxuICAgIGlmIChydWxlU2NoZW1hLnBhcmFtcykge1xyXG4gICAgICAgIHZhciBudW1UYXJnZXRzID0gcnVsZVNjaGVtYS5wYXJhbXMuZmlsdGVyKGZ1bmN0aW9uIChwYXJhbSkgeyByZXR1cm4gcGFyYW0uaXNUYXJnZXQ7IH0pLmxlbmd0aDtcclxuICAgICAgICBpZiAobnVtVGFyZ2V0cyA+IDApIHtcclxuICAgICAgICAgICAgdmFyIG5hbWVzID0ge307XHJcbiAgICAgICAgICAgIGZvciAodmFyIGluZGV4ID0gMDsgaW5kZXggPCBydWxlU2NoZW1hLnBhcmFtcy5sZW5ndGg7IGluZGV4KyspIHtcclxuICAgICAgICAgICAgICAgIHZhciBwYXJhbSA9IHJ1bGVTY2hlbWEucGFyYW1zW2luZGV4XTtcclxuICAgICAgICAgICAgICAgIGlmIChwYXJhbS5pc1RhcmdldCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHZhciBrZXkgPSBmaWVsZC5ydWxlc1tydWxlTmFtZV1baW5kZXhdO1xyXG4gICAgICAgICAgICAgICAgICAgIHZhciBuYW1lXzEgPSBmaWVsZC5uYW1lc1trZXldIHx8IGtleTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAobnVtVGFyZ2V0cyA9PT0gMSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBuYW1lcy5fdGFyZ2V0XyA9IG5hbWVfMTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBuYW1lc1tcIl9cIiArIHBhcmFtLm5hbWUgKyBcIlRhcmdldF9cIl0gPSBuYW1lXzE7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybiBuYW1lcztcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICByZXR1cm4ge307XHJcbn1cclxuZnVuY3Rpb24gX25vcm1hbGl6ZU1lc3NhZ2UodGVtcGxhdGUsIGZpZWxkLCB2YWx1ZXMpIHtcclxuICAgIGlmICh0eXBlb2YgdGVtcGxhdGUgPT09ICdmdW5jdGlvbicpIHtcclxuICAgICAgICByZXR1cm4gdGVtcGxhdGUoZmllbGQsIHZhbHVlcyk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gaW50ZXJwb2xhdGUodGVtcGxhdGUsIF9fYXNzaWduKF9fYXNzaWduKHt9LCB2YWx1ZXMpLCB7IF9maWVsZF86IGZpZWxkIH0pKTtcclxufVxyXG5mdW5jdGlvbiBfYnVpbGRQYXJhbXMocHJvdmlkZWQsIGRlZmluZWQsIGNyb3NzVGFibGUpIHtcclxuICAgIHZhciBwYXJhbXMgPSB7fTtcclxuICAgIGlmICghZGVmaW5lZCAmJiAhQXJyYXkuaXNBcnJheShwcm92aWRlZCkpIHtcclxuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1lvdSBwcm92aWRlZCBhbiBvYmplY3QgcGFyYW1zIHRvIGEgcnVsZSB0aGF0IGhhcyBubyBkZWZpbmVkIHNjaGVtYS4nKTtcclxuICAgIH1cclxuICAgIC8vIFJ1bGUgcHJvYmFibHkgdXNlcyBhbiBhcnJheSBmb3IgdGhlaXIgYXJncywga2VlcCBpdCBhcyBpcy5cclxuICAgIGlmIChBcnJheS5pc0FycmF5KHByb3ZpZGVkKSAmJiAhZGVmaW5lZCkge1xyXG4gICAgICAgIHJldHVybiBwcm92aWRlZDtcclxuICAgIH1cclxuICAgIHZhciBkZWZpbmVkUnVsZXM7XHJcbiAgICAvLyBjb2xsZWN0IHRoZSBwYXJhbXMgc2NoZW1hLlxyXG4gICAgaWYgKCFkZWZpbmVkIHx8IChkZWZpbmVkLmxlbmd0aCA8IHByb3ZpZGVkLmxlbmd0aCAmJiBBcnJheS5pc0FycmF5KHByb3ZpZGVkKSkpIHtcclxuICAgICAgICB2YXIgbGFzdERlZmluZWRQYXJhbV8xO1xyXG4gICAgICAgIC8vIGNvbGxlY3QgYW55IGFkZGl0aW9uYWwgcGFyYW1ldGVycyBpbiB0aGUgbGFzdCBpdGVtLlxyXG4gICAgICAgIGRlZmluZWRSdWxlcyA9IHByb3ZpZGVkLm1hcChmdW5jdGlvbiAoXywgaWR4KSB7XHJcbiAgICAgICAgICAgIHZhciBwYXJhbSA9IGRlZmluZWQgJiYgZGVmaW5lZFtpZHhdO1xyXG4gICAgICAgICAgICBsYXN0RGVmaW5lZFBhcmFtXzEgPSBwYXJhbSB8fCBsYXN0RGVmaW5lZFBhcmFtXzE7XHJcbiAgICAgICAgICAgIGlmICghcGFyYW0pIHtcclxuICAgICAgICAgICAgICAgIHBhcmFtID0gbGFzdERlZmluZWRQYXJhbV8xO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybiBwYXJhbTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuICAgIGVsc2Uge1xyXG4gICAgICAgIGRlZmluZWRSdWxlcyA9IGRlZmluZWQ7XHJcbiAgICB9XHJcbiAgICAvLyBNYXRjaCB0aGUgcHJvdmlkZWQgYXJyYXkgbGVuZ3RoIHdpdGggYSB0ZW1wb3Jhcnkgc2NoZW1hLlxyXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBkZWZpbmVkUnVsZXMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICB2YXIgb3B0aW9ucyA9IGRlZmluZWRSdWxlc1tpXTtcclxuICAgICAgICB2YXIgdmFsdWUgPSBvcHRpb25zLmRlZmF1bHQ7XHJcbiAgICAgICAgLy8gaWYgdGhlIHByb3ZpZGVkIGlzIGFuIGFycmF5LCBtYXAgZWxlbWVudCB2YWx1ZS5cclxuICAgICAgICBpZiAoQXJyYXkuaXNBcnJheShwcm92aWRlZCkpIHtcclxuICAgICAgICAgICAgaWYgKGkgaW4gcHJvdmlkZWQpIHtcclxuICAgICAgICAgICAgICAgIHZhbHVlID0gcHJvdmlkZWRbaV07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgIC8vIElmIHRoZSBwYXJhbSBleGlzdHMgaW4gdGhlIHByb3ZpZGVkIG9iamVjdC5cclxuICAgICAgICAgICAgaWYgKG9wdGlvbnMubmFtZSBpbiBwcm92aWRlZCkge1xyXG4gICAgICAgICAgICAgICAgdmFsdWUgPSBwcm92aWRlZFtvcHRpb25zLm5hbWVdO1xyXG4gICAgICAgICAgICAgICAgLy8gaWYgdGhlIHByb3ZpZGVkIGlzIHRoZSBmaXJzdCBwYXJhbSB2YWx1ZS5cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBlbHNlIGlmIChkZWZpbmVkUnVsZXMubGVuZ3RoID09PSAxKSB7XHJcbiAgICAgICAgICAgICAgICB2YWx1ZSA9IHByb3ZpZGVkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vIGlmIHRoZSBwYXJhbSBpcyBhIHRhcmdldCwgcmVzb2x2ZSB0aGUgdGFyZ2V0IHZhbHVlLlxyXG4gICAgICAgIGlmIChvcHRpb25zLmlzVGFyZ2V0KSB7XHJcbiAgICAgICAgICAgIHZhbHVlID0gY3Jvc3NUYWJsZVt2YWx1ZV07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vIElmIHRoZXJlIGlzIGEgdHJhbnNmb3JtZXIgZGVmaW5lZC5cclxuICAgICAgICBpZiAob3B0aW9ucy5jYXN0KSB7XHJcbiAgICAgICAgICAgIHZhbHVlID0gb3B0aW9ucy5jYXN0KHZhbHVlKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLy8gYWxyZWFkeSBiZWVuIHNldCwgcHJvYmFibHkgbXVsdGlwbGUgdmFsdWVzLlxyXG4gICAgICAgIGlmIChwYXJhbXNbb3B0aW9ucy5uYW1lXSkge1xyXG4gICAgICAgICAgICBwYXJhbXNbb3B0aW9ucy5uYW1lXSA9IEFycmF5LmlzQXJyYXkocGFyYW1zW29wdGlvbnMubmFtZV0pID8gcGFyYW1zW29wdGlvbnMubmFtZV0gOiBbcGFyYW1zW29wdGlvbnMubmFtZV1dO1xyXG4gICAgICAgICAgICBwYXJhbXNbb3B0aW9ucy5uYW1lXS5wdXNoKHZhbHVlKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgIC8vIHNldCB0aGUgdmFsdWUuXHJcbiAgICAgICAgICAgIHBhcmFtc1tvcHRpb25zLm5hbWVdID0gdmFsdWU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgcmV0dXJuIHBhcmFtcztcclxufVxuXG5mdW5jdGlvbiBpbnN0YWxsKF8sIGNvbmZpZykge1xyXG4gICAgc2V0Q29uZmlnKGNvbmZpZyk7XHJcbn1cblxudmFyIGFnZ3Jlc3NpdmUgPSBmdW5jdGlvbiAoKSB7IHJldHVybiAoe1xyXG4gICAgb246IFsnaW5wdXQnLCAnYmx1ciddXHJcbn0pOyB9O1xyXG52YXIgbGF6eSA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuICh7XHJcbiAgICBvbjogWydjaGFuZ2UnXVxyXG59KTsgfTtcclxudmFyIGVhZ2VyID0gZnVuY3Rpb24gKF9hKSB7XHJcbiAgICB2YXIgZXJyb3JzID0gX2EuZXJyb3JzO1xyXG4gICAgaWYgKGVycm9ycy5sZW5ndGgpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBvbjogWydpbnB1dCcsICdjaGFuZ2UnXVxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICAgIG9uOiBbJ2NoYW5nZScsICdibHVyJ11cclxuICAgIH07XHJcbn07XHJcbnZhciBwYXNzaXZlID0gZnVuY3Rpb24gKCkgeyByZXR1cm4gKHtcclxuICAgIG9uOiBbXVxyXG59KTsgfTtcclxudmFyIG1vZGVzID0ge1xyXG4gICAgYWdncmVzc2l2ZTogYWdncmVzc2l2ZSxcclxuICAgIGVhZ2VyOiBlYWdlcixcclxuICAgIHBhc3NpdmU6IHBhc3NpdmUsXHJcbiAgICBsYXp5OiBsYXp5XHJcbn07XHJcbnZhciBzZXRJbnRlcmFjdGlvbk1vZGUgPSBmdW5jdGlvbiAobW9kZSwgaW1wbGVtZW50YXRpb24pIHtcclxuICAgIHNldENvbmZpZyh7IG1vZGU6IG1vZGUgfSk7XHJcbiAgICBpZiAoIWltcGxlbWVudGF0aW9uKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgaWYgKCFpc0NhbGxhYmxlKGltcGxlbWVudGF0aW9uKSkge1xyXG4gICAgICAgIHRocm93IG5ldyBFcnJvcignQSBtb2RlIGltcGxlbWVudGF0aW9uIG11c3QgYmUgYSBmdW5jdGlvbicpO1xyXG4gICAgfVxyXG4gICAgbW9kZXNbbW9kZV0gPSBpbXBsZW1lbnRhdGlvbjtcclxufTtcblxudmFyIERpY3Rpb25hcnkgPSAvKiogQGNsYXNzICovIChmdW5jdGlvbiAoKSB7XHJcbiAgICBmdW5jdGlvbiBEaWN0aW9uYXJ5KGxvY2FsZSwgZGljdGlvbmFyeSkge1xyXG4gICAgICAgIHRoaXMuY29udGFpbmVyID0ge307XHJcbiAgICAgICAgdGhpcy5sb2NhbGUgPSBsb2NhbGU7XHJcbiAgICAgICAgdGhpcy5tZXJnZShkaWN0aW9uYXJ5KTtcclxuICAgIH1cclxuICAgIERpY3Rpb25hcnkucHJvdG90eXBlLnJlc29sdmUgPSBmdW5jdGlvbiAoZmllbGQsIHJ1bGUsIHZhbHVlcykge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmZvcm1hdCh0aGlzLmxvY2FsZSwgZmllbGQsIHJ1bGUsIHZhbHVlcyk7XHJcbiAgICB9O1xyXG4gICAgRGljdGlvbmFyeS5wcm90b3R5cGUuX2hhc0xvY2FsZSA9IGZ1bmN0aW9uIChsb2NhbGUpIHtcclxuICAgICAgICByZXR1cm4gISF0aGlzLmNvbnRhaW5lcltsb2NhbGVdO1xyXG4gICAgfTtcclxuICAgIERpY3Rpb25hcnkucHJvdG90eXBlLmZvcm1hdCA9IGZ1bmN0aW9uIChsb2NhbGUsIGZpZWxkLCBydWxlLCB2YWx1ZXMpIHtcclxuICAgICAgICB2YXIgbWVzc2FnZTtcclxuICAgICAgICAvLyBmaW5kIGlmIHNwZWNpZmljIG1lc3NhZ2UgZm9yIHRoYXQgZmllbGQgd2FzIHNwZWNpZmllZC5cclxuICAgICAgICB2YXIgZGljdCA9IHRoaXMuY29udGFpbmVyW2xvY2FsZV0gJiYgdGhpcy5jb250YWluZXJbbG9jYWxlXS5maWVsZHMgJiYgdGhpcy5jb250YWluZXJbbG9jYWxlXS5maWVsZHNbZmllbGRdO1xyXG4gICAgICAgIGlmIChkaWN0ICYmIGRpY3RbcnVsZV0pIHtcclxuICAgICAgICAgICAgbWVzc2FnZSA9IGRpY3RbcnVsZV07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICghbWVzc2FnZSAmJiB0aGlzLl9oYXNMb2NhbGUobG9jYWxlKSAmJiB0aGlzLl9oYXNNZXNzYWdlKGxvY2FsZSwgcnVsZSkpIHtcclxuICAgICAgICAgICAgbWVzc2FnZSA9IHRoaXMuY29udGFpbmVyW2xvY2FsZV0ubWVzc2FnZXNbcnVsZV07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICghbWVzc2FnZSkge1xyXG4gICAgICAgICAgICBtZXNzYWdlID0gZ2V0Q29uZmlnKCkuZGVmYXVsdE1lc3NhZ2U7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLl9oYXNOYW1lKGxvY2FsZSwgZmllbGQpKSB7XHJcbiAgICAgICAgICAgIGZpZWxkID0gdGhpcy5nZXROYW1lKGxvY2FsZSwgZmllbGQpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gaXNDYWxsYWJsZShtZXNzYWdlKSA/IG1lc3NhZ2UoZmllbGQsIHZhbHVlcykgOiBpbnRlcnBvbGF0ZShtZXNzYWdlLCBfX2Fzc2lnbihfX2Fzc2lnbih7fSwgdmFsdWVzKSwgeyBfZmllbGRfOiBmaWVsZCB9KSk7XHJcbiAgICB9O1xyXG4gICAgRGljdGlvbmFyeS5wcm90b3R5cGUubWVyZ2UgPSBmdW5jdGlvbiAoZGljdGlvbmFyeSkge1xyXG4gICAgICAgIG1lcmdlKHRoaXMuY29udGFpbmVyLCBkaWN0aW9uYXJ5KTtcclxuICAgIH07XHJcbiAgICBEaWN0aW9uYXJ5LnByb3RvdHlwZS5oYXNSdWxlID0gZnVuY3Rpb24gKG5hbWUpIHtcclxuICAgICAgICB2YXIgbG9jYWxlID0gdGhpcy5jb250YWluZXJbdGhpcy5sb2NhbGVdO1xyXG4gICAgICAgIGlmICghbG9jYWxlKVxyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgcmV0dXJuICEhKGxvY2FsZS5tZXNzYWdlcyAmJiBsb2NhbGUubWVzc2FnZXNbbmFtZV0pO1xyXG4gICAgfTtcclxuICAgIERpY3Rpb25hcnkucHJvdG90eXBlLmdldE5hbWUgPSBmdW5jdGlvbiAobG9jYWxlLCBrZXkpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5jb250YWluZXJbbG9jYWxlXS5uYW1lc1trZXldO1xyXG4gICAgfTtcclxuICAgIERpY3Rpb25hcnkucHJvdG90eXBlLl9oYXNNZXNzYWdlID0gZnVuY3Rpb24gKGxvY2FsZSwga2V5KSB7XHJcbiAgICAgICAgcmV0dXJuICEhKHRoaXMuX2hhc0xvY2FsZShsb2NhbGUpICYmIHRoaXMuY29udGFpbmVyW2xvY2FsZV0ubWVzc2FnZXMgJiYgdGhpcy5jb250YWluZXJbbG9jYWxlXS5tZXNzYWdlc1trZXldKTtcclxuICAgIH07XHJcbiAgICBEaWN0aW9uYXJ5LnByb3RvdHlwZS5faGFzTmFtZSA9IGZ1bmN0aW9uIChsb2NhbGUsIGtleSkge1xyXG4gICAgICAgIHJldHVybiAhISh0aGlzLl9oYXNMb2NhbGUobG9jYWxlKSAmJiB0aGlzLmNvbnRhaW5lcltsb2NhbGVdLm5hbWVzICYmIHRoaXMuY29udGFpbmVyW2xvY2FsZV0ubmFtZXNba2V5XSk7XHJcbiAgICB9O1xyXG4gICAgcmV0dXJuIERpY3Rpb25hcnk7XHJcbn0oKSk7XHJcbnZhciBESUNUSU9OQVJZO1xyXG52YXIgSU5TVEFMTEVEID0gZmFsc2U7XHJcbmZ1bmN0aW9uIHVwZGF0ZVJ1bGVzKCkge1xyXG4gICAgaWYgKElOU1RBTExFRCkge1xyXG4gICAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIFJ1bGVDb250YWluZXIuaXRlcmF0ZShmdW5jdGlvbiAobmFtZSwgc2NoZW1hKSB7XHJcbiAgICAgICAgdmFyIF9hLCBfYjtcclxuICAgICAgICBpZiAoc2NoZW1hLm1lc3NhZ2UgJiYgIURJQ1RJT05BUlkuaGFzUnVsZShuYW1lKSkge1xyXG4gICAgICAgICAgICBESUNUSU9OQVJZLm1lcmdlKChfYSA9IHt9LFxyXG4gICAgICAgICAgICAgICAgX2FbRElDVElPTkFSWS5sb2NhbGVdID0ge1xyXG4gICAgICAgICAgICAgICAgICAgIG1lc3NhZ2VzOiAoX2IgPSB7fSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgX2JbbmFtZV0gPSBzY2hlbWEubWVzc2FnZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgX2IpXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgX2EpKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZXh0ZW5kKG5hbWUsIHtcclxuICAgICAgICAgICAgbWVzc2FnZTogZnVuY3Rpb24gKGZpZWxkLCB2YWx1ZXMpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBESUNUSU9OQVJZLnJlc29sdmUoZmllbGQsIG5hbWUsIHZhbHVlcyB8fCB7fSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH0pO1xyXG4gICAgSU5TVEFMTEVEID0gdHJ1ZTtcclxufVxyXG5mdW5jdGlvbiBsb2NhbGl6ZShsb2NhbGUsIGRpY3Rpb25hcnkpIHtcclxuICAgIHZhciBfYTtcclxuICAgIGlmICghRElDVElPTkFSWSkge1xyXG4gICAgICAgIERJQ1RJT05BUlkgPSBuZXcgRGljdGlvbmFyeSgnZW4nLCB7fSk7XHJcbiAgICB9XHJcbiAgICBpZiAodHlwZW9mIGxvY2FsZSA9PT0gJ3N0cmluZycpIHtcclxuICAgICAgICBESUNUSU9OQVJZLmxvY2FsZSA9IGxvY2FsZTtcclxuICAgICAgICBpZiAoZGljdGlvbmFyeSkge1xyXG4gICAgICAgICAgICBESUNUSU9OQVJZLm1lcmdlKChfYSA9IHt9LCBfYVtsb2NhbGVdID0gZGljdGlvbmFyeSwgX2EpKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdXBkYXRlUnVsZXMoKTtcclxuICAgICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICBESUNUSU9OQVJZLm1lcmdlKGxvY2FsZSk7XHJcbiAgICB1cGRhdGVSdWxlcygpO1xyXG59XG5cbnZhciBpc0V2ZW50ID0gZnVuY3Rpb24gKGV2dCkge1xyXG4gICAgaWYgKCFldnQpIHtcclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcbiAgICBpZiAodHlwZW9mIEV2ZW50ICE9PSAndW5kZWZpbmVkJyAmJiBpc0NhbGxhYmxlKEV2ZW50KSAmJiBldnQgaW5zdGFuY2VvZiBFdmVudCkge1xyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG4gICAgLy8gdGhpcyBpcyBmb3IgSUVcclxuICAgIC8qIGlzdGFuYnVsIGlnbm9yZSBuZXh0ICovXHJcbiAgICBpZiAoZXZ0ICYmIGV2dC5zcmNFbGVtZW50KSB7XHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gZmFsc2U7XHJcbn07XHJcbmZ1bmN0aW9uIG5vcm1hbGl6ZUV2ZW50VmFsdWUodmFsdWUpIHtcclxuICAgIGlmICghaXNFdmVudCh2YWx1ZSkpIHtcclxuICAgICAgICByZXR1cm4gdmFsdWU7XHJcbiAgICB9XHJcbiAgICB2YXIgaW5wdXQgPSB2YWx1ZS50YXJnZXQ7XHJcbiAgICBpZiAoaW5wdXQudHlwZSA9PT0gJ2ZpbGUnICYmIGlucHV0LmZpbGVzKSB7XHJcbiAgICAgICAgcmV0dXJuIHRvQXJyYXkoaW5wdXQuZmlsZXMpO1xyXG4gICAgfVxyXG4gICAgLy8gSWYgdGhlIGlucHV0IGhhcyBhIGB2LW1vZGVsLm51bWJlcmAgbW9kaWZpZXIgYXBwbGllZC5cclxuICAgIGlmIChpbnB1dC5fdk1vZGlmaWVycyAmJiBpbnB1dC5fdk1vZGlmaWVycy5udW1iZXIpIHtcclxuICAgICAgICAvLyBhcyBwZXIgdGhlIHNwZWMgdGhlIHYtbW9kZWwubnVtYmVyIHVzZXMgcGFyc2VGbG9hdFxyXG4gICAgICAgIHZhciB2YWx1ZUFzTnVtYmVyID0gcGFyc2VGbG9hdChpbnB1dC52YWx1ZSk7XHJcbiAgICAgICAgaWYgKGlzTmFOKHZhbHVlQXNOdW1iZXIpKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBpbnB1dC52YWx1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHZhbHVlQXNOdW1iZXI7XHJcbiAgICB9XHJcbiAgICBpZiAoaW5wdXQuX3ZNb2RpZmllcnMgJiYgaW5wdXQuX3ZNb2RpZmllcnMudHJpbSkge1xyXG4gICAgICAgIHZhciB0cmltbWVkVmFsdWUgPSB0eXBlb2YgaW5wdXQudmFsdWUgPT09ICdzdHJpbmcnID8gaW5wdXQudmFsdWUudHJpbSgpIDogaW5wdXQudmFsdWU7XHJcbiAgICAgICAgcmV0dXJuIHRyaW1tZWRWYWx1ZTtcclxuICAgIH1cclxuICAgIHJldHVybiBpbnB1dC52YWx1ZTtcclxufVxuXG52YXIgaXNUZXh0SW5wdXQgPSBmdW5jdGlvbiAodm5vZGUpIHtcclxuICAgIHZhciBhdHRycyA9ICh2bm9kZS5kYXRhICYmIHZub2RlLmRhdGEuYXR0cnMpIHx8IHZub2RlLmVsbTtcclxuICAgIC8vIGl0IHdpbGwgZmFsbGJhY2sgdG8gYmVpbmcgYSB0ZXh0IGlucHV0IHBlciBicm93c2VycyBzcGVjLlxyXG4gICAgaWYgKHZub2RlLnRhZyA9PT0gJ2lucHV0JyAmJiAoIWF0dHJzIHx8ICFhdHRycy50eXBlKSkge1xyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG4gICAgaWYgKHZub2RlLnRhZyA9PT0gJ3RleHRhcmVhJykge1xyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIGluY2x1ZGVzKFsndGV4dCcsICdwYXNzd29yZCcsICdzZWFyY2gnLCAnZW1haWwnLCAndGVsJywgJ3VybCcsICdudW1iZXInXSwgYXR0cnMgJiYgYXR0cnMudHlwZSk7XHJcbn07XHJcbi8vIGV4cG9ydCBjb25zdCBpc0NoZWNrYm94T3JSYWRpb0lucHV0ID0gKHZub2RlOiBWTm9kZSk6IGJvb2xlYW4gPT4ge1xyXG4vLyAgIGNvbnN0IGF0dHJzID0gKHZub2RlLmRhdGEgJiYgdm5vZGUuZGF0YS5hdHRycykgfHwgdm5vZGUuZWxtO1xyXG4vLyAgIHJldHVybiBpbmNsdWRlcyhbJ3JhZGlvJywgJ2NoZWNrYm94J10sIGF0dHJzICYmIGF0dHJzLnR5cGUpO1xyXG4vLyB9O1xyXG4vLyBHZXRzIHRoZSBtb2RlbCBvYmplY3Qgb24gdGhlIHZub2RlLlxyXG5mdW5jdGlvbiBmaW5kTW9kZWwodm5vZGUpIHtcclxuICAgIGlmICghdm5vZGUuZGF0YSkge1xyXG4gICAgICAgIHJldHVybiB1bmRlZmluZWQ7XHJcbiAgICB9XHJcbiAgICAvLyBDb21wb25lbnQgTW9kZWxcclxuICAgIC8vIFRISVMgSVMgTk9UIFRZUEVEIElOIE9GRklDSUFMIFZVRSBUWVBJTkdTXHJcbiAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmVcclxuICAgIHZhciBub25TdGFuZGFyZFZOb2RlRGF0YSA9IHZub2RlLmRhdGE7XHJcbiAgICBpZiAoJ21vZGVsJyBpbiBub25TdGFuZGFyZFZOb2RlRGF0YSkge1xyXG4gICAgICAgIHJldHVybiBub25TdGFuZGFyZFZOb2RlRGF0YS5tb2RlbDtcclxuICAgIH1cclxuICAgIGlmICghdm5vZGUuZGF0YS5kaXJlY3RpdmVzKSB7XHJcbiAgICAgICAgcmV0dXJuIHVuZGVmaW5lZDtcclxuICAgIH1cclxuICAgIHJldHVybiBmaW5kKHZub2RlLmRhdGEuZGlyZWN0aXZlcywgZnVuY3Rpb24gKGQpIHsgcmV0dXJuIGQubmFtZSA9PT0gJ21vZGVsJzsgfSk7XHJcbn1cclxuZnVuY3Rpb24gZmluZFZhbHVlKHZub2RlKSB7XHJcbiAgICB2YXIgbW9kZWwgPSBmaW5kTW9kZWwodm5vZGUpO1xyXG4gICAgaWYgKG1vZGVsKSB7XHJcbiAgICAgICAgcmV0dXJuIHsgdmFsdWU6IG1vZGVsLnZhbHVlIH07XHJcbiAgICB9XHJcbiAgICB2YXIgY29uZmlnID0gZmluZE1vZGVsQ29uZmlnKHZub2RlKTtcclxuICAgIHZhciBwcm9wID0gKGNvbmZpZyAmJiBjb25maWcucHJvcCkgfHwgJ3ZhbHVlJztcclxuICAgIGlmICh2bm9kZS5jb21wb25lbnRPcHRpb25zICYmIHZub2RlLmNvbXBvbmVudE9wdGlvbnMucHJvcHNEYXRhICYmIHByb3AgaW4gdm5vZGUuY29tcG9uZW50T3B0aW9ucy5wcm9wc0RhdGEpIHtcclxuICAgICAgICB2YXIgcHJvcHNEYXRhV2l0aFZhbHVlID0gdm5vZGUuY29tcG9uZW50T3B0aW9ucy5wcm9wc0RhdGE7XHJcbiAgICAgICAgcmV0dXJuIHsgdmFsdWU6IHByb3BzRGF0YVdpdGhWYWx1ZVtwcm9wXSB9O1xyXG4gICAgfVxyXG4gICAgaWYgKHZub2RlLmRhdGEgJiYgdm5vZGUuZGF0YS5kb21Qcm9wcyAmJiAndmFsdWUnIGluIHZub2RlLmRhdGEuZG9tUHJvcHMpIHtcclxuICAgICAgICByZXR1cm4geyB2YWx1ZTogdm5vZGUuZGF0YS5kb21Qcm9wcy52YWx1ZSB9O1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHVuZGVmaW5lZDtcclxufVxyXG5mdW5jdGlvbiBleHRyYWN0Q2hpbGRyZW4odm5vZGUpIHtcclxuICAgIGlmIChBcnJheS5pc0FycmF5KHZub2RlKSkge1xyXG4gICAgICAgIHJldHVybiB2bm9kZTtcclxuICAgIH1cclxuICAgIGlmIChBcnJheS5pc0FycmF5KHZub2RlLmNoaWxkcmVuKSkge1xyXG4gICAgICAgIHJldHVybiB2bm9kZS5jaGlsZHJlbjtcclxuICAgIH1cclxuICAgIC8qIGlzdGFuYnVsIGlnbm9yZSBuZXh0ICovXHJcbiAgICBpZiAodm5vZGUuY29tcG9uZW50T3B0aW9ucyAmJiBBcnJheS5pc0FycmF5KHZub2RlLmNvbXBvbmVudE9wdGlvbnMuY2hpbGRyZW4pKSB7XHJcbiAgICAgICAgcmV0dXJuIHZub2RlLmNvbXBvbmVudE9wdGlvbnMuY2hpbGRyZW47XHJcbiAgICB9XHJcbiAgICByZXR1cm4gW107XHJcbn1cclxuZnVuY3Rpb24gZXh0cmFjdFZOb2Rlcyh2bm9kZSkge1xyXG4gICAgaWYgKCFBcnJheS5pc0FycmF5KHZub2RlKSAmJiBmaW5kVmFsdWUodm5vZGUpICE9PSB1bmRlZmluZWQpIHtcclxuICAgICAgICByZXR1cm4gW3Zub2RlXTtcclxuICAgIH1cclxuICAgIHZhciBjaGlsZHJlbiA9IGV4dHJhY3RDaGlsZHJlbih2bm9kZSk7XHJcbiAgICByZXR1cm4gY2hpbGRyZW4ucmVkdWNlKGZ1bmN0aW9uIChub2Rlcywgbm9kZSkge1xyXG4gICAgICAgIHZhciBjYW5kaWRhdGVzID0gZXh0cmFjdFZOb2Rlcyhub2RlKTtcclxuICAgICAgICBpZiAoY2FuZGlkYXRlcy5sZW5ndGgpIHtcclxuICAgICAgICAgICAgbm9kZXMucHVzaC5hcHBseShub2RlcywgY2FuZGlkYXRlcyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBub2RlcztcclxuICAgIH0sIFtdKTtcclxufVxyXG4vLyBSZXNvbHZlcyB2LW1vZGVsIGNvbmZpZyBpZiBleGlzdHMuXHJcbmZ1bmN0aW9uIGZpbmRNb2RlbENvbmZpZyh2bm9kZSkge1xyXG4gICAgLyogaXN0YW5idWwgaWdub3JlIG5leHQgKi9cclxuICAgIGlmICghdm5vZGUuY29tcG9uZW50T3B0aW9ucylcclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgIC8vIFRoaXMgaXMgYWxzbyBub3QgdHlwZWQgaW4gdGhlIHN0YW5kYXJkIFZ1ZSBUUy5cclxuICAgIHJldHVybiB2bm9kZS5jb21wb25lbnRPcHRpb25zLkN0b3Iub3B0aW9ucy5tb2RlbDtcclxufVxyXG4vLyBBZGRzIGEgbGlzdGVuZXIgdG8gdm5vZGUgbGlzdGVuZXIgb2JqZWN0LlxyXG5mdW5jdGlvbiBtZXJnZVZOb2RlTGlzdGVuZXJzKG9iaiwgZXZlbnROYW1lLCBoYW5kbGVyKSB7XHJcbiAgICAvLyBubyBsaXN0ZW5lciBhdCBhbGwuXHJcbiAgICBpZiAoaXNOdWxsT3JVbmRlZmluZWQob2JqW2V2ZW50TmFtZV0pKSB7XHJcbiAgICAgICAgb2JqW2V2ZW50TmFtZV0gPSBbaGFuZGxlcl07XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgLy8gSXMgYW4gaW52b2tlci5cclxuICAgIGlmIChpc0NhbGxhYmxlKG9ialtldmVudE5hbWVdKSAmJiBvYmpbZXZlbnROYW1lXS5mbnMpIHtcclxuICAgICAgICB2YXIgaW52b2tlciA9IG9ialtldmVudE5hbWVdO1xyXG4gICAgICAgIGludm9rZXIuZm5zID0gQXJyYXkuaXNBcnJheShpbnZva2VyLmZucykgPyBpbnZva2VyLmZucyA6IFtpbnZva2VyLmZuc107XHJcbiAgICAgICAgaWYgKCFpbmNsdWRlcyhpbnZva2VyLmZucywgaGFuZGxlcikpIHtcclxuICAgICAgICAgICAgaW52b2tlci5mbnMucHVzaChoYW5kbGVyKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgaWYgKGlzQ2FsbGFibGUob2JqW2V2ZW50TmFtZV0pKSB7XHJcbiAgICAgICAgdmFyIHByZXYgPSBvYmpbZXZlbnROYW1lXTtcclxuICAgICAgICBvYmpbZXZlbnROYW1lXSA9IFtwcmV2XTtcclxuICAgIH1cclxuICAgIGlmIChBcnJheS5pc0FycmF5KG9ialtldmVudE5hbWVdKSAmJiAhaW5jbHVkZXMob2JqW2V2ZW50TmFtZV0sIGhhbmRsZXIpKSB7XHJcbiAgICAgICAgb2JqW2V2ZW50TmFtZV0ucHVzaChoYW5kbGVyKTtcclxuICAgIH1cclxufVxyXG4vLyBBZGRzIGEgbGlzdGVuZXIgdG8gYSBuYXRpdmUgSFRNTCB2bm9kZS5cclxuZnVuY3Rpb24gYWRkTmF0aXZlTm9kZUxpc3RlbmVyKG5vZGUsIGV2ZW50TmFtZSwgaGFuZGxlcikge1xyXG4gICAgLyogaXN0YW5idWwgaWdub3JlIG5leHQgKi9cclxuICAgIGlmICghbm9kZS5kYXRhKSB7XHJcbiAgICAgICAgbm9kZS5kYXRhID0ge307XHJcbiAgICB9XHJcbiAgICBpZiAoaXNOdWxsT3JVbmRlZmluZWQobm9kZS5kYXRhLm9uKSkge1xyXG4gICAgICAgIG5vZGUuZGF0YS5vbiA9IHt9O1xyXG4gICAgfVxyXG4gICAgbWVyZ2VWTm9kZUxpc3RlbmVycyhub2RlLmRhdGEub24sIGV2ZW50TmFtZSwgaGFuZGxlcik7XHJcbn1cclxuLy8gQWRkcyBhIGxpc3RlbmVyIHRvIGEgVnVlIGNvbXBvbmVudCB2bm9kZS5cclxuZnVuY3Rpb24gYWRkQ29tcG9uZW50Tm9kZUxpc3RlbmVyKG5vZGUsIGV2ZW50TmFtZSwgaGFuZGxlcikge1xyXG4gICAgLyogaXN0YW5idWwgaWdub3JlIG5leHQgKi9cclxuICAgIGlmICghbm9kZS5jb21wb25lbnRPcHRpb25zKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgLyogaXN0YW5idWwgaWdub3JlIG5leHQgKi9cclxuICAgIGlmICghbm9kZS5jb21wb25lbnRPcHRpb25zLmxpc3RlbmVycykge1xyXG4gICAgICAgIG5vZGUuY29tcG9uZW50T3B0aW9ucy5saXN0ZW5lcnMgPSB7fTtcclxuICAgIH1cclxuICAgIG1lcmdlVk5vZGVMaXN0ZW5lcnMobm9kZS5jb21wb25lbnRPcHRpb25zLmxpc3RlbmVycywgZXZlbnROYW1lLCBoYW5kbGVyKTtcclxufVxyXG5mdW5jdGlvbiBhZGRWTm9kZUxpc3RlbmVyKHZub2RlLCBldmVudE5hbWUsIGhhbmRsZXIpIHtcclxuICAgIGlmICh2bm9kZS5jb21wb25lbnRPcHRpb25zKSB7XHJcbiAgICAgICAgYWRkQ29tcG9uZW50Tm9kZUxpc3RlbmVyKHZub2RlLCBldmVudE5hbWUsIGhhbmRsZXIpO1xyXG4gICAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIGFkZE5hdGl2ZU5vZGVMaXN0ZW5lcih2bm9kZSwgZXZlbnROYW1lLCBoYW5kbGVyKTtcclxufVxyXG4vLyBEZXRlcm1pbmVzIGlmIGBjaGFuZ2VgIHNob3VsZCBiZSB1c2VkIG92ZXIgYGlucHV0YCBmb3IgbGlzdGVuZXJzLlxyXG5mdW5jdGlvbiBnZXRJbnB1dEV2ZW50TmFtZSh2bm9kZSwgbW9kZWwpIHtcclxuICAgIC8vIElzIGEgY29tcG9uZW50LlxyXG4gICAgaWYgKHZub2RlLmNvbXBvbmVudE9wdGlvbnMpIHtcclxuICAgICAgICB2YXIgZXZlbnRfMSA9IChmaW5kTW9kZWxDb25maWcodm5vZGUpIHx8IHsgZXZlbnQ6ICdpbnB1dCcgfSkuZXZlbnQ7XHJcbiAgICAgICAgcmV0dXJuIGV2ZW50XzE7XHJcbiAgICB9XHJcbiAgICAvLyBMYXp5IE1vZGVscyB0eXBpY2FsbHkgdXNlIGNoYW5nZSBldmVudFxyXG4gICAgaWYgKG1vZGVsICYmIG1vZGVsLm1vZGlmaWVycyAmJiBtb2RlbC5tb2RpZmllcnMubGF6eSkge1xyXG4gICAgICAgIHJldHVybiAnY2hhbmdlJztcclxuICAgIH1cclxuICAgIC8vIGlzIGEgdGV4dHVhbC10eXBlIGlucHV0LlxyXG4gICAgaWYgKGlzVGV4dElucHV0KHZub2RlKSkge1xyXG4gICAgICAgIHJldHVybiAnaW5wdXQnO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuICdjaGFuZ2UnO1xyXG59XHJcbi8vIFRPRE86IFR5cGUgdGhpcyBvbmUgcHJvcGVybHkuXHJcbmZ1bmN0aW9uIG5vcm1hbGl6ZVNsb3RzKHNsb3RzLCBjdHgpIHtcclxuICAgIHZhciBhY2MgPSBbXTtcclxuICAgIHJldHVybiBPYmplY3Qua2V5cyhzbG90cykucmVkdWNlKGZ1bmN0aW9uIChhcnIsIGtleSkge1xyXG4gICAgICAgIHNsb3RzW2tleV0uZm9yRWFjaChmdW5jdGlvbiAodm5vZGUpIHtcclxuICAgICAgICAgICAgaWYgKCF2bm9kZS5jb250ZXh0KSB7XHJcbiAgICAgICAgICAgICAgICBzbG90c1trZXldLmNvbnRleHQgPSBjdHg7XHJcbiAgICAgICAgICAgICAgICBpZiAoIXZub2RlLmRhdGEpIHtcclxuICAgICAgICAgICAgICAgICAgICB2bm9kZS5kYXRhID0ge307XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB2bm9kZS5kYXRhLnNsb3QgPSBrZXk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICByZXR1cm4gYXJyLmNvbmNhdChzbG90c1trZXldKTtcclxuICAgIH0sIGFjYyk7XHJcbn1cclxuZnVuY3Rpb24gcmVzb2x2ZVRleHR1YWxSdWxlcyh2bm9kZSkge1xyXG4gICAgdmFyIGF0dHJzID0gdm5vZGUuZGF0YSAmJiB2bm9kZS5kYXRhLmF0dHJzO1xyXG4gICAgdmFyIHJ1bGVzID0ge307XHJcbiAgICBpZiAoIWF0dHJzKVxyXG4gICAgICAgIHJldHVybiBydWxlcztcclxuICAgIGlmIChhdHRycy50eXBlID09PSAnZW1haWwnKSB7XHJcbiAgICAgICAgcnVsZXMuZW1haWwgPSBbJ211bHRpcGxlJyBpbiBhdHRyc107XHJcbiAgICB9XHJcbiAgICBpZiAoYXR0cnMucGF0dGVybikge1xyXG4gICAgICAgIHJ1bGVzLnJlZ2V4ID0gYXR0cnMucGF0dGVybjtcclxuICAgIH1cclxuICAgIGlmIChhdHRycy5tYXhsZW5ndGggPj0gMCkge1xyXG4gICAgICAgIHJ1bGVzLm1heCA9IGF0dHJzLm1heGxlbmd0aDtcclxuICAgIH1cclxuICAgIGlmIChhdHRycy5taW5sZW5ndGggPj0gMCkge1xyXG4gICAgICAgIHJ1bGVzLm1pbiA9IGF0dHJzLm1pbmxlbmd0aDtcclxuICAgIH1cclxuICAgIGlmIChhdHRycy50eXBlID09PSAnbnVtYmVyJykge1xyXG4gICAgICAgIGlmIChpc1NwZWNpZmllZChhdHRycy5taW4pKSB7XHJcbiAgICAgICAgICAgIHJ1bGVzLm1pbl92YWx1ZSA9IE51bWJlcihhdHRycy5taW4pO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoaXNTcGVjaWZpZWQoYXR0cnMubWF4KSkge1xyXG4gICAgICAgICAgICBydWxlcy5tYXhfdmFsdWUgPSBOdW1iZXIoYXR0cnMubWF4KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICByZXR1cm4gcnVsZXM7XHJcbn1cclxuZnVuY3Rpb24gcmVzb2x2ZVJ1bGVzKHZub2RlKSB7XHJcbiAgICB2YXIgaHRtbFRhZ3MgPSBbJ2lucHV0JywgJ3NlbGVjdCddO1xyXG4gICAgdmFyIGF0dHJzID0gdm5vZGUuZGF0YSAmJiB2bm9kZS5kYXRhLmF0dHJzO1xyXG4gICAgaWYgKCFpbmNsdWRlcyhodG1sVGFncywgdm5vZGUudGFnKSB8fCAhYXR0cnMpIHtcclxuICAgICAgICByZXR1cm4ge307XHJcbiAgICB9XHJcbiAgICB2YXIgcnVsZXMgPSB7fTtcclxuICAgIGlmICgncmVxdWlyZWQnIGluIGF0dHJzICYmIGF0dHJzLnJlcXVpcmVkICE9PSBmYWxzZSkge1xyXG4gICAgICAgIHJ1bGVzLnJlcXVpcmVkID0gYXR0cnMudHlwZSA9PT0gJ2NoZWNrYm94JyA/IFt0cnVlXSA6IHRydWU7XHJcbiAgICB9XHJcbiAgICBpZiAoaXNUZXh0SW5wdXQodm5vZGUpKSB7XHJcbiAgICAgICAgcmV0dXJuIG5vcm1hbGl6ZVJ1bGVzKF9fYXNzaWduKF9fYXNzaWduKHt9LCBydWxlcyksIHJlc29sdmVUZXh0dWFsUnVsZXModm5vZGUpKSk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gbm9ybWFsaXplUnVsZXMocnVsZXMpO1xyXG59XHJcbmZ1bmN0aW9uIG5vcm1hbGl6ZUNoaWxkcmVuKGNvbnRleHQsIHNsb3RQcm9wcykge1xyXG4gICAgaWYgKGNvbnRleHQuJHNjb3BlZFNsb3RzLmRlZmF1bHQpIHtcclxuICAgICAgICByZXR1cm4gY29udGV4dC4kc2NvcGVkU2xvdHMuZGVmYXVsdChzbG90UHJvcHMpIHx8IFtdO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIGNvbnRleHQuJHNsb3RzLmRlZmF1bHQgfHwgW107XHJcbn1cblxuLyoqXHJcbiAqIERldGVybWluZXMgaWYgYSBwcm92aWRlciBuZWVkcyB0byBydW4gdmFsaWRhdGlvbi5cclxuICovXHJcbmZ1bmN0aW9uIHNob3VsZFZhbGlkYXRlKGN0eCwgdmFsdWUpIHtcclxuICAgIC8vIHdoZW4gYW4gaW1tZWRpYXRlL2luaXRpYWwgdmFsaWRhdGlvbiBpcyBuZWVkZWQgYW5kIHdhc24ndCBkb25lIGJlZm9yZS5cclxuICAgIGlmICghY3R4Ll9pZ25vcmVJbW1lZGlhdGUgJiYgY3R4LmltbWVkaWF0ZSkge1xyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG4gICAgLy8gd2hlbiB0aGUgdmFsdWUgY2hhbmdlcyBmb3Igd2hhdGV2ZXIgcmVhc29uLlxyXG4gICAgaWYgKGN0eC52YWx1ZSAhPT0gdmFsdWUgJiYgY3R4Lm5vcm1hbGl6ZWRFdmVudHMubGVuZ3RoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcbiAgICAvLyB3aGVuIGl0IG5lZWRzIHZhbGlkYXRpb24gZHVlIHRvIHByb3BzL2Nyb3NzLWZpZWxkcyBjaGFuZ2VzLlxyXG4gICAgaWYgKGN0eC5fbmVlZHNWYWxpZGF0aW9uKSB7XHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcbiAgICAvLyB3aGVuIHRoZSBpbml0aWFsIHZhbHVlIGlzIHVuZGVmaW5lZCBhbmQgdGhlIGZpZWxkIHdhc24ndCByZW5kZXJlZCB5ZXQuXHJcbiAgICBpZiAoIWN0eC5pbml0aWFsaXplZCAmJiB2YWx1ZSA9PT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gZmFsc2U7XHJcbn1cclxuZnVuY3Rpb24gY3JlYXRlVmFsaWRhdGlvbkN0eChjdHgpIHtcclxuICAgIHJldHVybiBfX2Fzc2lnbihfX2Fzc2lnbih7fSwgY3R4LmZsYWdzKSwgeyBlcnJvcnM6IGN0eC5tZXNzYWdlcywgY2xhc3NlczogY3R4LmNsYXNzZXMsIGZhaWxlZFJ1bGVzOiBjdHguZmFpbGVkUnVsZXMsIHJlc2V0OiBmdW5jdGlvbiAoKSB7IHJldHVybiBjdHgucmVzZXQoKTsgfSwgdmFsaWRhdGU6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgdmFyIGFyZ3MgPSBbXTtcclxuICAgICAgICAgICAgZm9yICh2YXIgX2kgPSAwOyBfaSA8IGFyZ3VtZW50cy5sZW5ndGg7IF9pKyspIHtcclxuICAgICAgICAgICAgICAgIGFyZ3NbX2ldID0gYXJndW1lbnRzW19pXTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gY3R4LnZhbGlkYXRlLmFwcGx5KGN0eCwgYXJncyk7XHJcbiAgICAgICAgfSwgYXJpYUlucHV0OiB7XHJcbiAgICAgICAgICAgICdhcmlhLWludmFsaWQnOiBjdHguZmxhZ3MuaW52YWxpZCA/ICd0cnVlJyA6ICdmYWxzZScsXHJcbiAgICAgICAgICAgICdhcmlhLXJlcXVpcmVkJzogY3R4LmlzUmVxdWlyZWQgPyAndHJ1ZScgOiAnZmFsc2UnLFxyXG4gICAgICAgICAgICAnYXJpYS1lcnJvcm1lc3NhZ2UnOiBcInZlZV9cIiArIGN0eC5pZFxyXG4gICAgICAgIH0sIGFyaWFNc2c6IHtcclxuICAgICAgICAgICAgaWQ6IFwidmVlX1wiICsgY3R4LmlkLFxyXG4gICAgICAgICAgICAnYXJpYS1saXZlJzogY3R4Lm1lc3NhZ2VzLmxlbmd0aCA/ICdhc3NlcnRpdmUnIDogJ29mZidcclxuICAgICAgICB9IH0pO1xyXG59XHJcbmZ1bmN0aW9uIG9uUmVuZGVyVXBkYXRlKHZtLCB2YWx1ZSkge1xyXG4gICAgaWYgKCF2bS5pbml0aWFsaXplZCkge1xyXG4gICAgICAgIHZtLmluaXRpYWxWYWx1ZSA9IHZhbHVlO1xyXG4gICAgfVxyXG4gICAgdmFyIHZhbGlkYXRlTm93ID0gc2hvdWxkVmFsaWRhdGUodm0sIHZhbHVlKTtcclxuICAgIHZtLl9uZWVkc1ZhbGlkYXRpb24gPSBmYWxzZTtcclxuICAgIHZtLnZhbHVlID0gdmFsdWU7XHJcbiAgICB2bS5faWdub3JlSW1tZWRpYXRlID0gdHJ1ZTtcclxuICAgIGlmICghdmFsaWRhdGVOb3cpIHtcclxuICAgICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICB2bS52YWxpZGF0ZVNpbGVudCgpLnRoZW4odm0uaW1tZWRpYXRlIHx8IHZtLmZsYWdzLnZhbGlkYXRlZCA/IHZtLmFwcGx5UmVzdWx0IDogaWRlbnRpdHkpO1xyXG59XHJcbmZ1bmN0aW9uIGNvbXB1dGVNb2RlU2V0dGluZyhjdHgpIHtcclxuICAgIHZhciBjb21wdXRlID0gKGlzQ2FsbGFibGUoY3R4Lm1vZGUpID8gY3R4Lm1vZGUgOiBtb2Rlc1tjdHgubW9kZV0pO1xyXG4gICAgcmV0dXJuIGNvbXB1dGUoe1xyXG4gICAgICAgIGVycm9yczogY3R4Lm1lc3NhZ2VzLFxyXG4gICAgICAgIHZhbHVlOiBjdHgudmFsdWUsXHJcbiAgICAgICAgZmxhZ3M6IGN0eC5mbGFnc1xyXG4gICAgfSk7XHJcbn1cclxuLy8gQ3JlYXRlcyB0aGUgY29tbW9uIGhhbmRsZXJzIGZvciBhIHZhbGlkYXRhYmxlIGNvbnRleHQuXHJcbmZ1bmN0aW9uIGNyZWF0ZUNvbW1vbkhhbmRsZXJzKHZtKSB7XHJcbiAgICBpZiAoIXZtLiR2ZWVPbklucHV0KSB7XHJcbiAgICAgICAgdm0uJHZlZU9uSW5wdXQgPSBmdW5jdGlvbiAoZSkge1xyXG4gICAgICAgICAgICB2bS5zeW5jVmFsdWUoZSk7IC8vIHRyYWNrIGFuZCBrZWVwIHRoZSB2YWx1ZSB1cGRhdGVkLlxyXG4gICAgICAgICAgICB2bS5zZXRGbGFncyh7IGRpcnR5OiB0cnVlLCBwcmlzdGluZTogZmFsc2UgfSk7XHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuICAgIHZhciBvbklucHV0ID0gdm0uJHZlZU9uSW5wdXQ7XHJcbiAgICBpZiAoIXZtLiR2ZWVPbkJsdXIpIHtcclxuICAgICAgICB2bS4kdmVlT25CbHVyID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICB2bS5zZXRGbGFncyh7IHRvdWNoZWQ6IHRydWUsIHVudG91Y2hlZDogZmFsc2UgfSk7XHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuICAgIC8vIEJsdXIgZXZlbnQgbGlzdGVuZXIuXHJcbiAgICB2YXIgb25CbHVyID0gdm0uJHZlZU9uQmx1cjtcclxuICAgIHZhciBvblZhbGlkYXRlID0gdm0uJHZlZUhhbmRsZXI7XHJcbiAgICB2YXIgbW9kZSA9IGNvbXB1dGVNb2RlU2V0dGluZyh2bSk7XHJcbiAgICAvLyBIYW5kbGUgZGVib3VuY2UgY2hhbmdlcy5cclxuICAgIGlmICghb25WYWxpZGF0ZSB8fCB2bS4kdmVlRGVib3VuY2UgIT09IHZtLmRlYm91bmNlKSB7XHJcbiAgICAgICAgb25WYWxpZGF0ZSA9IGRlYm91bmNlKGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgdm0uJG5leHRUaWNrKGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgIHZhciBwZW5kaW5nUHJvbWlzZSA9IHZtLnZhbGlkYXRlU2lsZW50KCk7XHJcbiAgICAgICAgICAgICAgICAvLyBhdm9pZHMgcmFjZSBjb25kaXRpb25zIGJldHdlZW4gc3VjY2Vzc2l2ZSB2YWxpZGF0aW9ucy5cclxuICAgICAgICAgICAgICAgIHZtLl9wZW5kaW5nVmFsaWRhdGlvbiA9IHBlbmRpbmdQcm9taXNlO1xyXG4gICAgICAgICAgICAgICAgcGVuZGluZ1Byb21pc2UudGhlbihmdW5jdGlvbiAocmVzdWx0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHBlbmRpbmdQcm9taXNlID09PSB2bS5fcGVuZGluZ1ZhbGlkYXRpb24pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdm0uYXBwbHlSZXN1bHQocmVzdWx0KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdm0uX3BlbmRpbmdWYWxpZGF0aW9uID0gdW5kZWZpbmVkO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LCBtb2RlLmRlYm91bmNlIHx8IHZtLmRlYm91bmNlKTtcclxuICAgICAgICAvLyBDYWNoZSB0aGUgaGFuZGxlciBzbyB3ZSBkb24ndCBjcmVhdGUgaXQgZWFjaCB0aW1lLlxyXG4gICAgICAgIHZtLiR2ZWVIYW5kbGVyID0gb25WYWxpZGF0ZTtcclxuICAgICAgICAvLyBjYWNoZSB0aGUgZGVib3VuY2UgdmFsdWUgc28gd2UgZGV0ZWN0IGlmIGl0IHdhcyBjaGFuZ2VkLlxyXG4gICAgICAgIHZtLiR2ZWVEZWJvdW5jZSA9IHZtLmRlYm91bmNlO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHsgb25JbnB1dDogb25JbnB1dCwgb25CbHVyOiBvbkJsdXIsIG9uVmFsaWRhdGU6IG9uVmFsaWRhdGUgfTtcclxufVxyXG4vLyBBZGRzIGFsbCBwbHVnaW4gbGlzdGVuZXJzIHRvIHRoZSB2bm9kZS5cclxuZnVuY3Rpb24gYWRkTGlzdGVuZXJzKHZtLCBub2RlKSB7XHJcbiAgICB2YXIgdmFsdWUgPSBmaW5kVmFsdWUobm9kZSk7XHJcbiAgICAvLyBjYWNoZSB0aGUgaW5wdXQgZXZlbnROYW1lLlxyXG4gICAgdm0uX2lucHV0RXZlbnROYW1lID0gdm0uX2lucHV0RXZlbnROYW1lIHx8IGdldElucHV0RXZlbnROYW1lKG5vZGUsIGZpbmRNb2RlbChub2RlKSk7XHJcbiAgICBvblJlbmRlclVwZGF0ZSh2bSwgdmFsdWUgJiYgdmFsdWUudmFsdWUpO1xyXG4gICAgdmFyIF9hID0gY3JlYXRlQ29tbW9uSGFuZGxlcnModm0pLCBvbklucHV0ID0gX2Eub25JbnB1dCwgb25CbHVyID0gX2Eub25CbHVyLCBvblZhbGlkYXRlID0gX2Eub25WYWxpZGF0ZTtcclxuICAgIGFkZFZOb2RlTGlzdGVuZXIobm9kZSwgdm0uX2lucHV0RXZlbnROYW1lLCBvbklucHV0KTtcclxuICAgIGFkZFZOb2RlTGlzdGVuZXIobm9kZSwgJ2JsdXInLCBvbkJsdXIpO1xyXG4gICAgLy8gYWRkIHRoZSB2YWxpZGF0aW9uIGxpc3RlbmVycy5cclxuICAgIHZtLm5vcm1hbGl6ZWRFdmVudHMuZm9yRWFjaChmdW5jdGlvbiAoZXZ0KSB7XHJcbiAgICAgICAgYWRkVk5vZGVMaXN0ZW5lcihub2RlLCBldnQsIG9uVmFsaWRhdGUpO1xyXG4gICAgfSk7XHJcbiAgICB2bS5pbml0aWFsaXplZCA9IHRydWU7XHJcbn1cblxudmFyIFBST1ZJREVSX0NPVU5URVIgPSAwO1xyXG5mdW5jdGlvbiBkYXRhKCkge1xyXG4gICAgdmFyIG1lc3NhZ2VzID0gW107XHJcbiAgICB2YXIgZGVmYXVsdFZhbHVlcyA9IHtcclxuICAgICAgICBtZXNzYWdlczogbWVzc2FnZXMsXHJcbiAgICAgICAgdmFsdWU6IHVuZGVmaW5lZCxcclxuICAgICAgICBpbml0aWFsaXplZDogZmFsc2UsXHJcbiAgICAgICAgaW5pdGlhbFZhbHVlOiB1bmRlZmluZWQsXHJcbiAgICAgICAgZmxhZ3M6IGNyZWF0ZUZsYWdzKCksXHJcbiAgICAgICAgZmFpbGVkUnVsZXM6IHt9LFxyXG4gICAgICAgIGlzRGVhY3RpdmF0ZWQ6IGZhbHNlLFxyXG4gICAgICAgIGlkOiAnJ1xyXG4gICAgfTtcclxuICAgIHJldHVybiBkZWZhdWx0VmFsdWVzO1xyXG59XHJcbnZhciBWYWxpZGF0aW9uUHJvdmlkZXIgPSBWdWUuZXh0ZW5kKHtcclxuICAgIGluamVjdDoge1xyXG4gICAgICAgICRfdmVlT2JzZXJ2ZXI6IHtcclxuICAgICAgICAgICAgZnJvbTogJyRfdmVlT2JzZXJ2ZXInLFxyXG4gICAgICAgICAgICBkZWZhdWx0OiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIXRoaXMuJHZub2RlLmNvbnRleHQuJF92ZWVPYnNlcnZlcikge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuJHZub2RlLmNvbnRleHQuJF92ZWVPYnNlcnZlciA9IGNyZWF0ZU9ic2VydmVyKCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy4kdm5vZGUuY29udGV4dC4kX3ZlZU9ic2VydmVyO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIHByb3BzOiB7XHJcbiAgICAgICAgdmlkOiB7XHJcbiAgICAgICAgICAgIHR5cGU6IFN0cmluZyxcclxuICAgICAgICAgICAgZGVmYXVsdDogJydcclxuICAgICAgICB9LFxyXG4gICAgICAgIG5hbWU6IHtcclxuICAgICAgICAgICAgdHlwZTogU3RyaW5nLFxyXG4gICAgICAgICAgICBkZWZhdWx0OiBudWxsXHJcbiAgICAgICAgfSxcclxuICAgICAgICBtb2RlOiB7XHJcbiAgICAgICAgICAgIHR5cGU6IFtTdHJpbmcsIEZ1bmN0aW9uXSxcclxuICAgICAgICAgICAgZGVmYXVsdDogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGdldENvbmZpZygpLm1vZGU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG4gICAgICAgIHJ1bGVzOiB7XHJcbiAgICAgICAgICAgIHR5cGU6IFtPYmplY3QsIFN0cmluZ10sXHJcbiAgICAgICAgICAgIGRlZmF1bHQ6IG51bGxcclxuICAgICAgICB9LFxyXG4gICAgICAgIGltbWVkaWF0ZToge1xyXG4gICAgICAgICAgICB0eXBlOiBCb29sZWFuLFxyXG4gICAgICAgICAgICBkZWZhdWx0OiBmYWxzZVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgcGVyc2lzdDoge1xyXG4gICAgICAgICAgICB0eXBlOiBCb29sZWFuLFxyXG4gICAgICAgICAgICBkZWZhdWx0OiBmYWxzZVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgYmFpbHM6IHtcclxuICAgICAgICAgICAgdHlwZTogQm9vbGVhbixcclxuICAgICAgICAgICAgZGVmYXVsdDogZnVuY3Rpb24gKCkgeyByZXR1cm4gZ2V0Q29uZmlnKCkuYmFpbHM7IH1cclxuICAgICAgICB9LFxyXG4gICAgICAgIHNraXBJZkVtcHR5OiB7XHJcbiAgICAgICAgICAgIHR5cGU6IEJvb2xlYW4sXHJcbiAgICAgICAgICAgIGRlZmF1bHQ6IGZ1bmN0aW9uICgpIHsgcmV0dXJuIGdldENvbmZpZygpLnNraXBPcHRpb25hbDsgfVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgZGVib3VuY2U6IHtcclxuICAgICAgICAgICAgdHlwZTogTnVtYmVyLFxyXG4gICAgICAgICAgICBkZWZhdWx0OiAwXHJcbiAgICAgICAgfSxcclxuICAgICAgICB0YWc6IHtcclxuICAgICAgICAgICAgdHlwZTogU3RyaW5nLFxyXG4gICAgICAgICAgICBkZWZhdWx0OiAnc3BhbidcclxuICAgICAgICB9LFxyXG4gICAgICAgIHNsaW06IHtcclxuICAgICAgICAgICAgdHlwZTogQm9vbGVhbixcclxuICAgICAgICAgICAgZGVmYXVsdDogZmFsc2VcclxuICAgICAgICB9LFxyXG4gICAgICAgIGRpc2FibGVkOiB7XHJcbiAgICAgICAgICAgIHR5cGU6IEJvb2xlYW4sXHJcbiAgICAgICAgICAgIGRlZmF1bHQ6IGZhbHNlXHJcbiAgICAgICAgfSxcclxuICAgICAgICBjdXN0b21NZXNzYWdlczoge1xyXG4gICAgICAgICAgICB0eXBlOiBPYmplY3QsXHJcbiAgICAgICAgICAgIGRlZmF1bHQ6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB7fTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICB3YXRjaDoge1xyXG4gICAgICAgIHJ1bGVzOiB7XHJcbiAgICAgICAgICAgIGRlZXA6IHRydWUsXHJcbiAgICAgICAgICAgIGhhbmRsZXI6IGZ1bmN0aW9uICh2YWwsIG9sZFZhbCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fbmVlZHNWYWxpZGF0aW9uID0gIWlzRXF1YWwodmFsLCBvbGRWYWwpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIGRhdGE6IGRhdGEsXHJcbiAgICBjb21wdXRlZDoge1xyXG4gICAgICAgIGZpZWxkRGVwczogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xyXG4gICAgICAgICAgICByZXR1cm4gT2JqZWN0LmtleXModGhpcy5ub3JtYWxpemVkUnVsZXMpXHJcbiAgICAgICAgICAgICAgICAuZmlsdGVyKFJ1bGVDb250YWluZXIuaXNUYXJnZXRSdWxlKVxyXG4gICAgICAgICAgICAgICAgLnJlZHVjZShmdW5jdGlvbiAoYWNjLCBydWxlKSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgZGVwcyA9IFJ1bGVDb250YWluZXIuZ2V0VGFyZ2V0UGFyYW1OYW1lcyhydWxlLCBfdGhpcy5ub3JtYWxpemVkUnVsZXNbcnVsZV0pO1xyXG4gICAgICAgICAgICAgICAgYWNjLnB1c2guYXBwbHkoYWNjLCBkZXBzKTtcclxuICAgICAgICAgICAgICAgIGRlcHMuZm9yRWFjaChmdW5jdGlvbiAoZGVwTmFtZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHdhdGNoQ3Jvc3NGaWVsZERlcChfdGhpcywgZGVwTmFtZSk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIHJldHVybiBhY2M7XHJcbiAgICAgICAgICAgIH0sIFtdKTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIG5vcm1hbGl6ZWRFdmVudHM6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgdmFyIF90aGlzID0gdGhpcztcclxuICAgICAgICAgICAgdmFyIG9uID0gY29tcHV0ZU1vZGVTZXR0aW5nKHRoaXMpLm9uO1xyXG4gICAgICAgICAgICByZXR1cm4gKG9uIHx8IFtdKS5tYXAoZnVuY3Rpb24gKGUpIHtcclxuICAgICAgICAgICAgICAgIGlmIChlID09PSAnaW5wdXQnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIF90aGlzLl9pbnB1dEV2ZW50TmFtZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHJldHVybiBlO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIGlzUmVxdWlyZWQ6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgdmFyIHJ1bGVzID0gX19hc3NpZ24oX19hc3NpZ24oe30sIHRoaXMuX3Jlc29sdmVkUnVsZXMpLCB0aGlzLm5vcm1hbGl6ZWRSdWxlcyk7XHJcbiAgICAgICAgICAgIHZhciBpc1JlcXVpcmVkID0gT2JqZWN0LmtleXMocnVsZXMpLnNvbWUoUnVsZUNvbnRhaW5lci5pc1JlcXVpcmVSdWxlKTtcclxuICAgICAgICAgICAgdGhpcy5mbGFncy5yZXF1aXJlZCA9ICEhaXNSZXF1aXJlZDtcclxuICAgICAgICAgICAgcmV0dXJuIGlzUmVxdWlyZWQ7XHJcbiAgICAgICAgfSxcclxuICAgICAgICBjbGFzc2VzOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIHZhciBuYW1lcyA9IGdldENvbmZpZygpLmNsYXNzZXM7XHJcbiAgICAgICAgICAgIHJldHVybiBjb21wdXRlQ2xhc3NPYmoobmFtZXMsIHRoaXMuZmxhZ3MpO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgbm9ybWFsaXplZFJ1bGVzOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBub3JtYWxpemVSdWxlcyh0aGlzLnJ1bGVzKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAgcmVuZGVyOiBmdW5jdGlvbiAoaCkge1xyXG4gICAgICAgIHZhciBfdGhpcyA9IHRoaXM7XHJcbiAgICAgICAgdGhpcy5yZWdpc3RlckZpZWxkKCk7XHJcbiAgICAgICAgdmFyIGN0eCA9IGNyZWF0ZVZhbGlkYXRpb25DdHgodGhpcyk7XHJcbiAgICAgICAgdmFyIGNoaWxkcmVuID0gbm9ybWFsaXplQ2hpbGRyZW4odGhpcywgY3R4KTtcclxuICAgICAgICAvLyBIYW5kbGUgc2luZ2xlLXJvb3Qgc2xvdC5cclxuICAgICAgICBleHRyYWN0Vk5vZGVzKGNoaWxkcmVuKS5mb3JFYWNoKGZ1bmN0aW9uIChpbnB1dCkge1xyXG4gICAgICAgICAgICBfdGhpcy5fcmVzb2x2ZWRSdWxlcyA9IGdldENvbmZpZygpLnVzZUNvbnN0cmFpbnRBdHRycyA/IHJlc29sdmVSdWxlcyhpbnB1dCkgOiB7fTtcclxuICAgICAgICAgICAgYWRkTGlzdGVuZXJzKF90aGlzLCBpbnB1dCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc2xpbSAmJiBjaGlsZHJlbi5sZW5ndGggPD0gMSA/IGNoaWxkcmVuWzBdIDogaCh0aGlzLnRhZywgY2hpbGRyZW4pO1xyXG4gICAgfSxcclxuICAgIGJlZm9yZURlc3Ryb3k6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAvLyBjbGVhbnVwIHJlZmVyZW5jZS5cclxuICAgICAgICB0aGlzLiRfdmVlT2JzZXJ2ZXIudW5zdWJzY3JpYmUodGhpcy5pZCk7XHJcbiAgICB9LFxyXG4gICAgYWN0aXZhdGVkOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdGhpcy4kX3ZlZU9ic2VydmVyLnN1YnNjcmliZSh0aGlzKTtcclxuICAgICAgICB0aGlzLmlzRGVhY3RpdmF0ZWQgPSBmYWxzZTtcclxuICAgIH0sXHJcbiAgICBkZWFjdGl2YXRlZDogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHRoaXMuJF92ZWVPYnNlcnZlci51bnN1YnNjcmliZSh0aGlzLmlkKTtcclxuICAgICAgICB0aGlzLmlzRGVhY3RpdmF0ZWQgPSB0cnVlO1xyXG4gICAgfSxcclxuICAgIG1ldGhvZHM6IHtcclxuICAgICAgICBzZXRGbGFnczogZnVuY3Rpb24gKGZsYWdzKSB7XHJcbiAgICAgICAgICAgIHZhciBfdGhpcyA9IHRoaXM7XHJcbiAgICAgICAgICAgIE9iamVjdC5rZXlzKGZsYWdzKS5mb3JFYWNoKGZ1bmN0aW9uIChmbGFnKSB7XHJcbiAgICAgICAgICAgICAgICBfdGhpcy5mbGFnc1tmbGFnXSA9IGZsYWdzW2ZsYWddO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIHN5bmNWYWx1ZTogZnVuY3Rpb24gKHYpIHtcclxuICAgICAgICAgICAgdmFyIHZhbHVlID0gbm9ybWFsaXplRXZlbnRWYWx1ZSh2KTtcclxuICAgICAgICAgICAgdGhpcy52YWx1ZSA9IHZhbHVlO1xyXG4gICAgICAgICAgICB0aGlzLmZsYWdzLmNoYW5nZWQgPSB0aGlzLmluaXRpYWxWYWx1ZSAhPT0gdmFsdWU7XHJcbiAgICAgICAgfSxcclxuICAgICAgICByZXNldDogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICB0aGlzLm1lc3NhZ2VzID0gW107XHJcbiAgICAgICAgICAgIHRoaXMuaW5pdGlhbFZhbHVlID0gdGhpcy52YWx1ZTtcclxuICAgICAgICAgICAgdmFyIGZsYWdzID0gY3JlYXRlRmxhZ3MoKTtcclxuICAgICAgICAgICAgZmxhZ3MucmVxdWlyZWQgPSB0aGlzLmlzUmVxdWlyZWQ7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0RmxhZ3MoZmxhZ3MpO1xyXG4gICAgICAgICAgICB0aGlzLnZhbGlkYXRlU2lsZW50KCk7XHJcbiAgICAgICAgfSxcclxuICAgICAgICB2YWxpZGF0ZTogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICB2YXIgYXJncyA9IFtdO1xyXG4gICAgICAgICAgICBmb3IgKHZhciBfaSA9IDA7IF9pIDwgYXJndW1lbnRzLmxlbmd0aDsgX2krKykge1xyXG4gICAgICAgICAgICAgICAgYXJnc1tfaV0gPSBhcmd1bWVudHNbX2ldO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybiBfX2F3YWl0ZXIodGhpcywgdm9pZCAwLCB2b2lkIDAsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgIHZhciByZXN1bHQ7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gX19nZW5lcmF0b3IodGhpcywgZnVuY3Rpb24gKF9hKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgc3dpdGNoIChfYS5sYWJlbCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjYXNlIDA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoYXJncy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zeW5jVmFsdWUoYXJnc1swXSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gWzQgLyp5aWVsZCovLCB0aGlzLnZhbGlkYXRlU2lsZW50KCldO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjYXNlIDE6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXN1bHQgPSBfYS5zZW50KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmFwcGx5UmVzdWx0KHJlc3VsdCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gWzIgLypyZXR1cm4qLywgcmVzdWx0XTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSxcclxuICAgICAgICB2YWxpZGF0ZVNpbGVudDogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICByZXR1cm4gX19hd2FpdGVyKHRoaXMsIHZvaWQgMCwgdm9pZCAwLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgcnVsZXMsIHJlc3VsdDtcclxuICAgICAgICAgICAgICAgIHJldHVybiBfX2dlbmVyYXRvcih0aGlzLCBmdW5jdGlvbiAoX2EpIHtcclxuICAgICAgICAgICAgICAgICAgICBzd2l0Y2ggKF9hLmxhYmVsKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhc2UgMDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0RmxhZ3MoeyBwZW5kaW5nOiB0cnVlIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcnVsZXMgPSBfX2Fzc2lnbihfX2Fzc2lnbih7fSwgdGhpcy5fcmVzb2x2ZWRSdWxlcyksIHRoaXMubm9ybWFsaXplZFJ1bGVzKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShydWxlcywgJ18kJGlzTm9ybWFsaXplZCcsIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB3cml0YWJsZTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZW51bWVyYWJsZTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uZmlndXJhYmxlOiBmYWxzZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gWzQgLyp5aWVsZCovLCB2YWxpZGF0ZSh0aGlzLnZhbHVlLCBydWxlcywge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiB0aGlzLm5hbWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlczogY3JlYXRlVmFsdWVzTG9va3VwKHRoaXMpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBiYWlsczogdGhpcy5iYWlscyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2tpcElmRW1wdHk6IHRoaXMuc2tpcElmRW1wdHksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlzSW5pdGlhbDogIXRoaXMuaW5pdGlhbGl6ZWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGN1c3RvbU1lc3NhZ2VzOiB0aGlzLmN1c3RvbU1lc3NhZ2VzXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSldO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjYXNlIDE6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXN1bHQgPSBfYS5zZW50KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNldEZsYWdzKHsgcGVuZGluZzogZmFsc2UgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNldEZsYWdzKHsgdmFsaWQ6IHJlc3VsdC52YWxpZCwgaW52YWxpZDogIXJlc3VsdC52YWxpZCB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBbMiAvKnJldHVybiovLCByZXN1bHRdO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIHNldEVycm9yczogZnVuY3Rpb24gKGVycm9ycykge1xyXG4gICAgICAgICAgICB0aGlzLmFwcGx5UmVzdWx0KHsgZXJyb3JzOiBlcnJvcnMsIGZhaWxlZFJ1bGVzOiB7fSB9KTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIGFwcGx5UmVzdWx0OiBmdW5jdGlvbiAoX2EpIHtcclxuICAgICAgICAgICAgdmFyIGVycm9ycyA9IF9hLmVycm9ycywgZmFpbGVkUnVsZXMgPSBfYS5mYWlsZWRSdWxlcztcclxuICAgICAgICAgICAgdGhpcy5tZXNzYWdlcyA9IGVycm9ycztcclxuICAgICAgICAgICAgdGhpcy5mYWlsZWRSdWxlcyA9IF9fYXNzaWduKHt9LCAoZmFpbGVkUnVsZXMgfHwge30pKTtcclxuICAgICAgICAgICAgdGhpcy5zZXRGbGFncyh7XHJcbiAgICAgICAgICAgICAgICB2YWxpZDogIWVycm9ycy5sZW5ndGgsXHJcbiAgICAgICAgICAgICAgICBjaGFuZ2VkOiB0aGlzLnZhbHVlICE9PSB0aGlzLmluaXRpYWxWYWx1ZSxcclxuICAgICAgICAgICAgICAgIGludmFsaWQ6ICEhZXJyb3JzLmxlbmd0aCxcclxuICAgICAgICAgICAgICAgIHZhbGlkYXRlZDogdHJ1ZVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIHJlZ2lzdGVyRmllbGQ6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgdXBkYXRlUmVuZGVyaW5nQ29udGV4dFJlZnModGhpcyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59KTtcclxuZnVuY3Rpb24gY3JlYXRlVmFsdWVzTG9va3VwKHZtKSB7XHJcbiAgICB2YXIgcHJvdmlkZXJzID0gdm0uJF92ZWVPYnNlcnZlci5yZWZzO1xyXG4gICAgdmFyIHJlZHVjZWQgPSB7fTtcclxuICAgIHJldHVybiB2bS5maWVsZERlcHMucmVkdWNlKGZ1bmN0aW9uIChhY2MsIGRlcE5hbWUpIHtcclxuICAgICAgICBpZiAoIXByb3ZpZGVyc1tkZXBOYW1lXSkge1xyXG4gICAgICAgICAgICByZXR1cm4gYWNjO1xyXG4gICAgICAgIH1cclxuICAgICAgICBhY2NbZGVwTmFtZV0gPSBwcm92aWRlcnNbZGVwTmFtZV0udmFsdWU7XHJcbiAgICAgICAgcmV0dXJuIGFjYztcclxuICAgIH0sIHJlZHVjZWQpO1xyXG59XHJcbmZ1bmN0aW9uIGV4dHJhY3RJZCh2bSkge1xyXG4gICAgaWYgKHZtLnZpZCkge1xyXG4gICAgICAgIHJldHVybiB2bS52aWQ7XHJcbiAgICB9XHJcbiAgICBpZiAodm0ubmFtZSkge1xyXG4gICAgICAgIHJldHVybiB2bS5uYW1lO1xyXG4gICAgfVxyXG4gICAgaWYgKHZtLmlkKSB7XHJcbiAgICAgICAgcmV0dXJuIHZtLmlkO1xyXG4gICAgfVxyXG4gICAgUFJPVklERVJfQ09VTlRFUisrO1xyXG4gICAgcmV0dXJuIFwiX3ZlZV9cIiArIFBST1ZJREVSX0NPVU5URVI7XHJcbn1cclxuZnVuY3Rpb24gdXBkYXRlUmVuZGVyaW5nQ29udGV4dFJlZnModm0pIHtcclxuICAgIHZhciBwcm92aWRlZElkID0gZXh0cmFjdElkKHZtKTtcclxuICAgIHZhciBpZCA9IHZtLmlkO1xyXG4gICAgLy8gTm90aGluZyBoYXMgY2hhbmdlZC5cclxuICAgIGlmICh2bS5pc0RlYWN0aXZhdGVkIHx8IChpZCA9PT0gcHJvdmlkZWRJZCAmJiB2bS4kX3ZlZU9ic2VydmVyLnJlZnNbaWRdKSkge1xyXG4gICAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIC8vIHZpZCB3YXMgY2hhbmdlZC5cclxuICAgIGlmIChpZCAhPT0gcHJvdmlkZWRJZCAmJiB2bS4kX3ZlZU9ic2VydmVyLnJlZnNbaWRdID09PSB2bSkge1xyXG4gICAgICAgIHZtLiRfdmVlT2JzZXJ2ZXIudW5zdWJzY3JpYmUoaWQpO1xyXG4gICAgfVxyXG4gICAgdm0uaWQgPSBwcm92aWRlZElkO1xyXG4gICAgdm0uJF92ZWVPYnNlcnZlci5zdWJzY3JpYmUodm0pO1xyXG59XHJcbmZ1bmN0aW9uIGNyZWF0ZU9ic2VydmVyKCkge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgICByZWZzOiB7fSxcclxuICAgICAgICBzdWJzY3JpYmU6IGZ1bmN0aW9uICh2bSkge1xyXG4gICAgICAgICAgICB0aGlzLnJlZnNbdm0uaWRdID0gdm07XHJcbiAgICAgICAgfSxcclxuICAgICAgICB1bnN1YnNjcmliZTogZnVuY3Rpb24gKGlkKSB7XHJcbiAgICAgICAgICAgIGRlbGV0ZSB0aGlzLnJlZnNbaWRdO1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcbn1cclxuZnVuY3Rpb24gd2F0Y2hDcm9zc0ZpZWxkRGVwKGN0eCwgZGVwTmFtZSwgd2l0aEhvb2tzKSB7XHJcbiAgICBpZiAod2l0aEhvb2tzID09PSB2b2lkIDApIHsgd2l0aEhvb2tzID0gdHJ1ZTsgfVxyXG4gICAgdmFyIHByb3ZpZGVycyA9IGN0eC4kX3ZlZU9ic2VydmVyLnJlZnM7XHJcbiAgICBpZiAoIWN0eC5fdmVlV2F0Y2hlcnMpIHtcclxuICAgICAgICBjdHguX3ZlZVdhdGNoZXJzID0ge307XHJcbiAgICB9XHJcbiAgICBpZiAoIXByb3ZpZGVyc1tkZXBOYW1lXSAmJiB3aXRoSG9va3MpIHtcclxuICAgICAgICByZXR1cm4gY3R4LiRvbmNlKCdob29rOm1vdW50ZWQnLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIHdhdGNoQ3Jvc3NGaWVsZERlcChjdHgsIGRlcE5hbWUsIGZhbHNlKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuICAgIGlmICghaXNDYWxsYWJsZShjdHguX3ZlZVdhdGNoZXJzW2RlcE5hbWVdKSAmJiBwcm92aWRlcnNbZGVwTmFtZV0pIHtcclxuICAgICAgICBjdHguX3ZlZVdhdGNoZXJzW2RlcE5hbWVdID0gcHJvdmlkZXJzW2RlcE5hbWVdLiR3YXRjaCgndmFsdWUnLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIGlmIChjdHguZmxhZ3MudmFsaWRhdGVkKSB7XHJcbiAgICAgICAgICAgICAgICBjdHguX25lZWRzVmFsaWRhdGlvbiA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICBjdHgudmFsaWRhdGUoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG59XG5cbnZhciBmbGFnTWVyZ2luZ1N0cmF0ZWd5ID0ge1xyXG4gICAgcHJpc3RpbmU6ICdldmVyeScsXHJcbiAgICBkaXJ0eTogJ3NvbWUnLFxyXG4gICAgdG91Y2hlZDogJ3NvbWUnLFxyXG4gICAgdW50b3VjaGVkOiAnZXZlcnknLFxyXG4gICAgdmFsaWQ6ICdldmVyeScsXHJcbiAgICBpbnZhbGlkOiAnc29tZScsXHJcbiAgICBwZW5kaW5nOiAnc29tZScsXHJcbiAgICB2YWxpZGF0ZWQ6ICdldmVyeScsXHJcbiAgICBjaGFuZ2VkOiAnc29tZSdcclxufTtcclxuZnVuY3Rpb24gbWVyZ2VGbGFncyhsaHMsIHJocywgc3RyYXRlZ3kpIHtcclxuICAgIHZhciBzdHJhdE5hbWUgPSBmbGFnTWVyZ2luZ1N0cmF0ZWd5W3N0cmF0ZWd5XTtcclxuICAgIHJldHVybiBbbGhzLCByaHNdW3N0cmF0TmFtZV0oZnVuY3Rpb24gKGYpIHsgcmV0dXJuIGY7IH0pO1xyXG59XHJcbnZhciBPQlNFUlZFUl9DT1VOVEVSID0gMDtcclxuZnVuY3Rpb24gZGF0YSQxKCkge1xyXG4gICAgdmFyIHJlZnMgPSB7fTtcclxuICAgIHZhciByZWZzQnlOYW1lID0ge307XHJcbiAgICB2YXIgaW5hY3RpdmVSZWZzID0ge307XHJcbiAgICAvLyBGSVhNRTogTm90IHN1cmUgb2YgdGhpcyBvbmUgY2FuIGJlIHR5cGVkLCBjaXJjdWxhciB0eXBlIHJlZmVyZW5jZS5cclxuICAgIHZhciBvYnNlcnZlcnMgPSBbXTtcclxuICAgIHJldHVybiB7XHJcbiAgICAgICAgaWQ6ICcnLFxyXG4gICAgICAgIHJlZnM6IHJlZnMsXHJcbiAgICAgICAgcmVmc0J5TmFtZTogcmVmc0J5TmFtZSxcclxuICAgICAgICBvYnNlcnZlcnM6IG9ic2VydmVycyxcclxuICAgICAgICBpbmFjdGl2ZVJlZnM6IGluYWN0aXZlUmVmc1xyXG4gICAgfTtcclxufVxyXG52YXIgVmFsaWRhdGlvbk9ic2VydmVyID0gVnVlLmV4dGVuZCh7XHJcbiAgICBuYW1lOiAnVmFsaWRhdGlvbk9ic2VydmVyJyxcclxuICAgIHByb3ZpZGU6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAkX3ZlZU9ic2VydmVyOiB0aGlzXHJcbiAgICAgICAgfTtcclxuICAgIH0sXHJcbiAgICBpbmplY3Q6IHtcclxuICAgICAgICAkX3ZlZU9ic2VydmVyOiB7XHJcbiAgICAgICAgICAgIGZyb206ICckX3ZlZU9ic2VydmVyJyxcclxuICAgICAgICAgICAgZGVmYXVsdDogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgaWYgKCF0aGlzLiR2bm9kZS5jb250ZXh0LiRfdmVlT2JzZXJ2ZXIpIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLiR2bm9kZS5jb250ZXh0LiRfdmVlT2JzZXJ2ZXI7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAgcHJvcHM6IHtcclxuICAgICAgICB0YWc6IHtcclxuICAgICAgICAgICAgdHlwZTogU3RyaW5nLFxyXG4gICAgICAgICAgICBkZWZhdWx0OiAnc3BhbidcclxuICAgICAgICB9LFxyXG4gICAgICAgIHZpZDoge1xyXG4gICAgICAgICAgICB0eXBlOiBTdHJpbmcsXHJcbiAgICAgICAgICAgIGRlZmF1bHQ6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBcIm9ic19cIiArIE9CU0VSVkVSX0NPVU5URVIrKztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgc2xpbToge1xyXG4gICAgICAgICAgICB0eXBlOiBCb29sZWFuLFxyXG4gICAgICAgICAgICBkZWZhdWx0OiBmYWxzZVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgZGlzYWJsZWQ6IHtcclxuICAgICAgICAgICAgdHlwZTogQm9vbGVhbixcclxuICAgICAgICAgICAgZGVmYXVsdDogZmFsc2VcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAgZGF0YTogZGF0YSQxLFxyXG4gICAgY29tcHV0ZWQ6IHtcclxuICAgICAgICBjdHg6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgdmFyIF90aGlzID0gdGhpcztcclxuICAgICAgICAgICAgdmFyIGN0eCA9IHtcclxuICAgICAgICAgICAgICAgIGVycm9yczoge30sXHJcbiAgICAgICAgICAgICAgICBwYXNzZXM6IGZ1bmN0aW9uIChjYikge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBfdGhpcy52YWxpZGF0ZSgpLnRoZW4oZnVuY3Rpb24gKHJlc3VsdCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVzdWx0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gY2IoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIHZhbGlkYXRlOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFyIGFyZ3MgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgICBmb3IgKHZhciBfaSA9IDA7IF9pIDwgYXJndW1lbnRzLmxlbmd0aDsgX2krKykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhcmdzW19pXSA9IGFyZ3VtZW50c1tfaV07XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBfdGhpcy52YWxpZGF0ZS5hcHBseShfdGhpcywgYXJncyk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgcmVzZXQ6IGZ1bmN0aW9uICgpIHsgcmV0dXJuIF90aGlzLnJlc2V0KCk7IH1cclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgcmV0dXJuIF9fc3ByZWFkQXJyYXlzKHZhbHVlcyh0aGlzLnJlZnMpLCBPYmplY3Qua2V5cyh0aGlzLmluYWN0aXZlUmVmcykubWFwKGZ1bmN0aW9uIChrZXkpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdmlkOiBrZXksXHJcbiAgICAgICAgICAgICAgICAgICAgZmxhZ3M6IF90aGlzLmluYWN0aXZlUmVmc1trZXldLmZsYWdzLFxyXG4gICAgICAgICAgICAgICAgICAgIG1lc3NhZ2VzOiBfdGhpcy5pbmFjdGl2ZVJlZnNba2V5XS5lcnJvcnNcclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIH0pLCB0aGlzLm9ic2VydmVycykucmVkdWNlKGZ1bmN0aW9uIChhY2MsIHByb3ZpZGVyKSB7XHJcbiAgICAgICAgICAgICAgICBPYmplY3Qua2V5cyhmbGFnTWVyZ2luZ1N0cmF0ZWd5KS5mb3JFYWNoKGZ1bmN0aW9uIChmbGFnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFyIGZsYWdzID0gcHJvdmlkZXIuZmxhZ3MgfHwgcHJvdmlkZXIuY3R4O1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICghKGZsYWcgaW4gYWNjKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhY2NbZmxhZ10gPSBmbGFnc1tmbGFnXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBhY2NbZmxhZ10gPSBtZXJnZUZsYWdzKGFjY1tmbGFnXSwgZmxhZ3NbZmxhZ10sIGZsYWcpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICBhY2MuZXJyb3JzW3Byb3ZpZGVyLmlkXSA9XHJcbiAgICAgICAgICAgICAgICAgICAgcHJvdmlkZXIubWVzc2FnZXMgfHxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWVzKHByb3ZpZGVyLmN0eC5lcnJvcnMpLnJlZHVjZShmdW5jdGlvbiAoZXJycywgb2JzRXJyb3JzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gZXJycy5jb25jYXQob2JzRXJyb3JzKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSwgW10pO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGFjYztcclxuICAgICAgICAgICAgfSwgY3R4KTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAgY3JlYXRlZDogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHRoaXMuaWQgPSB0aGlzLnZpZDtcclxuICAgICAgICBpZiAodGhpcy4kX3ZlZU9ic2VydmVyKSB7XHJcbiAgICAgICAgICAgIHRoaXMuJF92ZWVPYnNlcnZlci5zdWJzY3JpYmUodGhpcywgJ29ic2VydmVyJyk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIGFjdGl2YXRlZDogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIGlmICh0aGlzLiRfdmVlT2JzZXJ2ZXIpIHtcclxuICAgICAgICAgICAgdGhpcy4kX3ZlZU9ic2VydmVyLnN1YnNjcmliZSh0aGlzLCAnb2JzZXJ2ZXInKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAgZGVhY3RpdmF0ZWQ6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICBpZiAodGhpcy4kX3ZlZU9ic2VydmVyKSB7XHJcbiAgICAgICAgICAgIHRoaXMuJF92ZWVPYnNlcnZlci51bnN1YnNjcmliZSh0aGlzLmlkLCAnb2JzZXJ2ZXInKTtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAgYmVmb3JlRGVzdHJveTogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIGlmICh0aGlzLiRfdmVlT2JzZXJ2ZXIpIHtcclxuICAgICAgICAgICAgdGhpcy4kX3ZlZU9ic2VydmVyLnVuc3Vic2NyaWJlKHRoaXMuaWQsICdvYnNlcnZlcicpO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICByZW5kZXI6IGZ1bmN0aW9uIChoKSB7XHJcbiAgICAgICAgdmFyIGNoaWxkcmVuID0gbm9ybWFsaXplQ2hpbGRyZW4odGhpcywgdGhpcy5jdHgpO1xyXG4gICAgICAgIHJldHVybiB0aGlzLnNsaW0gJiYgY2hpbGRyZW4ubGVuZ3RoIDw9IDEgPyBjaGlsZHJlblswXSA6IGgodGhpcy50YWcsIHsgb246IHRoaXMuJGxpc3RlbmVycyB9LCBjaGlsZHJlbik7XHJcbiAgICB9LFxyXG4gICAgbWV0aG9kczoge1xyXG4gICAgICAgIHN1YnNjcmliZTogZnVuY3Rpb24gKHN1YnNjcmliZXIsIGtpbmQpIHtcclxuICAgICAgICAgICAgdmFyIF9hLCBfYjtcclxuICAgICAgICAgICAgaWYgKGtpbmQgPT09IHZvaWQgMCkgeyBraW5kID0gJ3Byb3ZpZGVyJzsgfVxyXG4gICAgICAgICAgICBpZiAoa2luZCA9PT0gJ29ic2VydmVyJykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5vYnNlcnZlcnMucHVzaChzdWJzY3JpYmVyKTtcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLnJlZnMgPSBfX2Fzc2lnbihfX2Fzc2lnbih7fSwgdGhpcy5yZWZzKSwgKF9hID0ge30sIF9hW3N1YnNjcmliZXIuaWRdID0gc3Vic2NyaWJlciwgX2EpKTtcclxuICAgICAgICAgICAgdGhpcy5yZWZzQnlOYW1lID0gX19hc3NpZ24oX19hc3NpZ24oe30sIHRoaXMucmVmc0J5TmFtZSksIChfYiA9IHt9LCBfYltzdWJzY3JpYmVyLm5hbWVdID0gc3Vic2NyaWJlciwgX2IpKTtcclxuICAgICAgICAgICAgaWYgKHN1YnNjcmliZXIucGVyc2lzdCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5yZXN0b3JlUHJvdmlkZXJTdGF0ZShzdWJzY3JpYmVyKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgdW5zdWJzY3JpYmU6IGZ1bmN0aW9uIChpZCwga2luZCkge1xyXG4gICAgICAgICAgICBpZiAoa2luZCA9PT0gdm9pZCAwKSB7IGtpbmQgPSAncHJvdmlkZXInOyB9XHJcbiAgICAgICAgICAgIGlmIChraW5kID09PSAncHJvdmlkZXInKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnJlbW92ZVByb3ZpZGVyKGlkKTtcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB2YXIgaWR4ID0gZmluZEluZGV4KHRoaXMub2JzZXJ2ZXJzLCBmdW5jdGlvbiAobykgeyByZXR1cm4gby5pZCA9PT0gaWQ7IH0pO1xyXG4gICAgICAgICAgICBpZiAoaWR4ICE9PSAtMSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5vYnNlcnZlcnMuc3BsaWNlKGlkeCwgMSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG4gICAgICAgIHZhbGlkYXRlOiBmdW5jdGlvbiAoX2EpIHtcclxuICAgICAgICAgICAgdmFyIF9iID0gKF9hID09PSB2b2lkIDAgPyB7fSA6IF9hKS5zaWxlbnQsIHNpbGVudCA9IF9iID09PSB2b2lkIDAgPyBmYWxzZSA6IF9iO1xyXG4gICAgICAgICAgICByZXR1cm4gX19hd2FpdGVyKHRoaXMsIHZvaWQgMCwgdm9pZCAwLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgcmVzdWx0cztcclxuICAgICAgICAgICAgICAgIHJldHVybiBfX2dlbmVyYXRvcih0aGlzLCBmdW5jdGlvbiAoX2MpIHtcclxuICAgICAgICAgICAgICAgICAgICBzd2l0Y2ggKF9jLmxhYmVsKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhc2UgMDogcmV0dXJuIFs0IC8qeWllbGQqLywgUHJvbWlzZS5hbGwoX19zcHJlYWRBcnJheXModmFsdWVzKHRoaXMucmVmcylcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuZmlsdGVyKGZ1bmN0aW9uIChyKSB7IHJldHVybiAhci5kaXNhYmxlZDsgfSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAubWFwKGZ1bmN0aW9uIChyZWYpIHsgcmV0dXJuIHJlZltzaWxlbnQgPyAndmFsaWRhdGVTaWxlbnQnIDogJ3ZhbGlkYXRlJ10oKS50aGVuKGZ1bmN0aW9uIChyKSB7IHJldHVybiByLnZhbGlkOyB9KTsgfSksIHRoaXMub2JzZXJ2ZXJzLmZpbHRlcihmdW5jdGlvbiAobykgeyByZXR1cm4gIW8uZGlzYWJsZWQ7IH0pLm1hcChmdW5jdGlvbiAob2JzKSB7IHJldHVybiBvYnMudmFsaWRhdGUoeyBzaWxlbnQ6IHNpbGVudCB9KTsgfSkpKV07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhc2UgMTpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc3VsdHMgPSBfYy5zZW50KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gWzIgLypyZXR1cm4qLywgcmVzdWx0cy5ldmVyeShmdW5jdGlvbiAocikgeyByZXR1cm4gcjsgfSldO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIHJlc2V0OiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIHZhciBfdGhpcyA9IHRoaXM7XHJcbiAgICAgICAgICAgIE9iamVjdC5rZXlzKHRoaXMuaW5hY3RpdmVSZWZzKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcclxuICAgICAgICAgICAgICAgIF90aGlzLiRkZWxldGUoX3RoaXMuaW5hY3RpdmVSZWZzLCBrZXkpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgcmV0dXJuIF9fc3ByZWFkQXJyYXlzKHZhbHVlcyh0aGlzLnJlZnMpLCB0aGlzLm9ic2VydmVycykuZm9yRWFjaChmdW5jdGlvbiAocmVmKSB7IHJldHVybiByZWYucmVzZXQoKTsgfSk7XHJcbiAgICAgICAgfSxcclxuICAgICAgICByZXN0b3JlUHJvdmlkZXJTdGF0ZTogZnVuY3Rpb24gKHByb3ZpZGVyKSB7XHJcbiAgICAgICAgICAgIHZhciBpZCA9IHByb3ZpZGVyLmlkO1xyXG4gICAgICAgICAgICB2YXIgc3RhdGUgPSB0aGlzLmluYWN0aXZlUmVmc1tpZF07XHJcbiAgICAgICAgICAgIGlmICghc3RhdGUpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBwcm92aWRlci5zZXRGbGFncyhzdGF0ZS5mbGFncyk7XHJcbiAgICAgICAgICAgIHByb3ZpZGVyLmFwcGx5UmVzdWx0KHN0YXRlKTtcclxuICAgICAgICAgICAgdGhpcy4kZGVsZXRlKHRoaXMuaW5hY3RpdmVSZWZzLCBwcm92aWRlci5pZCk7XHJcbiAgICAgICAgfSxcclxuICAgICAgICByZW1vdmVQcm92aWRlcjogZnVuY3Rpb24gKGlkKSB7XHJcbiAgICAgICAgICAgIHZhciBwcm92aWRlciA9IHRoaXMucmVmc1tpZF07XHJcbiAgICAgICAgICAgIGlmICghcHJvdmlkZXIpIHtcclxuICAgICAgICAgICAgICAgIC8vIEZJWE1FOiBpbmFjdGl2ZSByZWZzIGFyZSBub3QgYmVpbmcgY2xlYW5lZCB1cC5cclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAocHJvdmlkZXIucGVyc2lzdCkge1xyXG4gICAgICAgICAgICAgICAgLyogaXN0YW5idWwgaWdub3JlIG5leHQgKi9cclxuICAgICAgICAgICAgICAgIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGlkLmluZGV4T2YoJ192ZWVfJykgPT09IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgd2FybignUGxlYXNlIHByb3ZpZGUgYSBgdmlkYCBvciBhIGBuYW1lYCBwcm9wIHdoZW4gdXNpbmcgYHBlcnNpc3RgLCB0aGVyZSBtaWdodCBiZSB1bmV4cGVjdGVkIGlzc3VlcyBvdGhlcndpc2UuJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgLy8gc2F2ZSBpdCBmb3IgdGhlIG5leHQgdGltZS5cclxuICAgICAgICAgICAgICAgIHRoaXMuaW5hY3RpdmVSZWZzW2lkXSA9IHtcclxuICAgICAgICAgICAgICAgICAgICBmbGFnczogcHJvdmlkZXIuZmxhZ3MsXHJcbiAgICAgICAgICAgICAgICAgICAgZXJyb3JzOiBwcm92aWRlci5tZXNzYWdlcyxcclxuICAgICAgICAgICAgICAgICAgICBmYWlsZWRSdWxlczogcHJvdmlkZXIuZmFpbGVkUnVsZXNcclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy4kZGVsZXRlKHRoaXMucmVmcywgaWQpO1xyXG4gICAgICAgICAgICB0aGlzLiRkZWxldGUodGhpcy5yZWZzQnlOYW1lLCBwcm92aWRlci5uYW1lKTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIHNldEVycm9yczogZnVuY3Rpb24gKGVycm9ycykge1xyXG4gICAgICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xyXG4gICAgICAgICAgICBPYmplY3Qua2V5cyhlcnJvcnMpLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xyXG4gICAgICAgICAgICAgICAgdmFyIHByb3ZpZGVyID0gX3RoaXMucmVmc1trZXldIHx8IF90aGlzLnJlZnNCeU5hbWVba2V5XTtcclxuICAgICAgICAgICAgICAgIGlmICghcHJvdmlkZXIpXHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgcHJvdmlkZXIuc2V0RXJyb3JzKGVycm9yc1trZXldIHx8IFtdKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIHRoaXMub2JzZXJ2ZXJzLmZvckVhY2goZnVuY3Rpb24gKG9ic2VydmVyKSB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5zZXRFcnJvcnMoZXJyb3JzKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59KTtcblxuZnVuY3Rpb24gd2l0aFZhbGlkYXRpb24oY29tcG9uZW50LCBtYXBQcm9wcykge1xyXG4gICAgaWYgKG1hcFByb3BzID09PSB2b2lkIDApIHsgbWFwUHJvcHMgPSBpZGVudGl0eTsgfVxyXG4gICAgdmFyIG9wdGlvbnMgPSAnb3B0aW9ucycgaW4gY29tcG9uZW50ID8gY29tcG9uZW50Lm9wdGlvbnMgOiBjb21wb25lbnQ7XHJcbiAgICB2YXIgcHJvdmlkZXJPcHRzID0gVmFsaWRhdGlvblByb3ZpZGVyLm9wdGlvbnM7XHJcbiAgICB2YXIgaG9jID0ge1xyXG4gICAgICAgIG5hbWU6IChvcHRpb25zLm5hbWUgfHwgJ0Fub255bW91c0hvYycpICsgXCJXaXRoVmFsaWRhdGlvblwiLFxyXG4gICAgICAgIHByb3BzOiBfX2Fzc2lnbih7fSwgcHJvdmlkZXJPcHRzLnByb3BzKSxcclxuICAgICAgICBkYXRhOiBwcm92aWRlck9wdHMuZGF0YSxcclxuICAgICAgICBjb21wdXRlZDogX19hc3NpZ24oe30sIHByb3ZpZGVyT3B0cy5jb21wdXRlZCksXHJcbiAgICAgICAgbWV0aG9kczogX19hc3NpZ24oe30sIHByb3ZpZGVyT3B0cy5tZXRob2RzKSxcclxuICAgICAgICBiZWZvcmVEZXN0cm95OiBwcm92aWRlck9wdHMuYmVmb3JlRGVzdHJveSxcclxuICAgICAgICBpbmplY3Q6IHByb3ZpZGVyT3B0cy5pbmplY3RcclxuICAgIH07XHJcbiAgICB2YXIgZXZlbnROYW1lID0gKG9wdGlvbnMubW9kZWwgJiYgb3B0aW9ucy5tb2RlbC5ldmVudCkgfHwgJ2lucHV0JztcclxuICAgIGhvYy5yZW5kZXIgPSBmdW5jdGlvbiAoaCkge1xyXG4gICAgICAgIHZhciBfYTtcclxuICAgICAgICB0aGlzLnJlZ2lzdGVyRmllbGQoKTtcclxuICAgICAgICB2YXIgdmN0eCA9IGNyZWF0ZVZhbGlkYXRpb25DdHgodGhpcyk7XHJcbiAgICAgICAgdmFyIGxpc3RlbmVycyA9IF9fYXNzaWduKHt9LCB0aGlzLiRsaXN0ZW5lcnMpO1xyXG4gICAgICAgIHZhciBtb2RlbCA9IGZpbmRNb2RlbCh0aGlzLiR2bm9kZSk7XHJcbiAgICAgICAgdGhpcy5faW5wdXRFdmVudE5hbWUgPSB0aGlzLl9pbnB1dEV2ZW50TmFtZSB8fCBnZXRJbnB1dEV2ZW50TmFtZSh0aGlzLiR2bm9kZSwgbW9kZWwpO1xyXG4gICAgICAgIHZhciB2YWx1ZSA9IGZpbmRWYWx1ZSh0aGlzLiR2bm9kZSk7XHJcbiAgICAgICAgb25SZW5kZXJVcGRhdGUodGhpcywgdmFsdWUgJiYgdmFsdWUudmFsdWUpO1xyXG4gICAgICAgIHZhciBfYiA9IGNyZWF0ZUNvbW1vbkhhbmRsZXJzKHRoaXMpLCBvbklucHV0ID0gX2Iub25JbnB1dCwgb25CbHVyID0gX2Iub25CbHVyLCBvblZhbGlkYXRlID0gX2Iub25WYWxpZGF0ZTtcclxuICAgICAgICBtZXJnZVZOb2RlTGlzdGVuZXJzKGxpc3RlbmVycywgZXZlbnROYW1lLCBvbklucHV0KTtcclxuICAgICAgICBtZXJnZVZOb2RlTGlzdGVuZXJzKGxpc3RlbmVycywgJ2JsdXInLCBvbkJsdXIpO1xyXG4gICAgICAgIHRoaXMubm9ybWFsaXplZEV2ZW50cy5mb3JFYWNoKGZ1bmN0aW9uIChldnQpIHtcclxuICAgICAgICAgICAgbWVyZ2VWTm9kZUxpc3RlbmVycyhsaXN0ZW5lcnMsIGV2dCwgb25WYWxpZGF0ZSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgLy8gUHJvcHMgYXJlIGFueSBhdHRycyBub3QgYXNzb2NpYXRlZCB3aXRoIFZhbGlkYXRpb25Qcm92aWRlciBQbHVzIHRoZSBtb2RlbCBwcm9wLlxyXG4gICAgICAgIC8vIFdBUk5JTkc6IEFjY2lkZW50YWwgcHJvcCBvdmVyd3JpdGUgd2lsbCBwcm9iYWJseSBoYXBwZW4uXHJcbiAgICAgICAgdmFyIHByb3AgPSAoZmluZE1vZGVsQ29uZmlnKHRoaXMuJHZub2RlKSB8fCB7IHByb3A6ICd2YWx1ZScgfSkucHJvcDtcclxuICAgICAgICB2YXIgcHJvcHMgPSBfX2Fzc2lnbihfX2Fzc2lnbihfX2Fzc2lnbih7fSwgdGhpcy4kYXR0cnMpLCAoX2EgPSB7fSwgX2FbcHJvcF0gPSBtb2RlbCAmJiBtb2RlbC52YWx1ZSwgX2EpKSwgbWFwUHJvcHModmN0eCkpO1xyXG4gICAgICAgIHJldHVybiBoKG9wdGlvbnMsIHtcclxuICAgICAgICAgICAgYXR0cnM6IHRoaXMuJGF0dHJzLFxyXG4gICAgICAgICAgICBwcm9wczogcHJvcHMsXHJcbiAgICAgICAgICAgIG9uOiBsaXN0ZW5lcnNcclxuICAgICAgICB9LCBub3JtYWxpemVTbG90cyh0aGlzLiRzbG90cywgdGhpcy4kdm5vZGUuY29udGV4dCkpO1xyXG4gICAgfTtcclxuICAgIHJldHVybiBob2M7XHJcbn1cblxudmFyIHZlcnNpb24gPSAnMy4wLjExJztcblxuZXhwb3J0IHsgVmFsaWRhdGlvbk9ic2VydmVyLCBWYWxpZGF0aW9uUHJvdmlkZXIsIGNvbmZpZ3VyZSwgZXh0ZW5kLCBpbnN0YWxsLCBsb2NhbGl6ZSwgc2V0SW50ZXJhY3Rpb25Nb2RlLCB2YWxpZGF0ZSwgdmVyc2lvbiwgd2l0aFZhbGlkYXRpb24gfTtcbiIsInZhciByZW5kZXIgPSBmdW5jdGlvbigpIHtcbiAgdmFyIF92bSA9IHRoaXNcbiAgdmFyIF9oID0gX3ZtLiRjcmVhdGVFbGVtZW50XG4gIHZhciBfYyA9IF92bS5fc2VsZi5fYyB8fCBfaFxuICByZXR1cm4gX2MoXG4gICAgXCJkaXZcIixcbiAgICB7IHN0YXRpY0NsYXNzOiBcIm1vZGFsIGZhZGUgbW9kYWwtZnNcIiwgYXR0cnM6IHsgaWQ6IFwibW9kYWwtZnNcIiB9IH0sXG4gICAgW1xuICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJtb2RhbC1kaWFsb2dcIiwgYXR0cnM6IHsgcm9sZTogXCJkb2N1bWVudFwiIH0gfSwgW1xuICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcIm1vZGFsLWNvbnRlbnRcIiB9LCBbXG4gICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJtb2RhbC1ib2R5XCIgfSwgW1xuICAgICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjb250YWluZXIgdGV4dC1jZW50ZXJcIiB9LCBbXG4gICAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwicm93IGZ1bGwtaGVpZ2h0IGFsaWduLWl0ZW1zLWNlbnRlclwiIH0sIFtcbiAgICAgICAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImNvbC1tZC01IG0taC1hdXRvXCIgfSwgW1xuICAgICAgICAgICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJwLWgtMzAgcC1iLTUwXCIgfSwgW1xuICAgICAgICAgICAgICAgICAgICBfdm0uX20oMCksXG4gICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgIF9jKFwiaDFcIiwgeyBzdGF0aWNDbGFzczogXCJ0ZXh0LXRoaW4gbS12LTE1XCIgfSwgW1xuICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihfdm0uX3MoX3ZtLnRpdGxlKSlcbiAgICAgICAgICAgICAgICAgICAgXSksXG4gICAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICAgIF9jKFwicFwiLCB7IHN0YXRpY0NsYXNzOiBcIm0tYi0yNSBsZWFkXCIgfSwgW1xuICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihfdm0uX3MoX3ZtLmNvbnRlbnQpKVxuICAgICAgICAgICAgICAgICAgICBdKSxcbiAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJ0ZXh0LWNlbnRlclwiIH0sIFtcbiAgICAgICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiYnV0dG9uXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tZGVmYXVsdCBidG4tbGdcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgb246IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGljazogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkZXZlbnQucHJldmVudERlZmF1bHQoKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIF92bS5jb25maXJtKCRldmVudClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICBbX3ZtLl92KFwiQ29uZmlybVxcbiAgICAgICAgICAgICAgICAgIFwiKV1cbiAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICBdKSxcbiAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgXCJhXCIsXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJtb2RhbC1jbG9zZVwiLFxuICAgICAgICAgICAgICAgIGF0dHJzOiB7IGhyZWY6IFwiI1wiLCBcImRhdGEtZGlzbWlzc1wiOiBcIm1vZGFsXCIgfSxcbiAgICAgICAgICAgICAgICBvbjoge1xuICAgICAgICAgICAgICAgICAgY2xpY2s6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgICAgICAgICAgICAkZXZlbnQucHJldmVudERlZmF1bHQoKVxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gX3ZtLmNvbmZpcm0oJGV2ZW50KVxuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgW19jKFwiaVwiLCB7IHN0YXRpY0NsYXNzOiBcInRpLWNsb3NlXCIgfSldXG4gICAgICAgICAgICApXG4gICAgICAgICAgXSlcbiAgICAgICAgXSlcbiAgICAgIF0pXG4gICAgXVxuICApXG59XG52YXIgc3RhdGljUmVuZGVyRm5zID0gW1xuICBmdW5jdGlvbigpIHtcbiAgICB2YXIgX3ZtID0gdGhpc1xuICAgIHZhciBfaCA9IF92bS4kY3JlYXRlRWxlbWVudFxuICAgIHZhciBfYyA9IF92bS5fc2VsZi5fYyB8fCBfaFxuICAgIHJldHVybiBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcInRleHQtY2VudGVyIGZvbnQtc2l6ZS03MFwiIH0sIFtcbiAgICAgIF9jKFwiaVwiLCB7XG4gICAgICAgIHN0YXRpY0NsYXNzOlxuICAgICAgICAgIFwibWRpIG1kaS1jaGVja2JveC1tYXJrZWQtY2lyY2xlLW91dGxpbmUgaWNvbi1ncmFkaWVudC1zdWNjZXNzXCJcbiAgICAgIH0pXG4gICAgXSlcbiAgfVxuXVxucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5cbmV4cG9ydCB7IHJlbmRlciwgc3RhdGljUmVuZGVyRm5zIH0iLCJ2YXIgcmVuZGVyID0gZnVuY3Rpb24oKSB7XG4gIHZhciBfdm0gPSB0aGlzXG4gIHZhciBfaCA9IF92bS4kY3JlYXRlRWxlbWVudFxuICB2YXIgX2MgPSBfdm0uX3NlbGYuX2MgfHwgX2hcbiAgcmV0dXJuIF9jKFwicm91dGVyLXZpZXdcIilcbn1cbnZhciBzdGF0aWNSZW5kZXJGbnMgPSBbXVxucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5cbmV4cG9ydCB7IHJlbmRlciwgc3RhdGljUmVuZGVyRm5zIH0iLCJ2YXIgcmVuZGVyID0gZnVuY3Rpb24oKSB7XG4gIHZhciBfdm0gPSB0aGlzXG4gIHZhciBfaCA9IF92bS4kY3JlYXRlRWxlbWVudFxuICB2YXIgX2MgPSBfdm0uX3NlbGYuX2MgfHwgX2hcbiAgcmV0dXJuIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY29udGFpbmVyLWZsdWlkXCIgfSwgW1xuICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwicGFnZS1oZWFkZXJcIiB9LCBbXG4gICAgICBfYyhcImgyXCIsIHsgc3RhdGljQ2xhc3M6IFwiaGVhZGVyLXRpdGxlXCIgfSwgW192bS5fdihcIk5nxrDhu51pIGTDuW5nXCIpXSksXG4gICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJoZWFkZXItc3ViLXRpdGxlXCIgfSwgW1xuICAgICAgICBfYyhcIm5hdlwiLCB7IHN0YXRpY0NsYXNzOiBcImJyZWFkY3J1bWIgYnJlYWRjcnVtYi1kYXNoXCIgfSwgW1xuICAgICAgICAgIF92bS5fbSgwKSxcbiAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgIF9jKFxuICAgICAgICAgICAgXCJhXCIsXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImJyZWFkY3J1bWItaXRlbVwiLFxuICAgICAgICAgICAgICBhdHRyczogeyBocmVmOiBcIiNcIiB9LFxuICAgICAgICAgICAgICBvbjoge1xuICAgICAgICAgICAgICAgIGNsaWNrOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAgICRldmVudC5wcmV2ZW50RGVmYXVsdCgpXG4gICAgICAgICAgICAgICAgICByZXR1cm4gX3ZtLmdvUmVhZCgpXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgW19jKFwiaVwiLCB7IHN0YXRpY0NsYXNzOiBcInRpLXJvbGUgcC1yLTVcIiB9KSwgX3ZtLl92KFwiTmjDs21cIildXG4gICAgICAgICAgKSxcbiAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgIF92bS5fbSgxKVxuICAgICAgICBdKVxuICAgICAgXSksXG4gICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJfaGVhZGVyLWJ1dHRvblwiIH0sIFtcbiAgICAgICAgX2MoXG4gICAgICAgICAgXCJidXR0b25cIixcbiAgICAgICAgICB7XG4gICAgICAgICAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLWdyYWRpZW50LXN1Y2Nlc3NcIixcbiAgICAgICAgICAgIG9uOiB7XG4gICAgICAgICAgICAgIGNsaWNrOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAkZXZlbnQucHJldmVudERlZmF1bHQoKVxuICAgICAgICAgICAgICAgIHJldHVybiBfdm0uZ29DcmVhdGUoKVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSxcbiAgICAgICAgICBbX3ZtLl92KFwiVOG6oW8gbmfGsOG7nWkgZMO5bmdcXG4gICAgICBcIildXG4gICAgICAgIClcbiAgICAgIF0pXG4gICAgXSksXG4gICAgX3ZtLl92KFwiIFwiKSxcbiAgICBfYyhcbiAgICAgIFwiZGl2XCIsXG4gICAgICB7IHN0YXRpY0NsYXNzOiBcImNhcmRcIiB9LFxuICAgICAgW1xuICAgICAgICBfYyhcInZhbGlkYXRpb24tb2JzZXJ2ZXJcIiwge1xuICAgICAgICAgIHJlZjogXCJvYnNlcnZlclwiLFxuICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImNhcmQtYm9keVwiLFxuICAgICAgICAgIGF0dHJzOiB7IHRhZzogXCJkaXZcIiB9LFxuICAgICAgICAgIHNjb3BlZFNsb3RzOiBfdm0uX3UoW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBrZXk6IFwiZGVmYXVsdFwiLFxuICAgICAgICAgICAgICBmbjogZnVuY3Rpb24ocmVmKSB7XG4gICAgICAgICAgICAgICAgdmFyIGludmFsaWQgPSByZWYuaW52YWxpZFxuICAgICAgICAgICAgICAgIHJldHVybiBbXG4gICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgXCJkaXZcIixcbiAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcInJvdyBtLXYtMzBcIixcbiAgICAgICAgICAgICAgICAgICAgICBvbjoge1xuICAgICAgICAgICAgICAgICAgICAgICAga2V5dXA6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgISRldmVudC50eXBlLmluZGV4T2YoXCJrZXlcIikgJiZcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX2soXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkZXZlbnQua2V5Q29kZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiZW50ZXJcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDEzLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJGV2ZW50LmtleSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiRW50ZXJcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG51bGxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gX3ZtLnVwZGF0ZShpbnZhbGlkKVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgW1xuICAgICAgICAgICAgICAgICAgICAgIF9jKFwiZGl2XCIsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImNvbC1zbS0zIHRleHQtY2VudGVyIHRleHQtc20tbGVmdFwiXG4gICAgICAgICAgICAgICAgICAgICAgfSksXG4gICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiZGl2XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICB7IHN0YXRpY0NsYXNzOiBcImNvbC1zbS02IHRleHQtY2VudGVyIHRleHQtc20tbGVmdFwiIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwidmFsaWRhdGlvbi1wcm92aWRlclwiLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF0dHJzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiBcIlTDqm4gbmjDs20gcXV54buBblwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcnVsZXM6IFwicmVxdWlyZWRcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRhZzogXCJkaXZcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGVkU2xvdHM6IF92bS5fdShcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGtleTogXCJkZWZhdWx0XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZm46IGZ1bmN0aW9uKHJlZikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGVycm9ycyA9IHJlZi5lcnJvcnNcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciB2YWxpZCA9IHJlZi52YWxpZFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJsYWJlbFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImNvbnRyb2wtbGFiZWxcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF0dHJzOiB7IGZvcjogXCJpY29uLWlucHV0XCIgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgW192bS5fdihcIk5hbWU6XCIpXVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImljb24taW5wdXRcIiB9LCBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJpXCIsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcIm1kaSBtZGktYWNjb3VudFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcImlucHV0XCIsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpcmVjdGl2ZXM6IFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogX3ZtLmZybURhdGEucm9sZV9uYW1lLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV4cHJlc3Npb246IFwiZnJtRGF0YS5yb2xlX25hbWVcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBlcnJvcjpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uZmxhZy5pc1ZhbGlkYXRpbmcgJiYgIXZhbGlkXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXR0cnM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogXCJ0ZXh0XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyOiBcIlJvbGUgbmFtZS4uLlwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZG9tUHJvcHM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IF92bS5mcm1EYXRhLnJvbGVfbmFtZVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlucHV0OiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoJGV2ZW50LnRhcmdldC5jb21wb3NpbmcpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVyblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uJHNldChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5mcm1EYXRhLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJyb2xlX25hbWVcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRldmVudC50YXJnZXQudmFsdWVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJsYWJlbFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpcmVjdGl2ZXM6IFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IFwic2hvd1wiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uZmxhZy5pc1ZhbGlkYXRpbmcgJiYgIXZhbGlkLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV4cHJlc3Npb246XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImZsYWcuaXNWYWxpZGF0aW5nICYmICF2YWxpZFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJlcnJvclwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbX3ZtLl92KF92bS5fcyhlcnJvcnNbMF0pKV1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bGwsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0cnVlXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICB9KSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjaGVja2JveFwiIH0sIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcImlucHV0XCIsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpcmVjdGl2ZXM6IFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogX3ZtLmZybURhdGEuaXNfZnVsbGFjY2VzcyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBleHByZXNzaW9uOiBcImZybURhdGEuaXNfZnVsbGFjY2Vzc1wiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhdHRyczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZDogXCJjaGVja2JveDFcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogXCJjaGVja2JveFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInRydWUtdmFsdWVcIjogXCIxXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiZmFsc2UtdmFsdWVcIjogXCIwXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkb21Qcm9wczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjaGVja2VkOiBBcnJheS5pc0FycmF5KFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5mcm1EYXRhLmlzX2Z1bGxhY2Nlc3NcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgID8gX3ZtLl9pKF92bS5mcm1EYXRhLmlzX2Z1bGxhY2Nlc3MsIG51bGwpID4gLTFcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6IF92bS5fcShfdm0uZnJtRGF0YS5pc19mdWxsYWNjZXNzLCBcIjFcIilcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjaGFuZ2U6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciAkJGEgPSBfdm0uZnJtRGF0YS5pc19mdWxsYWNjZXNzLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJCRlbCA9ICRldmVudC50YXJnZXQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkJGMgPSAkJGVsLmNoZWNrZWQgPyBcIjFcIiA6IFwiMFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKEFycmF5LmlzQXJyYXkoJCRhKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyICQkdiA9IG51bGwsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICQkaSA9IF92bS5faSgkJGEsICQkdilcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICgkJGVsLmNoZWNrZWQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJCRpIDwgMCAmJlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS4kc2V0KFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLmZybURhdGEsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImlzX2Z1bGxhY2Nlc3NcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICQkYS5jb25jYXQoWyQkdl0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJCRpID4gLTEgJiZcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uJHNldChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5mcm1EYXRhLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJpc19mdWxsYWNjZXNzXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkJGFcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLnNsaWNlKDAsICQkaSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLmNvbmNhdCgkJGEuc2xpY2UoJCRpICsgMSkpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uJHNldChfdm0uZnJtRGF0YSwgXCJpc19mdWxsYWNjZXNzXCIsICQkYylcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwibGFiZWxcIiwgeyBhdHRyczogeyBmb3I6IFwiY2hlY2tib3gxXCIgfSB9LCBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCJLw61jaCBob+G6oXRcIilcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgICAgICAgICAgICAgICBdKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJtLXQtMjVcIiB9LCBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImJ1dHRvblwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLXN1Y2Nlc3NcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXR0cnM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZDogXCJzYXZlXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogXCJidXR0b25cIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZDogX3ZtLmZsYWcuaXNVcGRhdGluZ1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsaWNrOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRldmVudC5wcmV2ZW50RGVmYXVsdCgpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gX3ZtLnVwZGF0ZShpbnZhbGlkKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLmZsYWcuaXNVcGRhdGluZ1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgID8gW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcImlcIiwge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJmYSBmYS1zcGlubmVyIGZhLXB1bHNlIGZhLWZ3IG0tci01XCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIlVwZGF0aW5nXFxuICAgICAgICAgICAgICBcIilcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6IFtfdm0uX3YoXCJVcGRhdGVcIildXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgMlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiYnV0dG9uXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tZGVmYXVsdFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhdHRyczogeyBpZDogXCJlZGl0XCIsIHR5cGU6IFwiYnV0dG9uXCIgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb246IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGljazogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkZXZlbnQucHJldmVudERlZmF1bHQoKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIF92bS5iYWNrKCRldmVudClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwiaVwiLCB7IHN0YXRpY0NsYXNzOiBcInRpLWJhY2stbGVmdCBwLXItMTBcIiB9KSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiQmFja1xcbiAgICAgICAgICAgIFwiKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgICAgICAgICAgICAgIF0sXG4gICAgICAgICAgICAgICAgICAgICAgICAxXG4gICAgICAgICAgICAgICAgICAgICAgKSxcbiAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY29sXCIgfSlcbiAgICAgICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgIF0pXG4gICAgICAgIH0pXG4gICAgICBdLFxuICAgICAgMVxuICAgIClcbiAgXSlcbn1cbnZhciBzdGF0aWNSZW5kZXJGbnMgPSBbXG4gIGZ1bmN0aW9uKCkge1xuICAgIHZhciBfdm0gPSB0aGlzXG4gICAgdmFyIF9oID0gX3ZtLiRjcmVhdGVFbGVtZW50XG4gICAgdmFyIF9jID0gX3ZtLl9zZWxmLl9jIHx8IF9oXG4gICAgcmV0dXJuIF9jKFxuICAgICAgXCJhXCIsXG4gICAgICB7IHN0YXRpY0NsYXNzOiBcImJyZWFkY3J1bWItaXRlbVwiLCBhdHRyczogeyBocmVmOiBcIi9iYWNrZW5kXCIgfSB9LFxuICAgICAgW19jKFwiaVwiLCB7IHN0YXRpY0NsYXNzOiBcInRpLWhvbWUgcC1yLTVcIiB9KSwgX3ZtLl92KFwiSOG7hyB0aOG7kW5nXCIpXVxuICAgIClcbiAgfSxcbiAgZnVuY3Rpb24oKSB7XG4gICAgdmFyIF92bSA9IHRoaXNcbiAgICB2YXIgX2ggPSBfdm0uJGNyZWF0ZUVsZW1lbnRcbiAgICB2YXIgX2MgPSBfdm0uX3NlbGYuX2MgfHwgX2hcbiAgICByZXR1cm4gX2MoXCJzcGFuXCIsIHsgc3RhdGljQ2xhc3M6IFwiYnJlYWRjcnVtYi1pdGVtIGFjdGl2ZVwiIH0sIFtcbiAgICAgIF9jKFwiaVwiLCB7IHN0YXRpY0NsYXNzOiBcInRpLXBlbmNpbC1hbHQgcC1yLTVcIiB9KSxcbiAgICAgIF92bS5fdihcIlThuqFvIE5ow7NtXCIpXG4gICAgXSlcbiAgfVxuXVxucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5cbmV4cG9ydCB7IHJlbmRlciwgc3RhdGljUmVuZGVyRm5zIH0iLCJ2YXIgcmVuZGVyID0gZnVuY3Rpb24oKSB7XG4gIHZhciBfdm0gPSB0aGlzXG4gIHZhciBfaCA9IF92bS4kY3JlYXRlRWxlbWVudFxuICB2YXIgX2MgPSBfdm0uX3NlbGYuX2MgfHwgX2hcbiAgcmV0dXJuIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY29udGFpbmVyLWZsdWlkXCIgfSwgW1xuICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwicGFnZS1oZWFkZXJcIiB9LCBbXG4gICAgICBfYyhcImgyXCIsIHsgc3RhdGljQ2xhc3M6IFwiaGVhZGVyLXRpdGxlXCIgfSwgW192bS5fdihcIk5nxrDhu51pIGTDuW5nXCIpXSksXG4gICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJoZWFkZXItc3ViLXRpdGxlXCIgfSwgW1xuICAgICAgICBfYyhcIm5hdlwiLCB7IHN0YXRpY0NsYXNzOiBcImJyZWFkY3J1bWIgYnJlYWRjcnVtYi1kYXNoXCIgfSwgW1xuICAgICAgICAgIF92bS5fbSgwKSxcbiAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgIF9jKFxuICAgICAgICAgICAgXCJhXCIsXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImJyZWFkY3J1bWItaXRlbVwiLFxuICAgICAgICAgICAgICBhdHRyczogeyBocmVmOiBcIiNcIiB9LFxuICAgICAgICAgICAgICBvbjoge1xuICAgICAgICAgICAgICAgIGNsaWNrOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAgICRldmVudC5wcmV2ZW50RGVmYXVsdCgpXG4gICAgICAgICAgICAgICAgICByZXR1cm4gX3ZtLmdvUmVhZCgpXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgW19jKFwiaVwiLCB7IHN0YXRpY0NsYXNzOiBcInRpLXJvbGUgcC1yLTVcIiB9KSwgX3ZtLl92KFwiTmjDs21cIildXG4gICAgICAgICAgKSxcbiAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgIF92bS5fbSgxKVxuICAgICAgICBdKVxuICAgICAgXSksXG4gICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJfaGVhZGVyLWJ1dHRvblwiIH0sIFtcbiAgICAgICAgX2MoXG4gICAgICAgICAgXCJidXR0b25cIixcbiAgICAgICAgICB7XG4gICAgICAgICAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLWdyYWRpZW50LXN1Y2Nlc3NcIixcbiAgICAgICAgICAgIG9uOiB7XG4gICAgICAgICAgICAgIGNsaWNrOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAkZXZlbnQucHJldmVudERlZmF1bHQoKVxuICAgICAgICAgICAgICAgIHJldHVybiBfdm0uZ29DcmVhdGUoKVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSxcbiAgICAgICAgICBbX3ZtLl92KFwiVOG6oW8gbmjDs21cXG4gICAgICBcIildXG4gICAgICAgIClcbiAgICAgIF0pXG4gICAgXSksXG4gICAgX3ZtLl92KFwiIFwiKSxcbiAgICBfYyhcbiAgICAgIFwiZGl2XCIsXG4gICAgICB7IHN0YXRpY0NsYXNzOiBcImNhcmRcIiB9LFxuICAgICAgW1xuICAgICAgICBfYyhcInZhbGlkYXRpb24tb2JzZXJ2ZXJcIiwge1xuICAgICAgICAgIHJlZjogXCJvYnNlcnZlclwiLFxuICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImNhcmQtYm9keVwiLFxuICAgICAgICAgIGF0dHJzOiB7IHRhZzogXCJkaXZcIiB9LFxuICAgICAgICAgIHNjb3BlZFNsb3RzOiBfdm0uX3UoW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBrZXk6IFwiZGVmYXVsdFwiLFxuICAgICAgICAgICAgICBmbjogZnVuY3Rpb24ocmVmKSB7XG4gICAgICAgICAgICAgICAgdmFyIGludmFsaWQgPSByZWYuaW52YWxpZFxuICAgICAgICAgICAgICAgIHJldHVybiBbXG4gICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgXCJkaXZcIixcbiAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcInJvdyBtLXYtMzBcIixcbiAgICAgICAgICAgICAgICAgICAgICBvbjoge1xuICAgICAgICAgICAgICAgICAgICAgICAga2V5dXA6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgISRldmVudC50eXBlLmluZGV4T2YoXCJrZXlcIikgJiZcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX2soXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkZXZlbnQua2V5Q29kZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiZW50ZXJcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDEzLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJGV2ZW50LmtleSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiRW50ZXJcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG51bGxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gX3ZtLnVwZGF0ZShpbnZhbGlkKVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgW1xuICAgICAgICAgICAgICAgICAgICAgIF9jKFwiZGl2XCIsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImNvbC1zbS0zIHRleHQtY2VudGVyIHRleHQtc20tbGVmdFwiXG4gICAgICAgICAgICAgICAgICAgICAgfSksXG4gICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiZGl2XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICB7IHN0YXRpY0NsYXNzOiBcImNvbC1zbS02IHRleHQtY2VudGVyIHRleHQtc20tbGVmdFwiIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwidmFsaWRhdGlvbi1wcm92aWRlclwiLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF0dHJzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiBcIlTDqm4gbmjDs20gcXV54buBblwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcnVsZXM6IFwicmVxdWlyZWRcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRhZzogXCJkaXZcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGVkU2xvdHM6IF92bS5fdShcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGtleTogXCJkZWZhdWx0XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZm46IGZ1bmN0aW9uKHJlZikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGVycm9ycyA9IHJlZi5lcnJvcnNcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciB2YWxpZCA9IHJlZi52YWxpZFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJsYWJlbFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImNvbnRyb2wtbGFiZWxcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF0dHJzOiB7IGZvcjogXCJpY29uLWlucHV0XCIgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgW192bS5fdihcIk5hbWU6XCIpXVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImljb24taW5wdXRcIiB9LCBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJpXCIsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcIm1kaSBtZGktYWNjb3VudFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcImlucHV0XCIsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpcmVjdGl2ZXM6IFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogX3ZtLmZybURhdGEucm9sZV9uYW1lLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV4cHJlc3Npb246IFwiZnJtRGF0YS5yb2xlX25hbWVcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBlcnJvcjpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uZmxhZy5pc1ZhbGlkYXRpbmcgJiYgIXZhbGlkXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXR0cnM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogXCJ0ZXh0XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyOiBcIlJvbGUgbmFtZS4uLlwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZG9tUHJvcHM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IF92bS5mcm1EYXRhLnJvbGVfbmFtZVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlucHV0OiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoJGV2ZW50LnRhcmdldC5jb21wb3NpbmcpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVyblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uJHNldChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5mcm1EYXRhLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJyb2xlX25hbWVcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRldmVudC50YXJnZXQudmFsdWVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJsYWJlbFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpcmVjdGl2ZXM6IFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IFwic2hvd1wiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uZmxhZy5pc1ZhbGlkYXRpbmcgJiYgIXZhbGlkLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV4cHJlc3Npb246XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImZsYWcuaXNWYWxpZGF0aW5nICYmICF2YWxpZFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJlcnJvclwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbX3ZtLl92KF92bS5fcyhlcnJvcnNbMF0pKV1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG51bGwsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0cnVlXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICB9KSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjaGVja2JveFwiIH0sIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcImlucHV0XCIsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRpcmVjdGl2ZXM6IFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IFwibW9kZWxcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByYXdOYW1lOiBcInYtbW9kZWxcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogX3ZtLmZybURhdGEuaXNfZnVsbGFjY2VzcyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBleHByZXNzaW9uOiBcImZybURhdGEuaXNfZnVsbGFjY2Vzc1wiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhdHRyczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZDogXCJjaGVja2JveDFcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogXCJjaGVja2JveFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcInRydWUtdmFsdWVcIjogXCIxXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiZmFsc2UtdmFsdWVcIjogXCIwXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkb21Qcm9wczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjaGVja2VkOiBBcnJheS5pc0FycmF5KFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5mcm1EYXRhLmlzX2Z1bGxhY2Nlc3NcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgID8gX3ZtLl9pKF92bS5mcm1EYXRhLmlzX2Z1bGxhY2Nlc3MsIG51bGwpID4gLTFcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6IF92bS5fcShfdm0uZnJtRGF0YS5pc19mdWxsYWNjZXNzLCBcIjFcIilcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjaGFuZ2U6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciAkJGEgPSBfdm0uZnJtRGF0YS5pc19mdWxsYWNjZXNzLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJCRlbCA9ICRldmVudC50YXJnZXQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkJGMgPSAkJGVsLmNoZWNrZWQgPyBcIjFcIiA6IFwiMFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKEFycmF5LmlzQXJyYXkoJCRhKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyICQkdiA9IG51bGwsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICQkaSA9IF92bS5faSgkJGEsICQkdilcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICgkJGVsLmNoZWNrZWQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJCRpIDwgMCAmJlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS4kc2V0KFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLmZybURhdGEsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImlzX2Z1bGxhY2Nlc3NcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICQkYS5jb25jYXQoWyQkdl0pXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJCRpID4gLTEgJiZcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uJHNldChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5mcm1EYXRhLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJpc19mdWxsYWNjZXNzXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkJGFcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLnNsaWNlKDAsICQkaSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLmNvbmNhdCgkJGEuc2xpY2UoJCRpICsgMSkpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uJHNldChfdm0uZnJtRGF0YSwgXCJpc19mdWxsYWNjZXNzXCIsICQkYylcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwibGFiZWxcIiwgeyBhdHRyczogeyBmb3I6IFwiY2hlY2tib3gxXCIgfSB9LCBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCJLw61jaCBob+G6oXRcIilcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgICAgICAgICAgICAgICBdKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJtLXQtMjVcIiB9LCBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcImJ1dHRvblwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJidG4gYnRuLXN1Y2Nlc3NcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXR0cnM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZDogXCJzYXZlXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogXCJidXR0b25cIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZDogX3ZtLmZsYWcuaXNVcGRhdGluZ1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsaWNrOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRldmVudC5wcmV2ZW50RGVmYXVsdCgpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gX3ZtLnVwZGF0ZShpbnZhbGlkKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLmZsYWcuaXNVcGRhdGluZ1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgID8gW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcImlcIiwge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJmYSBmYS1zcGlubmVyIGZhLXB1bHNlIGZhLWZ3IG0tci01XCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF92bS5fdihcIlVwZGF0aW5nXFxuICAgICAgICAgICAgICBcIilcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6IFtfdm0uX3YoXCJVcGRhdGVcIildXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBdLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgMlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiYnV0dG9uXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tZGVmYXVsdFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhdHRyczogeyBpZDogXCJlZGl0XCIsIHR5cGU6IFwiYnV0dG9uXCIgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb246IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGljazogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkZXZlbnQucHJldmVudERlZmF1bHQoKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIF92bS5iYWNrKCRldmVudClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9jKFwiaVwiLCB7IHN0YXRpY0NsYXNzOiBcInRpLWJhY2stbGVmdCBwLXItMTBcIiB9KSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiQmFja1xcbiAgICAgICAgICAgIFwiKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgICAgICAgICAgICAgIF0sXG4gICAgICAgICAgICAgICAgICAgICAgICAxXG4gICAgICAgICAgICAgICAgICAgICAgKSxcbiAgICAgICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY29sXCIgfSlcbiAgICAgICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgIF0pXG4gICAgICAgIH0pXG4gICAgICBdLFxuICAgICAgMVxuICAgIClcbiAgXSlcbn1cbnZhciBzdGF0aWNSZW5kZXJGbnMgPSBbXG4gIGZ1bmN0aW9uKCkge1xuICAgIHZhciBfdm0gPSB0aGlzXG4gICAgdmFyIF9oID0gX3ZtLiRjcmVhdGVFbGVtZW50XG4gICAgdmFyIF9jID0gX3ZtLl9zZWxmLl9jIHx8IF9oXG4gICAgcmV0dXJuIF9jKFxuICAgICAgXCJhXCIsXG4gICAgICB7IHN0YXRpY0NsYXNzOiBcImJyZWFkY3J1bWItaXRlbVwiLCBhdHRyczogeyBocmVmOiBcIi9iYWNrZW5kXCIgfSB9LFxuICAgICAgW19jKFwiaVwiLCB7IHN0YXRpY0NsYXNzOiBcInRpLWhvbWUgcC1yLTVcIiB9KSwgX3ZtLl92KFwiSOG7hyB0aOG7kW5nXCIpXVxuICAgIClcbiAgfSxcbiAgZnVuY3Rpb24oKSB7XG4gICAgdmFyIF92bSA9IHRoaXNcbiAgICB2YXIgX2ggPSBfdm0uJGNyZWF0ZUVsZW1lbnRcbiAgICB2YXIgX2MgPSBfdm0uX3NlbGYuX2MgfHwgX2hcbiAgICByZXR1cm4gX2MoXCJzcGFuXCIsIHsgc3RhdGljQ2xhc3M6IFwiYnJlYWRjcnVtYi1pdGVtIGFjdGl2ZVwiIH0sIFtcbiAgICAgIF9jKFwiaVwiLCB7IHN0YXRpY0NsYXNzOiBcInRpLXBlbmNpbC1hbHQgcC1yLTVcIiB9KSxcbiAgICAgIF92bS5fdihcIkPhuq1wIG5o4bqtdCBOaMOzbVwiKVxuICAgIF0pXG4gIH1cbl1cbnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuXG5leHBvcnQgeyByZW5kZXIsIHN0YXRpY1JlbmRlckZucyB9IiwidmFyIHJlbmRlciA9IGZ1bmN0aW9uKCkge1xuICB2YXIgX3ZtID0gdGhpc1xuICB2YXIgX2ggPSBfdm0uJGNyZWF0ZUVsZW1lbnRcbiAgdmFyIF9jID0gX3ZtLl9zZWxmLl9jIHx8IF9oXG4gIHJldHVybiBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImNvbnRhaW5lci1mbHVpZFwiIH0sIFtcbiAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcInBhZ2UtaGVhZGVyXCIgfSwgW1xuICAgICAgX2MoXCJoMlwiLCB7IHN0YXRpY0NsYXNzOiBcImhlYWRlci10aXRsZVwiIH0sIFtfdm0uX3YoXCJOZ8aw4budaSBkw7luZ1wiKV0pLFxuICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiaGVhZGVyLXN1Yi10aXRsZVwiIH0sIFtcbiAgICAgICAgX2MoXCJuYXZcIiwgeyBzdGF0aWNDbGFzczogXCJicmVhZGNydW1iIGJyZWFkY3J1bWItZGFzaFwiIH0sIFtcbiAgICAgICAgICBfdm0uX20oMCksXG4gICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICBfYyhcbiAgICAgICAgICAgIFwiYVwiLFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJicmVhZGNydW1iLWl0ZW1cIixcbiAgICAgICAgICAgICAgYXR0cnM6IHsgaHJlZjogXCIjXCIgfSxcbiAgICAgICAgICAgICAgb246IHtcbiAgICAgICAgICAgICAgICBjbGljazogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgICAgICAgICAkZXZlbnQucHJldmVudERlZmF1bHQoKVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIFtfYyhcImlcIiwgeyBzdGF0aWNDbGFzczogXCJ0aS1yb2xlIHAtci01XCIgfSksIF92bS5fdihcIk5ow7NtXCIpXVxuICAgICAgICAgICksXG4gICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICBfdm0uX20oMSlcbiAgICAgICAgXSlcbiAgICAgIF0pLFxuICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiX2hlYWRlci1idXR0b25cIiB9LCBbXG4gICAgICAgIF9jKFxuICAgICAgICAgIFwiYnV0dG9uXCIsXG4gICAgICAgICAge1xuICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1ncmFkaWVudC1zdWNjZXNzXCIsXG4gICAgICAgICAgICBvbjoge1xuICAgICAgICAgICAgICBjbGljazogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgICAgICAgJGV2ZW50LnByZXZlbnREZWZhdWx0KClcbiAgICAgICAgICAgICAgICByZXR1cm4gX3ZtLmdvQ3JlYXRlKClcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgIH0sXG4gICAgICAgICAgW192bS5fdihcIlThuqFvIG5ow7NtXFxuICAgICAgXCIpXVxuICAgICAgICApXG4gICAgICBdKVxuICAgIF0pLFxuICAgIF92bS5fdihcIiBcIiksXG4gICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjYXJkXCIgfSwgW1xuICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjYXJkLWJvZHlcIiB9LCBbX2MoXCJyb2xlLXRhYmxlXCIpXSwgMSlcbiAgICBdKVxuICBdKVxufVxudmFyIHN0YXRpY1JlbmRlckZucyA9IFtcbiAgZnVuY3Rpb24oKSB7XG4gICAgdmFyIF92bSA9IHRoaXNcbiAgICB2YXIgX2ggPSBfdm0uJGNyZWF0ZUVsZW1lbnRcbiAgICB2YXIgX2MgPSBfdm0uX3NlbGYuX2MgfHwgX2hcbiAgICByZXR1cm4gX2MoXG4gICAgICBcImFcIixcbiAgICAgIHsgc3RhdGljQ2xhc3M6IFwiYnJlYWRjcnVtYi1pdGVtXCIsIGF0dHJzOiB7IGhyZWY6IFwiL2JhY2tlbmRcIiB9IH0sXG4gICAgICBbX2MoXCJpXCIsIHsgc3RhdGljQ2xhc3M6IFwidGktaG9tZSBwLXItNVwiIH0pLCBfdm0uX3YoXCJI4buHIHRo4buRbmdcIildXG4gICAgKVxuICB9LFxuICBmdW5jdGlvbigpIHtcbiAgICB2YXIgX3ZtID0gdGhpc1xuICAgIHZhciBfaCA9IF92bS4kY3JlYXRlRWxlbWVudFxuICAgIHZhciBfYyA9IF92bS5fc2VsZi5fYyB8fCBfaFxuICAgIHJldHVybiBfYyhcInNwYW5cIiwgeyBzdGF0aWNDbGFzczogXCJicmVhZGNydW1iLWl0ZW0gYWN0aXZlXCIgfSwgW1xuICAgICAgX2MoXCJpXCIsIHsgc3RhdGljQ2xhc3M6IFwidGktbGlzdCBwLXItNVwiIH0pLFxuICAgICAgX3ZtLl92KFwiRGFuaCBzw6FjaFwiKVxuICAgIF0pXG4gIH1cbl1cbnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuXG5leHBvcnQgeyByZW5kZXIsIHN0YXRpY1JlbmRlckZucyB9IiwidmFyIHJlbmRlciA9IGZ1bmN0aW9uKCkge1xuICB2YXIgX3ZtID0gdGhpc1xuICB2YXIgX2ggPSBfdm0uJGNyZWF0ZUVsZW1lbnRcbiAgdmFyIF9jID0gX3ZtLl9zZWxmLl9jIHx8IF9oXG4gIHJldHVybiBfYyhcbiAgICBcImRpdlwiLFxuICAgIHsgc3RhdGljQ2xhc3M6IFwidGFibGUtb3ZlcmZsb3dcIiwgYXR0cnM6IHsgaWQ6IFwicm9sZS10YWJsZVwiIH0gfSxcbiAgICBbXG4gICAgICBfYyhcbiAgICAgICAgXCJ0YWJsZVwiLFxuICAgICAgICB7IHN0YXRpY0NsYXNzOiBcInRhYmxlIHRhYmxlLWhvdmVyIHRhYmxlLXhsXCIsIGF0dHJzOiB7IGlkOiBcImR0LW9wdFwiIH0gfSxcbiAgICAgICAgW1xuICAgICAgICAgIF92bS5fbSgwKSxcbiAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgIF9jKFxuICAgICAgICAgICAgXCJ0Ym9keVwiLFxuICAgICAgICAgICAgW1xuICAgICAgICAgICAgICBfdm0uX2woX3ZtLmFyckVudGl0eUxpc3QsIGZ1bmN0aW9uKG9iakVudGl0eSwgaW5kZXgpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gW1xuICAgICAgICAgICAgICAgICAgX2MoXCJyb2xlLXRhYmxlLXJvd1wiLCB7XG4gICAgICAgICAgICAgICAgICAgIGF0dHJzOiB7IGluZGV4OiBpbmRleCwgXCJvYmotZW50aXR5XCI6IG9iakVudGl0eSB9XG4gICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICAyXG4gICAgICAgICAgKVxuICAgICAgICBdXG4gICAgICApXG4gICAgXVxuICApXG59XG52YXIgc3RhdGljUmVuZGVyRm5zID0gW1xuICBmdW5jdGlvbigpIHtcbiAgICB2YXIgX3ZtID0gdGhpc1xuICAgIHZhciBfaCA9IF92bS4kY3JlYXRlRWxlbWVudFxuICAgIHZhciBfYyA9IF92bS5fc2VsZi5fYyB8fCBfaFxuICAgIHJldHVybiBfYyhcInRoZWFkXCIsIFtcbiAgICAgIF9jKFwidHJcIiwgW1xuICAgICAgICBfYyhcInRoXCIsIFtcbiAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImNoZWNrYm94IHAtMFwiIH0sIFtcbiAgICAgICAgICAgIF9jKFwiaW5wdXRcIiwge1xuICAgICAgICAgICAgICBzdGF0aWNDbGFzczogXCJjaGVja0FsbFwiLFxuICAgICAgICAgICAgICBhdHRyczogeyBpZDogXCJzZWxlY3RhYmxlMVwiLCB0eXBlOiBcImNoZWNrYm94XCIsIG5hbWU6IFwiY2hlY2tBbGxcIiB9XG4gICAgICAgICAgICB9KSxcbiAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICBfYyhcImxhYmVsXCIsIHsgYXR0cnM6IHsgZm9yOiBcInNlbGVjdGFibGUxXCIgfSB9KVxuICAgICAgICAgIF0pXG4gICAgICAgIF0pLFxuICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICBfYyhcInRoXCIsIFtfdm0uX3YoXCJST0xFXCIpXSksXG4gICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgIF9jKFwidGhcIiwgW192bS5fdihcIlNUQVRVU1wiKV0pLFxuICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICBfYyhcInRoXCIsIFtfdm0uX3YoXCIjXCIpXSksXG4gICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgIF9jKFwidGhcIilcbiAgICAgIF0pXG4gICAgXSlcbiAgfVxuXVxucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5cbmV4cG9ydCB7IHJlbmRlciwgc3RhdGljUmVuZGVyRm5zIH0iLCJ2YXIgcmVuZGVyID0gZnVuY3Rpb24oKSB7XG4gIHZhciBfdm0gPSB0aGlzXG4gIHZhciBfaCA9IF92bS4kY3JlYXRlRWxlbWVudFxuICB2YXIgX2MgPSBfdm0uX3NlbGYuX2MgfHwgX2hcbiAgcmV0dXJuIF9jKFwidHJcIiwgW1xuICAgIF92bS5fbSgwKSxcbiAgICBfdm0uX3YoXCIgXCIpLFxuICAgIF9jKFwidGRcIiwgW192bS5fdihfdm0uX3MoX3ZtLm9iakVudGl0eS5yb2xlX25hbWUpKV0pLFxuICAgIF92bS5fdihcIiBcIiksXG4gICAgX2MoXG4gICAgICBcInRkXCIsXG4gICAgICBbXG4gICAgICAgIF92bS5vYmpFbnRpdHkuaXNfZnVsbGFjY2VzcyA9PSAxXG4gICAgICAgICAgPyBbXG4gICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgIFwic3BhblwiLFxuICAgICAgICAgICAgICAgIHsgc3RhdGljQ2xhc3M6IFwiYmFkZ2UgYmFkZ2UtcGlsbCBiYWRnZS1ncmFkaWVudC1zdWNjZXNzXCIgfSxcbiAgICAgICAgICAgICAgICBbX3ZtLl92KFwixJDhuqd5IMSR4bunIHF1eeG7gW4gaOG6oW5cIildXG4gICAgICAgICAgICAgIClcbiAgICAgICAgICAgIF1cbiAgICAgICAgICA6IFtcbiAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgXCJzcGFuXCIsXG4gICAgICAgICAgICAgICAgeyBzdGF0aWNDbGFzczogXCJiYWRnZSBiYWRnZS1waWxsIGJhZGdlLWdyYWRpZW50LWRhbmdlclwiIH0sXG4gICAgICAgICAgICAgICAgW192bS5fdihcIkNoxrBhIMSR4bqneSDEkeG7pyBxdXnhu4FuIGjhuqFuXCIpXVxuICAgICAgICAgICAgICApXG4gICAgICAgICAgICBdXG4gICAgICBdLFxuICAgICAgMlxuICAgICksXG4gICAgX3ZtLl92KFwiIFwiKSxcbiAgICBfYyhcInRkXCIsIHsgc3RhdGljQ2xhc3M6IFwidGV4dC1jZW50ZXIgZm9udC1zaXplLTE4XCIgfSwgW1xuICAgICAgX2MoXG4gICAgICAgIFwiYVwiLFxuICAgICAgICB7XG4gICAgICAgICAgc3RhdGljQ2xhc3M6IFwidGV4dC1ncmF5IG0tci0xNVwiLFxuICAgICAgICAgIG9uOiB7XG4gICAgICAgICAgICBjbGljazogZnVuY3Rpb24oJGV2ZW50KSB7XG4gICAgICAgICAgICAgIHJldHVybiBfdm0uZ29VcGRhdGUoKVxuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgW19jKFwiaVwiLCB7IHN0YXRpY0NsYXNzOiBcInRpLXBlbmNpbFwiIH0pXVxuICAgICAgKSxcbiAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICBfYyhcbiAgICAgICAgXCJhXCIsXG4gICAgICAgIHtcbiAgICAgICAgICBzdGF0aWNDbGFzczogXCJ0ZXh0LWdyYXlcIixcbiAgICAgICAgICBvbjoge1xuICAgICAgICAgICAgY2xpY2s6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgICAgICByZXR1cm4gX3ZtLnJlbW92ZShfdm0ub2JqRW50aXR5LnJvbGVfaWQpXG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBbXG4gICAgICAgICAgX2MoXCJpXCIsIHtcbiAgICAgICAgICAgIGNsYXNzOiB7XG4gICAgICAgICAgICAgIFwiZmEgZmEtc3Bpbm5lciBmYS1wdWxzZSBmYS1md1wiOiBfdm0uZmxhZy5pc0RlbGV0aW5nLFxuICAgICAgICAgICAgICBcInRpLXRyYXNoXCI6ICFfdm0uZmxhZy5pc0RlbGV0aW5nXG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSlcbiAgICAgICAgXVxuICAgICAgKVxuICAgIF0pXG4gIF0pXG59XG52YXIgc3RhdGljUmVuZGVyRm5zID0gW1xuICBmdW5jdGlvbigpIHtcbiAgICB2YXIgX3ZtID0gdGhpc1xuICAgIHZhciBfaCA9IF92bS4kY3JlYXRlRWxlbWVudFxuICAgIHZhciBfYyA9IF92bS5fc2VsZi5fYyB8fCBfaFxuICAgIHJldHVybiBfYyhcInRkXCIsIFtcbiAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY2hlY2tib3hcIiB9LCBbXG4gICAgICAgIF9jKFwiaW5wdXRcIiwgeyBhdHRyczogeyBpZDogXCJzZWxlY3RhYmxlMlwiLCB0eXBlOiBcImNoZWNrYm94XCIgfSB9KSxcbiAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgX2MoXCJsYWJlbFwiLCB7IGF0dHJzOiB7IGZvcjogXCJzZWxlY3RhYmxlMlwiIH0gfSlcbiAgICAgIF0pXG4gICAgXSlcbiAgfVxuXVxucmVuZGVyLl93aXRoU3RyaXBwZWQgPSB0cnVlXG5cbmV4cG9ydCB7IHJlbmRlciwgc3RhdGljUmVuZGVyRm5zIH0iLCIvKiFcbiAgKiB2dWUtcm91dGVyIHYzLjEuM1xuICAqIChjKSAyMDE5IEV2YW4gWW91XG4gICogQGxpY2Vuc2UgTUlUXG4gICovXG4vKiAgKi9cblxuZnVuY3Rpb24gYXNzZXJ0IChjb25kaXRpb24sIG1lc3NhZ2UpIHtcbiAgaWYgKCFjb25kaXRpb24pIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoKFwiW3Z1ZS1yb3V0ZXJdIFwiICsgbWVzc2FnZSkpXG4gIH1cbn1cblxuZnVuY3Rpb24gd2FybiAoY29uZGl0aW9uLCBtZXNzYWdlKSB7XG4gIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nICYmICFjb25kaXRpb24pIHtcbiAgICB0eXBlb2YgY29uc29sZSAhPT0gJ3VuZGVmaW5lZCcgJiYgY29uc29sZS53YXJuKChcIlt2dWUtcm91dGVyXSBcIiArIG1lc3NhZ2UpKTtcbiAgfVxufVxuXG5mdW5jdGlvbiBpc0Vycm9yIChlcnIpIHtcbiAgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcuY2FsbChlcnIpLmluZGV4T2YoJ0Vycm9yJykgPiAtMVxufVxuXG5mdW5jdGlvbiBpc0V4dGVuZGVkRXJyb3IgKGNvbnN0cnVjdG9yLCBlcnIpIHtcbiAgcmV0dXJuIChcbiAgICBlcnIgaW5zdGFuY2VvZiBjb25zdHJ1Y3RvciB8fFxuICAgIC8vIF9uYW1lIGlzIHRvIHN1cHBvcnQgSUU5IHRvb1xuICAgIChlcnIgJiYgKGVyci5uYW1lID09PSBjb25zdHJ1Y3Rvci5uYW1lIHx8IGVyci5fbmFtZSA9PT0gY29uc3RydWN0b3IuX25hbWUpKVxuICApXG59XG5cbmZ1bmN0aW9uIGV4dGVuZCAoYSwgYikge1xuICBmb3IgKHZhciBrZXkgaW4gYikge1xuICAgIGFba2V5XSA9IGJba2V5XTtcbiAgfVxuICByZXR1cm4gYVxufVxuXG52YXIgVmlldyA9IHtcbiAgbmFtZTogJ1JvdXRlclZpZXcnLFxuICBmdW5jdGlvbmFsOiB0cnVlLFxuICBwcm9wczoge1xuICAgIG5hbWU6IHtcbiAgICAgIHR5cGU6IFN0cmluZyxcbiAgICAgIGRlZmF1bHQ6ICdkZWZhdWx0J1xuICAgIH1cbiAgfSxcbiAgcmVuZGVyOiBmdW5jdGlvbiByZW5kZXIgKF8sIHJlZikge1xuICAgIHZhciBwcm9wcyA9IHJlZi5wcm9wcztcbiAgICB2YXIgY2hpbGRyZW4gPSByZWYuY2hpbGRyZW47XG4gICAgdmFyIHBhcmVudCA9IHJlZi5wYXJlbnQ7XG4gICAgdmFyIGRhdGEgPSByZWYuZGF0YTtcblxuICAgIC8vIHVzZWQgYnkgZGV2dG9vbHMgdG8gZGlzcGxheSBhIHJvdXRlci12aWV3IGJhZGdlXG4gICAgZGF0YS5yb3V0ZXJWaWV3ID0gdHJ1ZTtcblxuICAgIC8vIGRpcmVjdGx5IHVzZSBwYXJlbnQgY29udGV4dCdzIGNyZWF0ZUVsZW1lbnQoKSBmdW5jdGlvblxuICAgIC8vIHNvIHRoYXQgY29tcG9uZW50cyByZW5kZXJlZCBieSByb3V0ZXItdmlldyBjYW4gcmVzb2x2ZSBuYW1lZCBzbG90c1xuICAgIHZhciBoID0gcGFyZW50LiRjcmVhdGVFbGVtZW50O1xuICAgIHZhciBuYW1lID0gcHJvcHMubmFtZTtcbiAgICB2YXIgcm91dGUgPSBwYXJlbnQuJHJvdXRlO1xuICAgIHZhciBjYWNoZSA9IHBhcmVudC5fcm91dGVyVmlld0NhY2hlIHx8IChwYXJlbnQuX3JvdXRlclZpZXdDYWNoZSA9IHt9KTtcblxuICAgIC8vIGRldGVybWluZSBjdXJyZW50IHZpZXcgZGVwdGgsIGFsc28gY2hlY2sgdG8gc2VlIGlmIHRoZSB0cmVlXG4gICAgLy8gaGFzIGJlZW4gdG9nZ2xlZCBpbmFjdGl2ZSBidXQga2VwdC1hbGl2ZS5cbiAgICB2YXIgZGVwdGggPSAwO1xuICAgIHZhciBpbmFjdGl2ZSA9IGZhbHNlO1xuICAgIHdoaWxlIChwYXJlbnQgJiYgcGFyZW50Ll9yb3V0ZXJSb290ICE9PSBwYXJlbnQpIHtcbiAgICAgIHZhciB2bm9kZURhdGEgPSBwYXJlbnQuJHZub2RlICYmIHBhcmVudC4kdm5vZGUuZGF0YTtcbiAgICAgIGlmICh2bm9kZURhdGEpIHtcbiAgICAgICAgaWYgKHZub2RlRGF0YS5yb3V0ZXJWaWV3KSB7XG4gICAgICAgICAgZGVwdGgrKztcbiAgICAgICAgfVxuICAgICAgICBpZiAodm5vZGVEYXRhLmtlZXBBbGl2ZSAmJiBwYXJlbnQuX2luYWN0aXZlKSB7XG4gICAgICAgICAgaW5hY3RpdmUgPSB0cnVlO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgICBwYXJlbnQgPSBwYXJlbnQuJHBhcmVudDtcbiAgICB9XG4gICAgZGF0YS5yb3V0ZXJWaWV3RGVwdGggPSBkZXB0aDtcblxuICAgIC8vIHJlbmRlciBwcmV2aW91cyB2aWV3IGlmIHRoZSB0cmVlIGlzIGluYWN0aXZlIGFuZCBrZXB0LWFsaXZlXG4gICAgaWYgKGluYWN0aXZlKSB7XG4gICAgICByZXR1cm4gaChjYWNoZVtuYW1lXSwgZGF0YSwgY2hpbGRyZW4pXG4gICAgfVxuXG4gICAgdmFyIG1hdGNoZWQgPSByb3V0ZS5tYXRjaGVkW2RlcHRoXTtcbiAgICAvLyByZW5kZXIgZW1wdHkgbm9kZSBpZiBubyBtYXRjaGVkIHJvdXRlXG4gICAgaWYgKCFtYXRjaGVkKSB7XG4gICAgICBjYWNoZVtuYW1lXSA9IG51bGw7XG4gICAgICByZXR1cm4gaCgpXG4gICAgfVxuXG4gICAgdmFyIGNvbXBvbmVudCA9IGNhY2hlW25hbWVdID0gbWF0Y2hlZC5jb21wb25lbnRzW25hbWVdO1xuXG4gICAgLy8gYXR0YWNoIGluc3RhbmNlIHJlZ2lzdHJhdGlvbiBob29rXG4gICAgLy8gdGhpcyB3aWxsIGJlIGNhbGxlZCBpbiB0aGUgaW5zdGFuY2UncyBpbmplY3RlZCBsaWZlY3ljbGUgaG9va3NcbiAgICBkYXRhLnJlZ2lzdGVyUm91dGVJbnN0YW5jZSA9IGZ1bmN0aW9uICh2bSwgdmFsKSB7XG4gICAgICAvLyB2YWwgY291bGQgYmUgdW5kZWZpbmVkIGZvciB1bnJlZ2lzdHJhdGlvblxuICAgICAgdmFyIGN1cnJlbnQgPSBtYXRjaGVkLmluc3RhbmNlc1tuYW1lXTtcbiAgICAgIGlmIChcbiAgICAgICAgKHZhbCAmJiBjdXJyZW50ICE9PSB2bSkgfHxcbiAgICAgICAgKCF2YWwgJiYgY3VycmVudCA9PT0gdm0pXG4gICAgICApIHtcbiAgICAgICAgbWF0Y2hlZC5pbnN0YW5jZXNbbmFtZV0gPSB2YWw7XG4gICAgICB9XG4gICAgfVxuXG4gICAgLy8gYWxzbyByZWdpc3RlciBpbnN0YW5jZSBpbiBwcmVwYXRjaCBob29rXG4gICAgLy8gaW4gY2FzZSB0aGUgc2FtZSBjb21wb25lbnQgaW5zdGFuY2UgaXMgcmV1c2VkIGFjcm9zcyBkaWZmZXJlbnQgcm91dGVzXG4gICAgOyhkYXRhLmhvb2sgfHwgKGRhdGEuaG9vayA9IHt9KSkucHJlcGF0Y2ggPSBmdW5jdGlvbiAoXywgdm5vZGUpIHtcbiAgICAgIG1hdGNoZWQuaW5zdGFuY2VzW25hbWVdID0gdm5vZGUuY29tcG9uZW50SW5zdGFuY2U7XG4gICAgfTtcblxuICAgIC8vIHJlZ2lzdGVyIGluc3RhbmNlIGluIGluaXQgaG9va1xuICAgIC8vIGluIGNhc2Uga2VwdC1hbGl2ZSBjb21wb25lbnQgYmUgYWN0aXZlZCB3aGVuIHJvdXRlcyBjaGFuZ2VkXG4gICAgZGF0YS5ob29rLmluaXQgPSBmdW5jdGlvbiAodm5vZGUpIHtcbiAgICAgIGlmICh2bm9kZS5kYXRhLmtlZXBBbGl2ZSAmJlxuICAgICAgICB2bm9kZS5jb21wb25lbnRJbnN0YW5jZSAmJlxuICAgICAgICB2bm9kZS5jb21wb25lbnRJbnN0YW5jZSAhPT0gbWF0Y2hlZC5pbnN0YW5jZXNbbmFtZV1cbiAgICAgICkge1xuICAgICAgICBtYXRjaGVkLmluc3RhbmNlc1tuYW1lXSA9IHZub2RlLmNvbXBvbmVudEluc3RhbmNlO1xuICAgICAgfVxuICAgIH07XG5cbiAgICAvLyByZXNvbHZlIHByb3BzXG4gICAgdmFyIHByb3BzVG9QYXNzID0gZGF0YS5wcm9wcyA9IHJlc29sdmVQcm9wcyhyb3V0ZSwgbWF0Y2hlZC5wcm9wcyAmJiBtYXRjaGVkLnByb3BzW25hbWVdKTtcbiAgICBpZiAocHJvcHNUb1Bhc3MpIHtcbiAgICAgIC8vIGNsb25lIHRvIHByZXZlbnQgbXV0YXRpb25cbiAgICAgIHByb3BzVG9QYXNzID0gZGF0YS5wcm9wcyA9IGV4dGVuZCh7fSwgcHJvcHNUb1Bhc3MpO1xuICAgICAgLy8gcGFzcyBub24tZGVjbGFyZWQgcHJvcHMgYXMgYXR0cnNcbiAgICAgIHZhciBhdHRycyA9IGRhdGEuYXR0cnMgPSBkYXRhLmF0dHJzIHx8IHt9O1xuICAgICAgZm9yICh2YXIga2V5IGluIHByb3BzVG9QYXNzKSB7XG4gICAgICAgIGlmICghY29tcG9uZW50LnByb3BzIHx8ICEoa2V5IGluIGNvbXBvbmVudC5wcm9wcykpIHtcbiAgICAgICAgICBhdHRyc1trZXldID0gcHJvcHNUb1Bhc3Nba2V5XTtcbiAgICAgICAgICBkZWxldGUgcHJvcHNUb1Bhc3Nba2V5XTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiBoKGNvbXBvbmVudCwgZGF0YSwgY2hpbGRyZW4pXG4gIH1cbn07XG5cbmZ1bmN0aW9uIHJlc29sdmVQcm9wcyAocm91dGUsIGNvbmZpZykge1xuICBzd2l0Y2ggKHR5cGVvZiBjb25maWcpIHtcbiAgICBjYXNlICd1bmRlZmluZWQnOlxuICAgICAgcmV0dXJuXG4gICAgY2FzZSAnb2JqZWN0JzpcbiAgICAgIHJldHVybiBjb25maWdcbiAgICBjYXNlICdmdW5jdGlvbic6XG4gICAgICByZXR1cm4gY29uZmlnKHJvdXRlKVxuICAgIGNhc2UgJ2Jvb2xlYW4nOlxuICAgICAgcmV0dXJuIGNvbmZpZyA/IHJvdXRlLnBhcmFtcyA6IHVuZGVmaW5lZFxuICAgIGRlZmF1bHQ6XG4gICAgICBpZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJykge1xuICAgICAgICB3YXJuKFxuICAgICAgICAgIGZhbHNlLFxuICAgICAgICAgIFwicHJvcHMgaW4gXFxcIlwiICsgKHJvdXRlLnBhdGgpICsgXCJcXFwiIGlzIGEgXCIgKyAodHlwZW9mIGNvbmZpZykgKyBcIiwgXCIgK1xuICAgICAgICAgIFwiZXhwZWN0aW5nIGFuIG9iamVjdCwgZnVuY3Rpb24gb3IgYm9vbGVhbi5cIlxuICAgICAgICApO1xuICAgICAgfVxuICB9XG59XG5cbi8qICAqL1xuXG52YXIgZW5jb2RlUmVzZXJ2ZVJFID0gL1shJygpKl0vZztcbnZhciBlbmNvZGVSZXNlcnZlUmVwbGFjZXIgPSBmdW5jdGlvbiAoYykgeyByZXR1cm4gJyUnICsgYy5jaGFyQ29kZUF0KDApLnRvU3RyaW5nKDE2KTsgfTtcbnZhciBjb21tYVJFID0gLyUyQy9nO1xuXG4vLyBmaXhlZCBlbmNvZGVVUklDb21wb25lbnQgd2hpY2ggaXMgbW9yZSBjb25mb3JtYW50IHRvIFJGQzM5ODY6XG4vLyAtIGVzY2FwZXMgWyEnKCkqXVxuLy8gLSBwcmVzZXJ2ZSBjb21tYXNcbnZhciBlbmNvZGUgPSBmdW5jdGlvbiAoc3RyKSB7IHJldHVybiBlbmNvZGVVUklDb21wb25lbnQoc3RyKVxuICAucmVwbGFjZShlbmNvZGVSZXNlcnZlUkUsIGVuY29kZVJlc2VydmVSZXBsYWNlcilcbiAgLnJlcGxhY2UoY29tbWFSRSwgJywnKTsgfTtcblxudmFyIGRlY29kZSA9IGRlY29kZVVSSUNvbXBvbmVudDtcblxuZnVuY3Rpb24gcmVzb2x2ZVF1ZXJ5IChcbiAgcXVlcnksXG4gIGV4dHJhUXVlcnksXG4gIF9wYXJzZVF1ZXJ5XG4pIHtcbiAgaWYgKCBleHRyYVF1ZXJ5ID09PSB2b2lkIDAgKSBleHRyYVF1ZXJ5ID0ge307XG5cbiAgdmFyIHBhcnNlID0gX3BhcnNlUXVlcnkgfHwgcGFyc2VRdWVyeTtcbiAgdmFyIHBhcnNlZFF1ZXJ5O1xuICB0cnkge1xuICAgIHBhcnNlZFF1ZXJ5ID0gcGFyc2UocXVlcnkgfHwgJycpO1xuICB9IGNhdGNoIChlKSB7XG4gICAgcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJyAmJiB3YXJuKGZhbHNlLCBlLm1lc3NhZ2UpO1xuICAgIHBhcnNlZFF1ZXJ5ID0ge307XG4gIH1cbiAgZm9yICh2YXIga2V5IGluIGV4dHJhUXVlcnkpIHtcbiAgICBwYXJzZWRRdWVyeVtrZXldID0gZXh0cmFRdWVyeVtrZXldO1xuICB9XG4gIHJldHVybiBwYXJzZWRRdWVyeVxufVxuXG5mdW5jdGlvbiBwYXJzZVF1ZXJ5IChxdWVyeSkge1xuICB2YXIgcmVzID0ge307XG5cbiAgcXVlcnkgPSBxdWVyeS50cmltKCkucmVwbGFjZSgvXihcXD98I3wmKS8sICcnKTtcblxuICBpZiAoIXF1ZXJ5KSB7XG4gICAgcmV0dXJuIHJlc1xuICB9XG5cbiAgcXVlcnkuc3BsaXQoJyYnKS5mb3JFYWNoKGZ1bmN0aW9uIChwYXJhbSkge1xuICAgIHZhciBwYXJ0cyA9IHBhcmFtLnJlcGxhY2UoL1xcKy9nLCAnICcpLnNwbGl0KCc9Jyk7XG4gICAgdmFyIGtleSA9IGRlY29kZShwYXJ0cy5zaGlmdCgpKTtcbiAgICB2YXIgdmFsID0gcGFydHMubGVuZ3RoID4gMFxuICAgICAgPyBkZWNvZGUocGFydHMuam9pbignPScpKVxuICAgICAgOiBudWxsO1xuXG4gICAgaWYgKHJlc1trZXldID09PSB1bmRlZmluZWQpIHtcbiAgICAgIHJlc1trZXldID0gdmFsO1xuICAgIH0gZWxzZSBpZiAoQXJyYXkuaXNBcnJheShyZXNba2V5XSkpIHtcbiAgICAgIHJlc1trZXldLnB1c2godmFsKTtcbiAgICB9IGVsc2Uge1xuICAgICAgcmVzW2tleV0gPSBbcmVzW2tleV0sIHZhbF07XG4gICAgfVxuICB9KTtcblxuICByZXR1cm4gcmVzXG59XG5cbmZ1bmN0aW9uIHN0cmluZ2lmeVF1ZXJ5IChvYmopIHtcbiAgdmFyIHJlcyA9IG9iaiA/IE9iamVjdC5rZXlzKG9iaikubWFwKGZ1bmN0aW9uIChrZXkpIHtcbiAgICB2YXIgdmFsID0gb2JqW2tleV07XG5cbiAgICBpZiAodmFsID09PSB1bmRlZmluZWQpIHtcbiAgICAgIHJldHVybiAnJ1xuICAgIH1cblxuICAgIGlmICh2YWwgPT09IG51bGwpIHtcbiAgICAgIHJldHVybiBlbmNvZGUoa2V5KVxuICAgIH1cblxuICAgIGlmIChBcnJheS5pc0FycmF5KHZhbCkpIHtcbiAgICAgIHZhciByZXN1bHQgPSBbXTtcbiAgICAgIHZhbC5mb3JFYWNoKGZ1bmN0aW9uICh2YWwyKSB7XG4gICAgICAgIGlmICh2YWwyID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICByZXR1cm5cbiAgICAgICAgfVxuICAgICAgICBpZiAodmFsMiA9PT0gbnVsbCkge1xuICAgICAgICAgIHJlc3VsdC5wdXNoKGVuY29kZShrZXkpKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICByZXN1bHQucHVzaChlbmNvZGUoa2V5KSArICc9JyArIGVuY29kZSh2YWwyKSk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgICAgcmV0dXJuIHJlc3VsdC5qb2luKCcmJylcbiAgICB9XG5cbiAgICByZXR1cm4gZW5jb2RlKGtleSkgKyAnPScgKyBlbmNvZGUodmFsKVxuICB9KS5maWx0ZXIoZnVuY3Rpb24gKHgpIHsgcmV0dXJuIHgubGVuZ3RoID4gMDsgfSkuam9pbignJicpIDogbnVsbDtcbiAgcmV0dXJuIHJlcyA/IChcIj9cIiArIHJlcykgOiAnJ1xufVxuXG4vKiAgKi9cblxudmFyIHRyYWlsaW5nU2xhc2hSRSA9IC9cXC8/JC87XG5cbmZ1bmN0aW9uIGNyZWF0ZVJvdXRlIChcbiAgcmVjb3JkLFxuICBsb2NhdGlvbixcbiAgcmVkaXJlY3RlZEZyb20sXG4gIHJvdXRlclxuKSB7XG4gIHZhciBzdHJpbmdpZnlRdWVyeSA9IHJvdXRlciAmJiByb3V0ZXIub3B0aW9ucy5zdHJpbmdpZnlRdWVyeTtcblxuICB2YXIgcXVlcnkgPSBsb2NhdGlvbi5xdWVyeSB8fCB7fTtcbiAgdHJ5IHtcbiAgICBxdWVyeSA9IGNsb25lKHF1ZXJ5KTtcbiAgfSBjYXRjaCAoZSkge31cblxuICB2YXIgcm91dGUgPSB7XG4gICAgbmFtZTogbG9jYXRpb24ubmFtZSB8fCAocmVjb3JkICYmIHJlY29yZC5uYW1lKSxcbiAgICBtZXRhOiAocmVjb3JkICYmIHJlY29yZC5tZXRhKSB8fCB7fSxcbiAgICBwYXRoOiBsb2NhdGlvbi5wYXRoIHx8ICcvJyxcbiAgICBoYXNoOiBsb2NhdGlvbi5oYXNoIHx8ICcnLFxuICAgIHF1ZXJ5OiBxdWVyeSxcbiAgICBwYXJhbXM6IGxvY2F0aW9uLnBhcmFtcyB8fCB7fSxcbiAgICBmdWxsUGF0aDogZ2V0RnVsbFBhdGgobG9jYXRpb24sIHN0cmluZ2lmeVF1ZXJ5KSxcbiAgICBtYXRjaGVkOiByZWNvcmQgPyBmb3JtYXRNYXRjaChyZWNvcmQpIDogW11cbiAgfTtcbiAgaWYgKHJlZGlyZWN0ZWRGcm9tKSB7XG4gICAgcm91dGUucmVkaXJlY3RlZEZyb20gPSBnZXRGdWxsUGF0aChyZWRpcmVjdGVkRnJvbSwgc3RyaW5naWZ5UXVlcnkpO1xuICB9XG4gIHJldHVybiBPYmplY3QuZnJlZXplKHJvdXRlKVxufVxuXG5mdW5jdGlvbiBjbG9uZSAodmFsdWUpIHtcbiAgaWYgKEFycmF5LmlzQXJyYXkodmFsdWUpKSB7XG4gICAgcmV0dXJuIHZhbHVlLm1hcChjbG9uZSlcbiAgfSBlbHNlIGlmICh2YWx1ZSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnKSB7XG4gICAgdmFyIHJlcyA9IHt9O1xuICAgIGZvciAodmFyIGtleSBpbiB2YWx1ZSkge1xuICAgICAgcmVzW2tleV0gPSBjbG9uZSh2YWx1ZVtrZXldKTtcbiAgICB9XG4gICAgcmV0dXJuIHJlc1xuICB9IGVsc2Uge1xuICAgIHJldHVybiB2YWx1ZVxuICB9XG59XG5cbi8vIHRoZSBzdGFydGluZyByb3V0ZSB0aGF0IHJlcHJlc2VudHMgdGhlIGluaXRpYWwgc3RhdGVcbnZhciBTVEFSVCA9IGNyZWF0ZVJvdXRlKG51bGwsIHtcbiAgcGF0aDogJy8nXG59KTtcblxuZnVuY3Rpb24gZm9ybWF0TWF0Y2ggKHJlY29yZCkge1xuICB2YXIgcmVzID0gW107XG4gIHdoaWxlIChyZWNvcmQpIHtcbiAgICByZXMudW5zaGlmdChyZWNvcmQpO1xuICAgIHJlY29yZCA9IHJlY29yZC5wYXJlbnQ7XG4gIH1cbiAgcmV0dXJuIHJlc1xufVxuXG5mdW5jdGlvbiBnZXRGdWxsUGF0aCAoXG4gIHJlZixcbiAgX3N0cmluZ2lmeVF1ZXJ5XG4pIHtcbiAgdmFyIHBhdGggPSByZWYucGF0aDtcbiAgdmFyIHF1ZXJ5ID0gcmVmLnF1ZXJ5OyBpZiAoIHF1ZXJ5ID09PSB2b2lkIDAgKSBxdWVyeSA9IHt9O1xuICB2YXIgaGFzaCA9IHJlZi5oYXNoOyBpZiAoIGhhc2ggPT09IHZvaWQgMCApIGhhc2ggPSAnJztcblxuICB2YXIgc3RyaW5naWZ5ID0gX3N0cmluZ2lmeVF1ZXJ5IHx8IHN0cmluZ2lmeVF1ZXJ5O1xuICByZXR1cm4gKHBhdGggfHwgJy8nKSArIHN0cmluZ2lmeShxdWVyeSkgKyBoYXNoXG59XG5cbmZ1bmN0aW9uIGlzU2FtZVJvdXRlIChhLCBiKSB7XG4gIGlmIChiID09PSBTVEFSVCkge1xuICAgIHJldHVybiBhID09PSBiXG4gIH0gZWxzZSBpZiAoIWIpIHtcbiAgICByZXR1cm4gZmFsc2VcbiAgfSBlbHNlIGlmIChhLnBhdGggJiYgYi5wYXRoKSB7XG4gICAgcmV0dXJuIChcbiAgICAgIGEucGF0aC5yZXBsYWNlKHRyYWlsaW5nU2xhc2hSRSwgJycpID09PSBiLnBhdGgucmVwbGFjZSh0cmFpbGluZ1NsYXNoUkUsICcnKSAmJlxuICAgICAgYS5oYXNoID09PSBiLmhhc2ggJiZcbiAgICAgIGlzT2JqZWN0RXF1YWwoYS5xdWVyeSwgYi5xdWVyeSlcbiAgICApXG4gIH0gZWxzZSBpZiAoYS5uYW1lICYmIGIubmFtZSkge1xuICAgIHJldHVybiAoXG4gICAgICBhLm5hbWUgPT09IGIubmFtZSAmJlxuICAgICAgYS5oYXNoID09PSBiLmhhc2ggJiZcbiAgICAgIGlzT2JqZWN0RXF1YWwoYS5xdWVyeSwgYi5xdWVyeSkgJiZcbiAgICAgIGlzT2JqZWN0RXF1YWwoYS5wYXJhbXMsIGIucGFyYW1zKVxuICAgIClcbiAgfSBlbHNlIHtcbiAgICByZXR1cm4gZmFsc2VcbiAgfVxufVxuXG5mdW5jdGlvbiBpc09iamVjdEVxdWFsIChhLCBiKSB7XG4gIGlmICggYSA9PT0gdm9pZCAwICkgYSA9IHt9O1xuICBpZiAoIGIgPT09IHZvaWQgMCApIGIgPSB7fTtcblxuICAvLyBoYW5kbGUgbnVsbCB2YWx1ZSAjMTU2NlxuICBpZiAoIWEgfHwgIWIpIHsgcmV0dXJuIGEgPT09IGIgfVxuICB2YXIgYUtleXMgPSBPYmplY3Qua2V5cyhhKTtcbiAgdmFyIGJLZXlzID0gT2JqZWN0LmtleXMoYik7XG4gIGlmIChhS2V5cy5sZW5ndGggIT09IGJLZXlzLmxlbmd0aCkge1xuICAgIHJldHVybiBmYWxzZVxuICB9XG4gIHJldHVybiBhS2V5cy5ldmVyeShmdW5jdGlvbiAoa2V5KSB7XG4gICAgdmFyIGFWYWwgPSBhW2tleV07XG4gICAgdmFyIGJWYWwgPSBiW2tleV07XG4gICAgLy8gY2hlY2sgbmVzdGVkIGVxdWFsaXR5XG4gICAgaWYgKHR5cGVvZiBhVmFsID09PSAnb2JqZWN0JyAmJiB0eXBlb2YgYlZhbCA9PT0gJ29iamVjdCcpIHtcbiAgICAgIHJldHVybiBpc09iamVjdEVxdWFsKGFWYWwsIGJWYWwpXG4gICAgfVxuICAgIHJldHVybiBTdHJpbmcoYVZhbCkgPT09IFN0cmluZyhiVmFsKVxuICB9KVxufVxuXG5mdW5jdGlvbiBpc0luY2x1ZGVkUm91dGUgKGN1cnJlbnQsIHRhcmdldCkge1xuICByZXR1cm4gKFxuICAgIGN1cnJlbnQucGF0aC5yZXBsYWNlKHRyYWlsaW5nU2xhc2hSRSwgJy8nKS5pbmRleE9mKFxuICAgICAgdGFyZ2V0LnBhdGgucmVwbGFjZSh0cmFpbGluZ1NsYXNoUkUsICcvJylcbiAgICApID09PSAwICYmXG4gICAgKCF0YXJnZXQuaGFzaCB8fCBjdXJyZW50Lmhhc2ggPT09IHRhcmdldC5oYXNoKSAmJlxuICAgIHF1ZXJ5SW5jbHVkZXMoY3VycmVudC5xdWVyeSwgdGFyZ2V0LnF1ZXJ5KVxuICApXG59XG5cbmZ1bmN0aW9uIHF1ZXJ5SW5jbHVkZXMgKGN1cnJlbnQsIHRhcmdldCkge1xuICBmb3IgKHZhciBrZXkgaW4gdGFyZ2V0KSB7XG4gICAgaWYgKCEoa2V5IGluIGN1cnJlbnQpKSB7XG4gICAgICByZXR1cm4gZmFsc2VcbiAgICB9XG4gIH1cbiAgcmV0dXJuIHRydWVcbn1cblxuLyogICovXG5cbmZ1bmN0aW9uIHJlc29sdmVQYXRoIChcbiAgcmVsYXRpdmUsXG4gIGJhc2UsXG4gIGFwcGVuZFxuKSB7XG4gIHZhciBmaXJzdENoYXIgPSByZWxhdGl2ZS5jaGFyQXQoMCk7XG4gIGlmIChmaXJzdENoYXIgPT09ICcvJykge1xuICAgIHJldHVybiByZWxhdGl2ZVxuICB9XG5cbiAgaWYgKGZpcnN0Q2hhciA9PT0gJz8nIHx8IGZpcnN0Q2hhciA9PT0gJyMnKSB7XG4gICAgcmV0dXJuIGJhc2UgKyByZWxhdGl2ZVxuICB9XG5cbiAgdmFyIHN0YWNrID0gYmFzZS5zcGxpdCgnLycpO1xuXG4gIC8vIHJlbW92ZSB0cmFpbGluZyBzZWdtZW50IGlmOlxuICAvLyAtIG5vdCBhcHBlbmRpbmdcbiAgLy8gLSBhcHBlbmRpbmcgdG8gdHJhaWxpbmcgc2xhc2ggKGxhc3Qgc2VnbWVudCBpcyBlbXB0eSlcbiAgaWYgKCFhcHBlbmQgfHwgIXN0YWNrW3N0YWNrLmxlbmd0aCAtIDFdKSB7XG4gICAgc3RhY2sucG9wKCk7XG4gIH1cblxuICAvLyByZXNvbHZlIHJlbGF0aXZlIHBhdGhcbiAgdmFyIHNlZ21lbnRzID0gcmVsYXRpdmUucmVwbGFjZSgvXlxcLy8sICcnKS5zcGxpdCgnLycpO1xuICBmb3IgKHZhciBpID0gMDsgaSA8IHNlZ21lbnRzLmxlbmd0aDsgaSsrKSB7XG4gICAgdmFyIHNlZ21lbnQgPSBzZWdtZW50c1tpXTtcbiAgICBpZiAoc2VnbWVudCA9PT0gJy4uJykge1xuICAgICAgc3RhY2sucG9wKCk7XG4gICAgfSBlbHNlIGlmIChzZWdtZW50ICE9PSAnLicpIHtcbiAgICAgIHN0YWNrLnB1c2goc2VnbWVudCk7XG4gICAgfVxuICB9XG5cbiAgLy8gZW5zdXJlIGxlYWRpbmcgc2xhc2hcbiAgaWYgKHN0YWNrWzBdICE9PSAnJykge1xuICAgIHN0YWNrLnVuc2hpZnQoJycpO1xuICB9XG5cbiAgcmV0dXJuIHN0YWNrLmpvaW4oJy8nKVxufVxuXG5mdW5jdGlvbiBwYXJzZVBhdGggKHBhdGgpIHtcbiAgdmFyIGhhc2ggPSAnJztcbiAgdmFyIHF1ZXJ5ID0gJyc7XG5cbiAgdmFyIGhhc2hJbmRleCA9IHBhdGguaW5kZXhPZignIycpO1xuICBpZiAoaGFzaEluZGV4ID49IDApIHtcbiAgICBoYXNoID0gcGF0aC5zbGljZShoYXNoSW5kZXgpO1xuICAgIHBhdGggPSBwYXRoLnNsaWNlKDAsIGhhc2hJbmRleCk7XG4gIH1cblxuICB2YXIgcXVlcnlJbmRleCA9IHBhdGguaW5kZXhPZignPycpO1xuICBpZiAocXVlcnlJbmRleCA+PSAwKSB7XG4gICAgcXVlcnkgPSBwYXRoLnNsaWNlKHF1ZXJ5SW5kZXggKyAxKTtcbiAgICBwYXRoID0gcGF0aC5zbGljZSgwLCBxdWVyeUluZGV4KTtcbiAgfVxuXG4gIHJldHVybiB7XG4gICAgcGF0aDogcGF0aCxcbiAgICBxdWVyeTogcXVlcnksXG4gICAgaGFzaDogaGFzaFxuICB9XG59XG5cbmZ1bmN0aW9uIGNsZWFuUGF0aCAocGF0aCkge1xuICByZXR1cm4gcGF0aC5yZXBsYWNlKC9cXC9cXC8vZywgJy8nKVxufVxuXG52YXIgaXNhcnJheSA9IEFycmF5LmlzQXJyYXkgfHwgZnVuY3Rpb24gKGFycikge1xuICByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZy5jYWxsKGFycikgPT0gJ1tvYmplY3QgQXJyYXldJztcbn07XG5cbi8qKlxuICogRXhwb3NlIGBwYXRoVG9SZWdleHBgLlxuICovXG52YXIgcGF0aFRvUmVnZXhwXzEgPSBwYXRoVG9SZWdleHA7XG52YXIgcGFyc2VfMSA9IHBhcnNlO1xudmFyIGNvbXBpbGVfMSA9IGNvbXBpbGU7XG52YXIgdG9rZW5zVG9GdW5jdGlvbl8xID0gdG9rZW5zVG9GdW5jdGlvbjtcbnZhciB0b2tlbnNUb1JlZ0V4cF8xID0gdG9rZW5zVG9SZWdFeHA7XG5cbi8qKlxuICogVGhlIG1haW4gcGF0aCBtYXRjaGluZyByZWdleHAgdXRpbGl0eS5cbiAqXG4gKiBAdHlwZSB7UmVnRXhwfVxuICovXG52YXIgUEFUSF9SRUdFWFAgPSBuZXcgUmVnRXhwKFtcbiAgLy8gTWF0Y2ggZXNjYXBlZCBjaGFyYWN0ZXJzIHRoYXQgd291bGQgb3RoZXJ3aXNlIGFwcGVhciBpbiBmdXR1cmUgbWF0Y2hlcy5cbiAgLy8gVGhpcyBhbGxvd3MgdGhlIHVzZXIgdG8gZXNjYXBlIHNwZWNpYWwgY2hhcmFjdGVycyB0aGF0IHdvbid0IHRyYW5zZm9ybS5cbiAgJyhcXFxcXFxcXC4pJyxcbiAgLy8gTWF0Y2ggRXhwcmVzcy1zdHlsZSBwYXJhbWV0ZXJzIGFuZCB1bi1uYW1lZCBwYXJhbWV0ZXJzIHdpdGggYSBwcmVmaXhcbiAgLy8gYW5kIG9wdGlvbmFsIHN1ZmZpeGVzLiBNYXRjaGVzIGFwcGVhciBhczpcbiAgLy9cbiAgLy8gXCIvOnRlc3QoXFxcXGQrKT9cIiA9PiBbXCIvXCIsIFwidGVzdFwiLCBcIlxcZCtcIiwgdW5kZWZpbmVkLCBcIj9cIiwgdW5kZWZpbmVkXVxuICAvLyBcIi9yb3V0ZShcXFxcZCspXCIgID0+IFt1bmRlZmluZWQsIHVuZGVmaW5lZCwgdW5kZWZpbmVkLCBcIlxcZCtcIiwgdW5kZWZpbmVkLCB1bmRlZmluZWRdXG4gIC8vIFwiLypcIiAgICAgICAgICAgID0+IFtcIi9cIiwgdW5kZWZpbmVkLCB1bmRlZmluZWQsIHVuZGVmaW5lZCwgdW5kZWZpbmVkLCBcIipcIl1cbiAgJyhbXFxcXC8uXSk/KD86KD86XFxcXDooXFxcXHcrKSg/OlxcXFwoKCg/OlxcXFxcXFxcLnxbXlxcXFxcXFxcKCldKSspXFxcXCkpP3xcXFxcKCgoPzpcXFxcXFxcXC58W15cXFxcXFxcXCgpXSkrKVxcXFwpKShbKyo/XSk/fChcXFxcKikpJ1xuXS5qb2luKCd8JyksICdnJyk7XG5cbi8qKlxuICogUGFyc2UgYSBzdHJpbmcgZm9yIHRoZSByYXcgdG9rZW5zLlxuICpcbiAqIEBwYXJhbSAge3N0cmluZ30gIHN0clxuICogQHBhcmFtICB7T2JqZWN0PX0gb3B0aW9uc1xuICogQHJldHVybiB7IUFycmF5fVxuICovXG5mdW5jdGlvbiBwYXJzZSAoc3RyLCBvcHRpb25zKSB7XG4gIHZhciB0b2tlbnMgPSBbXTtcbiAgdmFyIGtleSA9IDA7XG4gIHZhciBpbmRleCA9IDA7XG4gIHZhciBwYXRoID0gJyc7XG4gIHZhciBkZWZhdWx0RGVsaW1pdGVyID0gb3B0aW9ucyAmJiBvcHRpb25zLmRlbGltaXRlciB8fCAnLyc7XG4gIHZhciByZXM7XG5cbiAgd2hpbGUgKChyZXMgPSBQQVRIX1JFR0VYUC5leGVjKHN0cikpICE9IG51bGwpIHtcbiAgICB2YXIgbSA9IHJlc1swXTtcbiAgICB2YXIgZXNjYXBlZCA9IHJlc1sxXTtcbiAgICB2YXIgb2Zmc2V0ID0gcmVzLmluZGV4O1xuICAgIHBhdGggKz0gc3RyLnNsaWNlKGluZGV4LCBvZmZzZXQpO1xuICAgIGluZGV4ID0gb2Zmc2V0ICsgbS5sZW5ndGg7XG5cbiAgICAvLyBJZ25vcmUgYWxyZWFkeSBlc2NhcGVkIHNlcXVlbmNlcy5cbiAgICBpZiAoZXNjYXBlZCkge1xuICAgICAgcGF0aCArPSBlc2NhcGVkWzFdO1xuICAgICAgY29udGludWVcbiAgICB9XG5cbiAgICB2YXIgbmV4dCA9IHN0cltpbmRleF07XG4gICAgdmFyIHByZWZpeCA9IHJlc1syXTtcbiAgICB2YXIgbmFtZSA9IHJlc1szXTtcbiAgICB2YXIgY2FwdHVyZSA9IHJlc1s0XTtcbiAgICB2YXIgZ3JvdXAgPSByZXNbNV07XG4gICAgdmFyIG1vZGlmaWVyID0gcmVzWzZdO1xuICAgIHZhciBhc3RlcmlzayA9IHJlc1s3XTtcblxuICAgIC8vIFB1c2ggdGhlIGN1cnJlbnQgcGF0aCBvbnRvIHRoZSB0b2tlbnMuXG4gICAgaWYgKHBhdGgpIHtcbiAgICAgIHRva2Vucy5wdXNoKHBhdGgpO1xuICAgICAgcGF0aCA9ICcnO1xuICAgIH1cblxuICAgIHZhciBwYXJ0aWFsID0gcHJlZml4ICE9IG51bGwgJiYgbmV4dCAhPSBudWxsICYmIG5leHQgIT09IHByZWZpeDtcbiAgICB2YXIgcmVwZWF0ID0gbW9kaWZpZXIgPT09ICcrJyB8fCBtb2RpZmllciA9PT0gJyonO1xuICAgIHZhciBvcHRpb25hbCA9IG1vZGlmaWVyID09PSAnPycgfHwgbW9kaWZpZXIgPT09ICcqJztcbiAgICB2YXIgZGVsaW1pdGVyID0gcmVzWzJdIHx8IGRlZmF1bHREZWxpbWl0ZXI7XG4gICAgdmFyIHBhdHRlcm4gPSBjYXB0dXJlIHx8IGdyb3VwO1xuXG4gICAgdG9rZW5zLnB1c2goe1xuICAgICAgbmFtZTogbmFtZSB8fCBrZXkrKyxcbiAgICAgIHByZWZpeDogcHJlZml4IHx8ICcnLFxuICAgICAgZGVsaW1pdGVyOiBkZWxpbWl0ZXIsXG4gICAgICBvcHRpb25hbDogb3B0aW9uYWwsXG4gICAgICByZXBlYXQ6IHJlcGVhdCxcbiAgICAgIHBhcnRpYWw6IHBhcnRpYWwsXG4gICAgICBhc3RlcmlzazogISFhc3RlcmlzayxcbiAgICAgIHBhdHRlcm46IHBhdHRlcm4gPyBlc2NhcGVHcm91cChwYXR0ZXJuKSA6IChhc3RlcmlzayA/ICcuKicgOiAnW14nICsgZXNjYXBlU3RyaW5nKGRlbGltaXRlcikgKyAnXSs/JylcbiAgICB9KTtcbiAgfVxuXG4gIC8vIE1hdGNoIGFueSBjaGFyYWN0ZXJzIHN0aWxsIHJlbWFpbmluZy5cbiAgaWYgKGluZGV4IDwgc3RyLmxlbmd0aCkge1xuICAgIHBhdGggKz0gc3RyLnN1YnN0cihpbmRleCk7XG4gIH1cblxuICAvLyBJZiB0aGUgcGF0aCBleGlzdHMsIHB1c2ggaXQgb250byB0aGUgZW5kLlxuICBpZiAocGF0aCkge1xuICAgIHRva2Vucy5wdXNoKHBhdGgpO1xuICB9XG5cbiAgcmV0dXJuIHRva2Vuc1xufVxuXG4vKipcbiAqIENvbXBpbGUgYSBzdHJpbmcgdG8gYSB0ZW1wbGF0ZSBmdW5jdGlvbiBmb3IgdGhlIHBhdGguXG4gKlxuICogQHBhcmFtICB7c3RyaW5nfSAgICAgICAgICAgICBzdHJcbiAqIEBwYXJhbSAge09iamVjdD19ICAgICAgICAgICAgb3B0aW9uc1xuICogQHJldHVybiB7IWZ1bmN0aW9uKE9iamVjdD0sIE9iamVjdD0pfVxuICovXG5mdW5jdGlvbiBjb21waWxlIChzdHIsIG9wdGlvbnMpIHtcbiAgcmV0dXJuIHRva2Vuc1RvRnVuY3Rpb24ocGFyc2Uoc3RyLCBvcHRpb25zKSlcbn1cblxuLyoqXG4gKiBQcmV0dGllciBlbmNvZGluZyBvZiBVUkkgcGF0aCBzZWdtZW50cy5cbiAqXG4gKiBAcGFyYW0gIHtzdHJpbmd9XG4gKiBAcmV0dXJuIHtzdHJpbmd9XG4gKi9cbmZ1bmN0aW9uIGVuY29kZVVSSUNvbXBvbmVudFByZXR0eSAoc3RyKSB7XG4gIHJldHVybiBlbmNvZGVVUkkoc3RyKS5yZXBsYWNlKC9bXFwvPyNdL2csIGZ1bmN0aW9uIChjKSB7XG4gICAgcmV0dXJuICclJyArIGMuY2hhckNvZGVBdCgwKS50b1N0cmluZygxNikudG9VcHBlckNhc2UoKVxuICB9KVxufVxuXG4vKipcbiAqIEVuY29kZSB0aGUgYXN0ZXJpc2sgcGFyYW1ldGVyLiBTaW1pbGFyIHRvIGBwcmV0dHlgLCBidXQgYWxsb3dzIHNsYXNoZXMuXG4gKlxuICogQHBhcmFtICB7c3RyaW5nfVxuICogQHJldHVybiB7c3RyaW5nfVxuICovXG5mdW5jdGlvbiBlbmNvZGVBc3RlcmlzayAoc3RyKSB7XG4gIHJldHVybiBlbmNvZGVVUkkoc3RyKS5yZXBsYWNlKC9bPyNdL2csIGZ1bmN0aW9uIChjKSB7XG4gICAgcmV0dXJuICclJyArIGMuY2hhckNvZGVBdCgwKS50b1N0cmluZygxNikudG9VcHBlckNhc2UoKVxuICB9KVxufVxuXG4vKipcbiAqIEV4cG9zZSBhIG1ldGhvZCBmb3IgdHJhbnNmb3JtaW5nIHRva2VucyBpbnRvIHRoZSBwYXRoIGZ1bmN0aW9uLlxuICovXG5mdW5jdGlvbiB0b2tlbnNUb0Z1bmN0aW9uICh0b2tlbnMpIHtcbiAgLy8gQ29tcGlsZSBhbGwgdGhlIHRva2VucyBpbnRvIHJlZ2V4cHMuXG4gIHZhciBtYXRjaGVzID0gbmV3IEFycmF5KHRva2Vucy5sZW5ndGgpO1xuXG4gIC8vIENvbXBpbGUgYWxsIHRoZSBwYXR0ZXJucyBiZWZvcmUgY29tcGlsYXRpb24uXG4gIGZvciAodmFyIGkgPSAwOyBpIDwgdG9rZW5zLmxlbmd0aDsgaSsrKSB7XG4gICAgaWYgKHR5cGVvZiB0b2tlbnNbaV0gPT09ICdvYmplY3QnKSB7XG4gICAgICBtYXRjaGVzW2ldID0gbmV3IFJlZ0V4cCgnXig/OicgKyB0b2tlbnNbaV0ucGF0dGVybiArICcpJCcpO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiBmdW5jdGlvbiAob2JqLCBvcHRzKSB7XG4gICAgdmFyIHBhdGggPSAnJztcbiAgICB2YXIgZGF0YSA9IG9iaiB8fCB7fTtcbiAgICB2YXIgb3B0aW9ucyA9IG9wdHMgfHwge307XG4gICAgdmFyIGVuY29kZSA9IG9wdGlvbnMucHJldHR5ID8gZW5jb2RlVVJJQ29tcG9uZW50UHJldHR5IDogZW5jb2RlVVJJQ29tcG9uZW50O1xuXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCB0b2tlbnMubGVuZ3RoOyBpKyspIHtcbiAgICAgIHZhciB0b2tlbiA9IHRva2Vuc1tpXTtcblxuICAgICAgaWYgKHR5cGVvZiB0b2tlbiA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgcGF0aCArPSB0b2tlbjtcblxuICAgICAgICBjb250aW51ZVxuICAgICAgfVxuXG4gICAgICB2YXIgdmFsdWUgPSBkYXRhW3Rva2VuLm5hbWVdO1xuICAgICAgdmFyIHNlZ21lbnQ7XG5cbiAgICAgIGlmICh2YWx1ZSA9PSBudWxsKSB7XG4gICAgICAgIGlmICh0b2tlbi5vcHRpb25hbCkge1xuICAgICAgICAgIC8vIFByZXBlbmQgcGFydGlhbCBzZWdtZW50IHByZWZpeGVzLlxuICAgICAgICAgIGlmICh0b2tlbi5wYXJ0aWFsKSB7XG4gICAgICAgICAgICBwYXRoICs9IHRva2VuLnByZWZpeDtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBjb250aW51ZVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHRocm93IG5ldyBUeXBlRXJyb3IoJ0V4cGVjdGVkIFwiJyArIHRva2VuLm5hbWUgKyAnXCIgdG8gYmUgZGVmaW5lZCcpXG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgaWYgKGlzYXJyYXkodmFsdWUpKSB7XG4gICAgICAgIGlmICghdG9rZW4ucmVwZWF0KSB7XG4gICAgICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcignRXhwZWN0ZWQgXCInICsgdG9rZW4ubmFtZSArICdcIiB0byBub3QgcmVwZWF0LCBidXQgcmVjZWl2ZWQgYCcgKyBKU09OLnN0cmluZ2lmeSh2YWx1ZSkgKyAnYCcpXG4gICAgICAgIH1cblxuICAgICAgICBpZiAodmFsdWUubGVuZ3RoID09PSAwKSB7XG4gICAgICAgICAgaWYgKHRva2VuLm9wdGlvbmFsKSB7XG4gICAgICAgICAgICBjb250aW51ZVxuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgVHlwZUVycm9yKCdFeHBlY3RlZCBcIicgKyB0b2tlbi5uYW1lICsgJ1wiIHRvIG5vdCBiZSBlbXB0eScpXG4gICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgZm9yICh2YXIgaiA9IDA7IGogPCB2YWx1ZS5sZW5ndGg7IGorKykge1xuICAgICAgICAgIHNlZ21lbnQgPSBlbmNvZGUodmFsdWVbal0pO1xuXG4gICAgICAgICAgaWYgKCFtYXRjaGVzW2ldLnRlc3Qoc2VnbWVudCkpIHtcbiAgICAgICAgICAgIHRocm93IG5ldyBUeXBlRXJyb3IoJ0V4cGVjdGVkIGFsbCBcIicgKyB0b2tlbi5uYW1lICsgJ1wiIHRvIG1hdGNoIFwiJyArIHRva2VuLnBhdHRlcm4gKyAnXCIsIGJ1dCByZWNlaXZlZCBgJyArIEpTT04uc3RyaW5naWZ5KHNlZ21lbnQpICsgJ2AnKVxuICAgICAgICAgIH1cblxuICAgICAgICAgIHBhdGggKz0gKGogPT09IDAgPyB0b2tlbi5wcmVmaXggOiB0b2tlbi5kZWxpbWl0ZXIpICsgc2VnbWVudDtcbiAgICAgICAgfVxuXG4gICAgICAgIGNvbnRpbnVlXG4gICAgICB9XG5cbiAgICAgIHNlZ21lbnQgPSB0b2tlbi5hc3RlcmlzayA/IGVuY29kZUFzdGVyaXNrKHZhbHVlKSA6IGVuY29kZSh2YWx1ZSk7XG5cbiAgICAgIGlmICghbWF0Y2hlc1tpXS50ZXN0KHNlZ21lbnQpKSB7XG4gICAgICAgIHRocm93IG5ldyBUeXBlRXJyb3IoJ0V4cGVjdGVkIFwiJyArIHRva2VuLm5hbWUgKyAnXCIgdG8gbWF0Y2ggXCInICsgdG9rZW4ucGF0dGVybiArICdcIiwgYnV0IHJlY2VpdmVkIFwiJyArIHNlZ21lbnQgKyAnXCInKVxuICAgICAgfVxuXG4gICAgICBwYXRoICs9IHRva2VuLnByZWZpeCArIHNlZ21lbnQ7XG4gICAgfVxuXG4gICAgcmV0dXJuIHBhdGhcbiAgfVxufVxuXG4vKipcbiAqIEVzY2FwZSBhIHJlZ3VsYXIgZXhwcmVzc2lvbiBzdHJpbmcuXG4gKlxuICogQHBhcmFtICB7c3RyaW5nfSBzdHJcbiAqIEByZXR1cm4ge3N0cmluZ31cbiAqL1xuZnVuY3Rpb24gZXNjYXBlU3RyaW5nIChzdHIpIHtcbiAgcmV0dXJuIHN0ci5yZXBsYWNlKC8oWy4rKj89XiE6JHt9KClbXFxdfFxcL1xcXFxdKS9nLCAnXFxcXCQxJylcbn1cblxuLyoqXG4gKiBFc2NhcGUgdGhlIGNhcHR1cmluZyBncm91cCBieSBlc2NhcGluZyBzcGVjaWFsIGNoYXJhY3RlcnMgYW5kIG1lYW5pbmcuXG4gKlxuICogQHBhcmFtICB7c3RyaW5nfSBncm91cFxuICogQHJldHVybiB7c3RyaW5nfVxuICovXG5mdW5jdGlvbiBlc2NhcGVHcm91cCAoZ3JvdXApIHtcbiAgcmV0dXJuIGdyb3VwLnJlcGxhY2UoLyhbPSE6JFxcLygpXSkvZywgJ1xcXFwkMScpXG59XG5cbi8qKlxuICogQXR0YWNoIHRoZSBrZXlzIGFzIGEgcHJvcGVydHkgb2YgdGhlIHJlZ2V4cC5cbiAqXG4gKiBAcGFyYW0gIHshUmVnRXhwfSByZVxuICogQHBhcmFtICB7QXJyYXl9ICAga2V5c1xuICogQHJldHVybiB7IVJlZ0V4cH1cbiAqL1xuZnVuY3Rpb24gYXR0YWNoS2V5cyAocmUsIGtleXMpIHtcbiAgcmUua2V5cyA9IGtleXM7XG4gIHJldHVybiByZVxufVxuXG4vKipcbiAqIEdldCB0aGUgZmxhZ3MgZm9yIGEgcmVnZXhwIGZyb20gdGhlIG9wdGlvbnMuXG4gKlxuICogQHBhcmFtICB7T2JqZWN0fSBvcHRpb25zXG4gKiBAcmV0dXJuIHtzdHJpbmd9XG4gKi9cbmZ1bmN0aW9uIGZsYWdzIChvcHRpb25zKSB7XG4gIHJldHVybiBvcHRpb25zLnNlbnNpdGl2ZSA/ICcnIDogJ2knXG59XG5cbi8qKlxuICogUHVsbCBvdXQga2V5cyBmcm9tIGEgcmVnZXhwLlxuICpcbiAqIEBwYXJhbSAgeyFSZWdFeHB9IHBhdGhcbiAqIEBwYXJhbSAgeyFBcnJheX0gIGtleXNcbiAqIEByZXR1cm4geyFSZWdFeHB9XG4gKi9cbmZ1bmN0aW9uIHJlZ2V4cFRvUmVnZXhwIChwYXRoLCBrZXlzKSB7XG4gIC8vIFVzZSBhIG5lZ2F0aXZlIGxvb2thaGVhZCB0byBtYXRjaCBvbmx5IGNhcHR1cmluZyBncm91cHMuXG4gIHZhciBncm91cHMgPSBwYXRoLnNvdXJjZS5tYXRjaCgvXFwoKD8hXFw/KS9nKTtcblxuICBpZiAoZ3JvdXBzKSB7XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBncm91cHMubGVuZ3RoOyBpKyspIHtcbiAgICAgIGtleXMucHVzaCh7XG4gICAgICAgIG5hbWU6IGksXG4gICAgICAgIHByZWZpeDogbnVsbCxcbiAgICAgICAgZGVsaW1pdGVyOiBudWxsLFxuICAgICAgICBvcHRpb25hbDogZmFsc2UsXG4gICAgICAgIHJlcGVhdDogZmFsc2UsXG4gICAgICAgIHBhcnRpYWw6IGZhbHNlLFxuICAgICAgICBhc3RlcmlzazogZmFsc2UsXG4gICAgICAgIHBhdHRlcm46IG51bGxcbiAgICAgIH0pO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiBhdHRhY2hLZXlzKHBhdGgsIGtleXMpXG59XG5cbi8qKlxuICogVHJhbnNmb3JtIGFuIGFycmF5IGludG8gYSByZWdleHAuXG4gKlxuICogQHBhcmFtICB7IUFycmF5fSAgcGF0aFxuICogQHBhcmFtICB7QXJyYXl9ICAga2V5c1xuICogQHBhcmFtICB7IU9iamVjdH0gb3B0aW9uc1xuICogQHJldHVybiB7IVJlZ0V4cH1cbiAqL1xuZnVuY3Rpb24gYXJyYXlUb1JlZ2V4cCAocGF0aCwga2V5cywgb3B0aW9ucykge1xuICB2YXIgcGFydHMgPSBbXTtcblxuICBmb3IgKHZhciBpID0gMDsgaSA8IHBhdGgubGVuZ3RoOyBpKyspIHtcbiAgICBwYXJ0cy5wdXNoKHBhdGhUb1JlZ2V4cChwYXRoW2ldLCBrZXlzLCBvcHRpb25zKS5zb3VyY2UpO1xuICB9XG5cbiAgdmFyIHJlZ2V4cCA9IG5ldyBSZWdFeHAoJyg/OicgKyBwYXJ0cy5qb2luKCd8JykgKyAnKScsIGZsYWdzKG9wdGlvbnMpKTtcblxuICByZXR1cm4gYXR0YWNoS2V5cyhyZWdleHAsIGtleXMpXG59XG5cbi8qKlxuICogQ3JlYXRlIGEgcGF0aCByZWdleHAgZnJvbSBzdHJpbmcgaW5wdXQuXG4gKlxuICogQHBhcmFtICB7c3RyaW5nfSAgcGF0aFxuICogQHBhcmFtICB7IUFycmF5fSAga2V5c1xuICogQHBhcmFtICB7IU9iamVjdH0gb3B0aW9uc1xuICogQHJldHVybiB7IVJlZ0V4cH1cbiAqL1xuZnVuY3Rpb24gc3RyaW5nVG9SZWdleHAgKHBhdGgsIGtleXMsIG9wdGlvbnMpIHtcbiAgcmV0dXJuIHRva2Vuc1RvUmVnRXhwKHBhcnNlKHBhdGgsIG9wdGlvbnMpLCBrZXlzLCBvcHRpb25zKVxufVxuXG4vKipcbiAqIEV4cG9zZSBhIGZ1bmN0aW9uIGZvciB0YWtpbmcgdG9rZW5zIGFuZCByZXR1cm5pbmcgYSBSZWdFeHAuXG4gKlxuICogQHBhcmFtICB7IUFycmF5fSAgICAgICAgICB0b2tlbnNcbiAqIEBwYXJhbSAgeyhBcnJheXxPYmplY3QpPX0ga2V5c1xuICogQHBhcmFtICB7T2JqZWN0PX0gICAgICAgICBvcHRpb25zXG4gKiBAcmV0dXJuIHshUmVnRXhwfVxuICovXG5mdW5jdGlvbiB0b2tlbnNUb1JlZ0V4cCAodG9rZW5zLCBrZXlzLCBvcHRpb25zKSB7XG4gIGlmICghaXNhcnJheShrZXlzKSkge1xuICAgIG9wdGlvbnMgPSAvKiogQHR5cGUgeyFPYmplY3R9ICovIChrZXlzIHx8IG9wdGlvbnMpO1xuICAgIGtleXMgPSBbXTtcbiAgfVxuXG4gIG9wdGlvbnMgPSBvcHRpb25zIHx8IHt9O1xuXG4gIHZhciBzdHJpY3QgPSBvcHRpb25zLnN0cmljdDtcbiAgdmFyIGVuZCA9IG9wdGlvbnMuZW5kICE9PSBmYWxzZTtcbiAgdmFyIHJvdXRlID0gJyc7XG5cbiAgLy8gSXRlcmF0ZSBvdmVyIHRoZSB0b2tlbnMgYW5kIGNyZWF0ZSBvdXIgcmVnZXhwIHN0cmluZy5cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCB0b2tlbnMubGVuZ3RoOyBpKyspIHtcbiAgICB2YXIgdG9rZW4gPSB0b2tlbnNbaV07XG5cbiAgICBpZiAodHlwZW9mIHRva2VuID09PSAnc3RyaW5nJykge1xuICAgICAgcm91dGUgKz0gZXNjYXBlU3RyaW5nKHRva2VuKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdmFyIHByZWZpeCA9IGVzY2FwZVN0cmluZyh0b2tlbi5wcmVmaXgpO1xuICAgICAgdmFyIGNhcHR1cmUgPSAnKD86JyArIHRva2VuLnBhdHRlcm4gKyAnKSc7XG5cbiAgICAgIGtleXMucHVzaCh0b2tlbik7XG5cbiAgICAgIGlmICh0b2tlbi5yZXBlYXQpIHtcbiAgICAgICAgY2FwdHVyZSArPSAnKD86JyArIHByZWZpeCArIGNhcHR1cmUgKyAnKSonO1xuICAgICAgfVxuXG4gICAgICBpZiAodG9rZW4ub3B0aW9uYWwpIHtcbiAgICAgICAgaWYgKCF0b2tlbi5wYXJ0aWFsKSB7XG4gICAgICAgICAgY2FwdHVyZSA9ICcoPzonICsgcHJlZml4ICsgJygnICsgY2FwdHVyZSArICcpKT8nO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGNhcHR1cmUgPSBwcmVmaXggKyAnKCcgKyBjYXB0dXJlICsgJyk/JztcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgY2FwdHVyZSA9IHByZWZpeCArICcoJyArIGNhcHR1cmUgKyAnKSc7XG4gICAgICB9XG5cbiAgICAgIHJvdXRlICs9IGNhcHR1cmU7XG4gICAgfVxuICB9XG5cbiAgdmFyIGRlbGltaXRlciA9IGVzY2FwZVN0cmluZyhvcHRpb25zLmRlbGltaXRlciB8fCAnLycpO1xuICB2YXIgZW5kc1dpdGhEZWxpbWl0ZXIgPSByb3V0ZS5zbGljZSgtZGVsaW1pdGVyLmxlbmd0aCkgPT09IGRlbGltaXRlcjtcblxuICAvLyBJbiBub24tc3RyaWN0IG1vZGUgd2UgYWxsb3cgYSBzbGFzaCBhdCB0aGUgZW5kIG9mIG1hdGNoLiBJZiB0aGUgcGF0aCB0b1xuICAvLyBtYXRjaCBhbHJlYWR5IGVuZHMgd2l0aCBhIHNsYXNoLCB3ZSByZW1vdmUgaXQgZm9yIGNvbnNpc3RlbmN5LiBUaGUgc2xhc2hcbiAgLy8gaXMgdmFsaWQgYXQgdGhlIGVuZCBvZiBhIHBhdGggbWF0Y2gsIG5vdCBpbiB0aGUgbWlkZGxlLiBUaGlzIGlzIGltcG9ydGFudFxuICAvLyBpbiBub24tZW5kaW5nIG1vZGUsIHdoZXJlIFwiL3Rlc3QvXCIgc2hvdWxkbid0IG1hdGNoIFwiL3Rlc3QvL3JvdXRlXCIuXG4gIGlmICghc3RyaWN0KSB7XG4gICAgcm91dGUgPSAoZW5kc1dpdGhEZWxpbWl0ZXIgPyByb3V0ZS5zbGljZSgwLCAtZGVsaW1pdGVyLmxlbmd0aCkgOiByb3V0ZSkgKyAnKD86JyArIGRlbGltaXRlciArICcoPz0kKSk/JztcbiAgfVxuXG4gIGlmIChlbmQpIHtcbiAgICByb3V0ZSArPSAnJCc7XG4gIH0gZWxzZSB7XG4gICAgLy8gSW4gbm9uLWVuZGluZyBtb2RlLCB3ZSBuZWVkIHRoZSBjYXB0dXJpbmcgZ3JvdXBzIHRvIG1hdGNoIGFzIG11Y2ggYXNcbiAgICAvLyBwb3NzaWJsZSBieSB1c2luZyBhIHBvc2l0aXZlIGxvb2thaGVhZCB0byB0aGUgZW5kIG9yIG5leHQgcGF0aCBzZWdtZW50LlxuICAgIHJvdXRlICs9IHN0cmljdCAmJiBlbmRzV2l0aERlbGltaXRlciA/ICcnIDogJyg/PScgKyBkZWxpbWl0ZXIgKyAnfCQpJztcbiAgfVxuXG4gIHJldHVybiBhdHRhY2hLZXlzKG5ldyBSZWdFeHAoJ14nICsgcm91dGUsIGZsYWdzKG9wdGlvbnMpKSwga2V5cylcbn1cblxuLyoqXG4gKiBOb3JtYWxpemUgdGhlIGdpdmVuIHBhdGggc3RyaW5nLCByZXR1cm5pbmcgYSByZWd1bGFyIGV4cHJlc3Npb24uXG4gKlxuICogQW4gZW1wdHkgYXJyYXkgY2FuIGJlIHBhc3NlZCBpbiBmb3IgdGhlIGtleXMsIHdoaWNoIHdpbGwgaG9sZCB0aGVcbiAqIHBsYWNlaG9sZGVyIGtleSBkZXNjcmlwdGlvbnMuIEZvciBleGFtcGxlLCB1c2luZyBgL3VzZXIvOmlkYCwgYGtleXNgIHdpbGxcbiAqIGNvbnRhaW4gYFt7IG5hbWU6ICdpZCcsIGRlbGltaXRlcjogJy8nLCBvcHRpb25hbDogZmFsc2UsIHJlcGVhdDogZmFsc2UgfV1gLlxuICpcbiAqIEBwYXJhbSAgeyhzdHJpbmd8UmVnRXhwfEFycmF5KX0gcGF0aFxuICogQHBhcmFtICB7KEFycmF5fE9iamVjdCk9fSAgICAgICBrZXlzXG4gKiBAcGFyYW0gIHtPYmplY3Q9fSAgICAgICAgICAgICAgIG9wdGlvbnNcbiAqIEByZXR1cm4geyFSZWdFeHB9XG4gKi9cbmZ1bmN0aW9uIHBhdGhUb1JlZ2V4cCAocGF0aCwga2V5cywgb3B0aW9ucykge1xuICBpZiAoIWlzYXJyYXkoa2V5cykpIHtcbiAgICBvcHRpb25zID0gLyoqIEB0eXBlIHshT2JqZWN0fSAqLyAoa2V5cyB8fCBvcHRpb25zKTtcbiAgICBrZXlzID0gW107XG4gIH1cblxuICBvcHRpb25zID0gb3B0aW9ucyB8fCB7fTtcblxuICBpZiAocGF0aCBpbnN0YW5jZW9mIFJlZ0V4cCkge1xuICAgIHJldHVybiByZWdleHBUb1JlZ2V4cChwYXRoLCAvKiogQHR5cGUgeyFBcnJheX0gKi8gKGtleXMpKVxuICB9XG5cbiAgaWYgKGlzYXJyYXkocGF0aCkpIHtcbiAgICByZXR1cm4gYXJyYXlUb1JlZ2V4cCgvKiogQHR5cGUgeyFBcnJheX0gKi8gKHBhdGgpLCAvKiogQHR5cGUgeyFBcnJheX0gKi8gKGtleXMpLCBvcHRpb25zKVxuICB9XG5cbiAgcmV0dXJuIHN0cmluZ1RvUmVnZXhwKC8qKiBAdHlwZSB7c3RyaW5nfSAqLyAocGF0aCksIC8qKiBAdHlwZSB7IUFycmF5fSAqLyAoa2V5cyksIG9wdGlvbnMpXG59XG5wYXRoVG9SZWdleHBfMS5wYXJzZSA9IHBhcnNlXzE7XG5wYXRoVG9SZWdleHBfMS5jb21waWxlID0gY29tcGlsZV8xO1xucGF0aFRvUmVnZXhwXzEudG9rZW5zVG9GdW5jdGlvbiA9IHRva2Vuc1RvRnVuY3Rpb25fMTtcbnBhdGhUb1JlZ2V4cF8xLnRva2Vuc1RvUmVnRXhwID0gdG9rZW5zVG9SZWdFeHBfMTtcblxuLyogICovXG5cbi8vICRmbG93LWRpc2FibGUtbGluZVxudmFyIHJlZ2V4cENvbXBpbGVDYWNoZSA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG5cbmZ1bmN0aW9uIGZpbGxQYXJhbXMgKFxuICBwYXRoLFxuICBwYXJhbXMsXG4gIHJvdXRlTXNnXG4pIHtcbiAgcGFyYW1zID0gcGFyYW1zIHx8IHt9O1xuICB0cnkge1xuICAgIHZhciBmaWxsZXIgPVxuICAgICAgcmVnZXhwQ29tcGlsZUNhY2hlW3BhdGhdIHx8XG4gICAgICAocmVnZXhwQ29tcGlsZUNhY2hlW3BhdGhdID0gcGF0aFRvUmVnZXhwXzEuY29tcGlsZShwYXRoKSk7XG5cbiAgICAvLyBGaXggIzI1MDUgcmVzb2x2aW5nIGFzdGVyaXNrIHJvdXRlcyB7IG5hbWU6ICdub3QtZm91bmQnLCBwYXJhbXM6IHsgcGF0aE1hdGNoOiAnL25vdC1mb3VuZCcgfX1cbiAgICBpZiAocGFyYW1zLnBhdGhNYXRjaCkgeyBwYXJhbXNbMF0gPSBwYXJhbXMucGF0aE1hdGNoOyB9XG5cbiAgICByZXR1cm4gZmlsbGVyKHBhcmFtcywgeyBwcmV0dHk6IHRydWUgfSlcbiAgfSBjYXRjaCAoZSkge1xuICAgIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XG4gICAgICB3YXJuKGZhbHNlLCAoXCJtaXNzaW5nIHBhcmFtIGZvciBcIiArIHJvdXRlTXNnICsgXCI6IFwiICsgKGUubWVzc2FnZSkpKTtcbiAgICB9XG4gICAgcmV0dXJuICcnXG4gIH0gZmluYWxseSB7XG4gICAgLy8gZGVsZXRlIHRoZSAwIGlmIGl0IHdhcyBhZGRlZFxuICAgIGRlbGV0ZSBwYXJhbXNbMF07XG4gIH1cbn1cblxuLyogICovXG5cbmZ1bmN0aW9uIG5vcm1hbGl6ZUxvY2F0aW9uIChcbiAgcmF3LFxuICBjdXJyZW50LFxuICBhcHBlbmQsXG4gIHJvdXRlclxuKSB7XG4gIHZhciBuZXh0ID0gdHlwZW9mIHJhdyA9PT0gJ3N0cmluZycgPyB7IHBhdGg6IHJhdyB9IDogcmF3O1xuICAvLyBuYW1lZCB0YXJnZXRcbiAgaWYgKG5leHQuX25vcm1hbGl6ZWQpIHtcbiAgICByZXR1cm4gbmV4dFxuICB9IGVsc2UgaWYgKG5leHQubmFtZSkge1xuICAgIHJldHVybiBleHRlbmQoe30sIHJhdylcbiAgfVxuXG4gIC8vIHJlbGF0aXZlIHBhcmFtc1xuICBpZiAoIW5leHQucGF0aCAmJiBuZXh0LnBhcmFtcyAmJiBjdXJyZW50KSB7XG4gICAgbmV4dCA9IGV4dGVuZCh7fSwgbmV4dCk7XG4gICAgbmV4dC5fbm9ybWFsaXplZCA9IHRydWU7XG4gICAgdmFyIHBhcmFtcyA9IGV4dGVuZChleHRlbmQoe30sIGN1cnJlbnQucGFyYW1zKSwgbmV4dC5wYXJhbXMpO1xuICAgIGlmIChjdXJyZW50Lm5hbWUpIHtcbiAgICAgIG5leHQubmFtZSA9IGN1cnJlbnQubmFtZTtcbiAgICAgIG5leHQucGFyYW1zID0gcGFyYW1zO1xuICAgIH0gZWxzZSBpZiAoY3VycmVudC5tYXRjaGVkLmxlbmd0aCkge1xuICAgICAgdmFyIHJhd1BhdGggPSBjdXJyZW50Lm1hdGNoZWRbY3VycmVudC5tYXRjaGVkLmxlbmd0aCAtIDFdLnBhdGg7XG4gICAgICBuZXh0LnBhdGggPSBmaWxsUGFyYW1zKHJhd1BhdGgsIHBhcmFtcywgKFwicGF0aCBcIiArIChjdXJyZW50LnBhdGgpKSk7XG4gICAgfSBlbHNlIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XG4gICAgICB3YXJuKGZhbHNlLCBcInJlbGF0aXZlIHBhcmFtcyBuYXZpZ2F0aW9uIHJlcXVpcmVzIGEgY3VycmVudCByb3V0ZS5cIik7XG4gICAgfVxuICAgIHJldHVybiBuZXh0XG4gIH1cblxuICB2YXIgcGFyc2VkUGF0aCA9IHBhcnNlUGF0aChuZXh0LnBhdGggfHwgJycpO1xuICB2YXIgYmFzZVBhdGggPSAoY3VycmVudCAmJiBjdXJyZW50LnBhdGgpIHx8ICcvJztcbiAgdmFyIHBhdGggPSBwYXJzZWRQYXRoLnBhdGhcbiAgICA/IHJlc29sdmVQYXRoKHBhcnNlZFBhdGgucGF0aCwgYmFzZVBhdGgsIGFwcGVuZCB8fCBuZXh0LmFwcGVuZClcbiAgICA6IGJhc2VQYXRoO1xuXG4gIHZhciBxdWVyeSA9IHJlc29sdmVRdWVyeShcbiAgICBwYXJzZWRQYXRoLnF1ZXJ5LFxuICAgIG5leHQucXVlcnksXG4gICAgcm91dGVyICYmIHJvdXRlci5vcHRpb25zLnBhcnNlUXVlcnlcbiAgKTtcblxuICB2YXIgaGFzaCA9IG5leHQuaGFzaCB8fCBwYXJzZWRQYXRoLmhhc2g7XG4gIGlmIChoYXNoICYmIGhhc2guY2hhckF0KDApICE9PSAnIycpIHtcbiAgICBoYXNoID0gXCIjXCIgKyBoYXNoO1xuICB9XG5cbiAgcmV0dXJuIHtcbiAgICBfbm9ybWFsaXplZDogdHJ1ZSxcbiAgICBwYXRoOiBwYXRoLFxuICAgIHF1ZXJ5OiBxdWVyeSxcbiAgICBoYXNoOiBoYXNoXG4gIH1cbn1cblxuLyogICovXG5cbi8vIHdvcmsgYXJvdW5kIHdlaXJkIGZsb3cgYnVnXG52YXIgdG9UeXBlcyA9IFtTdHJpbmcsIE9iamVjdF07XG52YXIgZXZlbnRUeXBlcyA9IFtTdHJpbmcsIEFycmF5XTtcblxudmFyIG5vb3AgPSBmdW5jdGlvbiAoKSB7fTtcblxudmFyIExpbmsgPSB7XG4gIG5hbWU6ICdSb3V0ZXJMaW5rJyxcbiAgcHJvcHM6IHtcbiAgICB0bzoge1xuICAgICAgdHlwZTogdG9UeXBlcyxcbiAgICAgIHJlcXVpcmVkOiB0cnVlXG4gICAgfSxcbiAgICB0YWc6IHtcbiAgICAgIHR5cGU6IFN0cmluZyxcbiAgICAgIGRlZmF1bHQ6ICdhJ1xuICAgIH0sXG4gICAgZXhhY3Q6IEJvb2xlYW4sXG4gICAgYXBwZW5kOiBCb29sZWFuLFxuICAgIHJlcGxhY2U6IEJvb2xlYW4sXG4gICAgYWN0aXZlQ2xhc3M6IFN0cmluZyxcbiAgICBleGFjdEFjdGl2ZUNsYXNzOiBTdHJpbmcsXG4gICAgZXZlbnQ6IHtcbiAgICAgIHR5cGU6IGV2ZW50VHlwZXMsXG4gICAgICBkZWZhdWx0OiAnY2xpY2snXG4gICAgfVxuICB9LFxuICByZW5kZXI6IGZ1bmN0aW9uIHJlbmRlciAoaCkge1xuICAgIHZhciB0aGlzJDEgPSB0aGlzO1xuXG4gICAgdmFyIHJvdXRlciA9IHRoaXMuJHJvdXRlcjtcbiAgICB2YXIgY3VycmVudCA9IHRoaXMuJHJvdXRlO1xuICAgIHZhciByZWYgPSByb3V0ZXIucmVzb2x2ZShcbiAgICAgIHRoaXMudG8sXG4gICAgICBjdXJyZW50LFxuICAgICAgdGhpcy5hcHBlbmRcbiAgICApO1xuICAgIHZhciBsb2NhdGlvbiA9IHJlZi5sb2NhdGlvbjtcbiAgICB2YXIgcm91dGUgPSByZWYucm91dGU7XG4gICAgdmFyIGhyZWYgPSByZWYuaHJlZjtcblxuICAgIHZhciBjbGFzc2VzID0ge307XG4gICAgdmFyIGdsb2JhbEFjdGl2ZUNsYXNzID0gcm91dGVyLm9wdGlvbnMubGlua0FjdGl2ZUNsYXNzO1xuICAgIHZhciBnbG9iYWxFeGFjdEFjdGl2ZUNsYXNzID0gcm91dGVyLm9wdGlvbnMubGlua0V4YWN0QWN0aXZlQ2xhc3M7XG4gICAgLy8gU3VwcG9ydCBnbG9iYWwgZW1wdHkgYWN0aXZlIGNsYXNzXG4gICAgdmFyIGFjdGl2ZUNsYXNzRmFsbGJhY2sgPVxuICAgICAgZ2xvYmFsQWN0aXZlQ2xhc3MgPT0gbnVsbCA/ICdyb3V0ZXItbGluay1hY3RpdmUnIDogZ2xvYmFsQWN0aXZlQ2xhc3M7XG4gICAgdmFyIGV4YWN0QWN0aXZlQ2xhc3NGYWxsYmFjayA9XG4gICAgICBnbG9iYWxFeGFjdEFjdGl2ZUNsYXNzID09IG51bGxcbiAgICAgICAgPyAncm91dGVyLWxpbmstZXhhY3QtYWN0aXZlJ1xuICAgICAgICA6IGdsb2JhbEV4YWN0QWN0aXZlQ2xhc3M7XG4gICAgdmFyIGFjdGl2ZUNsYXNzID1cbiAgICAgIHRoaXMuYWN0aXZlQ2xhc3MgPT0gbnVsbCA/IGFjdGl2ZUNsYXNzRmFsbGJhY2sgOiB0aGlzLmFjdGl2ZUNsYXNzO1xuICAgIHZhciBleGFjdEFjdGl2ZUNsYXNzID1cbiAgICAgIHRoaXMuZXhhY3RBY3RpdmVDbGFzcyA9PSBudWxsXG4gICAgICAgID8gZXhhY3RBY3RpdmVDbGFzc0ZhbGxiYWNrXG4gICAgICAgIDogdGhpcy5leGFjdEFjdGl2ZUNsYXNzO1xuXG4gICAgdmFyIGNvbXBhcmVUYXJnZXQgPSByb3V0ZS5yZWRpcmVjdGVkRnJvbVxuICAgICAgPyBjcmVhdGVSb3V0ZShudWxsLCBub3JtYWxpemVMb2NhdGlvbihyb3V0ZS5yZWRpcmVjdGVkRnJvbSksIG51bGwsIHJvdXRlcilcbiAgICAgIDogcm91dGU7XG5cbiAgICBjbGFzc2VzW2V4YWN0QWN0aXZlQ2xhc3NdID0gaXNTYW1lUm91dGUoY3VycmVudCwgY29tcGFyZVRhcmdldCk7XG4gICAgY2xhc3Nlc1thY3RpdmVDbGFzc10gPSB0aGlzLmV4YWN0XG4gICAgICA/IGNsYXNzZXNbZXhhY3RBY3RpdmVDbGFzc11cbiAgICAgIDogaXNJbmNsdWRlZFJvdXRlKGN1cnJlbnQsIGNvbXBhcmVUYXJnZXQpO1xuXG4gICAgdmFyIGhhbmRsZXIgPSBmdW5jdGlvbiAoZSkge1xuICAgICAgaWYgKGd1YXJkRXZlbnQoZSkpIHtcbiAgICAgICAgaWYgKHRoaXMkMS5yZXBsYWNlKSB7XG4gICAgICAgICAgcm91dGVyLnJlcGxhY2UobG9jYXRpb24sIG5vb3ApO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHJvdXRlci5wdXNoKGxvY2F0aW9uLCBub29wKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH07XG5cbiAgICB2YXIgb24gPSB7IGNsaWNrOiBndWFyZEV2ZW50IH07XG4gICAgaWYgKEFycmF5LmlzQXJyYXkodGhpcy5ldmVudCkpIHtcbiAgICAgIHRoaXMuZXZlbnQuZm9yRWFjaChmdW5jdGlvbiAoZSkge1xuICAgICAgICBvbltlXSA9IGhhbmRsZXI7XG4gICAgICB9KTtcbiAgICB9IGVsc2Uge1xuICAgICAgb25bdGhpcy5ldmVudF0gPSBoYW5kbGVyO1xuICAgIH1cblxuICAgIHZhciBkYXRhID0geyBjbGFzczogY2xhc3NlcyB9O1xuXG4gICAgdmFyIHNjb3BlZFNsb3QgPVxuICAgICAgIXRoaXMuJHNjb3BlZFNsb3RzLiRoYXNOb3JtYWwgJiZcbiAgICAgIHRoaXMuJHNjb3BlZFNsb3RzLmRlZmF1bHQgJiZcbiAgICAgIHRoaXMuJHNjb3BlZFNsb3RzLmRlZmF1bHQoe1xuICAgICAgICBocmVmOiBocmVmLFxuICAgICAgICByb3V0ZTogcm91dGUsXG4gICAgICAgIG5hdmlnYXRlOiBoYW5kbGVyLFxuICAgICAgICBpc0FjdGl2ZTogY2xhc3Nlc1thY3RpdmVDbGFzc10sXG4gICAgICAgIGlzRXhhY3RBY3RpdmU6IGNsYXNzZXNbZXhhY3RBY3RpdmVDbGFzc11cbiAgICAgIH0pO1xuXG4gICAgaWYgKHNjb3BlZFNsb3QpIHtcbiAgICAgIGlmIChzY29wZWRTbG90Lmxlbmd0aCA9PT0gMSkge1xuICAgICAgICByZXR1cm4gc2NvcGVkU2xvdFswXVxuICAgICAgfSBlbHNlIGlmIChzY29wZWRTbG90Lmxlbmd0aCA+IDEgfHwgIXNjb3BlZFNsb3QubGVuZ3RoKSB7XG4gICAgICAgIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XG4gICAgICAgICAgd2FybihcbiAgICAgICAgICAgIGZhbHNlLFxuICAgICAgICAgICAgKFwiUm91dGVyTGluayB3aXRoIHRvPVxcXCJcIiArICh0aGlzLnByb3BzLnRvKSArIFwiXFxcIiBpcyB0cnlpbmcgdG8gdXNlIGEgc2NvcGVkIHNsb3QgYnV0IGl0IGRpZG4ndCBwcm92aWRlIGV4YWN0bHkgb25lIGNoaWxkLlwiKVxuICAgICAgICAgICk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHNjb3BlZFNsb3QubGVuZ3RoID09PSAwID8gaCgpIDogaCgnc3BhbicsIHt9LCBzY29wZWRTbG90KVxuICAgICAgfVxuICAgIH1cblxuICAgIGlmICh0aGlzLnRhZyA9PT0gJ2EnKSB7XG4gICAgICBkYXRhLm9uID0gb247XG4gICAgICBkYXRhLmF0dHJzID0geyBocmVmOiBocmVmIH07XG4gICAgfSBlbHNlIHtcbiAgICAgIC8vIGZpbmQgdGhlIGZpcnN0IDxhPiBjaGlsZCBhbmQgYXBwbHkgbGlzdGVuZXIgYW5kIGhyZWZcbiAgICAgIHZhciBhID0gZmluZEFuY2hvcih0aGlzLiRzbG90cy5kZWZhdWx0KTtcbiAgICAgIGlmIChhKSB7XG4gICAgICAgIC8vIGluIGNhc2UgdGhlIDxhPiBpcyBhIHN0YXRpYyBub2RlXG4gICAgICAgIGEuaXNTdGF0aWMgPSBmYWxzZTtcbiAgICAgICAgdmFyIGFEYXRhID0gKGEuZGF0YSA9IGV4dGVuZCh7fSwgYS5kYXRhKSk7XG4gICAgICAgIGFEYXRhLm9uID0gYURhdGEub24gfHwge307XG4gICAgICAgIC8vIHRyYW5zZm9ybSBleGlzdGluZyBldmVudHMgaW4gYm90aCBvYmplY3RzIGludG8gYXJyYXlzIHNvIHdlIGNhbiBwdXNoIGxhdGVyXG4gICAgICAgIGZvciAodmFyIGV2ZW50IGluIGFEYXRhLm9uKSB7XG4gICAgICAgICAgdmFyIGhhbmRsZXIkMSA9IGFEYXRhLm9uW2V2ZW50XTtcbiAgICAgICAgICBpZiAoZXZlbnQgaW4gb24pIHtcbiAgICAgICAgICAgIGFEYXRhLm9uW2V2ZW50XSA9IEFycmF5LmlzQXJyYXkoaGFuZGxlciQxKSA/IGhhbmRsZXIkMSA6IFtoYW5kbGVyJDFdO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICAvLyBhcHBlbmQgbmV3IGxpc3RlbmVycyBmb3Igcm91dGVyLWxpbmtcbiAgICAgICAgZm9yICh2YXIgZXZlbnQkMSBpbiBvbikge1xuICAgICAgICAgIGlmIChldmVudCQxIGluIGFEYXRhLm9uKSB7XG4gICAgICAgICAgICAvLyBvbltldmVudF0gaXMgYWx3YXlzIGEgZnVuY3Rpb25cbiAgICAgICAgICAgIGFEYXRhLm9uW2V2ZW50JDFdLnB1c2gob25bZXZlbnQkMV0pO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBhRGF0YS5vbltldmVudCQxXSA9IGhhbmRsZXI7XG4gICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgdmFyIGFBdHRycyA9IChhLmRhdGEuYXR0cnMgPSBleHRlbmQoe30sIGEuZGF0YS5hdHRycykpO1xuICAgICAgICBhQXR0cnMuaHJlZiA9IGhyZWY7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICAvLyBkb2Vzbid0IGhhdmUgPGE+IGNoaWxkLCBhcHBseSBsaXN0ZW5lciB0byBzZWxmXG4gICAgICAgIGRhdGEub24gPSBvbjtcbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gaCh0aGlzLnRhZywgZGF0YSwgdGhpcy4kc2xvdHMuZGVmYXVsdClcbiAgfVxufTtcblxuZnVuY3Rpb24gZ3VhcmRFdmVudCAoZSkge1xuICAvLyBkb24ndCByZWRpcmVjdCB3aXRoIGNvbnRyb2wga2V5c1xuICBpZiAoZS5tZXRhS2V5IHx8IGUuYWx0S2V5IHx8IGUuY3RybEtleSB8fCBlLnNoaWZ0S2V5KSB7IHJldHVybiB9XG4gIC8vIGRvbid0IHJlZGlyZWN0IHdoZW4gcHJldmVudERlZmF1bHQgY2FsbGVkXG4gIGlmIChlLmRlZmF1bHRQcmV2ZW50ZWQpIHsgcmV0dXJuIH1cbiAgLy8gZG9uJ3QgcmVkaXJlY3Qgb24gcmlnaHQgY2xpY2tcbiAgaWYgKGUuYnV0dG9uICE9PSB1bmRlZmluZWQgJiYgZS5idXR0b24gIT09IDApIHsgcmV0dXJuIH1cbiAgLy8gZG9uJ3QgcmVkaXJlY3QgaWYgYHRhcmdldD1cIl9ibGFua1wiYFxuICBpZiAoZS5jdXJyZW50VGFyZ2V0ICYmIGUuY3VycmVudFRhcmdldC5nZXRBdHRyaWJ1dGUpIHtcbiAgICB2YXIgdGFyZ2V0ID0gZS5jdXJyZW50VGFyZ2V0LmdldEF0dHJpYnV0ZSgndGFyZ2V0Jyk7XG4gICAgaWYgKC9cXGJfYmxhbmtcXGIvaS50ZXN0KHRhcmdldCkpIHsgcmV0dXJuIH1cbiAgfVxuICAvLyB0aGlzIG1heSBiZSBhIFdlZXggZXZlbnQgd2hpY2ggZG9lc24ndCBoYXZlIHRoaXMgbWV0aG9kXG4gIGlmIChlLnByZXZlbnREZWZhdWx0KSB7XG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICB9XG4gIHJldHVybiB0cnVlXG59XG5cbmZ1bmN0aW9uIGZpbmRBbmNob3IgKGNoaWxkcmVuKSB7XG4gIGlmIChjaGlsZHJlbikge1xuICAgIHZhciBjaGlsZDtcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IGNoaWxkcmVuLmxlbmd0aDsgaSsrKSB7XG4gICAgICBjaGlsZCA9IGNoaWxkcmVuW2ldO1xuICAgICAgaWYgKGNoaWxkLnRhZyA9PT0gJ2EnKSB7XG4gICAgICAgIHJldHVybiBjaGlsZFxuICAgICAgfVxuICAgICAgaWYgKGNoaWxkLmNoaWxkcmVuICYmIChjaGlsZCA9IGZpbmRBbmNob3IoY2hpbGQuY2hpbGRyZW4pKSkge1xuICAgICAgICByZXR1cm4gY2hpbGRcbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cblxudmFyIF9WdWU7XG5cbmZ1bmN0aW9uIGluc3RhbGwgKFZ1ZSkge1xuICBpZiAoaW5zdGFsbC5pbnN0YWxsZWQgJiYgX1Z1ZSA9PT0gVnVlKSB7IHJldHVybiB9XG4gIGluc3RhbGwuaW5zdGFsbGVkID0gdHJ1ZTtcblxuICBfVnVlID0gVnVlO1xuXG4gIHZhciBpc0RlZiA9IGZ1bmN0aW9uICh2KSB7IHJldHVybiB2ICE9PSB1bmRlZmluZWQ7IH07XG5cbiAgdmFyIHJlZ2lzdGVySW5zdGFuY2UgPSBmdW5jdGlvbiAodm0sIGNhbGxWYWwpIHtcbiAgICB2YXIgaSA9IHZtLiRvcHRpb25zLl9wYXJlbnRWbm9kZTtcbiAgICBpZiAoaXNEZWYoaSkgJiYgaXNEZWYoaSA9IGkuZGF0YSkgJiYgaXNEZWYoaSA9IGkucmVnaXN0ZXJSb3V0ZUluc3RhbmNlKSkge1xuICAgICAgaSh2bSwgY2FsbFZhbCk7XG4gICAgfVxuICB9O1xuXG4gIFZ1ZS5taXhpbih7XG4gICAgYmVmb3JlQ3JlYXRlOiBmdW5jdGlvbiBiZWZvcmVDcmVhdGUgKCkge1xuICAgICAgaWYgKGlzRGVmKHRoaXMuJG9wdGlvbnMucm91dGVyKSkge1xuICAgICAgICB0aGlzLl9yb3V0ZXJSb290ID0gdGhpcztcbiAgICAgICAgdGhpcy5fcm91dGVyID0gdGhpcy4kb3B0aW9ucy5yb3V0ZXI7XG4gICAgICAgIHRoaXMuX3JvdXRlci5pbml0KHRoaXMpO1xuICAgICAgICBWdWUudXRpbC5kZWZpbmVSZWFjdGl2ZSh0aGlzLCAnX3JvdXRlJywgdGhpcy5fcm91dGVyLmhpc3RvcnkuY3VycmVudCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLl9yb3V0ZXJSb290ID0gKHRoaXMuJHBhcmVudCAmJiB0aGlzLiRwYXJlbnQuX3JvdXRlclJvb3QpIHx8IHRoaXM7XG4gICAgICB9XG4gICAgICByZWdpc3Rlckluc3RhbmNlKHRoaXMsIHRoaXMpO1xuICAgIH0sXG4gICAgZGVzdHJveWVkOiBmdW5jdGlvbiBkZXN0cm95ZWQgKCkge1xuICAgICAgcmVnaXN0ZXJJbnN0YW5jZSh0aGlzKTtcbiAgICB9XG4gIH0pO1xuXG4gIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShWdWUucHJvdG90eXBlLCAnJHJvdXRlcicsIHtcbiAgICBnZXQ6IGZ1bmN0aW9uIGdldCAoKSB7IHJldHVybiB0aGlzLl9yb3V0ZXJSb290Ll9yb3V0ZXIgfVxuICB9KTtcblxuICBPYmplY3QuZGVmaW5lUHJvcGVydHkoVnVlLnByb3RvdHlwZSwgJyRyb3V0ZScsIHtcbiAgICBnZXQ6IGZ1bmN0aW9uIGdldCAoKSB7IHJldHVybiB0aGlzLl9yb3V0ZXJSb290Ll9yb3V0ZSB9XG4gIH0pO1xuXG4gIFZ1ZS5jb21wb25lbnQoJ1JvdXRlclZpZXcnLCBWaWV3KTtcbiAgVnVlLmNvbXBvbmVudCgnUm91dGVyTGluaycsIExpbmspO1xuXG4gIHZhciBzdHJhdHMgPSBWdWUuY29uZmlnLm9wdGlvbk1lcmdlU3RyYXRlZ2llcztcbiAgLy8gdXNlIHRoZSBzYW1lIGhvb2sgbWVyZ2luZyBzdHJhdGVneSBmb3Igcm91dGUgaG9va3NcbiAgc3RyYXRzLmJlZm9yZVJvdXRlRW50ZXIgPSBzdHJhdHMuYmVmb3JlUm91dGVMZWF2ZSA9IHN0cmF0cy5iZWZvcmVSb3V0ZVVwZGF0ZSA9IHN0cmF0cy5jcmVhdGVkO1xufVxuXG4vKiAgKi9cblxudmFyIGluQnJvd3NlciA9IHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnO1xuXG4vKiAgKi9cblxuZnVuY3Rpb24gY3JlYXRlUm91dGVNYXAgKFxuICByb3V0ZXMsXG4gIG9sZFBhdGhMaXN0LFxuICBvbGRQYXRoTWFwLFxuICBvbGROYW1lTWFwXG4pIHtcbiAgLy8gdGhlIHBhdGggbGlzdCBpcyB1c2VkIHRvIGNvbnRyb2wgcGF0aCBtYXRjaGluZyBwcmlvcml0eVxuICB2YXIgcGF0aExpc3QgPSBvbGRQYXRoTGlzdCB8fCBbXTtcbiAgLy8gJGZsb3ctZGlzYWJsZS1saW5lXG4gIHZhciBwYXRoTWFwID0gb2xkUGF0aE1hcCB8fCBPYmplY3QuY3JlYXRlKG51bGwpO1xuICAvLyAkZmxvdy1kaXNhYmxlLWxpbmVcbiAgdmFyIG5hbWVNYXAgPSBvbGROYW1lTWFwIHx8IE9iamVjdC5jcmVhdGUobnVsbCk7XG5cbiAgcm91dGVzLmZvckVhY2goZnVuY3Rpb24gKHJvdXRlKSB7XG4gICAgYWRkUm91dGVSZWNvcmQocGF0aExpc3QsIHBhdGhNYXAsIG5hbWVNYXAsIHJvdXRlKTtcbiAgfSk7XG5cbiAgLy8gZW5zdXJlIHdpbGRjYXJkIHJvdXRlcyBhcmUgYWx3YXlzIGF0IHRoZSBlbmRcbiAgZm9yICh2YXIgaSA9IDAsIGwgPSBwYXRoTGlzdC5sZW5ndGg7IGkgPCBsOyBpKyspIHtcbiAgICBpZiAocGF0aExpc3RbaV0gPT09ICcqJykge1xuICAgICAgcGF0aExpc3QucHVzaChwYXRoTGlzdC5zcGxpY2UoaSwgMSlbMF0pO1xuICAgICAgbC0tO1xuICAgICAgaS0tO1xuICAgIH1cbiAgfVxuXG4gIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViA9PT0gJ2RldmVsb3BtZW50Jykge1xuICAgIC8vIHdhcm4gaWYgcm91dGVzIGRvIG5vdCBpbmNsdWRlIGxlYWRpbmcgc2xhc2hlc1xuICAgIHZhciBmb3VuZCA9IHBhdGhMaXN0XG4gICAgLy8gY2hlY2sgZm9yIG1pc3NpbmcgbGVhZGluZyBzbGFzaFxuICAgICAgLmZpbHRlcihmdW5jdGlvbiAocGF0aCkgeyByZXR1cm4gcGF0aCAmJiBwYXRoLmNoYXJBdCgwKSAhPT0gJyonICYmIHBhdGguY2hhckF0KDApICE9PSAnLyc7IH0pO1xuXG4gICAgaWYgKGZvdW5kLmxlbmd0aCA+IDApIHtcbiAgICAgIHZhciBwYXRoTmFtZXMgPSBmb3VuZC5tYXAoZnVuY3Rpb24gKHBhdGgpIHsgcmV0dXJuIChcIi0gXCIgKyBwYXRoKTsgfSkuam9pbignXFxuJyk7XG4gICAgICB3YXJuKGZhbHNlLCAoXCJOb24tbmVzdGVkIHJvdXRlcyBtdXN0IGluY2x1ZGUgYSBsZWFkaW5nIHNsYXNoIGNoYXJhY3Rlci4gRml4IHRoZSBmb2xsb3dpbmcgcm91dGVzOiBcXG5cIiArIHBhdGhOYW1lcykpO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiB7XG4gICAgcGF0aExpc3Q6IHBhdGhMaXN0LFxuICAgIHBhdGhNYXA6IHBhdGhNYXAsXG4gICAgbmFtZU1hcDogbmFtZU1hcFxuICB9XG59XG5cbmZ1bmN0aW9uIGFkZFJvdXRlUmVjb3JkIChcbiAgcGF0aExpc3QsXG4gIHBhdGhNYXAsXG4gIG5hbWVNYXAsXG4gIHJvdXRlLFxuICBwYXJlbnQsXG4gIG1hdGNoQXNcbikge1xuICB2YXIgcGF0aCA9IHJvdXRlLnBhdGg7XG4gIHZhciBuYW1lID0gcm91dGUubmFtZTtcbiAgaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcbiAgICBhc3NlcnQocGF0aCAhPSBudWxsLCBcIlxcXCJwYXRoXFxcIiBpcyByZXF1aXJlZCBpbiBhIHJvdXRlIGNvbmZpZ3VyYXRpb24uXCIpO1xuICAgIGFzc2VydChcbiAgICAgIHR5cGVvZiByb3V0ZS5jb21wb25lbnQgIT09ICdzdHJpbmcnLFxuICAgICAgXCJyb3V0ZSBjb25maWcgXFxcImNvbXBvbmVudFxcXCIgZm9yIHBhdGg6IFwiICsgKFN0cmluZyhcbiAgICAgICAgcGF0aCB8fCBuYW1lXG4gICAgICApKSArIFwiIGNhbm5vdCBiZSBhIFwiICsgXCJzdHJpbmcgaWQuIFVzZSBhbiBhY3R1YWwgY29tcG9uZW50IGluc3RlYWQuXCJcbiAgICApO1xuICB9XG5cbiAgdmFyIHBhdGhUb1JlZ2V4cE9wdGlvbnMgPVxuICAgIHJvdXRlLnBhdGhUb1JlZ2V4cE9wdGlvbnMgfHwge307XG4gIHZhciBub3JtYWxpemVkUGF0aCA9IG5vcm1hbGl6ZVBhdGgocGF0aCwgcGFyZW50LCBwYXRoVG9SZWdleHBPcHRpb25zLnN0cmljdCk7XG5cbiAgaWYgKHR5cGVvZiByb3V0ZS5jYXNlU2Vuc2l0aXZlID09PSAnYm9vbGVhbicpIHtcbiAgICBwYXRoVG9SZWdleHBPcHRpb25zLnNlbnNpdGl2ZSA9IHJvdXRlLmNhc2VTZW5zaXRpdmU7XG4gIH1cblxuICB2YXIgcmVjb3JkID0ge1xuICAgIHBhdGg6IG5vcm1hbGl6ZWRQYXRoLFxuICAgIHJlZ2V4OiBjb21waWxlUm91dGVSZWdleChub3JtYWxpemVkUGF0aCwgcGF0aFRvUmVnZXhwT3B0aW9ucyksXG4gICAgY29tcG9uZW50czogcm91dGUuY29tcG9uZW50cyB8fCB7IGRlZmF1bHQ6IHJvdXRlLmNvbXBvbmVudCB9LFxuICAgIGluc3RhbmNlczoge30sXG4gICAgbmFtZTogbmFtZSxcbiAgICBwYXJlbnQ6IHBhcmVudCxcbiAgICBtYXRjaEFzOiBtYXRjaEFzLFxuICAgIHJlZGlyZWN0OiByb3V0ZS5yZWRpcmVjdCxcbiAgICBiZWZvcmVFbnRlcjogcm91dGUuYmVmb3JlRW50ZXIsXG4gICAgbWV0YTogcm91dGUubWV0YSB8fCB7fSxcbiAgICBwcm9wczpcbiAgICAgIHJvdXRlLnByb3BzID09IG51bGxcbiAgICAgICAgPyB7fVxuICAgICAgICA6IHJvdXRlLmNvbXBvbmVudHNcbiAgICAgICAgICA/IHJvdXRlLnByb3BzXG4gICAgICAgICAgOiB7IGRlZmF1bHQ6IHJvdXRlLnByb3BzIH1cbiAgfTtcblxuICBpZiAocm91dGUuY2hpbGRyZW4pIHtcbiAgICAvLyBXYXJuIGlmIHJvdXRlIGlzIG5hbWVkLCBkb2VzIG5vdCByZWRpcmVjdCBhbmQgaGFzIGEgZGVmYXVsdCBjaGlsZCByb3V0ZS5cbiAgICAvLyBJZiB1c2VycyBuYXZpZ2F0ZSB0byB0aGlzIHJvdXRlIGJ5IG5hbWUsIHRoZSBkZWZhdWx0IGNoaWxkIHdpbGxcbiAgICAvLyBub3QgYmUgcmVuZGVyZWQgKEdIIElzc3VlICM2MjkpXG4gICAgaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcbiAgICAgIGlmIChcbiAgICAgICAgcm91dGUubmFtZSAmJlxuICAgICAgICAhcm91dGUucmVkaXJlY3QgJiZcbiAgICAgICAgcm91dGUuY2hpbGRyZW4uc29tZShmdW5jdGlvbiAoY2hpbGQpIHsgcmV0dXJuIC9eXFwvPyQvLnRlc3QoY2hpbGQucGF0aCk7IH0pXG4gICAgICApIHtcbiAgICAgICAgd2FybihcbiAgICAgICAgICBmYWxzZSxcbiAgICAgICAgICBcIk5hbWVkIFJvdXRlICdcIiArIChyb3V0ZS5uYW1lKSArIFwiJyBoYXMgYSBkZWZhdWx0IGNoaWxkIHJvdXRlLiBcIiArXG4gICAgICAgICAgICBcIldoZW4gbmF2aWdhdGluZyB0byB0aGlzIG5hbWVkIHJvdXRlICg6dG89XFxcIntuYW1lOiAnXCIgKyAocm91dGUubmFtZSkgKyBcIidcXFwiKSwgXCIgK1xuICAgICAgICAgICAgXCJ0aGUgZGVmYXVsdCBjaGlsZCByb3V0ZSB3aWxsIG5vdCBiZSByZW5kZXJlZC4gUmVtb3ZlIHRoZSBuYW1lIGZyb20gXCIgK1xuICAgICAgICAgICAgXCJ0aGlzIHJvdXRlIGFuZCB1c2UgdGhlIG5hbWUgb2YgdGhlIGRlZmF1bHQgY2hpbGQgcm91dGUgZm9yIG5hbWVkIFwiICtcbiAgICAgICAgICAgIFwibGlua3MgaW5zdGVhZC5cIlxuICAgICAgICApO1xuICAgICAgfVxuICAgIH1cbiAgICByb3V0ZS5jaGlsZHJlbi5mb3JFYWNoKGZ1bmN0aW9uIChjaGlsZCkge1xuICAgICAgdmFyIGNoaWxkTWF0Y2hBcyA9IG1hdGNoQXNcbiAgICAgICAgPyBjbGVhblBhdGgoKG1hdGNoQXMgKyBcIi9cIiArIChjaGlsZC5wYXRoKSkpXG4gICAgICAgIDogdW5kZWZpbmVkO1xuICAgICAgYWRkUm91dGVSZWNvcmQocGF0aExpc3QsIHBhdGhNYXAsIG5hbWVNYXAsIGNoaWxkLCByZWNvcmQsIGNoaWxkTWF0Y2hBcyk7XG4gICAgfSk7XG4gIH1cblxuICBpZiAoIXBhdGhNYXBbcmVjb3JkLnBhdGhdKSB7XG4gICAgcGF0aExpc3QucHVzaChyZWNvcmQucGF0aCk7XG4gICAgcGF0aE1hcFtyZWNvcmQucGF0aF0gPSByZWNvcmQ7XG4gIH1cblxuICBpZiAocm91dGUuYWxpYXMgIT09IHVuZGVmaW5lZCkge1xuICAgIHZhciBhbGlhc2VzID0gQXJyYXkuaXNBcnJheShyb3V0ZS5hbGlhcykgPyByb3V0ZS5hbGlhcyA6IFtyb3V0ZS5hbGlhc107XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBhbGlhc2VzLmxlbmd0aDsgKytpKSB7XG4gICAgICB2YXIgYWxpYXMgPSBhbGlhc2VzW2ldO1xuICAgICAgaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicgJiYgYWxpYXMgPT09IHBhdGgpIHtcbiAgICAgICAgd2FybihcbiAgICAgICAgICBmYWxzZSxcbiAgICAgICAgICAoXCJGb3VuZCBhbiBhbGlhcyB3aXRoIHRoZSBzYW1lIHZhbHVlIGFzIHRoZSBwYXRoOiBcXFwiXCIgKyBwYXRoICsgXCJcXFwiLiBZb3UgaGF2ZSB0byByZW1vdmUgdGhhdCBhbGlhcy4gSXQgd2lsbCBiZSBpZ25vcmVkIGluIGRldmVsb3BtZW50LlwiKVxuICAgICAgICApO1xuICAgICAgICAvLyBza2lwIGluIGRldiB0byBtYWtlIGl0IHdvcmtcbiAgICAgICAgY29udGludWVcbiAgICAgIH1cblxuICAgICAgdmFyIGFsaWFzUm91dGUgPSB7XG4gICAgICAgIHBhdGg6IGFsaWFzLFxuICAgICAgICBjaGlsZHJlbjogcm91dGUuY2hpbGRyZW5cbiAgICAgIH07XG4gICAgICBhZGRSb3V0ZVJlY29yZChcbiAgICAgICAgcGF0aExpc3QsXG4gICAgICAgIHBhdGhNYXAsXG4gICAgICAgIG5hbWVNYXAsXG4gICAgICAgIGFsaWFzUm91dGUsXG4gICAgICAgIHBhcmVudCxcbiAgICAgICAgcmVjb3JkLnBhdGggfHwgJy8nIC8vIG1hdGNoQXNcbiAgICAgICk7XG4gICAgfVxuICB9XG5cbiAgaWYgKG5hbWUpIHtcbiAgICBpZiAoIW5hbWVNYXBbbmFtZV0pIHtcbiAgICAgIG5hbWVNYXBbbmFtZV0gPSByZWNvcmQ7XG4gICAgfSBlbHNlIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nICYmICFtYXRjaEFzKSB7XG4gICAgICB3YXJuKFxuICAgICAgICBmYWxzZSxcbiAgICAgICAgXCJEdXBsaWNhdGUgbmFtZWQgcm91dGVzIGRlZmluaXRpb246IFwiICtcbiAgICAgICAgICBcInsgbmFtZTogXFxcIlwiICsgbmFtZSArIFwiXFxcIiwgcGF0aDogXFxcIlwiICsgKHJlY29yZC5wYXRoKSArIFwiXFxcIiB9XCJcbiAgICAgICk7XG4gICAgfVxuICB9XG59XG5cbmZ1bmN0aW9uIGNvbXBpbGVSb3V0ZVJlZ2V4IChcbiAgcGF0aCxcbiAgcGF0aFRvUmVnZXhwT3B0aW9uc1xuKSB7XG4gIHZhciByZWdleCA9IHBhdGhUb1JlZ2V4cF8xKHBhdGgsIFtdLCBwYXRoVG9SZWdleHBPcHRpb25zKTtcbiAgaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcbiAgICB2YXIga2V5cyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gICAgcmVnZXgua2V5cy5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgIHdhcm4oXG4gICAgICAgICFrZXlzW2tleS5uYW1lXSxcbiAgICAgICAgKFwiRHVwbGljYXRlIHBhcmFtIGtleXMgaW4gcm91dGUgd2l0aCBwYXRoOiBcXFwiXCIgKyBwYXRoICsgXCJcXFwiXCIpXG4gICAgICApO1xuICAgICAga2V5c1trZXkubmFtZV0gPSB0cnVlO1xuICAgIH0pO1xuICB9XG4gIHJldHVybiByZWdleFxufVxuXG5mdW5jdGlvbiBub3JtYWxpemVQYXRoIChcbiAgcGF0aCxcbiAgcGFyZW50LFxuICBzdHJpY3Rcbikge1xuICBpZiAoIXN0cmljdCkgeyBwYXRoID0gcGF0aC5yZXBsYWNlKC9cXC8kLywgJycpOyB9XG4gIGlmIChwYXRoWzBdID09PSAnLycpIHsgcmV0dXJuIHBhdGggfVxuICBpZiAocGFyZW50ID09IG51bGwpIHsgcmV0dXJuIHBhdGggfVxuICByZXR1cm4gY2xlYW5QYXRoKCgocGFyZW50LnBhdGgpICsgXCIvXCIgKyBwYXRoKSlcbn1cblxuLyogICovXG5cblxuXG5mdW5jdGlvbiBjcmVhdGVNYXRjaGVyIChcbiAgcm91dGVzLFxuICByb3V0ZXJcbikge1xuICB2YXIgcmVmID0gY3JlYXRlUm91dGVNYXAocm91dGVzKTtcbiAgdmFyIHBhdGhMaXN0ID0gcmVmLnBhdGhMaXN0O1xuICB2YXIgcGF0aE1hcCA9IHJlZi5wYXRoTWFwO1xuICB2YXIgbmFtZU1hcCA9IHJlZi5uYW1lTWFwO1xuXG4gIGZ1bmN0aW9uIGFkZFJvdXRlcyAocm91dGVzKSB7XG4gICAgY3JlYXRlUm91dGVNYXAocm91dGVzLCBwYXRoTGlzdCwgcGF0aE1hcCwgbmFtZU1hcCk7XG4gIH1cblxuICBmdW5jdGlvbiBtYXRjaCAoXG4gICAgcmF3LFxuICAgIGN1cnJlbnRSb3V0ZSxcbiAgICByZWRpcmVjdGVkRnJvbVxuICApIHtcbiAgICB2YXIgbG9jYXRpb24gPSBub3JtYWxpemVMb2NhdGlvbihyYXcsIGN1cnJlbnRSb3V0ZSwgZmFsc2UsIHJvdXRlcik7XG4gICAgdmFyIG5hbWUgPSBsb2NhdGlvbi5uYW1lO1xuXG4gICAgaWYgKG5hbWUpIHtcbiAgICAgIHZhciByZWNvcmQgPSBuYW1lTWFwW25hbWVdO1xuICAgICAgaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcbiAgICAgICAgd2FybihyZWNvcmQsIChcIlJvdXRlIHdpdGggbmFtZSAnXCIgKyBuYW1lICsgXCInIGRvZXMgbm90IGV4aXN0XCIpKTtcbiAgICAgIH1cbiAgICAgIGlmICghcmVjb3JkKSB7IHJldHVybiBfY3JlYXRlUm91dGUobnVsbCwgbG9jYXRpb24pIH1cbiAgICAgIHZhciBwYXJhbU5hbWVzID0gcmVjb3JkLnJlZ2V4LmtleXNcbiAgICAgICAgLmZpbHRlcihmdW5jdGlvbiAoa2V5KSB7IHJldHVybiAha2V5Lm9wdGlvbmFsOyB9KVxuICAgICAgICAubWFwKGZ1bmN0aW9uIChrZXkpIHsgcmV0dXJuIGtleS5uYW1lOyB9KTtcblxuICAgICAgaWYgKHR5cGVvZiBsb2NhdGlvbi5wYXJhbXMgIT09ICdvYmplY3QnKSB7XG4gICAgICAgIGxvY2F0aW9uLnBhcmFtcyA9IHt9O1xuICAgICAgfVxuXG4gICAgICBpZiAoY3VycmVudFJvdXRlICYmIHR5cGVvZiBjdXJyZW50Um91dGUucGFyYW1zID09PSAnb2JqZWN0Jykge1xuICAgICAgICBmb3IgKHZhciBrZXkgaW4gY3VycmVudFJvdXRlLnBhcmFtcykge1xuICAgICAgICAgIGlmICghKGtleSBpbiBsb2NhdGlvbi5wYXJhbXMpICYmIHBhcmFtTmFtZXMuaW5kZXhPZihrZXkpID4gLTEpIHtcbiAgICAgICAgICAgIGxvY2F0aW9uLnBhcmFtc1trZXldID0gY3VycmVudFJvdXRlLnBhcmFtc1trZXldO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBsb2NhdGlvbi5wYXRoID0gZmlsbFBhcmFtcyhyZWNvcmQucGF0aCwgbG9jYXRpb24ucGFyYW1zLCAoXCJuYW1lZCByb3V0ZSBcXFwiXCIgKyBuYW1lICsgXCJcXFwiXCIpKTtcbiAgICAgIHJldHVybiBfY3JlYXRlUm91dGUocmVjb3JkLCBsb2NhdGlvbiwgcmVkaXJlY3RlZEZyb20pXG4gICAgfSBlbHNlIGlmIChsb2NhdGlvbi5wYXRoKSB7XG4gICAgICBsb2NhdGlvbi5wYXJhbXMgPSB7fTtcbiAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgcGF0aExpc3QubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgdmFyIHBhdGggPSBwYXRoTGlzdFtpXTtcbiAgICAgICAgdmFyIHJlY29yZCQxID0gcGF0aE1hcFtwYXRoXTtcbiAgICAgICAgaWYgKG1hdGNoUm91dGUocmVjb3JkJDEucmVnZXgsIGxvY2F0aW9uLnBhdGgsIGxvY2F0aW9uLnBhcmFtcykpIHtcbiAgICAgICAgICByZXR1cm4gX2NyZWF0ZVJvdXRlKHJlY29yZCQxLCBsb2NhdGlvbiwgcmVkaXJlY3RlZEZyb20pXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gICAgLy8gbm8gbWF0Y2hcbiAgICByZXR1cm4gX2NyZWF0ZVJvdXRlKG51bGwsIGxvY2F0aW9uKVxuICB9XG5cbiAgZnVuY3Rpb24gcmVkaXJlY3QgKFxuICAgIHJlY29yZCxcbiAgICBsb2NhdGlvblxuICApIHtcbiAgICB2YXIgb3JpZ2luYWxSZWRpcmVjdCA9IHJlY29yZC5yZWRpcmVjdDtcbiAgICB2YXIgcmVkaXJlY3QgPSB0eXBlb2Ygb3JpZ2luYWxSZWRpcmVjdCA9PT0gJ2Z1bmN0aW9uJ1xuICAgICAgPyBvcmlnaW5hbFJlZGlyZWN0KGNyZWF0ZVJvdXRlKHJlY29yZCwgbG9jYXRpb24sIG51bGwsIHJvdXRlcikpXG4gICAgICA6IG9yaWdpbmFsUmVkaXJlY3Q7XG5cbiAgICBpZiAodHlwZW9mIHJlZGlyZWN0ID09PSAnc3RyaW5nJykge1xuICAgICAgcmVkaXJlY3QgPSB7IHBhdGg6IHJlZGlyZWN0IH07XG4gICAgfVxuXG4gICAgaWYgKCFyZWRpcmVjdCB8fCB0eXBlb2YgcmVkaXJlY3QgIT09ICdvYmplY3QnKSB7XG4gICAgICBpZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJykge1xuICAgICAgICB3YXJuKFxuICAgICAgICAgIGZhbHNlLCAoXCJpbnZhbGlkIHJlZGlyZWN0IG9wdGlvbjogXCIgKyAoSlNPTi5zdHJpbmdpZnkocmVkaXJlY3QpKSlcbiAgICAgICAgKTtcbiAgICAgIH1cbiAgICAgIHJldHVybiBfY3JlYXRlUm91dGUobnVsbCwgbG9jYXRpb24pXG4gICAgfVxuXG4gICAgdmFyIHJlID0gcmVkaXJlY3Q7XG4gICAgdmFyIG5hbWUgPSByZS5uYW1lO1xuICAgIHZhciBwYXRoID0gcmUucGF0aDtcbiAgICB2YXIgcXVlcnkgPSBsb2NhdGlvbi5xdWVyeTtcbiAgICB2YXIgaGFzaCA9IGxvY2F0aW9uLmhhc2g7XG4gICAgdmFyIHBhcmFtcyA9IGxvY2F0aW9uLnBhcmFtcztcbiAgICBxdWVyeSA9IHJlLmhhc093blByb3BlcnR5KCdxdWVyeScpID8gcmUucXVlcnkgOiBxdWVyeTtcbiAgICBoYXNoID0gcmUuaGFzT3duUHJvcGVydHkoJ2hhc2gnKSA/IHJlLmhhc2ggOiBoYXNoO1xuICAgIHBhcmFtcyA9IHJlLmhhc093blByb3BlcnR5KCdwYXJhbXMnKSA/IHJlLnBhcmFtcyA6IHBhcmFtcztcblxuICAgIGlmIChuYW1lKSB7XG4gICAgICAvLyByZXNvbHZlZCBuYW1lZCBkaXJlY3RcbiAgICAgIHZhciB0YXJnZXRSZWNvcmQgPSBuYW1lTWFwW25hbWVdO1xuICAgICAgaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcbiAgICAgICAgYXNzZXJ0KHRhcmdldFJlY29yZCwgKFwicmVkaXJlY3QgZmFpbGVkOiBuYW1lZCByb3V0ZSBcXFwiXCIgKyBuYW1lICsgXCJcXFwiIG5vdCBmb3VuZC5cIikpO1xuICAgICAgfVxuICAgICAgcmV0dXJuIG1hdGNoKHtcbiAgICAgICAgX25vcm1hbGl6ZWQ6IHRydWUsXG4gICAgICAgIG5hbWU6IG5hbWUsXG4gICAgICAgIHF1ZXJ5OiBxdWVyeSxcbiAgICAgICAgaGFzaDogaGFzaCxcbiAgICAgICAgcGFyYW1zOiBwYXJhbXNcbiAgICAgIH0sIHVuZGVmaW5lZCwgbG9jYXRpb24pXG4gICAgfSBlbHNlIGlmIChwYXRoKSB7XG4gICAgICAvLyAxLiByZXNvbHZlIHJlbGF0aXZlIHJlZGlyZWN0XG4gICAgICB2YXIgcmF3UGF0aCA9IHJlc29sdmVSZWNvcmRQYXRoKHBhdGgsIHJlY29yZCk7XG4gICAgICAvLyAyLiByZXNvbHZlIHBhcmFtc1xuICAgICAgdmFyIHJlc29sdmVkUGF0aCA9IGZpbGxQYXJhbXMocmF3UGF0aCwgcGFyYW1zLCAoXCJyZWRpcmVjdCByb3V0ZSB3aXRoIHBhdGggXFxcIlwiICsgcmF3UGF0aCArIFwiXFxcIlwiKSk7XG4gICAgICAvLyAzLiByZW1hdGNoIHdpdGggZXhpc3RpbmcgcXVlcnkgYW5kIGhhc2hcbiAgICAgIHJldHVybiBtYXRjaCh7XG4gICAgICAgIF9ub3JtYWxpemVkOiB0cnVlLFxuICAgICAgICBwYXRoOiByZXNvbHZlZFBhdGgsXG4gICAgICAgIHF1ZXJ5OiBxdWVyeSxcbiAgICAgICAgaGFzaDogaGFzaFxuICAgICAgfSwgdW5kZWZpbmVkLCBsb2NhdGlvbilcbiAgICB9IGVsc2Uge1xuICAgICAgaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcbiAgICAgICAgd2FybihmYWxzZSwgKFwiaW52YWxpZCByZWRpcmVjdCBvcHRpb246IFwiICsgKEpTT04uc3RyaW5naWZ5KHJlZGlyZWN0KSkpKTtcbiAgICAgIH1cbiAgICAgIHJldHVybiBfY3JlYXRlUm91dGUobnVsbCwgbG9jYXRpb24pXG4gICAgfVxuICB9XG5cbiAgZnVuY3Rpb24gYWxpYXMgKFxuICAgIHJlY29yZCxcbiAgICBsb2NhdGlvbixcbiAgICBtYXRjaEFzXG4gICkge1xuICAgIHZhciBhbGlhc2VkUGF0aCA9IGZpbGxQYXJhbXMobWF0Y2hBcywgbG9jYXRpb24ucGFyYW1zLCAoXCJhbGlhc2VkIHJvdXRlIHdpdGggcGF0aCBcXFwiXCIgKyBtYXRjaEFzICsgXCJcXFwiXCIpKTtcbiAgICB2YXIgYWxpYXNlZE1hdGNoID0gbWF0Y2goe1xuICAgICAgX25vcm1hbGl6ZWQ6IHRydWUsXG4gICAgICBwYXRoOiBhbGlhc2VkUGF0aFxuICAgIH0pO1xuICAgIGlmIChhbGlhc2VkTWF0Y2gpIHtcbiAgICAgIHZhciBtYXRjaGVkID0gYWxpYXNlZE1hdGNoLm1hdGNoZWQ7XG4gICAgICB2YXIgYWxpYXNlZFJlY29yZCA9IG1hdGNoZWRbbWF0Y2hlZC5sZW5ndGggLSAxXTtcbiAgICAgIGxvY2F0aW9uLnBhcmFtcyA9IGFsaWFzZWRNYXRjaC5wYXJhbXM7XG4gICAgICByZXR1cm4gX2NyZWF0ZVJvdXRlKGFsaWFzZWRSZWNvcmQsIGxvY2F0aW9uKVxuICAgIH1cbiAgICByZXR1cm4gX2NyZWF0ZVJvdXRlKG51bGwsIGxvY2F0aW9uKVxuICB9XG5cbiAgZnVuY3Rpb24gX2NyZWF0ZVJvdXRlIChcbiAgICByZWNvcmQsXG4gICAgbG9jYXRpb24sXG4gICAgcmVkaXJlY3RlZEZyb21cbiAgKSB7XG4gICAgaWYgKHJlY29yZCAmJiByZWNvcmQucmVkaXJlY3QpIHtcbiAgICAgIHJldHVybiByZWRpcmVjdChyZWNvcmQsIHJlZGlyZWN0ZWRGcm9tIHx8IGxvY2F0aW9uKVxuICAgIH1cbiAgICBpZiAocmVjb3JkICYmIHJlY29yZC5tYXRjaEFzKSB7XG4gICAgICByZXR1cm4gYWxpYXMocmVjb3JkLCBsb2NhdGlvbiwgcmVjb3JkLm1hdGNoQXMpXG4gICAgfVxuICAgIHJldHVybiBjcmVhdGVSb3V0ZShyZWNvcmQsIGxvY2F0aW9uLCByZWRpcmVjdGVkRnJvbSwgcm91dGVyKVxuICB9XG5cbiAgcmV0dXJuIHtcbiAgICBtYXRjaDogbWF0Y2gsXG4gICAgYWRkUm91dGVzOiBhZGRSb3V0ZXNcbiAgfVxufVxuXG5mdW5jdGlvbiBtYXRjaFJvdXRlIChcbiAgcmVnZXgsXG4gIHBhdGgsXG4gIHBhcmFtc1xuKSB7XG4gIHZhciBtID0gcGF0aC5tYXRjaChyZWdleCk7XG5cbiAgaWYgKCFtKSB7XG4gICAgcmV0dXJuIGZhbHNlXG4gIH0gZWxzZSBpZiAoIXBhcmFtcykge1xuICAgIHJldHVybiB0cnVlXG4gIH1cblxuICBmb3IgKHZhciBpID0gMSwgbGVuID0gbS5sZW5ndGg7IGkgPCBsZW47ICsraSkge1xuICAgIHZhciBrZXkgPSByZWdleC5rZXlzW2kgLSAxXTtcbiAgICB2YXIgdmFsID0gdHlwZW9mIG1baV0gPT09ICdzdHJpbmcnID8gZGVjb2RlVVJJQ29tcG9uZW50KG1baV0pIDogbVtpXTtcbiAgICBpZiAoa2V5KSB7XG4gICAgICAvLyBGaXggIzE5OTQ6IHVzaW5nICogd2l0aCBwcm9wczogdHJ1ZSBnZW5lcmF0ZXMgYSBwYXJhbSBuYW1lZCAwXG4gICAgICBwYXJhbXNba2V5Lm5hbWUgfHwgJ3BhdGhNYXRjaCddID0gdmFsO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiB0cnVlXG59XG5cbmZ1bmN0aW9uIHJlc29sdmVSZWNvcmRQYXRoIChwYXRoLCByZWNvcmQpIHtcbiAgcmV0dXJuIHJlc29sdmVQYXRoKHBhdGgsIHJlY29yZC5wYXJlbnQgPyByZWNvcmQucGFyZW50LnBhdGggOiAnLycsIHRydWUpXG59XG5cbi8qICAqL1xuXG4vLyB1c2UgVXNlciBUaW1pbmcgYXBpIChpZiBwcmVzZW50KSBmb3IgbW9yZSBhY2N1cmF0ZSBrZXkgcHJlY2lzaW9uXG52YXIgVGltZSA9XG4gIGluQnJvd3NlciAmJiB3aW5kb3cucGVyZm9ybWFuY2UgJiYgd2luZG93LnBlcmZvcm1hbmNlLm5vd1xuICAgID8gd2luZG93LnBlcmZvcm1hbmNlXG4gICAgOiBEYXRlO1xuXG5mdW5jdGlvbiBnZW5TdGF0ZUtleSAoKSB7XG4gIHJldHVybiBUaW1lLm5vdygpLnRvRml4ZWQoMylcbn1cblxudmFyIF9rZXkgPSBnZW5TdGF0ZUtleSgpO1xuXG5mdW5jdGlvbiBnZXRTdGF0ZUtleSAoKSB7XG4gIHJldHVybiBfa2V5XG59XG5cbmZ1bmN0aW9uIHNldFN0YXRlS2V5IChrZXkpIHtcbiAgcmV0dXJuIChfa2V5ID0ga2V5KVxufVxuXG4vKiAgKi9cblxudmFyIHBvc2l0aW9uU3RvcmUgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuXG5mdW5jdGlvbiBzZXR1cFNjcm9sbCAoKSB7XG4gIC8vIEZpeCBmb3IgIzE1ODUgZm9yIEZpcmVmb3hcbiAgLy8gRml4IGZvciAjMjE5NSBBZGQgb3B0aW9uYWwgdGhpcmQgYXR0cmlidXRlIHRvIHdvcmthcm91bmQgYSBidWcgaW4gc2FmYXJpIGh0dHBzOi8vYnVncy53ZWJraXQub3JnL3Nob3dfYnVnLmNnaT9pZD0xODI2NzhcbiAgLy8gRml4IGZvciAjMjc3NCBTdXBwb3J0IGZvciBhcHBzIGxvYWRlZCBmcm9tIFdpbmRvd3MgZmlsZSBzaGFyZXMgbm90IG1hcHBlZCB0byBuZXR3b3JrIGRyaXZlczogcmVwbGFjZWQgbG9jYXRpb24ub3JpZ2luIHdpdGhcbiAgLy8gd2luZG93LmxvY2F0aW9uLnByb3RvY29sICsgJy8vJyArIHdpbmRvdy5sb2NhdGlvbi5ob3N0XG4gIC8vIGxvY2F0aW9uLmhvc3QgY29udGFpbnMgdGhlIHBvcnQgYW5kIGxvY2F0aW9uLmhvc3RuYW1lIGRvZXNuJ3RcbiAgdmFyIHByb3RvY29sQW5kUGF0aCA9IHdpbmRvdy5sb2NhdGlvbi5wcm90b2NvbCArICcvLycgKyB3aW5kb3cubG9jYXRpb24uaG9zdDtcbiAgdmFyIGFic29sdXRlUGF0aCA9IHdpbmRvdy5sb2NhdGlvbi5ocmVmLnJlcGxhY2UocHJvdG9jb2xBbmRQYXRoLCAnJyk7XG4gIHdpbmRvdy5oaXN0b3J5LnJlcGxhY2VTdGF0ZSh7IGtleTogZ2V0U3RhdGVLZXkoKSB9LCAnJywgYWJzb2x1dGVQYXRoKTtcbiAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ3BvcHN0YXRlJywgZnVuY3Rpb24gKGUpIHtcbiAgICBzYXZlU2Nyb2xsUG9zaXRpb24oKTtcbiAgICBpZiAoZS5zdGF0ZSAmJiBlLnN0YXRlLmtleSkge1xuICAgICAgc2V0U3RhdGVLZXkoZS5zdGF0ZS5rZXkpO1xuICAgIH1cbiAgfSk7XG59XG5cbmZ1bmN0aW9uIGhhbmRsZVNjcm9sbCAoXG4gIHJvdXRlcixcbiAgdG8sXG4gIGZyb20sXG4gIGlzUG9wXG4pIHtcbiAgaWYgKCFyb3V0ZXIuYXBwKSB7XG4gICAgcmV0dXJuXG4gIH1cblxuICB2YXIgYmVoYXZpb3IgPSByb3V0ZXIub3B0aW9ucy5zY3JvbGxCZWhhdmlvcjtcbiAgaWYgKCFiZWhhdmlvcikge1xuICAgIHJldHVyblxuICB9XG5cbiAgaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcbiAgICBhc3NlcnQodHlwZW9mIGJlaGF2aW9yID09PSAnZnVuY3Rpb24nLCBcInNjcm9sbEJlaGF2aW9yIG11c3QgYmUgYSBmdW5jdGlvblwiKTtcbiAgfVxuXG4gIC8vIHdhaXQgdW50aWwgcmUtcmVuZGVyIGZpbmlzaGVzIGJlZm9yZSBzY3JvbGxpbmdcbiAgcm91dGVyLmFwcC4kbmV4dFRpY2soZnVuY3Rpb24gKCkge1xuICAgIHZhciBwb3NpdGlvbiA9IGdldFNjcm9sbFBvc2l0aW9uKCk7XG4gICAgdmFyIHNob3VsZFNjcm9sbCA9IGJlaGF2aW9yLmNhbGwoXG4gICAgICByb3V0ZXIsXG4gICAgICB0byxcbiAgICAgIGZyb20sXG4gICAgICBpc1BvcCA/IHBvc2l0aW9uIDogbnVsbFxuICAgICk7XG5cbiAgICBpZiAoIXNob3VsZFNjcm9sbCkge1xuICAgICAgcmV0dXJuXG4gICAgfVxuXG4gICAgaWYgKHR5cGVvZiBzaG91bGRTY3JvbGwudGhlbiA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgc2hvdWxkU2Nyb2xsXG4gICAgICAgIC50aGVuKGZ1bmN0aW9uIChzaG91bGRTY3JvbGwpIHtcbiAgICAgICAgICBzY3JvbGxUb1Bvc2l0aW9uKChzaG91bGRTY3JvbGwpLCBwb3NpdGlvbik7XG4gICAgICAgIH0pXG4gICAgICAgIC5jYXRjaChmdW5jdGlvbiAoZXJyKSB7XG4gICAgICAgICAgaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcbiAgICAgICAgICAgIGFzc2VydChmYWxzZSwgZXJyLnRvU3RyaW5nKCkpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHNjcm9sbFRvUG9zaXRpb24oc2hvdWxkU2Nyb2xsLCBwb3NpdGlvbik7XG4gICAgfVxuICB9KTtcbn1cblxuZnVuY3Rpb24gc2F2ZVNjcm9sbFBvc2l0aW9uICgpIHtcbiAgdmFyIGtleSA9IGdldFN0YXRlS2V5KCk7XG4gIGlmIChrZXkpIHtcbiAgICBwb3NpdGlvblN0b3JlW2tleV0gPSB7XG4gICAgICB4OiB3aW5kb3cucGFnZVhPZmZzZXQsXG4gICAgICB5OiB3aW5kb3cucGFnZVlPZmZzZXRcbiAgICB9O1xuICB9XG59XG5cbmZ1bmN0aW9uIGdldFNjcm9sbFBvc2l0aW9uICgpIHtcbiAgdmFyIGtleSA9IGdldFN0YXRlS2V5KCk7XG4gIGlmIChrZXkpIHtcbiAgICByZXR1cm4gcG9zaXRpb25TdG9yZVtrZXldXG4gIH1cbn1cblxuZnVuY3Rpb24gZ2V0RWxlbWVudFBvc2l0aW9uIChlbCwgb2Zmc2V0KSB7XG4gIHZhciBkb2NFbCA9IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudDtcbiAgdmFyIGRvY1JlY3QgPSBkb2NFbC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKTtcbiAgdmFyIGVsUmVjdCA9IGVsLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpO1xuICByZXR1cm4ge1xuICAgIHg6IGVsUmVjdC5sZWZ0IC0gZG9jUmVjdC5sZWZ0IC0gb2Zmc2V0LngsXG4gICAgeTogZWxSZWN0LnRvcCAtIGRvY1JlY3QudG9wIC0gb2Zmc2V0LnlcbiAgfVxufVxuXG5mdW5jdGlvbiBpc1ZhbGlkUG9zaXRpb24gKG9iaikge1xuICByZXR1cm4gaXNOdW1iZXIob2JqLngpIHx8IGlzTnVtYmVyKG9iai55KVxufVxuXG5mdW5jdGlvbiBub3JtYWxpemVQb3NpdGlvbiAob2JqKSB7XG4gIHJldHVybiB7XG4gICAgeDogaXNOdW1iZXIob2JqLngpID8gb2JqLnggOiB3aW5kb3cucGFnZVhPZmZzZXQsXG4gICAgeTogaXNOdW1iZXIob2JqLnkpID8gb2JqLnkgOiB3aW5kb3cucGFnZVlPZmZzZXRcbiAgfVxufVxuXG5mdW5jdGlvbiBub3JtYWxpemVPZmZzZXQgKG9iaikge1xuICByZXR1cm4ge1xuICAgIHg6IGlzTnVtYmVyKG9iai54KSA/IG9iai54IDogMCxcbiAgICB5OiBpc051bWJlcihvYmoueSkgPyBvYmoueSA6IDBcbiAgfVxufVxuXG5mdW5jdGlvbiBpc051bWJlciAodikge1xuICByZXR1cm4gdHlwZW9mIHYgPT09ICdudW1iZXInXG59XG5cbnZhciBoYXNoU3RhcnRzV2l0aE51bWJlclJFID0gL14jXFxkLztcblxuZnVuY3Rpb24gc2Nyb2xsVG9Qb3NpdGlvbiAoc2hvdWxkU2Nyb2xsLCBwb3NpdGlvbikge1xuICB2YXIgaXNPYmplY3QgPSB0eXBlb2Ygc2hvdWxkU2Nyb2xsID09PSAnb2JqZWN0JztcbiAgaWYgKGlzT2JqZWN0ICYmIHR5cGVvZiBzaG91bGRTY3JvbGwuc2VsZWN0b3IgPT09ICdzdHJpbmcnKSB7XG4gICAgLy8gZ2V0RWxlbWVudEJ5SWQgd291bGQgc3RpbGwgZmFpbCBpZiB0aGUgc2VsZWN0b3IgY29udGFpbnMgYSBtb3JlIGNvbXBsaWNhdGVkIHF1ZXJ5IGxpa2UgI21haW5bZGF0YS1hdHRyXVxuICAgIC8vIGJ1dCBhdCB0aGUgc2FtZSB0aW1lLCBpdCBkb2Vzbid0IG1ha2UgbXVjaCBzZW5zZSB0byBzZWxlY3QgYW4gZWxlbWVudCB3aXRoIGFuIGlkIGFuZCBhbiBleHRyYSBzZWxlY3RvclxuICAgIHZhciBlbCA9IGhhc2hTdGFydHNXaXRoTnVtYmVyUkUudGVzdChzaG91bGRTY3JvbGwuc2VsZWN0b3IpIC8vICRmbG93LWRpc2FibGUtbGluZVxuICAgICAgPyBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChzaG91bGRTY3JvbGwuc2VsZWN0b3Iuc2xpY2UoMSkpIC8vICRmbG93LWRpc2FibGUtbGluZVxuICAgICAgOiBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKHNob3VsZFNjcm9sbC5zZWxlY3Rvcik7XG5cbiAgICBpZiAoZWwpIHtcbiAgICAgIHZhciBvZmZzZXQgPVxuICAgICAgICBzaG91bGRTY3JvbGwub2Zmc2V0ICYmIHR5cGVvZiBzaG91bGRTY3JvbGwub2Zmc2V0ID09PSAnb2JqZWN0J1xuICAgICAgICAgID8gc2hvdWxkU2Nyb2xsLm9mZnNldFxuICAgICAgICAgIDoge307XG4gICAgICBvZmZzZXQgPSBub3JtYWxpemVPZmZzZXQob2Zmc2V0KTtcbiAgICAgIHBvc2l0aW9uID0gZ2V0RWxlbWVudFBvc2l0aW9uKGVsLCBvZmZzZXQpO1xuICAgIH0gZWxzZSBpZiAoaXNWYWxpZFBvc2l0aW9uKHNob3VsZFNjcm9sbCkpIHtcbiAgICAgIHBvc2l0aW9uID0gbm9ybWFsaXplUG9zaXRpb24oc2hvdWxkU2Nyb2xsKTtcbiAgICB9XG4gIH0gZWxzZSBpZiAoaXNPYmplY3QgJiYgaXNWYWxpZFBvc2l0aW9uKHNob3VsZFNjcm9sbCkpIHtcbiAgICBwb3NpdGlvbiA9IG5vcm1hbGl6ZVBvc2l0aW9uKHNob3VsZFNjcm9sbCk7XG4gIH1cblxuICBpZiAocG9zaXRpb24pIHtcbiAgICB3aW5kb3cuc2Nyb2xsVG8ocG9zaXRpb24ueCwgcG9zaXRpb24ueSk7XG4gIH1cbn1cblxuLyogICovXG5cbnZhciBzdXBwb3J0c1B1c2hTdGF0ZSA9XG4gIGluQnJvd3NlciAmJlxuICAoZnVuY3Rpb24gKCkge1xuICAgIHZhciB1YSA9IHdpbmRvdy5uYXZpZ2F0b3IudXNlckFnZW50O1xuXG4gICAgaWYgKFxuICAgICAgKHVhLmluZGV4T2YoJ0FuZHJvaWQgMi4nKSAhPT0gLTEgfHwgdWEuaW5kZXhPZignQW5kcm9pZCA0LjAnKSAhPT0gLTEpICYmXG4gICAgICB1YS5pbmRleE9mKCdNb2JpbGUgU2FmYXJpJykgIT09IC0xICYmXG4gICAgICB1YS5pbmRleE9mKCdDaHJvbWUnKSA9PT0gLTEgJiZcbiAgICAgIHVhLmluZGV4T2YoJ1dpbmRvd3MgUGhvbmUnKSA9PT0gLTFcbiAgICApIHtcbiAgICAgIHJldHVybiBmYWxzZVxuICAgIH1cblxuICAgIHJldHVybiB3aW5kb3cuaGlzdG9yeSAmJiAncHVzaFN0YXRlJyBpbiB3aW5kb3cuaGlzdG9yeVxuICB9KSgpO1xuXG5mdW5jdGlvbiBwdXNoU3RhdGUgKHVybCwgcmVwbGFjZSkge1xuICBzYXZlU2Nyb2xsUG9zaXRpb24oKTtcbiAgLy8gdHJ5Li4uY2F0Y2ggdGhlIHB1c2hTdGF0ZSBjYWxsIHRvIGdldCBhcm91bmQgU2FmYXJpXG4gIC8vIERPTSBFeGNlcHRpb24gMTggd2hlcmUgaXQgbGltaXRzIHRvIDEwMCBwdXNoU3RhdGUgY2FsbHNcbiAgdmFyIGhpc3RvcnkgPSB3aW5kb3cuaGlzdG9yeTtcbiAgdHJ5IHtcbiAgICBpZiAocmVwbGFjZSkge1xuICAgICAgaGlzdG9yeS5yZXBsYWNlU3RhdGUoeyBrZXk6IGdldFN0YXRlS2V5KCkgfSwgJycsIHVybCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGhpc3RvcnkucHVzaFN0YXRlKHsga2V5OiBzZXRTdGF0ZUtleShnZW5TdGF0ZUtleSgpKSB9LCAnJywgdXJsKTtcbiAgICB9XG4gIH0gY2F0Y2ggKGUpIHtcbiAgICB3aW5kb3cubG9jYXRpb25bcmVwbGFjZSA/ICdyZXBsYWNlJyA6ICdhc3NpZ24nXSh1cmwpO1xuICB9XG59XG5cbmZ1bmN0aW9uIHJlcGxhY2VTdGF0ZSAodXJsKSB7XG4gIHB1c2hTdGF0ZSh1cmwsIHRydWUpO1xufVxuXG4vKiAgKi9cblxuZnVuY3Rpb24gcnVuUXVldWUgKHF1ZXVlLCBmbiwgY2IpIHtcbiAgdmFyIHN0ZXAgPSBmdW5jdGlvbiAoaW5kZXgpIHtcbiAgICBpZiAoaW5kZXggPj0gcXVldWUubGVuZ3RoKSB7XG4gICAgICBjYigpO1xuICAgIH0gZWxzZSB7XG4gICAgICBpZiAocXVldWVbaW5kZXhdKSB7XG4gICAgICAgIGZuKHF1ZXVlW2luZGV4XSwgZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHN0ZXAoaW5kZXggKyAxKTtcbiAgICAgICAgfSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBzdGVwKGluZGV4ICsgMSk7XG4gICAgICB9XG4gICAgfVxuICB9O1xuICBzdGVwKDApO1xufVxuXG4vKiAgKi9cblxuZnVuY3Rpb24gcmVzb2x2ZUFzeW5jQ29tcG9uZW50cyAobWF0Y2hlZCkge1xuICByZXR1cm4gZnVuY3Rpb24gKHRvLCBmcm9tLCBuZXh0KSB7XG4gICAgdmFyIGhhc0FzeW5jID0gZmFsc2U7XG4gICAgdmFyIHBlbmRpbmcgPSAwO1xuICAgIHZhciBlcnJvciA9IG51bGw7XG5cbiAgICBmbGF0TWFwQ29tcG9uZW50cyhtYXRjaGVkLCBmdW5jdGlvbiAoZGVmLCBfLCBtYXRjaCwga2V5KSB7XG4gICAgICAvLyBpZiBpdCdzIGEgZnVuY3Rpb24gYW5kIGRvZXNuJ3QgaGF2ZSBjaWQgYXR0YWNoZWQsXG4gICAgICAvLyBhc3N1bWUgaXQncyBhbiBhc3luYyBjb21wb25lbnQgcmVzb2x2ZSBmdW5jdGlvbi5cbiAgICAgIC8vIHdlIGFyZSBub3QgdXNpbmcgVnVlJ3MgZGVmYXVsdCBhc3luYyByZXNvbHZpbmcgbWVjaGFuaXNtIGJlY2F1c2VcbiAgICAgIC8vIHdlIHdhbnQgdG8gaGFsdCB0aGUgbmF2aWdhdGlvbiB1bnRpbCB0aGUgaW5jb21pbmcgY29tcG9uZW50IGhhcyBiZWVuXG4gICAgICAvLyByZXNvbHZlZC5cbiAgICAgIGlmICh0eXBlb2YgZGVmID09PSAnZnVuY3Rpb24nICYmIGRlZi5jaWQgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICBoYXNBc3luYyA9IHRydWU7XG4gICAgICAgIHBlbmRpbmcrKztcblxuICAgICAgICB2YXIgcmVzb2x2ZSA9IG9uY2UoZnVuY3Rpb24gKHJlc29sdmVkRGVmKSB7XG4gICAgICAgICAgaWYgKGlzRVNNb2R1bGUocmVzb2x2ZWREZWYpKSB7XG4gICAgICAgICAgICByZXNvbHZlZERlZiA9IHJlc29sdmVkRGVmLmRlZmF1bHQ7XG4gICAgICAgICAgfVxuICAgICAgICAgIC8vIHNhdmUgcmVzb2x2ZWQgb24gYXN5bmMgZmFjdG9yeSBpbiBjYXNlIGl0J3MgdXNlZCBlbHNld2hlcmVcbiAgICAgICAgICBkZWYucmVzb2x2ZWQgPSB0eXBlb2YgcmVzb2x2ZWREZWYgPT09ICdmdW5jdGlvbidcbiAgICAgICAgICAgID8gcmVzb2x2ZWREZWZcbiAgICAgICAgICAgIDogX1Z1ZS5leHRlbmQocmVzb2x2ZWREZWYpO1xuICAgICAgICAgIG1hdGNoLmNvbXBvbmVudHNba2V5XSA9IHJlc29sdmVkRGVmO1xuICAgICAgICAgIHBlbmRpbmctLTtcbiAgICAgICAgICBpZiAocGVuZGluZyA8PSAwKSB7XG4gICAgICAgICAgICBuZXh0KCk7XG4gICAgICAgICAgfVxuICAgICAgICB9KTtcblxuICAgICAgICB2YXIgcmVqZWN0ID0gb25jZShmdW5jdGlvbiAocmVhc29uKSB7XG4gICAgICAgICAgdmFyIG1zZyA9IFwiRmFpbGVkIHRvIHJlc29sdmUgYXN5bmMgY29tcG9uZW50IFwiICsga2V5ICsgXCI6IFwiICsgcmVhc29uO1xuICAgICAgICAgIHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicgJiYgd2FybihmYWxzZSwgbXNnKTtcbiAgICAgICAgICBpZiAoIWVycm9yKSB7XG4gICAgICAgICAgICBlcnJvciA9IGlzRXJyb3IocmVhc29uKVxuICAgICAgICAgICAgICA/IHJlYXNvblxuICAgICAgICAgICAgICA6IG5ldyBFcnJvcihtc2cpO1xuICAgICAgICAgICAgbmV4dChlcnJvcik7XG4gICAgICAgICAgfVxuICAgICAgICB9KTtcblxuICAgICAgICB2YXIgcmVzO1xuICAgICAgICB0cnkge1xuICAgICAgICAgIHJlcyA9IGRlZihyZXNvbHZlLCByZWplY3QpO1xuICAgICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgICAgcmVqZWN0KGUpO1xuICAgICAgICB9XG4gICAgICAgIGlmIChyZXMpIHtcbiAgICAgICAgICBpZiAodHlwZW9mIHJlcy50aGVuID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgICByZXMudGhlbihyZXNvbHZlLCByZWplY3QpO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAvLyBuZXcgc3ludGF4IGluIFZ1ZSAyLjNcbiAgICAgICAgICAgIHZhciBjb21wID0gcmVzLmNvbXBvbmVudDtcbiAgICAgICAgICAgIGlmIChjb21wICYmIHR5cGVvZiBjb21wLnRoZW4gPT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgICAgICAgY29tcC50aGVuKHJlc29sdmUsIHJlamVjdCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfSk7XG5cbiAgICBpZiAoIWhhc0FzeW5jKSB7IG5leHQoKTsgfVxuICB9XG59XG5cbmZ1bmN0aW9uIGZsYXRNYXBDb21wb25lbnRzIChcbiAgbWF0Y2hlZCxcbiAgZm5cbikge1xuICByZXR1cm4gZmxhdHRlbihtYXRjaGVkLm1hcChmdW5jdGlvbiAobSkge1xuICAgIHJldHVybiBPYmplY3Qua2V5cyhtLmNvbXBvbmVudHMpLm1hcChmdW5jdGlvbiAoa2V5KSB7IHJldHVybiBmbihcbiAgICAgIG0uY29tcG9uZW50c1trZXldLFxuICAgICAgbS5pbnN0YW5jZXNba2V5XSxcbiAgICAgIG0sIGtleVxuICAgICk7IH0pXG4gIH0pKVxufVxuXG5mdW5jdGlvbiBmbGF0dGVuIChhcnIpIHtcbiAgcmV0dXJuIEFycmF5LnByb3RvdHlwZS5jb25jYXQuYXBwbHkoW10sIGFycilcbn1cblxudmFyIGhhc1N5bWJvbCA9XG4gIHR5cGVvZiBTeW1ib2wgPT09ICdmdW5jdGlvbicgJiZcbiAgdHlwZW9mIFN5bWJvbC50b1N0cmluZ1RhZyA9PT0gJ3N5bWJvbCc7XG5cbmZ1bmN0aW9uIGlzRVNNb2R1bGUgKG9iaikge1xuICByZXR1cm4gb2JqLl9fZXNNb2R1bGUgfHwgKGhhc1N5bWJvbCAmJiBvYmpbU3ltYm9sLnRvU3RyaW5nVGFnXSA9PT0gJ01vZHVsZScpXG59XG5cbi8vIGluIFdlYnBhY2sgMiwgcmVxdWlyZS5lbnN1cmUgbm93IGFsc28gcmV0dXJucyBhIFByb21pc2Vcbi8vIHNvIHRoZSByZXNvbHZlL3JlamVjdCBmdW5jdGlvbnMgbWF5IGdldCBjYWxsZWQgYW4gZXh0cmEgdGltZVxuLy8gaWYgdGhlIHVzZXIgdXNlcyBhbiBhcnJvdyBmdW5jdGlvbiBzaG9ydGhhbmQgdGhhdCBoYXBwZW5zIHRvXG4vLyByZXR1cm4gdGhhdCBQcm9taXNlLlxuZnVuY3Rpb24gb25jZSAoZm4pIHtcbiAgdmFyIGNhbGxlZCA9IGZhbHNlO1xuICByZXR1cm4gZnVuY3Rpb24gKCkge1xuICAgIHZhciBhcmdzID0gW10sIGxlbiA9IGFyZ3VtZW50cy5sZW5ndGg7XG4gICAgd2hpbGUgKCBsZW4tLSApIGFyZ3NbIGxlbiBdID0gYXJndW1lbnRzWyBsZW4gXTtcblxuICAgIGlmIChjYWxsZWQpIHsgcmV0dXJuIH1cbiAgICBjYWxsZWQgPSB0cnVlO1xuICAgIHJldHVybiBmbi5hcHBseSh0aGlzLCBhcmdzKVxuICB9XG59XG5cbnZhciBOYXZpZ2F0aW9uRHVwbGljYXRlZCA9IC8qQF9fUFVSRV9fKi8oZnVuY3Rpb24gKEVycm9yKSB7XG4gIGZ1bmN0aW9uIE5hdmlnYXRpb25EdXBsaWNhdGVkIChub3JtYWxpemVkTG9jYXRpb24pIHtcbiAgICBFcnJvci5jYWxsKHRoaXMpO1xuICAgIHRoaXMubmFtZSA9IHRoaXMuX25hbWUgPSAnTmF2aWdhdGlvbkR1cGxpY2F0ZWQnO1xuICAgIC8vIHBhc3NpbmcgdGhlIG1lc3NhZ2UgdG8gc3VwZXIoKSBkb2Vzbid0IHNlZW0gdG8gd29yayBpbiB0aGUgdHJhbnNwaWxlZCB2ZXJzaW9uXG4gICAgdGhpcy5tZXNzYWdlID0gXCJOYXZpZ2F0aW5nIHRvIGN1cnJlbnQgbG9jYXRpb24gKFxcXCJcIiArIChub3JtYWxpemVkTG9jYXRpb24uZnVsbFBhdGgpICsgXCJcXFwiKSBpcyBub3QgYWxsb3dlZFwiO1xuICAgIC8vIGFkZCBhIHN0YWNrIHByb3BlcnR5IHNvIHNlcnZpY2VzIGxpa2UgU2VudHJ5IGNhbiBjb3JyZWN0bHkgZGlzcGxheSBpdFxuICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0aGlzLCAnc3RhY2snLCB7XG4gICAgICB2YWx1ZTogbmV3IEVycm9yKCkuc3RhY2ssXG4gICAgICB3cml0YWJsZTogdHJ1ZSxcbiAgICAgIGNvbmZpZ3VyYWJsZTogdHJ1ZVxuICAgIH0pO1xuICAgIC8vIHdlIGNvdWxkIGFsc28gaGF2ZSB1c2VkXG4gICAgLy8gRXJyb3IuY2FwdHVyZVN0YWNrVHJhY2UodGhpcywgdGhpcy5jb25zdHJ1Y3RvcilcbiAgICAvLyBidXQgaXQgb25seSBleGlzdHMgb24gbm9kZSBhbmQgY2hyb21lXG4gIH1cblxuICBpZiAoIEVycm9yICkgTmF2aWdhdGlvbkR1cGxpY2F0ZWQuX19wcm90b19fID0gRXJyb3I7XG4gIE5hdmlnYXRpb25EdXBsaWNhdGVkLnByb3RvdHlwZSA9IE9iamVjdC5jcmVhdGUoIEVycm9yICYmIEVycm9yLnByb3RvdHlwZSApO1xuICBOYXZpZ2F0aW9uRHVwbGljYXRlZC5wcm90b3R5cGUuY29uc3RydWN0b3IgPSBOYXZpZ2F0aW9uRHVwbGljYXRlZDtcblxuICByZXR1cm4gTmF2aWdhdGlvbkR1cGxpY2F0ZWQ7XG59KEVycm9yKSk7XG5cbi8vIHN1cHBvcnQgSUU5XG5OYXZpZ2F0aW9uRHVwbGljYXRlZC5fbmFtZSA9ICdOYXZpZ2F0aW9uRHVwbGljYXRlZCc7XG5cbi8qICAqL1xuXG52YXIgSGlzdG9yeSA9IGZ1bmN0aW9uIEhpc3RvcnkgKHJvdXRlciwgYmFzZSkge1xuICB0aGlzLnJvdXRlciA9IHJvdXRlcjtcbiAgdGhpcy5iYXNlID0gbm9ybWFsaXplQmFzZShiYXNlKTtcbiAgLy8gc3RhcnQgd2l0aCBhIHJvdXRlIG9iamVjdCB0aGF0IHN0YW5kcyBmb3IgXCJub3doZXJlXCJcbiAgdGhpcy5jdXJyZW50ID0gU1RBUlQ7XG4gIHRoaXMucGVuZGluZyA9IG51bGw7XG4gIHRoaXMucmVhZHkgPSBmYWxzZTtcbiAgdGhpcy5yZWFkeUNicyA9IFtdO1xuICB0aGlzLnJlYWR5RXJyb3JDYnMgPSBbXTtcbiAgdGhpcy5lcnJvckNicyA9IFtdO1xufTtcblxuSGlzdG9yeS5wcm90b3R5cGUubGlzdGVuID0gZnVuY3Rpb24gbGlzdGVuIChjYikge1xuICB0aGlzLmNiID0gY2I7XG59O1xuXG5IaXN0b3J5LnByb3RvdHlwZS5vblJlYWR5ID0gZnVuY3Rpb24gb25SZWFkeSAoY2IsIGVycm9yQ2IpIHtcbiAgaWYgKHRoaXMucmVhZHkpIHtcbiAgICBjYigpO1xuICB9IGVsc2Uge1xuICAgIHRoaXMucmVhZHlDYnMucHVzaChjYik7XG4gICAgaWYgKGVycm9yQ2IpIHtcbiAgICAgIHRoaXMucmVhZHlFcnJvckNicy5wdXNoKGVycm9yQ2IpO1xuICAgIH1cbiAgfVxufTtcblxuSGlzdG9yeS5wcm90b3R5cGUub25FcnJvciA9IGZ1bmN0aW9uIG9uRXJyb3IgKGVycm9yQ2IpIHtcbiAgdGhpcy5lcnJvckNicy5wdXNoKGVycm9yQ2IpO1xufTtcblxuSGlzdG9yeS5wcm90b3R5cGUudHJhbnNpdGlvblRvID0gZnVuY3Rpb24gdHJhbnNpdGlvblRvIChcbiAgbG9jYXRpb24sXG4gIG9uQ29tcGxldGUsXG4gIG9uQWJvcnRcbikge1xuICAgIHZhciB0aGlzJDEgPSB0aGlzO1xuXG4gIHZhciByb3V0ZSA9IHRoaXMucm91dGVyLm1hdGNoKGxvY2F0aW9uLCB0aGlzLmN1cnJlbnQpO1xuICB0aGlzLmNvbmZpcm1UcmFuc2l0aW9uKFxuICAgIHJvdXRlLFxuICAgIGZ1bmN0aW9uICgpIHtcbiAgICAgIHRoaXMkMS51cGRhdGVSb3V0ZShyb3V0ZSk7XG4gICAgICBvbkNvbXBsZXRlICYmIG9uQ29tcGxldGUocm91dGUpO1xuICAgICAgdGhpcyQxLmVuc3VyZVVSTCgpO1xuXG4gICAgICAvLyBmaXJlIHJlYWR5IGNicyBvbmNlXG4gICAgICBpZiAoIXRoaXMkMS5yZWFkeSkge1xuICAgICAgICB0aGlzJDEucmVhZHkgPSB0cnVlO1xuICAgICAgICB0aGlzJDEucmVhZHlDYnMuZm9yRWFjaChmdW5jdGlvbiAoY2IpIHtcbiAgICAgICAgICBjYihyb3V0ZSk7XG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgIH0sXG4gICAgZnVuY3Rpb24gKGVycikge1xuICAgICAgaWYgKG9uQWJvcnQpIHtcbiAgICAgICAgb25BYm9ydChlcnIpO1xuICAgICAgfVxuICAgICAgaWYgKGVyciAmJiAhdGhpcyQxLnJlYWR5KSB7XG4gICAgICAgIHRoaXMkMS5yZWFkeSA9IHRydWU7XG4gICAgICAgIHRoaXMkMS5yZWFkeUVycm9yQ2JzLmZvckVhY2goZnVuY3Rpb24gKGNiKSB7XG4gICAgICAgICAgY2IoZXJyKTtcbiAgICAgICAgfSk7XG4gICAgICB9XG4gICAgfVxuICApO1xufTtcblxuSGlzdG9yeS5wcm90b3R5cGUuY29uZmlybVRyYW5zaXRpb24gPSBmdW5jdGlvbiBjb25maXJtVHJhbnNpdGlvbiAocm91dGUsIG9uQ29tcGxldGUsIG9uQWJvcnQpIHtcbiAgICB2YXIgdGhpcyQxID0gdGhpcztcblxuICB2YXIgY3VycmVudCA9IHRoaXMuY3VycmVudDtcbiAgdmFyIGFib3J0ID0gZnVuY3Rpb24gKGVycikge1xuICAgIC8vIGFmdGVyIG1lcmdpbmcgaHR0cHM6Ly9naXRodWIuY29tL3Z1ZWpzL3Z1ZS1yb3V0ZXIvcHVsbC8yNzcxIHdlXG4gICAgLy8gV2hlbiB0aGUgdXNlciBuYXZpZ2F0ZXMgdGhyb3VnaCBoaXN0b3J5IHRocm91Z2ggYmFjay9mb3J3YXJkIGJ1dHRvbnNcbiAgICAvLyB3ZSBkbyBub3Qgd2FudCB0byB0aHJvdyB0aGUgZXJyb3IuIFdlIG9ubHkgdGhyb3cgaXQgaWYgZGlyZWN0bHkgY2FsbGluZ1xuICAgIC8vIHB1c2gvcmVwbGFjZS4gVGhhdCdzIHdoeSBpdCdzIG5vdCBpbmNsdWRlZCBpbiBpc0Vycm9yXG4gICAgaWYgKCFpc0V4dGVuZGVkRXJyb3IoTmF2aWdhdGlvbkR1cGxpY2F0ZWQsIGVycikgJiYgaXNFcnJvcihlcnIpKSB7XG4gICAgICBpZiAodGhpcyQxLmVycm9yQ2JzLmxlbmd0aCkge1xuICAgICAgICB0aGlzJDEuZXJyb3JDYnMuZm9yRWFjaChmdW5jdGlvbiAoY2IpIHtcbiAgICAgICAgICBjYihlcnIpO1xuICAgICAgICB9KTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHdhcm4oZmFsc2UsICd1bmNhdWdodCBlcnJvciBkdXJpbmcgcm91dGUgbmF2aWdhdGlvbjonKTtcbiAgICAgICAgY29uc29sZS5lcnJvcihlcnIpO1xuICAgICAgfVxuICAgIH1cbiAgICBvbkFib3J0ICYmIG9uQWJvcnQoZXJyKTtcbiAgfTtcbiAgaWYgKFxuICAgIGlzU2FtZVJvdXRlKHJvdXRlLCBjdXJyZW50KSAmJlxuICAgIC8vIGluIHRoZSBjYXNlIHRoZSByb3V0ZSBtYXAgaGFzIGJlZW4gZHluYW1pY2FsbHkgYXBwZW5kZWQgdG9cbiAgICByb3V0ZS5tYXRjaGVkLmxlbmd0aCA9PT0gY3VycmVudC5tYXRjaGVkLmxlbmd0aFxuICApIHtcbiAgICB0aGlzLmVuc3VyZVVSTCgpO1xuICAgIHJldHVybiBhYm9ydChuZXcgTmF2aWdhdGlvbkR1cGxpY2F0ZWQocm91dGUpKVxuICB9XG5cbiAgdmFyIHJlZiA9IHJlc29sdmVRdWV1ZShcbiAgICB0aGlzLmN1cnJlbnQubWF0Y2hlZCxcbiAgICByb3V0ZS5tYXRjaGVkXG4gICk7XG4gICAgdmFyIHVwZGF0ZWQgPSByZWYudXBkYXRlZDtcbiAgICB2YXIgZGVhY3RpdmF0ZWQgPSByZWYuZGVhY3RpdmF0ZWQ7XG4gICAgdmFyIGFjdGl2YXRlZCA9IHJlZi5hY3RpdmF0ZWQ7XG5cbiAgdmFyIHF1ZXVlID0gW10uY29uY2F0KFxuICAgIC8vIGluLWNvbXBvbmVudCBsZWF2ZSBndWFyZHNcbiAgICBleHRyYWN0TGVhdmVHdWFyZHMoZGVhY3RpdmF0ZWQpLFxuICAgIC8vIGdsb2JhbCBiZWZvcmUgaG9va3NcbiAgICB0aGlzLnJvdXRlci5iZWZvcmVIb29rcyxcbiAgICAvLyBpbi1jb21wb25lbnQgdXBkYXRlIGhvb2tzXG4gICAgZXh0cmFjdFVwZGF0ZUhvb2tzKHVwZGF0ZWQpLFxuICAgIC8vIGluLWNvbmZpZyBlbnRlciBndWFyZHNcbiAgICBhY3RpdmF0ZWQubWFwKGZ1bmN0aW9uIChtKSB7IHJldHVybiBtLmJlZm9yZUVudGVyOyB9KSxcbiAgICAvLyBhc3luYyBjb21wb25lbnRzXG4gICAgcmVzb2x2ZUFzeW5jQ29tcG9uZW50cyhhY3RpdmF0ZWQpXG4gICk7XG5cbiAgdGhpcy5wZW5kaW5nID0gcm91dGU7XG4gIHZhciBpdGVyYXRvciA9IGZ1bmN0aW9uIChob29rLCBuZXh0KSB7XG4gICAgaWYgKHRoaXMkMS5wZW5kaW5nICE9PSByb3V0ZSkge1xuICAgICAgcmV0dXJuIGFib3J0KClcbiAgICB9XG4gICAgdHJ5IHtcbiAgICAgIGhvb2socm91dGUsIGN1cnJlbnQsIGZ1bmN0aW9uICh0bykge1xuICAgICAgICBpZiAodG8gPT09IGZhbHNlIHx8IGlzRXJyb3IodG8pKSB7XG4gICAgICAgICAgLy8gbmV4dChmYWxzZSkgLT4gYWJvcnQgbmF2aWdhdGlvbiwgZW5zdXJlIGN1cnJlbnQgVVJMXG4gICAgICAgICAgdGhpcyQxLmVuc3VyZVVSTCh0cnVlKTtcbiAgICAgICAgICBhYm9ydCh0byk7XG4gICAgICAgIH0gZWxzZSBpZiAoXG4gICAgICAgICAgdHlwZW9mIHRvID09PSAnc3RyaW5nJyB8fFxuICAgICAgICAgICh0eXBlb2YgdG8gPT09ICdvYmplY3QnICYmXG4gICAgICAgICAgICAodHlwZW9mIHRvLnBhdGggPT09ICdzdHJpbmcnIHx8IHR5cGVvZiB0by5uYW1lID09PSAnc3RyaW5nJykpXG4gICAgICAgICkge1xuICAgICAgICAgIC8vIG5leHQoJy8nKSBvciBuZXh0KHsgcGF0aDogJy8nIH0pIC0+IHJlZGlyZWN0XG4gICAgICAgICAgYWJvcnQoKTtcbiAgICAgICAgICBpZiAodHlwZW9mIHRvID09PSAnb2JqZWN0JyAmJiB0by5yZXBsYWNlKSB7XG4gICAgICAgICAgICB0aGlzJDEucmVwbGFjZSh0byk7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMkMS5wdXNoKHRvKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgLy8gY29uZmlybSB0cmFuc2l0aW9uIGFuZCBwYXNzIG9uIHRoZSB2YWx1ZVxuICAgICAgICAgIG5leHQodG8pO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9IGNhdGNoIChlKSB7XG4gICAgICBhYm9ydChlKTtcbiAgICB9XG4gIH07XG5cbiAgcnVuUXVldWUocXVldWUsIGl0ZXJhdG9yLCBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIHBvc3RFbnRlckNicyA9IFtdO1xuICAgIHZhciBpc1ZhbGlkID0gZnVuY3Rpb24gKCkgeyByZXR1cm4gdGhpcyQxLmN1cnJlbnQgPT09IHJvdXRlOyB9O1xuICAgIC8vIHdhaXQgdW50aWwgYXN5bmMgY29tcG9uZW50cyBhcmUgcmVzb2x2ZWQgYmVmb3JlXG4gICAgLy8gZXh0cmFjdGluZyBpbi1jb21wb25lbnQgZW50ZXIgZ3VhcmRzXG4gICAgdmFyIGVudGVyR3VhcmRzID0gZXh0cmFjdEVudGVyR3VhcmRzKGFjdGl2YXRlZCwgcG9zdEVudGVyQ2JzLCBpc1ZhbGlkKTtcbiAgICB2YXIgcXVldWUgPSBlbnRlckd1YXJkcy5jb25jYXQodGhpcyQxLnJvdXRlci5yZXNvbHZlSG9va3MpO1xuICAgIHJ1blF1ZXVlKHF1ZXVlLCBpdGVyYXRvciwgZnVuY3Rpb24gKCkge1xuICAgICAgaWYgKHRoaXMkMS5wZW5kaW5nICE9PSByb3V0ZSkge1xuICAgICAgICByZXR1cm4gYWJvcnQoKVxuICAgICAgfVxuICAgICAgdGhpcyQxLnBlbmRpbmcgPSBudWxsO1xuICAgICAgb25Db21wbGV0ZShyb3V0ZSk7XG4gICAgICBpZiAodGhpcyQxLnJvdXRlci5hcHApIHtcbiAgICAgICAgdGhpcyQxLnJvdXRlci5hcHAuJG5leHRUaWNrKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICBwb3N0RW50ZXJDYnMuZm9yRWFjaChmdW5jdGlvbiAoY2IpIHtcbiAgICAgICAgICAgIGNiKCk7XG4gICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgIH0pO1xuICB9KTtcbn07XG5cbkhpc3RvcnkucHJvdG90eXBlLnVwZGF0ZVJvdXRlID0gZnVuY3Rpb24gdXBkYXRlUm91dGUgKHJvdXRlKSB7XG4gIHZhciBwcmV2ID0gdGhpcy5jdXJyZW50O1xuICB0aGlzLmN1cnJlbnQgPSByb3V0ZTtcbiAgdGhpcy5jYiAmJiB0aGlzLmNiKHJvdXRlKTtcbiAgdGhpcy5yb3V0ZXIuYWZ0ZXJIb29rcy5mb3JFYWNoKGZ1bmN0aW9uIChob29rKSB7XG4gICAgaG9vayAmJiBob29rKHJvdXRlLCBwcmV2KTtcbiAgfSk7XG59O1xuXG5mdW5jdGlvbiBub3JtYWxpemVCYXNlIChiYXNlKSB7XG4gIGlmICghYmFzZSkge1xuICAgIGlmIChpbkJyb3dzZXIpIHtcbiAgICAgIC8vIHJlc3BlY3QgPGJhc2U+IHRhZ1xuICAgICAgdmFyIGJhc2VFbCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ2Jhc2UnKTtcbiAgICAgIGJhc2UgPSAoYmFzZUVsICYmIGJhc2VFbC5nZXRBdHRyaWJ1dGUoJ2hyZWYnKSkgfHwgJy8nO1xuICAgICAgLy8gc3RyaXAgZnVsbCBVUkwgb3JpZ2luXG4gICAgICBiYXNlID0gYmFzZS5yZXBsYWNlKC9eaHR0cHM/OlxcL1xcL1teXFwvXSsvLCAnJyk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGJhc2UgPSAnLyc7XG4gICAgfVxuICB9XG4gIC8vIG1ha2Ugc3VyZSB0aGVyZSdzIHRoZSBzdGFydGluZyBzbGFzaFxuICBpZiAoYmFzZS5jaGFyQXQoMCkgIT09ICcvJykge1xuICAgIGJhc2UgPSAnLycgKyBiYXNlO1xuICB9XG4gIC8vIHJlbW92ZSB0cmFpbGluZyBzbGFzaFxuICByZXR1cm4gYmFzZS5yZXBsYWNlKC9cXC8kLywgJycpXG59XG5cbmZ1bmN0aW9uIHJlc29sdmVRdWV1ZSAoXG4gIGN1cnJlbnQsXG4gIG5leHRcbikge1xuICB2YXIgaTtcbiAgdmFyIG1heCA9IE1hdGgubWF4KGN1cnJlbnQubGVuZ3RoLCBuZXh0Lmxlbmd0aCk7XG4gIGZvciAoaSA9IDA7IGkgPCBtYXg7IGkrKykge1xuICAgIGlmIChjdXJyZW50W2ldICE9PSBuZXh0W2ldKSB7XG4gICAgICBicmVha1xuICAgIH1cbiAgfVxuICByZXR1cm4ge1xuICAgIHVwZGF0ZWQ6IG5leHQuc2xpY2UoMCwgaSksXG4gICAgYWN0aXZhdGVkOiBuZXh0LnNsaWNlKGkpLFxuICAgIGRlYWN0aXZhdGVkOiBjdXJyZW50LnNsaWNlKGkpXG4gIH1cbn1cblxuZnVuY3Rpb24gZXh0cmFjdEd1YXJkcyAoXG4gIHJlY29yZHMsXG4gIG5hbWUsXG4gIGJpbmQsXG4gIHJldmVyc2Vcbikge1xuICB2YXIgZ3VhcmRzID0gZmxhdE1hcENvbXBvbmVudHMocmVjb3JkcywgZnVuY3Rpb24gKGRlZiwgaW5zdGFuY2UsIG1hdGNoLCBrZXkpIHtcbiAgICB2YXIgZ3VhcmQgPSBleHRyYWN0R3VhcmQoZGVmLCBuYW1lKTtcbiAgICBpZiAoZ3VhcmQpIHtcbiAgICAgIHJldHVybiBBcnJheS5pc0FycmF5KGd1YXJkKVxuICAgICAgICA/IGd1YXJkLm1hcChmdW5jdGlvbiAoZ3VhcmQpIHsgcmV0dXJuIGJpbmQoZ3VhcmQsIGluc3RhbmNlLCBtYXRjaCwga2V5KTsgfSlcbiAgICAgICAgOiBiaW5kKGd1YXJkLCBpbnN0YW5jZSwgbWF0Y2gsIGtleSlcbiAgICB9XG4gIH0pO1xuICByZXR1cm4gZmxhdHRlbihyZXZlcnNlID8gZ3VhcmRzLnJldmVyc2UoKSA6IGd1YXJkcylcbn1cblxuZnVuY3Rpb24gZXh0cmFjdEd1YXJkIChcbiAgZGVmLFxuICBrZXlcbikge1xuICBpZiAodHlwZW9mIGRlZiAhPT0gJ2Z1bmN0aW9uJykge1xuICAgIC8vIGV4dGVuZCBub3cgc28gdGhhdCBnbG9iYWwgbWl4aW5zIGFyZSBhcHBsaWVkLlxuICAgIGRlZiA9IF9WdWUuZXh0ZW5kKGRlZik7XG4gIH1cbiAgcmV0dXJuIGRlZi5vcHRpb25zW2tleV1cbn1cblxuZnVuY3Rpb24gZXh0cmFjdExlYXZlR3VhcmRzIChkZWFjdGl2YXRlZCkge1xuICByZXR1cm4gZXh0cmFjdEd1YXJkcyhkZWFjdGl2YXRlZCwgJ2JlZm9yZVJvdXRlTGVhdmUnLCBiaW5kR3VhcmQsIHRydWUpXG59XG5cbmZ1bmN0aW9uIGV4dHJhY3RVcGRhdGVIb29rcyAodXBkYXRlZCkge1xuICByZXR1cm4gZXh0cmFjdEd1YXJkcyh1cGRhdGVkLCAnYmVmb3JlUm91dGVVcGRhdGUnLCBiaW5kR3VhcmQpXG59XG5cbmZ1bmN0aW9uIGJpbmRHdWFyZCAoZ3VhcmQsIGluc3RhbmNlKSB7XG4gIGlmIChpbnN0YW5jZSkge1xuICAgIHJldHVybiBmdW5jdGlvbiBib3VuZFJvdXRlR3VhcmQgKCkge1xuICAgICAgcmV0dXJuIGd1YXJkLmFwcGx5KGluc3RhbmNlLCBhcmd1bWVudHMpXG4gICAgfVxuICB9XG59XG5cbmZ1bmN0aW9uIGV4dHJhY3RFbnRlckd1YXJkcyAoXG4gIGFjdGl2YXRlZCxcbiAgY2JzLFxuICBpc1ZhbGlkXG4pIHtcbiAgcmV0dXJuIGV4dHJhY3RHdWFyZHMoXG4gICAgYWN0aXZhdGVkLFxuICAgICdiZWZvcmVSb3V0ZUVudGVyJyxcbiAgICBmdW5jdGlvbiAoZ3VhcmQsIF8sIG1hdGNoLCBrZXkpIHtcbiAgICAgIHJldHVybiBiaW5kRW50ZXJHdWFyZChndWFyZCwgbWF0Y2gsIGtleSwgY2JzLCBpc1ZhbGlkKVxuICAgIH1cbiAgKVxufVxuXG5mdW5jdGlvbiBiaW5kRW50ZXJHdWFyZCAoXG4gIGd1YXJkLFxuICBtYXRjaCxcbiAga2V5LFxuICBjYnMsXG4gIGlzVmFsaWRcbikge1xuICByZXR1cm4gZnVuY3Rpb24gcm91dGVFbnRlckd1YXJkICh0bywgZnJvbSwgbmV4dCkge1xuICAgIHJldHVybiBndWFyZCh0bywgZnJvbSwgZnVuY3Rpb24gKGNiKSB7XG4gICAgICBpZiAodHlwZW9mIGNiID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgIGNicy5wdXNoKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAvLyAjNzUwXG4gICAgICAgICAgLy8gaWYgYSByb3V0ZXItdmlldyBpcyB3cmFwcGVkIHdpdGggYW4gb3V0LWluIHRyYW5zaXRpb24sXG4gICAgICAgICAgLy8gdGhlIGluc3RhbmNlIG1heSBub3QgaGF2ZSBiZWVuIHJlZ2lzdGVyZWQgYXQgdGhpcyB0aW1lLlxuICAgICAgICAgIC8vIHdlIHdpbGwgbmVlZCB0byBwb2xsIGZvciByZWdpc3RyYXRpb24gdW50aWwgY3VycmVudCByb3V0ZVxuICAgICAgICAgIC8vIGlzIG5vIGxvbmdlciB2YWxpZC5cbiAgICAgICAgICBwb2xsKGNiLCBtYXRjaC5pbnN0YW5jZXMsIGtleSwgaXNWYWxpZCk7XG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgICAgbmV4dChjYik7XG4gICAgfSlcbiAgfVxufVxuXG5mdW5jdGlvbiBwb2xsIChcbiAgY2IsIC8vIHNvbWVob3cgZmxvdyBjYW5ub3QgaW5mZXIgdGhpcyBpcyBhIGZ1bmN0aW9uXG4gIGluc3RhbmNlcyxcbiAga2V5LFxuICBpc1ZhbGlkXG4pIHtcbiAgaWYgKFxuICAgIGluc3RhbmNlc1trZXldICYmXG4gICAgIWluc3RhbmNlc1trZXldLl9pc0JlaW5nRGVzdHJveWVkIC8vIGRvIG5vdCByZXVzZSBiZWluZyBkZXN0cm95ZWQgaW5zdGFuY2VcbiAgKSB7XG4gICAgY2IoaW5zdGFuY2VzW2tleV0pO1xuICB9IGVsc2UgaWYgKGlzVmFsaWQoKSkge1xuICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xuICAgICAgcG9sbChjYiwgaW5zdGFuY2VzLCBrZXksIGlzVmFsaWQpO1xuICAgIH0sIDE2KTtcbiAgfVxufVxuXG4vKiAgKi9cblxudmFyIEhUTUw1SGlzdG9yeSA9IC8qQF9fUFVSRV9fKi8oZnVuY3Rpb24gKEhpc3RvcnkpIHtcbiAgZnVuY3Rpb24gSFRNTDVIaXN0b3J5IChyb3V0ZXIsIGJhc2UpIHtcbiAgICB2YXIgdGhpcyQxID0gdGhpcztcblxuICAgIEhpc3RvcnkuY2FsbCh0aGlzLCByb3V0ZXIsIGJhc2UpO1xuXG4gICAgdmFyIGV4cGVjdFNjcm9sbCA9IHJvdXRlci5vcHRpb25zLnNjcm9sbEJlaGF2aW9yO1xuICAgIHZhciBzdXBwb3J0c1Njcm9sbCA9IHN1cHBvcnRzUHVzaFN0YXRlICYmIGV4cGVjdFNjcm9sbDtcblxuICAgIGlmIChzdXBwb3J0c1Njcm9sbCkge1xuICAgICAgc2V0dXBTY3JvbGwoKTtcbiAgICB9XG5cbiAgICB2YXIgaW5pdExvY2F0aW9uID0gZ2V0TG9jYXRpb24odGhpcy5iYXNlKTtcbiAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcigncG9wc3RhdGUnLCBmdW5jdGlvbiAoZSkge1xuICAgICAgdmFyIGN1cnJlbnQgPSB0aGlzJDEuY3VycmVudDtcblxuICAgICAgLy8gQXZvaWRpbmcgZmlyc3QgYHBvcHN0YXRlYCBldmVudCBkaXNwYXRjaGVkIGluIHNvbWUgYnJvd3NlcnMgYnV0IGZpcnN0XG4gICAgICAvLyBoaXN0b3J5IHJvdXRlIG5vdCB1cGRhdGVkIHNpbmNlIGFzeW5jIGd1YXJkIGF0IHRoZSBzYW1lIHRpbWUuXG4gICAgICB2YXIgbG9jYXRpb24gPSBnZXRMb2NhdGlvbih0aGlzJDEuYmFzZSk7XG4gICAgICBpZiAodGhpcyQxLmN1cnJlbnQgPT09IFNUQVJUICYmIGxvY2F0aW9uID09PSBpbml0TG9jYXRpb24pIHtcbiAgICAgICAgcmV0dXJuXG4gICAgICB9XG5cbiAgICAgIHRoaXMkMS50cmFuc2l0aW9uVG8obG9jYXRpb24sIGZ1bmN0aW9uIChyb3V0ZSkge1xuICAgICAgICBpZiAoc3VwcG9ydHNTY3JvbGwpIHtcbiAgICAgICAgICBoYW5kbGVTY3JvbGwocm91dGVyLCByb3V0ZSwgY3VycmVudCwgdHJ1ZSk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG5cbiAgaWYgKCBIaXN0b3J5ICkgSFRNTDVIaXN0b3J5Ll9fcHJvdG9fXyA9IEhpc3Rvcnk7XG4gIEhUTUw1SGlzdG9yeS5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKCBIaXN0b3J5ICYmIEhpc3RvcnkucHJvdG90eXBlICk7XG4gIEhUTUw1SGlzdG9yeS5wcm90b3R5cGUuY29uc3RydWN0b3IgPSBIVE1MNUhpc3Rvcnk7XG5cbiAgSFRNTDVIaXN0b3J5LnByb3RvdHlwZS5nbyA9IGZ1bmN0aW9uIGdvIChuKSB7XG4gICAgd2luZG93Lmhpc3RvcnkuZ28obik7XG4gIH07XG5cbiAgSFRNTDVIaXN0b3J5LnByb3RvdHlwZS5wdXNoID0gZnVuY3Rpb24gcHVzaCAobG9jYXRpb24sIG9uQ29tcGxldGUsIG9uQWJvcnQpIHtcbiAgICB2YXIgdGhpcyQxID0gdGhpcztcblxuICAgIHZhciByZWYgPSB0aGlzO1xuICAgIHZhciBmcm9tUm91dGUgPSByZWYuY3VycmVudDtcbiAgICB0aGlzLnRyYW5zaXRpb25Ubyhsb2NhdGlvbiwgZnVuY3Rpb24gKHJvdXRlKSB7XG4gICAgICBwdXNoU3RhdGUoY2xlYW5QYXRoKHRoaXMkMS5iYXNlICsgcm91dGUuZnVsbFBhdGgpKTtcbiAgICAgIGhhbmRsZVNjcm9sbCh0aGlzJDEucm91dGVyLCByb3V0ZSwgZnJvbVJvdXRlLCBmYWxzZSk7XG4gICAgICBvbkNvbXBsZXRlICYmIG9uQ29tcGxldGUocm91dGUpO1xuICAgIH0sIG9uQWJvcnQpO1xuICB9O1xuXG4gIEhUTUw1SGlzdG9yeS5wcm90b3R5cGUucmVwbGFjZSA9IGZ1bmN0aW9uIHJlcGxhY2UgKGxvY2F0aW9uLCBvbkNvbXBsZXRlLCBvbkFib3J0KSB7XG4gICAgdmFyIHRoaXMkMSA9IHRoaXM7XG5cbiAgICB2YXIgcmVmID0gdGhpcztcbiAgICB2YXIgZnJvbVJvdXRlID0gcmVmLmN1cnJlbnQ7XG4gICAgdGhpcy50cmFuc2l0aW9uVG8obG9jYXRpb24sIGZ1bmN0aW9uIChyb3V0ZSkge1xuICAgICAgcmVwbGFjZVN0YXRlKGNsZWFuUGF0aCh0aGlzJDEuYmFzZSArIHJvdXRlLmZ1bGxQYXRoKSk7XG4gICAgICBoYW5kbGVTY3JvbGwodGhpcyQxLnJvdXRlciwgcm91dGUsIGZyb21Sb3V0ZSwgZmFsc2UpO1xuICAgICAgb25Db21wbGV0ZSAmJiBvbkNvbXBsZXRlKHJvdXRlKTtcbiAgICB9LCBvbkFib3J0KTtcbiAgfTtcblxuICBIVE1MNUhpc3RvcnkucHJvdG90eXBlLmVuc3VyZVVSTCA9IGZ1bmN0aW9uIGVuc3VyZVVSTCAocHVzaCkge1xuICAgIGlmIChnZXRMb2NhdGlvbih0aGlzLmJhc2UpICE9PSB0aGlzLmN1cnJlbnQuZnVsbFBhdGgpIHtcbiAgICAgIHZhciBjdXJyZW50ID0gY2xlYW5QYXRoKHRoaXMuYmFzZSArIHRoaXMuY3VycmVudC5mdWxsUGF0aCk7XG4gICAgICBwdXNoID8gcHVzaFN0YXRlKGN1cnJlbnQpIDogcmVwbGFjZVN0YXRlKGN1cnJlbnQpO1xuICAgIH1cbiAgfTtcblxuICBIVE1MNUhpc3RvcnkucHJvdG90eXBlLmdldEN1cnJlbnRMb2NhdGlvbiA9IGZ1bmN0aW9uIGdldEN1cnJlbnRMb2NhdGlvbiAoKSB7XG4gICAgcmV0dXJuIGdldExvY2F0aW9uKHRoaXMuYmFzZSlcbiAgfTtcblxuICByZXR1cm4gSFRNTDVIaXN0b3J5O1xufShIaXN0b3J5KSk7XG5cbmZ1bmN0aW9uIGdldExvY2F0aW9uIChiYXNlKSB7XG4gIHZhciBwYXRoID0gZGVjb2RlVVJJKHdpbmRvdy5sb2NhdGlvbi5wYXRobmFtZSk7XG4gIGlmIChiYXNlICYmIHBhdGguaW5kZXhPZihiYXNlKSA9PT0gMCkge1xuICAgIHBhdGggPSBwYXRoLnNsaWNlKGJhc2UubGVuZ3RoKTtcbiAgfVxuICByZXR1cm4gKHBhdGggfHwgJy8nKSArIHdpbmRvdy5sb2NhdGlvbi5zZWFyY2ggKyB3aW5kb3cubG9jYXRpb24uaGFzaFxufVxuXG4vKiAgKi9cblxudmFyIEhhc2hIaXN0b3J5ID0gLypAX19QVVJFX18qLyhmdW5jdGlvbiAoSGlzdG9yeSkge1xuICBmdW5jdGlvbiBIYXNoSGlzdG9yeSAocm91dGVyLCBiYXNlLCBmYWxsYmFjaykge1xuICAgIEhpc3RvcnkuY2FsbCh0aGlzLCByb3V0ZXIsIGJhc2UpO1xuICAgIC8vIGNoZWNrIGhpc3RvcnkgZmFsbGJhY2sgZGVlcGxpbmtpbmdcbiAgICBpZiAoZmFsbGJhY2sgJiYgY2hlY2tGYWxsYmFjayh0aGlzLmJhc2UpKSB7XG4gICAgICByZXR1cm5cbiAgICB9XG4gICAgZW5zdXJlU2xhc2goKTtcbiAgfVxuXG4gIGlmICggSGlzdG9yeSApIEhhc2hIaXN0b3J5Ll9fcHJvdG9fXyA9IEhpc3Rvcnk7XG4gIEhhc2hIaXN0b3J5LnByb3RvdHlwZSA9IE9iamVjdC5jcmVhdGUoIEhpc3RvcnkgJiYgSGlzdG9yeS5wcm90b3R5cGUgKTtcbiAgSGFzaEhpc3RvcnkucHJvdG90eXBlLmNvbnN0cnVjdG9yID0gSGFzaEhpc3Rvcnk7XG5cbiAgLy8gdGhpcyBpcyBkZWxheWVkIHVudGlsIHRoZSBhcHAgbW91bnRzXG4gIC8vIHRvIGF2b2lkIHRoZSBoYXNoY2hhbmdlIGxpc3RlbmVyIGJlaW5nIGZpcmVkIHRvbyBlYXJseVxuICBIYXNoSGlzdG9yeS5wcm90b3R5cGUuc2V0dXBMaXN0ZW5lcnMgPSBmdW5jdGlvbiBzZXR1cExpc3RlbmVycyAoKSB7XG4gICAgdmFyIHRoaXMkMSA9IHRoaXM7XG5cbiAgICB2YXIgcm91dGVyID0gdGhpcy5yb3V0ZXI7XG4gICAgdmFyIGV4cGVjdFNjcm9sbCA9IHJvdXRlci5vcHRpb25zLnNjcm9sbEJlaGF2aW9yO1xuICAgIHZhciBzdXBwb3J0c1Njcm9sbCA9IHN1cHBvcnRzUHVzaFN0YXRlICYmIGV4cGVjdFNjcm9sbDtcblxuICAgIGlmIChzdXBwb3J0c1Njcm9sbCkge1xuICAgICAgc2V0dXBTY3JvbGwoKTtcbiAgICB9XG5cbiAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcbiAgICAgIHN1cHBvcnRzUHVzaFN0YXRlID8gJ3BvcHN0YXRlJyA6ICdoYXNoY2hhbmdlJyxcbiAgICAgIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIGN1cnJlbnQgPSB0aGlzJDEuY3VycmVudDtcbiAgICAgICAgaWYgKCFlbnN1cmVTbGFzaCgpKSB7XG4gICAgICAgICAgcmV0dXJuXG4gICAgICAgIH1cbiAgICAgICAgdGhpcyQxLnRyYW5zaXRpb25UbyhnZXRIYXNoKCksIGZ1bmN0aW9uIChyb3V0ZSkge1xuICAgICAgICAgIGlmIChzdXBwb3J0c1Njcm9sbCkge1xuICAgICAgICAgICAgaGFuZGxlU2Nyb2xsKHRoaXMkMS5yb3V0ZXIsIHJvdXRlLCBjdXJyZW50LCB0cnVlKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYgKCFzdXBwb3J0c1B1c2hTdGF0ZSkge1xuICAgICAgICAgICAgcmVwbGFjZUhhc2gocm91dGUuZnVsbFBhdGgpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICB9XG4gICAgKTtcbiAgfTtcblxuICBIYXNoSGlzdG9yeS5wcm90b3R5cGUucHVzaCA9IGZ1bmN0aW9uIHB1c2ggKGxvY2F0aW9uLCBvbkNvbXBsZXRlLCBvbkFib3J0KSB7XG4gICAgdmFyIHRoaXMkMSA9IHRoaXM7XG5cbiAgICB2YXIgcmVmID0gdGhpcztcbiAgICB2YXIgZnJvbVJvdXRlID0gcmVmLmN1cnJlbnQ7XG4gICAgdGhpcy50cmFuc2l0aW9uVG8oXG4gICAgICBsb2NhdGlvbixcbiAgICAgIGZ1bmN0aW9uIChyb3V0ZSkge1xuICAgICAgICBwdXNoSGFzaChyb3V0ZS5mdWxsUGF0aCk7XG4gICAgICAgIGhhbmRsZVNjcm9sbCh0aGlzJDEucm91dGVyLCByb3V0ZSwgZnJvbVJvdXRlLCBmYWxzZSk7XG4gICAgICAgIG9uQ29tcGxldGUgJiYgb25Db21wbGV0ZShyb3V0ZSk7XG4gICAgICB9LFxuICAgICAgb25BYm9ydFxuICAgICk7XG4gIH07XG5cbiAgSGFzaEhpc3RvcnkucHJvdG90eXBlLnJlcGxhY2UgPSBmdW5jdGlvbiByZXBsYWNlIChsb2NhdGlvbiwgb25Db21wbGV0ZSwgb25BYm9ydCkge1xuICAgIHZhciB0aGlzJDEgPSB0aGlzO1xuXG4gICAgdmFyIHJlZiA9IHRoaXM7XG4gICAgdmFyIGZyb21Sb3V0ZSA9IHJlZi5jdXJyZW50O1xuICAgIHRoaXMudHJhbnNpdGlvblRvKFxuICAgICAgbG9jYXRpb24sXG4gICAgICBmdW5jdGlvbiAocm91dGUpIHtcbiAgICAgICAgcmVwbGFjZUhhc2gocm91dGUuZnVsbFBhdGgpO1xuICAgICAgICBoYW5kbGVTY3JvbGwodGhpcyQxLnJvdXRlciwgcm91dGUsIGZyb21Sb3V0ZSwgZmFsc2UpO1xuICAgICAgICBvbkNvbXBsZXRlICYmIG9uQ29tcGxldGUocm91dGUpO1xuICAgICAgfSxcbiAgICAgIG9uQWJvcnRcbiAgICApO1xuICB9O1xuXG4gIEhhc2hIaXN0b3J5LnByb3RvdHlwZS5nbyA9IGZ1bmN0aW9uIGdvIChuKSB7XG4gICAgd2luZG93Lmhpc3RvcnkuZ28obik7XG4gIH07XG5cbiAgSGFzaEhpc3RvcnkucHJvdG90eXBlLmVuc3VyZVVSTCA9IGZ1bmN0aW9uIGVuc3VyZVVSTCAocHVzaCkge1xuICAgIHZhciBjdXJyZW50ID0gdGhpcy5jdXJyZW50LmZ1bGxQYXRoO1xuICAgIGlmIChnZXRIYXNoKCkgIT09IGN1cnJlbnQpIHtcbiAgICAgIHB1c2ggPyBwdXNoSGFzaChjdXJyZW50KSA6IHJlcGxhY2VIYXNoKGN1cnJlbnQpO1xuICAgIH1cbiAgfTtcblxuICBIYXNoSGlzdG9yeS5wcm90b3R5cGUuZ2V0Q3VycmVudExvY2F0aW9uID0gZnVuY3Rpb24gZ2V0Q3VycmVudExvY2F0aW9uICgpIHtcbiAgICByZXR1cm4gZ2V0SGFzaCgpXG4gIH07XG5cbiAgcmV0dXJuIEhhc2hIaXN0b3J5O1xufShIaXN0b3J5KSk7XG5cbmZ1bmN0aW9uIGNoZWNrRmFsbGJhY2sgKGJhc2UpIHtcbiAgdmFyIGxvY2F0aW9uID0gZ2V0TG9jYXRpb24oYmFzZSk7XG4gIGlmICghL15cXC8jLy50ZXN0KGxvY2F0aW9uKSkge1xuICAgIHdpbmRvdy5sb2NhdGlvbi5yZXBsYWNlKGNsZWFuUGF0aChiYXNlICsgJy8jJyArIGxvY2F0aW9uKSk7XG4gICAgcmV0dXJuIHRydWVcbiAgfVxufVxuXG5mdW5jdGlvbiBlbnN1cmVTbGFzaCAoKSB7XG4gIHZhciBwYXRoID0gZ2V0SGFzaCgpO1xuICBpZiAocGF0aC5jaGFyQXQoMCkgPT09ICcvJykge1xuICAgIHJldHVybiB0cnVlXG4gIH1cbiAgcmVwbGFjZUhhc2goJy8nICsgcGF0aCk7XG4gIHJldHVybiBmYWxzZVxufVxuXG5mdW5jdGlvbiBnZXRIYXNoICgpIHtcbiAgLy8gV2UgY2FuJ3QgdXNlIHdpbmRvdy5sb2NhdGlvbi5oYXNoIGhlcmUgYmVjYXVzZSBpdCdzIG5vdFxuICAvLyBjb25zaXN0ZW50IGFjcm9zcyBicm93c2VycyAtIEZpcmVmb3ggd2lsbCBwcmUtZGVjb2RlIGl0IVxuICB2YXIgaHJlZiA9IHdpbmRvdy5sb2NhdGlvbi5ocmVmO1xuICB2YXIgaW5kZXggPSBocmVmLmluZGV4T2YoJyMnKTtcbiAgLy8gZW1wdHkgcGF0aFxuICBpZiAoaW5kZXggPCAwKSB7IHJldHVybiAnJyB9XG5cbiAgaHJlZiA9IGhyZWYuc2xpY2UoaW5kZXggKyAxKTtcbiAgLy8gZGVjb2RlIHRoZSBoYXNoIGJ1dCBub3QgdGhlIHNlYXJjaCBvciBoYXNoXG4gIC8vIGFzIHNlYXJjaChxdWVyeSkgaXMgYWxyZWFkeSBkZWNvZGVkXG4gIC8vIGh0dHBzOi8vZ2l0aHViLmNvbS92dWVqcy92dWUtcm91dGVyL2lzc3Vlcy8yNzA4XG4gIHZhciBzZWFyY2hJbmRleCA9IGhyZWYuaW5kZXhPZignPycpO1xuICBpZiAoc2VhcmNoSW5kZXggPCAwKSB7XG4gICAgdmFyIGhhc2hJbmRleCA9IGhyZWYuaW5kZXhPZignIycpO1xuICAgIGlmIChoYXNoSW5kZXggPiAtMSkge1xuICAgICAgaHJlZiA9IGRlY29kZVVSSShocmVmLnNsaWNlKDAsIGhhc2hJbmRleCkpICsgaHJlZi5zbGljZShoYXNoSW5kZXgpO1xuICAgIH0gZWxzZSB7IGhyZWYgPSBkZWNvZGVVUkkoaHJlZik7IH1cbiAgfSBlbHNlIHtcbiAgICBpZiAoc2VhcmNoSW5kZXggPiAtMSkge1xuICAgICAgaHJlZiA9IGRlY29kZVVSSShocmVmLnNsaWNlKDAsIHNlYXJjaEluZGV4KSkgKyBocmVmLnNsaWNlKHNlYXJjaEluZGV4KTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gaHJlZlxufVxuXG5mdW5jdGlvbiBnZXRVcmwgKHBhdGgpIHtcbiAgdmFyIGhyZWYgPSB3aW5kb3cubG9jYXRpb24uaHJlZjtcbiAgdmFyIGkgPSBocmVmLmluZGV4T2YoJyMnKTtcbiAgdmFyIGJhc2UgPSBpID49IDAgPyBocmVmLnNsaWNlKDAsIGkpIDogaHJlZjtcbiAgcmV0dXJuIChiYXNlICsgXCIjXCIgKyBwYXRoKVxufVxuXG5mdW5jdGlvbiBwdXNoSGFzaCAocGF0aCkge1xuICBpZiAoc3VwcG9ydHNQdXNoU3RhdGUpIHtcbiAgICBwdXNoU3RhdGUoZ2V0VXJsKHBhdGgpKTtcbiAgfSBlbHNlIHtcbiAgICB3aW5kb3cubG9jYXRpb24uaGFzaCA9IHBhdGg7XG4gIH1cbn1cblxuZnVuY3Rpb24gcmVwbGFjZUhhc2ggKHBhdGgpIHtcbiAgaWYgKHN1cHBvcnRzUHVzaFN0YXRlKSB7XG4gICAgcmVwbGFjZVN0YXRlKGdldFVybChwYXRoKSk7XG4gIH0gZWxzZSB7XG4gICAgd2luZG93LmxvY2F0aW9uLnJlcGxhY2UoZ2V0VXJsKHBhdGgpKTtcbiAgfVxufVxuXG4vKiAgKi9cblxudmFyIEFic3RyYWN0SGlzdG9yeSA9IC8qQF9fUFVSRV9fKi8oZnVuY3Rpb24gKEhpc3RvcnkpIHtcbiAgZnVuY3Rpb24gQWJzdHJhY3RIaXN0b3J5IChyb3V0ZXIsIGJhc2UpIHtcbiAgICBIaXN0b3J5LmNhbGwodGhpcywgcm91dGVyLCBiYXNlKTtcbiAgICB0aGlzLnN0YWNrID0gW107XG4gICAgdGhpcy5pbmRleCA9IC0xO1xuICB9XG5cbiAgaWYgKCBIaXN0b3J5ICkgQWJzdHJhY3RIaXN0b3J5Ll9fcHJvdG9fXyA9IEhpc3Rvcnk7XG4gIEFic3RyYWN0SGlzdG9yeS5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKCBIaXN0b3J5ICYmIEhpc3RvcnkucHJvdG90eXBlICk7XG4gIEFic3RyYWN0SGlzdG9yeS5wcm90b3R5cGUuY29uc3RydWN0b3IgPSBBYnN0cmFjdEhpc3Rvcnk7XG5cbiAgQWJzdHJhY3RIaXN0b3J5LnByb3RvdHlwZS5wdXNoID0gZnVuY3Rpb24gcHVzaCAobG9jYXRpb24sIG9uQ29tcGxldGUsIG9uQWJvcnQpIHtcbiAgICB2YXIgdGhpcyQxID0gdGhpcztcblxuICAgIHRoaXMudHJhbnNpdGlvblRvKFxuICAgICAgbG9jYXRpb24sXG4gICAgICBmdW5jdGlvbiAocm91dGUpIHtcbiAgICAgICAgdGhpcyQxLnN0YWNrID0gdGhpcyQxLnN0YWNrLnNsaWNlKDAsIHRoaXMkMS5pbmRleCArIDEpLmNvbmNhdChyb3V0ZSk7XG4gICAgICAgIHRoaXMkMS5pbmRleCsrO1xuICAgICAgICBvbkNvbXBsZXRlICYmIG9uQ29tcGxldGUocm91dGUpO1xuICAgICAgfSxcbiAgICAgIG9uQWJvcnRcbiAgICApO1xuICB9O1xuXG4gIEFic3RyYWN0SGlzdG9yeS5wcm90b3R5cGUucmVwbGFjZSA9IGZ1bmN0aW9uIHJlcGxhY2UgKGxvY2F0aW9uLCBvbkNvbXBsZXRlLCBvbkFib3J0KSB7XG4gICAgdmFyIHRoaXMkMSA9IHRoaXM7XG5cbiAgICB0aGlzLnRyYW5zaXRpb25UbyhcbiAgICAgIGxvY2F0aW9uLFxuICAgICAgZnVuY3Rpb24gKHJvdXRlKSB7XG4gICAgICAgIHRoaXMkMS5zdGFjayA9IHRoaXMkMS5zdGFjay5zbGljZSgwLCB0aGlzJDEuaW5kZXgpLmNvbmNhdChyb3V0ZSk7XG4gICAgICAgIG9uQ29tcGxldGUgJiYgb25Db21wbGV0ZShyb3V0ZSk7XG4gICAgICB9LFxuICAgICAgb25BYm9ydFxuICAgICk7XG4gIH07XG5cbiAgQWJzdHJhY3RIaXN0b3J5LnByb3RvdHlwZS5nbyA9IGZ1bmN0aW9uIGdvIChuKSB7XG4gICAgdmFyIHRoaXMkMSA9IHRoaXM7XG5cbiAgICB2YXIgdGFyZ2V0SW5kZXggPSB0aGlzLmluZGV4ICsgbjtcbiAgICBpZiAodGFyZ2V0SW5kZXggPCAwIHx8IHRhcmdldEluZGV4ID49IHRoaXMuc3RhY2subGVuZ3RoKSB7XG4gICAgICByZXR1cm5cbiAgICB9XG4gICAgdmFyIHJvdXRlID0gdGhpcy5zdGFja1t0YXJnZXRJbmRleF07XG4gICAgdGhpcy5jb25maXJtVHJhbnNpdGlvbihcbiAgICAgIHJvdXRlLFxuICAgICAgZnVuY3Rpb24gKCkge1xuICAgICAgICB0aGlzJDEuaW5kZXggPSB0YXJnZXRJbmRleDtcbiAgICAgICAgdGhpcyQxLnVwZGF0ZVJvdXRlKHJvdXRlKTtcbiAgICAgIH0sXG4gICAgICBmdW5jdGlvbiAoZXJyKSB7XG4gICAgICAgIGlmIChpc0V4dGVuZGVkRXJyb3IoTmF2aWdhdGlvbkR1cGxpY2F0ZWQsIGVycikpIHtcbiAgICAgICAgICB0aGlzJDEuaW5kZXggPSB0YXJnZXRJbmRleDtcbiAgICAgICAgfVxuICAgICAgfVxuICAgICk7XG4gIH07XG5cbiAgQWJzdHJhY3RIaXN0b3J5LnByb3RvdHlwZS5nZXRDdXJyZW50TG9jYXRpb24gPSBmdW5jdGlvbiBnZXRDdXJyZW50TG9jYXRpb24gKCkge1xuICAgIHZhciBjdXJyZW50ID0gdGhpcy5zdGFja1t0aGlzLnN0YWNrLmxlbmd0aCAtIDFdO1xuICAgIHJldHVybiBjdXJyZW50ID8gY3VycmVudC5mdWxsUGF0aCA6ICcvJ1xuICB9O1xuXG4gIEFic3RyYWN0SGlzdG9yeS5wcm90b3R5cGUuZW5zdXJlVVJMID0gZnVuY3Rpb24gZW5zdXJlVVJMICgpIHtcbiAgICAvLyBub29wXG4gIH07XG5cbiAgcmV0dXJuIEFic3RyYWN0SGlzdG9yeTtcbn0oSGlzdG9yeSkpO1xuXG4vKiAgKi9cblxuXG5cbnZhciBWdWVSb3V0ZXIgPSBmdW5jdGlvbiBWdWVSb3V0ZXIgKG9wdGlvbnMpIHtcbiAgaWYgKCBvcHRpb25zID09PSB2b2lkIDAgKSBvcHRpb25zID0ge307XG5cbiAgdGhpcy5hcHAgPSBudWxsO1xuICB0aGlzLmFwcHMgPSBbXTtcbiAgdGhpcy5vcHRpb25zID0gb3B0aW9ucztcbiAgdGhpcy5iZWZvcmVIb29rcyA9IFtdO1xuICB0aGlzLnJlc29sdmVIb29rcyA9IFtdO1xuICB0aGlzLmFmdGVySG9va3MgPSBbXTtcbiAgdGhpcy5tYXRjaGVyID0gY3JlYXRlTWF0Y2hlcihvcHRpb25zLnJvdXRlcyB8fCBbXSwgdGhpcyk7XG5cbiAgdmFyIG1vZGUgPSBvcHRpb25zLm1vZGUgfHwgJ2hhc2gnO1xuICB0aGlzLmZhbGxiYWNrID0gbW9kZSA9PT0gJ2hpc3RvcnknICYmICFzdXBwb3J0c1B1c2hTdGF0ZSAmJiBvcHRpb25zLmZhbGxiYWNrICE9PSBmYWxzZTtcbiAgaWYgKHRoaXMuZmFsbGJhY2spIHtcbiAgICBtb2RlID0gJ2hhc2gnO1xuICB9XG4gIGlmICghaW5Ccm93c2VyKSB7XG4gICAgbW9kZSA9ICdhYnN0cmFjdCc7XG4gIH1cbiAgdGhpcy5tb2RlID0gbW9kZTtcblxuICBzd2l0Y2ggKG1vZGUpIHtcbiAgICBjYXNlICdoaXN0b3J5JzpcbiAgICAgIHRoaXMuaGlzdG9yeSA9IG5ldyBIVE1MNUhpc3RvcnkodGhpcywgb3B0aW9ucy5iYXNlKTtcbiAgICAgIGJyZWFrXG4gICAgY2FzZSAnaGFzaCc6XG4gICAgICB0aGlzLmhpc3RvcnkgPSBuZXcgSGFzaEhpc3RvcnkodGhpcywgb3B0aW9ucy5iYXNlLCB0aGlzLmZhbGxiYWNrKTtcbiAgICAgIGJyZWFrXG4gICAgY2FzZSAnYWJzdHJhY3QnOlxuICAgICAgdGhpcy5oaXN0b3J5ID0gbmV3IEFic3RyYWN0SGlzdG9yeSh0aGlzLCBvcHRpb25zLmJhc2UpO1xuICAgICAgYnJlYWtcbiAgICBkZWZhdWx0OlxuICAgICAgaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcbiAgICAgICAgYXNzZXJ0KGZhbHNlLCAoXCJpbnZhbGlkIG1vZGU6IFwiICsgbW9kZSkpO1xuICAgICAgfVxuICB9XG59O1xuXG52YXIgcHJvdG90eXBlQWNjZXNzb3JzID0geyBjdXJyZW50Um91dGU6IHsgY29uZmlndXJhYmxlOiB0cnVlIH0gfTtcblxuVnVlUm91dGVyLnByb3RvdHlwZS5tYXRjaCA9IGZ1bmN0aW9uIG1hdGNoIChcbiAgcmF3LFxuICBjdXJyZW50LFxuICByZWRpcmVjdGVkRnJvbVxuKSB7XG4gIHJldHVybiB0aGlzLm1hdGNoZXIubWF0Y2gocmF3LCBjdXJyZW50LCByZWRpcmVjdGVkRnJvbSlcbn07XG5cbnByb3RvdHlwZUFjY2Vzc29ycy5jdXJyZW50Um91dGUuZ2V0ID0gZnVuY3Rpb24gKCkge1xuICByZXR1cm4gdGhpcy5oaXN0b3J5ICYmIHRoaXMuaGlzdG9yeS5jdXJyZW50XG59O1xuXG5WdWVSb3V0ZXIucHJvdG90eXBlLmluaXQgPSBmdW5jdGlvbiBpbml0IChhcHAgLyogVnVlIGNvbXBvbmVudCBpbnN0YW5jZSAqLykge1xuICAgIHZhciB0aGlzJDEgPSB0aGlzO1xuXG4gIHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicgJiYgYXNzZXJ0KFxuICAgIGluc3RhbGwuaW5zdGFsbGVkLFxuICAgIFwibm90IGluc3RhbGxlZC4gTWFrZSBzdXJlIHRvIGNhbGwgYFZ1ZS51c2UoVnVlUm91dGVyKWAgXCIgK1xuICAgIFwiYmVmb3JlIGNyZWF0aW5nIHJvb3QgaW5zdGFuY2UuXCJcbiAgKTtcblxuICB0aGlzLmFwcHMucHVzaChhcHApO1xuXG4gIC8vIHNldCB1cCBhcHAgZGVzdHJveWVkIGhhbmRsZXJcbiAgLy8gaHR0cHM6Ly9naXRodWIuY29tL3Z1ZWpzL3Z1ZS1yb3V0ZXIvaXNzdWVzLzI2MzlcbiAgYXBwLiRvbmNlKCdob29rOmRlc3Ryb3llZCcsIGZ1bmN0aW9uICgpIHtcbiAgICAvLyBjbGVhbiBvdXQgYXBwIGZyb20gdGhpcy5hcHBzIGFycmF5IG9uY2UgZGVzdHJveWVkXG4gICAgdmFyIGluZGV4ID0gdGhpcyQxLmFwcHMuaW5kZXhPZihhcHApO1xuICAgIGlmIChpbmRleCA+IC0xKSB7IHRoaXMkMS5hcHBzLnNwbGljZShpbmRleCwgMSk7IH1cbiAgICAvLyBlbnN1cmUgd2Ugc3RpbGwgaGF2ZSBhIG1haW4gYXBwIG9yIG51bGwgaWYgbm8gYXBwc1xuICAgIC8vIHdlIGRvIG5vdCByZWxlYXNlIHRoZSByb3V0ZXIgc28gaXQgY2FuIGJlIHJldXNlZFxuICAgIGlmICh0aGlzJDEuYXBwID09PSBhcHApIHsgdGhpcyQxLmFwcCA9IHRoaXMkMS5hcHBzWzBdIHx8IG51bGw7IH1cbiAgfSk7XG5cbiAgLy8gbWFpbiBhcHAgcHJldmlvdXNseSBpbml0aWFsaXplZFxuICAvLyByZXR1cm4gYXMgd2UgZG9uJ3QgbmVlZCB0byBzZXQgdXAgbmV3IGhpc3RvcnkgbGlzdGVuZXJcbiAgaWYgKHRoaXMuYXBwKSB7XG4gICAgcmV0dXJuXG4gIH1cblxuICB0aGlzLmFwcCA9IGFwcDtcblxuICB2YXIgaGlzdG9yeSA9IHRoaXMuaGlzdG9yeTtcblxuICBpZiAoaGlzdG9yeSBpbnN0YW5jZW9mIEhUTUw1SGlzdG9yeSkge1xuICAgIGhpc3RvcnkudHJhbnNpdGlvblRvKGhpc3RvcnkuZ2V0Q3VycmVudExvY2F0aW9uKCkpO1xuICB9IGVsc2UgaWYgKGhpc3RvcnkgaW5zdGFuY2VvZiBIYXNoSGlzdG9yeSkge1xuICAgIHZhciBzZXR1cEhhc2hMaXN0ZW5lciA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIGhpc3Rvcnkuc2V0dXBMaXN0ZW5lcnMoKTtcbiAgICB9O1xuICAgIGhpc3RvcnkudHJhbnNpdGlvblRvKFxuICAgICAgaGlzdG9yeS5nZXRDdXJyZW50TG9jYXRpb24oKSxcbiAgICAgIHNldHVwSGFzaExpc3RlbmVyLFxuICAgICAgc2V0dXBIYXNoTGlzdGVuZXJcbiAgICApO1xuICB9XG5cbiAgaGlzdG9yeS5saXN0ZW4oZnVuY3Rpb24gKHJvdXRlKSB7XG4gICAgdGhpcyQxLmFwcHMuZm9yRWFjaChmdW5jdGlvbiAoYXBwKSB7XG4gICAgICBhcHAuX3JvdXRlID0gcm91dGU7XG4gICAgfSk7XG4gIH0pO1xufTtcblxuVnVlUm91dGVyLnByb3RvdHlwZS5iZWZvcmVFYWNoID0gZnVuY3Rpb24gYmVmb3JlRWFjaCAoZm4pIHtcbiAgcmV0dXJuIHJlZ2lzdGVySG9vayh0aGlzLmJlZm9yZUhvb2tzLCBmbilcbn07XG5cblZ1ZVJvdXRlci5wcm90b3R5cGUuYmVmb3JlUmVzb2x2ZSA9IGZ1bmN0aW9uIGJlZm9yZVJlc29sdmUgKGZuKSB7XG4gIHJldHVybiByZWdpc3Rlckhvb2sodGhpcy5yZXNvbHZlSG9va3MsIGZuKVxufTtcblxuVnVlUm91dGVyLnByb3RvdHlwZS5hZnRlckVhY2ggPSBmdW5jdGlvbiBhZnRlckVhY2ggKGZuKSB7XG4gIHJldHVybiByZWdpc3Rlckhvb2sodGhpcy5hZnRlckhvb2tzLCBmbilcbn07XG5cblZ1ZVJvdXRlci5wcm90b3R5cGUub25SZWFkeSA9IGZ1bmN0aW9uIG9uUmVhZHkgKGNiLCBlcnJvckNiKSB7XG4gIHRoaXMuaGlzdG9yeS5vblJlYWR5KGNiLCBlcnJvckNiKTtcbn07XG5cblZ1ZVJvdXRlci5wcm90b3R5cGUub25FcnJvciA9IGZ1bmN0aW9uIG9uRXJyb3IgKGVycm9yQ2IpIHtcbiAgdGhpcy5oaXN0b3J5Lm9uRXJyb3IoZXJyb3JDYik7XG59O1xuXG5WdWVSb3V0ZXIucHJvdG90eXBlLnB1c2ggPSBmdW5jdGlvbiBwdXNoIChsb2NhdGlvbiwgb25Db21wbGV0ZSwgb25BYm9ydCkge1xuICAgIHZhciB0aGlzJDEgPSB0aGlzO1xuXG4gIC8vICRmbG93LWRpc2FibGUtbGluZVxuICBpZiAoIW9uQ29tcGxldGUgJiYgIW9uQWJvcnQgJiYgdHlwZW9mIFByb21pc2UgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKGZ1bmN0aW9uIChyZXNvbHZlLCByZWplY3QpIHtcbiAgICAgIHRoaXMkMS5oaXN0b3J5LnB1c2gobG9jYXRpb24sIHJlc29sdmUsIHJlamVjdCk7XG4gICAgfSlcbiAgfSBlbHNlIHtcbiAgICB0aGlzLmhpc3RvcnkucHVzaChsb2NhdGlvbiwgb25Db21wbGV0ZSwgb25BYm9ydCk7XG4gIH1cbn07XG5cblZ1ZVJvdXRlci5wcm90b3R5cGUucmVwbGFjZSA9IGZ1bmN0aW9uIHJlcGxhY2UgKGxvY2F0aW9uLCBvbkNvbXBsZXRlLCBvbkFib3J0KSB7XG4gICAgdmFyIHRoaXMkMSA9IHRoaXM7XG5cbiAgLy8gJGZsb3ctZGlzYWJsZS1saW5lXG4gIGlmICghb25Db21wbGV0ZSAmJiAhb25BYm9ydCAmJiB0eXBlb2YgUHJvbWlzZSAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICByZXR1cm4gbmV3IFByb21pc2UoZnVuY3Rpb24gKHJlc29sdmUsIHJlamVjdCkge1xuICAgICAgdGhpcyQxLmhpc3RvcnkucmVwbGFjZShsb2NhdGlvbiwgcmVzb2x2ZSwgcmVqZWN0KTtcbiAgICB9KVxuICB9IGVsc2Uge1xuICAgIHRoaXMuaGlzdG9yeS5yZXBsYWNlKGxvY2F0aW9uLCBvbkNvbXBsZXRlLCBvbkFib3J0KTtcbiAgfVxufTtcblxuVnVlUm91dGVyLnByb3RvdHlwZS5nbyA9IGZ1bmN0aW9uIGdvIChuKSB7XG4gIHRoaXMuaGlzdG9yeS5nbyhuKTtcbn07XG5cblZ1ZVJvdXRlci5wcm90b3R5cGUuYmFjayA9IGZ1bmN0aW9uIGJhY2sgKCkge1xuICB0aGlzLmdvKC0xKTtcbn07XG5cblZ1ZVJvdXRlci5wcm90b3R5cGUuZm9yd2FyZCA9IGZ1bmN0aW9uIGZvcndhcmQgKCkge1xuICB0aGlzLmdvKDEpO1xufTtcblxuVnVlUm91dGVyLnByb3RvdHlwZS5nZXRNYXRjaGVkQ29tcG9uZW50cyA9IGZ1bmN0aW9uIGdldE1hdGNoZWRDb21wb25lbnRzICh0bykge1xuICB2YXIgcm91dGUgPSB0b1xuICAgID8gdG8ubWF0Y2hlZFxuICAgICAgPyB0b1xuICAgICAgOiB0aGlzLnJlc29sdmUodG8pLnJvdXRlXG4gICAgOiB0aGlzLmN1cnJlbnRSb3V0ZTtcbiAgaWYgKCFyb3V0ZSkge1xuICAgIHJldHVybiBbXVxuICB9XG4gIHJldHVybiBbXS5jb25jYXQuYXBwbHkoW10sIHJvdXRlLm1hdGNoZWQubWFwKGZ1bmN0aW9uIChtKSB7XG4gICAgcmV0dXJuIE9iamVjdC5rZXlzKG0uY29tcG9uZW50cykubWFwKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgIHJldHVybiBtLmNvbXBvbmVudHNba2V5XVxuICAgIH0pXG4gIH0pKVxufTtcblxuVnVlUm91dGVyLnByb3RvdHlwZS5yZXNvbHZlID0gZnVuY3Rpb24gcmVzb2x2ZSAoXG4gIHRvLFxuICBjdXJyZW50LFxuICBhcHBlbmRcbikge1xuICBjdXJyZW50ID0gY3VycmVudCB8fCB0aGlzLmhpc3RvcnkuY3VycmVudDtcbiAgdmFyIGxvY2F0aW9uID0gbm9ybWFsaXplTG9jYXRpb24oXG4gICAgdG8sXG4gICAgY3VycmVudCxcbiAgICBhcHBlbmQsXG4gICAgdGhpc1xuICApO1xuICB2YXIgcm91dGUgPSB0aGlzLm1hdGNoKGxvY2F0aW9uLCBjdXJyZW50KTtcbiAgdmFyIGZ1bGxQYXRoID0gcm91dGUucmVkaXJlY3RlZEZyb20gfHwgcm91dGUuZnVsbFBhdGg7XG4gIHZhciBiYXNlID0gdGhpcy5oaXN0b3J5LmJhc2U7XG4gIHZhciBocmVmID0gY3JlYXRlSHJlZihiYXNlLCBmdWxsUGF0aCwgdGhpcy5tb2RlKTtcbiAgcmV0dXJuIHtcbiAgICBsb2NhdGlvbjogbG9jYXRpb24sXG4gICAgcm91dGU6IHJvdXRlLFxuICAgIGhyZWY6IGhyZWYsXG4gICAgLy8gZm9yIGJhY2t3YXJkcyBjb21wYXRcbiAgICBub3JtYWxpemVkVG86IGxvY2F0aW9uLFxuICAgIHJlc29sdmVkOiByb3V0ZVxuICB9XG59O1xuXG5WdWVSb3V0ZXIucHJvdG90eXBlLmFkZFJvdXRlcyA9IGZ1bmN0aW9uIGFkZFJvdXRlcyAocm91dGVzKSB7XG4gIHRoaXMubWF0Y2hlci5hZGRSb3V0ZXMocm91dGVzKTtcbiAgaWYgKHRoaXMuaGlzdG9yeS5jdXJyZW50ICE9PSBTVEFSVCkge1xuICAgIHRoaXMuaGlzdG9yeS50cmFuc2l0aW9uVG8odGhpcy5oaXN0b3J5LmdldEN1cnJlbnRMb2NhdGlvbigpKTtcbiAgfVxufTtcblxuT2JqZWN0LmRlZmluZVByb3BlcnRpZXMoIFZ1ZVJvdXRlci5wcm90b3R5cGUsIHByb3RvdHlwZUFjY2Vzc29ycyApO1xuXG5mdW5jdGlvbiByZWdpc3Rlckhvb2sgKGxpc3QsIGZuKSB7XG4gIGxpc3QucHVzaChmbik7XG4gIHJldHVybiBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIGkgPSBsaXN0LmluZGV4T2YoZm4pO1xuICAgIGlmIChpID4gLTEpIHsgbGlzdC5zcGxpY2UoaSwgMSk7IH1cbiAgfVxufVxuXG5mdW5jdGlvbiBjcmVhdGVIcmVmIChiYXNlLCBmdWxsUGF0aCwgbW9kZSkge1xuICB2YXIgcGF0aCA9IG1vZGUgPT09ICdoYXNoJyA/ICcjJyArIGZ1bGxQYXRoIDogZnVsbFBhdGg7XG4gIHJldHVybiBiYXNlID8gY2xlYW5QYXRoKGJhc2UgKyAnLycgKyBwYXRoKSA6IHBhdGhcbn1cblxuVnVlUm91dGVyLmluc3RhbGwgPSBpbnN0YWxsO1xuVnVlUm91dGVyLnZlcnNpb24gPSAnMy4xLjMnO1xuXG5pZiAoaW5Ccm93c2VyICYmIHdpbmRvdy5WdWUpIHtcbiAgd2luZG93LlZ1ZS51c2UoVnVlUm91dGVyKTtcbn1cblxuZXhwb3J0IGRlZmF1bHQgVnVlUm91dGVyO1xuIiwidmFyIG1hcCA9IHtcblx0XCIuL2xvZ1wiOiBcIi4vbm9kZV9tb2R1bGVzL3dlYnBhY2svaG90L2xvZy5qc1wiXG59O1xuXG5cbmZ1bmN0aW9uIHdlYnBhY2tDb250ZXh0KHJlcSkge1xuXHR2YXIgaWQgPSB3ZWJwYWNrQ29udGV4dFJlc29sdmUocmVxKTtcblx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oaWQpO1xufVxuZnVuY3Rpb24gd2VicGFja0NvbnRleHRSZXNvbHZlKHJlcSkge1xuXHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKG1hcCwgcmVxKSkge1xuXHRcdHZhciBlID0gbmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIiArIHJlcSArIFwiJ1wiKTtcblx0XHRlLmNvZGUgPSAnTU9EVUxFX05PVF9GT1VORCc7XG5cdFx0dGhyb3cgZTtcblx0fVxuXHRyZXR1cm4gbWFwW3JlcV07XG59XG53ZWJwYWNrQ29udGV4dC5rZXlzID0gZnVuY3Rpb24gd2VicGFja0NvbnRleHRLZXlzKCkge1xuXHRyZXR1cm4gT2JqZWN0LmtleXMobWFwKTtcbn07XG53ZWJwYWNrQ29udGV4dC5yZXNvbHZlID0gd2VicGFja0NvbnRleHRSZXNvbHZlO1xubW9kdWxlLmV4cG9ydHMgPSB3ZWJwYWNrQ29udGV4dDtcbndlYnBhY2tDb250ZXh0LmlkID0gXCIuL25vZGVfbW9kdWxlcy93ZWJwYWNrL2hvdCBzeW5jIF5cXFxcLlxcXFwvbG9nJFwiOyIsImV4cG9ydCBkZWZhdWx0IHtcclxuICBuYW1lOiAndmknLFxyXG4gIG1lc3NhZ2VzOiB7XHJcbiAgICBfZGVmYXVsdDogZnVuY3Rpb24gKG4pIHtcclxuICAgICAgcmV0dXJuICdHacOhIHRy4buLIGPhu6dhICcgKyBuICsgJyBraMO0bmcgxJHDum5nLic7XHJcbiAgICB9LFxyXG4gICAgYWZ0ZXI6IGZ1bmN0aW9uIChuLCB0KSB7XHJcbiAgICAgIHJldHVybiBuICsgJyBwaOG6o2kgeHXhuqV0IGhp4buHbiBzYXUgJyArIHQubGVuZ3RoICsgJy4nO1xyXG4gICAgfSxcclxuICAgIGFscGhhX2Rhc2g6IGZ1bmN0aW9uIChuKSB7XHJcbiAgICAgIHJldHVybiBuICsgJyBjw7MgdGjhu4MgY2jhu6lhIGPDoWMga8OtIHThu7EgY2jhu68gKEEtWiBhLXopLCBz4buRICgwLTkpLCBn4bqhY2ggbmdhbmcgKC0pIHbDoCBn4bqhY2ggZMaw4bubaSAoXykuJztcclxuICAgIH0sXHJcbiAgICBhbHBoYV9udW06IGZ1bmN0aW9uIChuKSB7XHJcbiAgICAgIHJldHVybiBuICsgJyBjaOG7iSBjw7MgdGjhu4MgY2jhu6lhIGPDoWMga8OtIHThu7EgY2jhu68gdsOgIHPhu5EuJztcclxuICAgIH0sXHJcbiAgICBhbHBoYV9zcGFjZXM6IGZ1bmN0aW9uIChuKSB7XHJcbiAgICAgIHJldHVybiBuICsgJyBjaOG7iSBjw7MgdGjhur8gY2jhu6lhIGPDoWMga8OtIHThu7EgdsOgIGtob+G6o25nIHRy4bqvbmcnO1xyXG4gICAgfSxcclxuICAgIGFscGhhOiBmdW5jdGlvbiAobikge1xyXG4gICAgICByZXR1cm4gbiArICcgY2jhu4kgY8OzIHRo4buDIGNo4bupYSBjw6FjIGvDrSB04buxIGNo4buvLic7XHJcbiAgICB9LFxyXG4gICAgYmVmb3JlOiBmdW5jdGlvbiAobiwgdCkge1xyXG4gICAgICByZXR1cm4gbiArICcgcGjhuqNpIHh14bqldCBoaeG7h24gdHLGsOG7m2MgJyArIHQubGVuZ3RoICsgJy4nO1xyXG4gICAgfSxcclxuICAgIGJldHdlZW46IGZ1bmN0aW9uIChuLCB0KSB7XHJcbiAgICAgIHJldHVybiBuICsgJyBwaOG6o2kgY8OzIGdpw6EgdHLhu4sgbuG6sW0gdHJvbmcga2hv4bqjbmcgZ2nhu69hICcgKyB0Lmxlbmd0aCArICcgdsOgICcgKyB0WyAxIF0gKyAnLic7XHJcbiAgICB9LFxyXG4gICAgY29uZmlybWVkOiBmdW5jdGlvbiAobiwgdCkge1xyXG4gICAgICByZXR1cm4gbiArICcga2jDoWMgduG7m2kgJyArIHQubGVuZ3RoICsgJy4nO1xyXG4gICAgfSxcclxuICAgIGNyZWRpdF9jYXJkOiBmdW5jdGlvbiAobikge1xyXG4gICAgICByZXR1cm4gJ8SQw6MgxJFp4buBbiAnICsgbiArICcga2jDtG5nIGNow61uaCB4w6FjLic7XHJcbiAgICB9LFxyXG4gICAgZGF0ZV9iZXR3ZWVuOiBmdW5jdGlvbiAobiwgdCkge1xyXG4gICAgICByZXR1cm4gbiArICcgcGjhuqNpIGPDsyBnacOhIHRy4buLIG7hurFtIHRyb25nIGtob+G6o25nIGdp4buvYSAgJyArIHQubGVuZ3RoICsgJyB2w6AgJyArIHRbIDEgXSArICcuJztcclxuICAgIH0sXHJcbiAgICBkYXRlX2Zvcm1hdDogZnVuY3Rpb24gKG4sIHQpIHtcclxuICAgICAgcmV0dXJuIG4gKyAnIHBo4bqjaSBjw7MgZ2nDoSB0cuG7iyBkxrDhu5tpIMSR4buLbmggZOG6oW5nICcgKyB0Lmxlbmd0aCArICcuJztcclxuICAgIH0sXHJcbiAgICBkZWNpbWFsOiBmdW5jdGlvbiAobiwgdCkge1xyXG4gICAgICB2b2lkIDAgPT09IHQgJiYgKHQgPSBbXSk7XHJcbiAgICAgIHZhciBjID0gdC5sZW5ndGg7XHJcbiAgICAgIHJldHVybiB2b2lkIDAgPT09IGMgJiYgKGMgPSAnKicpLCBuICsgJyBjaOG7iSBjw7MgdGjhu4MgY2jhu6lhIGPDoWMga8OtIHThu7Egc+G7kSB2w6AgZOG6pXUgdGjhuq1wIHBow6JuICcgKyAoJyonID09PSBjID8gJycgOiBjKSArICcuJztcclxuICAgIH0sXHJcbiAgICBkaWdpdHM6IGZ1bmN0aW9uIChuLCB0KSB7XHJcbiAgICAgIHJldHVybiAnVHLGsOG7nW5nICcgKyBuICsgJyBjaOG7iSBjw7MgdGjhu4MgY2jhu6lhIGPDoWMga8OtIHThu7Egc+G7kSB2w6AgYuG6r3QgYnXhu5ljIHBo4bqjaSBjw7MgxJHhu5kgZMOgaSBsw6AgJyArIHQubGVuZ3RoICsgJy4nO1xyXG4gICAgfSxcclxuICAgIGRpbWVuc2lvbnM6IGZ1bmN0aW9uIChuLCB0KSB7XHJcbiAgICAgIHJldHVybiBuICsgJyBwaOG6o2kgY8OzIGNoaeG7gXUgcuG7mW5nICcgKyB0Lmxlbmd0aCArICcgcGl4ZWxzIHbDoCBjaGnhu4F1IGNhbyAnICsgdFsgMSBdICsgJyBwaXhlbHMuJztcclxuICAgIH0sXHJcbiAgICBlbWFpbDogZnVuY3Rpb24gKG4pIHtcclxuICAgICAgcmV0dXJuIG4gKyAnIHBo4bqjaSBsw6AgbeG7mXQgxJHhu4thIGNo4buJIGVtYWlsIGjhu6NwIGzhu4cuJztcclxuICAgIH0sXHJcbiAgICBleHQ6IGZ1bmN0aW9uIChuKSB7XHJcbiAgICAgIHJldHVybiBuICsgJyBwaOG6o2kgbMOgIG3hu5l0IHThu4dwLic7XHJcbiAgICB9LFxyXG4gICAgaW1hZ2U6IGZ1bmN0aW9uIChuKSB7XHJcbiAgICAgIHJldHVybiAnVHLGsOG7nW5nICcgKyBuICsgJyBwaOG6o2kgbMOgIG3hu5l0IOG6o25oLic7XHJcbiAgICB9LFxyXG4gICAgaW5jbHVkZWQ6IGZ1bmN0aW9uIChuKSB7XHJcbiAgICAgIHJldHVybiBuICsgJyBwaOG6o2kgbMOgIG3hu5l0IGdpw6EgdHLhu4suJztcclxuICAgIH0sXHJcbiAgICBpcDogZnVuY3Rpb24gKG4pIHtcclxuICAgICAgcmV0dXJuIG4gKyAnIHBo4bqjaSBsw6AgbeG7mXQgxJHhu4thIGNo4buJIGlwIGjhu6NwIGzhu4cuJztcclxuICAgIH0sXHJcbiAgICBtYXg6IGZ1bmN0aW9uIChuLCB0KSB7XHJcbiAgICAgIHJldHVybiBuICsgJyBraMO0bmcgdGjhu4MgY8OzIG5oaeG7gXUgaMahbiAnICsgdC5sZW5ndGggKyAnIGvDrSB04buxLic7XHJcbiAgICB9LFxyXG4gICAgbWF4X3ZhbHVlOiBmdW5jdGlvbiAobiwgdCkge1xyXG4gICAgICByZXR1cm4gbiArICcgcGjhuqNpIG5o4buPIGjGoW4gaG/hurdjIGLhurFuZyAnICsgdC5sZW5ndGggKyAnLic7XHJcbiAgICB9LFxyXG4gICAgbWltZXM6IGZ1bmN0aW9uIChuKSB7XHJcbiAgICAgIHJldHVybiBuICsgJyBwaOG6o2kgY2jhu6lhIGtp4buDdSB04buHcCBwaMO5IGjhu6NwLic7XHJcbiAgICB9LFxyXG4gICAgbWluOiBmdW5jdGlvbiAobiwgdCkge1xyXG4gICAgICByZXR1cm4gbiArICcgcGjhuqNpIGNo4bupYSDDrXQgbmjhuqV0ICcgKyB0Lmxlbmd0aCArICcga8OtIHThu7EuJztcclxuICAgIH0sXHJcbiAgICBtaW5fdmFsdWU6IGZ1bmN0aW9uIChuLCB0KSB7XHJcbiAgICAgIHJldHVybiBuICsgJyBwaOG6o2kgbOG7m24gaMahbiBob+G6t2MgYuG6sW5nICcgKyB0Lmxlbmd0aCArICcuJztcclxuICAgIH0sXHJcbiAgICBleGNsdWRlZDogZnVuY3Rpb24gKG4pIHtcclxuICAgICAgcmV0dXJuIG4gKyAnIHBo4bqjaSBjaOG7qWEgbeG7mXQgZ2nDoSB0cuG7iyBo4bujcCBs4buHLic7XHJcbiAgICB9LFxyXG4gICAgbnVtZXJpYzogZnVuY3Rpb24gKG4pIHtcclxuICAgICAgcmV0dXJuIG4gKyAnIGNo4buJIGPDsyB0aOG7gyBjw7MgY8OhYyBrw60gdOG7sSBz4buRLic7XHJcbiAgICB9LFxyXG4gICAgcmVnZXg6IGZ1bmN0aW9uIChuKSB7XHJcbiAgICAgIHJldHVybiBuICsgJyBjw7MgxJHhu4tuaCBk4bqhbmcga2jDtG5nIMSRw7puZy4nO1xyXG4gICAgfSxcclxuICAgIHJlcXVpcmVkOiBmdW5jdGlvbiAobikge1xyXG4gICAgICByZXR1cm4gbiArICcga2jDtG5nIMSRxrDhu6NjIMSR4buDIHRy4buRbmcuJztcclxuICAgIH0sXHJcbiAgICBzaXplOiBmdW5jdGlvbiAobiwgdCkge1xyXG4gICAgICB2YXIgYyxcclxuICAgICAgICAgIGUsXHJcbiAgICAgICAgICBpLFxyXG4gICAgICAgICAgaCA9IHQubGVuZ3RoO1xyXG4gICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgbiArXHJcbiAgICAgICAgICAnIGNo4buJIGPDsyB0aOG7gyBjaOG7qWEgdOG7h3Agbmjhu48gaMahbiAnICtcclxuICAgICAgICAgICgoYyA9IGgpLFxyXG4gICAgICAgICAgICAgIChlID0gMTAyNCksXHJcbiAgICAgICAgICAgICAgKGkgPSAwID09IChjID0gTnVtYmVyKGMpICogZSkgPyAwIDogTWF0aC5mbG9vcihNYXRoLmxvZyhjKSAvIE1hdGgubG9nKGUpKSksXHJcbiAgICAgICAgICAxICogKGMgLyBNYXRoLnBvdyhlLCBpKSkudG9GaXhlZCgyKSArICcgJyArIFsgJ0J5dGUnLCAnS0InLCAnTUInLCAnR0InLCAnVEInLCAnUEInLCAnRUInLCAnWkInLCAnWUInIF1bIGkgXSkgK1xyXG4gICAgICAgICAgJy4nXHJcbiAgICAgICk7XHJcbiAgICB9LFxyXG4gICAgdXJsOiBmdW5jdGlvbiAobikge1xyXG4gICAgICByZXR1cm4gbiArICcga2jDtG5nIHBo4bqjaSBsw6AgbeG7mXQgxJHhu4thIGNo4buJIFVSTCBo4bujcCBs4buHLic7XHJcbiAgICB9LFxyXG4gIH0sXHJcbn07IiwiaW1wb3J0ICQgZnJvbSBcImpxdWVyeVwiXHJcbmltcG9ydCBWdWUgZnJvbSAndnVlJ1xyXG5pbXBvcnQgVnVleCBmcm9tICd2dWV4J1xyXG5pbXBvcnQgVnVlUm91dGVyIGZyb20gJ3Z1ZS1yb3V0ZXInXHJcblxyXG5pbXBvcnQgQXBwIGZyb20gJyR2dWVQYXJ0aWFsUGF0aC9yb2xlL0FwcCdcclxuaW1wb3J0IFJvbGVJbmRleFNlY3Rpb24gZnJvbSAnJHZ1ZVBhcnRpYWxQYXRoL3JvbGUvUm9sZUluZGV4U2VjdGlvbidcclxuaW1wb3J0IFJvbGVBZGRTZWN0aW9uIGZyb20gJyR2dWVQYXJ0aWFsUGF0aC9yb2xlL1JvbGVBZGRTZWN0aW9uJ1xyXG5pbXBvcnQgUm9sZUVkaXRTZWN0aW9uIGZyb20gJyR2dWVQYXJ0aWFsUGF0aC9yb2xlL1JvbGVFZGl0U2VjdGlvbidcclxuXHJcbmltcG9ydCBSb2xlVGFibGUgZnJvbSAnJHZ1ZVBhcnRpYWxQYXRoL3JvbGUvUm9sZVRhYmxlJ1xyXG5cclxuaW1wb3J0IFN0b3JlIGZyb20gJyRqc1N0b3JlUGF0aC9yb2xlJ1xyXG5cclxuVnVlLnVzZShWdWV4KVxyXG5WdWUudXNlKFZ1ZVJvdXRlcilcclxuXHJcbmNvbnN0IHN0b3JlID0gbmV3IFZ1ZXguU3RvcmUoU3RvcmUpXHJcbmNvbnN0IHJvdXRlciA9IG5ldyBWdWVSb3V0ZXIoe1xyXG4gIG1vZGU6ICdoaXN0b3J5JyxcclxuICByb3V0ZXM6IFtcclxuICAgIHsgbmFtZTogJ3JvbGU6aW5kZXgnLCBwYXRoOiAnL2JhY2tlbmQvcm9sZS9pbmRleC86cGFnZScsIGNvbXBvbmVudDogUm9sZUluZGV4U2VjdGlvbiB9LFxyXG4gICAgeyBuYW1lOiAncm9sZTphZGQnLCBwYXRoOiAnL2JhY2tlbmQvcm9sZS9hZGQvOnBhZ2UnLCBjb21wb25lbnQ6IFJvbGVBZGRTZWN0aW9uIH0sXHJcbiAgICB7IG5hbWU6ICdyb2xlOmVkaXQnLCBwYXRoOiAnL2JhY2tlbmQvcm9sZS9lZGl0LzppZC86cGFnZScsIGNvbXBvbmVudDogUm9sZUVkaXRTZWN0aW9uIH0sXHJcbiAgXVxyXG59KTtcclxuXHJcbmNvbnN0IE9iamVjdCA9ICgoKSA9PiB7XHJcbiAgY29uc3QgX3JlbmRlclJvbGVUYWJsZSA9ICgpID0+IHtcclxuICAgIGxldCAkZWwgPSAkKCcjcm9sZS1zZWN0aW9uJylcclxuXHJcbiAgICBuZXcgVnVlKHtcclxuICAgICAgcm91dGVyLFxyXG4gICAgICBzdG9yZSxcclxuICAgICAgcmVuZGVyOiBoID0+IGgoQXBwKVxyXG4gICAgfSkuJG1vdW50KCRlbFsgMCBdKVxyXG4gIH1cclxuXHJcbiAgcmV0dXJuIHtcclxuICAgIGluaXQoKSB7XHJcbiAgICAgIF9yZW5kZXJSb2xlVGFibGUoKVxyXG4gICAgfVxyXG4gIH1cclxufSkoKVxyXG5cclxuZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcIkRPTUNvbnRlbnRMb2FkZWRcIiwgZnVuY3Rpb24gKCkge1xyXG4gIE9iamVjdC5pbml0KClcclxufSk7IiwiaW1wb3J0IHsgZm9ySW4gYXMgX2ZvckluLCBpc0VtcHR5IGFzIF9pc0VtcHR5IH0gZnJvbSAnbG9kYXNoJztcclxuXHJcbmltcG9ydCAqIGFzIEFQSSBmcm9tICckanNDb21tb25QYXRoL2FwaSdcclxuaW1wb3J0IHsgaXNFcnJvckFQSSwgdHJpZ2dlck1vZGFsIH0gZnJvbSBcIiRqc0NvbW1vblBhdGgvdXRpbC9HZW5lcmFsVXRpbFwiO1xyXG5pbXBvcnQgeyBfaW50IH0gZnJvbSBcIiRqc0NvbW1vblBhdGgvdXRpbC9TYWZlVXRpbFwiO1xyXG5cclxubGV0IF9vYmpFbnRpdHkgPSB7XHJcbiAgcm9sZV9pZDogbnVsbCxcclxuICByb2xlX25hbWU6IG51bGwsXHJcbiAgaXNfZnVsbGFjY2VzczogbnVsbCxcclxuICBpc19kZWxldGVkOiBudWxsLFxyXG4gIGNyZWF0ZWRfYXQ6IDAsXHJcbiAgdXBkYXRlZF9hdDogMFxyXG59XHJcblxyXG5jbGFzcyBTdG9yZSB7XHJcbiAgY29uc3RydWN0b3IoKSB7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICBzdGF0ZToge1xyXG4gICAgICAgIGZsYWc6IHtcclxuICAgICAgICAgIGlzR2V0OiBmYWxzZVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgYXJyRW50aXR5TGlzdDogW10sXHJcbiAgICAgICAgb2JqRW50aXR5U2VsZWN0OiB7IC4uLl9vYmpFbnRpdHkgfSxcclxuICAgICAgfSxcclxuICAgICAgbXV0YXRpb25zOiB7XHJcbiAgICAgICAgcmVzZXRFbnRpdHkoc3RhdGUpIHtcclxuICAgICAgICAgIHN0YXRlLm9iakVudGl0eVNlbGVjdCA9IHsgLi4uX29iakVudGl0eSB9XHJcbiAgICAgICAgfSxcclxuICAgICAgICB1cGRhdGVGbGFnKHN0YXRlLCBwYXlsb2FkKSB7XHJcbiAgICAgICAgICBzdGF0ZS5mbGFnWyBwYXlsb2FkLmZsYWcgXSA9IHBheWxvYWQudmFsdWVcclxuICAgICAgICB9LFxyXG4gICAgICAgIHNldEVudGl0aWVzKHN0YXRlLCBwYXlsb2FkKSB7XHJcbiAgICAgICAgICBzdGF0ZS5hcnJFbnRpdHlMaXN0ID0gcGF5bG9hZC5hcnJFbnRpdHlMaXN0XHJcbiAgICAgICAgfSxcclxuICAgICAgICBzZXRFbnRpdHkoc3RhdGUsIHBheWxvYWQpIHtcclxuICAgICAgICAgIGlmIChfaXNFbXB0eShwYXlsb2FkLm9iakVudGl0eVNlbGVjdCkpXHJcbiAgICAgICAgICAgIHJldHVyblxyXG5cclxuICAgICAgICAgIGxldCB7IG9iakVudGl0eVNlbGVjdCB9ID0geyAuLi5wYXlsb2FkIH1cclxuXHJcbiAgICAgICAgICBvYmpFbnRpdHlTZWxlY3QucGFzc3dvcmQgPSBudWxsXHJcblxyXG4gICAgICAgICAgc3RhdGUub2JqRW50aXR5U2VsZWN0ID0gb2JqRW50aXR5U2VsZWN0XHJcbiAgICAgICAgfSxcclxuICAgICAgICB1cGRhdGVFbnRpdHkoc3RhdGUsIHBheWxvYWQpIHtcclxuICAgICAgICAgIGxldCB7IHR5cGUsIGluZGV4LCBhcnJFbnRpdHkgfSA9IHsgLi4ucGF5bG9hZCB9XHJcblxyXG4gICAgICAgICAgc3dpdGNoICh0eXBlKSB7XHJcbiAgICAgICAgICAgIGNhc2UgJ2NyZWF0ZSc6XHJcbiAgICAgICAgICAgICAgc3RhdGUuYXJyRW50aXR5TGlzdC5wdXNoKGFyckVudGl0eSlcclxuICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSAndXBkYXRlJzpcclxuICAgICAgICAgICAgICBzdGF0ZS5hcnJFbnRpdHlMaXN0WyBpbmRleCBdID0gYXJyRW50aXR5XHJcbiAgICAgICAgICAgICAgc3RhdGUub2JqRW50aXR5U2VsZWN0ID0gYXJyRW50aXR5XHJcblxyXG4gICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICdkZWxldGUnOlxyXG4gICAgICAgICAgICAgIHN0YXRlLmFyckVudGl0eUxpc3Quc3BsaWNlKGluZGV4LCAxKVxyXG4gICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfSxcclxuICAgICAgYWN0aW9uczoge1xyXG4gICAgICAgIGdldEVudGl0eUFjdCh7IGNvbW1pdCB9LCBwYXlsb2FkKSB7XHJcbiAgICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoZnVuY3Rpb24gKHJlc29sdmUsIHJlamVjdCkge1xyXG4gICAgICAgICAgICBsZXQgcmVxUGFyYW1zID0geyAuLi5wYXlsb2FkIH1cclxuICAgICAgICAgICAgQVBJLmdldFJvbGVSZXEocmVxUGFyYW1zKS5kb25lKChyZXMpID0+IHtcclxuICAgICAgICAgICAgICBpZiAoaXNFcnJvckFQSShyZXMpKSB7XHJcbiAgICAgICAgICAgICAgICByZWplY3QocmVzLnBheWxvYWQpXHJcbiAgICAgICAgICAgICAgICByZXR1cm5cclxuICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgIGxldCB7IGFyclJvbGVEZXRhaWwgfSA9IHsgLi4ucmVzLnBheWxvYWQgfVxyXG5cclxuICAgICAgICAgICAgICBjb21taXQoJ3NldEVudGl0eScsIHsgb2JqRW50aXR5U2VsZWN0OiBhcnJSb2xlRGV0YWlsIH0pXHJcblxyXG4gICAgICAgICAgICAgIHJlc29sdmUocmVzLnBheWxvYWQpXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICB9KVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgcmVhZEVudGl0eUFjdCh7IGNvbW1pdCB9KSB7XHJcbiAgICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoZnVuY3Rpb24gKHJlc29sdmUsIHJlamVjdCkge1xyXG4gICAgICAgICAgICBsZXQgcmVxUGFyYW1zID0ge31cclxuICAgICAgICAgICAgQVBJLnJlYWRSb2xlUmVxKHJlcVBhcmFtcykuZG9uZSgocmVzKSA9PiB7XHJcbiAgICAgICAgICAgICAgaWYgKGlzRXJyb3JBUEkocmVzKSkge1xyXG4gICAgICAgICAgICAgICAgcmVqZWN0KHJlcy5wYXlsb2FkKVxyXG4gICAgICAgICAgICAgICAgcmV0dXJuXHJcbiAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICBsZXQgeyBhcnJSb2xlTGlzdCB9ID0geyAuLi5yZXMucGF5bG9hZCB9XHJcblxyXG4gICAgICAgICAgICAgIGNvbW1pdCgnc2V0RW50aXRpZXMnLCB7IGFyckVudGl0eUxpc3Q6IGFyclJvbGVMaXN0IH0pXHJcblxyXG4gICAgICAgICAgICAgIHJlc29sdmUocmVzLnBheWxvYWQpXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICB9KVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgdXBsb2FkRmlsZUFjdCh7IGNvbW1pdCB9LCBwYXlsb2FkKSB7XHJcbiAgICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoZnVuY3Rpb24gKHJlc29sdmUsIHJlamVjdCkge1xyXG4gICAgICAgICAgICBsZXQgZm9ybURhdGEgPSBuZXcgRm9ybURhdGFcclxuXHJcbiAgICAgICAgICAgIGZvcm1EYXRhLmFwcGVuZCgnY29udHJvbGxlcicsIHBheWxvYWQuY29udHJvbGxlcilcclxuICAgICAgICAgICAgZm9ybURhdGEuYXBwZW5kKCdmaWxlJywgcGF5bG9hZC5maWxlKVxyXG5cclxuICAgICAgICAgICAgQVBJLnVwbG9hZFJlcShmb3JtRGF0YSwge1xyXG4gICAgICAgICAgICAgIHByb2Nlc3NEYXRhOiBmYWxzZSxcclxuICAgICAgICAgICAgICBjb250ZW50VHlwZTogZmFsc2UsXHJcbiAgICAgICAgICAgIH0pLmRvbmUocmVzID0+IHtcclxuICAgICAgICAgICAgICBpZiAoaXNFcnJvckFQSShyZXMpKSB7XHJcbiAgICAgICAgICAgICAgICB0cmlnZ2VyTW9kYWwocmVzLm1lc3NhZ2UpXHJcblxyXG4gICAgICAgICAgICAgICAgcmVqZWN0KHJlcy5wYXlsb2FkKVxyXG4gICAgICAgICAgICAgICAgcmV0dXJuXHJcbiAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICByZXNvbHZlKHJlcy5wYXlsb2FkKVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgfSlcclxuICAgICAgICB9LFxyXG4gICAgICAgIHNldEVudGl0eUFjdCh7IGNvbW1pdCwgZ2V0dGVycyB9LCBwYXlsb2FkKSB7XHJcbiAgICAgICAgICBpZiAoIXBheWxvYWQuaGFzT3duUHJvcGVydHkoJ2luZGV4JykpXHJcbiAgICAgICAgICAgIHJldHVyblxyXG5cclxuICAgICAgICAgIGxldCB7IGluZGV4IH0gPSB7IC4uLnBheWxvYWQgfVxyXG5cclxuICAgICAgICAgIGNvbW1pdCgnc2V0RW50aXR5JywgeyBvYmpFbnRpdHlTZWxlY3Q6IGdldHRlcnMuZ2V0RW50aXR5QnlJbmRleChpbmRleClbIDAgXSB9KVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgY3JlYXRlRW50aXR5QWN0KHsgY29tbWl0LCBnZXR0ZXJzIH0sIHBheWxvYWQpIHtcclxuICAgICAgICAgIHJldHVybiBuZXcgUHJvbWlzZShmdW5jdGlvbiAocmVzb2x2ZSwgcmVqZWN0KSB7XHJcbiAgICAgICAgICAgIGxldCByZXFQYXJhbXMgPSB7IC4uLnBheWxvYWQgfVxyXG4gICAgICAgICAgICBBUEkuY3JlYXRlUm9sZVJlcShyZXFQYXJhbXMpLmRvbmUoKHJlcykgPT4ge1xyXG4gICAgICAgICAgICAgIHRyaWdnZXJNb2RhbChyZXMubWVzc2FnZSlcclxuICAgICAgICAgICAgICBpZiAoaXNFcnJvckFQSShyZXMpKSB7XHJcbiAgICAgICAgICAgICAgICByZWplY3QocmVzLnBheWxvYWQpXHJcbiAgICAgICAgICAgICAgICByZXR1cm5cclxuICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgIGxldCB7IGFyclJvbGVEZXRhaWwgfSA9IHsgLi4ucmVzLnBheWxvYWQgfVxyXG5cclxuICAgICAgICAgICAgICBjb21taXQoJ3VwZGF0ZUVudGl0eScsIHtcclxuICAgICAgICAgICAgICAgIHR5cGU6ICdjcmVhdGUnLFxyXG4gICAgICAgICAgICAgICAgYXJyRW50aXR5OiBhcnJSb2xlRGV0YWlsXHJcbiAgICAgICAgICAgICAgfSlcclxuXHJcbiAgICAgICAgICAgICAgcmVzb2x2ZShyZXMucGF5bG9hZClcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgIH0pXHJcbiAgICAgICAgfSxcclxuICAgICAgICB1cGRhdGVFbnRpdHlBY3QoeyBjb21taXQsIGdldHRlcnMgfSwgcGF5bG9hZCkge1xyXG4gICAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKGZ1bmN0aW9uIChyZXNvbHZlLCByZWplY3QpIHtcclxuICAgICAgICAgICAgbGV0IHJlcVBhcmFtcyA9IHsgLi4ucGF5bG9hZCB9XHJcblxyXG4gICAgICAgICAgICBBUEkudXBkYXRlUm9sZVJlcShyZXFQYXJhbXMpLmRvbmUoKHJlcykgPT4ge1xyXG4gICAgICAgICAgICAgIHRyaWdnZXJNb2RhbChyZXMubWVzc2FnZSlcclxuICAgICAgICAgICAgICBpZiAoaXNFcnJvckFQSShyZXMpKSB7XHJcbiAgICAgICAgICAgICAgICByZWplY3QocmVzLnBheWxvYWQpXHJcbiAgICAgICAgICAgICAgICByZXR1cm5cclxuICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgIGxldCB7IGFyclJvbGVEZXRhaWwgfSA9IHsgLi4ucmVzLnBheWxvYWQgfVxyXG5cclxuICAgICAgICAgICAgICBjb21taXQoJ3VwZGF0ZUVudGl0eScsIHtcclxuICAgICAgICAgICAgICAgIHR5cGU6ICd1cGRhdGUnLFxyXG4gICAgICAgICAgICAgICAgaW5kZXg6IGdldHRlcnMuZ2V0SW5kZXhCeUlEKGFyclJvbGVEZXRhaWwucm9sZV9pZCksXHJcbiAgICAgICAgICAgICAgICBhcnJFbnRpdHk6IGFyclJvbGVEZXRhaWxcclxuICAgICAgICAgICAgICB9KVxyXG5cclxuICAgICAgICAgICAgICByZXNvbHZlKHJlcy5wYXlsb2FkKVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgfSlcclxuICAgICAgICB9LFxyXG4gICAgICAgIGRlbGV0ZUVudGl0eUFjdCh7IGNvbW1pdCwgZ2V0dGVycyB9LCBwYXlsb2FkKSB7XHJcbiAgICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoZnVuY3Rpb24gKHJlc29sdmUsIHJlamVjdCkge1xyXG4gICAgICAgICAgICBsZXQgcmVxUGFyYW1zID0geyAuLi5wYXlsb2FkIH1cclxuXHJcbiAgICAgICAgICAgIEFQSS5kZWxldGVSb2xlUmVxKHJlcVBhcmFtcykuZG9uZShyZXMgPT4ge1xyXG4gICAgICAgICAgICAgIHRyaWdnZXJNb2RhbChyZXMubWVzc2FnZSlcclxuICAgICAgICAgICAgICBpZiAoaXNFcnJvckFQSShyZXMpKSB7XHJcbiAgICAgICAgICAgICAgICByZWplY3QocmVzLnBheWxvYWQpXHJcblxyXG4gICAgICAgICAgICAgICAgcmV0dXJuXHJcbiAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICBsZXQgeyBpZCB9ID0geyAuLi5yZXMucGF5bG9hZCB9XHJcblxyXG4gICAgICAgICAgICAgIGNvbW1pdCgndXBkYXRlRW50aXR5Jywge1xyXG4gICAgICAgICAgICAgICAgdHlwZTogJ2RlbGV0ZScsXHJcbiAgICAgICAgICAgICAgICBpbmRleDogZ2V0dGVycy5nZXRJbmRleEJ5SUQoaWQpXHJcbiAgICAgICAgICAgICAgfSlcclxuXHJcbiAgICAgICAgICAgICAgcmVzb2x2ZShyZXMucGF5bG9hZClcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgIH0pXHJcbiAgICAgICAgfVxyXG4gICAgICB9LFxyXG4gICAgICBnZXR0ZXJzOiB7XHJcbiAgICAgICAgZ2V0RW50aXR5QnlJbmRleDogc3RhdGUgPT4gaW5kZXggPT4ge1xyXG4gICAgICAgICAgcmV0dXJuIHN0YXRlLmFyckVudGl0eUxpc3QuZmlsdGVyKChvYmpFbnRpdHksIGN1cnJlbnRJbmRleCkgPT4gY3VycmVudEluZGV4ID09IGluZGV4KVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgZ2V0SW5kZXhCeUlEOiBzdGF0ZSA9PiBpZCA9PiB7XHJcbiAgICAgICAgICBsZXQgaW5kZXggPSAtMVxyXG4gICAgICAgICAgX2ZvckluKHN0YXRlLmFyckVudGl0eUxpc3QsIChvYmpFbnRpdHksIGN1cnJlbnRJbmRleCkgPT4ge1xyXG4gICAgICAgICAgICBpZiAob2JqRW50aXR5LnJvbGVfaWQgIT0gaWQpXHJcbiAgICAgICAgICAgICAgcmV0dXJuXHJcblxyXG4gICAgICAgICAgICBpbmRleCA9IGN1cnJlbnRJbmRleFxyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2VcclxuICAgICAgICAgIH0pXHJcblxyXG4gICAgICAgICAgcmV0dXJuIGluZGV4XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCAobmV3IFN0b3JlKCkpO1xyXG5cclxuXHJcbiIsImltcG9ydCB7IHJlbmRlciwgc3RhdGljUmVuZGVyRm5zIH0gZnJvbSBcIi4vVGhlTW9kYWxGdWxsU2NyZWVuLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD0xZTY5Y2M2MyZcIlxuaW1wb3J0IHNjcmlwdCBmcm9tIFwiLi9UaGVNb2RhbEZ1bGxTY3JlZW4udnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiXG5leHBvcnQgKiBmcm9tIFwiLi9UaGVNb2RhbEZ1bGxTY3JlZW4udnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiXG5cblxuLyogbm9ybWFsaXplIGNvbXBvbmVudCAqL1xuaW1wb3J0IG5vcm1hbGl6ZXIgZnJvbSBcIiEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvcnVudGltZS9jb21wb25lbnROb3JtYWxpemVyLmpzXCJcbnZhciBjb21wb25lbnQgPSBub3JtYWxpemVyKFxuICBzY3JpcHQsXG4gIHJlbmRlcixcbiAgc3RhdGljUmVuZGVyRm5zLFxuICBmYWxzZSxcbiAgbnVsbCxcbiAgbnVsbCxcbiAgbnVsbFxuICBcbilcblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHtcbiAgdmFyIGFwaSA9IHJlcXVpcmUoXCJEOlxcXFxQcm9qZWN0XFxcXHNoYXJlXFxcXG91dHNvdWNlX3RodWVuaGFcXFxcaHRtbFxcXFxzdGF0aWNcXFxcYmFja2VuZFxcXFx2MVxcXFxub2RlX21vZHVsZXNcXFxcdnVlLWhvdC1yZWxvYWQtYXBpXFxcXGRpc3RcXFxcaW5kZXguanNcIilcbiAgYXBpLmluc3RhbGwocmVxdWlyZSgndnVlJykpXG4gIGlmIChhcGkuY29tcGF0aWJsZSkge1xuICAgIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgICBpZiAoIWFwaS5pc1JlY29yZGVkKCcxZTY5Y2M2MycpKSB7XG4gICAgICBhcGkuY3JlYXRlUmVjb3JkKCcxZTY5Y2M2MycsIGNvbXBvbmVudC5vcHRpb25zKVxuICAgIH0gZWxzZSB7XG4gICAgICBhcGkucmVsb2FkKCcxZTY5Y2M2MycsIGNvbXBvbmVudC5vcHRpb25zKVxuICAgIH1cbiAgICBtb2R1bGUuaG90LmFjY2VwdChcIi4vVGhlTW9kYWxGdWxsU2NyZWVuLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD0xZTY5Y2M2MyZcIiwgZnVuY3Rpb24gKCkge1xuICAgICAgYXBpLnJlcmVuZGVyKCcxZTY5Y2M2MycsIHtcbiAgICAgICAgcmVuZGVyOiByZW5kZXIsXG4gICAgICAgIHN0YXRpY1JlbmRlckZuczogc3RhdGljUmVuZGVyRm5zXG4gICAgICB9KVxuICAgIH0pXG4gIH1cbn1cbmNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwic3JjL3Z1ZS9wYXJ0aWFsL2dsb2JhbC9UaGVNb2RhbEZ1bGxTY3JlZW4udnVlXCJcbmV4cG9ydCBkZWZhdWx0IGNvbXBvbmVudC5leHBvcnRzIiwiaW1wb3J0IG1vZCBmcm9tIFwiLSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvYmFiZWwtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9pbmRleC5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4vVGhlTW9kYWxGdWxsU2NyZWVuLnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIjsgZXhwb3J0IGRlZmF1bHQgbW9kOyBleHBvcnQgKiBmcm9tIFwiLSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvYmFiZWwtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9pbmRleC5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4vVGhlTW9kYWxGdWxsU2NyZWVuLnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIiIsImV4cG9ydCAqIGZyb20gXCItIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9sb2FkZXJzL3RlbXBsYXRlTG9hZGVyLmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2luZGV4LmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi9UaGVNb2RhbEZ1bGxTY3JlZW4udnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPTFlNjljYzYzJlwiIiwiaW1wb3J0IHsgcmVuZGVyLCBzdGF0aWNSZW5kZXJGbnMgfSBmcm9tIFwiLi9BcHAudnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPTZkZjA5YWY4JlwiXG5pbXBvcnQgc2NyaXB0IGZyb20gXCIuL0FwcC52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCJcbmV4cG9ydCAqIGZyb20gXCIuL0FwcC52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCJcblxuXG4vKiBub3JtYWxpemUgY29tcG9uZW50ICovXG5pbXBvcnQgbm9ybWFsaXplciBmcm9tIFwiIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9ydW50aW1lL2NvbXBvbmVudE5vcm1hbGl6ZXIuanNcIlxudmFyIGNvbXBvbmVudCA9IG5vcm1hbGl6ZXIoXG4gIHNjcmlwdCxcbiAgcmVuZGVyLFxuICBzdGF0aWNSZW5kZXJGbnMsXG4gIGZhbHNlLFxuICBudWxsLFxuICBudWxsLFxuICBudWxsXG4gIFxuKVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkge1xuICB2YXIgYXBpID0gcmVxdWlyZShcIkQ6XFxcXFByb2plY3RcXFxcc2hhcmVcXFxcb3V0c291Y2VfdGh1ZW5oYVxcXFxodG1sXFxcXHN0YXRpY1xcXFxiYWNrZW5kXFxcXHYxXFxcXG5vZGVfbW9kdWxlc1xcXFx2dWUtaG90LXJlbG9hZC1hcGlcXFxcZGlzdFxcXFxpbmRleC5qc1wiKVxuICBhcGkuaW5zdGFsbChyZXF1aXJlKCd2dWUnKSlcbiAgaWYgKGFwaS5jb21wYXRpYmxlKSB7XG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICAgIGlmICghYXBpLmlzUmVjb3JkZWQoJzZkZjA5YWY4JykpIHtcbiAgICAgIGFwaS5jcmVhdGVSZWNvcmQoJzZkZjA5YWY4JywgY29tcG9uZW50Lm9wdGlvbnMpXG4gICAgfSBlbHNlIHtcbiAgICAgIGFwaS5yZWxvYWQoJzZkZjA5YWY4JywgY29tcG9uZW50Lm9wdGlvbnMpXG4gICAgfVxuICAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiLi9BcHAudnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPTZkZjA5YWY4JlwiLCBmdW5jdGlvbiAoKSB7XG4gICAgICBhcGkucmVyZW5kZXIoJzZkZjA5YWY4Jywge1xuICAgICAgICByZW5kZXI6IHJlbmRlcixcbiAgICAgICAgc3RhdGljUmVuZGVyRm5zOiBzdGF0aWNSZW5kZXJGbnNcbiAgICAgIH0pXG4gICAgfSlcbiAgfVxufVxuY29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJzcmMvdnVlL3BhcnRpYWwvcm9sZS9BcHAudnVlXCJcbmV4cG9ydCBkZWZhdWx0IGNvbXBvbmVudC5leHBvcnRzIiwiaW1wb3J0IG1vZCBmcm9tIFwiLSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvYmFiZWwtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9pbmRleC5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4vQXBwLnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIjsgZXhwb3J0IGRlZmF1bHQgbW9kOyBleHBvcnQgKiBmcm9tIFwiLSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvYmFiZWwtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9pbmRleC5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4vQXBwLnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIiIsImV4cG9ydCAqIGZyb20gXCItIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9sb2FkZXJzL3RlbXBsYXRlTG9hZGVyLmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2luZGV4LmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi9BcHAudnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPTZkZjA5YWY4JlwiIiwiaW1wb3J0IHsgcmVuZGVyLCBzdGF0aWNSZW5kZXJGbnMgfSBmcm9tIFwiLi9Sb2xlQWRkU2VjdGlvbi52dWU/dnVlJnR5cGU9dGVtcGxhdGUmaWQ9MzZkMWU5OTMmXCJcbmltcG9ydCBzY3JpcHQgZnJvbSBcIi4vUm9sZUFkZFNlY3Rpb24udnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiXG5leHBvcnQgKiBmcm9tIFwiLi9Sb2xlQWRkU2VjdGlvbi52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCJcblxuXG4vKiBub3JtYWxpemUgY29tcG9uZW50ICovXG5pbXBvcnQgbm9ybWFsaXplciBmcm9tIFwiIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9ydW50aW1lL2NvbXBvbmVudE5vcm1hbGl6ZXIuanNcIlxudmFyIGNvbXBvbmVudCA9IG5vcm1hbGl6ZXIoXG4gIHNjcmlwdCxcbiAgcmVuZGVyLFxuICBzdGF0aWNSZW5kZXJGbnMsXG4gIGZhbHNlLFxuICBudWxsLFxuICBudWxsLFxuICBudWxsXG4gIFxuKVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkge1xuICB2YXIgYXBpID0gcmVxdWlyZShcIkQ6XFxcXFByb2plY3RcXFxcc2hhcmVcXFxcb3V0c291Y2VfdGh1ZW5oYVxcXFxodG1sXFxcXHN0YXRpY1xcXFxiYWNrZW5kXFxcXHYxXFxcXG5vZGVfbW9kdWxlc1xcXFx2dWUtaG90LXJlbG9hZC1hcGlcXFxcZGlzdFxcXFxpbmRleC5qc1wiKVxuICBhcGkuaW5zdGFsbChyZXF1aXJlKCd2dWUnKSlcbiAgaWYgKGFwaS5jb21wYXRpYmxlKSB7XG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICAgIGlmICghYXBpLmlzUmVjb3JkZWQoJzM2ZDFlOTkzJykpIHtcbiAgICAgIGFwaS5jcmVhdGVSZWNvcmQoJzM2ZDFlOTkzJywgY29tcG9uZW50Lm9wdGlvbnMpXG4gICAgfSBlbHNlIHtcbiAgICAgIGFwaS5yZWxvYWQoJzM2ZDFlOTkzJywgY29tcG9uZW50Lm9wdGlvbnMpXG4gICAgfVxuICAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiLi9Sb2xlQWRkU2VjdGlvbi52dWU/dnVlJnR5cGU9dGVtcGxhdGUmaWQ9MzZkMWU5OTMmXCIsIGZ1bmN0aW9uICgpIHtcbiAgICAgIGFwaS5yZXJlbmRlcignMzZkMWU5OTMnLCB7XG4gICAgICAgIHJlbmRlcjogcmVuZGVyLFxuICAgICAgICBzdGF0aWNSZW5kZXJGbnM6IHN0YXRpY1JlbmRlckZuc1xuICAgICAgfSlcbiAgICB9KVxuICB9XG59XG5jb21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcInNyYy92dWUvcGFydGlhbC9yb2xlL1JvbGVBZGRTZWN0aW9uLnZ1ZVwiXG5leHBvcnQgZGVmYXVsdCBjb21wb25lbnQuZXhwb3J0cyIsImltcG9ydCBtb2QgZnJvbSBcIi0hLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2JhYmVsLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvaW5kZXguanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuL1JvbGVBZGRTZWN0aW9uLnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIjsgZXhwb3J0IGRlZmF1bHQgbW9kOyBleHBvcnQgKiBmcm9tIFwiLSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvYmFiZWwtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9pbmRleC5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4vUm9sZUFkZFNlY3Rpb24udnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiIiwiZXhwb3J0ICogZnJvbSBcIi0hLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2xvYWRlcnMvdGVtcGxhdGVMb2FkZXIuanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvaW5kZXguanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuL1JvbGVBZGRTZWN0aW9uLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD0zNmQxZTk5MyZcIiIsImltcG9ydCB7IHJlbmRlciwgc3RhdGljUmVuZGVyRm5zIH0gZnJvbSBcIi4vUm9sZUVkaXRTZWN0aW9uLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD1mMGJhMzkwOCZcIlxuaW1wb3J0IHNjcmlwdCBmcm9tIFwiLi9Sb2xlRWRpdFNlY3Rpb24udnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiXG5leHBvcnQgKiBmcm9tIFwiLi9Sb2xlRWRpdFNlY3Rpb24udnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiXG5cblxuLyogbm9ybWFsaXplIGNvbXBvbmVudCAqL1xuaW1wb3J0IG5vcm1hbGl6ZXIgZnJvbSBcIiEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvcnVudGltZS9jb21wb25lbnROb3JtYWxpemVyLmpzXCJcbnZhciBjb21wb25lbnQgPSBub3JtYWxpemVyKFxuICBzY3JpcHQsXG4gIHJlbmRlcixcbiAgc3RhdGljUmVuZGVyRm5zLFxuICBmYWxzZSxcbiAgbnVsbCxcbiAgbnVsbCxcbiAgbnVsbFxuICBcbilcblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHtcbiAgdmFyIGFwaSA9IHJlcXVpcmUoXCJEOlxcXFxQcm9qZWN0XFxcXHNoYXJlXFxcXG91dHNvdWNlX3RodWVuaGFcXFxcaHRtbFxcXFxzdGF0aWNcXFxcYmFja2VuZFxcXFx2MVxcXFxub2RlX21vZHVsZXNcXFxcdnVlLWhvdC1yZWxvYWQtYXBpXFxcXGRpc3RcXFxcaW5kZXguanNcIilcbiAgYXBpLmluc3RhbGwocmVxdWlyZSgndnVlJykpXG4gIGlmIChhcGkuY29tcGF0aWJsZSkge1xuICAgIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgICBpZiAoIWFwaS5pc1JlY29yZGVkKCdmMGJhMzkwOCcpKSB7XG4gICAgICBhcGkuY3JlYXRlUmVjb3JkKCdmMGJhMzkwOCcsIGNvbXBvbmVudC5vcHRpb25zKVxuICAgIH0gZWxzZSB7XG4gICAgICBhcGkucmVsb2FkKCdmMGJhMzkwOCcsIGNvbXBvbmVudC5vcHRpb25zKVxuICAgIH1cbiAgICBtb2R1bGUuaG90LmFjY2VwdChcIi4vUm9sZUVkaXRTZWN0aW9uLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD1mMGJhMzkwOCZcIiwgZnVuY3Rpb24gKCkge1xuICAgICAgYXBpLnJlcmVuZGVyKCdmMGJhMzkwOCcsIHtcbiAgICAgICAgcmVuZGVyOiByZW5kZXIsXG4gICAgICAgIHN0YXRpY1JlbmRlckZuczogc3RhdGljUmVuZGVyRm5zXG4gICAgICB9KVxuICAgIH0pXG4gIH1cbn1cbmNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwic3JjL3Z1ZS9wYXJ0aWFsL3JvbGUvUm9sZUVkaXRTZWN0aW9uLnZ1ZVwiXG5leHBvcnQgZGVmYXVsdCBjb21wb25lbnQuZXhwb3J0cyIsImltcG9ydCBtb2QgZnJvbSBcIi0hLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2JhYmVsLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvaW5kZXguanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuL1JvbGVFZGl0U2VjdGlvbi52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCI7IGV4cG9ydCBkZWZhdWx0IG1vZDsgZXhwb3J0ICogZnJvbSBcIi0hLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2JhYmVsLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvaW5kZXguanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuL1JvbGVFZGl0U2VjdGlvbi52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCIiLCJleHBvcnQgKiBmcm9tIFwiLSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvbG9hZGVycy90ZW1wbGF0ZUxvYWRlci5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9pbmRleC5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4vUm9sZUVkaXRTZWN0aW9uLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD1mMGJhMzkwOCZcIiIsImltcG9ydCB7IHJlbmRlciwgc3RhdGljUmVuZGVyRm5zIH0gZnJvbSBcIi4vUm9sZUluZGV4U2VjdGlvbi52dWU/dnVlJnR5cGU9dGVtcGxhdGUmaWQ9MDU5MjNmMDImXCJcbmltcG9ydCBzY3JpcHQgZnJvbSBcIi4vUm9sZUluZGV4U2VjdGlvbi52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCJcbmV4cG9ydCAqIGZyb20gXCIuL1JvbGVJbmRleFNlY3Rpb24udnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiXG5cblxuLyogbm9ybWFsaXplIGNvbXBvbmVudCAqL1xuaW1wb3J0IG5vcm1hbGl6ZXIgZnJvbSBcIiEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvcnVudGltZS9jb21wb25lbnROb3JtYWxpemVyLmpzXCJcbnZhciBjb21wb25lbnQgPSBub3JtYWxpemVyKFxuICBzY3JpcHQsXG4gIHJlbmRlcixcbiAgc3RhdGljUmVuZGVyRm5zLFxuICBmYWxzZSxcbiAgbnVsbCxcbiAgbnVsbCxcbiAgbnVsbFxuICBcbilcblxuLyogaG90IHJlbG9hZCAqL1xuaWYgKG1vZHVsZS5ob3QpIHtcbiAgdmFyIGFwaSA9IHJlcXVpcmUoXCJEOlxcXFxQcm9qZWN0XFxcXHNoYXJlXFxcXG91dHNvdWNlX3RodWVuaGFcXFxcaHRtbFxcXFxzdGF0aWNcXFxcYmFja2VuZFxcXFx2MVxcXFxub2RlX21vZHVsZXNcXFxcdnVlLWhvdC1yZWxvYWQtYXBpXFxcXGRpc3RcXFxcaW5kZXguanNcIilcbiAgYXBpLmluc3RhbGwocmVxdWlyZSgndnVlJykpXG4gIGlmIChhcGkuY29tcGF0aWJsZSkge1xuICAgIG1vZHVsZS5ob3QuYWNjZXB0KClcbiAgICBpZiAoIWFwaS5pc1JlY29yZGVkKCcwNTkyM2YwMicpKSB7XG4gICAgICBhcGkuY3JlYXRlUmVjb3JkKCcwNTkyM2YwMicsIGNvbXBvbmVudC5vcHRpb25zKVxuICAgIH0gZWxzZSB7XG4gICAgICBhcGkucmVsb2FkKCcwNTkyM2YwMicsIGNvbXBvbmVudC5vcHRpb25zKVxuICAgIH1cbiAgICBtb2R1bGUuaG90LmFjY2VwdChcIi4vUm9sZUluZGV4U2VjdGlvbi52dWU/dnVlJnR5cGU9dGVtcGxhdGUmaWQ9MDU5MjNmMDImXCIsIGZ1bmN0aW9uICgpIHtcbiAgICAgIGFwaS5yZXJlbmRlcignMDU5MjNmMDInLCB7XG4gICAgICAgIHJlbmRlcjogcmVuZGVyLFxuICAgICAgICBzdGF0aWNSZW5kZXJGbnM6IHN0YXRpY1JlbmRlckZuc1xuICAgICAgfSlcbiAgICB9KVxuICB9XG59XG5jb21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcInNyYy92dWUvcGFydGlhbC9yb2xlL1JvbGVJbmRleFNlY3Rpb24udnVlXCJcbmV4cG9ydCBkZWZhdWx0IGNvbXBvbmVudC5leHBvcnRzIiwiaW1wb3J0IG1vZCBmcm9tIFwiLSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvYmFiZWwtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9pbmRleC5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4vUm9sZUluZGV4U2VjdGlvbi52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCI7IGV4cG9ydCBkZWZhdWx0IG1vZDsgZXhwb3J0ICogZnJvbSBcIi0hLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL2JhYmVsLWxvYWRlci9saWIvaW5kZXguanM/P3JlZi0tMSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvaW5kZXguanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuL1JvbGVJbmRleFNlY3Rpb24udnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiIiwiZXhwb3J0ICogZnJvbSBcIi0hLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2xvYWRlcnMvdGVtcGxhdGVMb2FkZXIuanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvaW5kZXguanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuL1JvbGVJbmRleFNlY3Rpb24udnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPTA1OTIzZjAyJlwiIiwiaW1wb3J0IHsgcmVuZGVyLCBzdGF0aWNSZW5kZXJGbnMgfSBmcm9tIFwiLi9Sb2xlVGFibGUudnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPTQwNjUzNzBmJlwiXG5pbXBvcnQgc2NyaXB0IGZyb20gXCIuL1JvbGVUYWJsZS52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCJcbmV4cG9ydCAqIGZyb20gXCIuL1JvbGVUYWJsZS52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCJcblxuXG4vKiBub3JtYWxpemUgY29tcG9uZW50ICovXG5pbXBvcnQgbm9ybWFsaXplciBmcm9tIFwiIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9ydW50aW1lL2NvbXBvbmVudE5vcm1hbGl6ZXIuanNcIlxudmFyIGNvbXBvbmVudCA9IG5vcm1hbGl6ZXIoXG4gIHNjcmlwdCxcbiAgcmVuZGVyLFxuICBzdGF0aWNSZW5kZXJGbnMsXG4gIGZhbHNlLFxuICBudWxsLFxuICBudWxsLFxuICBudWxsXG4gIFxuKVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkge1xuICB2YXIgYXBpID0gcmVxdWlyZShcIkQ6XFxcXFByb2plY3RcXFxcc2hhcmVcXFxcb3V0c291Y2VfdGh1ZW5oYVxcXFxodG1sXFxcXHN0YXRpY1xcXFxiYWNrZW5kXFxcXHYxXFxcXG5vZGVfbW9kdWxlc1xcXFx2dWUtaG90LXJlbG9hZC1hcGlcXFxcZGlzdFxcXFxpbmRleC5qc1wiKVxuICBhcGkuaW5zdGFsbChyZXF1aXJlKCd2dWUnKSlcbiAgaWYgKGFwaS5jb21wYXRpYmxlKSB7XG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICAgIGlmICghYXBpLmlzUmVjb3JkZWQoJzQwNjUzNzBmJykpIHtcbiAgICAgIGFwaS5jcmVhdGVSZWNvcmQoJzQwNjUzNzBmJywgY29tcG9uZW50Lm9wdGlvbnMpXG4gICAgfSBlbHNlIHtcbiAgICAgIGFwaS5yZWxvYWQoJzQwNjUzNzBmJywgY29tcG9uZW50Lm9wdGlvbnMpXG4gICAgfVxuICAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiLi9Sb2xlVGFibGUudnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPTQwNjUzNzBmJlwiLCBmdW5jdGlvbiAoKSB7XG4gICAgICBhcGkucmVyZW5kZXIoJzQwNjUzNzBmJywge1xuICAgICAgICByZW5kZXI6IHJlbmRlcixcbiAgICAgICAgc3RhdGljUmVuZGVyRm5zOiBzdGF0aWNSZW5kZXJGbnNcbiAgICAgIH0pXG4gICAgfSlcbiAgfVxufVxuY29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJzcmMvdnVlL3BhcnRpYWwvcm9sZS9Sb2xlVGFibGUudnVlXCJcbmV4cG9ydCBkZWZhdWx0IGNvbXBvbmVudC5leHBvcnRzIiwiaW1wb3J0IG1vZCBmcm9tIFwiLSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvYmFiZWwtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9pbmRleC5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4vUm9sZVRhYmxlLnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIjsgZXhwb3J0IGRlZmF1bHQgbW9kOyBleHBvcnQgKiBmcm9tIFwiLSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvYmFiZWwtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9pbmRleC5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4vUm9sZVRhYmxlLnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIiIsImV4cG9ydCAqIGZyb20gXCItIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9sb2FkZXJzL3RlbXBsYXRlTG9hZGVyLmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2luZGV4LmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi9Sb2xlVGFibGUudnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPTQwNjUzNzBmJlwiIiwiaW1wb3J0IHsgcmVuZGVyLCBzdGF0aWNSZW5kZXJGbnMgfSBmcm9tIFwiLi9Sb2xlVGFibGVSb3cudnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPTI2ZjU5YjdiJlwiXG5pbXBvcnQgc2NyaXB0IGZyb20gXCIuL1JvbGVUYWJsZVJvdy52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCJcbmV4cG9ydCAqIGZyb20gXCIuL1JvbGVUYWJsZVJvdy52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCJcblxuXG4vKiBub3JtYWxpemUgY29tcG9uZW50ICovXG5pbXBvcnQgbm9ybWFsaXplciBmcm9tIFwiIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9ydW50aW1lL2NvbXBvbmVudE5vcm1hbGl6ZXIuanNcIlxudmFyIGNvbXBvbmVudCA9IG5vcm1hbGl6ZXIoXG4gIHNjcmlwdCxcbiAgcmVuZGVyLFxuICBzdGF0aWNSZW5kZXJGbnMsXG4gIGZhbHNlLFxuICBudWxsLFxuICBudWxsLFxuICBudWxsXG4gIFxuKVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkge1xuICB2YXIgYXBpID0gcmVxdWlyZShcIkQ6XFxcXFByb2plY3RcXFxcc2hhcmVcXFxcb3V0c291Y2VfdGh1ZW5oYVxcXFxodG1sXFxcXHN0YXRpY1xcXFxiYWNrZW5kXFxcXHYxXFxcXG5vZGVfbW9kdWxlc1xcXFx2dWUtaG90LXJlbG9hZC1hcGlcXFxcZGlzdFxcXFxpbmRleC5qc1wiKVxuICBhcGkuaW5zdGFsbChyZXF1aXJlKCd2dWUnKSlcbiAgaWYgKGFwaS5jb21wYXRpYmxlKSB7XG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICAgIGlmICghYXBpLmlzUmVjb3JkZWQoJzI2ZjU5YjdiJykpIHtcbiAgICAgIGFwaS5jcmVhdGVSZWNvcmQoJzI2ZjU5YjdiJywgY29tcG9uZW50Lm9wdGlvbnMpXG4gICAgfSBlbHNlIHtcbiAgICAgIGFwaS5yZWxvYWQoJzI2ZjU5YjdiJywgY29tcG9uZW50Lm9wdGlvbnMpXG4gICAgfVxuICAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiLi9Sb2xlVGFibGVSb3cudnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPTI2ZjU5YjdiJlwiLCBmdW5jdGlvbiAoKSB7XG4gICAgICBhcGkucmVyZW5kZXIoJzI2ZjU5YjdiJywge1xuICAgICAgICByZW5kZXI6IHJlbmRlcixcbiAgICAgICAgc3RhdGljUmVuZGVyRm5zOiBzdGF0aWNSZW5kZXJGbnNcbiAgICAgIH0pXG4gICAgfSlcbiAgfVxufVxuY29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJzcmMvdnVlL3BhcnRpYWwvcm9sZS9Sb2xlVGFibGVSb3cudnVlXCJcbmV4cG9ydCBkZWZhdWx0IGNvbXBvbmVudC5leHBvcnRzIiwiaW1wb3J0IG1vZCBmcm9tIFwiLSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvYmFiZWwtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9pbmRleC5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4vUm9sZVRhYmxlUm93LnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIjsgZXhwb3J0IGRlZmF1bHQgbW9kOyBleHBvcnQgKiBmcm9tIFwiLSEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvYmFiZWwtbG9hZGVyL2xpYi9pbmRleC5qcz8/cmVmLS0xIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9pbmRleC5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4vUm9sZVRhYmxlUm93LnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIiIsImV4cG9ydCAqIGZyb20gXCItIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9sb2FkZXJzL3RlbXBsYXRlTG9hZGVyLmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2luZGV4LmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi9Sb2xlVGFibGVSb3cudnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPTI2ZjU5YjdiJlwiIl0sInNvdXJjZVJvb3QiOiIifQ==
