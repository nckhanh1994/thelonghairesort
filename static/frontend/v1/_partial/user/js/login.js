(function () {
  var EVENT, REQUEST, CALLBACK, PLUGIN, UTIL;

  var Object = {
    init() {
      EVENT = this.event;
      REQUEST = this.request;
      CALLBACK = this.callback;
      PLUGIN = this.plugin;
      UTIL = this.util;

      this.event.init()
      this.request.init()
      this.callback.init()
      this.plugin.init()
    },
    event: {
      init() {
        this.login();
      },
      login() {
        var $elFrm = $('.js-login-frm');
        var $elBtn = $('.js-login-frm__sm-btn');

        $elBtn.off('click').on('click', function (e) {
          e.preventDefault();

          UTIL.validate($elFrm);
        })
      }
    },
    request: {
      init() {
      }
    },
    callback: {
      init() {
      }
    },
    plugin: {
      init() {
      },
    },
    util: {
      validate($el, callback) {
        if (typeof validationRules == 'undefined') {
          return false;
        }

        validationRules.errorElement = 'div';
        validationRules.errorClass = 'item_message';
        validationRules.errorPlacement = function (error, element) {
          element.closest('div').after(error);
        };
        validationRules.submitHandler = function (form) {
          $.ajax({
            type: 'POST',
            url: CDATA.url.loginURL,
            data: {
              form_data: $el.serialize()
            },
            dataType: 'json',
            beforeSend: function () {

            },
            error: function () {

            }
          }).done(function (res) {
            if (res.success !== 1) {
              window.SWAL.alert('error', res.message);
              return false;
            }

            window.SWAL.alert('success', res.message);

            setTimeout(function () {
              window.location.href = CDATA.url.refererURL;
            }, 800)
          });
        };
        validationRules.invalidHandler = function (event, validator) {
        };

        $el.validate(validationRules);
        $el.submit();
      }
    }
  };

  Object.init()
})();
