(function () {
  var EVENT, REQUEST, CALLBACK, PLUGIN, UTIL;

  var Object = {
    init() {
      EVENT = this.event;
      REQUEST = this.request;
      CALLBACK = this.callback;
      PLUGIN = this.plugin;
      UTIL = this.util;

      EVENT.init();
      REQUEST.init();
      PLUGIN.init();
      UTIL.init();
    },
    event: {
      init() {

      },
    },
    request: {
      init() {
      }
    },
    callback: {
      init() {
      }
    },
    plugin: {
      init() {
      },
    },
    util: {
      init() {

      },
    }
  };

  Object.init()
})();
