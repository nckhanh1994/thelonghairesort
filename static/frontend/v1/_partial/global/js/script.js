(function () {
  var EVENT, REQUEST, CALLBACK, PLUGIN, UTIL;

  var Object = {
    init() {
      EVENT = this.event;
      REQUEST = this.request;
      CALLBACK = this.callback;
      PLUGIN = this.plugin;
      UTIL = this.util;

      EVENT.init();
      REQUEST.init();
      CALLBACK.init();
      PLUGIN.init();
      UTIL.init();
    },
    event: {
      init() {
        this.filter.init();
      },
      filter: {
        init() {
          this.category();
          this.city();
          this.priceRange();
          this.floorArea();
        },
        category() {
          var $el = $('.js-filter__category-sel');
          $el.on('click', function (e) {
            var $self = $(this);

            var id = $self.data('id');
            if (!id)
              return false;

            $('.js-filter__category-id').val(id);
          })
        },
        city() {
          var $el = $('.js-filter__city-sel');
          $el.on('click', function (e) {
            var $self = $(this);

            var id = $self.data('id');
            if (!id)
              return false;

            $('.js-filter__city-id').val(id);
          })
        },
        priceRange() {
          var $el = $('.js-filter__price-range-sel');
          $el.on('click', function (e) {
            var $self = $(this);

            var id = $self.data('value');
            if (!id)
              return false;

            $('.js-filter__price-range').val(id);
          })
        },
        floorArea() {
          var $el = $('.js-filter__floor-area-sel');
          $el.on('click', function (e) {
            var $self = $(this);

            var id = $self.data('value');
            if (!id)
              return false;

            $('.js-filter__floor-area_ranger').val(id);
          })
        }
      }
    },
    request: {
      init() {
      }
    },
    callback: {
      init() {
      }
    },
    plugin: {
      init() {
      },
    },
    util: {
      init() {

      }
    }
  };

  Object.init()
})();
