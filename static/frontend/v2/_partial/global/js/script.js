(function () {
  var EVENT, REQUEST, CALLBACK, PLUGIN, UTIL;

  var Object = {
    init() {
      EVENT = this.event;
      REQUEST = this.request;
      CALLBACK = this.callback;
      PLUGIN = this.plugin;
      UTIL = this.util;

      EVENT.init();
      REQUEST.init();
      CALLBACK.init();
      PLUGIN.init();
      UTIL.init();
    },
    event: {
      init() {
        this.filter.init();
        this.fixedHeader();

        $('.header__navbar-toggle').on('click', function (e) {
          $('.navbar__wrap--init').toggle();
          $('.js-site-wrap').toggleClass('site-wrap--move');
          $('.header__navbar-show').toggle();
          $('.header__navbar-hide').toggle();
        })
      },
      filter: {
        init() {
          this.category();
          this.price();
          this.floorArea();
        },
        category() {
          var $el = $('.js-search-box__category-sel');
          $el.on('click', function (e) {
            var $self = $(this);

            var text = $.trim($self.text());
            var val = $self.data('val');
            if (!val)
              return false;

            $('.js-search-box__category-desc').text(text);
            $('.js-search-box__category-id').val(val);

            $self.closest('.search-pop').hide();
          })
        },
        price() {
          var $el = $('.js-search-box__price-sel');
          $el.on('click', function (e) {
            var $self = $(this);

            var text = $.trim($self.text());
            var val = $self.data('val');
            if (!val)
              return false;

            text = text.replace('>', 'Hơn');

            $('.js-search-box__price-desc').text(text);
            $('.js-search-box__price').val(val);

            $self.closest('.search-pop').hide();
          })

          var priceRange = [ 0, 0 ];
          var $el = $('.js-search-box__price-input-from');
          $el.on('keyup', function (e) {
            var $self = $(this);

            priceRange[ 0 ] = $self.val();
            priceRange[ 1 ] = priceRange[ 1 ] < priceRange[ 0 ] ? priceRange[ 0 ] : priceRange[ 1 ];

            var text = 'Từ ' + priceRange[ 0 ] + ' đến ' + priceRange[ 1 ] + ' triệu';

            $('.js-search-box__price-desc').text(text);
            $('.js-search-box__price').val(priceRange[ 0 ] + '-' + priceRange[ 1 ]);
          });

          var $el = $('.js-search-box__price-input-to');
          $el.on('keyup', function (e) {
            var $self = $(this);

            priceRange[ 1 ] = $self.val();
            priceRange[ 1 ] = priceRange[ 1 ] < priceRange[ 0 ] ? priceRange[ 0 ] : priceRange[ 1 ];

            var text = 'Từ ' + priceRange[ 0 ] + ' đến ' + priceRange[ 1 ] + ' triệu';

            $('.js-search-box__price-desc').text(text);
            $('.js-search-box__price').val(priceRange[ 0 ] + '-' + priceRange[ 1 ]);
          });
        },
        floorArea() {
          var $el = $('.js-search-box__floor-area-sel');
          $el.on('click', function (e) {
            var $self = $(this);

            var text = $.trim($self.text());
            var val = $self.data('val');
            if (!val)
              return false;

            text = text.replace('>', 'Hơn');

            $('.js-search-box__floor-area-desc').text(text);
            $('.js-search-box__floor-area').val(val);

            $self.closest('.search-pop').hide();
          })

          var floorAreaRange = [ 0, 0 ];
          var $el = $('.js-search-box__floor-area-input-from');
          $el.on('keyup', function (e) {
            var $self = $(this);

            floorAreaRange[ 0 ] = parseFloat($self.val());
            floorAreaRange[ 1 ] = floorAreaRange[ 1 ] < floorAreaRange[ 0 ] ? floorAreaRange[ 0 ] : floorAreaRange[ 1 ];

            var text = 'Từ ' + floorAreaRange[ 0 ] + ' đến ' + floorAreaRange[ 1 ] + ' triệu';

            $('.js-search-box__floor-area-desc').text(text);
            $('.js-search-box__floor-area').val(floorAreaRange[ 0 ] + '-' + floorAreaRange[ 1 ]);
          });

          var $el = $('.js-search-box__floor-area-input-to');
          $el.on('keyup', function (e) {
            var $self = $(this);

            floorAreaRange[ 1 ] = parseFloat($self.val());

            floorAreaRange[ 1 ] = floorAreaRange[ 1 ] < floorAreaRange[ 0 ] ? floorAreaRange[ 0 ] : floorAreaRange[ 1 ];

            var text = 'Từ ' + floorAreaRange[ 0 ] + ' đến ' + floorAreaRange[ 1 ] + ' m2';

            $('.js-search-box__floor-area-desc').text(text);
            $('.js-search-box__floor-area').val(floorAreaRange[ 0 ] + '-' + floorAreaRange[ 1 ]);
          });
        }
      },
      fixedHeader() {
        window.onscroll = function () {
          myFunction()
        };

        var sticky = 60;

        function myFunction() {
          if (window.pageYOffset > sticky) {
            $('#header-nav').addClass("sticky");
          } else {
            $('#header-nav').removeClass("sticky");
          }
        }
      }
    },
    request: {
      init() {
        this.contact();
      },
      contact() {
        var $elFrm = $('.js-contact-frm');
        var $elBtn = $('.js-contact-frm__submit');

        $elBtn.off('click').on('click', function (e) {
          e.preventDefault();

          UTIL.validate($elFrm);
        })
      }
    },
    callback: {
      init() {
      }
    },
    plugin: {
      init() {
        this.swiper();
        this.typeahead();
      },
      swiper() {
        var slidesPerView = 3;
        if (device == 'mobile') {
          slidesPerView = 1
        }
        var swiper = new Swiper('#recents-post-block .swiper-container', {
          autoHeight: true, //enable auto height
          slidesPerView: slidesPerView,
          spaceBetween: 30,
          navigation: {
            nextEl: '#recents-post-block .swiper-button-next',
            prevEl: '#recents-post-block .swiper-button-prev',
          },
        });
      },
      typeahead() {
        $('.js-search-box__street').typeahead({
          items: 8,
          source: CDATA.common.streets,
        });
      }
    },
    util: {
      init() {

      },
      validate($el) {
        if (typeof validationRules == 'undefined') {
          return false;
        }

        validationRules.errorElement = 'span';
        validationRules.errorClass = 'errors';
        validationRules.errorPlacement = function (error, element) {
          element.closest('div').append(error);
        };
        validationRules.submitHandler = function (form) {
          $.ajax({
            type: 'POST',
            url: 'http://api.dev.thuenha.com/user/contact',
            data: {
              type: CDATA.common.action,
              form_data: $el.serialize()
            },
            dataType: 'json',
            beforeSend: function () {

            },
            error: function () {

            }
          }).done(function (res) {
            if (res.success !== 1) {
              window.SWAL.alert('error', res.message);
              return false;
            }

            window.SWAL.alert('success', res.message);

            var payload = res.payload;
            setTimeout(function () {
              window.location.href = payload.redirect_url;
            }, 800)
          });
        };
        validationRules.invalidHandler = function (event, validator) {
        };

        console.log($el);
        $el.validate(validationRules);
        $el.submit();
      }
    }
  };

  $(document).ready(function () {
    Object.init()
  })
})();
