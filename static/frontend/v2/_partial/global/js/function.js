function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

function regexOnlyNumbers(v) {
  return v.replace(/[^\d]/g, ''); // numbers and decimals only
}

const openSocialLogin = (url) => {
  const left = screen.width / 2 - 600 / 2;
  return window.open(url,
      'Đăng nhập',
      'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=600, height=800, top=0, left=' +
      left);
};

window.SWAL = {
  alert(type, msg) {
    return Swal.fire(
        'Thông báo',
        msg,
        type
    )
  }
}
