// #rent
(function () {
  var EVENT, REQUEST, CALLBACK, PLUGIN, UTIL;

  var Object = {
    init() {
      EVENT = this.event;
      REQUEST = this.request;
      CALLBACK = this.callback;
      PLUGIN = this.plugin;
      UTIL = this.util;

      EVENT.init()
      REQUEST.init()
      PLUGIN.init()
      UTIL.init()
    },
    event: {
      init() {
        this.rent();
        this.regexOnlyNumbers();
        this.addressSelect();
      },
      rent() {
        var $elFrm = $('.js-rent-frm');
        var $elBtn = $('.js-rent-frm__sm-btn');

        $elBtn.off('click').on('click', function (e) {
          e.preventDefault();

          UTIL.validate($elFrm);
        })
      },
      regexOnlyNumbers() {
        /* ## price */
        var $el = $('.js-reg-frm__price');
        $el.on('input', function (e) {
          var $self = $(this);

          this.value = regexOnlyNumbers(this.value);
          var valueDisplay = new Intl.NumberFormat('vi-VI', { style: 'currency', currency: 'VND' }).format(this.value);
          var objClasses = $self.attr('class').split(' ');
          if (objClasses.includes('pct_price_from')) {
            $('.js-reg-frm__price-from-display').html(valueDisplay);
          } else if (objClasses.includes('pct_price_to')) {
            $('.js-reg-frm__price-to-display').html(valueDisplay);
          }
        });
        /* ## */

        /* ## floor_area */
        var $el = $('.js-reg-frm__floor-area');
        $el.on('input', function (e) {
          var $self = $(this);

          this.value = regexOnlyNumbers(this.value);
        });
        /* ## */
      },
      addressSelect() {
        var $elCitySelect = $('.js-rent-frm__city-select');
        var $elDistrictSelect = $('.js-rent-frm__district-select');

        /* ## City */
        $elCitySelect.on('change', function (e) {
          var $self = $(this);

          var cityName = $self.find('option:selected').data('name');

          $('.js-rent-frm__city-name').val(cityName);
        })
        /* ## */

        /* ## District */
        $elDistrictSelect.on('change', function (e) {
          var $self = $(this);

          var districtName = $self.find('option:selected').data('name');

          $('.js-rent-frm__district-name').val(districtName);
        })
        /* ## */
      }
    },
    request: {
      init() {

      },
    },
    callback: {
      init() {
      }
    },
    plugin: {
      init() {
        this.nileUpload();
      },
      nileUpload() {
        $("#photoimg").Nileupload({
          action: 'http://api.dev.thuenha.com/global/upload',
          size: '3MB',
          extension: 'jpg,jpeg,png',
          progress: $("#progress"),
          preview: $(".mainPhotoImageList"),
          multi: true
        });
      }
    },
    util: {
      init() {

      },
      validate($el, callback) {
        if (typeof validationRules == 'undefined') {
          return false;
        }

        validationRules.errorElement = 'div';
        validationRules.errorClass = 'item_message';
        validationRules.errorPlacement = function (error, element) {
          element.closest('div').after(error);
        };
        validationRules.submitHandler = function (form) {
          $.ajax({
            type: 'POST',
            url: 'http://api.dev.thuenha.com/user/rent',
            data: {
              type: CDATA.common.action,
              form_data: $el.serialize()
            },
            dataType: 'json',
            beforeSend: function () {

            },
            error: function () {

            }
          }).done(function (res) {
            if (res.success !== 1) {
              window.SWAL.alert('error', res.message);
              return false;
            }

            window.SWAL.alert('success', res.message);

            var payload = res.payload;
            setTimeout(function () {
              window.location.href = CDATA.url.baseURL + '/' + payload.slug + '-' + payload.id + '.html';
            }, 800)
          });
        };
        validationRules.invalidHandler = function (event, validator) {
        };

        $el.validate(validationRules);
        $el.submit();
      }
    }
  };

  Object.init()
})();
