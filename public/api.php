<?php
  require_once './define.php';
  
  use
    Phalcon\Mvc\Micro,
    Phalcon\Mvc\Router,
    Phalcon\Mvc\Micro\Collection as MicroCollection,
    Phalcon\Loader,
    Phalcon\Di\FactoryDefault,
    Phalcon\Config\Adapter\Ini as IniConfig,
    Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter,
    Phalcon\Http\Response\Cookies,
    Phalcon\Session\Adapter\Files as SessionAdapter;
  
  use
    Module\Api\Controller\GlobalController,
    Module\Api\Controller\UserController,
    Module\Api\Controller\RoleController,
    Module\Api\Controller\ItemController,
    Module\Api\Controller\CityController,
    Module\Api\Controller\DistrictController,
    Module\Api\Controller\WardController,
    Module\Api\Controller\StreetController;
  
  try {
    /**
     * ! Include Autoloader
     */
    require BASE_PATH . '/config/loader.php';
    
    $app = new Micro();
    
    $di = new FactoryDefault();
    
    $di['cookies'] = function () {
      $cookies = new Cookies();
      $cookies->useEncryption(false);
      
      return $cookies;
    };
    
    /**
     * Start the session the first time some component request the session service
     */
    $di['session'] = function () {
      session_save_path('/data/www/thuenha/session');
      //ini_set('session.gc_maxlifetime', 15 * 24 * 60 * 60);
      //ini_set('session.gc_probability', 1);
      //ini_set('session.gc_divisor', 1);
      $session = new SessionAdapter([
        "uniqueId" => SECRET_KEY,
      ]);
      $session->start();
      
      return $session;
    };
    
    $di->setShared('config', function () {
      return require_once BASE_PATH . "/config/config.php";
    });
    
    $di->setShared('translate', function () use ($di) {
      $translate = require_once BASE_PATH . "/config/translate.php";
  
      $interpolator = new InterpolatorFactory();
      $factory      = new TranslateFactory($interpolator);
  
      $translator = $factory->newInstance('array', [
        "content" => $translate,
      ]);
  
      return $translator;
    });
    
    $di->setShared('translateUtil', function () use ($di) {
      return new \Util\TranslateUtil();
    });
    
    $di->set('db', function () {
      $config = $this->getConfig();
      
      $params     = [
        'host'     => $config->database->host,
        'username' => $config->database->username,
        'password' => $config->database->password,
        'dbname'   => $config->database->dbname,
        'charset'  => $config->database->charset,
      ];
      $connection = new DbAdapter($params);
      
      return $connection;
    });
    
    $di['cache'] = function () {
      $instanceCache = new \Memcached();
      $instanceCache->addServer('127.0.0.1', 11211);
      $instanceCache->setOption(\Memcached::OPT_HASH, \Memcached::HASH_MD5);
      $instanceCache->setOption(\Memcached::OPT_PREFIX_KEY, PREFIX);
      
      return $instanceCache;
    };
    
    $app->setDI($di);
    
    // Set the main handler. ie. a controller instance
    $global = new MicroCollection();
    $global->setHandler(GlobalController::class, true);
    $global->setPrefix('/global');
    $global->map('/upload', 'upload');
    
    // Set the main handler. ie. a controller instance
    $user = new MicroCollection();
    $user->setHandler(UserController::class, true);
    $user->setPrefix('/user');
    $user->post('/read', 'read');
    $user->post('/get', 'get');
    $user->post('/create', 'create');
    $user->post('/update', 'update');
    $user->post('/delete', 'delete');
    $user->post('/reg', 'reg');
    $user->post('/login', 'login');
    $user->post('/rent', 'rent');
    $user->post('/contact', 'contact');
    
    // Set the main handler. ie. a controller instance
    $role = new MicroCollection();
    $role->setHandler(RoleController::class, true);
    $role->setPrefix('/role');
    $role->post('/read', 'read');
    $role->post('/get', 'get');
    $role->post('/create', 'create');
    $role->post('/update', 'update');
    $role->post('/delete', 'delete');
    $role->post('/getRolePermission', 'getRolePermission');
    $role->post('/createRolePermission', 'createRolePermission');
    $role->post('/deleteRolePermission', 'deleteRolePermission');
    
    // Set the main handler. ie. a controller instance
    $item = new MicroCollection();
    $item->setHandler(ItemController::class, true);
    $item->setPrefix('/item');
    $item->post('/read', 'read');
    $item->post('/get', 'get');
    $item->post('/create', 'create');
    $item->post('/update', 'update');
    $item->post('/delete', 'delete');
    
    $city = new MicroCollection();
    $city->setHandler(CityController::class, true);
    $city->setPrefix('/city');
    $city->post('/read', 'read');
    $city->post('/get', 'get');
    $city->post('/create', 'create');
    $city->post('/update', 'update');
    $city->post('/delete', 'delete');
    
    $district = new MicroCollection();
    $district->setHandler(DistrictController::class, true);
    $district->setPrefix('/district');
    $district->post('/read', 'read');
    $district->post('/get', 'get');
    $district->post('/create', 'create');
    $district->post('/update', 'update');
    $district->post('/delete', 'delete');
    
    $ward = new MicroCollection();
    $ward->setHandler(WardController::class, true);
    $ward->setPrefix('/ward');
    $ward->post('/read', 'read');
    $ward->post('/get', 'get');
    $ward->post('/create', 'create');
    $ward->post('/update', 'update');
    $ward->post('/delete', 'delete');
    
    $street = new MicroCollection();
    $street->setHandler(StreetController::class, true);
    $street->setPrefix('/street');
    $street->post('/read', 'read');
    
    $app->getRouter()->handle($_SERVER["REQUEST_URI"]);
    $app->getRouter()->removeExtraSlashes(true);
    
    $app->mount($global);
    $app->mount($user);
    $app->mount($role);
    $app->mount($item);
    $app->mount($city);
    $app->mount($district);
    $app->mount($ward);
    $app->mount($street);
    
    if (APPLICATION_ENV == "dev") {
//      $di['app'] = $app; // debugbar
//      (new Snowair\Debugbar\ServiceProvider(BASE_PATH . '/config/debugbar.php'))->start();
//      new Whoops\Provider\Phalcon\WhoopsServiceProvider();
    }
    
    echo $app->handle($_SERVER["REQUEST_URI"]);
  } catch (Phalcon\Exception $e) {
    echo $e->getMessage();
  } catch (PDOException $e) {
    echo $e->getMessage();
  }
