<?php
  
  use Phalcon\Cli\Console as ConsoleApp;
  use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
  use Phalcon\Di\FactoryDefault\Cli as CliDI;
  use Phalcon\Loader;
  use PhpAmqpLib\Connection\AMQPStreamConnection;
  
  include_once dirname(__DIR__) . '/public/define.php';
  
  // Using the CLI factory default services container
  $di = new CliDI();
  
  /**
   * Register the autoloader and tell it to register the tasks directory
   */
  $loader = new Loader();
  $loader->registerDirs([
    __DIR__ . "/../module/Cli",
  ]);
  
  $loader->registerNamespaces([
    'Library' => BASE_PATH . '/library',
    'Models'  => BASE_PATH . '/library/Models'
  ]);
  
  $loader->registerFiles([
    BASE_PATH . '/vendor/autoload.php',
  ]);
  
  $loader->register();
  
  $di->setShared('config', function () {
    return include BASE_PATH . "/config/Config.php";
  });
  
  $di->setShared('db', function () {
    $config     = $this->get('config');
    $params     = [
      'host'     => $config->database->host,
      'username' => $config->database->username,
      'password' => $config->database->password,
      'dbname'   => $config->database->dbname,
      'charset'  => $config->database->charset,
    ];
    $connection = new DbAdapter($params);
    
    return $connection;
  });
  
  $di->setShared('queue', function () {
    $config     = $this->get('config');
    $connection = new AMQPStreamConnection($config->queue->host, $config->queue->port, $config->queue->username, $config->queue->password);
    
    return $connection;
  });
  
  // Create a console application
  $console = new ConsoleApp();
  $console->setDI($di);
  
  $arguments = [];
  
  foreach ($argv as $k => $arg) {
    if ($k === 1) {
      $arguments["task"] = $arg;
    } elseif ($k === 2) {
      $arguments["action"] = $arg;
    } elseif ($k >= 3) {
      list($key, $value) = explode('=', $arg);
      $key                         = str_replace('--', '', $key);
      $arguments["params"][ $key ] = $value;
    }
  }
//    var_dump($arguments);die;
  
  try {
    // Handle incoming arguments
    $console->handle($arguments);
  } catch (\Phalcon\Exception $e) {
    echo $e->getMessage();
    exit;
  }
