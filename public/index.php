<?php
//  ini_set('display_startup_errors', 1);
//  ini_set('display_errors', 1);
//  error_reporting(-1);
  
  include_once './define.php';
  
  use Phalcon\Mvc\Application;
  
  try {
    /**
     * ! Include Autoloader
     */
    require BASE_PATH . '/config/loader.php';
    
    /**
     * ! Include services
     */
    require BASE_PATH . '/config/services.php';
    
    /**
     * ! Handle the request
     */
    $application = new Application();
    
    /**
     * ! Assign the DI
     */
    
    $application->setDI($di);
    
    /**
     * ! Include modules
     */
    require BASE_PATH . '/config/module.php';
    
    /**
     * ! Include Routes
     */
    require BASE_PATH . '/config/router.php';
    
    if (APPLICATION_ENV == "dev") {
//      $di['app'] = $application; // debugbar
//
//      (new Snowair\Debugbar\ServiceProvider(BASE_PATH . '/config/debugbar.php'))->start();
//      new Whoops\Provider\Phalcon\WhoopsServiceProvider();
    }
    
    $response = $application->handle($_SERVER["REQUEST_URI"]);
    $response->send();
    
  } catch (Phalcon\Exception $e) {
    echo $e->getMessage();
  } catch (PDOException $e) {
    echo $e->getMessage();
  }
