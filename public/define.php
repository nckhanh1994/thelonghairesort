<?php
  // #env
  defined('APPLICATION_ENV') || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'dev'));
  
  $schema = isset($_SERVER['HTTPS']) ? 'https' : 'http';
  
  $compileDir = 'dist';
  switch (APPLICATION_ENV) {
    case 'production':
      $baseURL   = "$schema://thelonghairesort.net.vn";
      $staticURL = "$schema://static.thelonghairesort.net.vn";
      $apiURL    = "$schema://api.thelonghairesort.net.vn";
      
      break;
    case 'staging':
      $baseURL   = "$schema://dev.thuenha.com";
      $staticURL = "$schema://static.dev.thuenha.com";
      $apiURL    = "$schema://api.dev.thuenha.com";
      
      $compileDir = 'tmp';
      break;
    case 'dev':
      $baseURL   = "$schema://dev.thuenha.com";
      $staticURL = "$schema://static.dev.thuenha.com";
      $apiURL    = "$schema://api.dev.thuenha.com";
      
      $compileDir = 'tmp';
      break;
    default:
      break;
  }
  
  // #path
  define('DIR_PATH', dirname(__DIR__));
  define('COMPILE_DIR', $compileDir);
  define('PUBLIC_PATH', DIR_PATH . '/public');
  define('STATIC_PATH', DIR_PATH . '/static');
  define('UPLOAD_PATH', STATIC_PATH . '/upload');
  define('APP_PATH', DIR_PATH . '/app');
  define('BASE_PATH', APP_PATH . '/base');
  define('COMMON_PATH', APP_PATH . '/common');
  define('MODULE_PATH', APP_PATH . '/module');
  // #url
  define('BASE_URL', $baseURL);
  define('API_URL', "$apiURL");
  define('STATIC_URL', "$staticURL");
  define('FE_IMAGE_URL', STATIC_URL . '/f/images');
  define("UPLOAD_URL", "$staticURL/upload");
  // #
  define('SECRET_KEY', ':)MySuperApp^_^');
  define('PREFIX', 'thuenha_');
  define('BRAND_NAME', 'CPRLAND');
  define('SUPPORT_PHONE', '84799788689');
  define('SUPPORT_EMAIL', 'cprlandteam@gmail.com');
  define('FACEBOOK_FANPAGE_URL', '');
  
  define('CIPHER_METHOD', 'AES-256-CBC');
  
